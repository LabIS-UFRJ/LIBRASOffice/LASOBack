#. extracted from sfx2/source/appl
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2017-01-30 22:39+0000\n"
"Last-Translator: Samson B <sambelet@yahoo.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: am\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1485815989.000000\n"

#: app.src
msgctxt ""
"app.src\n"
"STR_NONAME\n"
"string.text"
msgid "Untitled"
msgstr "ያልተሰየመ"

#: app.src
msgctxt ""
"app.src\n"
"STR_CLOSE\n"
"string.text"
msgid "Close"
msgstr "መዝጊያ"

#: app.src
msgctxt ""
"app.src\n"
"STR_STYLE_FILTER_AUTO\n"
"string.text"
msgid "Automatic"
msgstr "ራሱ በራሱ"

#: app.src
msgctxt ""
"app.src\n"
"STR_STANDARD_SHORTCUT\n"
"string.text"
msgid "Standard"
msgstr "መደበኛ"

#: app.src
msgctxt ""
"app.src\n"
"STR_BYTES\n"
"string.text"
msgid "Bytes"
msgstr "ባይትስ"

#: app.src
msgctxt ""
"app.src\n"
"STR_KB\n"
"string.text"
msgid "KB"
msgstr "ኪ/ባ"

#: app.src
msgctxt ""
"app.src\n"
"STR_MB\n"
"string.text"
msgid "MB"
msgstr "ሜ/ባ"

#: app.src
msgctxt ""
"app.src\n"
"STR_GB\n"
"string.text"
msgid "GB"
msgstr "ጌ/ባ"

#: app.src
msgctxt ""
"app.src\n"
"STR_QUERY_LASTVERSION\n"
"string.text"
msgid "Cancel all changes?"
msgstr "ሁሉንም ለውጦች ልሰርዛቸው?"

#: app.src
msgctxt ""
"app.src\n"
"STR_NO_WEBBROWSER_FOUND\n"
"string.text"
msgid ""
"Opening \"$(ARG1)\" failed with error code $(ARG2) and message: \"$(ARG3)\"\n"
"Maybe no web browser could be found on your system. In that case, please check your Desktop Preferences or install a web browser (for example, Firefox) in the default location requested during the browser installation."
msgstr ""
"መክፈት \"$(ARG1)\" አልተቻለም: የ ስህተት ኮድ $(ARG2) እና መልእክት: \"$(ARG3)\"\n"
"ምናልባት የ ዌብ መቃኛ በ እርስዎ ስርአት ውስጥ አልተገኘም ይሆናል: እባክዎን የ እርስዎን ዴስክቶፕ ምርጫዎች ይመርምሩ ወይንም የ ዌብ መቃኛ ይግጠሙ: (ለምሳሌ: Firefox) በ ነባር ቦታ የ መቃኛ መግጠሚያ በሚጠየቅበት አካባቢ"

#: app.src
msgctxt ""
"app.src\n"
"STR_NO_ABS_URI_REF\n"
"string.text"
msgid "\"$(ARG1)\" is not an absolute URL that can be passed to an external application to open it."
msgstr "\"$(ARG1)\" ፍጹም URL አይደለም የ ውጪ መተግበሪያ ለ መክፈት የሚተላለፍ"

#: app.src
msgctxt ""
"app.src\n"
"GID_INTERN\n"
"string.text"
msgid "Internal"
msgstr "የውስጥ"

#: app.src
msgctxt ""
"app.src\n"
"GID_APPLICATION\n"
"string.text"
msgid "Application"
msgstr "መተግበሪያ"

#: app.src
msgctxt ""
"app.src\n"
"GID_VIEW\n"
"string.text"
msgid "View"
msgstr "መመልከቻ"

#: app.src
msgctxt ""
"app.src\n"
"GID_DOCUMENT\n"
"string.text"
msgid "Documents"
msgstr "ሰነዶች"

#: app.src
msgctxt ""
"app.src\n"
"GID_EDIT\n"
"string.text"
msgid "Edit"
msgstr "ማረሚያ"

#: app.src
msgctxt ""
"app.src\n"
"GID_MACRO\n"
"string.text"
msgid "BASIC"
msgstr "መሰረታዊ"

#: app.src
msgctxt ""
"app.src\n"
"GID_OPTIONS\n"
"string.text"
msgid "Options"
msgstr "ምርጫዎች"

#: app.src
msgctxt ""
"app.src\n"
"GID_MATH\n"
"string.text"
msgid "Math"
msgstr "ሂሳብ"

#: app.src
msgctxt ""
"app.src\n"
"GID_NAVIGATOR\n"
"string.text"
msgid "Navigate"
msgstr "መቃኛ"

#: app.src
msgctxt ""
"app.src\n"
"GID_INSERT\n"
"string.text"
msgid "Insert"
msgstr "ማስገቢያ"

#: app.src
msgctxt ""
"app.src\n"
"GID_FORMAT\n"
"string.text"
msgid "Format"
msgstr "አቀራረብ"

#: app.src
msgctxt ""
"app.src\n"
"GID_TEMPLATE\n"
"string.text"
msgid "Templates"
msgstr "ቴምፕሌትስ"

#: app.src
msgctxt ""
"app.src\n"
"GID_TEXT\n"
"string.text"
msgid "Text"
msgstr "ጽሁፍ"

#: app.src
msgctxt ""
"app.src\n"
"GID_FRAME\n"
"string.text"
msgid "Frame"
msgstr "ጠርዝ"

#: app.src
msgctxt ""
"app.src\n"
"GID_GRAPHIC\n"
"string.text"
msgid "Image"
msgstr "ምስል"

#: app.src
msgctxt ""
"app.src\n"
"GID_TABLE\n"
"string.text"
msgid "Table"
msgstr "ሰንጠረዥ"

#: app.src
msgctxt ""
"app.src\n"
"GID_ENUMERATION\n"
"string.text"
msgid "Numbering"
msgstr "ቁጥር መስጫ"

#: app.src
msgctxt ""
"app.src\n"
"GID_DATA\n"
"string.text"
msgid "Data"
msgstr "ዳታ"

#: app.src
msgctxt ""
"app.src\n"
"GID_SPECIAL\n"
"string.text"
msgid "Special Functions"
msgstr "የተለዩ ተግባሮች"

#: app.src
msgctxt ""
"app.src\n"
"GID_IMAGE\n"
"string.text"
msgid "Image"
msgstr "ምስል"

#: app.src
msgctxt ""
"app.src\n"
"GID_CHART\n"
"string.text"
msgid "Chart"
msgstr "ቻርት"

#: app.src
msgctxt ""
"app.src\n"
"GID_EXPLORER\n"
"string.text"
msgid "Explorer"
msgstr "መቃኛ"

#: app.src
msgctxt ""
"app.src\n"
"GID_CONNECTOR\n"
"string.text"
msgid "Connector"
msgstr "አገናኝ"

#: app.src
msgctxt ""
"app.src\n"
"GID_MODIFY\n"
"string.text"
msgid "Modify"
msgstr "ማሻሻያ"

#: app.src
msgctxt ""
"app.src\n"
"GID_DRAWING\n"
"string.text"
msgid "Drawing"
msgstr "መሳያ"

#: app.src
msgctxt ""
"app.src\n"
"GID_CONTROLS\n"
"string.text"
msgid "Controls"
msgstr "መቆጣጠሪያ"

#: app.src
msgctxt ""
"app.src\n"
"STR_ISMODIFIED\n"
"string.text"
msgid "Do you want to save the changes to %1?"
msgstr "ለውጦቹን ማስቀመጥ ይፈልጋሉ ወደ %1?"

#: app.src
msgctxt ""
"app.src\n"
"STR_QUITAPP\n"
"string.text"
msgid "E~xit %PRODUCTNAME"
msgstr "መ~ውጫ %PRODUCTNAME"

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_HELP\n"
"string.text"
msgid "Help"
msgstr "እርዳታ"

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_NOAUTOSTARTHELPAGENT\n"
"string.text"
msgid "No automatic start at 'XX'"
msgstr "ራሱ በራሱ አይጀምር በ 'XX'"

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_HLPFILENOTEXIST\n"
"string.text"
msgid "The help file for this topic is not installed."
msgstr "ለዚህ አርእስት የእርዳታ ፋይሉ አልተገጠመም"

#: app.src
msgctxt ""
"app.src\n"
"STR_QUICKSTART_EXIT\n"
"string.text"
msgid "Exit Quickstarter"
msgstr "ከ በፍጥነት ማስጀመሪያ መውጫ"

#: app.src
msgctxt ""
"app.src\n"
"STR_QUICKSTART_TIP\n"
"string.text"
msgid "%PRODUCTNAME %PRODUCTVERSION Quickstarter"
msgstr "%PRODUCTNAME %PRODUCTVERSION በፍጥነት ማስጀመሪያ"

#: app.src
msgctxt ""
"app.src\n"
"STR_QUICKSTART_FILEOPEN\n"
"string.text"
msgid "Open Document..."
msgstr "ሰነድ መክፈቻ..."

#: app.src
msgctxt ""
"app.src\n"
"STR_QUICKSTART_FROMTEMPLATE\n"
"string.text"
msgid "From Template..."
msgstr "ከ ቴምፕሌት..."

#: app.src
msgctxt ""
"app.src\n"
"STR_QUICKSTART_PRELAUNCH\n"
"string.text"
msgid "Load %PRODUCTNAME During System Start-Up"
msgstr "መጫኛ %PRODUCTNAME ስርአቱ በሚጀምር-ጊዜ"

#: app.src
msgctxt ""
"app.src\n"
"STR_QUICKSTART_PRELAUNCH_UNX\n"
"string.text"
msgid "Disable systray Quickstarter"
msgstr "በ ስርአቱ ትሪ ላይ በፍጥነት ማጥፊያ"

#: app.src
msgctxt ""
"app.src\n"
"STR_QUICKSTART_LNKNAME\n"
"string.text"
msgid "%PRODUCTNAME %PRODUCTVERSION"
msgstr "%PRODUCTNAME %PRODUCTVERSION"

#: app.src
msgctxt ""
"app.src\n"
"STR_QUICKSTART_FILE\n"
"string.text"
msgid "File"
msgstr "ፋይል"

#: app.src
msgctxt ""
"app.src\n"
"STR_QUICKSTART_STARTCENTER\n"
"string.text"
msgid "Startcenter"
msgstr "መሀከል መጀመሪያ"

#: app.src
msgctxt ""
"app.src\n"
"STR_QUICKSTART_RECENTDOC\n"
"string.text"
msgid "Recent Documents"
msgstr "የ ቅርብ ጊዜ ሰነዶች"

#: app.src
msgctxt ""
"app.src\n"
"STR_QUERY_UPDATE_LINKS\n"
"string.text"
msgid ""
"This document contains one or more links to external data.\n"
"\n"
"Would you like to change the document, and update all links\n"
"to get the most recent data?"
msgstr ""
"ይህ ሰነድ አንድ ወይም ከዚያ በላይ ወደ ውጪ አገናኝ ዳታ ይዟል \n"
"\n"
"ሰነዱን መቀየር እና አገናኞቹን በሙሉ ማሻሻል ይፈልጋሉ\n"
"በጣም የ ቅርብ ጊዜ ሰነዶችን ለማግኘት?"

#: app.src
msgctxt ""
"app.src\n"
"STR_DDE_ERROR\n"
"string.text"
msgid "DDE link to %1 for %2 area %3 are not available."
msgstr "DDE አገናኝ ወደ %1 ለ %2 ቦታ %3 ዝግጁ አይደለም"

#: app.src
msgctxt ""
"app.src\n"
"STR_SECURITY_WARNING_NO_HYPERLINKS\n"
"string.text"
msgid ""
"For security reasons, the hyperlink cannot be executed.\n"
"The stated address will not be opened."
msgstr ""
"በደህንነት ምክንያት ፡ ይህ hyperlink cannot be executed.\n"
"የ ተፈለገውን አድራሻ አይከፈትም"

#: app.src
msgctxt ""
"app.src\n"
"RID_SECURITY_WARNING_TITLE\n"
"string.text"
msgid "Security Warning"
msgstr "የደህንነት ማስጠንቀቂያ"

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_XMLSEC_QUERY_LOSINGSIGNATURE\n"
"string.text"
msgid ""
"Saving will remove all existing signatures.\n"
"Do you want to continue saving the document?"
msgstr ""
"ማስቀመጥ ቀደም ሲል የነበሩትን ፊርማዎች ያስወግዳቸዋል \n"
"ሰነዱን ማስቀመጥ መቀጠል ይፈልጋሉ?"

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_XMLSEC_QUERY_SAVEBEFORESIGN\n"
"string.text"
msgid ""
"The document has to be saved before it can be signed.\n"
"Do you want to save the document?"
msgstr ""
"ሰነዱ ከመፈረሙ በፊት መቀመጥ አለበት \n"
"ሰነዱን ማስቀመጥ ይፈልጋሉ?"

#: app.src
msgctxt ""
"app.src\n"
"STR_QUERY_CANCELCHECKOUT\n"
"string.text"
msgid ""
"This will discard all changes on the server since check-out.\n"
"Do you want to proceed?"
msgstr ""
"በ ሰርቨሩ ላይ ያለውን ለውጦች በሙሉ ያስወግዳል መጨረሻ-ከወጡበት ጊዜ ጀምሮ ያለውን \n"
"መቀጠል ይፈልጋሉ?"

#: app.src
msgctxt ""
"app.src\n"
"STR_INFO_WRONGDOCFORMAT\n"
"string.text"
msgid "This document must be saved in OpenDocument file format before it can be digitally signed."
msgstr "ይህ ሰነድ በ OpenDocument file format መቀመጥ አለበት: ዲጂታሊ ከመፈረሙ በፊት"

#: app.src
msgctxt ""
"app.src\n"
"RID_XMLSEC_DOCUMENTSIGNED\n"
"string.text"
msgid " (Signed)"
msgstr " (የተፈረመ)"

#: app.src
msgctxt ""
"app.src\n"
"STR_STANDARD\n"
"string.text"
msgid "Standard"
msgstr "መደበኛ"

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_FILELINK\n"
"string.text"
msgid "Document"
msgstr "ሰነድ"

#: app.src
msgctxt ""
"app.src\n"
"STR_NONE\n"
"string.text"
msgid "- None -"
msgstr "- ምንም -"

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_GRAFIKLINK\n"
"string.text"
msgid "Image"
msgstr "ምስል"

#: app.src
msgctxt ""
"app.src\n"
"STR_SFX_FILTERNAME_ALL\n"
"string.text"
msgid "All files"
msgstr "ሁሉንም ፋይሎች"

#: app.src
msgctxt ""
"app.src\n"
"STR_SFX_FILTERNAME_PDF\n"
"string.text"
msgid "PDF files"
msgstr "የ PDF ፋይሎች"

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_EDITGRFLINK\n"
"string.text"
msgid "Link Image"
msgstr "ምስል አገናኝ"

#: app.src
msgctxt ""
"app.src\n"
"STR_ERRUNOEVENTBINDUNG\n"
"string.text"
msgid ""
"An appropriate component method %1\n"
"could not be found.\n"
"\n"
"Check spelling of method name."
msgstr ""
"ትክክለኛውን የአካሉን ዘዴ %1\n"
"ማግኘት አልተቻለም \n"
"\n"
"የዘዴውን ስም አፃፃፍ ትክክል መሆኑን ይመርምሩ"

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_GRFILTER_OPENERROR\n"
"string.text"
msgid "Image file cannot be opened"
msgstr "የ ምስል ፋይሉን መክፈት አልተቻለም"

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_GRFILTER_IOERROR\n"
"string.text"
msgid "Image file cannot be read"
msgstr "የ ምስል ፋይሉን ማንበብ አልተቻለም"

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_GRFILTER_FORMATERROR\n"
"string.text"
msgid "Unknown image format"
msgstr "ያልታወቀ የ ምስል አቀራረብ"

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_GRFILTER_VERSIONERROR\n"
"string.text"
msgid "This version of the image file is not supported"
msgstr "የዚህ እትም ምስል ፋይል የተደገፈ አይደለም"

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_GRFILTER_FILTERERROR\n"
"string.text"
msgid "Image filter not found"
msgstr "የ ምስል ማጣሪያ አልተገኘም"

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_GRFILTER_TOOBIG\n"
"string.text"
msgid "Not enough memory to insert image"
msgstr "ምስል ለማስገባት በቂ memory የለም"

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_END_REDLINING_WARNING\n"
"string.text"
msgid ""
"This action will exit the change recording mode.\n"
"Any information about changes will be lost.\n"
"\n"
"Exit change recording mode?\n"
"\n"
msgstr ""
"ይህ ተግባር ከመመዝገቢያ መቀየሪያ ዘዴ ይወጣል \n"
"ስለ ለውጡ የነበሩ ማናቸውም መረጃዎች ይጠፋሉ \n"
"\n"
"ከመመዝገቢያ መቀየሪያ ዘዴ ልውጣ?\n"
"\n"

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_INCORRECT_PASSWORD\n"
"string.text"
msgid "Incorrect password"
msgstr "የተሳሳተ የመግቢያ ቃል"

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_FORWARD_ERRMSSG\n"
"string.text"
msgid "If you select the option \"%PLACEHOLDER%\", you must enter a URL."
msgstr "ይህን ምርጫ ከመረጡ \"%PLACEHOLDER%\", URL ማስገባት አለብዎት"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_WINDOW_TITLE\n"
"string.text"
msgid "%PRODUCTNAME Help"
msgstr "%PRODUCTNAME እርዳታ"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_BUTTON_INDEX_ON\n"
"string.text"
msgid "Show Navigation Pane"
msgstr "የመቃኛ ክፍል ማሳያ"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_BUTTON_INDEX_OFF\n"
"string.text"
msgid "Hide Navigation Pane"
msgstr "የ መቃኛ ክፍል መደበቂያ"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_BUTTON_START\n"
"string.text"
msgid "First Page"
msgstr "የመጀመሪያ ገጽ"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_BUTTON_PREV\n"
"string.text"
msgid "Previous Page"
msgstr "ቀደም ያለው ገጽ"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_BUTTON_NEXT\n"
"string.text"
msgid "Next Page"
msgstr "የሚቀጥለው ገጽ"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_BUTTON_PRINT\n"
"string.text"
msgid "Print..."
msgstr "ማተሚያ..."

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_BUTTON_ADDBOOKMARK\n"
"string.text"
msgid "Add to Bookmarks..."
msgstr "ወደ ምልክት ማድረጊያው መጨመሪያ..."

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_BUTTON_SEARCHDIALOG\n"
"string.text"
msgid "Find on this Page..."
msgstr "በዚህ ገጽ ላይ መፈለጊያ..."

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_BUTTON_SOURCEVIEW\n"
"string.text"
msgid "HTML Source"
msgstr "HTML Source"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_FIRST_MESSAGE\n"
"string.text"
msgid "The Help is being started..."
msgstr "እርዳታው እየጀመረ ነው..."

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_MENU_TEXT_SELECTION_MODE\n"
"string.text"
msgid "Select Text"
msgstr "ጽሁፍ ይምረጡ"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_MENU_TEXT_COPY\n"
"string.text"
msgid "~Copy"
msgstr "~ኮፒ"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"MENU_HELP_BOOKMARKS\n"
"MID_OPEN\n"
"menuitem.text"
msgid "Display"
msgstr "ማሳያ"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"MENU_HELP_BOOKMARKS\n"
"MID_RENAME\n"
"menuitem.text"
msgid "Rename..."
msgstr "እንደገና መሰየሚያ..."

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"MENU_HELP_BOOKMARKS\n"
"MID_DELETE\n"
"menuitem.text"
msgid "Delete"
msgstr "ማጥፊያ"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_INFO_NOSEARCHRESULTS\n"
"string.text"
msgid "No topics found."
msgstr "ምንም አርእስት አልተገኘም"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_INFO_NOSEARCHTEXTFOUND\n"
"string.text"
msgid "The text you entered was not found."
msgstr "ያስገቡት ጽሁፍ አልተገኘም"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"RID_HELP_ONSTARTUP_TEXT\n"
"string.text"
msgid "~Display %PRODUCTNAME %MODULENAME Help at Startup"
msgstr "~ማሳያ %PRODUCTNAME %MODULENAME እርዳታ በሚጀምር ጊዜ"

#: sfx.src
msgctxt ""
"sfx.src\n"
"STR_ACCTITLE_PRODUCTIVITYTOOLS\n"
"string.text"
msgid "%PRODUCTNAME"
msgstr "%PRODUCTNAME"

#: sfx.src
msgctxt ""
"sfx.src\n"
"STR_PASSWD_MIN_LEN\n"
"string.text"
msgid "(Minimum $(MINLEN) characters)"
msgstr "(አነስተኛ $(MINLEN) ባህሪዎች)"

#: sfx.src
msgctxt ""
"sfx.src\n"
"STR_PASSWD_MIN_LEN1\n"
"string.text"
msgid "(Minimum 1 character)"
msgstr "(አነስተኛ 1 ባህሪ)"

#: sfx.src
msgctxt ""
"sfx.src\n"
"STR_PASSWD_EMPTY\n"
"string.text"
msgid "(The password can be empty)"
msgstr "(የመግቢያ ቃሉ ባዶ ሊሆን ይችላል)"

#: sfx.src
msgctxt ""
"sfx.src\n"
"STR_MODULENOTINSTALLED\n"
"string.text"
msgid "The action could not be executed. The %PRODUCTNAME program module needed for this action is currently not installed."
msgstr "ተግባሩን ማስኬድ አልተቻለም የ %PRODUCTNAME ፕሮግራሙ ክፍል ለዚህ ተግባር እስከ አሁን አልተገጠመም"

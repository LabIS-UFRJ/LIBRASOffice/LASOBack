#. extracted from basctl/uiconfig/basicide/ui
msgid ""
msgstr ""
"Project-Id-Version: LibO 40l10n\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-04-22 23:40+0200\n"
"PO-Revision-Date: 2015-12-13 13:43+0000\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: none\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 2.7\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1450014213.000000\n"

#: basicmacrodialog.ui
msgctxt ""
"basicmacrodialog.ui\n"
"BasicMacroDialog\n"
"title\n"
"string.text"
msgid "%PRODUCTNAME Basic Macros"
msgstr "מאקרואים בשפת בייסיק של ‏‪%PRODUCTNAME‬‏‏"

#: basicmacrodialog.ui
msgctxt ""
"basicmacrodialog.ui\n"
"run\n"
"label\n"
"string.text"
msgid "Run"
msgstr "הרצה"

#: basicmacrodialog.ui
msgctxt ""
"basicmacrodialog.ui\n"
"assign\n"
"label\n"
"string.text"
msgid "Assign..."
msgstr "הקצאה..."

#: basicmacrodialog.ui
msgctxt ""
"basicmacrodialog.ui\n"
"edit\n"
"label\n"
"string.text"
msgid "Edit"
msgstr "עריכה"

#: basicmacrodialog.ui
msgctxt ""
"basicmacrodialog.ui\n"
"organize\n"
"label\n"
"string.text"
msgid "Organizer..."
msgstr "ארגון..."

#: basicmacrodialog.ui
msgctxt ""
"basicmacrodialog.ui\n"
"newlibrary\n"
"label\n"
"string.text"
msgid "New Library"
msgstr "ספריה חדשה"

#: basicmacrodialog.ui
msgctxt ""
"basicmacrodialog.ui\n"
"newmodule\n"
"label\n"
"string.text"
msgid "New Module"
msgstr "מודול חדש"

#: basicmacrodialog.ui
msgctxt ""
"basicmacrodialog.ui\n"
"existingmacrosft\n"
"label\n"
"string.text"
msgid "Existing Macros In:"
msgstr "פקודות מאקרו קיימות:"

#: basicmacrodialog.ui
msgctxt ""
"basicmacrodialog.ui\n"
"macrofromft\n"
"label\n"
"string.text"
msgid "Macro From"
msgstr "מאקרו מ-"

#: basicmacrodialog.ui
msgctxt ""
"basicmacrodialog.ui\n"
"macrotoft\n"
"label\n"
"string.text"
msgid "Save Macro In"
msgstr "שמירת מאקרו ב-"

#: basicmacrodialog.ui
msgctxt ""
"basicmacrodialog.ui\n"
"libraryft1\n"
"label\n"
"string.text"
msgid "Macro Name"
msgstr "שם מאקרו"

#: defaultlanguage.ui
msgctxt ""
"defaultlanguage.ui\n"
"DefaultLanguageDialog\n"
"title\n"
"string.text"
msgid "Set Default User Interface Language"
msgstr "הגדרת שפת מנשק המשתמש כבררת מחדל"

#: defaultlanguage.ui
msgctxt ""
"defaultlanguage.ui\n"
"defaultlabel\n"
"label\n"
"string.text"
msgid "Default language:"
msgstr "שפת בררת המחדל"

#: defaultlanguage.ui
msgctxt ""
"defaultlanguage.ui\n"
"checkedlabel\n"
"label\n"
"string.text"
msgid "Available languages:"
msgstr "שפות זמינות:"

#: defaultlanguage.ui
msgctxt ""
"defaultlanguage.ui\n"
"defined\n"
"label\n"
"string.text"
msgid "Select a language to define the default user interface language. All currently present strings will be assigned to the resources created for the selected language."
msgstr "נא לבחור בשפה שתשמש כשפת בררת המחדל למנשק המשתמש. כל המחרוזות שקיימות כרגע יוקצו למשאבים שנוצרו עבור השפה הנבחרת."

#: defaultlanguage.ui
msgctxt ""
"defaultlanguage.ui\n"
"added\n"
"label\n"
"string.text"
msgid "Select languages to be added. Resources for these languages will be created in the library. Strings of the current default user interface language will be copied to these new resources by default."
msgstr "נא לבחור בשפות להוספה. המשאבים עבור שפות אלו יווצרו בספרייה. מחרוזות בררת המחדל למנשק המשתמש יועתקו למשאבים החדשים האלה כבררת מחדל."

#: defaultlanguage.ui
msgctxt ""
"defaultlanguage.ui\n"
"alttitle\n"
"label\n"
"string.text"
msgid "Add User Interface Languages"
msgstr "הוספות שפות מנשק משתמש"

#: deletelangdialog.ui
msgctxt ""
"deletelangdialog.ui\n"
"DeleteLangDialog\n"
"title\n"
"string.text"
msgid "Delete Language Resources"
msgstr "מחיקת משאבי שפה"

#: deletelangdialog.ui
msgctxt ""
"deletelangdialog.ui\n"
"DeleteLangDialog\n"
"text\n"
"string.text"
msgid "Do you want to delete the resources of the selected language(s)?"
msgstr "האם למחוק את המשאבים של השפות הנבחרות?"

#: deletelangdialog.ui
msgctxt ""
"deletelangdialog.ui\n"
"DeleteLangDialog\n"
"secondary_text\n"
"string.text"
msgid "You are about to delete the resources for the selected language(s). All user interface strings for this language(s) will be deleted."
msgstr "פעולה זו תמחק את המשאבים עבור השפות הנבחרות. כל מחרוזות מנשק המשתמש עבור שפה זו תימחקנה."

#: dialogpage.ui
msgctxt ""
"dialogpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Dialog:"
msgstr "תיבת דו־שיח:"

#: dialogpage.ui
msgctxt ""
"dialogpage.ui\n"
"newmodule\n"
"label\n"
"string.text"
msgid "_New..."
msgstr "_חדש..."

#: dialogpage.ui
msgctxt ""
"dialogpage.ui\n"
"newdialog\n"
"label\n"
"string.text"
msgid "_New..."
msgstr "_חדש..."

#: dialogpage.ui
msgctxt ""
"dialogpage.ui\n"
"password\n"
"label\n"
"string.text"
msgid "_Password..."
msgstr "_ססמה..."

#: dialogpage.ui
msgctxt ""
"dialogpage.ui\n"
"import\n"
"label\n"
"string.text"
msgid "_Import..."
msgstr "י_בוא..."

#: dialogpage.ui
msgctxt ""
"dialogpage.ui\n"
"export\n"
"label\n"
"string.text"
msgid "_Export..."
msgstr "י_צוא..."

#: exportdialog.ui
msgctxt ""
"exportdialog.ui\n"
"ExportDialog\n"
"title\n"
"string.text"
msgid "Export Basic library"
msgstr "יצוא ספריית Basic"

#: exportdialog.ui
msgctxt ""
"exportdialog.ui\n"
"extension\n"
"label\n"
"string.text"
msgid "Export as _extension"
msgstr "יצוא כה_רחבה"

#: exportdialog.ui
msgctxt ""
"exportdialog.ui\n"
"basic\n"
"label\n"
"string.text"
msgid "Export as BASIC library"
msgstr "יצוא כספריית BASIC"

#: gotolinedialog.ui
msgctxt ""
"gotolinedialog.ui\n"
"GotoLineDialog\n"
"title\n"
"string.text"
msgid "Go to Line"
msgstr "מעבר לשורה"

#: gotolinedialog.ui
msgctxt ""
"gotolinedialog.ui\n"
"area\n"
"label\n"
"string.text"
msgid "_Line number:"
msgstr "מ_ספר שורה:"

#: importlibdialog.ui
msgctxt ""
"importlibdialog.ui\n"
"ImportLibDialog\n"
"title\n"
"string.text"
msgid "Import Libraries"
msgstr "יבוא ספריות"

#: importlibdialog.ui
msgctxt ""
"importlibdialog.ui\n"
"ref\n"
"label\n"
"string.text"
msgid "Insert as reference (read-only)"
msgstr "הוספה כהפנייה (קריאה בלבד)"

#: importlibdialog.ui
msgctxt ""
"importlibdialog.ui\n"
"replace\n"
"label\n"
"string.text"
msgid "Replace existing libraries"
msgstr "החלפת ספריות קיימות"

#: importlibdialog.ui
msgctxt ""
"importlibdialog.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Options"
msgstr "אפשרויות"

#: libpage.ui
msgctxt ""
"libpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "L_ocation:"
msgstr "מי_קום:"

#: libpage.ui
msgctxt ""
"libpage.ui\n"
"lingudictsft\n"
"label\n"
"string.text"
msgid "_Library:"
msgstr "ס_פרייה:"

#: libpage.ui
msgctxt ""
"libpage.ui\n"
"password\n"
"label\n"
"string.text"
msgid "_Password..."
msgstr "_ססמה..."

#: libpage.ui
msgctxt ""
"libpage.ui\n"
"new\n"
"label\n"
"string.text"
msgid "_New..."
msgstr "_חדש..."

#: libpage.ui
msgctxt ""
"libpage.ui\n"
"import\n"
"label\n"
"string.text"
msgid "_Import..."
msgstr "י_בוא..."

#: libpage.ui
msgctxt ""
"libpage.ui\n"
"export\n"
"label\n"
"string.text"
msgid "_Export..."
msgstr "י_צוא..."

#: managebreakpoints.ui
msgctxt ""
"managebreakpoints.ui\n"
"ManageBreakpointsDialog\n"
"title\n"
"string.text"
msgid "Manage Breakpoints"
msgstr "ניהול נקודות הפוגה"

#: managebreakpoints.ui
msgctxt ""
"managebreakpoints.ui\n"
"active\n"
"label\n"
"string.text"
msgid "Active"
msgstr "פעיל"

#: managebreakpoints.ui
msgctxt ""
"managebreakpoints.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Pass count:"
msgstr "ספירת מעברים:"

#: managebreakpoints.ui
msgctxt ""
"managebreakpoints.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Breakpoints"
msgstr "נקודות עצירה"

#: managelanguages.ui
msgctxt ""
"managelanguages.ui\n"
"ManageLanguagesDialog\n"
"title\n"
"string.text"
msgid "Manage User Interface Languages [$1]"
msgstr "ניהול שפות מנשק המשתמש [$1]"

#: managelanguages.ui
msgctxt ""
"managelanguages.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Present languages:"
msgstr "שפות נוכחיות:"

#: managelanguages.ui
msgctxt ""
"managelanguages.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "The default language is used if no localization for a user interface locale is present. Furthermore all strings from the default language are copied to resources of newly added languages."
msgstr "יעשה שימוש בשפת בררת המחדל אם לא קיים תרגום לשפת המנשק הנוכחית של המשתמש. יתר על כך, כל המחרוזות משפת בררת המחדל מועתקות למשאבים של שפות חדשות שנוספות."

#: managelanguages.ui
msgctxt ""
"managelanguages.ui\n"
"add\n"
"label\n"
"string.text"
msgid "Add..."
msgstr "הוספה..."

#: managelanguages.ui
msgctxt ""
"managelanguages.ui\n"
"default\n"
"label\n"
"string.text"
msgid "Default"
msgstr "בררת מחדל"

#: modulepage.ui
msgctxt ""
"modulepage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "M_odule:"
msgstr "מו_דול:"

#: modulepage.ui
msgctxt ""
"modulepage.ui\n"
"newmodule\n"
"label\n"
"string.text"
msgid "_New..."
msgstr "_חדש..."

#: modulepage.ui
msgctxt ""
"modulepage.ui\n"
"newdialog\n"
"label\n"
"string.text"
msgid "_New..."
msgstr "_חדש..."

#: modulepage.ui
msgctxt ""
"modulepage.ui\n"
"password\n"
"label\n"
"string.text"
msgid "_Password..."
msgstr "_ססמה..."

#: modulepage.ui
msgctxt ""
"modulepage.ui\n"
"import\n"
"label\n"
"string.text"
msgid "_Import..."
msgstr "י_בוא..."

#: modulepage.ui
msgctxt ""
"modulepage.ui\n"
"export\n"
"label\n"
"string.text"
msgid "_Export..."
msgstr "י_צוא..."

#: newlibdialog.ui
msgctxt ""
"newlibdialog.ui\n"
"area\n"
"label\n"
"string.text"
msgid "_Name:"
msgstr "_שם:"

#: organizedialog.ui
msgctxt ""
"organizedialog.ui\n"
"OrganizeDialog\n"
"title\n"
"string.text"
msgid "%PRODUCTNAME Basic Macro Organizer"
msgstr "מארגן תסריטי המאקרו בשפת Basic של %PRODUCTNAME"

#: organizedialog.ui
msgctxt ""
"organizedialog.ui\n"
"modules\n"
"label\n"
"string.text"
msgid "Modules"
msgstr "מודולים"

#: organizedialog.ui
msgctxt ""
"organizedialog.ui\n"
"dialogs\n"
"label\n"
"string.text"
msgid "Dialogs"
msgstr "תיבות דו־שיח"

#: organizedialog.ui
msgctxt ""
"organizedialog.ui\n"
"libraries\n"
"label\n"
"string.text"
msgid "Libraries"
msgstr "ספריות"

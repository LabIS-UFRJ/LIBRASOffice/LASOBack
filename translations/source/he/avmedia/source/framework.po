#. extracted from avmedia/source/framework
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-12-22 08:45+0000\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1482396328.000000\n"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_OPEN\n"
"string.text"
msgid "Open"
msgstr "פתיחה"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_INSERT\n"
"string.text"
msgid "Apply"
msgstr "להחיל"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_PLAY\n"
"string.text"
msgid "Play"
msgstr "לנגן"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_PAUSE\n"
"string.text"
msgid "Pause"
msgstr "השהייה"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_STOP\n"
"string.text"
msgid "Stop"
msgstr "לעצור"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_ENDLESS\n"
"string.text"
msgid "Repeat"
msgstr "חזרה על פעולה"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_MUTE\n"
"string.text"
msgid "Mute"
msgstr "עמעום"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_ZOOM\n"
"string.text"
msgid "View"
msgstr "תצוגה"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_ZOOM_50\n"
"string.text"
msgid "50%"
msgstr "50%"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_ZOOM_100\n"
"string.text"
msgid "100%"
msgstr "100%"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_ZOOM_200\n"
"string.text"
msgid "200%"
msgstr "200%"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_ZOOM_FIT\n"
"string.text"
msgid "Scaled"
msgstr "בהתאמת ממדים"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_MEDIAPLAYER\n"
"string.text"
msgid "Media Player"
msgstr "נגן מדיה"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_MEDIA_PATH\n"
"string.text"
msgid "Media Path"
msgstr "נתיב למדיה"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_MEDIA_PATH_DEFAULT\n"
"string.text"
msgid "No Media Selected"
msgstr "לא נבחרה מדיה"

#. extracted from sc/source/ui/navipi
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2015-12-10 19:31+0000\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1449775915.000000\n"

#: navipi.src
msgctxt ""
"navipi.src\n"
"SCSTR_CONTENT_ROOT\n"
"string.text"
msgid "Contents"
msgstr "תוכן עניינים"

#: navipi.src
msgctxt ""
"navipi.src\n"
"SCSTR_CONTENT_TABLE\n"
"string.text"
msgid "Sheets"
msgstr "גיליונות"

#: navipi.src
msgctxt ""
"navipi.src\n"
"SCSTR_CONTENT_RANGENAME\n"
"string.text"
msgid "Range names"
msgstr "שמות טווחים"

#: navipi.src
msgctxt ""
"navipi.src\n"
"SCSTR_CONTENT_DBAREA\n"
"string.text"
msgid "Database ranges"
msgstr "טווחי מסד נתונים"

#: navipi.src
msgctxt ""
"navipi.src\n"
"SCSTR_CONTENT_GRAPHIC\n"
"string.text"
msgid "Images"
msgstr "תמונות"

#: navipi.src
msgctxt ""
"navipi.src\n"
"SCSTR_CONTENT_OLEOBJECT\n"
"string.text"
msgid "OLE objects"
msgstr "עצם ‏‪OLE‬‏"

#: navipi.src
msgctxt ""
"navipi.src\n"
"SCSTR_CONTENT_NOTE\n"
"string.text"
msgid "Comments"
msgstr "הערות"

#: navipi.src
msgctxt ""
"navipi.src\n"
"SCSTR_CONTENT_AREALINK\n"
"string.text"
msgid "Linked areas"
msgstr "אזורים מקושרים"

#: navipi.src
msgctxt ""
"navipi.src\n"
"SCSTR_CONTENT_DRAWING\n"
"string.text"
msgid "Drawing objects"
msgstr "עצם סרטוט"

#: navipi.src
msgctxt ""
"navipi.src\n"
"SCSTR_DRAGMODE\n"
"string.text"
msgid "Drag Mode"
msgstr ""

#: navipi.src
msgctxt ""
"navipi.src\n"
"SCSTR_DISPLAY\n"
"string.text"
msgid "Display"
msgstr ""

#: navipi.src
msgctxt ""
"navipi.src\n"
"SCSTR_ACTIVE\n"
"string.text"
msgid "active"
msgstr ""

#: navipi.src
msgctxt ""
"navipi.src\n"
"SCSTR_NOTACTIVE\n"
"string.text"
msgid "inactive"
msgstr ""

#: navipi.src
msgctxt ""
"navipi.src\n"
"SCSTR_HIDDEN\n"
"string.text"
msgid "hidden"
msgstr ""

#: navipi.src
msgctxt ""
"navipi.src\n"
"SCSTR_ACTIVEWIN\n"
"string.text"
msgid "Active Window"
msgstr ""

#: navipi.src
msgctxt ""
"navipi.src\n"
"SCSTR_QHLP_SCEN_LISTBOX\n"
"string.text"
msgid "Scenario Name"
msgstr ""

#: navipi.src
msgctxt ""
"navipi.src\n"
"SCSTR_QHLP_SCEN_COMMENT\n"
"string.text"
msgid "Comment"
msgstr ""

#: navipi.src
msgctxt ""
"navipi.src\n"
"RID_POPUP_DROPMODE\n"
"RID_DROPMODE_URL\n"
"menuitem.text"
msgid "Insert as Hyperlink"
msgstr "הכנסה קישור"

#: navipi.src
msgctxt ""
"navipi.src\n"
"RID_POPUP_DROPMODE\n"
"RID_DROPMODE_LINK\n"
"menuitem.text"
msgid "Insert as Link"
msgstr "הכנסה כקישור"

#: navipi.src
msgctxt ""
"navipi.src\n"
"RID_POPUP_DROPMODE\n"
"RID_DROPMODE_COPY\n"
"menuitem.text"
msgid "Insert as Copy"
msgstr "הכנסה כהעתק"

#: navipi.src
msgctxt ""
"navipi.src\n"
"RID_POPUP_NAVIPI_SCENARIO\n"
"RID_NAVIPI_SCENARIO_DELETE\n"
"menuitem.text"
msgid "Delete"
msgstr "מחיקה"

#: navipi.src
msgctxt ""
"navipi.src\n"
"RID_POPUP_NAVIPI_SCENARIO\n"
"RID_NAVIPI_SCENARIO_EDIT\n"
"menuitem.text"
msgid "Properties..."
msgstr "תכונות...‏"

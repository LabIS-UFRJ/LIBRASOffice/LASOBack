#. extracted from reportdesign/uiconfig/dbreport/ui
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2017-02-18 11:44+0000\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1487418243.000000\n"

#: backgrounddialog.ui
msgctxt ""
"backgrounddialog.ui\n"
"BackgroundDialog\n"
"title\n"
"string.text"
msgid "Section Setup"
msgstr "הגדרת מקטע"

#: backgrounddialog.ui
msgctxt ""
"backgrounddialog.ui\n"
"background\n"
"label\n"
"string.text"
msgid "Background"
msgstr "רקע"

#: chardialog.ui
msgctxt ""
"chardialog.ui\n"
"CharDialog\n"
"title\n"
"string.text"
msgid "Character Settings"
msgstr "הגדרות תווים"

#: chardialog.ui
msgctxt ""
"chardialog.ui\n"
"font\n"
"label\n"
"string.text"
msgid "Font"
msgstr "גופן"

#: chardialog.ui
msgctxt ""
"chardialog.ui\n"
"fonteffects\n"
"label\n"
"string.text"
msgid "Font Effects"
msgstr "אפקטים של גופנים"

#: chardialog.ui
msgctxt ""
"chardialog.ui\n"
"position\n"
"label\n"
"string.text"
msgid "Position"
msgstr "מיקום"

#: chardialog.ui
msgctxt ""
"chardialog.ui\n"
"asianlayout\n"
"label\n"
"string.text"
msgid "Asian Layout"
msgstr "פריסת מזרח רחוק"

#: chardialog.ui
msgctxt ""
"chardialog.ui\n"
"background\n"
"label\n"
"string.text"
msgid "Highlighting"
msgstr "הדגשה"

#: chardialog.ui
msgctxt ""
"chardialog.ui\n"
"alignment\n"
"label\n"
"string.text"
msgid "Alignment"
msgstr "יישור"

#: condformatdialog.ui
msgctxt ""
"condformatdialog.ui\n"
"CondFormat\n"
"title\n"
"string.text"
msgid "Conditional Formatting"
msgstr "עיצוב מותנה"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"typeCombobox\n"
"0\n"
"stringlist.text"
msgid "Field Value Is"
msgstr "ערך השדה הוא"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"typeCombobox\n"
"1\n"
"stringlist.text"
msgid "Expression Is"
msgstr "הביטוי הוא"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"opCombobox\n"
"0\n"
"stringlist.text"
msgid "between"
msgstr "בין"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"opCombobox\n"
"1\n"
"stringlist.text"
msgid "not between"
msgstr "לא בין"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"opCombobox\n"
"2\n"
"stringlist.text"
msgid "equal to"
msgstr "שווה ל־"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"opCombobox\n"
"3\n"
"stringlist.text"
msgid "not equal to"
msgstr "שונה מ־"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"opCombobox\n"
"4\n"
"stringlist.text"
msgid "greater than"
msgstr "גדול מ־"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"opCombobox\n"
"5\n"
"stringlist.text"
msgid "less than"
msgstr "קטן מ־"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"opCombobox\n"
"6\n"
"stringlist.text"
msgid "greater than or equal to"
msgstr "גדול או שווה ל־"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"opCombobox\n"
"7\n"
"stringlist.text"
msgid "less than or equal to"
msgstr "קטן או שווה ל־"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"lhsButton\n"
"label\n"
"string.text"
msgid "..."
msgstr "…"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"andLabel\n"
"label\n"
"string.text"
msgid "and"
msgstr "וגם"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"rhsButton\n"
"label\n"
"string.text"
msgid "..."
msgstr "…"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"ToolBoxItem1\n"
"label\n"
"string.text"
msgid "Bold"
msgstr "מודגש"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"ToolBoxItem2\n"
"label\n"
"string.text"
msgid "Italic"
msgstr "נטוי"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"ToolBoxItem3\n"
"label\n"
"string.text"
msgid "Underline"
msgstr "קו תחתי"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"ToolBoxItem4\n"
"label\n"
"string.text"
msgid "Background Color"
msgstr "צבע הרקע"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"ToolBoxItem5\n"
"label\n"
"string.text"
msgid "Font Color"
msgstr "צבע הגופן"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"ToolBoxItem6\n"
"label\n"
"string.text"
msgid "Character Formatting"
msgstr "עיצוב תווים"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"removeButton\n"
"label\n"
"string.text"
msgid "-"
msgstr "-"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"addButton\n"
"label\n"
"string.text"
msgid "+"
msgstr "+"

#: datetimedialog.ui
msgctxt ""
"datetimedialog.ui\n"
"DateTimeDialog\n"
"title\n"
"string.text"
msgid "Date and Time"
msgstr "תאריך ושעה"

#: datetimedialog.ui
msgctxt ""
"datetimedialog.ui\n"
"date\n"
"label\n"
"string.text"
msgid "_Include Date"
msgstr "ל_כלול את התאריך"

#: datetimedialog.ui
msgctxt ""
"datetimedialog.ui\n"
"datelistbox_label\n"
"label\n"
"string.text"
msgid "_Format:"
msgstr "מ_בנה:"

#: datetimedialog.ui
msgctxt ""
"datetimedialog.ui\n"
"time\n"
"label\n"
"string.text"
msgid "Include _Time"
msgstr "לכלול את ה_שעה"

#: datetimedialog.ui
msgctxt ""
"datetimedialog.ui\n"
"timelistbox_label\n"
"label\n"
"string.text"
msgid "Fo_rmat:"
msgstr "מב_נה:"

#: floatingfield.ui
msgctxt ""
"floatingfield.ui\n"
"FloatingField\n"
"title\n"
"string.text"
msgid "Sorting and Grouping"
msgstr "מיון וקיבוץ"

#: floatingfield.ui
msgctxt ""
"floatingfield.ui\n"
"up\n"
"label\n"
"string.text"
msgid "Sort Ascending"
msgstr "מיון בסדר עולה"

#: floatingfield.ui
msgctxt ""
"floatingfield.ui\n"
"down\n"
"label\n"
"string.text"
msgid "Sort Descending"
msgstr "מיון בסדר יורד"

#: floatingfield.ui
msgctxt ""
"floatingfield.ui\n"
"delete\n"
"label\n"
"string.text"
msgid "Remove sorting"
msgstr "הסרת המיון"

#: floatingfield.ui
msgctxt ""
"floatingfield.ui\n"
"insert\n"
"label\n"
"string.text"
msgid "Insert"
msgstr ""

#: floatingfield.ui
msgctxt ""
"floatingfield.ui\n"
"helptext\n"
"label\n"
"string.text"
msgid "Highlight the fields to insert into the selected section of the template, then click Insert or press Enter."
msgstr ""

#: floatingnavigator.ui
msgctxt ""
"floatingnavigator.ui\n"
"FloatingNavigator\n"
"title\n"
"string.text"
msgid "Report navigator"
msgstr "סייר דוחות"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"FloatingSort\n"
"title\n"
"string.text"
msgid "Sorting and Grouping"
msgstr "מיון וקיבוץ"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"label5\n"
"label\n"
"string.text"
msgid "Group actions"
msgstr "פעולות קבוצתיות"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"up\n"
"label\n"
"string.text"
msgid "Move up"
msgstr "הזזה מעלה"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"down\n"
"label\n"
"string.text"
msgid "Move down"
msgstr "הזז למטה"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"delete\n"
"label\n"
"string.text"
msgid "Delete"
msgstr "מחיקה"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Groups"
msgstr "קבוצות"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"label6\n"
"label\n"
"string.text"
msgid "Sorting"
msgstr "מיון"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"label7\n"
"label\n"
"string.text"
msgid "Group Header"
msgstr "כותרת הקבוצה"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"label8\n"
"label\n"
"string.text"
msgid "Group Footer"
msgstr "כותרת תחתונה לקבוצה"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"label9\n"
"label\n"
"string.text"
msgid "Group On"
msgstr "קיבוץ לפי"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"label10\n"
"label\n"
"string.text"
msgid "Group Interval"
msgstr "מרווח קבוצה"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"label11\n"
"label\n"
"string.text"
msgid "Keep Together"
msgstr "להשאיר ביחד"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"sorting\n"
"0\n"
"stringlist.text"
msgid "Ascending"
msgstr "סדר עולה"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"sorting\n"
"1\n"
"stringlist.text"
msgid "Descending"
msgstr "סדר יורד"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"header\n"
"0\n"
"stringlist.text"
msgid "Present"
msgstr "נוכח"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"header\n"
"1\n"
"stringlist.text"
msgid "Not present"
msgstr "לא נוכח"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"keep\n"
"0\n"
"stringlist.text"
msgid "No"
msgstr "לא"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"keep\n"
"1\n"
"stringlist.text"
msgid "Whole Group"
msgstr "קבוצה שלמה"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"keep\n"
"2\n"
"stringlist.text"
msgid "With First Detail"
msgstr "עם הפרט הראשון"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"footer\n"
"0\n"
"stringlist.text"
msgid "Present"
msgstr "נוכח"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"footer\n"
"1\n"
"stringlist.text"
msgid "Not present"
msgstr "לא נוכח"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"group\n"
"0\n"
"stringlist.text"
msgid "Each Value"
msgstr "כל ערך"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Properties"
msgstr "מאפיינים"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "Help"
msgstr "עזרה"

#: pagedialog.ui
msgctxt ""
"pagedialog.ui\n"
"PageDialog\n"
"title\n"
"string.text"
msgid "Page Setup"
msgstr "הגדרת עמוד"

#: pagedialog.ui
msgctxt ""
"pagedialog.ui\n"
"page\n"
"label\n"
"string.text"
msgid "Page"
msgstr "עמוד"

#: pagedialog.ui
msgctxt ""
"pagedialog.ui\n"
"background\n"
"label\n"
"string.text"
msgid "Background"
msgstr "רקע"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"PageNumberDialog\n"
"title\n"
"string.text"
msgid "Page Numbers"
msgstr "מספרי עמודים"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"pagen\n"
"label\n"
"string.text"
msgid "_Page N"
msgstr "_עמוד נ"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"pagenofm\n"
"label\n"
"string.text"
msgid "Page _N of M"
msgstr "עמוד נ מתוך _מ"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Format"
msgstr "מבנה"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"toppage\n"
"label\n"
"string.text"
msgid "_Top of Page (Header)"
msgstr "_ראש העמוד (כותרת עליונה)"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"bottompage\n"
"label\n"
"string.text"
msgid "_Bottom of Page (Footer)"
msgstr "ת_חתית העמוד (כותרת תחתונה)"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Position"
msgstr "מיקום"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"alignment\n"
"0\n"
"stringlist.text"
msgid "Left"
msgstr "שמאל"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"alignment\n"
"1\n"
"stringlist.text"
msgid "Center"
msgstr "מרכז"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"alignment\n"
"2\n"
"stringlist.text"
msgid "Right"
msgstr "ימין"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"alignment_label\n"
"label\n"
"string.text"
msgid "_Alignment:"
msgstr "יי_שור:"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"shownumberonfirstpage\n"
"label\n"
"string.text"
msgid "Show Number on First Page"
msgstr "הצגת מספר על העמוד הראשון"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "General"
msgstr "כללי"

#. extracted from svtools/source/contnr
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-04-22 23:40+0200\n"
"PO-Revision-Date: 2014-09-25 13:48+0000\n"
"Last-Translator: Kaniska PSS <kaniska2008@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: dgo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1411652928.000000\n"

#: fileview.src
msgctxt ""
"fileview.src\n"
"STR_SVT_FILEVIEW_COLUMN_TITLE\n"
"string.text"
msgid "Name"
msgstr ""

#: fileview.src
msgctxt ""
"fileview.src\n"
"STR_SVT_FILEVIEW_COLUMN_SIZE\n"
"string.text"
msgid "Size"
msgstr "नाप "

#: fileview.src
msgctxt ""
"fileview.src\n"
"STR_SVT_FILEVIEW_COLUMN_DATE\n"
"string.text"
msgid "Date modified"
msgstr "तरीक तरमीमशुदा "

#: fileview.src
msgctxt ""
"fileview.src\n"
"STR_SVT_FILEVIEW_COLUMN_TYPE\n"
"string.text"
msgid "Type"
msgstr "किस्म "

#: fileview.src
msgctxt ""
"fileview.src\n"
"STR_SVT_FILEVIEW_ERR_MAKEFOLDER\n"
"string.text"
msgid "Could not create the folder %1."
msgstr "फोल्डर %1 नेईं सरजोई सकेआ. "

#: fileview.src
msgctxt ""
"fileview.src\n"
"STR_SVT_BYTES\n"
"string.text"
msgid "Bytes"
msgstr "बाइटां "

#: fileview.src
msgctxt ""
"fileview.src\n"
"STR_SVT_KB\n"
"string.text"
msgid "KB"
msgstr "KB"

#: fileview.src
msgctxt ""
"fileview.src\n"
"STR_SVT_MB\n"
"string.text"
msgid "MB"
msgstr "MB"

#: fileview.src
msgctxt ""
"fileview.src\n"
"STR_SVT_GB\n"
"string.text"
msgid "GB"
msgstr "GB"

#: fileview.src
msgctxt ""
"fileview.src\n"
"RID_FILEVIEW_CONTEXTMENU\n"
"MID_FILEVIEW_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr "~म्हेसो "

#: fileview.src
msgctxt ""
"fileview.src\n"
"RID_FILEVIEW_CONTEXTMENU\n"
"MID_FILEVIEW_RENAME\n"
"menuitem.text"
msgid "~Rename"
msgstr "~परतियै नांऽ देओ "

#: svcontnr.src
msgctxt ""
"svcontnr.src\n"
"STR_SVT_ACC_DESC_TABLISTBOX\n"
"string.text"
msgid "Row: %1, Column: %2"
msgstr "पंगताल: %1,स्तंभ : %2 "

#: svcontnr.src
msgctxt ""
"svcontnr.src\n"
"STR_SVT_ACC_DESC_FILEVIEW\n"
"string.text"
msgid ", Type: %1, URL: %2"
msgstr ", किस्म: %1, URL: %2 "

#: svcontnr.src
msgctxt ""
"svcontnr.src\n"
"STR_SVT_ACC_DESC_FOLDER\n"
"string.text"
msgid "Folder"
msgstr "फोल्डर "

#: svcontnr.src
msgctxt ""
"svcontnr.src\n"
"STR_SVT_ACC_DESC_FILE\n"
"string.text"
msgid "File"
msgstr "फाइल "

#: svcontnr.src
msgctxt ""
"svcontnr.src\n"
"STR_SVT_ACC_EMPTY_FIELD\n"
"string.text"
msgid "Empty Field"
msgstr "खाल्ली खेतर"

#: templwin.src
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"Title\n"
"itemlist.text"
msgid "Title"
msgstr "शीर्शक शीर्षक"

#: templwin.src
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"By\n"
"itemlist.text"
msgid "By"
msgstr "द्वारा,आसेआ"

#: templwin.src
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"Date\n"
"itemlist.text"
msgid "Date"
msgstr "तरीक "

#: templwin.src
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"Keywords\n"
"itemlist.text"
msgid "Keywords"
msgstr "कुंजी -शब्द (बहु.)कुंजी शब्द"

#: templwin.src
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"Description\n"
"itemlist.text"
msgid "Description"
msgstr "विवरण "

#: templwin.src
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"Type\n"
"itemlist.text"
msgid "Type"
msgstr "किस्म "

#: templwin.src
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"Modified on\n"
"itemlist.text"
msgid "Modified on"
msgstr "गी तरमीम होई "

#: templwin.src
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"Modified by\n"
"itemlist.text"
msgid "Modified by"
msgstr "द्वारा तरमीम होई"

#: templwin.src
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"Printed on\n"
"itemlist.text"
msgid "Printed on"
msgstr "गी छापेआ "

#: templwin.src
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"Printed by\n"
"itemlist.text"
msgid "Printed by"
msgstr "आसेआ छापेआ गेआ "

#: templwin.src
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"Subject\n"
"itemlist.text"
msgid "Subject"
msgstr "विशे "

#: templwin.src
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"Size\n"
"itemlist.text"
msgid "Size"
msgstr "नाप "

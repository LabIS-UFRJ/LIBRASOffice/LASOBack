#. extracted from sw/source/ui/config
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-03-09 20:49+0100\n"
"PO-Revision-Date: 2015-12-11 15:57+0000\n"
"Last-Translator: system user <>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: dgo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Pootle 2.7\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1449849473.000000\n"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"ST_SCRIPT_ASIAN\n"
"string.text"
msgid "Asian"
msgstr "एशियाई"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"ST_SCRIPT_CTL\n"
"string.text"
msgid "CTL"
msgstr ""

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"ST_SCRIPT_WESTERN\n"
"string.text"
msgid "Western"
msgstr "पच्छमीं"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PRODUCTNAME\n"
"string.text"
msgid "%PRODUCTNAME %s"
msgstr "%PRODUCTNAME API "

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_CONTENTS\n"
"string.text"
msgid "Contents"
msgstr "विशे-सूची "

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PAGE_BACKGROUND\n"
"string.text"
msgid "Page ba~ckground"
msgstr "पछौकड़ "

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PICTURES\n"
"string.text"
msgid "P~ictures and other graphic objects"
msgstr "तस्वीरां ते बाकी दूइयां ग्राफिक वस्तुआं (~i)"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_HIDDEN\n"
"string.text"
msgid "Hidden te~xt"
msgstr "छप्पी दी इबारत (~x)"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_TEXT_PLACEHOLDERS\n"
"string.text"
msgid "~Text placeholders"
msgstr "इबारत प्लेस-होल्डर (~T)"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_FORM_CONTROLS\n"
"string.text"
msgid "Form control~s"
msgstr "नियंत्रण (~s)"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_COLOR\n"
"string.text"
msgid "Color"
msgstr "रंग"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PRINT_BLACK\n"
"string.text"
msgid "Print text in blac~k"
msgstr "काला छापो (~k)"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PAGES_TEXT\n"
"string.text"
msgid "Pages"
msgstr "सफे"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PRINT_BLANK\n"
"string.text"
msgid "Print ~automatically inserted blank pages"
msgstr "समावेशत कोरे सफें पर अपने आप छापो (~a)"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_ONLY_PAPER\n"
"string.text"
msgid "~Use only paper tray from printer preferences"
msgstr "प्रिंटर तरजीहें चा सिर्फ कागज ट्रेऽ बरतो. (~U)"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PRINT\n"
"string.text"
msgid "Print"
msgstr "छापो"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_NONE\n"
"string.text"
msgid "None (document only)"
msgstr "कोई नेईं ( सिर्फ दस्तावेज)"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_COMMENTS_ONLY\n"
"string.text"
msgid "Comments only"
msgstr "सिर्फ टिप्पणियां"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PLACE_END\n"
"string.text"
msgid "Place at end of document"
msgstr "दस्तावेज दे अंत च रक्खो"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PLACE_PAGE\n"
"string.text"
msgid "Place at end of page"
msgstr "सफे दे अंत च रक्खो"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_COMMENTS\n"
"string.text"
msgid "~Comments"
msgstr "नोट (~C)"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PAGE_SIDES\n"
"string.text"
msgid "Page sides"
msgstr "सफा पास्से"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_ALL_PAGES\n"
"string.text"
msgid "All pages"
msgstr "सब सफे"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_BACK_PAGES\n"
"string.text"
msgid "Back sides / left pages"
msgstr "पिछला पासा/खब्बे सफे"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_FONT_PAGES\n"
"string.text"
msgid "Front sides / right pages"
msgstr "सामने पासे/सज्जे सफे"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_INCLUDE\n"
"string.text"
msgid "Include"
msgstr "शामल करो "

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_BROCHURE\n"
"string.text"
msgid "Broch~ure"
msgstr "जानकारी पोथी (~u)"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_LEFT_SCRIPT\n"
"string.text"
msgid "Left-to-right script"
msgstr "खब्बेआ सज्जै लिपि"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_RIGHT_SCRIPT\n"
"string.text"
msgid "Right-to-left script"
msgstr "सज्जेआ खब्बै लिपि"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_RANGE_COPIES\n"
"string.text"
msgid "Range and copies"
msgstr "रेंज ते कापियां"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_ALLPAGES\n"
"string.text"
msgid "~All pages"
msgstr "सब सफे (~A)"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_SOMEPAGES\n"
"string.text"
msgid "Pa~ges"
msgstr "सफें (~g)"

#: optdlg.src
#, fuzzy
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_SELECTION\n"
"string.text"
msgid "~Selection"
msgstr "चोन (~S)"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PLACE_MARGINS\n"
"string.text"
msgid "Place in margins"
msgstr ""

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Millimeter\n"
"itemlist.text"
msgid "Millimeter"
msgstr "मिलीमीटर "

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Centimeter\n"
"itemlist.text"
msgid "Centimeter"
msgstr "सैंटीमीटर "

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Meter\n"
"itemlist.text"
msgid "Meter"
msgstr "मीटर "

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Kilometer\n"
"itemlist.text"
msgid "Kilometer"
msgstr "किलोमीटर "

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Inch\n"
"itemlist.text"
msgid "Inch"
msgstr "इंच "

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Foot\n"
"itemlist.text"
msgid "Foot"
msgstr "फुट"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Miles\n"
"itemlist.text"
msgid "Miles"
msgstr "मीलां "

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Pica\n"
"itemlist.text"
msgid "Pica"
msgstr "पिका "

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Point\n"
"itemlist.text"
msgid "Point"
msgstr "बिंदु "

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Char\n"
"itemlist.text"
msgid "Char"
msgstr "वर्ण"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Line\n"
"itemlist.text"
msgid "Line"
msgstr "लाइन"

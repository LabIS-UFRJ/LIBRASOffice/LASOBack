#. extracted from extensions/source/bibliography
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-07-04 18:47+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: dgo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1467658039.000000\n"

#: bib.src
msgctxt ""
"bib.src\n"
"RID_BIB_STR_FIELDSELECTION\n"
"string.text"
msgid "Field selection:"
msgstr "खेतर चोन :"

#: bib.src
msgctxt ""
"bib.src\n"
"RID_BIB_STR_TABWIN_PREFIX\n"
"string.text"
msgid "Table;Query;Sql;Sql [Native]"
msgstr "टेबल;पुच्छ;Sql;Sql [मूल-भूत] "

#: bib.src
msgctxt ""
"bib.src\n"
"RID_BIB_STR_FRAME_TITLE\n"
"string.text"
msgid "Bibliography Database"
msgstr "ग्रंथ-सूची डेटाबेस"

#: bib.src
msgctxt ""
"bib.src\n"
"RID_MAP_QUESTION\n"
"string.text"
msgid "Do you want to edit the column arrangement?"
msgstr "क्या तुस स्तंभ तरतीबबंदी संपादत करना चांह्‌दे ओ ? "

#: bib.src
msgctxt ""
"bib.src\n"
"RID_BIB_STR_NONE\n"
"string.text"
msgid "<none>"
msgstr "<कोई नहीं>"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_ERROR_PREFIX\n"
"string.text"
msgid "The following column names could not be assigned:\n"
msgstr ""
"ख’लके  स्तंभ  नांऽ निर्दिश्ट नेईं होई सके:\n"
" "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_ARTICLE\n"
"string.text"
msgid "Article"
msgstr "लेख "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_BOOK\n"
"string.text"
msgid "Book"
msgstr "पुस्तक "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_BOOKLET\n"
"string.text"
msgid "Brochures"
msgstr "सूचना-पोथी"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_CONFERENCE\n"
"string.text"
msgid "Conference proceedings article (BiBTeX)"
msgstr ""

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_INBOOK\n"
"string.text"
msgid "Book excerpt"
msgstr "पुस्तक-अंश "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_INCOLLECTION\n"
"string.text"
msgid "Book excerpt with title"
msgstr "शीर्शक सनैं पुस्तक-अंश"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_INPROCEEDINGS\n"
"string.text"
msgid "Conference proceedings article"
msgstr ""

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_JOURNAL\n"
"string.text"
msgid "Journal"
msgstr "रसाला "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_MANUAL\n"
"string.text"
msgid "Techn. documentation"
msgstr "तकनीकी दस्तावेजीकरण "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_MASTERSTHESIS\n"
"string.text"
msgid "Thesis"
msgstr "शोध-प्रबंध "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_MISC\n"
"string.text"
msgid "Miscellaneous"
msgstr "रला-मिला "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_PHDTHESIS\n"
"string.text"
msgid "Dissertation"
msgstr "शोध-निबंध"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_PROCEEDINGS\n"
"string.text"
msgid "Conference proceedings"
msgstr "कांफ्रैंस दी कारवाई "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_TECHREPORT\n"
"string.text"
msgid "Research report"
msgstr "शोध-रपोट "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_UNPUBLISHED\n"
"string.text"
msgid "Unpublished"
msgstr "अप्रकाशत "

#: sections.src
#, fuzzy
msgctxt ""
"sections.src\n"
"ST_TYPE_EMAIL\n"
"string.text"
msgid "E-mail"
msgstr "ई-मेल "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_WWW\n"
"string.text"
msgid "WWW document"
msgstr "WWW दस्तावेज "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_CUSTOM1\n"
"string.text"
msgid "User-defined1"
msgstr "बरतूनी-परिभाशत 1 "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_CUSTOM2\n"
"string.text"
msgid "User-defined2"
msgstr "बरतूनी-परिभाशत2"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_CUSTOM3\n"
"string.text"
msgid "User-defined3"
msgstr "बरतूनी-परिभाशत3 "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_CUSTOM4\n"
"string.text"
msgid "User-defined4"
msgstr "बरतूनी-परिभाशत4"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_CUSTOM5\n"
"string.text"
msgid "User-defined5"
msgstr "बरतूनी-परिभाशत5 "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_TITLE\n"
"string.text"
msgid "General"
msgstr "सधारण "

#. extracted from extensions/source/bibliography
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-03-11 22:09+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: sd\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1457734193.000000\n"

#: bib.src
msgctxt ""
"bib.src\n"
"RID_BIB_STR_FIELDSELECTION\n"
"string.text"
msgid "Field selection:"
msgstr "کيتر جي چونڊ: "

#: bib.src
msgctxt ""
"bib.src\n"
"RID_BIB_STR_TABWIN_PREFIX\n"
"string.text"
msgid "Table;Query;Sql;Sql [Native]"
msgstr " تختي، پڇا، ;Sql;Sql [Native]"

#: bib.src
msgctxt ""
"bib.src\n"
"RID_BIB_STR_FRAME_TITLE\n"
"string.text"
msgid "Bibliography Database"
msgstr "ڪتابن جي ياداشت جي آڌار سامگري "

#: bib.src
msgctxt ""
"bib.src\n"
"RID_MAP_QUESTION\n"
"string.text"
msgid "Do you want to edit the column arrangement?"
msgstr "ڇا  توهين ڪالمن جو سلسلو سمپادت ڪرڻ چاهيو ٿا؟ "

#: bib.src
msgctxt ""
"bib.src\n"
"RID_BIB_STR_NONE\n"
"string.text"
msgid "<none>"
msgstr "<ناهي>"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_ERROR_PREFIX\n"
"string.text"
msgid "The following column names could not be assigned:\n"
msgstr ""
"هيٺيان ڪالم جا نالا نٿا ڏيئي سگهجن: \n"
" "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_ARTICLE\n"
"string.text"
msgid "Article"
msgstr "ليکُ "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_BOOK\n"
"string.text"
msgid "Book"
msgstr "ڪتابُ "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_BOOKLET\n"
"string.text"
msgid "Brochures"
msgstr "ڪتابڙيون "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_CONFERENCE\n"
"string.text"
msgid "Conference proceedings article (BiBTeX)"
msgstr ""

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_INBOOK\n"
"string.text"
msgid "Book excerpt"
msgstr "ڪتاب مان نڪتل ٽڪر "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_INCOLLECTION\n"
"string.text"
msgid "Book excerpt with title"
msgstr "عنوان سان ڪتاب مان نڪتل ٽڪر "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_INPROCEEDINGS\n"
"string.text"
msgid "Conference proceedings article"
msgstr ""

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_JOURNAL\n"
"string.text"
msgid "Journal"
msgstr "مخزن "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_MANUAL\n"
"string.text"
msgid "Techn. documentation"
msgstr "تڪنيڪي دستاويز تيار ڪرڻ "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_MASTERSTHESIS\n"
"string.text"
msgid "Thesis"
msgstr "ٿيس"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_MISC\n"
"string.text"
msgid "Miscellaneous"
msgstr "ڦٽڪر "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_PHDTHESIS\n"
"string.text"
msgid "Dissertation"
msgstr "ڊزارٽيشن "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_PROCEEDINGS\n"
"string.text"
msgid "Conference proceedings"
msgstr "ڪانفرينس جي ڪارگذاري "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_TECHREPORT\n"
"string.text"
msgid "Research report"
msgstr "کوجنا جي رپورٽ "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_UNPUBLISHED\n"
"string.text"
msgid "Unpublished"
msgstr "اڻچپيل "

#: sections.src
#, fuzzy
msgctxt ""
"sections.src\n"
"ST_TYPE_EMAIL\n"
"string.text"
msgid "E-mail"
msgstr "اي۔ميل "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_WWW\n"
"string.text"
msgid "WWW document"
msgstr "WWW دستاويز "

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_CUSTOM1\n"
"string.text"
msgid "User-defined1"
msgstr "اِستعمال ڪندڙ - وصف ڏنل 1"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_CUSTOM2\n"
"string.text"
msgid "User-defined2"
msgstr "اِستعمال ڪندڙ - وصف ڏنل 2"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_CUSTOM3\n"
"string.text"
msgid "User-defined3"
msgstr "اِستعمال ڪندڙ - وصف ڏنل 3"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_CUSTOM4\n"
"string.text"
msgid "User-defined4"
msgstr "اِستعمال ڪندڙ - وصف ڏنل 4"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_CUSTOM5\n"
"string.text"
msgid "User-defined5"
msgstr "اِستعمال ڪندڙ - وصف ڏنل 5"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_TITLE\n"
"string.text"
msgid "General"
msgstr "عام "

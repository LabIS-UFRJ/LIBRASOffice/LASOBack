#. extracted from svx/source/accessibility
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.freedesktop.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-05-23 12:06+0200\n"
"PO-Revision-Date: 2011-04-06 02:11+0200\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ss\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_3D_MATERIAL_COLOR\n"
"string.text"
msgid "3D material color"
msgstr "3D Umbala wendvwangu"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_TEXT_COLOR\n"
"string.text"
msgid "Font color"
msgstr "Umbala wefonti"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_BACKGROUND_COLOR\n"
"string.text"
msgid "Background color"
msgstr "Umbala welimuva"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_FILLSTYLE_NONE\n"
"string.text"
msgid "None"
msgstr "Kute"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_FILLSTYLE_SOLID\n"
"string.text"
msgid "Solid"
msgstr "Kucinile"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_FILLSTYLE_HATCH\n"
"string.text"
msgid "With hatching"
msgstr "Ngekuchobosela"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_FILLSTYLE_GRADIENT\n"
"string.text"
msgid "Gradient"
msgstr "Umenyukela"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_FILLSTYLE_BITMAP\n"
"string.text"
msgid "Bitmap"
msgstr "Bitbalave"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_WITH\n"
"string.text"
msgid "with"
msgstr "nge"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_STYLE\n"
"string.text"
msgid "Style"
msgstr "Sitayela"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_AND\n"
"string.text"
msgid "and"
msgstr "ne"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CORN_NAME\n"
"string.text"
msgid "Corner control"
msgstr "Likona lemphatsi"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CORN_DESCR\n"
"string.text"
msgid "Selection of a corner point."
msgstr "Kukhetfwa kweliphuzu lelikona."

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_ANGL_NAME\n"
"string.text"
msgid "Angle control"
msgstr "Kuphatfwa kwe-engele"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_ANGL_DESCR\n"
"string.text"
msgid "Selection of a major angle."
msgstr "Kukhetfwa kwe-engele lenkhulu."

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_LT\n"
"string.text"
msgid "Top left"
msgstr "Etulu ngesancele"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_MT\n"
"string.text"
msgid "Top middle"
msgstr "Etulu emkhatsini"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_RT\n"
"string.text"
msgid "Top right"
msgstr "Etulu ngesekudla"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_LM\n"
"string.text"
msgid "Left center"
msgstr "Ngesancele emkhatsini"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_MM\n"
"string.text"
msgid "Center"
msgstr "Emkhatsini"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_RM\n"
"string.text"
msgid "Right center"
msgstr "Ngesekudla emkhatsini"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_LB\n"
"string.text"
msgid "Bottom left"
msgstr "Phansi ngesancele"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_MB\n"
"string.text"
msgid "Bottom middle"
msgstr "Phansi emkhatsini"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_RB\n"
"string.text"
msgid "Bottom right"
msgstr "Phansi ngesekudla"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A000\n"
"string.text"
msgid "0 degrees"
msgstr "0 digrizi"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A045\n"
"string.text"
msgid "45 degrees"
msgstr "45 digrizi"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A090\n"
"string.text"
msgid "90 degrees"
msgstr "90 digrizi"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A135\n"
"string.text"
msgid "135 degrees"
msgstr "135 digrizi"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A180\n"
"string.text"
msgid "180 degrees"
msgstr "180 digrizi"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A225\n"
"string.text"
msgid "225 degrees"
msgstr "225 digrizi"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A270\n"
"string.text"
msgid "270 degrees"
msgstr "270 digrizi"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A315\n"
"string.text"
msgid "315 degrees"
msgstr "315 digrizi"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_GRAPHCTRL_ACC_NAME\n"
"string.text"
msgid "Contour control"
msgstr "Kuphatfwa kwemikhondovu"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_GRAPHCTRL_ACC_DESCRIPTION\n"
"string.text"
msgid "This is where you can edit the contour."
msgstr "Lapha ngulapho unga-editha khona umkhondovu."

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_CHARACTER_SELECTION\n"
"string.text"
msgid "Special character selection"
msgstr "Kukhetfwa kweluhlavu lolusipeshali"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_CHAR_SEL_DESC\n"
"string.text"
msgid "Select special characters in this area."
msgstr "Khetsa tinhlavu letisipeshali kulendzawo."

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_CHARACTER_CODE\n"
"string.text"
msgid "Character code "
msgstr "Ikhodi yeluhlavu "

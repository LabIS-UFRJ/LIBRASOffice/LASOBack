#. extracted from extensions/source/update/check
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-05-23 21:37+0200\n"
"PO-Revision-Date: 2016-03-12 02:13+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: Telugu <Fedora-trans-te@redhat.com>\n"
"Language: te\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1457748837.000000\n"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_STR_CHECKING\n"
"string.text"
msgid "Checking..."
msgstr "పరిశీలించుచున్నది..."

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_STR_CHECKING_ERR\n"
"string.text"
msgid "Checking for an update failed."
msgstr "నవీకరణ క్షేత్రముకొరకు పరిశీలిస్తున్నది."

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_STR_NO_UPD_FOUND\n"
"string.text"
msgid "%PRODUCTNAME %PRODUCTVERSION is up to date."
msgstr "%PRODUCTNAME %PRODUCTVERSION సరికొత్తగావుంది."

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_STR_UPD_FOUND\n"
"string.text"
msgid ""
"%PRODUCTNAME %NEXTVERSION is available.\n"
"\n"
"The installed version is %PRODUCTNAME %PRODUCTVERSION.\n"
"\n"
"Note: Before downloading an update, please ensure that you have sufficient access rights to install it.\n"
"A password, usually the administrator's or root password, may be required."
msgstr ""
"%PRODUCTNAME %NEXTVERSION అందుబాటులోవుంది.\n"
"\n"
"స్థాపిత వర్షన్ %PRODUCTNAME %PRODUCTVERSION.\n"
"\n"
"గమనిక: ఒక నవీకరణను డౌన్‌లోడ్ చేయుటకు ముందుగా, దానిని స్థాపించుటకు మీకు సరిపోయినన్ని హక్కులు వున్నాయేమో నిర్ధారించుకోండి.\n"
"ఒక సంకేతపదము, సాధారణంగా నిర్వహణాధికారిది లేదా root సంకేతపదము, అవసరము కావచ్చు."

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_STR_DLG_TITLE\n"
"string.text"
msgid "Check for Updates"
msgstr "నవీకరణలకొరకు పరిశీలించుము"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_STR_DOWNLOAD_PAUSE\n"
"string.text"
msgid "Downloading %PRODUCTNAME %NEXTVERSION paused at..."
msgstr "దీనివద్ద పాజ్‌చేసిన %PRODUCTNAME %NEXTVERSION ను డౌనులోడుచేస్తున్నది..."

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_STR_DOWNLOAD_ERR\n"
"string.text"
msgid "Downloading %PRODUCTNAME %NEXTVERSION stalled at"
msgstr "దీనివద్ద నిలుపబడిన %PRODUCTNAME %NEXTVERSION ను డౌనులోడుచేస్తున్నది...."

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_STR_DOWNLOAD_WARN\n"
"string.text"
msgid ""
"The download location is: %DOWNLOAD_PATH.\n"
"\n"
"Under Tools – Options... - %PRODUCTNAME – Online Update you can change the download location."
msgstr ""
"డౌనులోడు చేసిన స్థానము: %DOWNLOAD_PATH.\n"
"\n"
"సాధనములు - ఐచ్చికములు... - %PRODUCTNAME – ఆన్‌లైన్ నవీకరణ క్రిందన మీరు డౌనులోడు స్థానమును మార్చవచ్చు."

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_STR_DOWNLOAD_DESCR\n"
"string.text"
msgid "%FILE_NAME has been downloaded to %DOWNLOAD_PATH."
msgstr "%FILE_NAME అనునది %DOWNLOAD_PATH కు డౌనులోడు అయినది."

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_STR_DOWNLOAD_UNAVAIL\n"
"string.text"
msgid ""
"The automatic download of the update is currently not available.\n"
"\n"
"Click 'Download...' to download %PRODUCTNAME %NEXTVERSION manually from the web site."
msgstr ""
"నవీకరణయొక్క స్వయంచాలక డౌనులోడు యిప్పుడు అందుబాటులోలేదు.\n"
"\n"
"వెబ్‌సైటునుండి %PRODUCTNAME %NEXTVERSION ను మానవీయంగా డౌనులోడు చేయుటకు 'డౌనులోడును...'ను నొక్కండి."

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_STR_DOWNLOADING\n"
"string.text"
msgid "Downloading %PRODUCTNAME %NEXTVERSION..."
msgstr "%PRODUCTNAME %NEXTVERSION ను డౌనులోడు చేస్తున్నది..."

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_STR_READY_INSTALL\n"
"string.text"
msgid "Download of %PRODUCTNAME %NEXTVERSION completed. Ready for installation."
msgstr "%PRODUCTNAME %NEXTVERSION యొక్క డౌనులోడు పూర్తైనది. స్థాపనకు సిద్దముగావున్నది."

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_STR_CANCEL_TITLE\n"
"string.text"
msgid "%PRODUCTNAME %PRODUCTVERSION"
msgstr "%PRODUCTNAME %PRODUCTVERSION"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_STR_CANCEL_DOWNLOAD\n"
"string.text"
msgid "Do you really want to cancel the download?"
msgstr "మీరు నిజంగా డౌనులోడును రద్దుచేద్దామని అనుకొనుచున్నారా?"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_STR_BEGIN_INSTALL\n"
"string.text"
msgid "To install the update, %PRODUCTNAME %PRODUCTVERSION needs to be closed. Do you want to install the update now?"
msgstr "నవీకరణను స్థాపించుటకు, %PRODUCTNAME %PRODUCTVERSION మూయవలసి వుంది. మీరు నవీకరణను యిప్పుడు స్థాపించుదామని అనుకొనుచున్నారా?"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_STR_INSTALL_NOW\n"
"string.text"
msgid "Install ~now"
msgstr "ఇప్పుడు స్థాపించుము(~n)"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_STR_INSTALL_LATER\n"
"string.text"
msgid "Install ~later"
msgstr "తరువాత స్థాపించుము(~l)"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_STR_INSTALL_ERROR\n"
"string.text"
msgid "Could not run the installer application, please run %FILE_NAME in %DOWNLOAD_PATH manually."
msgstr "సంస్థాపకి అనువర్తనమును నడుపలేదు, దయచేసి %FILE_NAME ను  %DOWNLOAD_PATH నందు మానవీయంగా నడుపుము."

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_STR_OVERWRITE_WARNING\n"
"string.text"
msgid "A file with that name already exists! Do you want to overwrite the existing file?"
msgstr "ఆ పేరుతో వొకఫైలు యిప్పటికే వుంది! మీరు వున్న ఫైలు మార్చుదామని అనుకొనుచున్నారా?"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_STR_RELOAD_WARNING\n"
"string.text"
msgid "A file with the name '%FILENAME' already exists in '%DOWNLOAD_PATH'! Do you want to continue with the download or delete and reload the file?"
msgstr "'%DOWNLOAD_PATH' పాత్ నందు వొక ఫైలు '%FILENAME' పేరుతో యిప్పటికే వున్నది! మీరు డౌన్‌లోడ్‌తో కొనసాగాలని అనుకొనుచున్నారా లేక ఫైలును తొలగించి తిరిగిలోడ్ చేయాలని అనుకొనుచున్నారా?"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_STR_RELOAD_RELOAD\n"
"string.text"
msgid "Reload File"
msgstr "ఫైలును తిరిగి లోడ్‌చేయి"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_STR_RELOAD_CONTINUE\n"
"string.text"
msgid "Continue"
msgstr "కొనసాగించు"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_STR_PERCENT\n"
"string.text"
msgid "%PERCENT%"
msgstr "%PERCENT%"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_FT_STATUS\n"
"string.text"
msgid "Status"
msgstr "స్థితి"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_FT_DESCRIPTION\n"
"string.text"
msgid "Description"
msgstr "వివరణ"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_BTN_CLOSE\n"
"string.text"
msgid "Close"
msgstr "మూయు"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_BTN_DOWNLOAD\n"
"string.text"
msgid "~Download"
msgstr "డౌనులోడు(~D)"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_BTN_INSTALL\n"
"string.text"
msgid "~Install"
msgstr "స్థాపించు(~I)"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_BTN_PAUSE\n"
"string.text"
msgid "~Pause"
msgstr "నిలిపివుంచు(~P)"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_BTN_RESUME\n"
"string.text"
msgid "~Resume"
msgstr "తిరిగికొనసాగించు(~R)"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_BTN_CANCEL\n"
"string.text"
msgid "Cancel"
msgstr "రద్దు"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_BUBBLE_T_UPDATE_AVAIL\n"
"string.text"
msgid "%PRODUCTNAME update available"
msgstr "%PRODUCTNAME నవీకరణ అందుబాటులో వున్నది"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_BUBBLE_UPDATE_AVAIL\n"
"string.text"
msgid "Click the icon to start the download."
msgstr "డౌనులోడును ప్రారంభించుటకు ప్రతిమ నొక్కుము."

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_BUBBLE_T_UPDATE_NO_DOWN\n"
"string.text"
msgid "%PRODUCTNAME update available"
msgstr "%PRODUCTNAME నవీకరణ అందుబాటులో వున్నది"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_BUBBLE_UPDATE_NO_DOWN\n"
"string.text"
msgid "Click the icon for more information."
msgstr "ఎక్కువ సమాచారము కొరకు ప్రతిమ నొక్కుము."

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_BUBBLE_T_AUTO_START\n"
"string.text"
msgid "%PRODUCTNAME update available"
msgstr "%PRODUCTNAME నవీకరణ అందుబాటులో వున్నది"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_BUBBLE_AUTO_START\n"
"string.text"
msgid "Download of update begins."
msgstr "నవీకరణ యొక్క డౌనులోడు ప్రారంభమైంది."

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_BUBBLE_T_DOWNLOADING\n"
"string.text"
msgid "Download of update in progress"
msgstr "నవీకరణ యొక్క డౌనులోడు పురోగతిలోవుంది."

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_BUBBLE_T_DOWNLOAD_PAUSED\n"
"string.text"
msgid "Download of update paused"
msgstr "నవీకరణ యొక్క డౌనులోడు నిలిపివేయబడింది"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_BUBBLE_DOWNLOAD_PAUSED\n"
"string.text"
msgid "Click the icon to resume."
msgstr "తిరిగికొనసాగించుటకు ప్రతిమ నొక్కుము."

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_BUBBLE_T_ERROR_DOWNLOADING\n"
"string.text"
msgid "Download of update stalled"
msgstr "నవీకరణ డౌనులోడు నిలిపివేయబడింది."

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_BUBBLE_ERROR_DOWNLOADING\n"
"string.text"
msgid "Click the icon for more information."
msgstr "ఎక్కువ సమాచారము కొరకు ప్రతిమ నొక్కుము."

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_BUBBLE_T_DOWNLOAD_AVAIL\n"
"string.text"
msgid "Download of update completed"
msgstr "నవీకరణ డౌనులోడు పూర్తైనది"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_BUBBLE_DOWNLOAD_AVAIL\n"
"string.text"
msgid "Click the icon to start the installation."
msgstr "స్థాపనను కొనసాగించుటకు ప్రతిమ నొక్కుము."

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_BUBBLE_T_EXT_UPD_AVAIL\n"
"string.text"
msgid "Updates for extensions available"
msgstr "పొడిగింపులకు నవీకరణలు అందుబాటులోవున్నాయి"

#: updatehdl.src
msgctxt ""
"updatehdl.src\n"
"RID_UPDATE_BUBBLE_EXT_UPD_AVAIL\n"
"string.text"
msgid "Click the icon for more information."
msgstr "ఎక్కువ సమాచారము కొరకు ప్రతిమ నొక్కుము."

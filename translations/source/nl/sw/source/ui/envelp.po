#. extracted from sw/source/ui/envelp
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2014-11-18 11:23+0100\n"
"PO-Revision-Date: 2014-02-21 08:05+0000\n"
"Last-Translator: kees538 <kees538@hotmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1392969904.0\n"

#: envelp.src
msgctxt ""
"envelp.src\n"
"STR_DATABASE_NOT_OPENED\n"
"string.text"
msgid "Database could not be opened."
msgstr "Database kan niet worden geopend."

#: envelp.src
msgctxt ""
"envelp.src\n"
"STR_NO_DRIVERS\n"
"string.text"
msgid "No database drivers installed."
msgstr "Geen databasestuurprogramma's geïnstalleerd."

#: envelp.src
msgctxt ""
"envelp.src\n"
"STR_BTN_NEWDOC\n"
"string.text"
msgid "~New Doc."
msgstr "Nieuw ~doc."

#: envelp.src
msgctxt ""
"envelp.src\n"
"STR_SENDER_TOKENS\n"
"string.text"
msgid "COMPANY;CR;FIRSTNAME; ;LASTNAME;CR;ADDRESS;CR;CITY; ;STATEPROV; ;POSTALCODE;CR;COUNTRY;CR;"
msgstr "BEDRIJF;CR;VOORNAAM; ;ACHTERNAAM;CR;ADRES;CR;POSTCODE; ;PLAATS;CR;LAND;CR;"

#: label.src
msgctxt ""
"label.src\n"
"STR_CUSTOM\n"
"string.text"
msgid "[User]"
msgstr "[Gebruiker]"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_HDIST\n"
"string.text"
msgid "H. Pitch"
msgstr "H. afstand"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_VDIST\n"
"string.text"
msgid "V. Pitch"
msgstr "V. afstand"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_WIDTH\n"
"string.text"
msgid "Width"
msgstr "Breedte"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_HEIGHT\n"
"string.text"
msgid "Height"
msgstr "Hoogte"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_LEFT\n"
"string.text"
msgid "Left margin"
msgstr "Linkermarge"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_UPPER\n"
"string.text"
msgid "Top margin"
msgstr "Bovenmarge"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_COLS\n"
"string.text"
msgid "Columns"
msgstr "Kolommen"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_ROWS\n"
"string.text"
msgid "Rows"
msgstr "Rijen"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_PWIDTH\n"
"string.text"
msgid "Page Width"
msgstr "Pagina breedte"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_PHEIGHT\n"
"string.text"
msgid "Page Height"
msgstr "Pagina hoogte"

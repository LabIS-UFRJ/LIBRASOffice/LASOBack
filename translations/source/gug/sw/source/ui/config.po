#. extracted from sw/source/ui/config
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-03-09 20:49+0100\n"
"PO-Revision-Date: 2016-06-09 14:05+0000\n"
"Last-Translator: Giovanni Caligaris <giovannicaligaris@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: gug\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.7\n"
"X-POOTLE-MTIME: 1465481139.000000\n"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"ST_SCRIPT_ASIAN\n"
"string.text"
msgid "Asian"
msgstr "Asiático"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"ST_SCRIPT_CTL\n"
"string.text"
msgid "CTL"
msgstr "CTL"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"ST_SCRIPT_WESTERN\n"
"string.text"
msgid "Western"
msgstr "Occidental"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PRODUCTNAME\n"
"string.text"
msgid "%PRODUCTNAME %s"
msgstr "%PRODUCTNAME %s"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_CONTENTS\n"
"string.text"
msgid "Contents"
msgstr "Orekóva"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PAGE_BACKGROUND\n"
"string.text"
msgid "Page ba~ckground"
msgstr "Rogue ha~pykuegua"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PICTURES\n"
"string.text"
msgid "P~ictures and other graphic objects"
msgstr "~Ta'anga kuéra ha ambue mba'e kuéra gráficos"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_HIDDEN\n"
"string.text"
msgid "Hidden te~xt"
msgstr "Mo~ñe'ẽrã okañýa"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_TEXT_PLACEHOLDERS\n"
"string.text"
msgid "~Text placeholders"
msgstr "~Tenda ojereserva va'ekue moñe'ẽrã peguarã"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_FORM_CONTROLS\n"
"string.text"
msgid "Form control~s"
msgstr "Ñemañ~a formulario peguarã"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_COLOR\n"
"string.text"
msgid "Color"
msgstr "Sa'y"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PRINT_BLACK\n"
"string.text"
msgid "Print text in blac~k"
msgstr "Imprimir moñe'ẽrã hũp~e"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PAGES_TEXT\n"
"string.text"
msgid "Pages"
msgstr "Rogue kuéra"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PRINT_BLANK\n"
"string.text"
msgid "Print ~automatically inserted blank pages"
msgstr "Ojeimprimi rogue kuéra morotĩpe oñemoinge va'ekue ~automáticamente"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_ONLY_PAPER\n"
"string.text"
msgid "~Use only paper tray from printer preferences"
msgstr "~Jepuru bandeja año kuatiágui preferencia impresora gui"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PRINT\n"
"string.text"
msgid "Print"
msgstr "Imprimir"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_NONE\n"
"string.text"
msgid "None (document only)"
msgstr "Mavave (documento'año)"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_COMMENTS_ONLY\n"
"string.text"
msgid "Comments only"
msgstr "Oje'éva año"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PLACE_END\n"
"string.text"
msgid "Place at end of document"
msgstr "Emoĩ opahápe documento"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PLACE_PAGE\n"
"string.text"
msgid "Place at end of page"
msgstr "Moĩ rogue opahápe"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_COMMENTS\n"
"string.text"
msgid "~Comments"
msgstr "~Oje'éva"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PAGE_SIDES\n"
"string.text"
msgid "Page sides"
msgstr "Tova kuéra roguégui"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_ALL_PAGES\n"
"string.text"
msgid "All pages"
msgstr "Opavave Rogue kuéra"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_BACK_PAGES\n"
"string.text"
msgid "Back sides / left pages"
msgstr "Reversos / rogue asúpe"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_FONT_PAGES\n"
"string.text"
msgid "Front sides / right pages"
msgstr "Anversos / rogue akatúa"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_INCLUDE\n"
"string.text"
msgid "Include"
msgstr "Moinge"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_BROCHURE\n"
"string.text"
msgid "Broch~ure"
msgstr "Folle~to"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_LEFT_SCRIPT\n"
"string.text"
msgid "Left-to-right script"
msgstr "Ojehai va'ekue asu akatúa gotyo"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_RIGHT_SCRIPT\n"
"string.text"
msgid "Right-to-left script"
msgstr "Ojehai va'ekue akatúa asu gotyo"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_RANGE_COPIES\n"
"string.text"
msgid "Range and copies"
msgstr "Rango ha kópia kuéra"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_ALLPAGES\n"
"string.text"
msgid "~All pages"
msgstr "~Opavave rogue kuéra"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_SOMEPAGES\n"
"string.text"
msgid "Pa~ges"
msgstr "Ro~gue kuéra"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_SELECTION\n"
"string.text"
msgid "~Selection"
msgstr "~Selección"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PLACE_MARGINS\n"
"string.text"
msgid "Place in margins"
msgstr "Emoĩ tembe'y kuérape"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Millimeter\n"
"itemlist.text"
msgid "Millimeter"
msgstr "Milímetro"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Centimeter\n"
"itemlist.text"
msgid "Centimeter"
msgstr "Centímetro"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Meter\n"
"itemlist.text"
msgid "Meter"
msgstr "Metro"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Kilometer\n"
"itemlist.text"
msgid "Kilometer"
msgstr "Kilómetro"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Inch\n"
"itemlist.text"
msgid "Inch"
msgstr "Pulgada"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Foot\n"
"itemlist.text"
msgid "Foot"
msgstr "Pie"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Miles\n"
"itemlist.text"
msgid "Miles"
msgstr "Millas"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Pica\n"
"itemlist.text"
msgid "Pica"
msgstr "Pica"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Point\n"
"itemlist.text"
msgid "Point"
msgstr "Kyta"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Char\n"
"itemlist.text"
msgid "Char"
msgstr "Carácter"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Line\n"
"itemlist.text"
msgid "Line"
msgstr "Línea"

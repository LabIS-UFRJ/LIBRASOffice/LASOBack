#. extracted from cui/source/options
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2017-03-15 08:58+0000\n"
"Last-Translator: Tornoz <tornoz@laposte.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: br\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1489568306.000000\n"

#: connpooloptions.src
msgctxt ""
"connpooloptions.src\n"
"RID_SVXSTR_DRIVER_NAME\n"
"string.text"
msgid "Driver name"
msgstr "Anv ar stur"

#: connpooloptions.src
msgctxt ""
"connpooloptions.src\n"
"RID_SVXSTR_POOLED_FLAG\n"
"string.text"
msgid "Pool"
msgstr "Strollad"

#: connpooloptions.src
msgctxt ""
"connpooloptions.src\n"
"RID_SVXSTR_POOL_TIMEOUT\n"
"string.text"
msgid "Timeout"
msgstr "Diamzeret"

#: connpooloptions.src
msgctxt ""
"connpooloptions.src\n"
"RID_SVXSTR_YES\n"
"string.text"
msgid "Yes"
msgstr "Ya"

#: connpooloptions.src
msgctxt ""
"connpooloptions.src\n"
"RID_SVXSTR_NO\n"
"string.text"
msgid "No"
msgstr "Ket"

#: dbregister.src
msgctxt ""
"dbregister.src\n"
"RID_SVXSTR_TYPE\n"
"string.text"
msgid "Registered name"
msgstr "Anv marilhet"

#: dbregister.src
msgctxt ""
"dbregister.src\n"
"RID_SVXSTR_PATH\n"
"string.text"
msgid "Database file"
msgstr "Restr ar stlennvon"

#: doclinkdialog.src
msgctxt ""
"doclinkdialog.src\n"
"STR_LINKEDDOC_DOESNOTEXIST\n"
"string.text"
msgid ""
"The file\n"
"$file$\n"
"does not exist."
msgstr ""
"Ar restr \n"
"$file$\n"
"n'eus ket anezhi."

#: doclinkdialog.src
msgctxt ""
"doclinkdialog.src\n"
"STR_LINKEDDOC_NO_SYSTEM_FILE\n"
"string.text"
msgid ""
"The file\n"
"$file$\n"
"does not exist in the local file system."
msgstr ""
"Ar restr \n"
"$file$\n"
"n'eus ket anezhi er reizhiad restroù lec'hel"

#: doclinkdialog.src
msgctxt ""
"doclinkdialog.src\n"
"STR_NAME_CONFLICT\n"
"string.text"
msgid ""
"The name '$file$' is already used for another database.\n"
"Please choose a different name."
msgstr ""
"Dibabet eo bet an anv '$file$' evit ur stlennvon all.\n"
"Dibabit un anv all, mar plij."

#: doclinkdialog.src
msgctxt ""
"doclinkdialog.src\n"
"RID_SVXSTR_QUERY_DELETE_CONFIRM\n"
"string.text"
msgid "Do you want to delete the entry?"
msgstr "C'hoant hoc'h eus da zilemel an enankad ?"

#: optchart.src
msgctxt ""
"optchart.src\n"
"RID_SVXSTR_DIAGRAM_ROW\n"
"string.text"
msgid "Data Series $(ROW)"
msgstr "Steudad roadennoù $(ROW)"

#: optchart.src
msgctxt ""
"optchart.src\n"
"RID_OPTSTR_COLOR_CHART_DELETE\n"
"string.text"
msgid "Chart Color Deletion"
msgstr "Dilamadur livioù an diervad "

#: optcolor.src
msgctxt ""
"optcolor.src\n"
"RID_SVXSTR_COLOR_CONFIG_DELETE\n"
"string.text"
msgid "Do you really want to delete the color scheme?"
msgstr "Fellout a ra deoc'h dilemel al livaoueg da vat ?"

#: optcolor.src
msgctxt ""
"optcolor.src\n"
"RID_SVXSTR_COLOR_CONFIG_DELETE_TITLE\n"
"string.text"
msgid "Color Scheme Deletion"
msgstr "Dilamadur al livaoueg"

#: optcolor.src
msgctxt ""
"optcolor.src\n"
"RID_SVXSTR_COLOR_CONFIG_SAVE1\n"
"string.text"
msgid "Save scheme"
msgstr "Enrollañ ar spletad"

#: optcolor.src
msgctxt ""
"optcolor.src\n"
"RID_SVXSTR_COLOR_CONFIG_SAVE2\n"
"string.text"
msgid "Name of color scheme"
msgstr "Anv al livaoueg"

#: optdict.src
msgctxt ""
"optdict.src\n"
"RID_SVXSTR_OPT_DOUBLE_DICTS\n"
"string.text"
msgid ""
"The specified name already exists.\n"
"Please enter a new name."
msgstr ""
"An anv meneget a zo anezhañ endeo.\n"
"Enankit un anv all."

#: optdict.src
msgctxt ""
"optdict.src\n"
"STR_MODIFY\n"
"string.text"
msgid "~Replace"
msgstr "~Amsaviñ"

#: optdict.src
msgctxt ""
"optdict.src\n"
"RID_SVXSTR_CONFIRM_SET_LANGUAGE\n"
"string.text"
msgid "Do you want to change the '%1' dictionary language?"
msgstr "Ha fellout a ra deoc'h kemmañ yezh ar geriadur '%1' ?"

#: optfltr.src
msgctxt ""
"optfltr.src\n"
"RID_SVXSTR_HEADER1\n"
"string.text"
msgid "[L]"
msgstr "[L]"

#: optfltr.src
msgctxt ""
"optfltr.src\n"
"RID_SVXSTR_HEADER2\n"
"string.text"
msgid "[S]"
msgstr "[S]"

#: optfltr.src
msgctxt ""
"optfltr.src\n"
"RID_SVXSTR_CHG_MATH\n"
"string.text"
msgid "MathType to %PRODUCTNAME Math or reverse"
msgstr "MathType da %PRODUCTNAME Math pe tuginañ"

#: optfltr.src
msgctxt ""
"optfltr.src\n"
"RID_SVXSTR_CHG_WRITER\n"
"string.text"
msgid "WinWord to %PRODUCTNAME Writer or reverse"
msgstr "WinWord da %PRODUCTNAME Writer pe tuginañ"

#: optfltr.src
msgctxt ""
"optfltr.src\n"
"RID_SVXSTR_CHG_CALC\n"
"string.text"
msgid "Excel to %PRODUCTNAME Calc or reverse"
msgstr "Excel da %PRODUCTNAME Calc pe tuginañ"

#: optfltr.src
msgctxt ""
"optfltr.src\n"
"RID_SVXSTR_CHG_IMPRESS\n"
"string.text"
msgid "PowerPoint to %PRODUCTNAME Impress or reverse"
msgstr "PowerPoint da %PRODUCTNAME Impress pe tuginañ"

#: optfltr.src
msgctxt ""
"optfltr.src\n"
"RID_SVXSTR_CHG_SMARTART\n"
"string.text"
msgid "SmartArt to %PRODUCTNAME shapes or reverse"
msgstr "SmartArt davit lunioù %PRODUCTNAME pe ar c'hontrol"

#: optinet2.src
msgctxt ""
"optinet2.src\n"
"RID_SVXSTR_OPT_PROXYPORTS\n"
"string.text"
msgid ""
"Invalid value!\n"
"\n"
"The maximum value for a port number is 65535."
msgstr ""
"Gwerzh didalvoudek!\n"
"\n"
"Gwerzh uc'hek un niverenn borzh a dalv da 65535."

#: optjava.src
msgctxt ""
"optjava.src\n"
"RID_SVXSTR_JRE_NOT_RECOGNIZED\n"
"string.text"
msgid ""
"The folder you selected does not contain a Java runtime environment.\n"
"Please select a different folder."
msgstr ""
"N'eus amva erounit Java ebet en teuliad diuzet ganeoc'h.\n"
"Diuzit un teuliad all, mar plij."

#: optjava.src
msgctxt ""
"optjava.src\n"
"RID_SVXSTR_JRE_FAILED_VERSION\n"
"string.text"
msgid ""
"The Java runtime environment you selected is not the required version.\n"
"Please select a different folder."
msgstr ""
"An amva erounit Java diuzet ganeoc'h n'eo ket an handelv azgoulennet.\n"
"Diuzit un teuliad disheñvel, mar plij."

#: optjava.src
msgctxt ""
"optjava.src\n"
"RID_SVXSTR_OPTIONS_RESTART\n"
"string.text"
msgid "Please restart %PRODUCTNAME now so the new or modified values can take effect."
msgstr "Mar plij, adlañsit %PRODUCTNAME a-benn ma vo efedus ar gwerzhioù nevez pe zaskemmet."

#: optjava.src
msgctxt ""
"optjava.src\n"
"RID_SVXSTR_JAVA_START_PARAM\n"
"string.text"
msgid "Edit Parameter"
msgstr "Daskemmañ an arventenn"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_SPELL\n"
"string.text"
msgid "Spelling"
msgstr "Reizhskrivadur"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_HYPH\n"
"string.text"
msgid "Hyphenation"
msgstr "Troc'hadur ar gerioù"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_THES\n"
"string.text"
msgid "Thesaurus"
msgstr "Geriadur an heñvelsterioù"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_GRAMMAR\n"
"string.text"
msgid "Grammar"
msgstr "Yezhadur"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_CAPITAL_WORDS\n"
"string.text"
msgid "Check uppercase words"
msgstr "Gwiriañ ar gerioù e pennlizherennoù"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_WORDS_WITH_DIGITS\n"
"string.text"
msgid "Check words with numbers "
msgstr "Gwiriañ ar gerioù gant sifroù "

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_SPELL_SPECIAL\n"
"string.text"
msgid "Check special regions"
msgstr "Gwiriañ ar maezioù arbennik"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_SPELL_AUTO\n"
"string.text"
msgid "Check spelling as you type"
msgstr "Gwiriañ ar reizhskrivadur e-pad ar biziata"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_GRAMMAR_AUTO\n"
"string.text"
msgid "Check grammar as you type"
msgstr "Gwiriañ ar yezhadur e-pad ar biziata"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_NUM_MIN_WORDLEN\n"
"string.text"
msgid "Minimal number of characters for hyphenation: "
msgstr "Led ger izek evit an troc'hadur gerioù : "

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_NUM_PRE_BREAK\n"
"string.text"
msgid "Characters before line break: "
msgstr "Arouezennoù a-raok al lamm : "

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_NUM_POST_BREAK\n"
"string.text"
msgid "Characters after line break: "
msgstr "Arouezennoù goude al lamm : "

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_HYPH_AUTO\n"
"string.text"
msgid "Hyphenate without inquiry"
msgstr "Troc'hadur gerioù emgefreek"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_HYPH_SPECIAL\n"
"string.text"
msgid "Hyphenate special regions"
msgstr "Troc'hadur gerioù war ar maezioù arbennik"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_CONFIG_DIR\n"
"string.text"
msgid "Configuration"
msgstr "Kefluniañ"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_WORK_PATH\n"
"string.text"
msgid "My Documents"
msgstr "Ma zeulioù"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_GRAPHICS_PATH\n"
"string.text"
msgid "Images"
msgstr "Skeudennoù"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_BITMAP_PATH\n"
"string.text"
msgid "Icons"
msgstr "Arlunioù"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_PALETTE_PATH\n"
"string.text"
msgid "Palettes"
msgstr "Livaouegoù"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_BACKUP_PATH\n"
"string.text"
msgid "Backups"
msgstr "Gwaredoù"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_MODULES_PATH\n"
"string.text"
msgid "Modules"
msgstr "Molladoù"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_TEMPLATE_PATH\n"
"string.text"
msgid "Templates"
msgstr "Patromoù"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_GLOSSARY_PATH\n"
"string.text"
msgid "AutoText"
msgstr "EmDestenn"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_DICTIONARY_PATH\n"
"string.text"
msgid "Dictionaries"
msgstr "Geriadurioù"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_HELP_DIR\n"
"string.text"
msgid "Help"
msgstr "Skoazell"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_GALLERY_DIR\n"
"string.text"
msgid "Gallery"
msgstr "Skeudennaoueg"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_STORAGE_DIR\n"
"string.text"
msgid "Message Storage"
msgstr "Kadavadur ar c'hemennadennoù"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_TEMP_PATH\n"
"string.text"
msgid "Temporary files"
msgstr "Restroù padennek"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_PLUGINS_PATH\n"
"string.text"
msgid "Plug-ins"
msgstr "Enlugelladoù"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_FAVORITES_DIR\n"
"string.text"
msgid "Folder Bookmarks"
msgstr "Sinedoù an teuliadoù"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_FILTER_PATH\n"
"string.text"
msgid "Filters"
msgstr "Siloù"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_ADDINS_PATH\n"
"string.text"
msgid "Add-ins"
msgstr "Molladoù"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_USERCONFIG_PATH\n"
"string.text"
msgid "User Configuration"
msgstr "Kefluniadur an arveriad"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_USERDICTIONARY_DIR\n"
"string.text"
msgid "User-defined dictionaries"
msgstr "Geriadurioù an arveriad"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_CLASSIFICATION_PATH\n"
"string.text"
msgid "Classification"
msgstr "Rummata"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_AUTOCORRECT_DIR\n"
"string.text"
msgid "AutoCorrect"
msgstr "EmReizhañ"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_LINGUISTIC_DIR\n"
"string.text"
msgid "Writing aids"
msgstr "Skoazelloù reizhskrivañ"

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_SEARCHTERM\n"
"string.text"
msgid "Search term"
msgstr "Klask un termen"

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_SELECTEDPERSONA\n"
"string.text"
msgid "Selected Theme: "
msgstr "Neuz diuzet : "

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_SEARCHING\n"
"string.text"
msgid "Searching, please wait..."
msgstr "O klask, gortozit mar plij..."

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_SEARCHERROR\n"
"string.text"
msgid "Cannot open %1, please try again later."
msgstr "N'haller ket digeriñ %1, klaskit diwezhatoc'h, mar plij."

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_NORESULTS\n"
"string.text"
msgid "No results found."
msgstr "Disoc'h ebet bet kavet."

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_APPLYPERSONA\n"
"string.text"
msgid "Applying Theme..."
msgstr "O seveniñ an neuz..."

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_PERSONA_CATEGORIES\n"
"LibreOffice\n"
"itemlist.text"
msgid "LibreOffice"
msgstr "LibreOffice"

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_PERSONA_CATEGORIES\n"
"Abstract\n"
"itemlist.text"
msgid "Abstract"
msgstr "Kendod"

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_PERSONA_CATEGORIES\n"
"Color\n"
"itemlist.text"
msgid "Color"
msgstr "Liv"

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_PERSONA_CATEGORIES\n"
"Music\n"
"itemlist.text"
msgid "Music"
msgstr "Sonerezh"

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_PERSONA_CATEGORIES\n"
"Nature\n"
"itemlist.text"
msgid "Nature"
msgstr "Natur"

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_PERSONA_CATEGORIES\n"
"Solid\n"
"itemlist.text"
msgid "Solid"
msgstr "Unvan"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"%PRODUCTNAME\n"
"itemlist.text"
msgid "%PRODUCTNAME"
msgstr "%PRODUCTNAME"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"User Data\n"
"itemlist.text"
msgid "User Data"
msgstr "Roadoù arveriad"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"General\n"
"itemlist.text"
msgid "General"
msgstr "Hollek"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"Memory\n"
"itemlist.text"
msgid "Memory"
msgstr "Memor"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"View\n"
"itemlist.text"
msgid "View"
msgstr "Gwelout"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"Print\n"
"itemlist.text"
msgid "Print"
msgstr "Moullañ"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"Paths\n"
"itemlist.text"
msgid "Paths"
msgstr "Treugoù"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"Fonts\n"
"itemlist.text"
msgid "Fonts"
msgstr "Nodrezhoù"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"Security\n"
"itemlist.text"
msgid "Security"
msgstr "Diogelroez"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"Personalization\n"
"itemlist.text"
msgid "Personalization"
msgstr "Personelaat"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"Application Colors\n"
"itemlist.text"
msgid "Application Colors"
msgstr "Livioù an arload"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"Accessibility\n"
"itemlist.text"
msgid "Accessibility"
msgstr "Haezadusted"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"Advanced\n"
"itemlist.text"
msgid "Advanced"
msgstr "Kempleshoc'h"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"Basic IDE Options\n"
"itemlist.text"
msgid "Basic IDE Options"
msgstr "Dibarzhioù diazez an amva diorren enkorfet"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"Online Update\n"
"itemlist.text"
msgid "Online Update"
msgstr "Hizivaat enlinenn"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"OpenCL\n"
"itemlist.text"
msgid "OpenCL"
msgstr "OpenCL"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_LANGUAGE_OPTIONS\n"
"Language Settings\n"
"itemlist.text"
msgid "Language Settings"
msgstr "Arventennoù yezh"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_LANGUAGE_OPTIONS\n"
"Languages\n"
"itemlist.text"
msgid "Languages"
msgstr "Yezhoù"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_LANGUAGE_OPTIONS\n"
"Writing Aids\n"
"itemlist.text"
msgid "Writing Aids"
msgstr "Skoazelloù reizhskrivañ"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_LANGUAGE_OPTIONS\n"
"Searching in Japanese\n"
"itemlist.text"
msgid "Searching in Japanese"
msgstr "Klask e japaneg"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_LANGUAGE_OPTIONS\n"
"Asian Layout\n"
"itemlist.text"
msgid "Asian Layout"
msgstr "Pajennaozañ aziat"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_LANGUAGE_OPTIONS\n"
"Complex Text Layout\n"
"itemlist.text"
msgid "Complex Text Layout"
msgstr "Skridoù o neuz kemplezh"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_INET_DLG\n"
"Internet\n"
"itemlist.text"
msgid "Internet"
msgstr "Kenrouedad"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_INET_DLG\n"
"Proxy\n"
"itemlist.text"
msgid "Proxy"
msgstr "Proksi"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_INET_DLG\n"
"E-mail\n"
"itemlist.text"
msgid "E-mail"
msgstr "Postel"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"%PRODUCTNAME Writer\n"
"itemlist.text"
msgid "%PRODUCTNAME Writer"
msgstr "%PRODUCTNAME Writer"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"General\n"
"itemlist.text"
msgid "General"
msgstr "Hollek"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"View\n"
"itemlist.text"
msgid "View"
msgstr "Gwelout"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"Formatting Aids\n"
"itemlist.text"
msgid "Formatting Aids"
msgstr "Skoazell ventrezhañ"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"Grid\n"
"itemlist.text"
msgid "Grid"
msgstr "Kael"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"Basic Fonts (Western)\n"
"itemlist.text"
msgid "Basic Fonts (Western)"
msgstr "Nodrezhoù diazez (Kornog)"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"Basic Fonts (Asian)\n"
"itemlist.text"
msgid "Basic Fonts (Asian)"
msgstr "Nodrezhoù diazez (Azia)"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"Basic Fonts (CTL)\n"
"itemlist.text"
msgid "Basic Fonts (CTL)"
msgstr "Nodrezhoù diazez (Pajennaozañ kemplezh)"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"Print\n"
"itemlist.text"
msgid "Print"
msgstr "Moullañ"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"Table\n"
"itemlist.text"
msgid "Table"
msgstr "Taolenn"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"Changes\n"
"itemlist.text"
msgid "Changes"
msgstr "Kemmoù"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"Comparison\n"
"itemlist.text"
msgid "Comparison"
msgstr "Keñveriadur"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"Compatibility\n"
"itemlist.text"
msgid "Compatibility"
msgstr "Keverlec'hded"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"AutoCaption\n"
"itemlist.text"
msgid "AutoCaption"
msgstr "Leadell emgefreek"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"Mail Merge E-mail\n"
"itemlist.text"
msgid "Mail Merge E-mail"
msgstr "Postel mailing"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_ONLINEOPTIONS\n"
"%PRODUCTNAME Writer/Web\n"
"itemlist.text"
msgid "%PRODUCTNAME Writer/Web"
msgstr "%PRODUCTNAME Writer/Web"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_ONLINEOPTIONS\n"
"View\n"
"itemlist.text"
msgid "View"
msgstr "Gwelout"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_ONLINEOPTIONS\n"
"Formatting Aids\n"
"itemlist.text"
msgid "Formatting Aids"
msgstr "Skoazell ventrezhañ"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_ONLINEOPTIONS\n"
"Grid\n"
"itemlist.text"
msgid "Grid"
msgstr "Kael"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_ONLINEOPTIONS\n"
"Print\n"
"itemlist.text"
msgid "Print"
msgstr "Moullañ"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_ONLINEOPTIONS\n"
"Table\n"
"itemlist.text"
msgid "Table"
msgstr "Taolenn"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_ONLINEOPTIONS\n"
"Background\n"
"itemlist.text"
msgid "Background"
msgstr "Drekva"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SM_EDITOPTIONS\n"
"%PRODUCTNAME Math\n"
"itemlist.text"
msgid "%PRODUCTNAME Math"
msgstr "%PRODUCTNAME Math"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SM_EDITOPTIONS\n"
"Settings\n"
"itemlist.text"
msgid "Settings"
msgstr "Arventennoù"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SC_EDITOPTIONS\n"
"%PRODUCTNAME Calc\n"
"itemlist.text"
msgid "%PRODUCTNAME Calc"
msgstr "%PRODUCTNAME Calc"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SC_EDITOPTIONS\n"
"General\n"
"itemlist.text"
msgid "General"
msgstr "Hollek"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SC_EDITOPTIONS\n"
"Defaults\n"
"itemlist.text"
msgid "Defaults"
msgstr "Dre ziouer"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SC_EDITOPTIONS\n"
"View\n"
"itemlist.text"
msgid "View"
msgstr "Gwelout"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SC_EDITOPTIONS\n"
"Calculate\n"
"itemlist.text"
msgid "Calculate"
msgstr "Jediñ"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SC_EDITOPTIONS\n"
"Formula\n"
"itemlist.text"
msgid "Formula"
msgstr "Formulenn"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SC_EDITOPTIONS\n"
"Sort Lists\n"
"itemlist.text"
msgid "Sort Lists"
msgstr "Rolloù rummañ"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SC_EDITOPTIONS\n"
"Changes\n"
"itemlist.text"
msgid "Changes"
msgstr "Kemmoù"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SC_EDITOPTIONS\n"
"Compatibility\n"
"itemlist.text"
msgid "Compatibility"
msgstr "Keverlec'hded"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SC_EDITOPTIONS\n"
"Grid\n"
"itemlist.text"
msgid "Grid"
msgstr "Kael"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SC_EDITOPTIONS\n"
"Print\n"
"itemlist.text"
msgid "Print"
msgstr "Moullañ"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SD_EDITOPTIONS\n"
"%PRODUCTNAME Impress\n"
"itemlist.text"
msgid "%PRODUCTNAME Impress"
msgstr "%PRODUCTNAME Impress"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SD_EDITOPTIONS\n"
"General\n"
"itemlist.text"
msgid "General"
msgstr "Hollek"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SD_EDITOPTIONS\n"
"View\n"
"itemlist.text"
msgid "View"
msgstr "Gwelout"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SD_EDITOPTIONS\n"
"Grid\n"
"itemlist.text"
msgid "Grid"
msgstr "Kael"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SD_EDITOPTIONS\n"
"Print\n"
"itemlist.text"
msgid "Print"
msgstr "Moullañ"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SD_GRAPHIC_OPTIONS\n"
"%PRODUCTNAME Draw\n"
"itemlist.text"
msgid "%PRODUCTNAME Draw"
msgstr "%PRODUCTNAME Draw"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SD_GRAPHIC_OPTIONS\n"
"General\n"
"itemlist.text"
msgid "General"
msgstr "Hollek"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SD_GRAPHIC_OPTIONS\n"
"View\n"
"itemlist.text"
msgid "View"
msgstr "Gwelout"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SD_GRAPHIC_OPTIONS\n"
"Grid\n"
"itemlist.text"
msgid "Grid"
msgstr "Kael"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SD_GRAPHIC_OPTIONS\n"
"Print\n"
"itemlist.text"
msgid "Print"
msgstr "Moullañ"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SCH_EDITOPTIONS\n"
"Charts\n"
"itemlist.text"
msgid "Charts"
msgstr "Diervadoù"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SCH_EDITOPTIONS\n"
"Default Colors\n"
"itemlist.text"
msgid "Default Colors"
msgstr "Livioù dre ziouer"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_FILTER_DLG\n"
"Load/Save\n"
"itemlist.text"
msgid "Load/Save"
msgstr "Kargañ/Enrollañ"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_FILTER_DLG\n"
"General\n"
"itemlist.text"
msgid "General"
msgstr "Hollek"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_FILTER_DLG\n"
"VBA Properties\n"
"itemlist.text"
msgid "VBA Properties"
msgstr "Perzhioù VBA"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_FILTER_DLG\n"
"Microsoft Office\n"
"itemlist.text"
msgid "Microsoft Office"
msgstr "Microsoft Office"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_FILTER_DLG\n"
"HTML Compatibility\n"
"itemlist.text"
msgid "HTML Compatibility"
msgstr "Keverlec'hded HTML"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SB_STARBASEOPTIONS\n"
"%PRODUCTNAME Base\n"
"itemlist.text"
msgid "%PRODUCTNAME Base"
msgstr "%PRODUCTNAME Base"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SB_STARBASEOPTIONS\n"
"Connections\n"
"itemlist.text"
msgid "Connections"
msgstr "Kennaskadurioù"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SB_STARBASEOPTIONS\n"
"Databases\n"
"itemlist.text"
msgid "Databases"
msgstr "Stlennvonioù"

#. extracted from wizards/source/euro
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-04-22 23:40+0200\n"
"PO-Revision-Date: 2015-05-11 23:05+0000\n"
"Last-Translator: system user <>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 2.7\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1431385537.000000\n"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_ZERO\n"
"string.text"
msgid "~Cancel"
msgstr "~Encaboxar"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_ZERO + 1\n"
"string.text"
msgid "~Help"
msgstr "~Ayuda"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_ZERO + 2\n"
"string.text"
msgid "<<~Back"
msgstr "<<~Volver"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_ZERO + 3\n"
"string.text"
msgid "~Convert"
msgstr "~Convertir"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_ZERO + 4\n"
"string.text"
msgid "Note: Currency amounts from external links and currency conversion factors in formulas cannot be converted."
msgstr "Nota: Les cantidaes en divises d'enllaces esternos y los factores de conversión de divises nes fórmules nun pueden convertise."

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_ZERO + 5\n"
"string.text"
msgid "First, unprotect all sheets."
msgstr "Primero, desprotexe toles fueyes."

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_ZERO + 6\n"
"string.text"
msgid "Currencies:"
msgstr "Divises:"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_ZERO + 7\n"
"string.text"
msgid "C~ontinue>>"
msgstr "C~ontinuar>>"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_ZERO + 8\n"
"string.text"
msgid "C~lose"
msgstr "~Zarrar"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_CONVERTER\n"
"string.text"
msgid "~Entire document"
msgstr "Docum~entu enteru"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_CONVERTER + 1\n"
"string.text"
msgid "Selection"
msgstr "Esbilla"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_CONVERTER + 2\n"
"string.text"
msgid "Cell S~tyles"
msgstr "Es~tilos de caxella"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_CONVERTER + 3\n"
"string.text"
msgid "Currency cells in the current ~sheet"
msgstr "Caxelle~s de moneda na fueya actual"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_CONVERTER + 4\n"
"string.text"
msgid "Currency cells in the entire ~document"
msgstr "Caxelles de moneda en tol ~documentu"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_CONVERTER + 5\n"
"string.text"
msgid "~Selected range"
msgstr "Intervalu ~seleicionáu"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_CONVERTER + 6\n"
"string.text"
msgid "Select Cell Styles"
msgstr "Escoyer los estilos de caxella"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_CONVERTER + 7\n"
"string.text"
msgid "Select currency cells"
msgstr "Seleicionar les caxelles monetaries"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_CONVERTER + 8\n"
"string.text"
msgid "Currency ranges:"
msgstr "Intervalos monetarios:"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_CONVERTER + 9\n"
"string.text"
msgid "Templates:"
msgstr "Modelos:"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_AUTOPILOT\n"
"string.text"
msgid "Extent"
msgstr "Estensión"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_AUTOPILOT + 1\n"
"string.text"
msgid "~Single %PRODUCTNAME Calc document"
msgstr "Un ~solu documentu de %PRODUCTNAME Calc"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_AUTOPILOT + 2\n"
"string.text"
msgid "Complete ~directory"
msgstr "~Direutoriu completu"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_AUTOPILOT + 3\n"
"string.text"
msgid "Source Document:"
msgstr "Documentu fonte:"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_AUTOPILOT + 4\n"
"string.text"
msgid "Source directory:"
msgstr "Direutoriu fonte:"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_AUTOPILOT + 5\n"
"string.text"
msgid "~Including subfolders"
msgstr "~Incluyendo sodireutorios"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_AUTOPILOT + 6\n"
"string.text"
msgid "Target directory:"
msgstr "Direutoriu destín:"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_AUTOPILOT + 7\n"
"string.text"
msgid "Temporarily unprotect sheet without query"
msgstr "Desprotexer temporalmente la fueya ensin consultar"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_AUTOPILOT + 10\n"
"string.text"
msgid "Also convert fields and tables in text documents"
msgstr "Convertir tamién campos y tables de documentos de testu"

#: euro.src
msgctxt ""
"euro.src\n"
"STATUSLINE\n"
"string.text"
msgid "Conversion status: "
msgstr "Estáu de la conversión: "

#: euro.src
msgctxt ""
"euro.src\n"
"STATUSLINE + 1\n"
"string.text"
msgid "Conversion status of the cell templates:"
msgstr "Estáu de la conversión de los modelos de caxelles:"

#: euro.src
msgctxt ""
"euro.src\n"
"STATUSLINE + 2\n"
"string.text"
msgid "Registration of the relevant ranges: Sheet %1Number%1 of %2TotPageCount%2"
msgstr "Rexistru de los intervalos relevantes: Fueya %1Number%1 de %2TotPageCount%2"

#: euro.src
msgctxt ""
"euro.src\n"
"STATUSLINE + 3\n"
"string.text"
msgid "Entry of the ranges to be converted..."
msgstr "Entrada de los intervalos pa convertir..."

#: euro.src
msgctxt ""
"euro.src\n"
"STATUSLINE + 4\n"
"string.text"
msgid "Sheet protection for each sheet will be restored..."
msgstr "La proteición de fueyes va restaurase pa cada fueya..."

#: euro.src
msgctxt ""
"euro.src\n"
"STATUSLINE + 5\n"
"string.text"
msgid "Conversion of the currency units in the cell templates..."
msgstr "Conversión de les unidaes de moneda nos modelos de caxella..."

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES\n"
"string.text"
msgid "~Finish"
msgstr "~Finar"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 1\n"
"string.text"
msgid "Select directory"
msgstr "Seleicionar direutoriu"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 2\n"
"string.text"
msgid "Select file"
msgstr "Seleicionar archivu"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 3\n"
"string.text"
msgid "Select target directory"
msgstr "Seleicionar direutoriu destín"

#: euro.src
#, fuzzy
msgctxt ""
"euro.src\n"
"MESSAGES + 4\n"
"string.text"
msgid "non-existent"
msgstr "non esistente"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 5\n"
"string.text"
msgid "Euro Converter"
msgstr "Convertidor d'Euros"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 6\n"
"string.text"
msgid "Should protected spreadsheets be temporarily unprotected?"
msgstr "¿Quier desprotexer temporalmente les fueyes protexíes?"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 7\n"
"string.text"
msgid "Enter the password to unprotect the table %1TableName%1"
msgstr "Contraseña pa desprotexer la tabla %1TableName%1"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 8\n"
"string.text"
msgid "Wrong Password!"
msgstr "¡Contraseña incorreuta!"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 9\n"
"string.text"
msgid "Protected Sheet"
msgstr "Fueya Protexida"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 10\n"
"string.text"
msgid "Warning!"
msgstr "¡Avisu!"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 11\n"
"string.text"
msgid "Protection for the sheets will not be removed."
msgstr "Nun se quitará la proteición de les fueyes."

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 12\n"
"string.text"
msgid "Sheet cannot be unprotected"
msgstr "La fueya nun se pue desprotexer"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 13\n"
"string.text"
msgid "The Wizard cannot edit this document as cell formats cannot be modified in documents containing protected spreadsheets."
msgstr "L'asistente nun ye a editar esti documentu porque nun se puen camudar los formatos de caxella en documentos con fueyes protexíes."

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 14\n"
"string.text"
msgid "Please note that the Euro Converter will, otherwise, not be able to edit this document!"
msgstr "¡Tenga en cuenta que, d'otra manera, el Convertidor d'euros nun podrá editar esti documentu!"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 15\n"
"string.text"
msgid "Please choose a currency to be converted first!"
msgstr "¡Escueya primero la moneda que quier convertir!"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 16\n"
"string.text"
msgid "Password:"
msgstr "Clave:"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 17\n"
"string.text"
msgid "OK"
msgstr "Aceutar"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 18\n"
"string.text"
msgid "Cancel"
msgstr "Encaboxar"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 19\n"
"string.text"
msgid "Please select a %PRODUCTNAME Calc document for editing!"
msgstr "¡Escueya un documentu de %PRODUCTNAME Calc pa editalu!"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 20\n"
"string.text"
msgid "'<1>' is not a directory!"
msgstr "'<1>' nun ye un direutoriu!"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 21\n"
"string.text"
msgid "Document is read-only!"
msgstr "¡El documentu ye sólo de llectura!"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 22\n"
"string.text"
msgid "The '<1>' file already exists.<CR>Do you want to overwrite it?"
msgstr "Yá existe'l ficheru '<1>'.<CR>¿Quies sobrescribilu?"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 23\n"
"string.text"
msgid "Do you really want to terminate conversion at this point?"
msgstr "¿De verdá quier parar la conversión nesti puntu?"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 24\n"
"string.text"
msgid "Cancel Wizard"
msgstr "Encaboxar asistente"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES\n"
"string.text"
msgid "Portuguese Escudo"
msgstr "Escudu Portugués"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 1\n"
"string.text"
msgid "Dutch Guilder"
msgstr "Florín holandés"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 2\n"
"string.text"
msgid "French Franc"
msgstr "Francu Francés"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 3\n"
"string.text"
msgid "Spanish Peseta"
msgstr "Peseta española"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 4\n"
"string.text"
msgid "Italian Lira"
msgstr "Llira italiana"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 5\n"
"string.text"
msgid "German Mark"
msgstr "Marcu Alemán"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 6\n"
"string.text"
msgid "Belgian Franc"
msgstr "Francu Belga"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 7\n"
"string.text"
msgid "Irish Punt"
msgstr "Llibra irlandesa"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 8\n"
"string.text"
msgid "Luxembourg Franc"
msgstr "Francu Lluxemburgués"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 9\n"
"string.text"
msgid "Austrian Schilling"
msgstr "Chelín Austriacu"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 10\n"
"string.text"
msgid "Finnish Mark"
msgstr "Marcu finlandés"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 11\n"
"string.text"
msgid "Greek Drachma"
msgstr "Dracma Griegu"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 12\n"
"string.text"
msgid "Slovenian Tolar"
msgstr "Tolar Eslovenu"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 13\n"
"string.text"
msgid "Cypriot Pound"
msgstr "Llibra Chipriota"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 14\n"
"string.text"
msgid "Maltese Lira"
msgstr "Lira Maltesa"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 15\n"
"string.text"
msgid "Slovak Koruna"
msgstr "Koruna Eslovacu"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 16\n"
"string.text"
msgid "Estonian Kroon"
msgstr "Corona Estonia"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 17\n"
"string.text"
msgid "Latvian Lats"
msgstr "Lats letón"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 18\n"
"string.text"
msgid "Lithuanian Litas"
msgstr ""

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_LASTPAGE\n"
"string.text"
msgid "Progress"
msgstr "Progresu"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_LASTPAGE + 1\n"
"string.text"
msgid "Retrieving the relevant documents..."
msgstr "Recuperando los documentos importantes..."

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_LASTPAGE + 2\n"
"string.text"
msgid "Converting the documents..."
msgstr "Convirtiendo los documentos..."

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_LASTPAGE + 3\n"
"string.text"
msgid "Settings:"
msgstr "Configuración:"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_LASTPAGE + 4\n"
"string.text"
msgid "Sheet is always unprotected"
msgstr "Anular siempre la protección de fueya"

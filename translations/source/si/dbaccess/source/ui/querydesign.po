#. extracted from dbaccess/source/ui/querydesign
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-03-11 22:12+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: si\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1457734348.000000\n"

#: query.src
msgctxt ""
"query.src\n"
"RID_MENU_JOINVIEW_CONNECTION\n"
"SID_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr ""

#: query.src
msgctxt ""
"query.src\n"
"RID_MENU_JOINVIEW_CONNECTION\n"
"ID_QUERY_EDIT_JOINCONNECTION\n"
"menuitem.text"
msgid "Edit..."
msgstr ""

#: query.src
msgctxt ""
"query.src\n"
"RID_MENU_JOINVIEW_TABLE\n"
"SID_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr ""

#: query.src
msgctxt ""
"query.src\n"
"RID_QUERYCOLPOPUPMENU\n"
"ID_BROWSER_COLWIDTH\n"
"menuitem.text"
msgid "Column ~Width..."
msgstr ""

#: query.src
msgctxt ""
"query.src\n"
"RID_QUERYCOLPOPUPMENU\n"
"SID_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr ""

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_TABWINSHOW\n"
"string.text"
msgid "Add Table Window"
msgstr "වගු කවුළුවක් එක් කරන්න"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_MOVETABWIN\n"
"string.text"
msgid "Move table window"
msgstr "වගු කවුළුව ගමන් ගෙනයන්න"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_INSERTCONNECTION\n"
"string.text"
msgid "Insert Join"
msgstr "සන්ධිය ඇතුළු කරන්න"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_REMOVECONNECTION\n"
"string.text"
msgid "Delete Join"
msgstr "සන්ධිය මකන්න"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_SIZETABWIN\n"
"string.text"
msgid "Resize table window"
msgstr "වගු කවුළුව නැවත ප්‍රමාණ කරන්න"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_TABFIELDDELETE\n"
"string.text"
msgid "Delete Column"
msgstr "තීරුව මකන්න"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_TABFIELDMOVED\n"
"string.text"
msgid "Move column"
msgstr "තීරුව ගෙනයන්න"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_TABFIELDCREATE\n"
"string.text"
msgid "Add Column"
msgstr "තීරුව එක් කරන්න"

#: query.src
msgctxt ""
"query.src\n"
"RID_STR_TABLE_DOESNT_EXIST\n"
"string.text"
msgid "Invalid expression, table '$name$' does not exist."
msgstr "අවලංගු ප්‍රකාශයක්, '$name$' වගුව නොපවතී."

#: query.src
msgctxt ""
"query.src\n"
"RID_STR_FIELD_DOESNT_EXIST\n"
"string.text"
msgid "Invalid expression, field name '$name$' does not exist."
msgstr "අවලංගු ප්‍රකාශයක්, '$name$' ක්ෂේත්‍ර නාමය නොපවතී."

#: query.src
msgctxt ""
"query.src\n"
"RID_STR_TOMUCHTABLES\n"
"string.text"
msgid "The query covers #num# tables. The selected database type, however, can only process a maximum of #maxnum# table(s) per statement."
msgstr "විමසුම වගු #num# ප්‍රමාණයක් ආවරණය කරයි. කෙසේ වෙතත් තෝරා ඇති දත්ත සමුදාය වර්ගයට ප්‍රකාශයක් සඳහා සැකසුම් කලහැක්කේ වගුව(වගු) #maxnum# උපරිම ගණනක් පමණි."

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_TABWINDELETE\n"
"string.text"
msgid "Delete Table Window"
msgstr "වගු කවුළුව මකන්න"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_MODIFY_CELL\n"
"string.text"
msgid "Edit Column Description"
msgstr "තීරු විස්තරය සංස්කරණය කරන්න"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_SIZE_COLUMN\n"
"string.text"
msgid "Adjust column width"
msgstr "තීරු පළල සීරුමාරු කරන්න"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_SORTTEXT\n"
"string.text"
msgid "(not sorted);ascending;descending"
msgstr "(සුබෙදා නැත);ආරෝහණ;අවරෝහණ"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_FUNCTIONS\n"
"string.text"
msgid "(no function);Group"
msgstr "(ක්‍රියාවක් නැත);කාණ්ඩ කරන්න"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_NOTABLE\n"
"string.text"
msgid "(no table)"
msgstr "(වගුවක් නැත)"

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_ORDERBY_UNRELATED\n"
"string.text"
msgid "The database only supports sorting for visible fields."
msgstr "දත්ත සමුදාය සහය දක්වන්නේ දෘශ්‍ය ක්ෂේත්‍ර සුබෙදීමට පමණි."

#: query.src
msgctxt ""
"query.src\n"
"RID_QUERYFUNCTION_POPUPMENU\n"
"ID_QUERY_FUNCTION\n"
"menuitem.text"
msgid "Functions"
msgstr "ක්‍රියා"

#: query.src
msgctxt ""
"query.src\n"
"RID_QUERYFUNCTION_POPUPMENU\n"
"ID_QUERY_TABLENAME\n"
"menuitem.text"
msgid "Table Name"
msgstr "වගු නාමය"

#: query.src
msgctxt ""
"query.src\n"
"RID_QUERYFUNCTION_POPUPMENU\n"
"ID_QUERY_ALIASNAME\n"
"menuitem.text"
msgid "Alias"
msgstr "අන්වර්ථ නාමය"

#: query.src
msgctxt ""
"query.src\n"
"RID_QUERYFUNCTION_POPUPMENU\n"
"ID_QUERY_DISTINCT\n"
"menuitem.text"
msgid "Distinct Values"
msgstr "ප්‍රභින්න අගයන්"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_HANDLETEXT\n"
"string.text"
msgid "Field;Alias;Table;Sort;Visible;Function;Criterion;Or;Or"
msgstr "ක්ෂේත්‍රය;අන්වර්ථ නාමය;වගුව;සුපබදීම;දෘශ්‍ය;ක්‍රියාව;උපමානය;හෝ;හෝ"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_LIMIT_ALL\n"
"string.text"
msgid "All"
msgstr "සියළු"

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_TOO_MANY_COLUMNS\n"
"string.text"
msgid "There are too many columns."
msgstr "තීරු ඉතා වැඩි ගණනක් තිබේ."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_CRITERIA_ON_ASTERISK\n"
"string.text"
msgid "A condition cannot be applied to field [*]"
msgstr "[*] ක්ෂේත්‍රයට කොන්දේසියක් යෙදීමට නොහැක"

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_TOO_LONG_STATEMENT\n"
"string.text"
msgid "The SQL statement created is too long."
msgstr "තනන ලද SQL ප්‍රකාශය දිග වැඩිය."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_TOOCOMPLEX\n"
"string.text"
msgid "Query is too complex"
msgstr "විමසුම බොහෝ සංකීර්ණය"

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_NOSELECT\n"
"string.text"
msgid "Nothing has been selected."
msgstr "කිසිවක් තෝරා නොමැත."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_TOOMANYCOND\n"
"string.text"
msgid "Too many search criteria"
msgstr "බොහෝ සෙවුම් උපමාන"

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_SYNTAX\n"
"string.text"
msgid "SQL syntax error"
msgstr "SQL වාග් රීතියේ දෝෂයක්"

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_ORDERBY_ON_ASTERISK\n"
"string.text"
msgid "[*] cannot be used as a sort criterion."
msgstr "[*] සුබෙදුම උපමානයක් ලෙස භාවිතා කළ නොහැක."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_TOO_MANY_TABLES\n"
"string.text"
msgid "There are too many tables."
msgstr "වගු ඉතා වැඩි ගණනක් ඇත."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_NATIVE\n"
"string.text"
msgid "The statement will not be applied when querying in the SQL dialect of the database."
msgstr "දත්ත සමුදායේ SQL උපභාෂාවේ විමසීමේදී ප්‍රකාශය අදාළ නොවනු ඇත."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_ILLEGAL_JOIN\n"
"string.text"
msgid "Join could not be processed"
msgstr "සන්ධිය සැකසීමට නොහැකි විය"

#: query.src
msgctxt ""
"query.src\n"
"STR_SVT_SQL_SYNTAX_ERROR\n"
"string.text"
msgid "Syntax error in SQL statement"
msgstr "SQL ප්‍රකාශයේ වාග් රීති දෝෂයක්"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERYDESIGN_NO_VIEW_SUPPORT\n"
"string.text"
msgid "This database does not support table views."
msgstr "මෙම දත්ත සමුදාය වගු දසුන් සඳහා සහය දක්වන්නේ නැත."

#: query.src
msgctxt ""
"query.src\n"
"STR_NO_ALTER_VIEW_SUPPORT\n"
"string.text"
msgid "This database does not support altering of existing table views."
msgstr "මෙම දත්ත සමුදාය දැනට පවතින වගු දසුන් වෙනස් කිරීමට සහය දක්වන්නේ නැත."

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERYDESIGN_NO_VIEW_ASK\n"
"string.text"
msgid "Do you want to create a query instead?"
msgstr "ඔබට ඒ වෙනුවට විමසුමක් තැනීමට අවශ්‍යද?"

#: query.src
msgctxt ""
"query.src\n"
"STR_DATASOURCE_DELETED\n"
"string.text"
msgid "The corresponding data source has been deleted. Therefore, data relevant to that data source cannot be saved."
msgstr "අනුරූප දත්ත මූලය මකා දමා ඇත. එම නිසා එම දත්ත මූලයට අදාළ දත්ත සුරැකීමට නොහැක."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_COLUMN_NOT_FOUND\n"
"string.text"
msgid "The column '$name$' is unknown."
msgstr "'$name$' තීරුව නොදනී."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_JOIN_COLUMN_COMPARE\n"
"string.text"
msgid "Columns can only be compared using '='."
msgstr "තීරු සංසන්දනය කළ හැක්කේ '=' භාවිතයෙන් පමණි."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_LIKE_LEFT_NO_COLUMN\n"
"string.text"
msgid "You must use a column name before 'LIKE'."
msgstr "ඔබ 'LIKE' ට පෙර තීරු නාමයක් භාවිතා කළ යුතුයි."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_CHECK_CASESENSITIVE\n"
"string.text"
msgid "The column could not be found. Please note that the database is case-sensitive."
msgstr "තීරුව සොයා ගැනීමට නොහැකි විය. දත්ත සමුදාය අක්‍ෂර සංවේදී බව කරුණාකර සලකන්න."

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERYDESIGN\n"
"string.text"
msgid " - %PRODUCTNAME Base: Query Design"
msgstr " - %PRODUCTNAME පාදම: විමසුම සැලැස්ම"

#: query.src
msgctxt ""
"query.src\n"
"STR_VIEWDESIGN\n"
"string.text"
msgid " - %PRODUCTNAME Base: View Design"
msgstr " - %PRODUCTNAME පාදම: දසුන් සැලැස්ම"

#. For $object$, one of the values of the RSC_QUERY_OBJECT_TYPE resource will be inserted.
#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_SAVEMODIFIED\n"
"string.text"
msgid ""
"$object$ has been changed.\n"
"Do you want to save the changes?"
msgstr ""
"$object$ වෙනස් වී ඇත.\n"
"ඔබට වෙනස්කම් සුරැකීමට අවශ්‍යද?"

#. For $object$, one of the values of the RSC_QUERY_OBJECT_TYPE resource (except "SQL command", which doesn't make sense here) will be inserted.
#: query.src
msgctxt ""
"query.src\n"
"STR_ERROR_PARSING_STATEMENT\n"
"string.text"
msgid "$object$ is based on an SQL command which could not be parsed."
msgstr "$object$ SQL විධානයක් මත පදනම්ව ඇති බැවින් එය යොමු කළ නොහැක ."

#. For $object$, one of the values of the RSC_QUERY_OBJECT_TYPE resource (except "SQL command", which doesn't make sense here) will be inserted.
#: query.src
msgctxt ""
"query.src\n"
"STR_INFO_OPENING_IN_SQL_VIEW\n"
"string.text"
msgid "$object$ will be opened in SQL view."
msgstr "$object$ SQL දසුනෙහි විවෘත වනු ඇත."

#: query.src
msgctxt ""
"query.src\n"
"RSC_QUERY_OBJECT_TYPE\n"
"The table view\n"
"itemlist.text"
msgid "The table view"
msgstr ""

#: query.src
msgctxt ""
"query.src\n"
"RSC_QUERY_OBJECT_TYPE\n"
"The query\n"
"itemlist.text"
msgid "The query"
msgstr ""

#: query.src
msgctxt ""
"query.src\n"
"RSC_QUERY_OBJECT_TYPE\n"
"The SQL statement\n"
"itemlist.text"
msgid "The SQL statement"
msgstr ""

#: query.src
msgctxt ""
"query.src\n"
"STR_STATEMENT_WITHOUT_RESULT_SET\n"
"string.text"
msgid "The query does not create a result set, and thus cannot be part of another query."
msgstr "විමසුම ප්‍රතිඵල කුලකයක් සාදන්නේ නැත, ඒ නිසා තවත් විමසුමක අංගයක් වීමට නොහැක."

#: query.src
msgctxt ""
"query.src\n"
"STR_NO_DATASOURCE_OR_CONNECTION\n"
"string.text"
msgid "Both the ActiveConnection and the DataSourceName parameter are missing or wrong - cannot initialize the query designer."
msgstr "ActiveConnection සහ DataSourceName යන පරාමිතීන් දෙකම නැතිවී ඇත හෝ වැරදිය - විමසුම් සැලසුම්කරු ආරම්භ කිරීමට නොහැක."

#: querydlg.src
msgctxt ""
"querydlg.src\n"
"STR_JOIN_TYPE_HINT\n"
"string.text"
msgid "Please note that some databases may not support this join type."
msgstr "සමහර දත්ත සමුදායන් මෙම සන්ධි වර්ගයට සහාය නොදැක්වීමට ඉඩ ඇති බව කරුණාකර සලකන්න."

#: querydlg.src
msgctxt ""
"querydlg.src\n"
"STR_QUERY_INNER_JOIN\n"
"string.text"
msgid "Includes only records for which the contents of the related fields of both tables are identical."
msgstr "වගු දෙකෙහිම අදාළ ක්ෂේත්‍ර වල අන්තර්ගතය සර්වසම වන රෙකෝඩ පමණක් අඩංගු වේ."

#: querydlg.src
msgctxt ""
"querydlg.src\n"
"STR_QUERY_LEFTRIGHT_JOIN\n"
"string.text"
msgid "Contains ALL records from table '%1' but only records from table '%2' where the values in the related fields are matching."
msgstr "'%1' වගුවෙන් සියළු රෙකෝඩද '%2' වගුවෙන් අදාළ ක්ෂේත්‍ර වල අගයන් ගැළපෙන රෙකෝඩද පමණක්ද අඩංගු වේ."

#: querydlg.src
msgctxt ""
"querydlg.src\n"
"STR_QUERY_FULL_JOIN\n"
"string.text"
msgid "Contains ALL records from '%1' and from '%2'."
msgstr "'%2'.ගෙන් සහ '%1' ගෙන් සියළු රෙකෝඩ අඩංගු වේ."

#: querydlg.src
#, fuzzy
msgctxt ""
"querydlg.src\n"
"STR_QUERY_CROSS_JOIN\n"
"string.text"
msgid "Contains the Cartesian product of ALL records from '%1' and from '%2'."
msgstr "'%1' හි සහ '%2' හි සියළු රෙකෝඩ වල කාටිසියානු ගුණිතය අඩංගු වේ."

#: querydlg.src
msgctxt ""
"querydlg.src\n"
"STR_QUERY_NATURAL_JOIN\n"
"string.text"
msgid "Contains only one column for each pair of equally-named columns from '%1' and from '%2'."
msgstr "'%1' හි සහ '%2' හි සෑම සමාන ලෙස නම් කරන ලද තීරු යුගලයක් සඳහාම එක් තීරුවක් පමණක් අඩංගු වේ."

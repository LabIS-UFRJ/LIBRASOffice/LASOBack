#. extracted from scaddins/source/datefunc
msgid ""
msgstr ""
"Project-Id-Version: LO4-1\n"
"Report-Msgid-Bugs-To: https://bugs.freedesktop.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-05-23 12:05+0200\n"
"PO-Revision-Date: 2013-08-01 13:19+0300\n"
"Last-Translator: Tadele Assefa Wami <milkyswd@gmail.com>\n"
"Language-Team: Sidaama translation team\n"
"Language: sid\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-Project-Style: openoffice\n"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DiffWeeks\n"
"1\n"
"string.text"
msgid "Calculates the number of weeks in a specific period"
msgstr "Lamalate kiiro murancho yannanni shalagganno"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DiffWeeks\n"
"2\n"
"string.text"
msgid "Start date"
msgstr "Hanfote barra"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DiffWeeks\n"
"3\n"
"string.text"
msgid "First day of the period"
msgstr "Umi yanna"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DiffWeeks\n"
"4\n"
"string.text"
msgid "End date"
msgstr "Gumulote yanna"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DiffWeeks\n"
"5\n"
"string.text"
msgid "Last day of the period"
msgstr "Gumulote yanna"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DiffWeeks\n"
"6\n"
"string.text"
msgid "Type"
msgstr "Dana"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DiffWeeks\n"
"7\n"
"string.text"
msgid "Type of calculation: Type=0 means the time interval, Type=1 means calendar weeks."
msgstr "Shalagote dana: Dana=0 yaa yannate taxxo, Dana=1 yaa kalandarete lamallaati."

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DiffMonths\n"
"1\n"
"string.text"
msgid "Determines the number of months in a specific period."
msgstr "Agannate kiiro murancho yannanni gumultanno."

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DiffMonths\n"
"2\n"
"string.text"
msgid "Start date"
msgstr "Hanfote barra"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DiffMonths\n"
"3\n"
"string.text"
msgid "First day of the period."
msgstr "Umi yanna."

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DiffMonths\n"
"4\n"
"string.text"
msgid "End date"
msgstr "Gumulote yanna"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DiffMonths\n"
"5\n"
"string.text"
msgid "Last day of the period."
msgstr "Gumulote yanna."

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DiffMonths\n"
"6\n"
"string.text"
msgid "Type"
msgstr "Dana"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DiffMonths\n"
"7\n"
"string.text"
msgid "Type of calculation: Type=0 means the time interval, Type=1 means calendar months."
msgstr "Shalagote dana: Dana=0 yaa yannate taxxo, Dana=1 yaa kalandarete agannaati."

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DiffYears\n"
"1\n"
"string.text"
msgid "Calculates the number of years in a specific period."
msgstr "Dirrate kiiro murancho yannanni shalagganno."

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DiffYears\n"
"2\n"
"string.text"
msgid "Start date"
msgstr "Hanfote barra"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DiffYears\n"
"3\n"
"string.text"
msgid "First day of the period"
msgstr "Umi yanna"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DiffYears\n"
"4\n"
"string.text"
msgid "End date"
msgstr "Gumulote yanna"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DiffYears\n"
"5\n"
"string.text"
msgid "Last day of the period"
msgstr "Gumulote yanna"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DiffYears\n"
"6\n"
"string.text"
msgid "Type"
msgstr "Dana"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DiffYears\n"
"7\n"
"string.text"
msgid "Type of calculation: Type=0 means the time interval, Type=1 means calendar years."
msgstr "Shalagote dana: Dana=0 yaa yannate taxxo, Dana=1 yaa kalandarete dirraati."

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_IsLeapYear\n"
"1\n"
"string.text"
msgid "Returns 1 (TRUE) if the date is a day of a leap year, otherwise 0 (FALSE)."
msgstr "Barru fooqa leekki soodo ikkiro 1 (TRUE) qoltanno, ikka hoogiro 0 (FALSE)."

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_IsLeapYear\n"
"2\n"
"string.text"
msgid "Date"
msgstr "Barra"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_IsLeapYear\n"
"3\n"
"string.text"
msgid "Any day in the desired year"
msgstr "Mixi'noonni diriha ayee barrano"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DaysInMonth\n"
"1\n"
"string.text"
msgid "Returns the number of days of the month in which the date entered occurs"
msgstr "Eino barri leelliro aganu barruwa kiiro qoltanno"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DaysInMonth\n"
"2\n"
"string.text"
msgid "Date"
msgstr "Barra"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DaysInMonth\n"
"3\n"
"string.text"
msgid "Any day in the desired month"
msgstr "Mixi'noonni aganiha ayee barrano"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DaysInYear\n"
"1\n"
"string.text"
msgid "Returns the number of days of the year in which the date entered occurs."
msgstr "Eino barri leelliro diru barruwa kiiro qoltanno."

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DaysInYear\n"
"2\n"
"string.text"
msgid "Date"
msgstr "Barra"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_DaysInYear\n"
"3\n"
"string.text"
msgid "Any day in the desired year"
msgstr "Mixi'noonni diriha ayee barrano"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_WeeksInYear\n"
"1\n"
"string.text"
msgid "Returns the number of weeks of the year in which the date entered occurs"
msgstr "Eino barri leelliro diru lamalla kiiro qoltanno"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_WeeksInYear\n"
"2\n"
"string.text"
msgid "Date"
msgstr "Barra"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_WeeksInYear\n"
"3\n"
"string.text"
msgid "Any day in the desired year"
msgstr "Mixi'noonni diriha ayee barrano"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_Rot13\n"
"1\n"
"string.text"
msgid "Encrypts or decrypts a text using the ROT13 algorithm"
msgstr "ROT13 algorizime horonsi'ratenni borro maafudissanno woy maafudiweelsitanno"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_Rot13\n"
"2\n"
"string.text"
msgid "Text"
msgstr "Borro"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_DESCRIPTIONS.DATE_FUNCDESC_Rot13\n"
"3\n"
"string.text"
msgid "Text to be encrypted or text already encrypted"
msgstr "Maafudisantanno woy maafudisantino borro"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_NAMES\n"
"DATE_FUNCNAME_DiffWeeks\n"
"string.text"
msgid "WEEKS"
msgstr "WEEKS"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_NAMES\n"
"DATE_FUNCNAME_DiffMonths\n"
"string.text"
msgid "MONTHS"
msgstr "MONTHS"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_NAMES\n"
"DATE_FUNCNAME_DiffYears\n"
"string.text"
msgid "YEARS"
msgstr "YEARS"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_NAMES\n"
"DATE_FUNCNAME_IsLeapYear\n"
"string.text"
msgid "ISLEAPYEAR"
msgstr "ISLEAPYEAR"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_NAMES\n"
"DATE_FUNCNAME_DaysInMonth\n"
"string.text"
msgid "DAYSINMONTH"
msgstr "DAYSINMONTH"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_NAMES\n"
"DATE_FUNCNAME_DaysInYear\n"
"string.text"
msgid "DAYSINYEAR"
msgstr "DAYSINYEAR"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_NAMES\n"
"DATE_FUNCNAME_WeeksInYear\n"
"string.text"
msgid "WEEKSINYEAR"
msgstr "WEEKSINYEAR"

#: datefunc.src
msgctxt ""
"datefunc.src\n"
"RID_DATE_FUNCTION_NAMES\n"
"DATE_FUNCNAME_Rot13\n"
"string.text"
msgid "ROT13"
msgstr "ROT13"

#. extracted from desktop/uiconfig/ui
msgid ""
msgstr ""
"Project-Id-Version: LibO 40l10n\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-12-01 12:11+0100\n"
"PO-Revision-Date: 2017-01-30 16:55+0000\n"
"Last-Translator: Kolbjørn Stuestøl <kolbjoern@stuestoel.no>\n"
"Language-Team: none\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1485795341.000000\n"

#: cmdlinehelp.ui
msgctxt ""
"cmdlinehelp.ui\n"
"CmdLineHelp\n"
"title\n"
"string.text"
msgid "Help Message"
msgstr "Hjelpmelding"

#: dependenciesdialog.ui
msgctxt ""
"dependenciesdialog.ui\n"
"Dependencies\n"
"title\n"
"string.text"
msgid "System dependencies check"
msgstr "Kontroll av systemkrav"

#: dependenciesdialog.ui
msgctxt ""
"dependenciesdialog.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "The extension cannot be installed as the following system dependencies are not fulfilled:"
msgstr "Utvidinga kan ikkje installerast fordi desse systemkrava er ikkje oppfylte:"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"ExtensionManagerDialog\n"
"title\n"
"string.text"
msgid "Extension Manager"
msgstr "Utvidingar"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"optionsbtn\n"
"label\n"
"string.text"
msgid "_Options"
msgstr "_Innstillingar"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"updatebtn\n"
"label\n"
"string.text"
msgid "Check for _Updates"
msgstr "Sjå etter _oppdateringar"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"addbtn\n"
"label\n"
"string.text"
msgid "_Add"
msgstr "_Legg til"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"removebtn\n"
"label\n"
"string.text"
msgid "_Remove"
msgstr "_Fjern"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"enablebtn\n"
"label\n"
"string.text"
msgid "_Enable"
msgstr "Aktiv_er"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"shared\n"
"label\n"
"string.text"
msgid "Installed for all users"
msgstr "Installert for alle brukarar"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"user\n"
"label\n"
"string.text"
msgid "Installed for current user"
msgstr "Installert for gjeldande brukar"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"bundled\n"
"label\n"
"string.text"
msgid "Bundled with %PRODUCTNAME"
msgstr "Bunta med  %PRODUCTNAME"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Display Extensions"
msgstr "Vis utvidingane"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"progressft\n"
"label\n"
"string.text"
msgid "Adding %EXTENSION_NAME"
msgstr "Legg til %EXTENSION_NAME"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"getextensions\n"
"label\n"
"string.text"
msgid "Get more extensions online..."
msgstr "Hent fleire utvidingar på Internett …"

#: installforalldialog.ui
msgctxt ""
"installforalldialog.ui\n"
"InstallForAllDialog\n"
"text\n"
"string.text"
msgid "For whom do you want to install the extension?"
msgstr "Kven skal utvidingane installerast for?"

#: installforalldialog.ui
msgctxt ""
"installforalldialog.ui\n"
"InstallForAllDialog\n"
"secondary_text\n"
"string.text"
msgid "Make sure that no further users are working with the same %PRODUCTNAME, when installing an extension for all users in a multi user environment."
msgstr "Sjå til at ingen andre brukarar arbeidar med same %PRODUCTNAME når du installerer utvidingar i eit fleirbrukarmiljø."

#: installforalldialog.ui
msgctxt ""
"installforalldialog.ui\n"
"no\n"
"label\n"
"string.text"
msgid "_For all users"
msgstr "_For alle brukarane"

#: installforalldialog.ui
msgctxt ""
"installforalldialog.ui\n"
"yes\n"
"label\n"
"string.text"
msgid "_Only for me"
msgstr "_Berre for meg"

#: licensedialog.ui
msgctxt ""
"licensedialog.ui\n"
"LicenseDialog\n"
"title\n"
"string.text"
msgid "Extension Software License Agreement"
msgstr "Lisensavtale for programutvidingar"

#: licensedialog.ui
msgctxt ""
"licensedialog.ui\n"
"accept\n"
"label\n"
"string.text"
msgid "Accept"
msgstr "Godta"

#: licensedialog.ui
msgctxt ""
"licensedialog.ui\n"
"decline\n"
"label\n"
"string.text"
msgid "Decline"
msgstr "Avslå"

#: licensedialog.ui
msgctxt ""
"licensedialog.ui\n"
"head\n"
"label\n"
"string.text"
msgid "Please follow these steps to proceed with the installation of the extension:"
msgstr "Følg desse stega for å gå vidare med installasjonen av utvidinga:"

#: licensedialog.ui
msgctxt ""
"licensedialog.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "1."
msgstr "1."

#: licensedialog.ui
msgctxt ""
"licensedialog.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "2."
msgstr "2."

#: licensedialog.ui
msgctxt ""
"licensedialog.ui\n"
"label4\n"
"label\n"
"string.text"
msgid "Read the complete License Agreement. Use the scroll bar or the 'Scroll Down' button in this dialog to view the entire license text."
msgstr "Les gjennom heile lisensavtalen. Bruk rullefeltet eller knappen «Bla ned» i dette dialogvindauget for å lesa heile teksten."

#: licensedialog.ui
msgctxt ""
"licensedialog.ui\n"
"label5\n"
"label\n"
"string.text"
msgid "Accept the License Agreement for the extension by pressing the 'Accept' button."
msgstr "Godta lisensavtalen for utvidinga ved å trykkja på «Godta»."

#: licensedialog.ui
msgctxt ""
"licensedialog.ui\n"
"down\n"
"label\n"
"string.text"
msgid "_Scroll Down"
msgstr "Rull _ned"

#: showlicensedialog.ui
msgctxt ""
"showlicensedialog.ui\n"
"ShowLicenseDialog\n"
"title\n"
"string.text"
msgid "Extension Software License Agreement"
msgstr "Lisensavtale for programutvidingar"

#: updatedialog.ui
msgctxt ""
"updatedialog.ui\n"
"UpdateDialog\n"
"title\n"
"string.text"
msgid "Extension Update"
msgstr "Oppdater utvidinga"

#: updatedialog.ui
msgctxt ""
"updatedialog.ui\n"
"INSTALL\n"
"label\n"
"string.text"
msgid "_Install"
msgstr "_Installer"

#: updatedialog.ui
msgctxt ""
"updatedialog.ui\n"
"UPDATE_LABEL\n"
"label\n"
"string.text"
msgid "_Available extension updates"
msgstr "_Tilgjengelege oppdateringar til utvidingane"

#: updatedialog.ui
msgctxt ""
"updatedialog.ui\n"
"UPDATE_CHECKING\n"
"label\n"
"string.text"
msgid "Checking..."
msgstr "Kontrollerer …"

#: updatedialog.ui
msgctxt ""
"updatedialog.ui\n"
"UPDATE_ALL\n"
"label\n"
"string.text"
msgid "_Show all updates"
msgstr "_Vis alle oppdateringane"

#: updatedialog.ui
msgctxt ""
"updatedialog.ui\n"
"DESCRIPTION_LABEL\n"
"label\n"
"string.text"
msgid "Description"
msgstr "Skildring"

#: updatedialog.ui
msgctxt ""
"updatedialog.ui\n"
"PUBLISHER_LABEL\n"
"label\n"
"string.text"
msgid "Publisher:"
msgstr "Utgjevar:"

#: updatedialog.ui
msgctxt ""
"updatedialog.ui\n"
"PUBLISHER_LINK\n"
"label\n"
"string.text"
msgid "button"
msgstr "knapp"

#: updatedialog.ui
msgctxt ""
"updatedialog.ui\n"
"RELEASE_NOTES_LABEL\n"
"label\n"
"string.text"
msgid "What is new:"
msgstr "Kva er nytt:"

#: updatedialog.ui
msgctxt ""
"updatedialog.ui\n"
"RELEASE_NOTES_LINK\n"
"label\n"
"string.text"
msgid "Release notes"
msgstr "Utgjevingsnotat"

#: updateinstalldialog.ui
msgctxt ""
"updateinstalldialog.ui\n"
"UpdateInstallDialog\n"
"title\n"
"string.text"
msgid "Download and Installation"
msgstr "Nedlasting og installering"

#: updateinstalldialog.ui
msgctxt ""
"updateinstalldialog.ui\n"
"DOWNLOADING\n"
"label\n"
"string.text"
msgid "Downloading extensions..."
msgstr "Lastar ned utvidingar …"

#: updateinstalldialog.ui
msgctxt ""
"updateinstalldialog.ui\n"
"RESULTS\n"
"label\n"
"string.text"
msgid "Result"
msgstr "Resultat"

#: updaterequireddialog.ui
msgctxt ""
"updaterequireddialog.ui\n"
"UpdateRequiredDialog\n"
"title\n"
"string.text"
msgid "Extension Update Required"
msgstr "Utvidingane må oppdaterast"

#: updaterequireddialog.ui
msgctxt ""
"updaterequireddialog.ui\n"
"updatelabel\n"
"label\n"
"string.text"
msgid "%PRODUCTNAME has been updated to a new version. Some installed %PRODUCTNAME extensions are not compatible with this version and need to be updated before they can be used."
msgstr "%PRODUCTNAME er oppdatert til ein ny versjon. Nokre av dei installerte %PRODUCTNAME-utvidingane samspelar ikkje med denne versjonen og må oppdaterast før dei kan brukast."

#: updaterequireddialog.ui
msgctxt ""
"updaterequireddialog.ui\n"
"progresslabel\n"
"label\n"
"string.text"
msgid "Adding %EXTENSION_NAME"
msgstr "Legg til %EXTENSION_NAME"

#: updaterequireddialog.ui
msgctxt ""
"updaterequireddialog.ui\n"
"check\n"
"label\n"
"string.text"
msgid "Check for _Updates..."
msgstr "Sjå etter _oppdateringar …"

#: updaterequireddialog.ui
msgctxt ""
"updaterequireddialog.ui\n"
"disable\n"
"label\n"
"string.text"
msgid "Disable all"
msgstr "Slå av alle"

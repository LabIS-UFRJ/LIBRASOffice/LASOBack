#. extracted from vcl/source/edit
msgid ""
msgstr ""
"Project-Id-Version: LibO 40l10n\n"
"Report-Msgid-Bugs-To: https://bugs.freedesktop.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-05-23 12:05+0200\n"
"PO-Revision-Date: 2012-12-26 19:20+0000\n"
"Last-Translator: Kolbjørn <kolbjoern@stuestoel.no>\n"
"Language-Team: none\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1356549659.0\n"

#: textundo.src
msgctxt ""
"textundo.src\n"
"STR_TEXTUNDO_DELPARA\n"
"string.text"
msgid "delete line"
msgstr "slett linje"

#: textundo.src
msgctxt ""
"textundo.src\n"
"STR_TEXTUNDO_CONNECTPARAS\n"
"string.text"
msgid "delete multiple lines"
msgstr "slett fleire linjer"

#: textundo.src
msgctxt ""
"textundo.src\n"
"STR_TEXTUNDO_SPLITPARA\n"
"string.text"
msgid "insert multiple lines"
msgstr "set inn fleire linjer"

#: textundo.src
msgctxt ""
"textundo.src\n"
"STR_TEXTUNDO_INSERTCHARS\n"
"string.text"
msgid "insert '$1'"
msgstr "set inn «$1»"

#: textundo.src
msgctxt ""
"textundo.src\n"
"STR_TEXTUNDO_REMOVECHARS\n"
"string.text"
msgid "delete '$1'"
msgstr "slett «$1»"

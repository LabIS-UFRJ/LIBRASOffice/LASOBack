#. extracted from uui/source
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-11-10 19:33+0100\n"
"PO-Revision-Date: 2015-06-26 03:03+0000\n"
"Last-Translator: system user <>\n"
"Language-Team: Oriya <oriya-it@googlegroups.com>\n"
"Language: or\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 2.7\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1435287792.000000\n"

#: alreadyopen.src
#, fuzzy
msgctxt ""
"alreadyopen.src\n"
"STR_ALREADYOPEN_TITLE\n"
"string.text"
msgid "Document in Use"
msgstr "ବ୍ୟବହାର ହେଉଥିବା ଦଲିଲ"

#: alreadyopen.src
msgctxt ""
"alreadyopen.src\n"
"STR_ALREADYOPEN_MSG\n"
"string.text"
msgid ""
"Document file '$(ARG1)' is locked for editing by yourself on a different system since $(ARG2)\n"
"\n"
"Open document read-only, or ignore own file locking and open the document for editing.\n"
"\n"
msgstr ""
"ଦଲିଲ ଫାଇଲ '$(ARG1)' କୁ ସମ୍ପାଦନ ପାଇଁ ନିଜ ଦ୍ୱାରା ଅପରିବର୍ତ୍ତନୀୟ ରଖାଯାଇଛି $(ARG2)\n"
"\n"
"କେବଳ-ପଠନୀୟ ଦଲିଲକୁ ଖୋଲନ୍ତୁ, କିମ୍ବା ନିଜର ଫାଇଲ ଅପରିବର୍ତ୍ତନୀୟତାକୁ ଅଗ୍ରାହ୍ୟ କରନ୍ତୁ ଏବଂ ସମ୍ପାଦନ ପାଇଁ ଦଲିଲ ଖୋଲନ୍ତୁ।\n"
"\n"

#: alreadyopen.src
msgctxt ""
"alreadyopen.src\n"
"STR_ALREADYOPEN_READONLY_BTN\n"
"string.text"
msgid "Open ~Read-Only"
msgstr "କେବଳ-ପଠନୀୟକୁ ଖୋଲନ୍ତୁ (~R)"

#: alreadyopen.src
msgctxt ""
"alreadyopen.src\n"
"STR_ALREADYOPEN_OPEN_BTN\n"
"string.text"
msgid "~Open"
msgstr "ଖୋଲ"

#: alreadyopen.src
msgctxt ""
"alreadyopen.src\n"
"STR_ALREADYOPEN_SAVE_MSG\n"
"string.text"
msgid ""
"Document file '$(ARG1)' is locked for editing by yourself on a different system since $(ARG2)\n"
"\n"
"Close document on other system and retry saving or ignore own file locking and save current document.\n"
"\n"
msgstr ""
"ଦଲିଲ ଫାଇଲ '$(ARG1)' କୁ ସମ୍ପାଦନ ପାଇଁ ନିଜ ଦ୍ୱାରା ଅପରିବର୍ତ୍ତନୀୟ ରଖାଯାଇଛି $(ARG2)\n"
"\n"
"କେବଳ-ପଠନୀୟ ଦଲିଲକୁ ବନ୍ଦ କରନ୍ତୁ, କିମ୍ବା ନିଜର ଫାଇଲ ଅପରିବର୍ତ୍ତନୀୟତାକୁ ଅଗ୍ରାହ୍ୟ କରନ୍ତୁ ଏବଂ ସମ୍ପାଦନ ପାଇଁ ଦଲିଲ ଖୋଲନ୍ତୁ।\n"
"\n"

#: alreadyopen.src
msgctxt ""
"alreadyopen.src\n"
"STR_ALREADYOPEN_RETRY_SAVE_BTN\n"
"string.text"
msgid "~Retry Saving"
msgstr "ସଂରକ୍ଷଣକୁ ପୁଣିଚେଷ୍ଟା କରନ୍ତୁ (~R)"

#: alreadyopen.src
msgctxt ""
"alreadyopen.src\n"
"STR_ALREADYOPEN_SAVE_BTN\n"
"string.text"
msgid "~Save"
msgstr "ସଞ୍ଚଯ କର"

#: filechanged.src
msgctxt ""
"filechanged.src\n"
"STR_FILECHANGED_TITLE\n"
"string.text"
msgid "Document Has Been Changed by Others"
msgstr "ଅନ୍ୟମାନଙ୍କ ପରି ଦଲିଲକୁ ପରିବର୍ତ୍ତନ କରାଯାଇଛି"

#: filechanged.src
msgctxt ""
"filechanged.src\n"
"STR_FILECHANGED_MSG\n"
"string.text"
msgid ""
"The file has been changed since it was opened for editing in %PRODUCTNAME. Saving your version of the document will overwrite changes made by others.\n"
"\n"
"Do you want to save anyway?\n"
"\n"
msgstr ""
"%PRODUCTNAMEରେ ଖୋଲାହେବା ପରଠୁଁ ଏହି ଫାଇଲଟି ପରିବର୍ତ୍ତନ ହୋଇଛି।ଦଲିଲର ସଂସ୍କରଣକୁ ସଂରକ୍ଷଣ କରିବା ଦ୍ୱାରା ଅନ୍ୟ ପରିବର୍ତ୍ତନଗୁଡ଼ିକରେ ନବଲିଖନ କରିଥାଏ।\n"
"\n"
"ଆପଣ ଯେକୌଣସି ଉପାୟରେ ସଂରକ୍ଷଣ କରିବାକୁ ଚାହୁଁଛନ୍ତି କି?\n"
"\n"

#: filechanged.src
msgctxt ""
"filechanged.src\n"
"STR_FILECHANGED_SAVEANYWAY_BTN\n"
"string.text"
msgid "~Save Anyway"
msgstr "ଯେକୌଣସି ଉପାୟରେ ସଞ୍ଚୟ କରନ୍ତୁ"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_KEEP_PASSWORD\n"
"string.text"
msgid "~Remember password until end of session"
msgstr "~ଅଧିବେଶନ ସମାପ୍ତ ହେବା ପର୍ଯ୍ୟନ୍ତ ପ୍ରବେଶ ସଂକେତ ମନେରଖନ୍ତୁ"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_SAVE_PASSWORD\n"
"string.text"
msgid "~Remember password"
msgstr "ଗୁପ୍ତଶବ୍ଦ ମନେପକାଅ (~R)"

#: ids.src
msgctxt ""
"ids.src\n"
"STR_WARNING_BROKENSIGNATURE_TITLE\n"
"string.text"
msgid "Invalid Document Signature"
msgstr "ଅବୈଧ ଦଲିଲ ହସ୍ତାକ୍ଷର"

#: ids.src
msgctxt ""
"ids.src\n"
"STR_WARNING_INCOMPLETE_ENCRYPTION_TITLE\n"
"string.text"
msgid "Non-Encrypted Streams"
msgstr "ଅସଂଗୁପ୍ତ ଧାରାଗୁଡ଼ିକ"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_ABORT & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The operation executed on $(ARG1) was aborted."
msgstr "$(ARG1)ରେ ନିଷ୍ପାଦିତ ପ୍ରୟୋଗକୁ ଅଗ୍ରାହ୍ୟ କରାଯାଇଛି।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_ACCESSDENIED & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Access to $(ARG1) was denied."
msgstr "$(ARG1) ମଧ୍ଯକୁ ପ୍ରବେଶ ବାରଣ କରାଯାଇଛି।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_ALREADYEXISTS & ERRCODE_RES_MASK)\n"
"string.text"
msgid "$(ARG1) already exists."
msgstr "$(ARG1) ପୂର୍ବରୁ ଅବସ୍ଥିତ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_TARGETALREADYEXISTS & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Target already exists."
msgstr "ଲକ୍ଷ୍ଯ ପୂର୍ବରୁ ଅବସ୍ଥିତ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_MODULESIZEEXCEEDED & ERRCODE_RES_MASK)\n"
"string.text"
msgid ""
"You are about to save/export a password protected basic library containing module(s) \n"
"$(ARG1)\n"
"which are too large to store in binary format. If you wish users that don't have access to the library password to be able to run macros in those module(s) you must split those modules into a number of smaller modules. Do you wish to continue to save/export this library?"
msgstr ""
"ଆପଣ ପ୍ରବେଶ ସଂକେତ ସୁରକ୍ଷିତ ମୌଳିକ ଲାଇବ୍ରେରୀ ଏକକାଂଶ $(ARG1) କୁ ସଂରକ୍ଷଣ/ରପ୍ତାନୀ \n"
"କରିବାକୁ ଯାଉଛନ୍ତି\n"
"ଯାହାକୁକି ଦ୍ୱିମିକ ଶୈଳୀରେ ସଂରକ୍ଷଣ କରିବା ପାଇଁ ବହୁତ ବଡ଼। ଯଦି ଆପଣ ଅନୁମତି ନଥିବା ଚାଳକଙ୍କୁ ମାକ୍ରୋଗୁଡ଼ିକୁ ଚଲାଇବା ପାଇଁ ଅନୁମତି ଦେବାକୁ ଚାହୁଁଛନ୍ତି ତେବେ ଆପଣଙ୍କୁ ସେହି ଏକକାଂଶଗୁଡ଼ିକୁ ବିଚ୍ଛେଦ କରିବାକୁ ହେବ। ଆପଣ ସଂରକ୍ଷଣ/ରପ୍ତାନି କ୍ରିୟାକୁ ଆଗେଇବାକୁ ଚାହୁଁଛନ୍ତି କି?"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_BADCRC & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The data from $(ARG1) has an incorrect checksum."
msgstr "$(ARG1) ରୁ ତଥ୍ୟରେ ଭୁଲ ତଥ୍ୟ ଅଛି।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_CANTCREATE & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The object $(ARG1) cannot be created in directory $(ARG2)."
msgstr "ବସ୍ତୁ $(ARG1) କୁ ଡ଼ିରେକ୍ଟୋରୀ $(ARG2)ରେ ନିର୍ମାଣ କରିହେବ ନାହିଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_CANTREAD & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Data of $(ARG1) could not be read."
msgstr "$(ARG1) ର ତଥ୍ୟକୁ ପଢ଼ିହେବ ନାହିଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_CANTSEEK & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The seek operation on $(ARG1) could not be performed."
msgstr "$(ARG1)ରେ ଥିବା ସିକ ପ୍ରୟୋଗକୁ କାର୍ଯ୍ୟକାରୀ କରିହେବ ନାହିଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_CANTTELL & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The tell operation on $(ARG1) could not be performed."
msgstr "$(ARG1)ରେ ଥିବା ଟେଲ ପ୍ରୟୋଗକୁ କାର୍ଯ୍ୟକାରୀ କରିହେବ ନାହିଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_CANTWRITE & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Data for $(ARG1) could not be written."
msgstr "$(ARG1) ପାଇଁ ତଥ୍ୟକୁ ଲେଖାଯାଇପାରିବ ନାହିଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_CURRENTDIR & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Action impossible: $(ARG1) is the current directory."
msgstr "ଅସମ୍ଭବ କାର୍ଯ୍ୟ: $(ARG1) ଟି ହେଉଛି ପ୍ରଚଳିତ ଡ଼ିରେକ୍ଟୋରୀ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTREADY & ERRCODE_RES_MASK)\n"
"string.text"
msgid "$(ARG1) is not ready."
msgstr "$(ARG1) ଟି ପ୍ରସ୍ତୁତ ନାହିଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTSAMEDEVICE & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Action impossible: $(ARG1) and $(ARG2) are different devices (drives)."
msgstr "ଅସମ୍ଭବ କାର୍ଯ୍ୟ: $(ARG1) ଏବଂ $(ARG2) ହେଉଛି ଭିନ୍ନ ଉପକରଣ (ଡ୍ରାଇଭ)।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_GENERAL & ERRCODE_RES_MASK)\n"
"string.text"
msgid "General input/output error while accessing $(ARG1)."
msgstr "$(ARG1) ରେ ପ୍ରବେଶ କରିବା ସମୟରେ ସାଧାରଣ ନିବେଶ/ନିର୍ଗମ ତ୍ରୁଟି।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_INVALIDACCESS & ERRCODE_RES_MASK)\n"
"string.text"
msgid "An attempt was made to access $(ARG1) in an invalid way."
msgstr "$(ARG1) କୁ ଅବୈଧ ଉପାୟରେ ପ୍ରବେଶ କରିବା ପାଇଁ ଥରେ ଚେଷ୍ଟା କରାଯାଇଛି।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_INVALIDCHAR & ERRCODE_RES_MASK)\n"
"string.text"
msgid "$(ARG1) contains invalid characters."
msgstr "$(ARG1) ଅବୈଧ ଅକ୍ଷର ଧାରଣ କରିଥାଏ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_INVALIDDEVICE & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The device (drive) $(ARG1) is invalid."
msgstr "ଉପକରଣ (ଡ୍ରାଇଭ) $(ARG1) ଟି ଅବୈଧ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_INVALIDLENGTH & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The data from $(ARG1) has an invalid length."
msgstr "$(ARG1) ରୁ ତଥ୍ୟଟି ଅବୈଧ ଲମ୍ବା ଅଟେ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_INVALIDPARAMETER & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The operation on $(ARG1) was started with an invalid parameter."
msgstr "$(ARG1) ଉପରେ ପ୍ରୟୋଗକୁ ଅବୈଧ ପ୍ରାଚଳ ମାଧ୍ଯମରେ ଆରମ୍ଭ କରାଯାଇଛି।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_ISWILDCARD & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The operation cannot be performed because $(ARG1) contains wildcards."
msgstr "ପ୍ରୟୋଗକୁ କାର୍ଯ୍ୟକାରୀ କରିହେବ ନାହିଁ କାରଣ $(ARG1) wildcardଗୁଡ଼ିକୁ ଧାରଣ କରିଅଛି।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_LOCKVIOLATION & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Error during shared access to $(ARG1)."
msgstr "$(ARG1)କୁ ଭାଗକରି ପ୍ରବେଶକରିବା ସମଯରେ ତୃଟି"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_MISPLACEDCHAR & ERRCODE_RES_MASK)\n"
"string.text"
msgid "$(ARG1) contains misplaced characters."
msgstr "$(ARG1) ସ୍ଥାନପରିବର୍ତ୍ତିତ ଅକ୍ଷରଗୁଡ଼ିକୁ ଧାରଣ କରିଅଛି।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NAMETOOLONG & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The name $(ARG1) contains too many characters."
msgstr "ନାମ $(ARG1) ରେ ଅତ୍ୟଧିକ ଅକ୍ଷର ଅଛି।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTEXISTS & ERRCODE_RES_MASK)\n"
"string.text"
msgid "$(ARG1) does not exist."
msgstr "$(ARG1) ଅବସ୍ଥିତ ନାହିଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTEXISTSPATH & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The path $(ARG1) does not exist."
msgstr "ପଥ $(ARG1) ଅବସ୍ଥିତ ନାହିଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTSUPPORTED & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The operation on $(ARG1) is not supported on this operating system."
msgstr "$(ARG1) ରେ ପ୍ରୟୋଗକୁ ଏହି ପ୍ରଚାଳନ ତନ୍ତ୍ରରେ ସମର୍ଥନ କରାଯାଇପାରିବ ନାହିଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTADIRECTORY & ERRCODE_RES_MASK)\n"
"string.text"
msgid "$(ARG1) is not a directory."
msgstr "$(ARG1) ଟି ଗୋଟିଏ ଡ଼ିରେକ୍ଟୋରୀ ନୁହଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTAFILE & ERRCODE_RES_MASK)\n"
"string.text"
msgid "$(ARG1) is not a file."
msgstr "$(ARG1) ଟି ଗୋଟିଏ ଫାଇଲ ନୁହଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_OUTOFSPACE & ERRCODE_RES_MASK)\n"
"string.text"
msgid "There is no space left on device $(ARG1)."
msgstr "ଉପକରଣ $(ARG1)ରେ କୌଣସି ସ୍ଥାନ ବଳିନାହିଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_TOOMANYOPENFILES & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The operation on $(ARG1) cannot be performed because too many files are already open."
msgstr "$(ARG1) ରେ ପ୍ରୟୋଗକୁ କାର୍ଯ୍ୟକାରୀ କରିହେବ ନାହିଁ କାରଣ ପୂର୍ବରୁ ଅତ୍ୟଧିକ ଫାଇଲ ଖୋଲାଅଛି।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_OUTOFMEMORY & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The operation on $(ARG1) cannot be performed because there is no more memory available."
msgstr "$(ARG1)ରେ ପ୍ରୟୋଗକୁ କାର୍ଯ୍ୟକାରୀ କରିହେବ ନାହିଁ କାରଣ ସେଠାରେ ବଳକା ସ୍ଥାନ ନାହିଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_PENDING & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The operation on $(ARG1) cannot continue because more data is pending."
msgstr "$(ARG1) ରେ ପ୍ରୟୋଗକୁ ଆଗେଇ ହେବ ନାହିଁ କାରଣ ଅଧିକ ତଥ୍ୟ ବଳିଅଛି।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_RECURSIVE & ERRCODE_RES_MASK)\n"
"string.text"
msgid "$(ARG1) cannot be copied into itself."
msgstr "$(ARG1) ଟି ନିଜ ମଧ୍ଯରେ ନକଲ ହୋଇପାରିବ ନାହିଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_UNKNOWN & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Unknown input/output error while accessing $(ARG1)."
msgstr "$(ARG1)ରେ ପ୍ରବେଶ କରିବା ସମୟରେ ଅଜ୍ଞାତ ନିବେଶ/ଫଳାଫଳ ତ୍ରୁଟି।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_WRITEPROTECTED & ERRCODE_RES_MASK)\n"
"string.text"
msgid "$(ARG1) is write protected."
msgstr "$(ARG1) ଟି ଲେଖା ପ୍ରତିବନ୍ଧିତ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_WRONGFORMAT & ERRCODE_RES_MASK)\n"
"string.text"
msgid "$(ARG1) is not in the correct format."
msgstr "$(ARG1) ଟି ସଠିକ ଶୈଳୀରେ ନାହିଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_WRONGVERSION & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The version of $(ARG1) is not correct."
msgstr "ସଂସ୍କରଣ $(ARG1) ଟି ସଠିକ ନୁହଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTEXISTS_VOLUME & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Drive $(ARG1) does not exist."
msgstr "ଡ୍ରାଇଭ $(ARG1) ଅବସ୍ଥିତ ନାହିଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTEXISTS_FOLDER & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Folder $(ARG1) does not exist."
msgstr "ପୋଲଡର $(ARG1) ଅବସ୍ଥିତ ନାହିଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_WRONGJAVA & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The installed Java version is not supported."
msgstr "ସ୍ଥାପିତ Java ସଂସ୍କରଣ ସମର୍ଥିତ ନୁହଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_WRONGJAVA_VERSION & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The installed Java version $(ARG1) is not supported."
msgstr "ସ୍ଥାପିତ Java ସଂସ୍କରଣ $(ARG1) ସମର୍ଥିତ ନୁହଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_WRONGJAVA_MIN & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The installed Java version is not supported, at least version $(ARG1) is required."
msgstr "ସ୍ଥାପିତ Java ସଂସ୍କରଣ ସମର୍ଥିତ ନୁହଁ, ଅତିକମରେ ସଂସ୍କରଣ $(ARG1) ଆବଶ୍ୟକ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_WRONGJAVA_VERSION_MIN & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The installed Java version $(ARG1) is not supported, at least version $(ARG2) is required."
msgstr "ସ୍ଥାପିତ Java ସଂସ୍କରଣ $(ARG1) ସମର୍ଥିତ ନୁହଁ, ଅତିକମରେ ସଂସ୍କରଣ $(ARG2) ଆବଶ୍ୟକ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_BADPARTNERSHIP & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The data associated with the partnership is corrupted."
msgstr "ଅଶୀଂଦାର ସହିତ ସପୃକ୍ତ ହୋଇ ଥିବା ତାରିଖ ଭ୍ରଷ୍ଟ ହୋଇଛି।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_BADPARTNERSHIP_NAME & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The data associated with the partnership $(ARG1) is corrupted."
msgstr "$(ARG1) ଅଶୀଂଦାର ସହିତ ସପୃକ୍ତ ହୋଇ ଥିବା ତାରିଖ ଭ୍ରଷ୍ଟ ହୋଇଛି।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTREADY_VOLUME & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Volume $(ARG1) is not ready."
msgstr "ତିବ୍ରତା $(ARG1) ଟି ପ୍ରସ୍ତୁତ ନାହିଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTREADY_REMOVABLE & ERRCODE_RES_MASK)\n"
"string.text"
msgid "$(ARG1) is not ready; please insert a storage medium."
msgstr "$(ARG1) ଟି ପ୍ରସ୍ତୁତ ନାହିଁ; ଦୟାକରି ଗୋଟିଏ ଭଣ୍ଡାର ମାଧ୍ୟମ ଭର୍ତ୍ତିକରନ୍ତୁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTREADY_VOLUME_REMOVABLE & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Volume $(ARG1) is not ready; please insert a storage medium."
msgstr "ତୀବ୍ରତା $(ARG1) ପ୍ରସ୍ତୁତ ନାହିଁ; ଦୟାକରି ଗୋଟିଏ ଭଣ୍ଡାର ମାଧ୍ୟମ ଭର୍ତ୍ତିକରନ୍ତୁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_WRONGMEDIUM & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Please insert disk $(ARG1)."
msgstr "ଦୟାକରି ଡ଼ିସ୍କ $(ARG1)କୁ ଭର୍ତ୍ତିକରନ୍ତୁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_CANTCREATE_NONAME & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The object cannot be created in directory $(ARG1)."
msgstr "ଡିରେକ୍ଟୋରୀ $(ARG1)ରେ ବସ୍ତୁକୁ ନିର୍ମାଣ କରିହେବ ନାହିଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_UNSUPPORTEDOVERWRITE & ERRCODE_RES_MASK)\n"
"string.text"
msgid "%PRODUCTNAME cannot keep files from being overwritten when this transmission protocol is used. Do you want to continue anyway?"
msgstr "ଏହି ଟ୍ରାସମିସନ ପ୍ରୋଟୋକଲ ଉରୟୋଗହୋଇଥିବା ସମଯରେ ପୁନଃଲିଖନ ହୋଇ ଥିବା ଫାଇଲଗୁଡିକୁ %PRODUCTNAME ରଖିପାରିବ ନାହିଁ । ତୁମେ ୟେକୌଣସି ପ୍ରକାରରେ ଚାଲୁ ରଖିବାକୁ ଚାହୁଁଛ କି ?"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_BROKENPACKAGE & ERRCODE_RES_MASK)\n"
"string.text"
msgid ""
"The file '$(ARG1)' is corrupt and therefore cannot be opened. %PRODUCTNAME can try to repair the file.\n"
"\n"
"The corruption could be the result of document manipulation or of structural document damage due to data transmission.\n"
"\n"
"We recommend that you do not trust the content of the repaired document.\n"
"Execution of macros is disabled for this document.\n"
"\n"
"Should %PRODUCTNAME repair the file?\n"
msgstr ""
"ଫାଇଲ '$(ARG1)' ଟି ତ୍ରୁଟିଯୁକ୍ତ ଏବଂ ତେଣୁ କୋଲାଯାଇପାରିବ ନାହିଁ। %PRODUCTNAME ସେହି ଫାଇଲକୁ ଠିକ କରିବା ପାଇଁ ଚେଷ୍ଟାକରିବ।\n"
"\n"
"ତ୍ରୁଟିଟି ଦଲିଲ ପ୍ରକଳନ ଅଥବା ସଂରଚନା ଦଲିଲର ଫଳାଫଳ ତଥ୍ୟ ପରିବହନ ଫଳରେ।\n"
"\n"
"ଆମେ ପରାମର୍ଶ ଦେଉଅଛୁ ଯେ ଆପଣ ସେହି ପ୍ରସ୍ତୁତ ବିଷୟବସ୍ତୁକୁ ବିଶ୍ୱାସ କରନ୍ତୁ ନାହିଁ।\n"
"ମାକ୍ରୋଗୁଡ଼ିକର ନିଷ୍ପାଦନକୁ ଏହି ଦଲିଲ ପାଇଁ ନିଷ୍କ୍ରିୟ କରାଯାଇଛି।\n"
"\n"
"%PRODUCTNAME ସେହି ଫାଇଲକୁ ସଜାଡ଼ିବା ଉଚିତ କି?\n"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_BROKENPACKAGE_CANTREPAIR & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The file '$(ARG1)' could not be repaired and therefore cannot be opened."
msgstr "ଏହି ଫାଇଲ '$(ARG1)' ମରାମତିହୋଇପାରିବ ନାହିଁ ଏବଂ ତେଣୁ ଖୋଲା ହୋଇ ପାରିବ ନାହିଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_CONFIGURATION_BROKENDATA_NOREMOVE & ERRCODE_RES_MASK)\n"
"string.text"
msgid ""
"Configuration data in '$(ARG1)' is corrupted. Without this data some functions may not operate correctly.\n"
"Do you want to continue startup of %PRODUCTNAME without the corrupted configuration data?"
msgstr ""
"'$(ARG1)' କନଫିଗରେସନ ତଥ୍ଯ  ଭ୍ରଷ୍ଟହୋଇଛି।ଏହି ତଥ୍ଯ ବ୍ଯତିତ କେତେକ କାର୍ୟ୍ଯଗୁଡିକ ଭଲଭାବରେ କାର୍ୟ୍ଯକାରୀ ହୋଇ ନ ପାରେ। \n"
"ଭ୍ରଷ୍ଟହୋଇଥିବା କନଫିଗରେସନ ତଥ୍ଯ ବ୍ଯତିତ %PRODUCTNAMEର ସ୍ଟାଟଅପକୁ ତୁମେ ଚାଲୁ ରଖିବାକୁ ଚହୁଁଛ କି?"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_CONFIGURATION_BROKENDATA_WITHREMOVE & ERRCODE_RES_MASK)\n"
"string.text"
msgid ""
"The personal configuration file '$(ARG1)' is corrupted and must be deleted to continue. Some of your personal settings may be lost.\n"
"Do you want to continue startup of %PRODUCTNAME without the corrupted configuration data?"
msgstr ""
"'$(ARG1)' ବ୍ଯକ୍ତିଗତ କନଫିଗରେସନ ଫାଇଲ ଭ୍ରଷ୍ଟହୋଇଛି ଏବଂ ଚାଲୁକରିବା ପାଇଁ ନିଶ୍ଚଯ ବିଲୋପ ହେବା ଦରକାର। ତୁମର କେତେକ ବ୍ଯକ୍ତିଗତ ସେଟିଙ୍ଗଗୁଡିକ ନଷ୍ଟ ହୋଇ ପାରେ।\n"
"ଭ୍ରଷ୍ଟହୋଇଥିବା କନଫିଗରେସନ ତଥ୍ଯ ବ୍ଯତିତ %PRODUCTNAMEର ସ୍ଟାଟଅପକୁ ତୁମେ ଚାଲୁ ରଖିବାକୁ ଚହୁଁଛ କି?"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_CONFIGURATION_BACKENDMISSING & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The configuration data source '$(ARG1)' is unavailable. Without this data some functions may not operate correctly."
msgstr "'$(ARG1)'  କନଫିଗରେସନ ତଥ୍ଯ ମୂଳ ଉପଲବ୍ଧ ନାହିଁ।ଏହି ତଥ୍ଯ ବ୍ଯତିତ କେତେକ କାର୍ୟ୍ଯଗୁଡିକ ଭଲଭାବରେ କାର୍ୟ୍ଯକାରୀ ହୋଇ ନ ପାରେ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_CONFIGURATION_BACKENDMISSING_WITHRECOVER & ERRCODE_RES_MASK)\n"
"string.text"
msgid ""
"The configuration data source '$(ARG1)' is unavailable. Without this data some functions may not operate correctly.\n"
"Do you want to continue startup of %PRODUCTNAME without the missing configuration data?"
msgstr ""
"'$(ARG1)' କନଫିଗରେସନ ତଥ୍ଯ ମୂଳ ଉପଲବ୍ଧ ନାହିଁ। ଏହି ତଥ୍ଯ ବ୍ଯତିତ କେତେକ କାର୍ୟ୍ଯଗୁଡିକ ଭଲଭାବରେ କାର୍ୟ୍ଯକାରୀ ହୋଇ ନ ପାରେ। \n"
"ଅଦୃଶ୍ଯ କନଫିଗରେସନ ତଥ୍ଯ ବ୍ଯତିତ %PRODUCTNAMEର ସ୍ଟାଟଅପକୁ ତୁମେ ଚାଲୁ ରଖିବାକୁ ଚହୁଁଛ କି?"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_INVALID_XFORMS_SUBMISSION_DATA & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The form contains invalid data. Do you still want to continue?"
msgstr "ଫର୍ମରେ ଅମାନ୍ଯ ତଥ୍ଯ ଅଛି।ତୁମେ ଏପର୍ୟ୍ଯନ୍ତ ଚାଲୁ ରଖିବାକୁ ଚାହୁଁଛ କି?"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_LOCKING_LOCKED & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The file $(ARG1) is locked by another user. Currently, another write access to this file cannot be granted."
msgstr "ଫାଇଲ $(ARG1) କୁ ଅନ୍ୟ ଏକ ଚାଳକ ଦ୍ୱାରା ଅପରିବର୍ତ୍ତନୀୟ କରାଯାଇଛି। ବର୍ତ୍ତମାନ, ସେହି ଫାଇଲ ପାଇଁ ଅନ୍ୟ ଏକ ଲିଖନ ଅନୁମତି ଦେଇହେବ ନାହିଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_LOCKING_LOCKED_SELF & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The file $(ARG1) is locked by yourself. Currently, another write access to this file cannot be granted."
msgstr "ଫାଇଲ $(ARG1) କୁ ଅନ୍ୟ ଆପଣଙ୍କ ଦ୍ୱାରା ଅପରିବର୍ତ୍ତନୀୟ ହୋଇଛି। ବର୍ତ୍ତମାନ, ସେହି ଫାଇଲ ପାଇଁ ଅନ୍ୟ ଏକ ଲିଖନ ଅନୁମତି ଦେଇହେବ ନାହିଁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_LOCKING_NOT_LOCKED & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The file $(ARG1) is currently not locked by yourself."
msgstr "ଫାଇଲ $(ARG1) କୁ ଅନ୍ୟ ଆପଣଙ୍କ ଦ୍ୱାରା ଅପରିବର୍ତ୍ତନୀୟ ହୋଇଛି।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_LOCKING_LOCK_EXPIRED & ERRCODE_RES_MASK)\n"
"string.text"
msgid ""
"The previously obtained lock for file $(ARG1) has expired.\n"
"This can happen due to problems on the server managing the file lock. It cannot be guaranteed that write operations on this file will not overwrite changes done by other users!"
msgstr ""
"ଫାଇଲ $(ARG1) ପାଇଁ ପୂର୍ବରୁ ଧାରଣ କରାଯାଇଥିବା ଅପରିବର୍ତ୍ତନିୟତାର ସମୟ ସମାପ୍ତ ହୋଇଛି।\n"
"ଏହା ଫାଇଲ ଅପରିବର୍ତ୍ତନିୟତାକୁ ପରିଚାଳନା କରୁଥିବା ସର୍ଭରରେ ଥିବା ସମସ୍ୟା କାରଣରୁ ହୋଇଥାଏ। ଏହା ପାଇଁ ଭରଶା ଦେଇହେବ ନାହିଁ ଯାହା ଫଳରେ ଏହି ଫାଇଲରେ ଥିବା ଲିଖନ ପ୍ରୟୋଗଗୁଡ଼ିକରେ ଅନ୍ୟ ଚାଳକମାନଙ୍କ ଦ୍ୱାରା ପରିବର୍ତ୍ତନଗୁଡ଼ିକୁ ନବଲିଖନ କରିହେବ ନାହିଁ!"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_UNKNOWNAUTH_UNTRUSTED)\n"
"string.text"
msgid ""
"Unable to verify the identity of $(ARG1) site.\n"
"\n"
"Before accepting this certificate, you should examine this site's certificate carefully. Are you willing to accept this certificate for the purpose of identifying the Web site $(ARG1)?"
msgstr ""
"$(ARG1) ସାଇଟର ପରିଚୟକୁ ଯାଞ୍ଚ କରିବାରେ ଅସମର୍ଥ।\n"
"\n"
"ଏହି ପ୍ରମାଣପତ୍ରକୁ ଗ୍ରହଣ କରିବା ପୂର୍ବରୁ, ଆପଣଙ୍କୁ ଏହି ସାଇଟର ପ୍ରମାଣପତ୍ରକୁ ଠିକ ଭାବରେ ଯାଞ୍ଚ କରିବା ଆବଶ୍ୟକ। ୱେବ ସାଇଟ $(ARG1) ର ପରିଚୟକୁ ଜାଣିବା ପାଇଁ ଆପଣ ଏହାକୁ ବ୍ୟବହାର କରିବକୁ ଚାହୁଁଛନ୍ତି କି?"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_SSLWARN_EXPIRED_1)\n"
"string.text"
msgid ""
"$(ARG1) is a site that uses a security certificate to encrypt data during transmission, but its certificate expired on $(ARG2).\n"
"\n"
"You should check to make sure that your computer's time is correct."
msgstr ""
"$(ARG1) ସାଇଟଟି ତଥ୍ୟ ପରିବହନ ସମୟରେ ସୁରକ୍ଷା ପ୍ରମାଣପତ୍ର ବ୍ୟବହାର କରୁଅଛି, କିନ୍ତୁ ଏହାର ପ୍ରମାଣପତ୍ର $(ARG2)ରେ ସମାପ୍ତ ହୋଇଯିବ।\n"
"\n"
"ଆପଣଙ୍କର କମ୍ପୁଟରର ସମୟ ଠିକ ଅଛି କି ନାହିଁ ଯାଞ୍ଚ କରନ୍ତୁ।"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_SSLWARN_DOMAINMISMATCH_1)\n"
"string.text"
msgid ""
"You have attempted to establish a connection with $(ARG1). However, the security certificate presented belongs to $(ARG2). It is possible, though unlikely, that someone may be trying to intercept your communication with this web site.\n"
"\n"
"If you suspect the certificate shown does not belong to $(ARG1), please cancel the connection and notify the site administrator.\n"
"\n"
"Would you like to continue anyway?"
msgstr ""
"$(ARG1) ସହିତ ସମ୍ପର୍କ ସ୍ଥାପନ କରିବାକୁ ଚେଷ୍ଟାକରିଥିଲେ। ତଥାପି, ଉପସ୍ଥାପିତ ସୁରକ୍ଷା ପ୍ରମାଣପତ୍ର $(ARG2)ର ଅନ୍ତର୍ଗତ। ଏହା ସମ୍ଭବ, ଯଦିଚ, କେହିଜଣେ ଏହି ୱେବସାଇଟ ସହିତ ଆପଣଙ୍କର ସମ୍ପର୍କ ସ୍ଥାପନରେ ବ୍ୟାହାତ ସୃଷ୍ଟିକରିପାରେ।\n"
"\n"
"ଯଦି ଆପଣ ଦର୍ଶାଯାଇଥିବା ପ୍ରମାଣପତ୍ରକୁ $(ARG1)ର ନୁହଁ ବୋଲି ସନ୍ଦେହ କରୁଛନ୍ତି, ତେବେ ଦୟାକରି ସଂଯୋଗକୁ ବାତିଲ କରନ୍ତୁ ଏବଂ ପ୍ରଶାସକଙ୍କ ସହିତ ଯୋଗାଯୋଗ କରନ୍ତୁ।\n"
"\n"
"ଆପଣ ଯେକୌଣସି ଉପାୟରେ ଅଗ୍ରସର ହେବାକୁ ଚାହୁଁଛନ୍ତି କି?"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_SSLWARN_INVALID_1)\n"
"string.text"
msgid ""
"The certificate could not be validated. You should examine this site's certificate carefully.\n"
"\n"
"If you suspect the certificate shown, please cancel the connection and notify the site administrator."
msgstr "ଏବଂ."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(TITLE_UUI_SSLWARN_DOMAINMISMATCH)\n"
"string.text"
msgid "Security Warning: Domain Name Mismatch"
msgstr "ସୁରକ୍ଷା ଚେତାବନୀ: ଡମେନ ନାମ ମେଳଖାଉନାହିଁ"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(TITLE_UUI_SSLWARN_EXPIRED)\n"
"string.text"
msgid "Security Warning: Server Certificate Expired"
msgstr "ସୁରକ୍ଷା ଚେତାବନୀ: ସର୍ଭର ପ୍ରମାଣ ପତ୍ର ସମାପ୍ତ ହୋଇଛି"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(TITLE_UUI_SSLWARN_INVALID)\n"
"string.text"
msgid "Security Warning: Server Certificate Invalid"
msgstr "ସୁରକ୍ଷା ଚେତାବନୀ: ସର୍ଭର ପ୍ରମାଣପତ୍ରଟି ଅବୈଧ"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_CANNOT_ACTIVATE_FACTORY & ERRCODE_RES_MASK)\n"
"string.text"
msgid ""
"Component cannot be loaded, possibly broken or incomplete installation.\n"
"Full error message:\n"
"\n"
" $(ARG1)."
msgstr ""
"ଏହି ଉପାଦାନକୁ ଧାରଣ କରିହେବ ନାହିଁ, ସମ୍ଭବତଃ ଭାଙ୍ଗିଥାଇପାରେ ଅଥବା ସ୍ଥାପନ ସମ୍ପୂର୍ଣ୍ଣ ହୋଇନାହିଁ।\n"
"ସମ୍ପୂର୍ଣ୍ଣ ତ୍ରୁଟି ସନ୍ଦେଶ:\n"
"\n"
" $(ARG1)."

#: lockfailed.src
msgctxt ""
"lockfailed.src\n"
"STR_LOCKFAILED_TITLE\n"
"string.text"
msgid "Document Could Not Be Locked"
msgstr ""

#: lockfailed.src
msgctxt ""
"lockfailed.src\n"
"STR_LOCKFAILED_MSG\n"
"string.text"
msgid "The file could not be locked for exclusive access by %PRODUCTNAME, due to missing permission to create a lock file on that file location."
msgstr "ଅନୁପସ୍ଥିତ ଅନୁମତି ହେତୁ ଫାଇଲ ସ୍ଥାନରେ ନିର୍ମାଣକୁ ଅପରିବର୍ତ୍ତନୀୟ କରିବା ପାଇଁ, %PRODUCTNAME ଦ୍ୱାରା ଫାଇଲଟି ଅପରିବର୍ତ୍ତିତ ହୋଇନାହିଁ।"

#: lockfailed.src
msgctxt ""
"lockfailed.src\n"
"STR_LOCKFAILED_DONTSHOWAGAIN\n"
"string.text"
msgid "~Do not show this message again"
msgstr ""

#: nameclashdlg.src
msgctxt ""
"nameclashdlg.src\n"
"STR_RENAME_OR_REPLACE\n"
"string.text"
msgid ""
"A file with the name \"%NAME\" already exists in the location \"%FOLDER\".\n"
"Choose Replace to overwrite the existing file or provide a new name."
msgstr ""

#: nameclashdlg.src
msgctxt ""
"nameclashdlg.src\n"
"STR_NAME_CLASH_RENAME_ONLY\n"
"string.text"
msgid ""
"A file with the name \"%NAME\" already exists in the location \"%FOLDER\".\n"
"Please enter a new name."
msgstr ""

#: nameclashdlg.src
msgctxt ""
"nameclashdlg.src\n"
"STR_SAME_NAME_USED\n"
"string.text"
msgid "Please provide a different file name!"
msgstr ""

#: openlocked.src
msgctxt ""
"openlocked.src\n"
"STR_OPENLOCKED_TITLE\n"
"string.text"
msgid "Document in Use"
msgstr "ବ୍ୟବହାର ହେଉଥିବା ଦଲିଲ"

#: openlocked.src
msgctxt ""
"openlocked.src\n"
"STR_OPENLOCKED_MSG\n"
"string.text"
msgid ""
"Document file '$(ARG1)' is locked for editing by:\n"
"\n"
"$(ARG2)\n"
"\n"
"Open document read-only or open a copy of the document for editing.\n"
"\n"
msgstr ""
"ଦଲିଲ ଫାଇଲ '$(ARG1)' ଟି $(ARG2) ଦ୍ୱାରା ସମ୍ପାଦନ କରିବା ପାଇଁ:\n"
"\n"
"ଅପରିବର୍ତ୍ତିତ ରଖାଯାଇଛି।\n"
"\n"
"କେବଳ ପଠନୀୟ ଦଲିଲକୁ ଖୋଲନ୍ତୁ କିମ୍ବା ସମ୍ପାଦନ କରିବା ପାଇଁ ନକଲ କରନ୍ତୁ।\n"
"\n"

#: openlocked.src
msgctxt ""
"openlocked.src\n"
"STR_OPENLOCKED_OPENREADONLY_BTN\n"
"string.text"
msgid "Open ~Read-Only"
msgstr "କେବଳ-ପଠନୀୟକୁ ଖୋଲନ୍ତୁ (~R)"

#: openlocked.src
msgctxt ""
"openlocked.src\n"
"STR_OPENLOCKED_OPENCOPY_BTN\n"
"string.text"
msgid "Open ~Copy"
msgstr "ପ୍ରତିଲିପିକୁ ଖୋଲନ୍ତୁ (~C)"

#: openlocked.src
msgctxt ""
"openlocked.src\n"
"STR_UNKNOWNUSER\n"
"string.text"
msgid "Unknown User"
msgstr "ଅଜ୍ଞାତ ଚାଳକ"

#: passworddlg.src
msgctxt ""
"passworddlg.src\n"
"STR_ENTER_PASSWORD_TO_OPEN\n"
"string.text"
msgid "Enter password to open file: \n"
msgstr "ଫାଇଲକୁ ଖୋଲିବା ପାଇଁ ପ୍ରବେଶ ସଂକେତ ଭରଣ କରନ୍ତୁ: \n"

#: passworddlg.src
msgctxt ""
"passworddlg.src\n"
"STR_ENTER_PASSWORD_TO_MODIFY\n"
"string.text"
msgid "Enter password to modify file: \n"
msgstr "ଫାଇଲକୁ ପରିବର୍ତ୍ତନ କରିବା ପାଇଁ ପ୍ରବେଶ ସଂକେତ ଭରଣ କରନ୍ତୁ: \n"

#: passworddlg.src
msgctxt ""
"passworddlg.src\n"
"STR_ENTER_SIMPLE_PASSWORD\n"
"string.text"
msgid "Enter password: "
msgstr "ପ୍ରବେଶ ସଂକେତ ଭରଣ କରନ୍ତୁ: "

#: passworddlg.src
msgctxt ""
"passworddlg.src\n"
"STR_CONFIRM_SIMPLE_PASSWORD\n"
"string.text"
msgid "Confirm password: "
msgstr "ପ୍ରବେଶ ସଂକେତ ନିଶ୍ଚିତ କରନ୍ତୁ"

#: passworddlg.src
msgctxt ""
"passworddlg.src\n"
"STR_TITLE_CREATE_PASSWORD\n"
"string.text"
msgid "Set Password"
msgstr "ପ୍ରବେଶ ସଂକେତ ସେଟ କରନ୍ତୁ"

#: passworddlg.src
msgctxt ""
"passworddlg.src\n"
"STR_TITLE_ENTER_PASSWORD\n"
"string.text"
msgid "Enter Password"
msgstr "ପ୍ରବେଶ ସଙ୍କେତ ଭରଣକରନ୍ତୁ"

#: passworddlg.src
msgctxt ""
"passworddlg.src\n"
"STR_PASSWORD_MISMATCH\n"
"string.text"
msgid "The confirmation password did not match the password. Set the password again by entering the same password in both boxes."
msgstr "ନିଶ୍ଚିତକରଣ ପ୍ରବେଶ ସଂକେତ ମୂଳ ପ୍ରବେଶ ସଂକେତ ସହିତ ସମାନ ନୁହଁ। ସମାନ ପ୍ରବେଶ ସଂକେତକୁ ଉଭୟ ବାକ୍ସରେ ଭରଣ କରି ପୁଣିଥରେ ସେଟ କରନ୍ତୁ।"

#: passworderrs.src
msgctxt ""
"passworderrs.src\n"
"STR_ERROR_PASSWORD_TO_OPEN_WRONG\n"
"string.text"
msgid "The password is incorrect. The file cannot be opened."
msgstr "ଏହି ଗୁପ୍ତଶବ୍ଦ ଭୂଲ ଅଛି। ଏହି ଫାଇଲକୁ ଖୋଲି ପାରିବେ ନାହିଁ।"

#: passworderrs.src
msgctxt ""
"passworderrs.src\n"
"STR_ERROR_PASSWORD_TO_MODIFY_WRONG\n"
"string.text"
msgid "The password is incorrect. The file cannot be modified."
msgstr "ଏହି ଗୁପ୍ତଶବ୍ଦ ଭୂଲ ଅଛି। ଏହି ଫାଇଲକୁ ପରିବର୍ତ୍ତନ କରି ପାରିବେ ନାହିଁ।"

#: passworderrs.src
msgctxt ""
"passworderrs.src\n"
"STR_ERROR_MASTERPASSWORD_WRONG\n"
"string.text"
msgid "The master password is incorrect."
msgstr "ମୂଖ୍ୟ ପ୍ରବେଶ ସଂକେତଟି ଭୁଲ ଅଟେ।"

#: passworderrs.src
msgctxt ""
"passworderrs.src\n"
"STR_ERROR_SIMPLE_PASSWORD_WRONG\n"
"string.text"
msgid "The password is incorrect."
msgstr "ପ୍ରବେଶ ସଂକେତଟି ଭୁଲ ଅଟେ।"

#: passworderrs.src
msgctxt ""
"passworderrs.src\n"
"STR_ERROR_PASSWORDS_NOT_IDENTICAL\n"
"string.text"
msgid "The password confirmation does not match."
msgstr "ପ୍ରବେଶ ସଂକେତ ନିଶ୍ଚିତକରଣ ମେଳ ଖାଉନାହିଁ।"

#: trylater.src
msgctxt ""
"trylater.src\n"
"STR_TRYLATER_TITLE\n"
"string.text"
msgid "Document in Use"
msgstr "ବ୍ୟବହାର ହେଉଥିବା ଦଲିଲ"

#: trylater.src
msgctxt ""
"trylater.src\n"
"STR_TRYLATER_MSG\n"
"string.text"
msgid ""
"Document file '$(ARG1)' is locked for editing by:\n"
"\n"
"$(ARG2)\n"
"\n"
"Try again later to save document or save a copy of that document.\n"
"\n"
msgstr ""
"ଦଲିଲ ଫାଇଲ '$(ARG1)'କୁ $(ARG2) ଦ୍ୱାରା ସମ୍ପାଦନ କରିବା ପାଇଁ:\n"
"\n"
"ଅପରିବର୍ତ୍ତିତ ରଖାଯାଇଛି\n"
"\n"
"ପରେ ପୁଣିଥରେ ଚେଷ୍ଟା କରନ୍ତୁ କିମ୍ବା ସେହି ଦଲିଲର ଗୋଟିଏ ନକଲକୁ ସଂରକ୍ଷଣ କରନ୍ତୁ।\n"
"\n"

#: trylater.src
msgctxt ""
"trylater.src\n"
"STR_TRYLATER_RETRYSAVING_BTN\n"
"string.text"
msgid "~Retry Saving"
msgstr "ସଂରକ୍ଷଣକୁ ପୁଣିଚେଷ୍ଟା କରନ୍ତୁ (~R)"

#: trylater.src
msgctxt ""
"trylater.src\n"
"STR_TRYLATER_SAVEAS_BTN\n"
"string.text"
msgid "~Save As..."
msgstr "ଏହି ପରି ସଞ୍ଚୟ କରନ୍ତୁ (~S)"

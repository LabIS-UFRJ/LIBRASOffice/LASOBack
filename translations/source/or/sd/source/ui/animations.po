#. extracted from sd/source/ui/animations
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-05-23 22:01+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: Oriya <oriya-it@googlegroups.com>\n"
"Language: or\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1464040892.000000\n"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_EFFECT_CONTEXTMENU\n"
"CM_WITH_CLICK\n"
"menuitem.text"
msgid "Start On ~Click"
msgstr "ଆରମ୍ଭକୁ କ୍ଲିକ୍ କର (~C)"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_EFFECT_CONTEXTMENU\n"
"CM_WITH_PREVIOUS\n"
"menuitem.text"
msgid "Start ~With Previous"
msgstr "ପୂର୍ବବର୍ତ୍ତୀ ସହିତ ଆରମ୍ଭ କର"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_EFFECT_CONTEXTMENU\n"
"CM_AFTER_PREVIOUS\n"
"menuitem.text"
msgid "Start ~After Previous"
msgstr "ପରବର୍ତ୍ତୀ ସହିତ ଆରମ୍ଭକର"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_EFFECT_CONTEXTMENU\n"
"CM_OPTIONS\n"
"menuitem.text"
msgid "~Effect Options..."
msgstr "ବିକଳ୍ପଗୁଡିକର ପ୍ରଭାବ..."

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_EFFECT_CONTEXTMENU\n"
"CM_DURATION\n"
"menuitem.text"
msgid "~Timing..."
msgstr "ସମଯ..."

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_EFFECT_CONTEXTMENU\n"
"CM_REMOVE\n"
"menuitem.text"
msgid "~Remove"
msgstr "ହଟାଅ"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_CUSTOMANIMATION_FONTSIZE_POPUP\n"
"CM_SIZE_25\n"
"menuitem.text"
msgid "Tiny"
msgstr "ଟିନି"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_CUSTOMANIMATION_FONTSIZE_POPUP\n"
"CM_SIZE_50\n"
"menuitem.text"
msgid "Smaller"
msgstr "କ୍ଷୁଦ୍ରତମ"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_CUSTOMANIMATION_FONTSIZE_POPUP\n"
"CM_SIZE_150\n"
"menuitem.text"
msgid "Larger"
msgstr "ବୃହତ୍ତମ"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_CUSTOMANIMATION_FONTSIZE_POPUP\n"
"CM_SIZE_400\n"
"menuitem.text"
msgid "Extra large"
msgstr "ଅଧିକ ବଡ"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_CUSTOMANIMATION_SCALE_POPUP\n"
"CM_SIZE_25\n"
"menuitem.text"
msgid "Tiny"
msgstr "ଟିନି"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_CUSTOMANIMATION_SCALE_POPUP\n"
"CM_SIZE_50\n"
"menuitem.text"
msgid "Smaller"
msgstr "କ୍ଷୁଦ୍ରତମ"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_CUSTOMANIMATION_SCALE_POPUP\n"
"CM_SIZE_150\n"
"menuitem.text"
msgid "Larger"
msgstr "ବୃହତ୍ତମ"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_CUSTOMANIMATION_SCALE_POPUP\n"
"CM_SIZE_400\n"
"menuitem.text"
msgid "Extra large"
msgstr "ଅଧିକ ବଡ"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_CUSTOMANIMATION_SCALE_POPUP\n"
"CM_HORIZONTAL\n"
"menuitem.text"
msgid "Horizontal"
msgstr "ଦିଗବଳୀଯ"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_CUSTOMANIMATION_SCALE_POPUP\n"
"CM_VERTICAL\n"
"menuitem.text"
msgid "Vertical"
msgstr "ଲମ୍ବରୂପ"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_CUSTOMANIMATION_SCALE_POPUP\n"
"CM_BOTH\n"
"menuitem.text"
msgid "Both"
msgstr "ଉଭଯ"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_CUSTOMANIMATION_ROTATION_POPUP\n"
"CM_QUARTER_SPIN\n"
"menuitem.text"
msgid "Quarter spin"
msgstr "କ୍ବାଟର୍ ସ୍ପିନ୍"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_CUSTOMANIMATION_ROTATION_POPUP\n"
"CM_HALF_SPIN\n"
"menuitem.text"
msgid "Half spin"
msgstr "ଅର୍ଦ୍ଧ ସ୍ପିନ୍"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_CUSTOMANIMATION_ROTATION_POPUP\n"
"CM_FULL_SPIN\n"
"menuitem.text"
msgid "Full spin"
msgstr "ସଂପୂର୍ଣ୍ଣ ସ୍ପିନ୍"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_CUSTOMANIMATION_ROTATION_POPUP\n"
"CM_TWO_SPINS\n"
"menuitem.text"
msgid "Two spins"
msgstr "ଦୁଇଟି ସ୍ପିନ୍"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_CUSTOMANIMATION_ROTATION_POPUP\n"
"CM_CLOCKWISE\n"
"menuitem.text"
msgid "Clockwise"
msgstr "ଦକ୍ଷିଣାବର୍ତ୍ତୀ"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_CUSTOMANIMATION_ROTATION_POPUP\n"
"CM_COUNTERCLOCKWISE\n"
"menuitem.text"
msgid "Counter-clockwise"
msgstr "କାଉଣ୍ଟର୍-ଦକ୍ଷିଣାବର୍ତ୍ତୀ"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_CUSTOMANIMATION_FONTSTYLE_POPUP\n"
"CM_BOLD\n"
"menuitem.text"
msgid "Bold"
msgstr "ମୋଟା"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_CUSTOMANIMATION_FONTSTYLE_POPUP\n"
"CM_ITALIC\n"
"menuitem.text"
msgid "Italic"
msgstr "ତେର୍ଚ୍ଛା"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"RID_CUSTOMANIMATION_FONTSTYLE_POPUP\n"
"CM_UNDERLINED\n"
"menuitem.text"
msgid "Underlined"
msgstr "ରେଖାଙ୍କିତ"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_REPEAT_NONE\n"
"string.text"
msgid "none"
msgstr "ନାହିଁ"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_REPEAT_UNTIL_NEXT_CLICK\n"
"string.text"
msgid "Until next click"
msgstr "ପରବର୍ତ୍ତୀ କ୍ଲିକ୍କରିବା ପର୍ୟ୍ଯନ୍ତ"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_REPEAT_UNTIL_END_OF_SLIDE\n"
"string.text"
msgid "Until end of slide"
msgstr "ସ୍ଲାଇଡର ଶେଷ ପର୍ୟ୍ଯନ୍ତ"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_DIRECTION_PROPERTY\n"
"string.text"
msgid "Direction:"
msgstr ""

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_ZOOM_PROPERTY\n"
"string.text"
msgid "Zoom:"
msgstr ""

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_SPOKES_PROPERTY\n"
"string.text"
msgid "Spokes:"
msgstr ""

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_FIRST_COLOR_PROPERTY\n"
"string.text"
msgid "First color:"
msgstr ""

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_SECOND_COLOR_PROPERTY\n"
"string.text"
msgid "Second color:"
msgstr ""

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_FILL_COLOR_PROPERTY\n"
"string.text"
msgid "Fill color:"
msgstr ""

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_STYLE_PROPERTY\n"
"string.text"
msgid "Style:"
msgstr ""

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_FONT_PROPERTY\n"
"string.text"
msgid "Font:"
msgstr ""

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_FONT_COLOR_PROPERTY\n"
"string.text"
msgid "Font color:"
msgstr ""

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_FONT_SIZE_STYLE_PROPERTY\n"
"string.text"
msgid "Style:"
msgstr ""

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_FONT_STYLE_PROPERTY\n"
"string.text"
msgid "Typeface:"
msgstr ""

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_LINE_COLOR_PROPERTY\n"
"string.text"
msgid "Line color:"
msgstr ""

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_SIZE_PROPERTY\n"
"string.text"
msgid "Font size:"
msgstr ""

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_SCALE_PROPERTY\n"
"string.text"
msgid "Size:"
msgstr ""

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_AMOUNT_PROPERTY\n"
"string.text"
msgid "Amount:"
msgstr ""

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_COLOR_PROPERTY\n"
"string.text"
msgid "Color:"
msgstr ""

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_NO_SOUND\n"
"string.text"
msgid "(No sound)"
msgstr "(କୌଣସି ଧ୍ବନି ନାହିଁ)"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_STOP_PREVIOUS_SOUND\n"
"string.text"
msgid "(Stop previous sound)"
msgstr "(ପୂର୍ବବର୍ତ୍ତୀ ଧ୍ବନିକୁ ବନ୍ଦ କର)"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_BROWSE_SOUND\n"
"string.text"
msgid "Other sound..."
msgstr "ଅନ୍ଯ ଧ୍ବନି"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_SAMPLE\n"
"string.text"
msgid "Sample"
msgstr "ନମୁନା"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_1_SPOKES\n"
"string.text"
msgid "1 Spoke"
msgstr "୧ ସ୍ପୋକ"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_2_SPOKES\n"
"string.text"
msgid "2 Spokes"
msgstr "୨ ସ୍ପୋକ୍ଗୁଡିକ"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_3_SPOKES\n"
"string.text"
msgid "3 Spokes"
msgstr "୩ ସ୍ପୋକ୍ଗୁଡିକ"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_4_SPOKES\n"
"string.text"
msgid "4 Spokes"
msgstr "୪ ସ୍ପୋକ୍ଗୁଡିକ"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_8_SPOKES\n"
"string.text"
msgid "8 Spokes"
msgstr "୮ ସ୍ପୋକ୍ଗୁଡିକ"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_INSTANT\n"
"string.text"
msgid "Instant"
msgstr "ଜ୍ବଳନ୍ତ"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_GRADUAL\n"
"string.text"
msgid "Gradual"
msgstr "କ୍ରମିକ"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_TRIGGER\n"
"string.text"
msgid "Trigger"
msgstr "ଟ୍ରିଗର"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_LIST_HELPTEXT\n"
"string.text"
msgid "First select the slide element and then click 'Add...' to add an animation effect."
msgstr "ପ୍ରଥମେ ସ୍ଲାଇଡ ଅଶଂକୁ ମନୋନୀତକର ଏବଂ ପରେ ଏକ ଜୀବନ୍ତଚିତ୍ରର ପ୍ରଭାବକୁ ମିଶାଇବା ପାଇଁ  'Add...'କୁ କ୍ଲିକ କର୤"

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_USERPATH\n"
"string.text"
msgid "User paths"
msgstr ""

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_ENTRANCE\n"
"string.text"
msgid "Entrance: %1"
msgstr ""

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_EMPHASIS\n"
"string.text"
msgid "Emphasis: %1"
msgstr ""

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_EXIT\n"
"string.text"
msgid "Exit: %1"
msgstr ""

#: CustomAnimation.src
msgctxt ""
"CustomAnimation.src\n"
"STR_CUSTOMANIMATION_MOTION_PATHS\n"
"string.text"
msgid "Motion Paths: %1"
msgstr ""

#: CustomAnimation.src
#, fuzzy
msgctxt ""
"CustomAnimation.src\n"
"STR_SLIDETRANSITION_NONE\n"
"string.text"
msgid "None"
msgstr "ନାହିଁ"

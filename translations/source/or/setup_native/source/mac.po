#. extracted from setup_native/source/mac
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.freedesktop.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-05-23 12:05+0200\n"
"PO-Revision-Date: 2011-04-06 01:27+0200\n"
"Last-Translator: mgiri <mgiri@redhat.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: or\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"

#: macinstall.ulf
msgctxt ""
"macinstall.ulf\n"
"OKLabel\n"
"LngText.text"
msgid "Ok"
msgstr "ଠିକ ଅଛି"

#: macinstall.ulf
msgctxt ""
"macinstall.ulf\n"
"InstallLabel\n"
"LngText.text"
msgid "Install"
msgstr "ସ୍ଥାପନ କରନ୍ତୁ"

#: macinstall.ulf
msgctxt ""
"macinstall.ulf\n"
"AbortLabel\n"
"LngText.text"
msgid "Abort"
msgstr "ନିସ୍ଫଳରୂପେ"

#: macinstall.ulf
msgctxt ""
"macinstall.ulf\n"
"IntroText1\n"
"LngText.text"
msgid "Welcome to the [FULLPRODUCTNAME] Installation Wizard"
msgstr "[FULLPRODUCTNAME] ସ୍ଥାପନା ୱିଜାର୍ଡରେ ଆପଣଙ୍କୁ ସ୍ୱାଗତ କରୁଅଛି"

#: macinstall.ulf
msgctxt ""
"macinstall.ulf\n"
"IntroText2\n"
"LngText.text"
msgid "This installation will update your installed versions of [PRODUCTNAME]"
msgstr "ଏହି ସ୍ଥାପନାଟି ଆପଣଙ୍କର [PRODUCTNAME] ର ସ୍ଥାପିତ ସଂସ୍କରଣଗୁଡ଼ିକୁ ଅଦ୍ୟତନ କରିବ"

#: macinstall.ulf
msgctxt ""
"macinstall.ulf\n"
"IntroText3\n"
"LngText.text"
msgid "This might take a moment."
msgstr "ଏହା କିଛି ସମୟ ନେଇପାରେ।"

#: macinstall.ulf
msgctxt ""
"macinstall.ulf\n"
"ChooseMyOwnText\n"
"LngText.text"
msgid "Not listed (choose location in an extra step)"
msgstr "ତାଲିକାଭୁକ୍ତ ନୁହଁ (ଗୋଟିଏ ଅତିରିକ୍ତ ପଦକ୍ଷେପରେ ଅବସ୍ଥାନ ବାଛନ୍ତୁ)"

#: macinstall.ulf
msgctxt ""
"macinstall.ulf\n"
"ListPromptText\n"
"LngText.text"
msgid "Choose [PRODUCTNAME] [PRODUCTVERSION] installation for which you want to install the [FULLPRODUCTNAME]"
msgstr "[PRODUCTNAME] [PRODUCTVERSION] ସ୍ଥାପନାକୁ ବାଛନ୍ତୁ ଯାହା ପାଇଁ ଆପଣ [FULLPRODUCTNAME] କୁ ସ୍ଥାପନ କରିବାକୁ ଚାହୁଁଛନ୍ତି"

#: macinstall.ulf
msgctxt ""
"macinstall.ulf\n"
"ChooseManualText\n"
"LngText.text"
msgid "Point the dialog to your [PRODUCTNAME] [PRODUCTVERSION] installation."
msgstr "ଆପଣଙ୍କର [PRODUCTNAME] [PRODUCTVERSION] ସ୍ଥାପନାରେ ସଂଳାପକୁ ସୂଚାନ୍ତୁ।"

#: macinstall.ulf
msgctxt ""
"macinstall.ulf\n"
"ListOKLabelText\n"
"LngText.text"
msgid "Install"
msgstr "ସ୍ଥାପନ କରନ୍ତୁ"

#: macinstall.ulf
msgctxt ""
"macinstall.ulf\n"
"ListCancelLabel\n"
"LngText.text"
msgid "Abort"
msgstr "ନିସ୍ଫଳରୂପେ"

#: macinstall.ulf
msgctxt ""
"macinstall.ulf\n"
"AppInvalidText1\n"
"LngText.text"
msgid "This is not a valid [PRODUCTNAME] [PRODUCTVERSION] installation."
msgstr "ଏହା ଗୋଟିଏ ବୈଧ [PRODUCTNAME] [PRODUCTVERSION] ସ୍ଥାପନା ନୁହଁ।"

#: macinstall.ulf
msgctxt ""
"macinstall.ulf\n"
"AppInvalidText2\n"
"LngText.text"
msgid "Run the installer again and choose a valid [PRODUCTNAME] [PRODUCTVERSION] installation"
msgstr "ସ୍ଥାପକକୁ ପୁଣିଥରେ ଚଲାନ୍ତୁ ଏବଂ ଗୋଟିଏ ବୈଧ [PRODUCTNAME] [PRODUCTVERSION] ସ୍ଥାପନା ବାଛନ୍ତୁ"

#: macinstall.ulf
msgctxt ""
"macinstall.ulf\n"
"StartInstallText1\n"
"LngText.text"
msgid "Click Install to start the installation"
msgstr "ସ୍ଥାପନ କ୍ରିୟା ଆରମ୍ଭ କରିବା ପାଇଁ ସ୍ଥାପନ କରନ୍ତୁକୁ କ୍ଲିକ କରନ୍ତୁ"

#: macinstall.ulf
msgctxt ""
"macinstall.ulf\n"
"StartInstallText2\n"
"LngText.text"
msgid "Installation might take a minute..."
msgstr "ସ୍ଥାପନ କ୍ରିୟା କିଛି ମିନଟ ସମୟ ନେଇପାରେ..."

#: macinstall.ulf
msgctxt ""
"macinstall.ulf\n"
"IdentifyQText\n"
"LngText.text"
msgid "Installation failed, most likely your account does not have the necessary privileges."
msgstr "ସ୍ଥାପନା ବିଫଳ ହୋଇଛି, ଅଧିକାଂଶ ଭାବରେ ଆପଣଙ୍କ ଖାତାରେ ଆବଶ୍ୟକୀୟ ପ୍ରାଧିକାର ନାହିଁ।"

#: macinstall.ulf
msgctxt ""
"macinstall.ulf\n"
"IdentifyQText2\n"
"LngText.text"
msgid "Do you want to identify as administrator and try again?"
msgstr "ଆପଣ ପ୍ରଶାସକ ଭାବରେ ପରିଚିତ ହୋଇ ପୁଣିଥରେ ଚେଷ୍ଟା କରିବାକୁ ଚାହୁଁଛନ୍ତି କି?"

#: macinstall.ulf
msgctxt ""
"macinstall.ulf\n"
"IdentifyYES\n"
"LngText.text"
msgid "Yes, identify"
msgstr "ହଁ, ଚିହ୍ନନ୍ତୁ"

#: macinstall.ulf
msgctxt ""
"macinstall.ulf\n"
"IdentifyNO\n"
"LngText.text"
msgid "No, abort installation"
msgstr "ନାଁ, ସ୍ଥାପନା ପରିତ୍ୟାଗ କରନ୍ତୁ"

#: macinstall.ulf
msgctxt ""
"macinstall.ulf\n"
"InstallFailedText\n"
"LngText.text"
msgid "Installation failed."
msgstr "ସ୍ଥାପନ କ୍ରିୟା ବିଫଳ ହୋଇଛି।"

#: macinstall.ulf
msgctxt ""
"macinstall.ulf\n"
"InstallCompleteText\n"
"LngText.text"
msgid "Installation of [PRODUCTNAME] language pack completed."
msgstr "[PRODUCTNAME] ଭାଷା ପ୍ୟାକର ସ୍ଥାପନା ସମ୍ପୂର୍ଣ୍ଣ ହୋଇଛି।"

#: macinstall.ulf
msgctxt ""
"macinstall.ulf\n"
"InstallCompleteText2\n"
"LngText.text"
msgid "Call '[PRODUCTNAME]-Preferences-Language Settings-Languages' to change the user interface language."
msgstr "ଚାଳକ ଅନ୍ତରାପୃଷ୍ଠ ଭାଷାକୁ ପରିବର୍ତ୍ତନ କରିବା ପାଇଁ '[PRODUCTNAME]-Preferences-Language Settings-Languages' କୁ ଯାଆନ୍ତୁ।"

#: macinstall.ulf
msgctxt ""
"macinstall.ulf\n"
"InstallCompleteTextPatch\n"
"LngText.text"
msgid "Installation of [FULLPRODUCTNAME] completed"
msgstr "[FULLPRODUCTNAME] ର ସ୍ଥାପନା ସମ୍ପୂର୍ଣ୍ଣ ହୋଇଛି"

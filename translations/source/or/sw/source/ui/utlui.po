#. extracted from sw/source/ui/utlui
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-22 23:17+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_FOOTNOTE\n"
"string.text"
msgid "Footnote Characters"
msgstr "ପାଦଟିପପ୍ଣୀ ଅକ୍ଷରଗୁଡିକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_PAGENO\n"
"string.text"
msgid "Page Number"
msgstr "ପୃଷ୍ଠା ସଂଖ୍ଯା"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_LABEL\n"
"string.text"
msgid "Caption Characters"
msgstr "ଶିରୋନାମା ଅକ୍ଷରଗୁଡିକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_DROPCAPS\n"
"string.text"
msgid "Drop Caps"
msgstr "ଡ୍ରପସ କ୍ଯାପସ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_NUM_LEVEL\n"
"string.text"
msgid "Numbering Symbols"
msgstr "ସଂଖ୍ଯାଦେବା ସଂଦେଶଗୁଡିକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_BUL_LEVEL\n"
"string.text"
msgid "Bullets"
msgstr "ଗୁଳିଗୁଡିକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_INET_NORMAL\n"
"string.text"
msgid "Internet Link"
msgstr "ଇଣ୍ଟରନେଟ ଲିଙ୍କ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_INET_VISIT\n"
"string.text"
msgid "Visited Internet Link"
msgstr "ପରିଦର୍ଶିତ ଇଣ୍ଟରନେଟ ଲିଙ୍କ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_JUMPEDIT\n"
"string.text"
msgid "Placeholder"
msgstr "ସ୍ଥାନଧାରକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_TOXJUMP\n"
"string.text"
msgid "Index Link"
msgstr "ସୂଚୀ ଲିଙ୍କ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_ENDNOTE\n"
"string.text"
msgid "Endnote Characters"
msgstr "ଶେଷଟିପପ୍ଣୀ ଅକ୍ଷରଗୁଡିକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_LANDSCAPE\n"
"string.text"
msgid "Landscape"
msgstr "କଡ଼ୁଆ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_LINENUM\n"
"string.text"
msgid "Line Numbering"
msgstr "ଧାଡ଼ି ସଂଖ୍ଯାକରଣ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_IDX_MAIN_ENTRY\n"
"string.text"
msgid "Main Index Entry"
msgstr "ମୂଖ୍ଯ ସୂଚୀ ପ୍ରବେଶ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_FOOTNOTE_ANCHOR\n"
"string.text"
msgid "Footnote Anchor"
msgstr "ପାଦଟିକା ଏଙ୍କର"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_ENDNOTE_ANCHOR\n"
"string.text"
msgid "Endnote Anchor"
msgstr "ଶେଷଟିପା ଏଙ୍କର"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_EMPHASIS\n"
"string.text"
msgid "Emphasis"
msgstr "ଗୁରୁତ୍ବ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_CITIATION\n"
"string.text"
msgid "Quotation"
msgstr "ଉଦ୍ଧୃତାଶଂ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_STRONG\n"
"string.text"
msgid "Strong Emphasis"
msgstr "ବଳିଷ୍ଠ ଗୁରୁତ୍ବ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_CODE\n"
"string.text"
msgid "Source Text"
msgstr "ମୂଳ ଟେକ୍ସଟ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_SAMPLE\n"
"string.text"
msgid "Example"
msgstr "ଉଦାହରଣ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_KEYBOARD\n"
"string.text"
msgid "User Entry"
msgstr "ଉପୟୋଗକର୍ତ୍ତା ପ୍ରବେଶ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_VARIABLE\n"
"string.text"
msgid "Variable"
msgstr "ପରିବର୍ତ୍ତନଶୀଳ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_DEFINSTANCE\n"
"string.text"
msgid "Definition"
msgstr "ସଂଜ୍ଞା"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_TELETYPE\n"
"string.text"
msgid "Teletype"
msgstr "ଟେଲିଟାଇପ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLFRM_FRAME\n"
"string.text"
msgid "Frame"
msgstr "ଫ୍ରେମ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLFRM_GRAPHIC\n"
"string.text"
msgid "Graphics"
msgstr "ଲେଖାଚିତ୍ର"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLFRM_OLE\n"
"string.text"
msgid "OLE"
msgstr "OLE"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLFRM_FORMEL\n"
"string.text"
msgid "Formula"
msgstr "ସୂତ୍ର"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLFRM_MARGINAL\n"
"string.text"
msgid "Marginalia"
msgstr "ମାର୍ଜିନାଲିଆ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLFRM_WATERSIGN\n"
"string.text"
msgid "Watermark"
msgstr "ଜଳଚିହ୍ନ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLFRM_LABEL\n"
"string.text"
msgid "Labels"
msgstr "ଲେବଲଗୁଡିକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_STANDARD\n"
"string.text"
msgid "Default Style"
msgstr "ପୂର୍ବ ନିର୍ଦ୍ଧାରିତ ଶୈଳୀ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TEXT\n"
"string.text"
msgid "Text Body"
msgstr "ପାଠ୍ୟ ସ୍ଥାନ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TEXT_IDENT\n"
"string.text"
msgid "First Line Indent"
msgstr "ପ୍ରଥମ ରେଖା ଇନଡେଣ୍ଟ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TEXT_NEGIDENT\n"
"string.text"
msgid "Hanging Indent"
msgstr "ଅଟକିଯାଇଥିବା ଅନ୍ତଃକୋଟୀ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TEXT_MOVE\n"
"string.text"
msgid "Text Body Indent"
msgstr "ଟେକ୍ସଟ ବିଭାଗ ଅନ୍ତଃକୋଟୀ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_GREETING\n"
"string.text"
msgid "Complimentary Close"
msgstr "ପରିପୂରକ ବନ୍ଦ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_SIGNATURE\n"
"string.text"
msgid "Signature"
msgstr "ହସ୍ତାକ୍ଷର"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE_BASE\n"
"string.text"
msgid "Heading"
msgstr "ଶୀର୍ଷକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUMBUL_BASE\n"
"string.text"
msgid "List"
msgstr "ତାଲିକା"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_REGISTER_BASE\n"
"string.text"
msgid "Index"
msgstr "ସୂଚୀ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_CONFRONTATION\n"
"string.text"
msgid "List Indent"
msgstr "ତାଲିକା ଇନଡେଣ୍ଟ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_MARGINAL\n"
"string.text"
msgid "Marginalia"
msgstr "ମାର୍ଜିନାଲିଆ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE1\n"
"string.text"
msgid "Heading 1"
msgstr "ଶୀର୍ଷକ ୧"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE2\n"
"string.text"
msgid "Heading 2"
msgstr "ଶୀର୍ଷକ ୨"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE3\n"
"string.text"
msgid "Heading 3"
msgstr "ଶୀର୍ଷକ ୩"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE4\n"
"string.text"
msgid "Heading 4"
msgstr "ଶୀର୍ଷକ ୪"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE5\n"
"string.text"
msgid "Heading 5"
msgstr "ଶୀର୍ଷକ ୫"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE6\n"
"string.text"
msgid "Heading 6"
msgstr "ଶୀର୍ଷକ ୬"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE7\n"
"string.text"
msgid "Heading 7"
msgstr "ଶୀର୍ଷକ ୭"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE8\n"
"string.text"
msgid "Heading 8"
msgstr "ଶୀର୍ଷକ ୮"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE9\n"
"string.text"
msgid "Heading 9"
msgstr "ଶୀର୍ଷକ ୯"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE10\n"
"string.text"
msgid "Heading 10"
msgstr "ଶୀର୍ଷକ ୧୦"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL1S\n"
"string.text"
msgid "Numbering 1 Start"
msgstr "ସଂଖ୍ଯାଦେବା ୧ ଆରମ୍ଭକର"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL1\n"
"string.text"
msgid "Numbering 1"
msgstr "ସଂଖ୍ଯାଦେବା ୧"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL1E\n"
"string.text"
msgid "Numbering 1 End"
msgstr "ସଂଖ୍ଯାଦେବା ୧ ଶେଷ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_NONUM1\n"
"string.text"
msgid "Numbering 1 Cont."
msgstr "ସଂଖ୍ଯାଦେବା ୧ ଚାଲୁଅଛି"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL2S\n"
"string.text"
msgid "Numbering 2 Start"
msgstr "ସଂଖ୍ଯାଦେବା ୨ ଆରମ୍ଭ କର"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL2\n"
"string.text"
msgid "Numbering 2"
msgstr "ସଂଖ୍ଯାଦେବା ୨"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL2E\n"
"string.text"
msgid "Numbering 2 End"
msgstr "ସଂଖ୍ଯାଦେବା ୨ ଶେଷ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_NONUM2\n"
"string.text"
msgid "Numbering 2 Cont."
msgstr "ସଂଖ୍ଯାଦେବା ୨ ଚାଲୁଅଛି"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL3S\n"
"string.text"
msgid "Numbering 3 Start"
msgstr "ସଂଖ୍ଯାଦେବା ୩ ଆରମ୍ଭ କର"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL3\n"
"string.text"
msgid "Numbering 3"
msgstr "ସଂଖ୍ଯାଦେବା ୩"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL3E\n"
"string.text"
msgid "Numbering 3 End"
msgstr "ସଂଖ୍ଯାଦେବା ୩ ଶେଷ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_NONUM3\n"
"string.text"
msgid "Numbering 3 Cont."
msgstr "ସଂଖ୍ଯାଦେବା ୩ ଚାଲୁଅଛି"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL4S\n"
"string.text"
msgid "Numbering 4 Start"
msgstr "ସଂଖ୍ଯାଦେବା ୪ ଆରମ୍ଭ କର"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL4\n"
"string.text"
msgid "Numbering 4"
msgstr "ସଂଖ୍ଯାଦେବା ୪ "

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL4E\n"
"string.text"
msgid "Numbering 4 End"
msgstr "ସଂଖ୍ଯାଦେବା ୪ ଶେଷ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_NONUM4\n"
"string.text"
msgid "Numbering 4 Cont."
msgstr "ସଂଖ୍ଯାଦେବା ୪ ଚାଲୁଅଛି"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL5S\n"
"string.text"
msgid "Numbering 5 Start"
msgstr "ସଂଖ୍ଯାଦେବା ୫ ଆରମ୍ଭ କର"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL5\n"
"string.text"
msgid "Numbering 5"
msgstr "ସଂଖ୍ଯାଦେବା ୫"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL5E\n"
"string.text"
msgid "Numbering 5 End"
msgstr "ସଂଖ୍ଯାଦେବା ୫ ଶେଷ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_NONUM5\n"
"string.text"
msgid "Numbering 5 Cont."
msgstr "ସଂଖ୍ଯାଦେବା ୫ ଚାଲୁଅଛି"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL1S\n"
"string.text"
msgid "List 1 Start"
msgstr "ତାଲିକା ୧ ଆରମ୍ଭ କର"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL1\n"
"string.text"
msgid "List 1"
msgstr "ତାଲିକା ୧"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL1E\n"
"string.text"
msgid "List 1 End"
msgstr "ତାଲିକା ୧ ଶେଷ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_NONUM1\n"
"string.text"
msgid "List 1 Cont."
msgstr "ତାଲିକା ୧ ଚାଲୁଅଛି"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL2S\n"
"string.text"
msgid "List 2 Start"
msgstr "ତାଲିକା ୨ ଆରମ୍ଭ କର"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL2\n"
"string.text"
msgid "List 2"
msgstr "ତାଲିକା ୨"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL2E\n"
"string.text"
msgid "List 2 End"
msgstr "ତାଲିକା ୨ ଶେଷ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_NONUM2\n"
"string.text"
msgid "List 2 Cont."
msgstr "ତାଲିକା ୨ ଚାଲୁଅଛି"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL3S\n"
"string.text"
msgid "List 3 Start"
msgstr "ତାଲିକା ୩ ଆରମ୍ଭ କର"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL3\n"
"string.text"
msgid "List 3"
msgstr "ତାଲିକା ୩ "

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL3E\n"
"string.text"
msgid "List 3 End"
msgstr "ତାଲିକା ୩ ଶେଷ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_NONUM3\n"
"string.text"
msgid "List 3 Cont."
msgstr "ତାଲିକା ୩ ଚାଲୁଅଛି"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL4S\n"
"string.text"
msgid "List 4 Start"
msgstr "ତାଲିକା ୪  ଆରମ୍ଭ କର"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL4\n"
"string.text"
msgid "List 4"
msgstr "ତାଲିକା ୪"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL4E\n"
"string.text"
msgid "List 4 End"
msgstr "ତାଲିକା ୪ ଶେଷ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_NONUM4\n"
"string.text"
msgid "List 4 Cont."
msgstr "ତାଲିକା ୪ ଚାଲୁଅଛି"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL5S\n"
"string.text"
msgid "List 5 Start"
msgstr "ତାଲିକା ୫  ଆରମ୍ଭ କର"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL5\n"
"string.text"
msgid "List 5"
msgstr "ତାଲିକା ୫ "

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL5E\n"
"string.text"
msgid "List 5 End"
msgstr "ତାଲିକା ୫ ଶେଷ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_NONUM5\n"
"string.text"
msgid "List 5 Cont."
msgstr "ତାଲିକା ୫ ଚାଲୁଅଛି"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADER\n"
"string.text"
msgid "Header"
msgstr "ଶୀର୍ଷଟିକା"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADERL\n"
"string.text"
msgid "Header Left"
msgstr "ବାମ ଶୀର୍ଷକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADERR\n"
"string.text"
msgid "Header Right"
msgstr "ଡାହାଣ ଶୀର୍ଷକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_FOOTER\n"
"string.text"
msgid "Footer"
msgstr "ପାଦଟୀକା"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_FOOTERL\n"
"string.text"
msgid "Footer Left"
msgstr "ବାମ ପାଦଟୀକା"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_FOOTERR\n"
"string.text"
msgid "Footer Right"
msgstr "ଡାହାଣ ପାଦଟିକା"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TABLE\n"
"string.text"
msgid "Table Contents"
msgstr "ଟେବୁଲ ସୂଚୀପତ୍ରଗୁଡିକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TABLE_HDLN\n"
"string.text"
msgid "Table Heading"
msgstr "ଟେବୁଲ ଶୀର୍ଷକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_FRAME\n"
"string.text"
msgid "Frame Contents"
msgstr "ଫ୍ରେମ ସୂଚୀପତ୍ରଗୁଡିକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_FOOTNOTE\n"
"string.text"
msgid "Footnote"
msgstr "ପାଦଟିପପ୍ଣୀ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_ENDNOTE\n"
"string.text"
msgid "Endnote"
msgstr "ଶେଷଟିପପ୍ଣୀ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_LABEL\n"
"string.text"
msgid "Caption"
msgstr "ଶିରୋନାମା"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_LABEL_ABB\n"
"string.text"
msgid "Illustration"
msgstr "ଦୃଷ୍ଟାନ୍ତ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_LABEL_TABLE\n"
"string.text"
msgid "Table"
msgstr "ଟେବୁଲ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_LABEL_FRAME\n"
"string.text"
msgid "Text"
msgstr "ଟେକ୍ସଟ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_LABEL_DRAWING\n"
"string.text"
msgid "Drawing"
msgstr "ଚିତ୍ରାଙ୍କନ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_JAKETADRESS\n"
"string.text"
msgid "Addressee"
msgstr "ପତ୍ରଗ୍ରାହୀ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_SENDADRESS\n"
"string.text"
msgid "Sender"
msgstr "ପ୍ରେରକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_IDXH\n"
"string.text"
msgid "Index Heading"
msgstr "ସୂଚୀ ଶୀର୍ଷକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_IDX1\n"
"string.text"
msgid "Index 1"
msgstr "ସୂଚୀ ୧"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_IDX2\n"
"string.text"
msgid "Index 2"
msgstr "ସୂଚୀ ୨"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_IDX3\n"
"string.text"
msgid "Index 3"
msgstr "ସୂଚୀ ୩"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_IDXBREAK\n"
"string.text"
msgid "Index Separator"
msgstr "ସୂଚୀ ସେପାରେଟର"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNTH\n"
"string.text"
msgid "Contents Heading"
msgstr "ଶୀର୍ଷକ ସୂଚୀପତ୍ରଗୁଡିକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT1\n"
"string.text"
msgid "Contents 1"
msgstr "ସୂଚୀପତ୍ରଗୁଡିକ ୧"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT2\n"
"string.text"
msgid "Contents 2"
msgstr "ସୂଚୀପତ୍ରଗୁଡିକ ୨"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT3\n"
"string.text"
msgid "Contents 3"
msgstr "ସୂଚୀପତ୍ରଗୁଡିକ ୩"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT4\n"
"string.text"
msgid "Contents 4"
msgstr "ସୂଚୀପତ୍ରଗୁଡିକ ୪"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT5\n"
"string.text"
msgid "Contents 5"
msgstr "ସୂଚୀପତ୍ରଗୁଡିକ ୫"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT6\n"
"string.text"
msgid "Contents 6"
msgstr "ସୂଚୀପତ୍ରଗୁଡିକ ୬"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT7\n"
"string.text"
msgid "Contents 7"
msgstr "ସୂଚୀପତ୍ରଗୁଡିକ ୭"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT8\n"
"string.text"
msgid "Contents 8"
msgstr "ସୂଚୀପତ୍ରଗୁଡିକ ୮"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT9\n"
"string.text"
msgid "Contents 9"
msgstr "ସୂଚୀପତ୍ରଗୁଡିକ ୯"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT10\n"
"string.text"
msgid "Contents 10"
msgstr "ସୂଚୀପତ୍ରଗୁଡିକ ୧୦"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USERH\n"
"string.text"
msgid "User Index Heading"
msgstr "ଉପୟୋଗକର୍ତ୍ତା ସୂଚୀ ଶୀର୍ଷକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER1\n"
"string.text"
msgid "User Index 1"
msgstr "ଉପୟୋଗକର୍ତ୍ତା ସୂଚୀ ୧"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER2\n"
"string.text"
msgid "User Index 2"
msgstr "ଉପୟୋଗକର୍ତ୍ତା ସୂଚୀ ୨"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER3\n"
"string.text"
msgid "User Index 3"
msgstr "ଉପୟୋଗକର୍ତ୍ତା ସୂଚୀ ୩"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER4\n"
"string.text"
msgid "User Index 4"
msgstr "ଉପୟୋଗକର୍ତ୍ତା ସୂଚୀ ୪"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER5\n"
"string.text"
msgid "User Index 5"
msgstr "ଉପୟୋଗକର୍ତ୍ତା ସୂଚୀ ୫"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER6\n"
"string.text"
msgid "User Index 6"
msgstr "ଉପୟୋଗକର୍ତ୍ତା ସୂଚୀ ୬"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER7\n"
"string.text"
msgid "User Index 7"
msgstr "ଉପୟୋଗକର୍ତ୍ତା ସୂଚୀ ୭"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER8\n"
"string.text"
msgid "User Index 8"
msgstr "ଉପୟୋଗକର୍ତ୍ତା ସୂଚୀ ୮"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER9\n"
"string.text"
msgid "User Index 9"
msgstr "ଉପୟୋଗକର୍ତ୍ତା ସୂଚୀ ୯"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER10\n"
"string.text"
msgid "User Index 10"
msgstr "ଉପୟୋଗକର୍ତ୍ତା ସୂଚୀ ୧୦"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CITATION\n"
"string.text"
msgid "Citation"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_ILLUSH\n"
"string.text"
msgid "Illustration Index Heading"
msgstr "ଦୃଷ୍ଟାନ୍ତ ସୂଚୀ ଶୀର୍ଷକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_ILLUS1\n"
"string.text"
msgid "Illustration Index 1"
msgstr "ଦୃଷ୍ଟାନ୍ତ ସୂଚୀ ୧"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_OBJECTH\n"
"string.text"
msgid "Object Index Heading"
msgstr "ବସ୍ତୁ ସୂଚୀ ଶୀର୍ଷକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_OBJECT1\n"
"string.text"
msgid "Object Index 1"
msgstr "ବସ୍ତୁ ସୂଚୀ ୧"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_TABLESH\n"
"string.text"
msgid "Table Index Heading"
msgstr "ଟେବୁଲ ସୂଚୀ ଶୀର୍ଷକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_TABLES1\n"
"string.text"
msgid "Table Index 1"
msgstr "ଟେବୁଲ ସୂଚୀ ୧"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_AUTHORITIESH\n"
"string.text"
msgid "Bibliography Heading"
msgstr "ଗ୍ରନ୍ଥସୂଚୀ ଶୀର୍ଷକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_AUTHORITIES1\n"
"string.text"
msgid "Bibliography 1"
msgstr "ଗ୍ରନ୍ଥସୂଚୀ ୧"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_DOC_SUBTITEL\n"
"string.text"
msgid "Subtitle"
msgstr "ଉପଶୀର୍ଷକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HTML_BLOCKQUOTE\n"
"string.text"
msgid "Quotations"
msgstr "ଉଦ୍ଧୃତାଶଂଗୁଡିକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HTML_PRE\n"
"string.text"
msgid "Preformatted Text"
msgstr "ପୂର୍ବଫର୍ମାଟହୋଇଥିବା ଟେକ୍ସଟ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HTML_HR\n"
"string.text"
msgid "Horizontal Line"
msgstr "ଦିଗବଳୀଯ ରେଖା"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HTML_DD\n"
"string.text"
msgid "List Contents"
msgstr "ତାଲିକା ସୂଚୀପତ୍ରଗୁଡିକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HTML_DT\n"
"string.text"
msgid "List Heading"
msgstr "ତାଲିକା ଶୀର୍ଷକ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_STANDARD\n"
"string.text"
msgid "Default Style"
msgstr "ପୂର୍ବ ନିର୍ଦ୍ଧାରିତ ଶୈଳୀ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_FIRST\n"
"string.text"
msgid "First Page"
msgstr "ପ୍ରଥମ ପୃଷ୍ଠା"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_LEFT\n"
"string.text"
msgid "Left Page"
msgstr "ଶେଷ ପୃଷ୍ଠା"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_RIGHT\n"
"string.text"
msgid "Right Page"
msgstr "ଡାହାଣ ପୃଷ୍ଠା"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_JAKET\n"
"string.text"
msgid "Envelope"
msgstr "ଲଫାପା"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_REGISTER\n"
"string.text"
msgid "Index"
msgstr "ସୂଚୀ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_HTML\n"
"string.text"
msgid "HTML"
msgstr "HTML"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_FOOTNOTE\n"
"string.text"
msgid "Footnote"
msgstr "ପାଦଟିପପ୍ଣୀ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_ENDNOTE\n"
"string.text"
msgid "Endnote"
msgstr "ଶେଷଟିପପ୍ଣୀ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_NUM1\n"
"string.text"
msgid "Numbering 1"
msgstr "ସଂଖ୍ଯାଦେବା ୧"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_NUM2\n"
"string.text"
msgid "Numbering 2"
msgstr "ସଂଖ୍ଯାଦେବା ୨"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_NUM3\n"
"string.text"
msgid "Numbering 3"
msgstr "ସଂଖ୍ଯାଦେବା ୩"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_NUM4\n"
"string.text"
msgid "Numbering 4"
msgstr "ସଂଖ୍ଯାଦେବା ୪ "

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_NUM5\n"
"string.text"
msgid "Numbering 5"
msgstr "ସଂଖ୍ଯାଦେବା ୫"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_BUL1\n"
"string.text"
msgid "List 1"
msgstr "ତାଲିକା ୧"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_BUL2\n"
"string.text"
msgid "List 2"
msgstr "ତାଲିକା ୨"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_BUL3\n"
"string.text"
msgid "List 3"
msgstr "ତାଲିକା ୩ "

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_BUL4\n"
"string.text"
msgid "List 4"
msgstr "ତାଲିକା ୪"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_BUL5\n"
"string.text"
msgid "List 5"
msgstr "ତାଲିକା ୫ "

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_RUBYTEXT\n"
"string.text"
msgid "Rubies"
msgstr "ରୁବିଜ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_COLUMN_VALUESET_ITEM0\n"
"string.text"
msgid "1 column"
msgstr "1 ଟି ସ୍ତମ୍ଭ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_COLUMN_VALUESET_ITEM1\n"
"string.text"
msgid "2 columns with equal size"
msgstr "ସମାନ ଆକାର ବିଶିଷ୍ଟ 2 ଟି ସ୍ତମ୍ଭ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_COLUMN_VALUESET_ITEM2\n"
"string.text"
msgid "3 columns with equal size"
msgstr "ସମାନ ଆକାର ବିଶିଷ୍ଟ 3 ଟି ସ୍ତମ୍ଭ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_COLUMN_VALUESET_ITEM3\n"
"string.text"
msgid "2 columns with different size (left > right)"
msgstr "ଭିନ୍ନ ଆକାର ବିଶିଷ୍ଟ 2 ଟି ସ୍ତମ୍ଭ  (ବାମ > ଡ଼ାହାଣ)"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_COLUMN_VALUESET_ITEM4\n"
"string.text"
msgid "2 columns with different size (left < right)"
msgstr "ଭିନ୍ନ ଆକାର ବିଶିଷ୍ଟ 2 ଟି ସ୍ତମ୍ଭ  (ବାମ < ଡ଼ାହାଣ)"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_VERT_NUM\n"
"string.text"
msgid "Vertical Numbering Symbols"
msgstr "ଲମ୍ବରୂପ ସଂଖ୍ଯାଦେବା ସଂଦେଶଗୁଡିକ"

#: poolfmt.src
#, fuzzy
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_DEFAULT\n"
"string.text"
msgid "Default Style"
msgstr "ପୂର୍ବ ନିର୍ଦ୍ଧାରିତ ଶୈଳୀ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_3D\n"
"string.text"
msgid "3D"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_BLACK1\n"
"string.text"
msgid "Black 1"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_BLACK2\n"
"string.text"
msgid "Black 2"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_BLUE\n"
"string.text"
msgid "Blue"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_BROWN\n"
"string.text"
msgid "Brown"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_CURRENCY\n"
"string.text"
msgid "Currency"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_CURRENCY_3D\n"
"string.text"
msgid "Currency 3D"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_CURRENCY_GRAY\n"
"string.text"
msgid "Currency Gray"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_CURRENCY_LAVENDER\n"
"string.text"
msgid "Currency Lavender"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_CURRENCY_TURQUOISE\n"
"string.text"
msgid "Currency Turquoise"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_GRAY\n"
"string.text"
msgid "Gray"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_GREEN\n"
"string.text"
msgid "Green"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_LAVENDER\n"
"string.text"
msgid "Lavender"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_RED\n"
"string.text"
msgid "Red"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_TURQUOISE\n"
"string.text"
msgid "Turquoise"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_YELLOW\n"
"string.text"
msgid "Yellow"
msgstr ""

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_DEL_EMPTY_PARA+1\n"
"string.text"
msgid "Remove empty paragraphs"
msgstr "ଶୂନ୍ଯ ପରିଚ୍ଛେଦଗୁଡିକ ହଟାଅ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_USE_REPLACE+1\n"
"string.text"
msgid "Use replacement table"
msgstr "ପୁନଃସ୍ଥାପନ ଟେବୁଲ ଉପୟୋଗ କର"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_CPTL_STT_WORD+1\n"
"string.text"
msgid "Correct TWo INitial CApitals"
msgstr "ସଂଶୋଧନ କର TWo INitial CApitals"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_CPTL_STT_SENT+1\n"
"string.text"
msgid "Capitalize first letter of sentences"
msgstr "ବାକ୍ଯଗୁଡିକକର ପ୍ରଥମ ଅକ୍ଷର ବଡ କର"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_TYPO+1\n"
"string.text"
msgid "Replace \"standard\" quotes with %1 \\bcustom%2 quotes"
msgstr "%1 ସହିତ \\bcustom%2 କୋଟଗୁଡିକରେ \"ସ୍ଟାଣ୍ଡାଡ୍\" କୋଟ ଦ୍ବାରା ପୁନଃସ୍ଥାପିତ କର"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_USER_STYLE+1\n"
"string.text"
msgid "Replace Custom Styles"
msgstr "ବ୍ଯବସ୍ଥା ଶୈଳୀଗୁଡିକ ପୁନଃସ୍ଥାପିତ କର"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_BULLET+1\n"
"string.text"
msgid "Bullets replaced"
msgstr "ଗୁଳିଗୁଡିକ ପୁନଃସ୍ଥାପିତ ହୋଇଛି"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_UNDER+1\n"
"string.text"
msgid "Automatic _underline_"
msgstr "ସ୍ବଯଂଚାଳିତ_ରେଖାଙ୍କିତ_"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_BOLD+1\n"
"string.text"
msgid "Automatic *bold*"
msgstr "ସ୍ବଯଂଚାଳିତ *ମୋଟା*"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_FRACTION+1\n"
"string.text"
msgid "Replace 1/2 ... with ½ ..."
msgstr "½ ...ଦ୍ବାରା  1/2 ... କୁ ପୁନଃସ୍ଥାପିତ କର"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_DETECT_URL+1\n"
"string.text"
msgid "URL recognition"
msgstr "URL ପରିଚଯ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_DASH+1\n"
"string.text"
msgid "Replace dashes"
msgstr "ଡ୍ଯାସଗୁଡିକ ପୁନଃସ୍ଥାପିତ କର"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_ORDINAL+1\n"
"string.text"
msgid "Replace 1st... with 1^st..."
msgstr "1^st ...ଦ୍ବାରା 1st ... କୁ ପୁନଃସ୍ଥାପିତ କର"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_RIGHT_MARGIN+1\n"
"string.text"
msgid "Combine single line paragraphs"
msgstr "ଏକମାତ୍ର ରେଖା ପରିଚ୍ଛେଦଗୁଡିକ ମିଶାଅ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_SET_TMPL_TEXT +1\n"
"string.text"
msgid "Set \"Text body\" Style"
msgstr "\"ଟେକ୍ସଟ ବିଭାଗ\" ଶୈଳୀ ସେଟ କର"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_SET_TMPL_INDENT  +1\n"
"string.text"
msgid "Set \"Text body indent\" Style"
msgstr "\"ଟେକ୍ସଟ ବିଭାଗ ଇନଡେଣ୍ଟ\" ଶୈଳୀ ସେଟ କର"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_SET_TMPL_NEG_INDENT  +1\n"
"string.text"
msgid "Set \"Hanging indent\" Style"
msgstr "\"ଅଟକିବା ଇନଡେଣ୍ଟ\" ଶୈଳୀ ସେଟ କର"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_SET_TMPL_TEXT_INDENT +1\n"
"string.text"
msgid "Set \"Text body indent\" Style"
msgstr "\"ଟେକ୍ସଟ ବିଭାଗ ଇନଡେଣ୍ଟ\" ଶୈଳୀ ସେଟ କର"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_SET_TMPL_HEADLINE +1\n"
"string.text"
msgid "Set \"Heading $(ARG1)\" Style"
msgstr "\"ଶୀର୍ଷକ $(ARG1)\" ଶୈଳୀ ସେଟ କର"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_SET_NUMBULET +1\n"
"string.text"
msgid "Set \"Bullet\" or \"Numbering\" Style"
msgstr "\"ଗୁଳି\" କିମ୍ବା \"ସଂଖ୍ଯାଦେବା\"  ଶୈଳୀ ସେଟ କର"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_DEL_MORELINES +1\n"
"string.text"
msgid "Combine paragraphs"
msgstr "ପରିଚ୍ଛେଦଗୁଡିକ ମିଶାଅ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_NON_BREAK_SPACE +1\n"
"string.text"
msgid "Add non breaking space"
msgstr "ଅଭଙ୍ଗ ଖାଲିସ୍ଥାନକୁ ଯୋଗକରନ୍ତୁ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_OBJECT_SELECT\n"
"string.text"
msgid "Click object"
msgstr "ବସ୍ତୁକୁ କ୍ଲିକ୍ କର"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_START_INS_GLOSSARY\n"
"string.text"
msgid "Before inserting AutoText"
msgstr "ସ୍ବଯଂଚାଳିତଟେକ୍ସଟ ଭର୍ତ୍ତି କରିବା ପୂର୍ବରୁ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_END_INS_GLOSSARY\n"
"string.text"
msgid "After inserting AutoText"
msgstr "ସ୍ବଯଂଚାଳିତଟେକ୍ସଟ ଭର୍ତ୍ତି କରିବା ପରେ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_MOUSEOVER_OBJECT\n"
"string.text"
msgid "Mouse over object"
msgstr "ବସ୍ତୁ ଉପରେ ମାଉସ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_MOUSECLICK_OBJECT\n"
"string.text"
msgid "Trigger hyperlink"
msgstr "ଟ୍ରିଗର ହାଇପରଲିଙ୍କ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_MOUSEOUT_OBJECT\n"
"string.text"
msgid "Mouse leaves object"
msgstr "ମାଉସ ଛାଡୁଥିବା ବସ୍ତୁ "

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_IMAGE_LOAD\n"
"string.text"
msgid "Image loaded successfully"
msgstr ""

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_IMAGE_ABORT\n"
"string.text"
msgid "Image loading terminated"
msgstr ""

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_IMAGE_ERROR\n"
"string.text"
msgid "Could not load image"
msgstr ""

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_FRM_KEYINPUT_A\n"
"string.text"
msgid "Input of alphanumeric characters"
msgstr "ଆଲଫାନ୍ଯୁମରିକ ଅକ୍ଷରଗୁଡିକର ଇନପୁଟ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_FRM_KEYINPUT_NOA\n"
"string.text"
msgid "Input of non-alphanumeric characters"
msgstr "ନନ-ଆଲଫାନ୍ଯୁମରିକ ଅକ୍ଷରଗୁଡିକର ଇନପୁଟ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_FRM_RESIZE\n"
"string.text"
msgid "Resize frame"
msgstr "ଫ୍ରେମକୁ ପୁନଃଆକାର ଦିଅ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_FRM_MOVE\n"
"string.text"
msgid "Move frame"
msgstr "ଫ୍ରେମକୁ ଘୁଞ୍ଚାଅ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_OUTLINE\n"
"string.text"
msgid "Headings"
msgstr "ଶୀର୍ଷକ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_TABLE\n"
"string.text"
msgid "Tables"
msgstr "ଟେବୁଲ୍ଗୁଡିକ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_FRAME\n"
"string.text"
msgid "Text frames"
msgstr "ଟେକ୍ସଟ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_GRAPHIC\n"
"string.text"
msgid "Images"
msgstr ""

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_OLE\n"
"string.text"
msgid "OLE objects"
msgstr "OLE ବସ୍ତୁଗୁଡିକ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_BOOKMARK\n"
"string.text"
msgid "Bookmarks"
msgstr "ପୃଷ୍ଟାସଂକେତଗୁଡିକ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_REGION\n"
"string.text"
msgid "Sections"
msgstr "ବିଭାଗଗୁଡିକ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_URLFIELD\n"
"string.text"
msgid "Hyperlinks"
msgstr "ହାଇପରଲିଙ୍କଗୁଡିକ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_REFERENCE\n"
"string.text"
msgid "References"
msgstr "ପ୍ରସଙ୍ଗଗୁଡିକ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_INDEX\n"
"string.text"
msgid "Indexes"
msgstr "ସୂଚୀଗୁଡିକ"

#: utlui.src
#, fuzzy
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_DRAWOBJECT\n"
"string.text"
msgid "Drawing objects"
msgstr "ବସ୍ତୁଗୁଡିକ ଅଙ୍କନ କର"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_POSTIT\n"
"string.text"
msgid "Comments"
msgstr "ମନ୍ତବ୍ଯଗୁଡିକ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_HEADING1\n"
"string.text"
msgid "Heading 1"
msgstr "ଶୀର୍ଷକ ୧"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_ENTRY1\n"
"string.text"
msgid "This is the content from the first chapter. This is a user directory entry."
msgstr "ଏହା ହେଉଛି ପ୍ରଥମ ଅଧ୍ଯାୟରୁ ଆସିଥିବା ବିଷୟବସ୍ତୁ। ଏହା ଏକ ବ୍ୟବହାରକାରୀ ଡିରେକ୍ଟୋରୀ ନିବେଶ।"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_HEADING11\n"
"string.text"
msgid "Heading 1.1"
msgstr "ଶୀର୍ଷକ 1.1"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_ENTRY11\n"
"string.text"
msgid "This is the content from chapter 1.1. This is the entry for the table of contents."
msgstr "ଏହା ହେଉଛି ଅଧ୍ଯାୟ 1.1 ରୁ ଆସିଥିବା ବିଷୟବସ୍ତୁ। ଏହା ବିଷୟବସ୍ତୁ ସାରଣୀ ପାଇଁ ଏକ ନିବେଶ।"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_HEADING12\n"
"string.text"
msgid "Heading 1.2"
msgstr "ଶୀର୍ଷକ 1.2"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_ENTRY12\n"
"string.text"
msgid "This is the content from chapter 1.2. This keyword is a main entry."
msgstr "ଏହା ହେଉଛି ଅଧ୍ଯାୟ 1.2 ରୁ ଆସିଥିବା ବିଷୟବସ୍ତୁ। ଏହି ସୂଚକ ଶବ୍ଦଟି ହେଉଛି ଏକ ମୂଖ୍ୟ ଭରଣ।"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_TABLE1\n"
"string.text"
msgid "Table 1: This is table 1"
msgstr "ସାରଣୀ 1: ଏହା ହେଉଛି ସାରଣୀ 1"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_IMAGE1\n"
"string.text"
msgid "Image 1: This is image 1"
msgstr ""

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_OUTLINE\n"
"string.text"
msgid "Heading"
msgstr "ଶୀର୍ଷକ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_TABLE\n"
"string.text"
msgid "Table"
msgstr "ଟେବୁଲ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_FRAME\n"
"string.text"
msgid "Text frame"
msgstr "ଟେକ୍ସଟ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_GRAPHIC\n"
"string.text"
msgid "Image"
msgstr ""

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_OLE\n"
"string.text"
msgid "OLE object"
msgstr "OLE ବସ୍ତୁ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_BOOKMARK\n"
"string.text"
msgid "Bookmark"
msgstr "ପୃଷ୍ଠାସଙ୍କେତଗୁଡିକ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_REGION\n"
"string.text"
msgid "Section"
msgstr "ବିଭାଗ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_URLFIELD\n"
"string.text"
msgid "Hyperlink"
msgstr "ହାଇପରଲିଙ୍କ୍"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_REFERENCE\n"
"string.text"
msgid "Reference"
msgstr "ସନ୍ଦର୍ଭ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_INDEX\n"
"string.text"
msgid "Index"
msgstr "ସୂଚୀ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_POSTIT\n"
"string.text"
msgid "Comment"
msgstr "ମନ୍ତବ୍ଯ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_DRAWOBJECT\n"
"string.text"
msgid "Draw object"
msgstr "ବସ୍ତୁଗୁଡିକ ଅଙ୍କନ କର"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_DEFINE_NUMBERFORMAT\n"
"string.text"
msgid "Additional formats..."
msgstr "ଅତିରିକ୍ତ ଫର୍ମାଟଗୁଡିକ..."

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_STR_SYSTEM\n"
"string.text"
msgid "[System]"
msgstr "[ସିସ୍ଟମ]"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_MULT_INTERACT_SPELL_WARN\n"
"string.text"
msgid ""
"The interactive spellcheck is already active\n"
"in a different document"
msgstr ""
"ଇଣ୍ଟରାକଟିଭ ବର୍ଣ୍ଣଶୁଦ୍ଦୀକରଣ ଗୋଟିଏ ଭିନ୍ନ ନାମରେ\n"
"ପୂର୍ବରୁ ସକ୍ରିଯଅଛି"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_MULT_INTERACT_HYPH_WARN\n"
"string.text"
msgid ""
"The interactive hyphenation is already active\n"
"in a different document"
msgstr ""
"ଇଣ୍ଟରାକଟିଭ ହାଇପେନେସନ ଗୋଟିଏ ଭିନ୍ନ ଦଲିଲରେ\n"
"ପୂର୍ବରୁ ସକ୍ରିଯଅଛି"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_SPELL_TITLE\n"
"string.text"
msgid "Spellcheck"
msgstr "ବର୍ଣ୍ଣଶୁଦ୍ଦୀକରଣ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_HYPH_TITLE\n"
"string.text"
msgid "Hyphenation"
msgstr "ହାଇପେନେସନ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_HYPERCTRL_SEL\n"
"string.text"
msgid "SEL"
msgstr "SEL"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_HYPERCTRL_HYP\n"
"string.text"
msgid "HYP"
msgstr " HYP"

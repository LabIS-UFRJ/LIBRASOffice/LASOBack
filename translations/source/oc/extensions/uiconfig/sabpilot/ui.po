#. extracted from extensions/uiconfig/sabpilot/ui
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-11-10 19:33+0100\n"
"PO-Revision-Date: 2016-05-20 13:41+0000\n"
"Last-Translator: Cédric Valmary <cvalmary@yahoo.fr>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: oc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.7\n"
"X-POOTLE-MTIME: 1463751714.000000\n"

#: contentfieldpage.ui
msgctxt ""
"contentfieldpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Existing fields"
msgstr "Camps existents"

#: contentfieldpage.ui
msgctxt ""
"contentfieldpage.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "Display field"
msgstr "Camp d'afichatge"

#: contenttablepage.ui
msgctxt ""
"contenttablepage.ui\n"
"datasourcelabel\n"
"label\n"
"string.text"
msgid "Data source"
msgstr "Font de donadas"

#: contenttablepage.ui
msgctxt ""
"contenttablepage.ui\n"
"contenttypelabel\n"
"label\n"
"string.text"
msgid "Content type"
msgstr "Tipe de contengut"

#: contenttablepage.ui
msgctxt ""
"contenttablepage.ui\n"
"formtablelabel\n"
"label\n"
"string.text"
msgid "Content"
msgstr "Contengut"

#: contenttablepage.ui
msgctxt ""
"contenttablepage.ui\n"
"formsettings\n"
"label\n"
"string.text"
msgid "Form"
msgstr "Formulari"

#: contenttablepage.ui
msgctxt ""
"contenttablepage.ui\n"
"label3\n"
"label\n"
"string.text"
msgid ""
"On the right side, you see all the tables from the data source of the form.\n"
"\n"
"\n"
"Choose the table from which the data should be used as basis for the list content:"
msgstr ""
"Sul costat dreit, vejatz totas las taulas de la font de donadas del formulari.\n"
"\n"
"\n"
"Causissètz la taula a partir de la quala las donadas devon èsser utilizadas coma basa pel contengut de la lista :"

#: contenttablepage.ui
msgctxt ""
"contenttablepage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Control"
msgstr "Contraròtle"

#: datasourcepage.ui
msgctxt ""
"datasourcepage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid ""
"That was all the information necessary to integrate your address data into %PRODUCTNAME.\n"
"\n"
"Now, just enter the name under which you want to register the data source in %PRODUCTNAME."
msgstr ""
"Èran totas las informacions que calià per introduire las donadas de vòstra adreça dins  %PRODUCTNAME.\n"
"\n"
"Ara, vos cal picar lo nom que definirà vòstra font de donadas dins %PRODUCTNAME."

#: datasourcepage.ui
msgctxt ""
"datasourcepage.ui\n"
"embed\n"
"label\n"
"string.text"
msgid "Embed this address book definition into the current document."
msgstr "Integrar aquesta definicion del quasernat d'adreças dins lo document actual."

#: datasourcepage.ui
msgctxt ""
"datasourcepage.ui\n"
"locationft\n"
"label\n"
"string.text"
msgid "Location"
msgstr "Emplaçament"

#: datasourcepage.ui
msgctxt ""
"datasourcepage.ui\n"
"browse\n"
"label\n"
"string.text"
msgid "Browse..."
msgstr "Percórrer..."

#: datasourcepage.ui
msgctxt ""
"datasourcepage.ui\n"
"available\n"
"label\n"
"string.text"
msgid "Make this address book available to all modules in %PRODUCTNAME."
msgstr "Rendre aqueste quasernet d'adreças accessible a totes los moduls de %PRODUCTNAME."

#: datasourcepage.ui
msgctxt ""
"datasourcepage.ui\n"
"nameft\n"
"label\n"
"string.text"
msgid "Address book name"
msgstr "Nom del quasernet d'adreças"

#: datasourcepage.ui
msgctxt ""
"datasourcepage.ui\n"
"warning\n"
"label\n"
"string.text"
msgid "Another data source already has this name. As data sources have to have globally unique names, you need to choose another one."
msgstr "Una autra font de donadas a ja lo meteis nom. Coma las fonts de donadas devon aver de noms unics, vos cal causir un nom diferent."

#: defaultfieldselectionpage.ui
msgctxt ""
"defaultfieldselectionpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Should one option field be selected as a default?"
msgstr "Seleccionar un boton ràdio per defaut ?"

#: defaultfieldselectionpage.ui
msgctxt ""
"defaultfieldselectionpage.ui\n"
"defaultselectionyes\n"
"label\n"
"string.text"
msgid "_Yes, the following:"
msgstr "Òc, lo boton _seguent :"

#: defaultfieldselectionpage.ui
msgctxt ""
"defaultfieldselectionpage.ui\n"
"defaultselectionno\n"
"label\n"
"string.text"
msgid "No, one particular field is not going to be selected."
msgstr "Non, cap de boton particular deu pas èsser seleccionat."

#: fieldassignpage.ui
msgctxt ""
"fieldassignpage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid ""
"To incorporate the address data in your templates, %PRODUCTNAME has to know which fields contain which data.\n"
"\n"
"For instance, you could have stored the e-mail addresses in a field named \"email\", or \"E-mail\" or \"EM\" - or something completely different.\n"
"\n"
"Click the button below to open another dialog where you can enter the settings for your data source."
msgstr ""
"Per inclure las donadas d'adreças dins los modèls, %PRODUCTNAME deu saber quines camps contenon quinas donadas.\n"
"\n"
"Per exemple, podètz aver emmagazinadas las adreças e-mail dins un camp nomenat \"email\" o \"E-mail\" o \"EM\" - o quicòm de completament diferent.\n"
"\n"
"Clicatz sul boton çaijós per dobrir una autra bóstia de dialòg dins la quala podètz sasir los paramètres de la basa de donadas."

#: fieldassignpage.ui
msgctxt ""
"fieldassignpage.ui\n"
"assign\n"
"label\n"
"string.text"
msgid "Field Assignment"
msgstr "Assignacion de camp"

#: fieldlinkpage.ui
msgctxt ""
"fieldlinkpage.ui\n"
"desc\n"
"label\n"
"string.text"
msgid "This is where you select fields with matching contents so that the value from the display field will be shown."
msgstr "Es l'endreit ont seleccionatz los camps que lo contengut deu coïncidir per tal d'afichar la valor del camp de visualizacion. "

#: fieldlinkpage.ui
msgctxt ""
"fieldlinkpage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Field from the _Value Table"
msgstr "Camp de la taula _Valors"

#: fieldlinkpage.ui
msgctxt ""
"fieldlinkpage.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "Field from the _List Table"
msgstr "Camp de la taula _Lista"

#: gridfieldsselectionpage.ui
msgctxt ""
"gridfieldsselectionpage.ui\n"
"datasourcelabel\n"
"label\n"
"string.text"
msgid "Data source"
msgstr "Font de donadas"

#: gridfieldsselectionpage.ui
msgctxt ""
"gridfieldsselectionpage.ui\n"
"contenttypelabel\n"
"label\n"
"string.text"
msgid "Content type"
msgstr "Tipe de contengut"

#: gridfieldsselectionpage.ui
msgctxt ""
"gridfieldsselectionpage.ui\n"
"formtablelabel\n"
"label\n"
"string.text"
msgid "Content"
msgstr "Contengut"

#: gridfieldsselectionpage.ui
msgctxt ""
"gridfieldsselectionpage.ui\n"
"formsettings\n"
"label\n"
"string.text"
msgid "Form"
msgstr "Contengut"

#: gridfieldsselectionpage.ui
msgctxt ""
"gridfieldsselectionpage.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "Selected fields"
msgstr "Camps seleccionats"

#: gridfieldsselectionpage.ui
msgctxt ""
"gridfieldsselectionpage.ui\n"
"fieldright\n"
"label\n"
"string.text"
msgid "->"
msgstr "->"

#: gridfieldsselectionpage.ui
msgctxt ""
"gridfieldsselectionpage.ui\n"
"allfieldsright\n"
"label\n"
"string.text"
msgid "=>>"
msgstr "=>>"

#: gridfieldsselectionpage.ui
msgctxt ""
"gridfieldsselectionpage.ui\n"
"fieldleft\n"
"label\n"
"string.text"
msgid "<-"
msgstr "<-"

#: gridfieldsselectionpage.ui
msgctxt ""
"gridfieldsselectionpage.ui\n"
"allfieldsleft\n"
"label\n"
"string.text"
msgid "<<="
msgstr "<<="

#: gridfieldsselectionpage.ui
msgctxt ""
"gridfieldsselectionpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Existing fields"
msgstr "Camps existents"

#: gridfieldsselectionpage.ui
msgctxt ""
"gridfieldsselectionpage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Table element"
msgstr "Element de taula"

#: groupradioselectionpage.ui
msgctxt ""
"groupradioselectionpage.ui\n"
"datasourcelabel\n"
"label\n"
"string.text"
msgid "Data source"
msgstr "Font de donadas"

#: groupradioselectionpage.ui
msgctxt ""
"groupradioselectionpage.ui\n"
"contenttypelabel\n"
"label\n"
"string.text"
msgid "Content type"
msgstr "Tipe de contengut"

#: groupradioselectionpage.ui
msgctxt ""
"groupradioselectionpage.ui\n"
"formtablelabel\n"
"label\n"
"string.text"
msgid "Content"
msgstr "Contengut"

#: groupradioselectionpage.ui
msgctxt ""
"groupradioselectionpage.ui\n"
"formsettings\n"
"label\n"
"string.text"
msgid "Form"
msgstr "Formulari"

#: groupradioselectionpage.ui
msgctxt ""
"groupradioselectionpage.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "_Option fields"
msgstr "_Botons ràdio"

#: groupradioselectionpage.ui
msgctxt ""
"groupradioselectionpage.ui\n"
"toright\n"
"label\n"
"string.text"
msgid "_>>"
msgstr "_>>"

#: groupradioselectionpage.ui
msgctxt ""
"groupradioselectionpage.ui\n"
"toleft\n"
"label\n"
"string.text"
msgid "_<<"
msgstr "_<<"

#: groupradioselectionpage.ui
msgctxt ""
"groupradioselectionpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Which _names do you want to give the option fields?"
msgstr "Quins _noms volètz donar als botons ràdio ?"

#: groupradioselectionpage.ui
msgctxt ""
"groupradioselectionpage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Table element"
msgstr "Element de taula"

#: invokeadminpage.ui
msgctxt ""
"invokeadminpage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid ""
"To set up the new data source, additional information is required.\n"
"\n"
"Click the following button to open another dialog in which you then enter the necessary information."
msgstr ""
"D'informacions suplementàrias son necessàrias per definir la novèla font de donadas.\n"
"\n"
"Clicatz sus lo boton seguent per dobrir una autra bóstia de dialòg per i dintrar las informacions suplementàrias."

#: invokeadminpage.ui
msgctxt ""
"invokeadminpage.ui\n"
"settings\n"
"label\n"
"string.text"
msgid "Settings"
msgstr "Paramètres"

#: invokeadminpage.ui
msgctxt ""
"invokeadminpage.ui\n"
"warning\n"
"label\n"
"string.text"
msgid ""
"The connection to the data source could not be established.\n"
"Before you proceed, please check the settings made, or (on the previous page) choose another address data source type."
msgstr ""
"La connexion cap a la font de donadas es impossible.\n"
"Abans de contunhar, verificatz los paramètres definits, o (a la pagina precedenta) causissètz  un autre tipe de font de donadas de las adreças."

#: optiondbfieldpage.ui
msgctxt ""
"optiondbfieldpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Do you want to save the value in a database field?"
msgstr "Volètz enregistrar la valor dins un camp de basa de donadas ?"

#: optiondbfieldpage.ui
msgctxt ""
"optiondbfieldpage.ui\n"
"yesRadiobutton\n"
"label\n"
"string.text"
msgid "_Yes, I want to save it in the following database field:"
msgstr "Òc, lo _vòli enregistrar dins lo camp de basa de donadas seguent :"

#: optiondbfieldpage.ui
msgctxt ""
"optiondbfieldpage.ui\n"
"noRadiobutton\n"
"label\n"
"string.text"
msgid "_No, I only want to save the value in the form."
msgstr "_Non, vòli solament enregistrar la valor dins lo formulari."

#: optionsfinalpage.ui
msgctxt ""
"optionsfinalpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Which _caption is to be given to your option group?"
msgstr "Quina _legenda deu èsser donada al grop de botons ràdio ?"

#: optionsfinalpage.ui
msgctxt ""
"optionsfinalpage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "These were all details needed to create the option group."
msgstr "Totes aqueles detalhs èran necessaris a la creacion d'un grop de botons."

#: optionvaluespage.ui
msgctxt ""
"optionvaluespage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "When you select an option, the option group is given a specific value."
msgstr "Quand seleccionatz una opcion, lo grop recep una valor especifica."

#: optionvaluespage.ui
msgctxt ""
"optionvaluespage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Which _value do you want to assign to each option?"
msgstr "Quina valor volètz atribuir a cada opcion ?"

#: optionvaluespage.ui
msgctxt ""
"optionvaluespage.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "_Option fields"
msgstr "_Botons ràdio"

#: selecttablepage.ui
msgctxt ""
"selecttablepage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid ""
"The external data source you have chosen contains more than one address book.\n"
"Please select the one you mainly want to work with:"
msgstr ""
"La font extèrna de donadas qu'avètz causida conten mai d'un quasernet d'adreças.\n"
"Seleccionatz lo principal amb lo qual volètz trabalhar :"

#: selecttypepage.ui
msgctxt ""
"selecttypepage.ui\n"
"evolution\n"
"label\n"
"string.text"
msgid "Evolution"
msgstr "Evolution"

#: selecttypepage.ui
msgctxt ""
"selecttypepage.ui\n"
"groupwise\n"
"label\n"
"string.text"
msgid "Groupwise"
msgstr "Groupwise"

#: selecttypepage.ui
msgctxt ""
"selecttypepage.ui\n"
"evoldap\n"
"label\n"
"string.text"
msgid "Evolution LDAP"
msgstr "Evolution LDAP"

#: selecttypepage.ui
msgctxt ""
"selecttypepage.ui\n"
"firefox\n"
"label\n"
"string.text"
msgid "Firefox/Iceweasel"
msgstr "Firefox/Iceweasel"

#: selecttypepage.ui
msgctxt ""
"selecttypepage.ui\n"
"thunderbird\n"
"label\n"
"string.text"
msgid "Thunderbird/Icedove"
msgstr "Thunderbird/Icedove"

#: selecttypepage.ui
msgctxt ""
"selecttypepage.ui\n"
"kde\n"
"label\n"
"string.text"
msgid "KDE address book"
msgstr "Quasernet d'adreças KDE"

#: selecttypepage.ui
msgctxt ""
"selecttypepage.ui\n"
"macosx\n"
"label\n"
"string.text"
msgid "Mac OS X address book"
msgstr "Quasernet d'adreças Mac OS X"

#: selecttypepage.ui
msgctxt ""
"selecttypepage.ui\n"
"other\n"
"label\n"
"string.text"
msgid "Other external data source"
msgstr "Autra font de donadas extèrna"

#: selecttypepage.ui
msgctxt ""
"selecttypepage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Please Select the Type of Your External Address Book"
msgstr "Seleccionatz lo tipe de vòstre quasernet d'adreças extèrne"

#: selecttypepage.ui
msgctxt ""
"selecttypepage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid ""
"%PRODUCTNAME lets you access address data already present in your system. To do this, a %PRODUCTNAME data source will be created in which your address data is available in tabular form.\n"
"\n"
"This wizard helps you create the data source."
msgstr ""
"%PRODUCTNAME vos permet d'accedir a las donadas d'adreças ja presentas sus vòstre sistèma. Per aquò far, una font de donadas de %PRODUCTNAME serà creada dins la quala las donadas d'adreças son disponiblas jos la forma de taula.\n"
"\n"
"Aqueste assistent vos ajuda a crear la font de donadas."

#: tableselectionpage.ui
msgctxt ""
"tableselectionpage.ui\n"
"label3\n"
"label\n"
"string.text"
msgid ""
"Currently, the form the control belongs to is not (or not completely) bound to a data source.\n"
"\n"
"Please choose a data source and a table.\n"
"\n"
"\n"
"Please note that the settings made on this page will take effect immediately upon leaving the page."
msgstr ""
"Actualament, lo formulari, al qual aparten lo contraròtle, es pas (o pas completament) connectat a una font de donadas.\n"
"\n"
"Seleccionatz una font de donadas, e una taula.\n"
"\n"
"\n"
"Notatz que los paramètres especificats sus aquesta pagina seràn preses en compte tre que quitaretz la pagina."

#: tableselectionpage.ui
msgctxt ""
"tableselectionpage.ui\n"
"datasourcelabel\n"
"label\n"
"string.text"
msgid "_Data source:"
msgstr "_Font de donadas :"

#: tableselectionpage.ui
msgctxt ""
"tableselectionpage.ui\n"
"search\n"
"label\n"
"string.text"
msgid "_..."
msgstr "_..."

#: tableselectionpage.ui
msgctxt ""
"tableselectionpage.ui\n"
"tablelabel\n"
"label\n"
"string.text"
msgid "_Table / Query:"
msgstr "_Taula / requèsta :"

#: tableselectionpage.ui
msgctxt ""
"tableselectionpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Data"
msgstr "Donadas"

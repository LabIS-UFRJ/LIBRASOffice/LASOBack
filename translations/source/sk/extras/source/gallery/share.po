#. extracted from extras/source/gallery/share
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-04-22 23:40+0200\n"
"PO-Revision-Date: 2017-01-19 14:21+0000\n"
"Last-Translator: Miloš Šrámek <msramek22@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1484835684.000000\n"

#: gallery_names.ulf
msgctxt ""
"gallery_names.ulf\n"
"arrows\n"
"LngText.text"
msgid "Arrows"
msgstr "Šípky"

#: gallery_names.ulf
msgctxt ""
"gallery_names.ulf\n"
"computers\n"
"LngText.text"
msgid "Computers"
msgstr "Počítače"

#: gallery_names.ulf
msgctxt ""
"gallery_names.ulf\n"
"diagrams\n"
"LngText.text"
msgid "Diagrams"
msgstr "Diagramy"

#: gallery_names.ulf
msgctxt ""
"gallery_names.ulf\n"
"education\n"
"LngText.text"
msgid "School & University"
msgstr "Škola a univerzita"

#: gallery_names.ulf
msgctxt ""
"gallery_names.ulf\n"
"environment\n"
"LngText.text"
msgid "Environment"
msgstr "Prostredie"

#: gallery_names.ulf
msgctxt ""
"gallery_names.ulf\n"
"finance\n"
"LngText.text"
msgid "Finance"
msgstr "Financie"

#: gallery_names.ulf
msgctxt ""
"gallery_names.ulf\n"
"people\n"
"LngText.text"
msgid "People"
msgstr "Ľudia"

#: gallery_names.ulf
msgctxt ""
"gallery_names.ulf\n"
"sounds\n"
"LngText.text"
msgid "Sounds"
msgstr "Zvuky"

#: gallery_names.ulf
msgctxt ""
"gallery_names.ulf\n"
"symbols\n"
"LngText.text"
msgid "Symbols"
msgstr "Symboly"

#: gallery_names.ulf
msgctxt ""
"gallery_names.ulf\n"
"txtshapes\n"
"LngText.text"
msgid "Text Shapes"
msgstr "Textové objekty"

#: gallery_names.ulf
msgctxt ""
"gallery_names.ulf\n"
"transportation\n"
"LngText.text"
msgid "Transportation"
msgstr "Doprava"

#. extracted from cui/source/tabpages
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2017-01-19 14:21+0000\n"
"Last-Translator: Miloš Šrámek <msramek22@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1484835716.000000\n"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_TABLE_PRESET_NONE\n"
"string.text"
msgid "Set No Borders"
msgstr "Bez orámovania"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_TABLE_PRESET_ONLYOUTER\n"
"string.text"
msgid "Set Outer Border Only"
msgstr "Nastaviť len vonkajšie orámovanie tabuľky"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_TABLE_PRESET_OUTERHORI\n"
"string.text"
msgid "Set Outer Border and Horizontal Lines"
msgstr "Nastaviť vonkajšie orámovanie a vodorovné čiary"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_TABLE_PRESET_OUTERALL\n"
"string.text"
msgid "Set Outer Border and All Inner Lines"
msgstr "Nastaviť vonkajšie orámovanie a všetky vnútorné čiary"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_TABLE_PRESET_OUTERINNER\n"
"string.text"
msgid "Set Outer Border Without Changing Inner Lines"
msgstr "Nastaviť vonkajšie orámovanie bez zmeny vnútorných čiar"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_PARA_PRESET_DIAGONAL\n"
"string.text"
msgid "Set Diagonal Lines Only"
msgstr "Nastaviť len uhlopriečky"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_PARA_PRESET_ALL\n"
"string.text"
msgid "Set All Four Borders"
msgstr "Orámovanie"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_PARA_PRESET_LEFTRIGHT\n"
"string.text"
msgid "Set Left and Right Borders Only"
msgstr "Orámovanie len vľavo a vpravo"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_PARA_PRESET_TOPBOTTOM\n"
"string.text"
msgid "Set Top and Bottom Borders Only"
msgstr "Orámovanie len hore a dolu"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_PARA_PRESET_ONLYLEFT\n"
"string.text"
msgid "Set Left Border Only"
msgstr "Orámovanie len vľavo"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_HOR_PRESET_ONLYHOR\n"
"string.text"
msgid "Set Top and Bottom Borders, and All Inner Lines"
msgstr "Orámovanie hore a dolu a všetky vnútorné čiary"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_VER_PRESET_ONLYVER\n"
"string.text"
msgid "Set Left and Right Borders, and All Inner Lines"
msgstr "Orámovanie vľavo a vpravo a všetky vnútorné čiary"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_SHADOW_STYLE_NONE\n"
"string.text"
msgid "No Shadow"
msgstr "Bez tieňovania"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_SHADOW_STYLE_BOTTOMRIGHT\n"
"string.text"
msgid "Cast Shadow to Bottom Right"
msgstr "Tieňovanie vpravo dole"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_SHADOW_STYLE_TOPRIGHT\n"
"string.text"
msgid "Cast Shadow to Top Right"
msgstr "Tieňovanie hore vpravo"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_SHADOW_STYLE_BOTTOMLEFT\n"
"string.text"
msgid "Cast Shadow to Bottom Left"
msgstr "Tieňovanie vľavo dole"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_SHADOW_STYLE_TOPLEFT\n"
"string.text"
msgid "Cast Shadow to Top Left"
msgstr "Tieňovanie hore vľavo"

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_FRAMEDIR_LTR\n"
"string.text"
msgid "Left-to-right (LTR)"
msgstr "Zľava doprava (LTR)"

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_FRAMEDIR_RTL\n"
"string.text"
msgid "Right-to-left (RTL)"
msgstr "Sprava doľava (RTL)"

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_FRAMEDIR_SUPER\n"
"string.text"
msgid "Use superordinate object settings"
msgstr "Použiť nastavenia nadriadeného objektu"

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_PAGEDIR_LTR_HORI\n"
"string.text"
msgid "Left-to-right (horizontal)"
msgstr "Zľava-doprava (horizontálne)"

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_PAGEDIR_RTL_HORI\n"
"string.text"
msgid "Right-to-left (horizontal)"
msgstr "Sprava-doľava (horizontálne)"

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_PAGEDIR_RTL_VERT\n"
"string.text"
msgid "Right-to-left (vertical)"
msgstr "Sprava doľava (vertikálne)"

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_PAGEDIR_LTR_VERT\n"
"string.text"
msgid "Left-to-right (vertical)"
msgstr "Zľava-doprava (vertikálne)"

#: paragrph.src
msgctxt ""
"paragrph.src\n"
"STR_EXAMPLE\n"
"string.text"
msgid "Example"
msgstr "Príklad"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_GRADIENT\n"
"string.text"
msgid "Please enter a name for the gradient:"
msgstr "Prosím zadajte názov prechodu:"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_GRADIENT\n"
"string.text"
msgid ""
"The gradient was modified without saving. \n"
"Modify the selected gradient or add a new gradient."
msgstr ""
"Prechod bol zmenený bez uloženia.\n"
"Zmente zvolený prechod alebo pridajte nový."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_NEW_BITMAP\n"
"string.text"
msgid "Please enter a name for the bitmap:"
msgstr "Prosím zadajte názov bitmapy:"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_EXT_BITMAP\n"
"string.text"
msgid "Please enter a name for the external bitmap:"
msgstr "Prosím zadajte názov externej bitmapy:"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_NEW_PATTERN\n"
"string.text"
msgid "Please enter a name for the pattern:"
msgstr "Prosím zadajte názov vzoru:"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_BITMAP\n"
"string.text"
msgid ""
"The bitmap was modified without saving. \n"
"Modify the selected bitmap or add a new bitmap."
msgstr ""
"Bitmapa bola zmenená bez uloženia. \n"
"Zmeňte zvolenú bitmapu alebo pridajte novú."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_PATTERN\n"
"string.text"
msgid ""
"The pattern was modified without saving. \n"
"Modify the selected pattern or add a new pattern"
msgstr ""
"Vzor bol zmenený bez uloženia.\n"
"Zmeňte zvolený vzor alebo pridajte nový."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_LINESTYLE\n"
"string.text"
msgid "Please enter a name for the line style:"
msgstr "Prosím zadajte názov štýlu čiary:"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_LINESTYLE\n"
"string.text"
msgid ""
"The line style was modified without saving. \n"
"Modify the selected line style or add a new line style."
msgstr ""
"Štýl čiary bol zmenený bez uloženia. \n"
"Zmeňte zvolený štýl čiary alebo pridajte nový."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_HATCH\n"
"string.text"
msgid "Please enter a name for the hatching:"
msgstr "Prosím zadajte názov šrafovania:"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_HATCH\n"
"string.text"
msgid ""
"The hatching type was modified but not saved. \n"
"Modify the selected hatching type or add a new hatching type."
msgstr ""
"Šrafovanie bolo zmenené bez uloženia. \n"
"Zmente zvolené šrafovanie alebo pridajte nové."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHANGE\n"
"string.text"
msgid "Modify"
msgstr "Upraviť"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ADD\n"
"string.text"
msgid "Add"
msgstr "Pridať"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_COLOR\n"
"string.text"
msgid "Please enter a name for the new color:"
msgstr "Prosím zadajte názov pre novú farbu:"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_COLOR\n"
"string.text"
msgid ""
"The color was modified without saving.\n"
"Modify the selected color or add a new color."
msgstr ""
"Farba bola zmenená bez uloženia.\n"
"Zmeňte zvolenú farbu alebo pridajte novú."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_TABLE\n"
"string.text"
msgid "Table"
msgstr "Tabuľka"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_LINEEND\n"
"string.text"
msgid "Please enter a name for the new arrowhead:"
msgstr "Vložte, prosím, názov novej šípky:"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_NOSTYLE\n"
"string.text"
msgid "No %1"
msgstr "Čís. %1"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_FAMILY\n"
"string.text"
msgid "Family"
msgstr "Rodina"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_FONT\n"
"string.text"
msgid "Font"
msgstr "Písmo"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_STYLE\n"
"string.text"
msgid "Style"
msgstr "Štýl"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_TYPEFACE\n"
"string.text"
msgid "Typeface"
msgstr "Typ písma"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_HIGHLIGHTING\n"
"string.text"
msgid "Highlight Color"
msgstr "Farba zvýraznenia"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_USE_REPLACE\n"
"string.text"
msgid "Use replacement table"
msgstr "Použiť tabuľku náhrad)"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CPTL_STT_WORD\n"
"string.text"
msgid "Correct TWo INitial CApitals"
msgstr "Opraviť prvé DVe VEľké písmená"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CPTL_STT_SENT\n"
"string.text"
msgid "Capitalize first letter of every sentence"
msgstr "Veľké písmeno po znakoch .!? a na začiatku textu"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_BOLD_UNDER\n"
"string.text"
msgid "Automatic *bold* and _underline_"
msgstr "Automaticky *tučné* a _podčiarknuté_"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_NO_DBL_SPACES\n"
"string.text"
msgid "Ignore double spaces"
msgstr "Ignorovať dvojité medzery"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DETECT_URL\n"
"string.text"
msgid "URL Recognition"
msgstr "Rozpoznávať URL"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DASH\n"
"string.text"
msgid "Replace dashes"
msgstr "Nahradiť pomlčky"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CORRECT_ACCIDENTAL_CAPS_LOCK\n"
"string.text"
msgid "Correct accidental use of cAPS LOCK key"
msgstr "Opraviť náhodné použitie klávesy cAPS LOCK"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_NON_BREAK_SPACE\n"
"string.text"
msgid "Add non-breaking space before specific punctuation marks in French text"
msgstr "Vo francúzskom texte pridávať pred určitá interpunkčné znamienka nezalomiteľnú medzeru"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ORDINAL\n"
"string.text"
msgid "Format ordinal numbers suffixes (1st -> 1^st)"
msgstr "Formátovať prípony anglických poradových čísiel (1st -> 1^st)"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DEL_EMPTY_PARA\n"
"string.text"
msgid "Remove blank paragraphs"
msgstr "Odstrániť prázdne odseky"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_USER_STYLE\n"
"string.text"
msgid "Replace Custom Styles"
msgstr "Nahradiť vlastné štýly"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_BULLET\n"
"string.text"
msgid "Replace bullets with: "
msgstr "Nahradiť odrážky -+* odrážkovým zoznamom so symbolom odrážok: "

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_RIGHT_MARGIN\n"
"string.text"
msgid "Combine single line paragraphs if length greater than"
msgstr "Zlúčiť jednoriadkové odseky ak je dĺžka väčšia ako"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_NUM\n"
"string.text"
msgid "Bulleted and numbered lists. Bullet symbol: "
msgstr "Číslované a odrážkové zoznamy. Symbol odrážky:"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_BORDER\n"
"string.text"
msgid "Apply border"
msgstr "Použiť orámovanie"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CREATE_TABLE\n"
"string.text"
msgid "Create table"
msgstr "Vytvoriť tabuľku po (viacnásobnom) zadaní sérií znakov ++,  +-+ alebo +TAB+"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_REPLACE_TEMPLATES\n"
"string.text"
msgid "Apply Styles"
msgstr "Použiť štýly"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DEL_SPACES_AT_STT_END\n"
"string.text"
msgid "Delete spaces and tabs at beginning and end of paragraph"
msgstr "Odstrániť medzery a tabulátory na začiatku a konci odseku"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DEL_SPACES_BETWEEN_LINES\n"
"string.text"
msgid "Delete spaces and tabs at end and start of line"
msgstr "Odstrániť medzery a tabulátory na začiatku a konci riadka"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CONNECTOR\n"
"string.text"
msgid "Connector"
msgstr "Spojnica"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DIMENSION_LINE\n"
"string.text"
msgid "Dimension line"
msgstr "Kótovacia čiara"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_READ_DATA_ERROR\n"
"string.text"
msgid "The file could not be loaded!"
msgstr "Súbor nemohol byť načítaný!"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_LOAD_ERROR\n"
"string.text"
msgid "The selected module could not be loaded."
msgstr "Vybraný modul sa nedá načítať."

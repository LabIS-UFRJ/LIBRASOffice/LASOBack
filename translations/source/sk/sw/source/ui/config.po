#. extracted from sw/source/ui/config
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-03-09 20:49+0100\n"
"PO-Revision-Date: 2017-01-19 14:21+0000\n"
"Last-Translator: Miloš Šrámek <msramek22@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1484835704.000000\n"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"ST_SCRIPT_ASIAN\n"
"string.text"
msgid "Asian"
msgstr "Ázijský"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"ST_SCRIPT_CTL\n"
"string.text"
msgid "CTL"
msgstr "Komplexné skripty"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"ST_SCRIPT_WESTERN\n"
"string.text"
msgid "Western"
msgstr "Západné"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PRODUCTNAME\n"
"string.text"
msgid "%PRODUCTNAME %s"
msgstr "%PRODUCTNAME %s"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_CONTENTS\n"
"string.text"
msgid "Contents"
msgstr "Obsah"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PAGE_BACKGROUND\n"
"string.text"
msgid "Page ba~ckground"
msgstr "Po~zadie strany"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PICTURES\n"
"string.text"
msgid "P~ictures and other graphic objects"
msgstr "O~brázky a iné grafické objekty"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_HIDDEN\n"
"string.text"
msgid "Hidden te~xt"
msgstr "Skrytý te~xt"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_TEXT_PLACEHOLDERS\n"
"string.text"
msgid "~Text placeholders"
msgstr "Zástu_pné ~znaky textu"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_FORM_CONTROLS\n"
"string.text"
msgid "Form control~s"
msgstr "O~vládacie prvky formulára"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_COLOR\n"
"string.text"
msgid "Color"
msgstr "Farba"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PRINT_BLACK\n"
"string.text"
msgid "Print text in blac~k"
msgstr "Text vy~tlačiť čiernou farbou"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PAGES_TEXT\n"
"string.text"
msgid "Pages"
msgstr "Strany"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PRINT_BLANK\n"
"string.text"
msgid "Print ~automatically inserted blank pages"
msgstr "Tlačiť ~automaticky vložené prázdne strany"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_ONLY_PAPER\n"
"string.text"
msgid "~Use only paper tray from printer preferences"
msgstr "~Použiť len zásobník papiera z nastavení tlačiarne"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PRINT\n"
"string.text"
msgid "Print"
msgstr "Tlačiť"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_NONE\n"
"string.text"
msgid "None (document only)"
msgstr "Bez poznámok (tlačiť iba dokument)"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_COMMENTS_ONLY\n"
"string.text"
msgid "Comments only"
msgstr "Len poznámky"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PLACE_END\n"
"string.text"
msgid "Place at end of document"
msgstr "Umiestniť na koniec dokumentu"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PLACE_PAGE\n"
"string.text"
msgid "Place at end of page"
msgstr "Umiestniť na koniec strany"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_COMMENTS\n"
"string.text"
msgid "~Comments"
msgstr "~Poznámky"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PAGE_SIDES\n"
"string.text"
msgid "Page sides"
msgstr "Strany stránok"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_ALL_PAGES\n"
"string.text"
msgid "All pages"
msgstr "Všetky strany"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_BACK_PAGES\n"
"string.text"
msgid "Back sides / left pages"
msgstr "Zadné strany / ľavé stránky"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_FONT_PAGES\n"
"string.text"
msgid "Front sides / right pages"
msgstr "Predné strany / pravé stránky"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_INCLUDE\n"
"string.text"
msgid "Include"
msgstr "Zahrnúť"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_BROCHURE\n"
"string.text"
msgid "Broch~ure"
msgstr "Brožú~ra"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_LEFT_SCRIPT\n"
"string.text"
msgid "Left-to-right script"
msgstr "Písmo zľava doprava"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_RIGHT_SCRIPT\n"
"string.text"
msgid "Right-to-left script"
msgstr "Písmo sprava doľava"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_RANGE_COPIES\n"
"string.text"
msgid "Range and copies"
msgstr "Rozsah a kópie"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_ALLPAGES\n"
"string.text"
msgid "~All pages"
msgstr "Všetky str~any"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_SOMEPAGES\n"
"string.text"
msgid "Pa~ges"
msgstr "Stra~ny"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_SELECTION\n"
"string.text"
msgid "~Selection"
msgstr "~Výber"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PLACE_MARGINS\n"
"string.text"
msgid "Place in margins"
msgstr "Umiestniť na okrajoch"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Millimeter\n"
"itemlist.text"
msgid "Millimeter"
msgstr "Milimeter"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Centimeter\n"
"itemlist.text"
msgid "Centimeter"
msgstr "Centimeter"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Meter\n"
"itemlist.text"
msgid "Meter"
msgstr "Meter"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Kilometer\n"
"itemlist.text"
msgid "Kilometer"
msgstr "Kilometer"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Inch\n"
"itemlist.text"
msgid "Inch"
msgstr "Palec"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Foot\n"
"itemlist.text"
msgid "Foot"
msgstr "Stopa"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Miles\n"
"itemlist.text"
msgid "Miles"
msgstr "Míle"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Pica\n"
"itemlist.text"
msgid "Pica"
msgstr "Pika"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Point\n"
"itemlist.text"
msgid "Point"
msgstr "Bod"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Char\n"
"itemlist.text"
msgid "Char"
msgstr "Znak"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Line\n"
"itemlist.text"
msgid "Line"
msgstr "Čiara"

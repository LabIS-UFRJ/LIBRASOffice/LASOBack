#. extracted from scp2/source/writer
msgid ""
msgstr ""
"Project-Id-Version: writer\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-11-20 13:01+0100\n"
"PO-Revision-Date: 2013-07-24 22:52+0300\n"
"Last-Translator: \n"
"Language-Team: <en@li.org>\n"
"Language: be\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"

#: folderitem_writer.ulf
msgctxt ""
"folderitem_writer.ulf\n"
"STR_FI_NAME_HTMLDOKUMENT\n"
"LngText.text"
msgid "HTML Document"
msgstr "Дакумент HTML"

#: folderitem_writer.ulf
msgctxt ""
"folderitem_writer.ulf\n"
"STR_FI_NAME_TEXTDOKUMENT\n"
"LngText.text"
msgid "Text Document"
msgstr "Тэкставы дакумент"

#: folderitem_writer.ulf
msgctxt ""
"folderitem_writer.ulf\n"
"STR_FI_TOOLTIP_WRITER\n"
"LngText.text"
msgid "Create and edit text and images in letters, reports, documents and Web pages by using Writer."
msgstr ""

#: module_writer.ulf
msgctxt ""
"module_writer.ulf\n"
"STR_NAME_MODULE_PRG_WRT\n"
"LngText.text"
msgid "%PRODUCTNAME Writer"
msgstr "%PRODUCTNAME Writer"

#: module_writer.ulf
msgctxt ""
"module_writer.ulf\n"
"STR_DESC_MODULE_PRG_WRT\n"
"LngText.text"
msgid "Create and edit text and images in letters, reports, documents and Web pages by using %PRODUCTNAME Writer."
msgstr ""

#: module_writer.ulf
msgctxt ""
"module_writer.ulf\n"
"STR_NAME_MODULE_PRG_WRT_BIN\n"
"LngText.text"
msgid "Program Module"
msgstr "Праграмны модуль"

#: module_writer.ulf
msgctxt ""
"module_writer.ulf\n"
"STR_DESC_MODULE_PRG_WRT_BIN\n"
"LngText.text"
msgid "The application %PRODUCTNAME Writer"
msgstr "Праграма %PRODUCTNAME Write"

#: module_writer.ulf
msgctxt ""
"module_writer.ulf\n"
"STR_NAME_MODULE_PRG_WRT_HELP\n"
"LngText.text"
msgid "%PRODUCTNAME Writer Help"
msgstr "Даведка %PRODUCTNAME Writer"

#: module_writer.ulf
msgctxt ""
"module_writer.ulf\n"
"STR_DESC_MODULE_PRG_WRT_HELP\n"
"LngText.text"
msgid "Help about %PRODUCTNAME Writer"
msgstr "Звесткі пра %PRODUCTNAME Writer"

#: module_writer.ulf
msgctxt ""
"module_writer.ulf\n"
"STR_NAME_MODULE_PRG_WRT_WRITER2LATEX\n"
"LngText.text"
msgid "LaTeX Export"
msgstr "Экспарт у LaTeX"

#: module_writer.ulf
msgctxt ""
"module_writer.ulf\n"
"STR_DESC_MODULE_PRG_WRT_WRITER2LATEX\n"
"LngText.text"
msgid "LaTeX export filter for Writer documents."
msgstr "Фільтар экспарту дакументаў Writer у LaTeX."

#: registryitem_writer.ulf
msgctxt ""
"registryitem_writer.ulf\n"
"STR_REG_VAL_NEW\n"
"LngText.text"
msgid "&New"
msgstr "Дадаць"

#: registryitem_writer.ulf
msgctxt ""
"registryitem_writer.ulf\n"
"STR_REG_VAL_SO60_TEXT\n"
"LngText.text"
msgid "%SXWFORMATNAME %SXWFORMATVERSION Text Document"
msgstr "%SXWFORMATNAME %SXWFORMATVERSION Text Document"

#: registryitem_writer.ulf
msgctxt ""
"registryitem_writer.ulf\n"
"STR_REG_VAL_SO60_MASTERDOC\n"
"LngText.text"
msgid "%SXWFORMATNAME %SXWFORMATVERSION Master Document"
msgstr "%SXWFORMATNAME %SXWFORMATVERSION Master Document"

#: registryitem_writer.ulf
msgctxt ""
"registryitem_writer.ulf\n"
"STR_REG_VAL_SO60_WRITER_TEMPLATE\n"
"LngText.text"
msgid "%SXWFORMATNAME %SXWFORMATVERSION Text Document Template"
msgstr "%SXWFORMATNAME %SXWFORMATVERSION Text Document Template"

#: registryitem_writer.ulf
msgctxt ""
"registryitem_writer.ulf\n"
"STR_REG_VAL_OO_WRITER\n"
"LngText.text"
msgid "OpenDocument Text"
msgstr "Тэкст OpenDocument"

#: registryitem_writer.ulf
msgctxt ""
"registryitem_writer.ulf\n"
"STR_REG_VAL_OO_WRITER_TEMPLATE\n"
"LngText.text"
msgid "OpenDocument Text Template"
msgstr "Шаблон тэксту OpenDocument"

#: registryitem_writer.ulf
msgctxt ""
"registryitem_writer.ulf\n"
"STR_REG_VAL_OO_MASTERDOC\n"
"LngText.text"
msgid "OpenDocument Master Document"
msgstr "Майстар-дакумент OpenDocument"

#: registryitem_writer.ulf
msgctxt ""
"registryitem_writer.ulf\n"
"STR_REG_VAL_OO_WEBDOC\n"
"LngText.text"
msgid "HTML Document Template"
msgstr "Шаблон дакументу HTML"

#: registryitem_writer.ulf
msgctxt ""
"registryitem_writer.ulf\n"
"STR_REG_VAL_MS_WORD_DOCUMENT_OLD\n"
"LngText.text"
msgid "Microsoft Word 97-2003 Document"
msgstr "Дакумент Microsoft Word 97-2003"

#: registryitem_writer.ulf
msgctxt ""
"registryitem_writer.ulf\n"
"STR_REG_VAL_MS_WORD_DOCUMENT\n"
"LngText.text"
msgid "Microsoft Word Document"
msgstr "Дакумент Microsoft Word"

#: registryitem_writer.ulf
msgctxt ""
"registryitem_writer.ulf\n"
"STR_REG_VAL_MS_WORD_TEMPLATE_OLD\n"
"LngText.text"
msgid "Microsoft Word 97-2003 Template"
msgstr "Шаблон Microsoft Word 97-2003"

#: registryitem_writer.ulf
msgctxt ""
"registryitem_writer.ulf\n"
"STR_REG_VAL_MS_WORD_TEMPLATE\n"
"LngText.text"
msgid "Microsoft Word Template"
msgstr "Шаблон Microsoft Word"

#: registryitem_writer.ulf
msgctxt ""
"registryitem_writer.ulf\n"
"STR_REG_VAL_MS_RTF_DOCUMENT\n"
"LngText.text"
msgid "Rich Text Document"
msgstr "Паўнатэкставы* дакумент"

#: registryitem_writer.ulf
msgctxt ""
"registryitem_writer.ulf\n"
"STR_REG_VAL_UNIFORM_OFFICE_FORMAT_TEXT\n"
"LngText.text"
msgid "Uniform Office Format Text Document"
msgstr ""

#: registryitem_writer.ulf
msgctxt ""
"registryitem_writer.ulf\n"
"STR_REG_VAL_WORDPRO_DOCUMENT\n"
"LngText.text"
msgid "Lotus Word Pro Document"
msgstr ""

#: registryitem_writer.ulf
msgctxt ""
"registryitem_writer.ulf\n"
"STR_REG_VAL_T602_TEXT_FILE\n"
"LngText.text"
msgid "T602 Text File"
msgstr ""

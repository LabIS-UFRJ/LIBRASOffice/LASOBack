#. extracted from svx/source/toolbars
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.freedesktop.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-05-23 12:06+0200\n"
"PO-Revision-Date: 2012-11-13 20:14+0000\n"
"Last-Translator: dwayne <dwayne@translate.org.za>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: af\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1352837663.0\n"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVX_EXTRUSION_BAR\n"
"string.text"
msgid "Extrusion"
msgstr "Ekstrusie"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_ON_OFF\n"
"string.text"
msgid "Apply Extrusion On/Off"
msgstr "Pas ekstrusie aan/af"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_ROTATE_DOWN\n"
"string.text"
msgid "Tilt Down"
msgstr "Kantel af"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_ROTATE_UP\n"
"string.text"
msgid "Tilt Up"
msgstr "Kantel op"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_ROTATE_LEFT\n"
"string.text"
msgid "Tilt Left"
msgstr "Kantel links"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_ROTATE_RIGHT\n"
"string.text"
msgid "Tilt Right"
msgstr "Kantel regs"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_DEPTH\n"
"string.text"
msgid "Change Extrusion Depth"
msgstr "Verander ekstrusiediepte"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_ORIENTATION\n"
"string.text"
msgid "Change Orientation"
msgstr "Verander gerigtheid"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_PROJECTION\n"
"string.text"
msgid "Change Projection Type"
msgstr "Verander projeksiesoort"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_LIGHTING\n"
"string.text"
msgid "Change Lighting"
msgstr "Verander beligting"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_BRIGHTNESS\n"
"string.text"
msgid "Change Brightness"
msgstr "Verander helderheid"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_SURFACE\n"
"string.text"
msgid "Change Extrusion Surface"
msgstr "Verander ekstrusieoppervlak"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_COLOR\n"
"string.text"
msgid "Change Extrusion Color"
msgstr "Verander ekstrusiekleur"

#: fontworkbar.src
msgctxt ""
"fontworkbar.src\n"
"RID_SVX_FONTWORK_BAR\n"
"string.text"
msgid "Fontwork"
msgstr "Fontwerk"

#: fontworkbar.src
msgctxt ""
"fontworkbar.src\n"
"RID_SVXSTR_UNDO_APPLY_FONTWORK_SHAPE\n"
"string.text"
msgid "Apply Fontwork Shape"
msgstr "Pas Fontwerk-vorm toe"

#: fontworkbar.src
msgctxt ""
"fontworkbar.src\n"
"RID_SVXSTR_UNDO_APPLY_FONTWORK_SAME_LETTER_HEIGHT\n"
"string.text"
msgid "Apply Fontwork Same Letter Heights"
msgstr "Pas Fontwerk-selfdeletterhoogtes toe"

#: fontworkbar.src
msgctxt ""
"fontworkbar.src\n"
"RID_SVXSTR_UNDO_APPLY_FONTWORK_ALIGNMENT\n"
"string.text"
msgid "Apply Fontwork Alignment"
msgstr "Pas Fontwerk-belyning toe"

#: fontworkbar.src
msgctxt ""
"fontworkbar.src\n"
"RID_SVXSTR_UNDO_APPLY_FONTWORK_CHARACTER_SPACING\n"
"string.text"
msgid "Apply Fontwork Character Spacing"
msgstr "Pas Fontwerk-karakterspasiëring toe"

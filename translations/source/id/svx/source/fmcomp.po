#. extracted from svx/source/fmcomp
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-03-09 20:48+0100\n"
"PO-Revision-Date: 2016-07-08 15:34+0000\n"
"Last-Translator: Andika Triwidada <andika@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.7\n"
"X-POOTLE-MTIME: 1467992084.000000\n"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_ROWS\n"
"SID_FM_DELETEROWS\n"
"menuitem.text"
msgid "Delete Rows"
msgstr "Hapus Baris"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_ROWS\n"
"SID_FM_RECORD_SAVE\n"
"menuitem.text"
msgid "Save Record"
msgstr "Simpan Data"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_ROWS\n"
"SID_FM_RECORD_UNDO\n"
"menuitem.text"
msgid "Undo: Data entry"
msgstr "Batalkan: Entri data"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_INSERTCOL\n"
"SID_FM_EDIT\n"
"menuitem.text"
msgid "Text Box"
msgstr "Kotak Teks"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_INSERTCOL\n"
"SID_FM_CHECKBOX\n"
"menuitem.text"
msgid "Check Box"
msgstr "Kotak Cek"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_INSERTCOL\n"
"SID_FM_COMBOBOX\n"
"menuitem.text"
msgid "Combo Box"
msgstr "Kotak Kombo"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_INSERTCOL\n"
"SID_FM_LISTBOX\n"
"menuitem.text"
msgid "List Box"
msgstr "Kotak Senarai"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_INSERTCOL\n"
"SID_FM_DATEFIELD\n"
"menuitem.text"
msgid "Date Field"
msgstr "Ruas Tanggal"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_INSERTCOL\n"
"SID_FM_TIMEFIELD\n"
"menuitem.text"
msgid "Time Field"
msgstr "Ruas Waktu"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_INSERTCOL\n"
"SID_FM_NUMERICFIELD\n"
"menuitem.text"
msgid "Numeric Field"
msgstr "Ruas Numerik"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_INSERTCOL\n"
"SID_FM_CURRENCYFIELD\n"
"menuitem.text"
msgid "Currency Field"
msgstr "Ruas Mata Uang"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_INSERTCOL\n"
"SID_FM_PATTERNFIELD\n"
"menuitem.text"
msgid "Pattern Field"
msgstr "Ruas Pola"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_INSERTCOL\n"
"SID_FM_FORMATTEDFIELD\n"
"menuitem.text"
msgid "Formatted Field"
msgstr "Ruas Berformat"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_INSERTCOL\n"
"SID_FM_TWOFIELDS_DATE_N_TIME\n"
"menuitem.text"
msgid "Date and Time Field"
msgstr "Ruas Tanggal dan Waktu"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS\n"
"SID_FM_INSERTCOL\n"
"menuitem.text"
msgid "Insert ~Column"
msgstr "Sisip ~Kolom"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS\n"
"SID_FM_CHANGECOL\n"
"menuitem.text"
msgid "~Replace with"
msgstr "~Ganti dengan"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS\n"
"SID_FM_DELETECOL\n"
"menuitem.text"
msgid "Delete Column"
msgstr "Hapus Kolom"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS\n"
"SID_FM_HIDECOL\n"
"menuitem.text"
msgid "~Hide Column"
msgstr "~Sembunyikan Kolom"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_SHOWCOLS\n"
"SID_FM_SHOWCOLS_MORE\n"
"menuitem.text"
msgid "~More..."
msgstr "~Lainnya..."

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_SHOWCOLS\n"
"SID_FM_SHOWALLCOLS\n"
"menuitem.text"
msgid "~All"
msgstr "Semu~a"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS\n"
"SID_FM_SHOWCOLS\n"
"menuitem.text"
msgid "~Show Columns"
msgstr "~Tampilkan Kolom"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS\n"
"SID_FM_SHOW_PROPERTY_BROWSER\n"
"menuitem.text"
msgid "Column..."
msgstr "Kolom..."

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_CELL\n"
"SID_COPY\n"
"menuitem.text"
msgid "~Copy"
msgstr "~Salin"

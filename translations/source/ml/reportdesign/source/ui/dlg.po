#. extracted from reportdesign/source/ui/dlg
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-03-11 14:25+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ml\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1457706343.000000\n"

#: CondFormat.src
msgctxt ""
"CondFormat.src\n"
"STR_NUMBERED_CONDITION\n"
"string.text"
msgid "Condition $number$"
msgstr "Condition $number$"

#: GroupsSorting.src
msgctxt ""
"GroupsSorting.src\n"
"STR_RPT_EXPRESSION\n"
"string.text"
msgid "Field/Expression"
msgstr "ഫീള്‍ഡ്/എക്സ്പ്രെഷന്‍"

#: GroupsSorting.src
msgctxt ""
"GroupsSorting.src\n"
"STR_RPT_SORTING\n"
"string.text"
msgid "Sort Order"
msgstr "ക്രമത്തിലാക്കേണ്ട രീതി"

#: GroupsSorting.src
msgctxt ""
"GroupsSorting.src\n"
"STR_RPT_PREFIXCHARS\n"
"string.text"
msgid "Prefix Characters"
msgstr "പ്രിഫിക്സ് ക്യാരക്ടര്‍"

#: GroupsSorting.src
msgctxt ""
"GroupsSorting.src\n"
"STR_RPT_YEAR\n"
"string.text"
msgid "Year"
msgstr "വര്‍ഷം"

#: GroupsSorting.src
msgctxt ""
"GroupsSorting.src\n"
"STR_RPT_QUARTER\n"
"string.text"
msgid "Quarter"
msgstr "ക്വാര്‍ട്ടര്‍‌"

#: GroupsSorting.src
msgctxt ""
"GroupsSorting.src\n"
"STR_RPT_MONTH\n"
"string.text"
msgid "Month"
msgstr "മാസം"

#: GroupsSorting.src
msgctxt ""
"GroupsSorting.src\n"
"STR_RPT_WEEK\n"
"string.text"
msgid "Week"
msgstr "ആഴ്ച"

#: GroupsSorting.src
msgctxt ""
"GroupsSorting.src\n"
"STR_RPT_DAY\n"
"string.text"
msgid "Day"
msgstr "ദിവസം"

#: GroupsSorting.src
msgctxt ""
"GroupsSorting.src\n"
"STR_RPT_HOUR\n"
"string.text"
msgid "Hour"
msgstr "മണിക്കൂര്‍"

#: GroupsSorting.src
msgctxt ""
"GroupsSorting.src\n"
"STR_RPT_MINUTE\n"
"string.text"
msgid "Minute"
msgstr "മിനിട്ട്"

#: GroupsSorting.src
msgctxt ""
"GroupsSorting.src\n"
"STR_RPT_INTERVAL\n"
"string.text"
msgid "Interval"
msgstr "ഇടവേള"

#: GroupsSorting.src
msgctxt ""
"GroupsSorting.src\n"
"STR_RPT_HELP_FIELD\n"
"string.text"
msgid "Select a field or type an expression to sort or group on."
msgstr "ഒരു ഫീള്‍ഡ് തെരഞ്ഞെടുക്കുക അല്ലെങ്കില്‍ ക്രമീകരിക്കുന്നതിനായി ഒരു എക്സ്പ്രെഷന്‍ നല്‍കുക."

#: GroupsSorting.src
msgctxt ""
"GroupsSorting.src\n"
"STR_RPT_HELP_HEADER\n"
"string.text"
msgid "Display a header for this group?"
msgstr "ഈ ഗ്രൂപ്പിനു് ഒരു തലക്കെട്ട് നല്‍കണമോ?"

#: GroupsSorting.src
msgctxt ""
"GroupsSorting.src\n"
"STR_RPT_HELP_FOOTER\n"
"string.text"
msgid "Display a footer for this group?"
msgstr "ഈ ഗ്രൂപ്പിനു് ഒരു അടിക്കുറിപ്പു് നല്‍കണമോ?"

#: GroupsSorting.src
msgctxt ""
"GroupsSorting.src\n"
"STR_RPT_HELP_GROUPON\n"
"string.text"
msgid "Select the value or range of values that starts a new group."
msgstr "ഒരു പുതിയ ഗ്രൂപ്പിനുള്ള മൂല്ല്യം അല്ലെങ്കില്‍ മൂല്ല്യ പരിധി തെരഞ്ഞെടുക്കുക."

#: GroupsSorting.src
msgctxt ""
"GroupsSorting.src\n"
"STR_RPT_HELP_INTERVAL\n"
"string.text"
msgid "Interval or number of characters to group on."
msgstr "Interval or number of characters to group on."

#: GroupsSorting.src
msgctxt ""
"GroupsSorting.src\n"
"STR_RPT_HELP_KEEP\n"
"string.text"
msgid "Keep group together on one page?"
msgstr "ഗ്രൂപ്പ് ഒന്നിച്ചു് ഒറ്റ താളില്‍ സൂക്ഷിക്കണമോ?"

#: GroupsSorting.src
msgctxt ""
"GroupsSorting.src\n"
"STR_RPT_HELP_SORT\n"
"string.text"
msgid "Select ascending or descending sort order. Ascending means from A to Z or 0 to 9"
msgstr "കുറവില്‍ നിന്നും കൂടുതല്‍ അല്ലെങ്കില്‍ കൂടുതലില്‍ നിന്നും കുറവു് എന്ന ക്രമം തെരഞ്ഞെടുക്കുക. A-Z അല്ലെങ്കില്‍ 0-9 എന്നതാണു് കുറവില്‍ നിന്നും കൂടുതല്‍ എന്ന ക്രമം."

#: GroupsSorting.src
#, fuzzy
msgctxt ""
"GroupsSorting.src\n"
"RID_GROUPSROWPOPUPMENU\n"
"SID_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr "~വെട്ടി നീക്കുക"

#: Navigator.src
msgctxt ""
"Navigator.src\n"
"RID_STR_FUNCTIONS\n"
"string.text"
msgid "Functions"
msgstr "ഫംഗ്ഷനുകള്‍"

#: Navigator.src
msgctxt ""
"Navigator.src\n"
"RID_STR_GROUPS\n"
"string.text"
msgid "Groups"
msgstr "ഗ്രൂപ്പുകള്‍"

#: Navigator.src
msgctxt ""
"Navigator.src\n"
"RID_MENU_NAVIGATOR\n"
"SID_SORTINGANDGROUPING\n"
"menuitem.text"
msgid "Sorting and Grouping..."
msgstr "ക്രമത്തിലാക്കലും ഗ്രൂപ്പിങ്ങും..."

#: Navigator.src
msgctxt ""
"Navigator.src\n"
"RID_MENU_NAVIGATOR\n"
"SID_PAGEHEADERFOOTER\n"
"menuitem.text"
msgid "Page Header/Footer..."
msgstr "താളിന്റെ ഹെഡര്‍/അടിക്കുറിപ്പു്..."

#: Navigator.src
msgctxt ""
"Navigator.src\n"
"RID_MENU_NAVIGATOR\n"
"SID_REPORTHEADERFOOTER\n"
"menuitem.text"
msgid "Report Header/Footer..."
msgstr "തലക്കെട്ട്/അടിക്കുറിപ്പു് രേഖപ്പെടുത്തുക..."

#: Navigator.src
msgctxt ""
"Navigator.src\n"
"RID_MENU_NAVIGATOR\n"
"SID_RPT_NEW_FUNCTION\n"
"menuitem.text"
msgid "New Function"
msgstr "പുതിയ ഫംഗ്ഷന്‍"

#: Navigator.src
msgctxt ""
"Navigator.src\n"
"RID_MENU_NAVIGATOR\n"
"SID_SHOW_PROPERTYBROWSER\n"
"menuitem.text"
msgid "Properties..."
msgstr "വിശേഷതകള്‍..."

#: Navigator.src
#, fuzzy
msgctxt ""
"Navigator.src\n"
"RID_MENU_NAVIGATOR\n"
"SID_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr "~വെട്ടി നീക്കുക"

#. The space after the word is no error. #PAGENUMBER# is a replacement and & must not be translated as well as "
#: PageNumber.src
msgctxt ""
"PageNumber.src\n"
"STR_RPT_PN_PAGE\n"
"string.text"
msgid "\"Page \" & #PAGENUMBER#"
msgstr "\"താള്‍ \" & #PAGENUMBER#"

#. The space before and after the word is no error. #PAGECOUNT# is a replacement and & must not be translated  as well as "
#: PageNumber.src
msgctxt ""
"PageNumber.src\n"
"STR_RPT_PN_PAGE_OF\n"
"string.text"
msgid " & \" of \" & #PAGECOUNT#"
msgstr " & \" / \" & #PAGECOUNT#"

#. extracted from svtools/source/dialogs
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2017-04-11 22:26+0200\n"
"PO-Revision-Date: 2016-01-19 19:15+0000\n"
"Last-Translator: Anish Sheela <aneesh.nl@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ml\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1453230918.000000\n"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_NO_FIELD_SELECTION\n"
"string.text"
msgid "<none>"
msgstr "ഒന്നുമില്ല"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_COMPANY\n"
"string.text"
msgid "Company"
msgstr "കന്പനി"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_DEPARTMENT\n"
"string.text"
msgid "Department"
msgstr "വിഭാഗം"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_FIRSTNAME\n"
"string.text"
msgid "First name"
msgstr "ആദ്യ നാമം"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_LASTNAME\n"
"string.text"
msgid "Last name"
msgstr "അന്തിമ നാമം"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_STREET\n"
"string.text"
msgid "Street"
msgstr "തെരുവ്"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_COUNTRY\n"
"string.text"
msgid "Country"
msgstr "ദേശം"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_ZIPCODE\n"
"string.text"
msgid "ZIP Code"
msgstr "Zip കോഡ്"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_CITY\n"
"string.text"
msgid "City"
msgstr "നഗരം"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_TITLE\n"
"string.text"
msgid "Title"
msgstr "ശീര്ഷകം"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_POSITION\n"
"string.text"
msgid "Position"
msgstr "സ്ഥാനം"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_ADDRFORM\n"
"string.text"
msgid "Addr. Form"
msgstr "മേല്വ‍ിലാസ ഫോം"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_INITIALS\n"
"string.text"
msgid "Initials"
msgstr "ഇനിഷ്യല്സ്"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_SALUTATION\n"
"string.text"
msgid "Complimentary close"
msgstr "ബഹുമാനസൂചകമായ"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_HOMETEL\n"
"string.text"
msgid "Tel: Home"
msgstr "ടെലിഫോണ് (വീട്)"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_WORKTEL\n"
"string.text"
msgid "Tel: Work"
msgstr "ടെലിഫോണ് (ജോലി)"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_FAX\n"
"string.text"
msgid "FAX"
msgstr "ഫാക്സ്"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_EMAIL\n"
"string.text"
msgid "E-mail"
msgstr "ഇ-മെയില്"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_URL\n"
"string.text"
msgid "URL"
msgstr "URL"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_NOTE\n"
"string.text"
msgid "Note"
msgstr "കുറിപ്പ്"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_USER1\n"
"string.text"
msgid "User 1"
msgstr "ഉപഭോക്താവ് 1"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_USER2\n"
"string.text"
msgid "User 2"
msgstr "ഉപഭോക്താവ് 2"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_USER3\n"
"string.text"
msgid "User 3"
msgstr "ഉപഭോക്താവ് 3 "

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_USER4\n"
"string.text"
msgid "User 4"
msgstr "ഉപഭോക്താവ് 4"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_ID\n"
"string.text"
msgid "ID"
msgstr "ID"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_STATE\n"
"string.text"
msgid "State"
msgstr "സംസ്ഥാനം"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_OFFICETEL\n"
"string.text"
msgid "Tel: Office"
msgstr "ടെലിഫോണ് (ആഫീസ്)"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_PAGER\n"
"string.text"
msgid "Pager"
msgstr "പേജറ്"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_MOBILE\n"
"string.text"
msgid "Mobile"
msgstr "മൊബൈല്"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_TELOTHER\n"
"string.text"
msgid "Tel: Other"
msgstr "ടെലിഫോണ് (മറ്റുള്ള)"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_CALENDAR\n"
"string.text"
msgid "Calendar"
msgstr "കലണ്ടര്"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_INVITE\n"
"string.text"
msgid "Invite"
msgstr "ക്ഷണിക്കുക"

#: filedlg2.src
msgctxt ""
"filedlg2.src\n"
"STR_FILEDLG_OPEN\n"
"string.text"
msgid "Open"
msgstr "തുറക്കുക"

#: filedlg2.src
msgctxt ""
"filedlg2.src\n"
"STR_FILEDLG_TYPE\n"
"string.text"
msgid "File ~type"
msgstr "ഫയല് വിഭാഗം"

#: filedlg2.src
msgctxt ""
"filedlg2.src\n"
"STR_FILEDLG_SAVE\n"
"string.text"
msgid "Save"
msgstr "സംഭരിക്കുക"

#: filedlg2.src
msgctxt ""
"filedlg2.src\n"
"STR_SVT_DEFAULT_SERVICE_LABEL\n"
"string.text"
msgid "$user$'s $service$"
msgstr ""

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_STRING\n"
"string.text"
msgid "Unformatted text"
msgstr "രൂപരേഖചെയ്യാത്ത ടെക്സ്റ്റ്"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_BITMAP\n"
"string.text"
msgid "Bitmap"
msgstr "ബിറ്റ്മാപ്"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_GDIMETAFILE\n"
"string.text"
msgid "GDI metafile"
msgstr "GDI മെറ്റാഫയല്"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_RTF\n"
"string.text"
msgid "Formatted text [RTF]"
msgstr "രൂപകല്പന ചെയ്ത ടെക്സ്റ്റ് [RTF]"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_RICHTEXT\n"
"string.text"
msgid "Formatted text [Richtext]"
msgstr ""

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_DRAWING\n"
"string.text"
msgid "Drawing format"
msgstr "ചിത്രരചന രൂപരേഖ"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_SVXB\n"
"string.text"
msgid "SVXB (StarView bitmap/animation)"
msgstr "SVXB (സ്റ്റാര് വീക്ഷണം ബിറ്മാപ്/ആനിമേഷന്)"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_INTERNALLINK_STATE\n"
"string.text"
msgid "Status Info from Svx Internal Link"
msgstr "Svx  ആന്തരിക ലിങ്കില് നിന്നുള്ള സ്ഥാനവിവരം"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_SOLK\n"
"string.text"
msgid "SOLK (%PRODUCTNAME Link)"
msgstr "SOLK (%PRODUCTNAME ലിങ്ക്)"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_NETSCAPE_BOOKMARK\n"
"string.text"
msgid "Netscape Bookmark"
msgstr "നെറ്റ്സ്കേപ് അടയാളം"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARSERVER\n"
"string.text"
msgid "Star server format"
msgstr "സ്റ്റാര് സെര്‍വര് രൂപരേഖ"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STAROBJECT\n"
"string.text"
msgid "Star object format"
msgstr "സ്റ്റാര്  വസ്തു രൂപരേഖ"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_APPLETOBJECT\n"
"string.text"
msgid "Applet object"
msgstr "അപ്പലെറ്റ്വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_PLUGIN_OBJECT\n"
"string.text"
msgid "Plug-in object"
msgstr "പ്ലഗ്-ഇന് വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARWRITER_30\n"
"string.text"
msgid "StarWriter 3.0 object"
msgstr "സ്റ്റാര് റൈറ്റര് 3.0 വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARWRITER_40\n"
"string.text"
msgid "StarWriter 4.0 object"
msgstr "സ്റ്റാര് റൈറ്റര് 4.0 വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARWRITER_50\n"
"string.text"
msgid "StarWriter 5.0 object"
msgstr "സ്റ്റാര് റൈറ്റര് 5.0 വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARWRITERWEB_40\n"
"string.text"
msgid "StarWriter/Web 4.0 object"
msgstr "സ്റ്റാര് റൈറ്റര്/ വെബ് 4.0 വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARWRITERWEB_50\n"
"string.text"
msgid "StarWriter/Web 5.0 object"
msgstr "സ്റ്റാര് റൈറ്റര്/ വെബ് 5.0 വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARWRITERGLOB_40\n"
"string.text"
msgid "StarWriter/Master 4.0 object"
msgstr "സ്റ്റാര് റൈറ്റര്/ മുഖ്യ 4.0 വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARWRITERGLOB_50\n"
"string.text"
msgid "StarWriter/Master 5.0 object"
msgstr "സ്റ്റാര് റൈറ്റര്/ മുഖ്യ 5.0 വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARDRAW\n"
"string.text"
msgid "StarDraw object"
msgstr "സ്റ്റാര് ചിത്രരചന  വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARDRAW_40\n"
"string.text"
msgid "StarDraw 4.0 object"
msgstr "സ്റ്റാര് ചിത്രരചന 4.0 വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARIMPRESS_50\n"
"string.text"
msgid "StarImpress 5.0 object"
msgstr "സ്റ്റാര് ഇംപ്രസ് 5.0 വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARDRAW_50\n"
"string.text"
msgid "StarDraw 5.0 object"
msgstr "സ്റ്റാര് ചിത്രരചന 5.0 വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARCALC\n"
"string.text"
msgid "StarCalc object"
msgstr "സ്റ്റാര് കാല്ക്  വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARCALC_40\n"
"string.text"
msgid "StarCalc  4.0 object"
msgstr "സ്റ്റാര് കാല്ക് 4.0 വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARCALC_50\n"
"string.text"
msgid "StarCalc 5.0 object"
msgstr "സ്റ്റാര് കാല്ക് 5.0  വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARCHART\n"
"string.text"
msgid "StarChart object"
msgstr "സ്റ്റാര് ചാര്ട്ട്  വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARCHART_40\n"
"string.text"
msgid "StarChart 4.0 object"
msgstr "സ്റ്റാര് ചാര്ട്ട്4.0  വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARCHART_50\n"
"string.text"
msgid "StarChart 5.0 object"
msgstr "StarChart 5.0 ഒബ്ജക്ട്"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARIMAGE\n"
"string.text"
msgid "StarImage object"
msgstr "സ്റ്റാര് ചിത്ര  വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARIMAGE_40\n"
"string.text"
msgid "StarImage 4.0 object"
msgstr "സ്റ്റാര് ചിത്ര  4.0 വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARIMAGE_50\n"
"string.text"
msgid "StarImage 5.0 object"
msgstr "സ്റ്റാര് ചിത്ര  5.0 വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARMATH\n"
"string.text"
msgid "StarMath object"
msgstr "സ്റ്റാര് മാത് വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARMATH_40\n"
"string.text"
msgid "StarMath 4.0 object"
msgstr "സ്റ്റാര് മാത് 4.0 വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARMATH_50\n"
"string.text"
msgid "StarMath 5.0 object"
msgstr "സ്റ്റാര് മാത് 5.0 വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STAROBJECT_PAINTDOC\n"
"string.text"
msgid "StarObject Paint object"
msgstr "സ്റ്റാര് വസ്തു സെറ്റ് വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_HTML\n"
"string.text"
msgid "HTML (HyperText Markup Language)"
msgstr "HTML (ഹൈപ്പര്ടെക്സ്റ്റ് മാര്ക്കപ്പ് ഭാഷ)"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_HTML_SIMPLE\n"
"string.text"
msgid "HTML format"
msgstr " HTML രൂപരേഖ"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_BIFF_5\n"
"string.text"
msgid "Biff5 (Microsoft Excel 5.0/95)"
msgstr "Biff5 (Microsoft Excel 5.0/95)"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_BIFF_8\n"
"string.text"
msgid "Biff8 (Microsoft Excel 97/2000/XP/2003)"
msgstr "Biff8 (Microsoft Excel 97/2000/XP/2003)"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_SYLK\n"
"string.text"
msgid "Sylk"
msgstr "Sylk"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_LINK\n"
"string.text"
msgid "DDE link"
msgstr "DDE ലിങ്ക്"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_DIF\n"
"string.text"
msgid "DIF"
msgstr "DIF"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_MSWORD_DOC\n"
"string.text"
msgid "Microsoft Word object"
msgstr "മൈക്രോസോഫ്റ്റ് വേഡ് വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STAR_FRAMESET_DOC\n"
"string.text"
msgid "StarFrameSet object"
msgstr "സ്റ്റാര് ഫ്രെയിം സെറ്റ് വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_OFFICE_DOC\n"
"string.text"
msgid "Office document object"
msgstr "ഓഫീസ് ഡോക്കുമെന്റ് വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_NOTES_DOCINFO\n"
"string.text"
msgid "Notes document info"
msgstr "ഡോക്കുമെന്റ് വിവരക്കുറിപ്പുകള്"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_SFX_DOC\n"
"string.text"
msgid "Sfx document"
msgstr "Sfx ഡോക്കുമെന്റ്"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARCHARTDOCUMENT_50\n"
"string.text"
msgid "StarChart 5.0 object"
msgstr "StarChart 5.0 ഒബ്ജക്ട്"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_GRAPHOBJ\n"
"string.text"
msgid "Graphic object"
msgstr "ഗ്രാഫിക് വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARWRITER_60\n"
"string.text"
msgid "OpenOffice.org 1.0 Writer object"
msgstr ""

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARWRITERWEB_60\n"
"string.text"
msgid "OpenOffice.org 1.0 Writer/Web object"
msgstr ""

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARWRITERGLOB_60\n"
"string.text"
msgid "OpenOffice.org 1.0 Writer/Master object"
msgstr ""

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARDRAW_60\n"
"string.text"
msgid "OpenOffice.org 1.0 Draw object"
msgstr ""

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARIMPRESS_60\n"
"string.text"
msgid "OpenOffice.org 1.0 Impress object"
msgstr ""

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARCALC_60\n"
"string.text"
msgid "OpenOffice.org 1.0 Calc object"
msgstr ""

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARCHART_60\n"
"string.text"
msgid "OpenOffice.org 1.0 Chart object"
msgstr ""

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARMATH_60\n"
"string.text"
msgid "OpenOffice.org 1.0 Math object"
msgstr ""

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_WMF\n"
"string.text"
msgid "Windows metafile"
msgstr "വിന്ഡോസ് മെറ്റാഫയല്"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_DBACCESS_QUERY\n"
"string.text"
msgid "Data source object"
msgstr "ഡേറ്റായുടെ ഉറവിട വസ്തു"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_DBACCESS_TABLE\n"
"string.text"
msgid "Data source table"
msgstr "ഡേറ്റാ ഉറവിട പട്ടിക"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_DBACCESS_COMMAND\n"
"string.text"
msgid "SQL query"
msgstr "അന്വേഷണം- SQL"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_DIALOG_60\n"
"string.text"
msgid "OpenOffice.org 1.0 dialog"
msgstr ""

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_FILEGRPDESCRIPTOR\n"
"string.text"
msgid "Link"
msgstr "ലിങ്ക്"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_HTML_NO_COMMENT\n"
"string.text"
msgid "HTML format without comments"
msgstr "HTML രൂപരേഖ കല്പനയില്ലാതെ"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_GENERALERROR&S_MAX\n"
"string.text"
msgid "General OLE error."
msgstr "സാധാരണ ഒഎല്‍ഇ പിശക്."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_CANT_BINDTOSOURCE&S_MAX\n"
"string.text"
msgid "The connection to the object cannot be established."
msgstr "വസ്തുവിലേക്ക് കണക്ഷന് സ്ഥാപിക്കാന് കഴിയുന്നില്ല"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_NOCACHE_UPDATED&S_MAX\n"
"string.text"
msgid "No cache files were updated."
msgstr "കാഷ് ഫയലുകള്‍ പരിഷ്കരിച്ചിട്ടില്ല."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_SOMECACHES_NOTUPDATED&S_MAX\n"
"string.text"
msgid "Some cache files were not updated."
msgstr "ചില കാഷ് ഫയലുകള്‍ പരിഷ്കരിച്ചിട്ടില്ല‌."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_UNAVAILABLE&S_MAX\n"
"string.text"
msgid "Status of object cannot be determined in a timely manner."
msgstr "ഒബ്ജക്ടിന്റെ അവസ്ഥ സമയത്തിനനുസരിച്ചു് ലഭ്യമാക്കുവാന്‍ സാധ്യമല്ല."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_E_CLASSDIFF&S_MAX\n"
"string.text"
msgid "Source of the OLE link has been converted."
msgstr "ഒഎല്‍ഇ ലിങ്കിന്റെ ഉറവിടം വേര്‍തിരിച്ചിരിക്കുന്നു."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_NO_OBJECT&S_MAX\n"
"string.text"
msgid "The object could not be found."
msgstr "വസ്തു കണ്ടെത്താനായില്ല"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_EXCEEDED_DEADLINE&S_MAX\n"
"string.text"
msgid "The process could not be completed within the specified time period."
msgstr "നിര്ദ്ദിഷ്ട സമയപരിധിക്കുള്ളില് പ്രവൃത്തി പൂര്ണ്ണമാക്കാന് കഴിയില്ല."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_CONNECT_MANUALLY&S_MAX\n"
"string.text"
msgid "OLE could not connect to a network device (server)."
msgstr "ഒഎല്‍ഇയ്ക്കു് ഒരു നെറ്റ്‌വര്‍ക്ക് ഡിവൈസിലേക്കു് (സര്‍വര്‍) കണക്ട് ചെയ്യുവാന്‍ സാധ്യമായില്ല."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_INTERMEDIATE_INTERFACE_NOT_SUPPORTED&S_MAX\n"
"string.text"
msgid "The object found does not support the interface required for the desired operation."
msgstr "ആവശ്യമുള്ള പ്രക്രിയയ്ക്കുള്ള ഇന്റര്‍ഫെയിസ് ഒബ്ജക്ട് പിന്തുണയ്ക്കുന്നില്ല."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_NO_INTERFACE&S_MAX\n"
"string.text"
msgid "Interface not supported."
msgstr "ഇന്റര്‍ഫെയിസിനുള്ള പിന്തുണ ലഭ്യമല്ല."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_OUT_OF_MEMORY&S_MAX\n"
"string.text"
msgid "Insufficient memory."
msgstr "മതിയായ മെമ്മറി ലഭ്യമല്ല."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_SYNTAX&S_MAX\n"
"string.text"
msgid "The connection name could not be processed."
msgstr "കണക്ഷന് നാമം പ്രോസ്സസ് ചെയ്യാന് കഴിയില്ല"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_REDUCED_TO_SELF&S_MAX\n"
"string.text"
msgid "The connection name could not be reduced further."
msgstr "കണക്ഷനുള്ള പേരു് ഇനി ചെറുതാക്കുവാന്‍ സാധ്യമല്ല."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_NO_INVERSE&S_MAX\n"
"string.text"
msgid "The connection name has no inverse."
msgstr "കണക്ഷന് നാമത്തിന‍് ഇന്‍വേഴ്സ് ഇല്ല"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_NO_PREFIX&S_MAX\n"
"string.text"
msgid "No common prefix exists."
msgstr "പൊതുവായ പ്രിഫിക്സ് ലഭ്യമല്ല."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_HIM&S_MAX\n"
"string.text"
msgid "The connection name is contained in the other one."
msgstr "കണക്ഷനുള്ള പേരു് മറ്റൊന്നിലുണ്ടു്."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_US&S_MAX\n"
"string.text"
msgid "The connection names (the receiver and the other moniker) are identical."
msgstr "സമാനമായ കണക്ഷന് പേരുകള് (കൈപ്പറ്റുന്നളിന്റെയും മറ്റുള്ള മോണിക്കറുടെയും) ."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_ME&S_MAX\n"
"string.text"
msgid "The connection name is contained in the other one."
msgstr "കണക്ഷനുള്ള പേരു് മറ്റൊന്നിലുണ്ടു്."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_NOT_BINDABLE&S_MAX\n"
"string.text"
msgid "The connection name cannot be connected. This is a relative name."
msgstr "കണക്ഷന്റെ പേര‍് കണക്ട് ചെയ്യാന് കഴിയുന്നില്ല. ഇതൊരു ബന്ധപ്പെട്ട പേരാണ‍്"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_NOT_IMPLEMENTED&S_MAX\n"
"string.text"
msgid "Operation not implemented."
msgstr "പ്രക്രിയ നടത്തുവാന്‍ സാധ്യമല്ല."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_NO_STORAGE&S_MAX\n"
"string.text"
msgid "No storage."
msgstr "സ്റ്റോറേജ് ലഭ്യമല്ല."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_FALSE&S_MAX\n"
"string.text"
msgid "False."
msgstr "False."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_NEED_GENERIC&S_MAX\n"
"string.text"
msgid "Monikers must be composed generically."
msgstr "Monikers സാധാരണ രീതിയില്‍ സജ്ജമാക്കേണ്ടതാണു്."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_PENDING&S_MAX\n"
"string.text"
msgid "Data not available at this time."
msgstr "നിലവില്‍ ഡേറ്റാ ലഭ്യമല്ല."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_NOT_INPLACEACTIVE & S_MAX\n"
"string.text"
msgid "Object could not be activated InPlace."
msgstr "ഒബ്ജക്ട് InPlace-ല്‍ സജീവമാക്കുവാന്‍ സാധ്യമായില്ല."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_LINDEX & S_MAX\n"
"string.text"
msgid "Invalid index."
msgstr "തെറ്റായ സൂചിക."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_CANNOT_DOVERB_NOW & S_MAX\n"
"string.text"
msgid "The action cannot be executed in the object's current state."
msgstr "വസ്തുവിന്റെ ഇപ്പോഴത്തെ അവസ്ഥയില്  പ്രവൃത്തി ചെയ്യാന് കഴിയുന്നില്ല"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_OLEOBJ_INVALIDHWND & S_MAX\n"
"string.text"
msgid "An invalid window was passed when activated."
msgstr "പ്രവര്ത്തനസജ്ജമാക്കിയപ്പോള് ആസാധുവായ ഒരു വിന്ഡോ കടന്നുപോയി"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_NOVERBS & S_MAX\n"
"string.text"
msgid "The object does not support any actions."
msgstr "ഒബ്ജക്ട് ഒരു പ്രവര്‍ത്തനങ്ങളും പിന്തുണയ്ക്കുന്നില്ല."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_INVALIDVERB & S_MAX\n"
"string.text"
msgid "The action is not defined. The default action will be executed."
msgstr "പ്രവൃത്തി നിര്‍വചിച്ചിട്ടില്ല. സംസ്ഥാപിത പ്രവൃത്തി പ്രവര്ത്തിക്കും"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_CONNECT & S_MAX\n"
"string.text"
msgid "A link to the network could not be re-established."
msgstr "നെറ്റ്വര്ക്കിലേക്കുള്ള ലിങ്ക് പുനഃസ്ഥാപിക്കാന് കഴിയുന്നില്ല."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_NOTIMPL & S_MAX\n"
"string.text"
msgid "Object does not support this action."
msgstr "ഒബ്ജക്ട് ഈ പ്രവര്‍ത്ത പിന്തുണയ്ക്കുന്നില്ല."

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_CANTOPENFILE & S_MAX\n"
"string.text"
msgid "The specified file could not be opened."
msgstr "നിര്ദ്ദിഷ്ട  ഫയല് തുറക്കാന് കഴിയുന്നില്ല"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERRCTX\n"
"ERRCTX_SO_DOVERB\n"
"string.text"
msgid "$(ERR) activating object"
msgstr "$(ERR)  പ്രവര്ത്തിപ്പിക്കുന്നതുള്ള  വസ്തു."

#: so3res.src
msgctxt ""
"so3res.src\n"
"STR_ERROR_OBJNOCREATE\n"
"string.text"
msgid "Object % could not be inserted."
msgstr "വസ്തു % ചേക്കാന് കഴിയുന്നില്ല"

#: so3res.src
msgctxt ""
"so3res.src\n"
"STR_ERROR_OBJNOCREATE_FROM_FILE\n"
"string.text"
msgid "Object from file % could not be inserted."
msgstr "ഫയലില്%  നിന്നുള്ള വസ്തു ചേര്ക്കാന് കഴിയില്ല"

#: so3res.src
msgctxt ""
"so3res.src\n"
"STR_ERROR_OBJNOCREATE_PLUGIN\n"
"string.text"
msgid "Plug-in from document % could not be inserted."
msgstr "ഡോക്കുമെന്റ് %  പ്ലഗ്-ഇന് ചേര്ക്കാന് കഴിയില്ല"

#: so3res.src
msgctxt ""
"so3res.src\n"
"STR_FURTHER_OBJECT\n"
"string.text"
msgid "Further objects"
msgstr "കൂടുതല് വസ്തുക്കള്"

#: so3res.src
msgctxt ""
"so3res.src\n"
"STR_UNKNOWN_SOURCE\n"
"string.text"
msgid "Unknown source"
msgstr "അജ്ഞാതം ഉറവിടം"

#: wizardmachine.src
msgctxt ""
"wizardmachine.src\n"
"STR_WIZDLG_FINISH\n"
"string.text"
msgid "~Finish"
msgstr "സമാപ്തി"

#: wizardmachine.src
msgctxt ""
"wizardmachine.src\n"
"STR_WIZDLG_NEXT\n"
"string.text"
msgid "~Next >>"
msgstr "അടുത്ത"

#: wizardmachine.src
msgctxt ""
"wizardmachine.src\n"
"STR_WIZDLG_PREVIOUS\n"
"string.text"
msgid "<< Bac~k"
msgstr "<<പിന്നോട്ട്"

#: wizardmachine.src
msgctxt ""
"wizardmachine.src\n"
"STR_WIZDLG_ROADMAP_TITLE\n"
"string.text"
msgid "Steps"
msgstr "രീതികള്"

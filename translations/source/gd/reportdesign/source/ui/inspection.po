#. extracted from reportdesign/source/ui/inspection
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-12-24 01:50+0000\n"
"Last-Translator: Michael Bauer <fios@akerbeltz.org>\n"
"Language-Team: Akerbeltz\n"
"Language: gd\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 || n==11) ? 0 : (n==2 || n==12) ? 1 : (n > 2 && n < 20) ? 2 : 3;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1482544231.000000\n"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PROPPAGE_DEFAULT\n"
"string.text"
msgid "General"
msgstr "Coitcheann"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PROPPAGE_DATA\n"
"string.text"
msgid "Data"
msgstr "Dàta"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_BOOL\n"
"No\n"
"itemlist.text"
msgid "No"
msgstr "Chan eil"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_BOOL\n"
"Yes\n"
"itemlist.text"
msgid "Yes"
msgstr "Tha"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_FORCENEWPAGE\n"
"string.text"
msgid "Force New Page"
msgstr "Co-èignich duilleag ùr"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_FORCENEWPAGE_CONST\n"
"None\n"
"itemlist.text"
msgid "None"
msgstr "Chan eil gin"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_FORCENEWPAGE_CONST\n"
"Before Section\n"
"itemlist.text"
msgid "Before Section"
msgstr "Ron earrann"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_FORCENEWPAGE_CONST\n"
"After Section\n"
"itemlist.text"
msgid "After Section"
msgstr "An dèidh na h-earrainn"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_FORCENEWPAGE_CONST\n"
"Before & After Section\n"
"itemlist.text"
msgid "Before & After Section"
msgstr "Ron earrann ⁊ ’na dèidh"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_NEWROWORCOL\n"
"string.text"
msgid "New Row Or Column"
msgstr "Ràgh no colbh ùr"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_KEEPTOGETHER\n"
"string.text"
msgid "Keep Together"
msgstr "Cum còmhla"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_KEEPTOGETHER_CONST\n"
"No\n"
"itemlist.text"
msgid "No"
msgstr "Chan eil"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_KEEPTOGETHER_CONST\n"
"Whole Group\n"
"itemlist.text"
msgid "Whole Group"
msgstr "Am buidheann gu lèir"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_KEEPTOGETHER_CONST\n"
"With First Detail\n"
"itemlist.text"
msgid "With First Detail"
msgstr "Leis a’ chiad mhion-fhios"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_CANGROW\n"
"string.text"
msgid "Can Grow"
msgstr "'S urrainn dha a dhol am meud"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_CANSHRINK\n"
"string.text"
msgid "Can Shrink"
msgstr "'S urrainn dha lùghdachadh"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_REPEATSECTION\n"
"string.text"
msgid "Repeat Section"
msgstr "Ath-dhèan an earrann"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PRINTREPEATEDVALUES\n"
"string.text"
msgid "Print repeated values"
msgstr "Clò-bhuail luachan a tha ag ath-nochdadh"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_CONDITIONALPRINTEXPRESSION\n"
"string.text"
msgid "Conditional Print Expression"
msgstr "Eas-preisean clò-bhualaidh cumhach"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_STARTNEWCOLUMN\n"
"string.text"
msgid "Start new column"
msgstr "Tòisich air colbh ùr"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_STARTNEWPAGE\n"
"string.text"
msgid "Start new page"
msgstr "Tòisich air duilleag ùr"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_RESETPAGENUMBER\n"
"string.text"
msgid "Reset page number"
msgstr "Ath-shuidhich àireamh na duilleige"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_CHARTTYPE\n"
"string.text"
msgid "Chart type"
msgstr "Seòrsa na cairte"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PRINTWHENGROUPCHANGE\n"
"string.text"
msgid "Print repeated value on group change"
msgstr "Clò-bhuail an luach ath-chùrsach ri linn atharrachadh buidhinn"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_VISIBLE\n"
"string.text"
msgid "Visible"
msgstr "Ri fhaicinn"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_GROUPKEEPTOGETHER\n"
"string.text"
msgid "Group keep together"
msgstr "Cum am buidheann còmhla"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_GROUPKEEPTOGETHER_CONST\n"
"Per Page\n"
"itemlist.text"
msgid "Per Page"
msgstr "An duilleag"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_GROUPKEEPTOGETHER_CONST\n"
"Per Column\n"
"itemlist.text"
msgid "Per Column"
msgstr "An colbh"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_SECTIONPAGEBREAK_CONST\n"
"None\n"
"itemlist.text"
msgid "None"
msgstr "Chan eil gin"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_SECTIONPAGEBREAK_CONST\n"
"Section\n"
"itemlist.text"
msgid "Section"
msgstr "Earrann"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_SECTIONPAGEBREAK_CONST\n"
"Automatic\n"
"itemlist.text"
msgid "Automatic"
msgstr "Fèin-obrachail"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PAGEHEADEROPTION\n"
"string.text"
msgid "Page header"
msgstr "Bann-cinn na duilleige"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PAGEFOOTEROPTION\n"
"string.text"
msgid "Page footer"
msgstr "Bann-coise na duilleige"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_REPORTPRINTOPTION_CONST\n"
"All Pages\n"
"itemlist.text"
msgid "All Pages"
msgstr "Gach duilleag"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_REPORTPRINTOPTION_CONST\n"
"Not With Report Header\n"
"itemlist.text"
msgid "Not With Report Header"
msgstr "Gun bhann-cinn na h-aithisge"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_REPORTPRINTOPTION_CONST\n"
"Not With Report Footer\n"
"itemlist.text"
msgid "Not With Report Footer"
msgstr "Gun bhann-coise na h-aithisge"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_REPORTPRINTOPTION_CONST\n"
"Not With Report Header/Footer\n"
"itemlist.text"
msgid "Not With Report Header/Footer"
msgstr "Gun bhann-cinn/-coise na h-aithisge"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_DEEPTRAVERSING\n"
"string.text"
msgid "Deep traversing"
msgstr "Seall fo-aithisgean"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PREEVALUATED\n"
"string.text"
msgid "Pre evaluation"
msgstr "Ro-luachadh"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_POSITIONX\n"
"string.text"
msgid "Position X"
msgstr "Ionad X"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_POSITIONY\n"
"string.text"
msgid "Position Y"
msgstr "Ionad Y"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_WIDTH\n"
"string.text"
msgid "Width"
msgstr "Leud"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_HEIGHT\n"
"string.text"
msgid "Height"
msgstr "Àirde"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_INITIALFORMULA\n"
"string.text"
msgid "Initial value"
msgstr "Luach tòiseachaidh"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PRESERVEIRI\n"
"string.text"
msgid "Preserve as Link"
msgstr "Glèidh mar cheangal"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_FORMULA\n"
"string.text"
msgid "Formula"
msgstr "Foirmle"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_DATAFIELD\n"
"string.text"
msgid "Data field"
msgstr "Raon dàta"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_FONT\n"
"string.text"
msgid "Font"
msgstr "Cruth-clò"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_BACKCOLOR\n"
"string.text"
msgid "Background color"
msgstr "Dath a’ chùlaibh"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_BACKTRANSPARENT\n"
"string.text"
msgid "Background Transparent"
msgstr "Cùlaibh trìd-shoilleir"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_CONTROLBACKGROUNDTRANSPARENT\n"
"string.text"
msgid "Background Transparent"
msgstr "Cùlaibh trìd-shoilleir"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_OVERLAP_OTHER_CONTROL\n"
"string.text"
msgid "This operation is not allowed. The control overlaps with another one."
msgstr "Chan eil an gnìomh seo ceadaichte. Tha dà uidheam-smachd a' dol thairi air a chèile."

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_ILLEGAL_POSITION\n"
"string.text"
msgid "This position can not be set. It is invalid."
msgstr "Cha ghabh an t-ionad seo a shuidheachadh. Tha e mì-dhligheach."

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_SCOPE_GROUP\n"
"string.text"
msgid "Group: %1"
msgstr "Buidheann: %1"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_FORMULALIST\n"
"string.text"
msgid "Function"
msgstr "Foincsean"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_SCOPE\n"
"string.text"
msgid "Scope"
msgstr "Sgòp"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_TYPE\n"
"string.text"
msgid "Data Field Type"
msgstr "Seòrsa an raoin dàta"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_TYPE_CONST\n"
"Field or Formula\n"
"itemlist.text"
msgid "Field or Formula"
msgstr "Raon no foirmle"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_TYPE_CONST\n"
"Function\n"
"itemlist.text"
msgid "Function"
msgstr "Foincsean"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_TYPE_CONST\n"
"Counter\n"
"itemlist.text"
msgid "Counter"
msgstr "Cunntair"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_TYPE_CONST\n"
"User defined Function\n"
"itemlist.text"
msgid "User defined Function"
msgstr "Foincsean air a shònrachadh le cleachdaiche"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_MASTERFIELDS\n"
"string.text"
msgid "Link master fields"
msgstr "Ceangail na prìomh-raointean"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_DETAILFIELDS\n"
"string.text"
msgid "Link slave fields"
msgstr "Ceangail na raointean tràille"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_EXPLANATION\n"
"string.text"
msgid "Charts can be used to display detailed data about the current record of the report. To do this, you can specify which columns in the chart match which columns in the report."
msgstr "'S urrainn dhut cairtean a chleachdadh gus dàta mionaideach a shealltainn mu reacord làithreach na h-aithisge. Gus seo a dhèanamh, 's urrainn dhut sònrachadh dè na colbhan sa chairt a tha a' freagairt ris na colbhan san aithisg."

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_DETAILLABEL\n"
"string.text"
msgid "Chart"
msgstr "Cairt"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_MASTERLABEL\n"
"string.text"
msgid "Report"
msgstr "Aithisg"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PREVIEW_COUNT\n"
"string.text"
msgid "Preview Row(s)"
msgstr "Ro-sheall na ràghan"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_AREA\n"
"string.text"
msgid "Area"
msgstr "Àrainn"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_MIMETYPE\n"
"string.text"
msgid "Report Output Format"
msgstr "Fòrmat às-chur na h-aithisge"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_VERTICALALIGN\n"
"string.text"
msgid "Vert. Alignment"
msgstr "Co-thaobhadh inghearach"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_VERTICAL_ALIGN_CONST\n"
"Top\n"
"itemlist.text"
msgid "Top"
msgstr "Barr"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_VERTICAL_ALIGN_CONST\n"
"Middle\n"
"itemlist.text"
msgid "Middle"
msgstr "Meadhan"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_VERTICAL_ALIGN_CONST\n"
"Bottom\n"
"itemlist.text"
msgid "Bottom"
msgstr "Bonn"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PARAADJUST\n"
"string.text"
msgid "Horz. Alignment"
msgstr "Co-thaobhadh còmhnard"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PARAADJUST_CONST\n"
"Left\n"
"itemlist.text"
msgid "Left"
msgstr "Clì"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PARAADJUST_CONST\n"
"Right\n"
"itemlist.text"
msgid "Right"
msgstr "Deas"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PARAADJUST_CONST\n"
"Block\n"
"itemlist.text"
msgid "Block"
msgstr "Bloca"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PARAADJUST_CONST\n"
"Center\n"
"itemlist.text"
msgid "Center"
msgstr "Meadhan"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_F_COUNTER\n"
"string.text"
msgid "Counter"
msgstr "Cunntair"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_F_ACCUMULATION\n"
"string.text"
msgid "Accumulation"
msgstr "Co-chàrnadh"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_F_MINIMUM\n"
"string.text"
msgid "Minimum"
msgstr "Meud as lugha"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_F_MAXIMUM\n"
"string.text"
msgid "Maximum"
msgstr "Meud as motha"

#. extracted from sw/source/ui/index
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-07-04 23:21+0000\n"
"Last-Translator: Michael Bauer <fios@akerbeltz.org>\n"
"Language-Team: Akerbeltz\n"
"Language: gd\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 || n==11) ? 0 : (n==2 || n==12) ? 1 : (n > 2 && n < 20) ? 2 : 3;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-Project-Style: openoffice\n"
"X-POOTLE-MTIME: 1467674498.000000\n"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_TITLE\n"
"string.text"
msgid "Title"
msgstr "Tiotal"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_ALPHA\n"
"string.text"
msgid "Separator"
msgstr "Sgaradair"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_LEVEL\n"
"string.text"
msgid "Level "
msgstr "Leibheil "

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_FILE_NOT_FOUND\n"
"string.text"
msgid "The file, \"%1\" in the \"%2\" path could not be found."
msgstr "Cha deach am faidhle \"%1\" san t-slighe \"%2\" a lorg."

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_USER_DEFINED_INDEX\n"
"string.text"
msgid "User-Defined Index"
msgstr "Clàr-amais air a shònrachadh le cleachdaiche"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_NOSORTKEY\n"
"string.text"
msgid "<None>"
msgstr "<Chan eil gin>"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_NO_CHAR_STYLE\n"
"string.text"
msgid "<None>"
msgstr "<Chan eil gin>"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_DELIM\n"
"string.text"
msgid "S"
msgstr "S"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_TOKEN_ENTRY_NO\n"
"string.text"
msgid "E#"
msgstr "E#"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_TOKEN_ENTRY\n"
"string.text"
msgid "E"
msgstr "E"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_TOKEN_TAB_STOP\n"
"string.text"
msgid "T"
msgstr "T"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_TOKEN_PAGE_NUMS\n"
"string.text"
msgid "#"
msgstr "#"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_TOKEN_CHAPTER_INFO\n"
"string.text"
msgid "CI"
msgstr "CI"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_TOKEN_LINK_START\n"
"string.text"
msgid "LS"
msgstr "LS"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_TOKEN_LINK_END\n"
"string.text"
msgid "LE"
msgstr "LE"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_TOKEN_AUTHORITY\n"
"string.text"
msgid "A"
msgstr "A"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_TOKEN_HELP_ENTRY_NO\n"
"string.text"
msgid "Chapter number"
msgstr "Àireamh a' chaibidil"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_TOKEN_HELP_ENTRY\n"
"string.text"
msgid "Entry"
msgstr "Innteart"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_TOKEN_HELP_TAB_STOP\n"
"string.text"
msgid "Tab stop"
msgstr "Taba-stad"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_TOKEN_HELP_TEXT\n"
"string.text"
msgid "Text"
msgstr "Teacsa"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_TOKEN_HELP_PAGE_NUMS\n"
"string.text"
msgid "Page number"
msgstr "Àireamh na duilleige"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_TOKEN_HELP_CHAPTER_INFO\n"
"string.text"
msgid "Chapter info"
msgstr "Fiosrachadh a' chaibidil"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_TOKEN_HELP_LINK_START\n"
"string.text"
msgid "Hyperlink start"
msgstr "Toiseach a' cheangail-lìn"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_TOKEN_HELP_LINK_END\n"
"string.text"
msgid "Hyperlink end"
msgstr "Deireadh a' cheangail-lìn"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_TOKEN_HELP_AUTHORITY\n"
"string.text"
msgid "Bibliography entry: "
msgstr "Innteart ann an liosta nan tùsan: "

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_CHARSTYLE\n"
"string.text"
msgid "Character Style: "
msgstr "Stoidhle a' charactair: "

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"RES_SRCTYPES\n"
"%PRODUCTNAME Math\n"
"itemlist.text"
msgid "%PRODUCTNAME Math"
msgstr ""

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"RES_SRCTYPES\n"
"%PRODUCTNAME Chart\n"
"itemlist.text"
msgid "%PRODUCTNAME Chart"
msgstr ""

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"RES_SRCTYPES\n"
"%PRODUCTNAME Calc\n"
"itemlist.text"
msgid "%PRODUCTNAME Calc"
msgstr ""

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"RES_SRCTYPES\n"
"%PRODUCTNAME Draw/%PRODUCTNAME Impress\n"
"itemlist.text"
msgid "%PRODUCTNAME Draw/%PRODUCTNAME Impress"
msgstr ""

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"RES_SRCTYPES\n"
"Other OLE Objects\n"
"itemlist.text"
msgid "Other OLE Objects"
msgstr ""

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_STRUCTURE\n"
"string.text"
msgid "Structure text"
msgstr "Cuir structar air an teacsa"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_ADDITIONAL_ACCNAME_STRING1\n"
"string.text"
msgid "Press Ctrl+Alt+A to move focus for more operations"
msgstr "Brùth air Ctrl+Alt+A gus am fòcas a gluasad airson barrachd ghnìomhan"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_ADDITIONAL_ACCNAME_STRING2\n"
"string.text"
msgid "Press left or right arrow to choose the structure controls"
msgstr "Brùth an t-saighead chlì no dheas gus na h-uidheaman-smachd structair a thaghadh"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_ADDITIONAL_ACCNAME_STRING3\n"
"string.text"
msgid "Press Ctrl+Alt+B to move focus back to the current structure control"
msgstr "Brùth Ctrl+Alt+B gus am fòcas a ghluasad air ais dhan uidheam-smachd structair làithreach"

#: cnttab.src
msgctxt ""
"cnttab.src\n"
"STR_AUTOMARK_TYPE\n"
"string.text"
msgid "Selection file for the alphabetical index (*.sdi)"
msgstr "Am faidhle taghaidh airson a' chlàir-amais aibidilich (*.sdi)"

#: idxmrk.src
msgctxt ""
"idxmrk.src\n"
"STR_IDXMRK_EDIT\n"
"string.text"
msgid "Edit Index Entry"
msgstr "Deasaich innteart a' chlàir-amais"

#: idxmrk.src
msgctxt ""
"idxmrk.src\n"
"STR_IDXMRK_INSERT\n"
"string.text"
msgid "Insert Index Entry"
msgstr "Cuir a-steach innteart a' chlàir-amais"

#: idxmrk.src
msgctxt ""
"idxmrk.src\n"
"STR_QUERY_CHANGE_AUTH_ENTRY\n"
"string.text"
msgid "The document already contains the bibliography entry but with different data. Do you want to adjust the existing entries?"
msgstr "Tha an t-innteart seo ann an tùsan na sgrìobhainne seo mu thràth ach le dàta eadar-dhealaichte. A bheil thu ag iarraidh na h-innteartan làithreach a chur air gleus?"

#. extracted from extensions/uiconfig/sabpilot/ui
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-11-10 19:33+0100\n"
"PO-Revision-Date: 2016-04-07 22:30+0000\n"
"Last-Translator: Michael Bauer <fios@akerbeltz.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: gd\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 || n==11) ? 0 : (n==2 || n==12) ? 1 : (n > 2 && n < 20) ? 2 : 3;\n"
"X-Generator: Pootle 2.7\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1460068200.000000\n"

#: contentfieldpage.ui
msgctxt ""
"contentfieldpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Existing fields"
msgstr "Raointean a tha ann"

#: contentfieldpage.ui
msgctxt ""
"contentfieldpage.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "Display field"
msgstr "Taisbean an raon"

#: contenttablepage.ui
msgctxt ""
"contenttablepage.ui\n"
"datasourcelabel\n"
"label\n"
"string.text"
msgid "Data source"
msgstr "Tùs-dàta"

#: contenttablepage.ui
msgctxt ""
"contenttablepage.ui\n"
"contenttypelabel\n"
"label\n"
"string.text"
msgid "Content type"
msgstr "Seòrsa na susbainte"

#: contenttablepage.ui
msgctxt ""
"contenttablepage.ui\n"
"formtablelabel\n"
"label\n"
"string.text"
msgid "Content"
msgstr "Susbaint"

#: contenttablepage.ui
msgctxt ""
"contenttablepage.ui\n"
"formsettings\n"
"label\n"
"string.text"
msgid "Form"
msgstr "Foirm"

#: contenttablepage.ui
msgctxt ""
"contenttablepage.ui\n"
"label3\n"
"label\n"
"string.text"
msgid ""
"On the right side, you see all the tables from the data source of the form.\n"
"\n"
"\n"
"Choose the table from which the data should be used as basis for the list content:"
msgstr ""
"Chì thu gach clàr o thùs-dàta an fhoirm air an taobh deis.\n"
"\n"
"\n"
"Tagh an clàr a thèid a chuid dàta a chleachdadh mar bhunait airson susbaint na liosta:"

#: contenttablepage.ui
msgctxt ""
"contenttablepage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Control"
msgstr "Uidheam-smachd"

#: datasourcepage.ui
msgctxt ""
"datasourcepage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid ""
"That was all the information necessary to integrate your address data into %PRODUCTNAME.\n"
"\n"
"Now, just enter the name under which you want to register the data source in %PRODUCTNAME."
msgstr ""
"Sin am fiosrachadh uile gu lèir air a bheil feum airson dàta nan seòlaidhean agad fhilleadh a-steach dha %PRODUCTNAME.\n"
"\n"
"Nise, cha leig thu leas ach ainm a thoirt air tùs an dàta ann an %PRODUCTNAME."

#: datasourcepage.ui
msgctxt ""
"datasourcepage.ui\n"
"embed\n"
"label\n"
"string.text"
msgid "Embed this address book definition into the current document."
msgstr "Leabaich deifinisean an leabhair sheòlaidhean seo san sgrìobhainn làithreach."

#: datasourcepage.ui
msgctxt ""
"datasourcepage.ui\n"
"locationft\n"
"label\n"
"string.text"
msgid "Location"
msgstr "Ionad"

#: datasourcepage.ui
msgctxt ""
"datasourcepage.ui\n"
"browse\n"
"label\n"
"string.text"
msgid "Browse..."
msgstr "Brabhsaich..."

#: datasourcepage.ui
msgctxt ""
"datasourcepage.ui\n"
"available\n"
"label\n"
"string.text"
msgid "Make this address book available to all modules in %PRODUCTNAME."
msgstr "Cuir an leabhar sheòlaidhean seo ri làimh gach mòideal ann an %PRODUCTNAME."

#: datasourcepage.ui
msgctxt ""
"datasourcepage.ui\n"
"nameft\n"
"label\n"
"string.text"
msgid "Address book name"
msgstr "Ainm leabhar nan seòladh"

#: datasourcepage.ui
msgctxt ""
"datasourcepage.ui\n"
"warning\n"
"label\n"
"string.text"
msgid "Another data source already has this name. As data sources have to have globally unique names, you need to choose another one."
msgstr "Tha an t-ainm seo air tùs dàta eile. Feumaidh ainmean nan tùsan dàta a bhith àraidh air fheadh an t-siostaim agus feumaidh tu fear eile a thaghadh ri linn sin."

#: defaultfieldselectionpage.ui
msgctxt ""
"defaultfieldselectionpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Should one option field be selected as a default?"
msgstr "An cleachd sinn aon raon roghainn mar roghainn bhunaiteach?"

#: defaultfieldselectionpage.ui
msgctxt ""
"defaultfieldselectionpage.ui\n"
"defaultselectionyes\n"
"label\n"
"string.text"
msgid "_Yes, the following:"
msgstr "_Cleachdaidh, am fear seo:"

#: defaultfieldselectionpage.ui
msgctxt ""
"defaultfieldselectionpage.ui\n"
"defaultselectionno\n"
"label\n"
"string.text"
msgid "No, one particular field is not going to be selected."
msgstr "Na dèanaibh sin, cha dèid raon sònraichte a thaghadh."

#: fieldassignpage.ui
msgctxt ""
"fieldassignpage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid ""
"To incorporate the address data in your templates, %PRODUCTNAME has to know which fields contain which data.\n"
"\n"
"For instance, you could have stored the e-mail addresses in a field named \"email\", or \"E-mail\" or \"EM\" - or something completely different.\n"
"\n"
"Click the button below to open another dialog where you can enter the settings for your data source."
msgstr ""
"Gus dàta nan seòladh fhilleadh a-steach 'nad theamplaidean, feumaidh fios a bhith aig %PRODUCTNAME dè an dàta a tha anns gach raon.\n"
"\n"
"Mar eisimpleir, dh'fhaodadh gu bheil na seòlaidhean puist-dhealain agad ann an raon air a bheil \"post-d\" no \"post-dealain\" no \"email\" no rud gu tur eadar-dhealaichte.\n"
"\n"
"Briog air a' phutan gu h-ìosal gus còmhradh fhosgladh far an urrainn dhut roghainnean an tùs-dàta agad a shuidheachadh."

#: fieldassignpage.ui
msgctxt ""
"fieldassignpage.ui\n"
"assign\n"
"label\n"
"string.text"
msgid "Field Assignment"
msgstr "Iomruineadh nan raointean"

#: fieldlinkpage.ui
msgctxt ""
"fieldlinkpage.ui\n"
"desc\n"
"label\n"
"string.text"
msgid "This is where you select fields with matching contents so that the value from the display field will be shown."
msgstr "Taghaidh tu raointean an-seo anns a bheil susbaint a tha a' maidseadh a chèile gus am faicear luach an raoin taisbeanaidh."

#: fieldlinkpage.ui
msgctxt ""
"fieldlinkpage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Field from the _Value Table"
msgstr "Raon on _chlàr luachan"

#: fieldlinkpage.ui
msgctxt ""
"fieldlinkpage.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "Field from the _List Table"
msgstr "Raon on chlàr _liosta"

#: gridfieldsselectionpage.ui
msgctxt ""
"gridfieldsselectionpage.ui\n"
"datasourcelabel\n"
"label\n"
"string.text"
msgid "Data source"
msgstr "Tùs-dàta"

#: gridfieldsselectionpage.ui
msgctxt ""
"gridfieldsselectionpage.ui\n"
"contenttypelabel\n"
"label\n"
"string.text"
msgid "Content type"
msgstr "Seòrsa na susbainte"

#: gridfieldsselectionpage.ui
msgctxt ""
"gridfieldsselectionpage.ui\n"
"formtablelabel\n"
"label\n"
"string.text"
msgid "Content"
msgstr "Susbaint"

#: gridfieldsselectionpage.ui
msgctxt ""
"gridfieldsselectionpage.ui\n"
"formsettings\n"
"label\n"
"string.text"
msgid "Form"
msgstr "Foirm"

#: gridfieldsselectionpage.ui
msgctxt ""
"gridfieldsselectionpage.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "Selected fields"
msgstr "Na raointean a thagh thu"

#: gridfieldsselectionpage.ui
msgctxt ""
"gridfieldsselectionpage.ui\n"
"fieldright\n"
"label\n"
"string.text"
msgid "->"
msgstr "->"

#: gridfieldsselectionpage.ui
msgctxt ""
"gridfieldsselectionpage.ui\n"
"allfieldsright\n"
"label\n"
"string.text"
msgid "=>>"
msgstr "=>>"

#: gridfieldsselectionpage.ui
msgctxt ""
"gridfieldsselectionpage.ui\n"
"fieldleft\n"
"label\n"
"string.text"
msgid "<-"
msgstr "<-"

#: gridfieldsselectionpage.ui
msgctxt ""
"gridfieldsselectionpage.ui\n"
"allfieldsleft\n"
"label\n"
"string.text"
msgid "<<="
msgstr "<<="

#: gridfieldsselectionpage.ui
msgctxt ""
"gridfieldsselectionpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Existing fields"
msgstr "Raointean a tha ann"

#: gridfieldsselectionpage.ui
msgctxt ""
"gridfieldsselectionpage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Table element"
msgstr "Eileamaid a' chlàir"

#: groupradioselectionpage.ui
msgctxt ""
"groupradioselectionpage.ui\n"
"datasourcelabel\n"
"label\n"
"string.text"
msgid "Data source"
msgstr "Tùs-dàta"

#: groupradioselectionpage.ui
msgctxt ""
"groupradioselectionpage.ui\n"
"contenttypelabel\n"
"label\n"
"string.text"
msgid "Content type"
msgstr "Seòrsa na susbainte"

#: groupradioselectionpage.ui
msgctxt ""
"groupradioselectionpage.ui\n"
"formtablelabel\n"
"label\n"
"string.text"
msgid "Content"
msgstr "Susbaint"

#: groupradioselectionpage.ui
msgctxt ""
"groupradioselectionpage.ui\n"
"formsettings\n"
"label\n"
"string.text"
msgid "Form"
msgstr "Foirm"

#: groupradioselectionpage.ui
msgctxt ""
"groupradioselectionpage.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "_Option fields"
msgstr "Ra_ointean nan roghainnean"

#: groupradioselectionpage.ui
msgctxt ""
"groupradioselectionpage.ui\n"
"toright\n"
"label\n"
"string.text"
msgid "_>>"
msgstr "_>>"

#: groupradioselectionpage.ui
msgctxt ""
"groupradioselectionpage.ui\n"
"toleft\n"
"label\n"
"string.text"
msgid "_<<"
msgstr "_<<"

#: groupradioselectionpage.ui
msgctxt ""
"groupradioselectionpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Which _names do you want to give the option fields?"
msgstr "Dè na h-ai_nmean a tha thu ag iarraidh airson raointean nan roghainnean?"

#: groupradioselectionpage.ui
msgctxt ""
"groupradioselectionpage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Table element"
msgstr "Eileamaid a' chlàir"

#: invokeadminpage.ui
msgctxt ""
"invokeadminpage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid ""
"To set up the new data source, additional information is required.\n"
"\n"
"Click the following button to open another dialog in which you then enter the necessary information."
msgstr ""
"Tha feum air fiosrachadh a bharrachd mus gabh an tùs dàta ùr a shuidheachadh..\n"
"\n"
"Briog air a' phutan a leanas a dh'fhosglas còmhradh ùr far an urrainn dhut na tha a dhìth a chur a-steach."

#: invokeadminpage.ui
msgctxt ""
"invokeadminpage.ui\n"
"settings\n"
"label\n"
"string.text"
msgid "Settings"
msgstr "Roghainnean"

#: invokeadminpage.ui
msgctxt ""
"invokeadminpage.ui\n"
"warning\n"
"label\n"
"string.text"
msgid ""
"The connection to the data source could not be established.\n"
"Before you proceed, please check the settings made, or (on the previous page) choose another address data source type."
msgstr ""
"Cha b' urrainn dhuinn ceangal ri tùs an dàta.\n"
"Mus lean thu air adhart, thoir sùil air na roghainnean a thagh thu (air an duilleag roimhpe) no tagh seòrsa eile de thùs dàta."

#: optiondbfieldpage.ui
msgctxt ""
"optiondbfieldpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Do you want to save the value in a database field?"
msgstr "A bheil thu airson an luach a shàbhaladh ann an raon stòir-dhàta?"

#: optiondbfieldpage.ui
msgctxt ""
"optiondbfieldpage.ui\n"
"yesRadiobutton\n"
"label\n"
"string.text"
msgid "_Yes, I want to save it in the following database field:"
msgstr "Tha, _bu toigh leam a shàbhaladh san raon stòir-dhàta a leanas:"

#: optiondbfieldpage.ui
msgctxt ""
"optiondbfieldpage.ui\n"
"noRadiobutton\n"
"label\n"
"string.text"
msgid "_No, I only want to save the value in the form."
msgstr "Cha_n eil, cha bu toigh leam ach an luach a shàbhaladh san fhoirm."

#: optionsfinalpage.ui
msgctxt ""
"optionsfinalpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Which _caption is to be given to your option group?"
msgstr "_Dè an caipsean a chuirear air a' bhuidheann roghainn agad?"

#: optionsfinalpage.ui
msgctxt ""
"optionsfinalpage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "These were all details needed to create the option group."
msgstr "Seo am fiosrachadh gu lèir air a bheil feum gus am buidheann roghainn a chruthachadh."

#: optionvaluespage.ui
msgctxt ""
"optionvaluespage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "When you select an option, the option group is given a specific value."
msgstr "Nuair a thaghas tu roghainn, thèid luach sònraichte a chur air a' bhuidheann roghainn."

#: optionvaluespage.ui
msgctxt ""
"optionvaluespage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Which _value do you want to assign to each option?"
msgstr "Dè an l_uach a bu toigh leat iomruineadh do gach roghainn?"

#: optionvaluespage.ui
msgctxt ""
"optionvaluespage.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "_Option fields"
msgstr "Ra_ointean nan roghainnean"

#: selecttablepage.ui
msgctxt ""
"selecttablepage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid ""
"The external data source you have chosen contains more than one address book.\n"
"Please select the one you mainly want to work with:"
msgstr ""
"Tha barrachd air aon leabhar sheòlaidhean san tùs dàta air an taobh a-muigh a thagh thu.\n"
"Tagh am fear a bu toigh leat a chleachdadh:"

#: selecttypepage.ui
msgctxt ""
"selecttypepage.ui\n"
"evolution\n"
"label\n"
"string.text"
msgid "Evolution"
msgstr "Evolution"

#: selecttypepage.ui
msgctxt ""
"selecttypepage.ui\n"
"groupwise\n"
"label\n"
"string.text"
msgid "Groupwise"
msgstr "Groupwise"

#: selecttypepage.ui
msgctxt ""
"selecttypepage.ui\n"
"evoldap\n"
"label\n"
"string.text"
msgid "Evolution LDAP"
msgstr "Evolution LDAP"

#: selecttypepage.ui
msgctxt ""
"selecttypepage.ui\n"
"firefox\n"
"label\n"
"string.text"
msgid "Firefox/Iceweasel"
msgstr "Firefox/Iceweasel"

#: selecttypepage.ui
msgctxt ""
"selecttypepage.ui\n"
"thunderbird\n"
"label\n"
"string.text"
msgid "Thunderbird/Icedove"
msgstr "Thunderbird/Icedove"

#: selecttypepage.ui
msgctxt ""
"selecttypepage.ui\n"
"kde\n"
"label\n"
"string.text"
msgid "KDE address book"
msgstr "Leabhar nan seòladh KDE"

#: selecttypepage.ui
msgctxt ""
"selecttypepage.ui\n"
"macosx\n"
"label\n"
"string.text"
msgid "Mac OS X address book"
msgstr "Leabhar nan seòladh Mac OS X"

#: selecttypepage.ui
msgctxt ""
"selecttypepage.ui\n"
"other\n"
"label\n"
"string.text"
msgid "Other external data source"
msgstr "Tùs-dàta eile air an taobh a-muigh"

#: selecttypepage.ui
msgctxt ""
"selecttypepage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Please Select the Type of Your External Address Book"
msgstr "Tagh seòrsa leabhar nan seòladh agad taobh a-muigh a' phrògraim seo"

#: selecttypepage.ui
msgctxt ""
"selecttypepage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid ""
"%PRODUCTNAME lets you access address data already present in your system. To do this, a %PRODUCTNAME data source will be created in which your address data is available in tabular form.\n"
"\n"
"This wizard helps you create the data source."
msgstr ""
"Tha %PRODUCTNAME a' toirt cothrom dhut inntrigeadh a dhèanamh do dhàta sheòlaidhean a tha air an t-siostam agad mu thràth. Gus seo a dhèanamh, thèid tùs dàta %PRODUCTNAME a chruthachadh anns am bi dàta nan seòladh agad ann an cruth clàir..\n"
"\n"
"Cuidichidh an draoidh seo thu le cruthachadh an tùs-dàta."

#: tableselectionpage.ui
msgctxt ""
"tableselectionpage.ui\n"
"label3\n"
"label\n"
"string.text"
msgid ""
"Currently, the form the control belongs to is not (or not completely) bound to a data source.\n"
"\n"
"Please choose a data source and a table.\n"
"\n"
"\n"
"Please note that the settings made on this page will take effect immediately upon leaving the page."
msgstr ""
"Chan eil am foirm aig a bheil an t-uidheam-smachd co-cheangailte idir ri tùs-dàta an-dràsta (no air a cheangal gu neo-iomlan).\n"
"\n"
"Tagh tùs-dàta agus clàr.\n"
"\n"
"\n"
"Thoir an aire gum bi buaidh aig na roghainnean a thaghas tu air an duilleag seo cho luath ’s a dh’fhàgas tu e."

#: tableselectionpage.ui
msgctxt ""
"tableselectionpage.ui\n"
"datasourcelabel\n"
"label\n"
"string.text"
msgid "_Data source:"
msgstr "Tùs an _dàta:"

#: tableselectionpage.ui
msgctxt ""
"tableselectionpage.ui\n"
"search\n"
"label\n"
"string.text"
msgid "_..."
msgstr "_..."

#: tableselectionpage.ui
msgctxt ""
"tableselectionpage.ui\n"
"tablelabel\n"
"label\n"
"string.text"
msgid "_Table / Query:"
msgstr "Clàr / Ceis_t:"

#: tableselectionpage.ui
msgctxt ""
"tableselectionpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Data"
msgstr "Dàta"

#. extracted from sw/source/core/undo
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2017-10-14 05:23+0000\n"
"Last-Translator: Sophia Schröder <sophia.schroeder@outlook.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1507958605.000000\n"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_CANT_UNDO\n"
"string.text"
msgid "not possible"
msgstr "unmöglich"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_DELETE_UNDO\n"
"string.text"
msgid "Delete $1"
msgstr "Löschen: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_INSERT_UNDO\n"
"string.text"
msgid "Insert $1"
msgstr "Einfügen: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_OVR_UNDO\n"
"string.text"
msgid "Overwrite: $1"
msgstr "Überschreiben: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_SPLITNODE_UNDO\n"
"string.text"
msgid "New Paragraph"
msgstr "neuer Absatz"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_MOVE_UNDO\n"
"string.text"
msgid "Move"
msgstr "Verschieben"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_INSATTR_UNDO\n"
"string.text"
msgid "Apply attributes"
msgstr "Attribute anwenden"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_SETFMTCOLL_UNDO\n"
"string.text"
msgid "Apply Styles: $1"
msgstr "Vorlagen zuweisen: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_RESET_ATTR_UNDO\n"
"string.text"
msgid "Reset attributes"
msgstr "Attribute zurücksetzen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_INSFMT_ATTR_UNDO\n"
"string.text"
msgid "Change style: $1"
msgstr "Formatvorlage ändern: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_INSERT_DOC_UNDO\n"
"string.text"
msgid "Insert file"
msgstr "Datei einfügen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_INSERT_GLOSSARY\n"
"string.text"
msgid "Insert AutoText"
msgstr "AutoText einfügen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_DELBOOKMARK\n"
"string.text"
msgid "Delete bookmark: $1"
msgstr "Lesezeichen löschen: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_INSBOOKMARK\n"
"string.text"
msgid "Insert bookmark: $1"
msgstr "Lesezeichen einfügen: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_SORT_TBL\n"
"string.text"
msgid "Sort table"
msgstr "Tabelle sortieren"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_SORT_TXT\n"
"string.text"
msgid "Sort text"
msgstr "Text sortieren"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_INSTABLE_UNDO\n"
"string.text"
msgid "Insert table: $1$2$3"
msgstr "Tabelle einfügen: $1$2$3"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_TEXTTOTABLE_UNDO\n"
"string.text"
msgid "Convert text -> table"
msgstr "Konvertierung Text -> Tabelle"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_TABLETOTEXT_UNDO\n"
"string.text"
msgid "Convert table -> text"
msgstr "Konvertierung Tabelle -> Text"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_COPY_UNDO\n"
"string.text"
msgid "Copy: $1"
msgstr "Kopieren: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_REPLACE_UNDO\n"
"string.text"
msgid "Replace $1 $2 $3"
msgstr "Ersetzen: $1 $2 $3"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_INSERT_PAGE_BREAK_UNDO\n"
"string.text"
msgid "Insert page break"
msgstr "Seitenumbruch einfügen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_INSERT_COLUMN_BREAK_UNDO\n"
"string.text"
msgid "Insert column break"
msgstr "Spaltenumbruch einfügen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_PLAY_MACRO_UNDO\n"
"string.text"
msgid "Run macro"
msgstr "Makro abspielen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_INSERT_ENV_UNDO\n"
"string.text"
msgid "Insert Envelope"
msgstr "Briefumschlag einfügen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_DRAG_AND_COPY\n"
"string.text"
msgid "Copy: $1"
msgstr "Kopieren: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_DRAG_AND_MOVE\n"
"string.text"
msgid "Move: $1"
msgstr "Verschieben: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_INSERT_CHART\n"
"string.text"
msgid "Insert %PRODUCTNAME Chart"
msgstr "%PRODUCTNAME Chart einfügen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_INSERTFLY\n"
"string.text"
msgid "Insert frame"
msgstr "Rahmen einfügen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_DELETEFLY\n"
"string.text"
msgid "Delete frame"
msgstr "Rahmen löschen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_AUTOFORMAT\n"
"string.text"
msgid "AutoFormat"
msgstr "AutoFormat"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_TABLEHEADLINE\n"
"string.text"
msgid "Table heading"
msgstr "Tabellenüberschrift"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_REPLACE\n"
"string.text"
msgid "Replace: $1 $2 $3"
msgstr "Ersetzen: $1 $2 $3"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_INSERTSECTION\n"
"string.text"
msgid "Insert section"
msgstr "Bereich einfügen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_DELETESECTION\n"
"string.text"
msgid "Delete section"
msgstr "Bereich löschen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_CHANGESECTION\n"
"string.text"
msgid "Modify section"
msgstr "Bereich ändern"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_CHANGESECTPASSWD\n"
"string.text"
msgid "Change password protection"
msgstr "Kennwortschutz ändern"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_CHANGEDEFATTR\n"
"string.text"
msgid "Modify default values"
msgstr "Standardwerte ändern"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_REPLACE_STYLE\n"
"string.text"
msgid "Replace style: $1 $2 $3"
msgstr "Vorlage ersetzen: $1 $2 $3"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_DELETE_PAGE_BREAK\n"
"string.text"
msgid "Delete page break"
msgstr "Seitenumbruch löschen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_TEXT_CORRECTION\n"
"string.text"
msgid "Text Correction"
msgstr "Textkorrektur"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_OUTLINE_LR\n"
"string.text"
msgid "Promote/demote outline"
msgstr "Gliederung einstufen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_OUTLINE_UD\n"
"string.text"
msgid "Move outline"
msgstr "Gliederung verschieben"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_INSNUM\n"
"string.text"
msgid "Insert numbering"
msgstr "Nummerierung einfügen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_NUMUP\n"
"string.text"
msgid "Promote level"
msgstr "Ebene höher verschieben"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_NUMDOWN\n"
"string.text"
msgid "Demote level"
msgstr "Ebene niedriger verschieben"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_MOVENUM\n"
"string.text"
msgid "Move paragraphs"
msgstr "Absätze verschieben"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_INSERTDRAW\n"
"string.text"
msgid "Insert drawing object: $1"
msgstr "Zeichnungsobjekt einfügen: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_NUMORNONUM\n"
"string.text"
msgid "Number On/Off"
msgstr "Nummer ein/aus"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_INC_LEFTMARGIN\n"
"string.text"
msgid "Increase Indent"
msgstr "Einzug vergrößern"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_DEC_LEFTMARGIN\n"
"string.text"
msgid "Decrease indent"
msgstr "Einzug verkleiner"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_INSERTLABEL\n"
"string.text"
msgid "Insert caption: $1"
msgstr "Beschriftung einfügen: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_SETNUMRULESTART\n"
"string.text"
msgid "Restart numbering"
msgstr "Nummerierung neu starten"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_CHANGEFTN\n"
"string.text"
msgid "Modify footnote"
msgstr "Fußnote ändern"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_ACCEPT_REDLINE\n"
"string.text"
msgid "Accept change: $1"
msgstr "Änderung akzeptieren: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_REJECT_REDLINE\n"
"string.text"
msgid "Reject change: $1"
msgstr "Änderung verwerfen: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_SPLIT_TABLE\n"
"string.text"
msgid "Split Table"
msgstr "Tabelle auftrennen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_DONTEXPAND\n"
"string.text"
msgid "Stop attribute"
msgstr "Attribut stoppen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_AUTOCORRECT\n"
"string.text"
msgid "AutoCorrect"
msgstr "AutoKorrektur"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_MERGE_TABLE\n"
"string.text"
msgid "Merge table"
msgstr "Tabelle verbinden"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_TRANSLITERATE\n"
"string.text"
msgid "Change Case"
msgstr "Groß-/Kleinschreibung ändern"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_DELNUM\n"
"string.text"
msgid "Delete numbering"
msgstr "Nummerierung löschen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_DRAWUNDO\n"
"string.text"
msgid "Drawing objects: $1"
msgstr "Zeichnungsobjekte: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_DRAWGROUP\n"
"string.text"
msgid "Group draw objects"
msgstr "Zeichenobjekte gruppieren"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_DRAWUNGROUP\n"
"string.text"
msgid "Ungroup drawing objects"
msgstr "Gruppenobjekt auflösen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_DRAWDELETE\n"
"string.text"
msgid "Delete drawing objects"
msgstr "Zeichnungsobjekte löschen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_REREAD\n"
"string.text"
msgid "Replace Image"
msgstr "Bild ersetzen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_DELGRF\n"
"string.text"
msgid "Delete Image"
msgstr "Bild löschen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_DELOLE\n"
"string.text"
msgid "Delete object"
msgstr "Objekt löschen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_TABLE_ATTR\n"
"string.text"
msgid "Apply table attributes"
msgstr "Tabellenattribute anwenden"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_TABLE_AUTOFMT\n"
"string.text"
msgid "AutoFormat Table"
msgstr "Autoformat Tabelle"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_TABLE_INSCOL\n"
"string.text"
msgid "Insert Column"
msgstr "Spalte einfügen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_TABLE_INSROW\n"
"string.text"
msgid "Insert Row"
msgstr "Zeile einfügen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_TABLE_DELBOX\n"
"string.text"
msgid "Delete row/column"
msgstr "Zeile/Spalte löschen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_COL_DELETE\n"
"string.text"
msgid "Delete column"
msgstr "Spalte löschen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_ROW_DELETE\n"
"string.text"
msgid "Delete row"
msgstr "Zeile löschen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_TABLE_SPLIT\n"
"string.text"
msgid "Split Cells"
msgstr "Zellen teilen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_TABLE_MERGE\n"
"string.text"
msgid "Merge Cells"
msgstr "Zelle verbinden"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_TABLE_NUMFORMAT\n"
"string.text"
msgid "Format cell"
msgstr "Zelle formatieren"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_INSERT_TOX\n"
"string.text"
msgid "Insert index/table"
msgstr "Verzeichnis einfügen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_CLEAR_TOX_RANGE\n"
"string.text"
msgid "Remove index/table"
msgstr "Verzeichnis löschen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_TABLE_TBLCPYTBL\n"
"string.text"
msgid "Copy table"
msgstr "Tabelle kopieren"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_TABLE_CPYTBL\n"
"string.text"
msgid "Copy table"
msgstr "Tabelle kopieren"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_INS_FROM_SHADOWCRSR\n"
"string.text"
msgid "Set cursor"
msgstr "Cursor setzen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_CHAIN\n"
"string.text"
msgid "Link text frames"
msgstr "Textrahmen verbinden"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_UNCHAIN\n"
"string.text"
msgid "Unlink text frames"
msgstr "Textrahmenverbindung lösen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_FTNINFO\n"
"string.text"
msgid "Modify footnote options"
msgstr "Fußnoteneinstellungen ändern"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_ENDNOTEINFO\n"
"string.text"
msgid "Modify endnote settings"
msgstr "Endnoteneinstellungen ändern"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_COMPAREDOC\n"
"string.text"
msgid "Compare Document"
msgstr "Dokument vergleichen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_SETFLYFRMFMT\n"
"string.text"
msgid "Apply frame style: $1"
msgstr "Rahmenvorlage zuweisen: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_SETRUBYATTR\n"
"string.text"
msgid "Ruby Setting"
msgstr "Phonetische Symbole setzen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_TMPAUTOCORR\n"
"string.text"
msgid "AutoCorrect"
msgstr "AutoKorrektur"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_INSERT_FOOTNOTE\n"
"string.text"
msgid "Insert footnote"
msgstr "Fußnote einfügen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_INSERT_URLBTN\n"
"string.text"
msgid "insert URL button"
msgstr "URL Schaltfläche einfügen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_INSERT_URLTXT\n"
"string.text"
msgid "Insert Hyperlink"
msgstr "Hyperlink einfügen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_DELETE_INVISIBLECNTNT\n"
"string.text"
msgid "remove invisible content"
msgstr "unsichtbaren Inhalt entfernen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_TOXCHANGE\n"
"string.text"
msgid "Table/index changed"
msgstr "Verzeichnis geändert"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_START_QUOTE\n"
"string.text"
msgid "'"
msgstr "'"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_END_QUOTE\n"
"string.text"
msgid "'"
msgstr "'"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_LDOTS\n"
"string.text"
msgid "..."
msgstr "..."

#: undo.src
msgctxt ""
"undo.src\n"
"STR_CLIPBOARD\n"
"string.text"
msgid "clipboard"
msgstr "Zwischenablage"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_MULTISEL\n"
"string.text"
msgid "multiple selection"
msgstr "Mehrfachauswahl"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_TYPING_UNDO\n"
"string.text"
msgid "Typing: $1"
msgstr "Eingabe: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_PASTE_CLIPBOARD_UNDO\n"
"string.text"
msgid "Paste clipboard"
msgstr "Zwischenablage einfügen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_YIELDS\n"
"string.text"
msgid "->"
msgstr "->"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_OCCURRENCES_OF\n"
"string.text"
msgid "occurrences of"
msgstr "Vorkommen von"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_TABS\n"
"string.text"
msgid "$1 tab(s)"
msgstr "$1 Tab(s)"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_NLS\n"
"string.text"
msgid "$1 line break(s)"
msgstr "$1 Zeilenumbruch/-brüche"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_PAGEBREAKS\n"
"string.text"
msgid "page break"
msgstr "Seitenumbruch"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_COLBRKS\n"
"string.text"
msgid "column break"
msgstr "Spaltenumbruch"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_REDLINE_INSERT\n"
"string.text"
msgid "Insert $1"
msgstr "Einfügen: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_REDLINE_DELETE\n"
"string.text"
msgid "Delete $1"
msgstr "Löschen: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_REDLINE_FORMAT\n"
"string.text"
msgid "Attributes changed"
msgstr "Attribute geändert"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_REDLINE_TABLE\n"
"string.text"
msgid "Table changed"
msgstr "Tabelle geändert"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_REDLINE_FMTCOLL\n"
"string.text"
msgid "Style changed"
msgstr "Formatvorlage geändert"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_REDLINE_MULTIPLE\n"
"string.text"
msgid "multiple changes"
msgstr "Mehrere Änderungen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_N_REDLINES\n"
"string.text"
msgid "$1 changes"
msgstr "$1 Änderungen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_PAGEDESC\n"
"string.text"
msgid "Change page style: $1"
msgstr "Seitenvorlage ändern: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_PAGEDESC_CREATE\n"
"string.text"
msgid "Create page style: $1"
msgstr "Neue Seitenvorlage: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_PAGEDESC_DELETE\n"
"string.text"
msgid "Delete page style: $1"
msgstr "Seitenvorlage löschen: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_PAGEDESC_RENAME\n"
"string.text"
msgid "Rename page style: $1 $2 $3"
msgstr "Seitenvorlage umbenennen: $1 $2 $3"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_HEADER_FOOTER\n"
"string.text"
msgid "Header/footer changed"
msgstr "Kopf-/Fußzeile geändert"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_FIELD\n"
"string.text"
msgid "Field changed"
msgstr "Feld geändert"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_TXTFMTCOL_CREATE\n"
"string.text"
msgid "Create paragraph style: $1"
msgstr "Absatzformat erzeugen: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_TXTFMTCOL_DELETE\n"
"string.text"
msgid "Delete paragraph style: $1"
msgstr "Absatzformat löschen: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_TXTFMTCOL_RENAME\n"
"string.text"
msgid "Rename paragraph style: $1 $2 $3"
msgstr "Absatzformat umbenennen: $1 $2 $3"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_CHARFMT_CREATE\n"
"string.text"
msgid "Create character style: $1"
msgstr "Zeichenformat erzeugen: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_CHARFMT_DELETE\n"
"string.text"
msgid "Delete character style: $1"
msgstr "Zeichenformat löschen: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_CHARFMT_RENAME\n"
"string.text"
msgid "Rename character style: $1 $2 $3"
msgstr "Zeichenformat umbenennen: $1 $2 $3"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_FRMFMT_CREATE\n"
"string.text"
msgid "Create frame style: $1"
msgstr "Rahmenformat erzeugen: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_FRMFMT_DELETE\n"
"string.text"
msgid "Delete frame style: $1"
msgstr "Rahmenformat löschen: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_FRMFMT_RENAME\n"
"string.text"
msgid "Rename frame style: $1 $2 $3"
msgstr "Rahmenformat umbenennen: $1 $2 $3"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_NUMRULE_CREATE\n"
"string.text"
msgid "Create numbering style: $1"
msgstr "Listenformatvorlage erzeugen: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_NUMRULE_DELETE\n"
"string.text"
msgid "Delete numbering style: $1"
msgstr "Listenformatvorlage löschen: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_NUMRULE_RENAME\n"
"string.text"
msgid "Rename numbering style: $1 $2 $3"
msgstr "Listenformatvorlage umbenennen: $1 $2 $3"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_BOOKMARK_RENAME\n"
"string.text"
msgid "Rename bookmark: $1 $2 $3"
msgstr "Lesezeichen umbenennen: $1 $2 $3"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_INDEX_ENTRY_INSERT\n"
"string.text"
msgid "Insert index entry"
msgstr "Verzeichniseintrag einfügen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_INDEX_ENTRY_DELETE\n"
"string.text"
msgid "Delete index entry"
msgstr "Verzeichniseintrag löschen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_FIELD\n"
"string.text"
msgid "field"
msgstr "Feld"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_PARAGRAPHS\n"
"string.text"
msgid "Paragraphs"
msgstr "Absätze"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_FRAME\n"
"string.text"
msgid "frame"
msgstr "Textrahmen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_OLE\n"
"string.text"
msgid "OLE-object"
msgstr "OLE-Objekt"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_MATH_FORMULA\n"
"string.text"
msgid "formula"
msgstr "Formel"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_CHART\n"
"string.text"
msgid "chart"
msgstr "Chart"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_NOTE\n"
"string.text"
msgid "comment"
msgstr "Kommentar"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_REFERENCE\n"
"string.text"
msgid "cross-reference"
msgstr "Querverweis"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_SCRIPT\n"
"string.text"
msgid "script"
msgstr "Skript"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_AUTHORITY_ENTRY\n"
"string.text"
msgid "bibliography entry"
msgstr "Literaturverzeichniseintrag"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_SPECIALCHAR\n"
"string.text"
msgid "special character"
msgstr "Sonderzeichen"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_FOOTNOTE\n"
"string.text"
msgid "footnote"
msgstr "Fußnote"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_GRAPHIC\n"
"string.text"
msgid "picture"
msgstr "Grafik"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_DRAWING_OBJECTS\n"
"string.text"
msgid "drawing object(s)"
msgstr "Zeichnungsobjekt(e)"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_TABLE_NAME\n"
"string.text"
msgid "table: $1$2$3"
msgstr "Tabelle: $1$2$3"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_PARAGRAPH_UNDO\n"
"string.text"
msgid "paragraph"
msgstr "Absatz"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_FLYFRMFMT_TITLE\n"
"string.text"
msgid "Change object title of $1"
msgstr "Objekttitel für $1 ändern"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_FLYFRMFMT_DESCRITPTION\n"
"string.text"
msgid "Change object description of $1"
msgstr "Objektbeschreibung für $1 ändern"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_TBLSTYLE_CREATE\n"
"string.text"
msgid "Create table style: $1"
msgstr "Neue Tabellenvorlage: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_TBLSTYLE_DELETE\n"
"string.text"
msgid "Delete table style: $1"
msgstr "Tabellenvorlage löschen: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_TBLSTYLE_UPDATE\n"
"string.text"
msgid "Update table style: $1"
msgstr "Tabellenvorlage aktualisieren: $1"

#: undo.src
msgctxt ""
"undo.src\n"
"STR_UNDO_TABLE_DELETE\n"
"string.text"
msgid "Delete table"
msgstr "Tabelle löschen"

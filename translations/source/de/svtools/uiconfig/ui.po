#. extracted from svtools/uiconfig/ui
msgid ""
msgstr ""
"Project-Id-Version: LibO 40l10n\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2017-02-26 04:48+0000\n"
"Last-Translator: Christian Kühl <kuehl.christian@googlemail.com>\n"
"Language-Team: none\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1488084507.000000\n"

#: GraphicExportOptionsDialog.ui
msgctxt ""
"GraphicExportOptionsDialog.ui\n"
"GraphicExporter\n"
"title\n"
"string.text"
msgid "Image Options"
msgstr "Bildoptionen"

#: GraphicExportOptionsDialog.ui
msgctxt ""
"GraphicExportOptionsDialog.ui\n"
"label5\n"
"label\n"
"string.text"
msgid "Width:"
msgstr "Breite:"

#: GraphicExportOptionsDialog.ui
msgctxt ""
"GraphicExportOptionsDialog.ui\n"
"label6\n"
"label\n"
"string.text"
msgid "Height:"
msgstr "Höhe:"

#: GraphicExportOptionsDialog.ui
msgctxt ""
"GraphicExportOptionsDialog.ui\n"
"resolutionft\n"
"label\n"
"string.text"
msgid "Resolution:"
msgstr "Auflösung:"

#: GraphicExportOptionsDialog.ui
msgctxt ""
"GraphicExportOptionsDialog.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "px"
msgstr "px"

#: GraphicExportOptionsDialog.ui
msgctxt ""
"GraphicExportOptionsDialog.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "px"
msgstr "px"

#: GraphicExportOptionsDialog.ui
msgctxt ""
"GraphicExportOptionsDialog.ui\n"
"label4\n"
"label\n"
"string.text"
msgid "DPI"
msgstr "DPI"

#: GraphicExportOptionsDialog.ui
msgctxt ""
"GraphicExportOptionsDialog.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Size"
msgstr "Größe"

#: addresstemplatedialog.ui
msgctxt ""
"addresstemplatedialog.ui\n"
"AddressTemplateDialog\n"
"title\n"
"string.text"
msgid "Templates: Address Book Assignment"
msgstr "Vorlagen: Adressbuchzuweisung"

#: addresstemplatedialog.ui
msgctxt ""
"addresstemplatedialog.ui\n"
"label33\n"
"label\n"
"string.text"
msgid "Data source:"
msgstr "Datenquelle:"

#: addresstemplatedialog.ui
msgctxt ""
"addresstemplatedialog.ui\n"
"label43\n"
"label\n"
"string.text"
msgid "Table:"
msgstr "Tabelle:"

#: addresstemplatedialog.ui
msgctxt ""
"addresstemplatedialog.ui\n"
"admin\n"
"label\n"
"string.text"
msgid "_Address Data Source..."
msgstr "_Adressdatenquelle..."

#: addresstemplatedialog.ui
msgctxt ""
"addresstemplatedialog.ui\n"
"label100\n"
"label\n"
"string.text"
msgid "Address Book Source"
msgstr "Adressbuchquelle"

#: addresstemplatedialog.ui
msgctxt ""
"addresstemplatedialog.ui\n"
"label23\n"
"label\n"
"string.text"
msgid "Field Assignment"
msgstr "Feldzuordnung"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"GraphicExportDialog\n"
"title\n"
"string.text"
msgid "%1 Options"
msgstr "%1 Optionen"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"label5\n"
"label\n"
"string.text"
msgid "Width:"
msgstr "Breite:"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"label6\n"
"label\n"
"string.text"
msgid "Height:"
msgstr "Höhe:"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"resolutionft\n"
"label\n"
"string.text"
msgid "Resolution:"
msgstr "Auflösung:"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Size"
msgstr "Größe"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Color Depth"
msgstr "Farbtiefe"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"label9\n"
"label\n"
"string.text"
msgid "Quality"
msgstr "Qualität"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"label\n"
"label\n"
"string.text"
msgid "Compression"
msgstr "Kompression"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"rlecb\n"
"label\n"
"string.text"
msgid "RLE encoding"
msgstr "RLE-Kodierung"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "Compression"
msgstr "Kompression"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"interlacedcb\n"
"label\n"
"string.text"
msgid "Interlaced"
msgstr "Interlaced"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"label12\n"
"label\n"
"string.text"
msgid "Mode"
msgstr "Modus"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"savetransparencycb\n"
"label\n"
"string.text"
msgid "Save transparency"
msgstr "Transparenz speichern"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"labe\n"
"label\n"
"string.text"
msgid "Drawing Objects"
msgstr "Zeichnungsobjekte"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"binarycb\n"
"label\n"
"string.text"
msgid "Binary"
msgstr "Binär"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"textcb\n"
"label\n"
"string.text"
msgid "Text"
msgstr "Text"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"label16\n"
"label\n"
"string.text"
msgid "Encoding"
msgstr "Kodierung"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"tiffpreviewcb\n"
"label\n"
"string.text"
msgid "Image preview (TIFF)"
msgstr "Vorschaubild (TIFF)"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"epsipreviewcb\n"
"label\n"
"string.text"
msgid "Interchange (EPSI)"
msgstr "Interchange (EPSI)"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"label17\n"
"label\n"
"string.text"
msgid "Preview"
msgstr "Vorschau"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"color1rb\n"
"label\n"
"string.text"
msgid "Color"
msgstr "Farbe"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"color2rb\n"
"label\n"
"string.text"
msgid "Grayscale"
msgstr "Graustufen"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"label18\n"
"label\n"
"string.text"
msgid "Color Format"
msgstr "Farbformat"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"level1rb\n"
"label\n"
"string.text"
msgid "Level 1"
msgstr "Ebene 1"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"level2rb\n"
"label\n"
"string.text"
msgid "Level 2"
msgstr "Ebene 2"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"label19\n"
"label\n"
"string.text"
msgid "Version"
msgstr "Version"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"compresslzw\n"
"label\n"
"string.text"
msgid "LZW encoding"
msgstr "LZW Kodierung"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"compressnone\n"
"label\n"
"string.text"
msgid "None"
msgstr "Keine"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"label20\n"
"label\n"
"string.text"
msgid "Compression"
msgstr "Kompression"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"label4\n"
"label\n"
"string.text"
msgid "Information"
msgstr "Dokumentinfo"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"liststore1\n"
"0\n"
"stringlist.text"
msgid "pixels/cm"
msgstr "Pixel/cm"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"liststore1\n"
"1\n"
"stringlist.text"
msgid "pixels/inch"
msgstr "Pixel/Zoll"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"liststore1\n"
"2\n"
"stringlist.text"
msgid "pixels/meter"
msgstr "Pixel/Meter"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"liststore2\n"
"0\n"
"stringlist.text"
msgid "inches"
msgstr "Zoll"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"liststore2\n"
"1\n"
"stringlist.text"
msgid "cm"
msgstr "cm"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"liststore2\n"
"2\n"
"stringlist.text"
msgid "mm"
msgstr "mm"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"liststore2\n"
"3\n"
"stringlist.text"
msgid "points"
msgstr "Punkt"

#: graphicexport.ui
msgctxt ""
"graphicexport.ui\n"
"liststore2\n"
"4\n"
"stringlist.text"
msgid "pixels"
msgstr "Pixel"

#: javadisableddialog.ui
msgctxt ""
"javadisableddialog.ui\n"
"JavaDisabledDialog\n"
"title\n"
"string.text"
msgid "Enable JRE?"
msgstr "JRE aktivieren?"

#: javadisableddialog.ui
msgctxt ""
"javadisableddialog.ui\n"
"JavaDisabledDialog\n"
"text\n"
"string.text"
msgid "%PRODUCTNAME requires a Java runtime environment (JRE) to perform this task. However, use of a JRE has been disabled. Do you want to enable the use of a JRE now?"
msgstr "%PRODUCTNAME benötigt eine Java-Laufzeitumgebung (JRE), um diese Aufgabe durchführen zu können. Jedoch ist die Benutzung einer JRE deaktiviert. Möchten Sie die Benutzung von einer JRE jetzt aktivieren?"

#: placeedit.ui
msgctxt ""
"placeedit.ui\n"
"PlaceEditDialog\n"
"title\n"
"string.text"
msgid "File Services"
msgstr "Dateidienste"

#: placeedit.ui
msgctxt ""
"placeedit.ui\n"
"typeLabel\n"
"label\n"
"string.text"
msgid "Type:"
msgstr "Typ:"

#: placeedit.ui
msgctxt ""
"placeedit.ui\n"
"hostLabel\n"
"label\n"
"string.text"
msgid "Host:"
msgstr "Host:"

#: placeedit.ui
msgctxt ""
"placeedit.ui\n"
"pathLabel\n"
"label\n"
"string.text"
msgid "Root:"
msgstr "Root:"

#: placeedit.ui
msgctxt ""
"placeedit.ui\n"
"shareLabel\n"
"label\n"
"string.text"
msgid "Share:"
msgstr "Freigabe:"

#: placeedit.ui
msgctxt ""
"placeedit.ui\n"
"repositoryLabel\n"
"label\n"
"string.text"
msgid "Repository:"
msgstr "Repository:"

#: placeedit.ui
msgctxt ""
"placeedit.ui\n"
"webdavs\n"
"label\n"
"string.text"
msgid "Secure connection"
msgstr "Sichere Verbindung"

#: placeedit.ui
msgctxt ""
"placeedit.ui\n"
"loginLabel\n"
"label\n"
"string.text"
msgid "User:"
msgstr "Benutzer:"

#: placeedit.ui
msgctxt ""
"placeedit.ui\n"
"nameLabel\n"
"label\n"
"string.text"
msgid "Label:"
msgstr "Beschriftung:"

#: placeedit.ui
msgctxt ""
"placeedit.ui\n"
"portLabel\n"
"label\n"
"string.text"
msgid "Port:"
msgstr "Port:"

#: placeedit.ui
msgctxt ""
"placeedit.ui\n"
"passwordLabel\n"
"label\n"
"string.text"
msgid "Password:"
msgstr "Kennwort:"

#: placeedit.ui
msgctxt ""
"placeedit.ui\n"
"rememberPassword\n"
"label\n"
"string.text"
msgid "Remember password"
msgstr "Kennwort dauerhaft speichern"

#: placeedit.ui
msgctxt ""
"placeedit.ui\n"
"liststore1\n"
"0\n"
"stringlist.text"
msgid "WebDAV"
msgstr "WebDAV"

#: placeedit.ui
msgctxt ""
"placeedit.ui\n"
"liststore1\n"
"1\n"
"stringlist.text"
msgid "FTP"
msgstr "FTP"

#: placeedit.ui
msgctxt ""
"placeedit.ui\n"
"liststore1\n"
"2\n"
"stringlist.text"
msgid "SSH"
msgstr "SSH"

#: placeedit.ui
msgctxt ""
"placeedit.ui\n"
"liststore1\n"
"3\n"
"stringlist.text"
msgid "Windows Share"
msgstr "Windows Freigabe"

#: printersetupdialog.ui
msgctxt ""
"printersetupdialog.ui\n"
"PrinterSetupDialog\n"
"title\n"
"string.text"
msgid "Printer Setup"
msgstr "Drucker einrichten"

#: printersetupdialog.ui
msgctxt ""
"printersetupdialog.ui\n"
"options\n"
"label\n"
"string.text"
msgid "Options..."
msgstr "Optionen..."

#: printersetupdialog.ui
msgctxt ""
"printersetupdialog.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Name:"
msgstr "Name:"

#: printersetupdialog.ui
msgctxt ""
"printersetupdialog.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "Status:"
msgstr "Status:"

#: printersetupdialog.ui
msgctxt ""
"printersetupdialog.ui\n"
"label4\n"
"label\n"
"string.text"
msgid "Type:"
msgstr "Typ:"

#: printersetupdialog.ui
msgctxt ""
"printersetupdialog.ui\n"
"label5\n"
"label\n"
"string.text"
msgid "Location:"
msgstr "Ort:"

#: printersetupdialog.ui
msgctxt ""
"printersetupdialog.ui\n"
"label6\n"
"label\n"
"string.text"
msgid "Comment:"
msgstr "Kommentar:"

#: printersetupdialog.ui
msgctxt ""
"printersetupdialog.ui\n"
"properties\n"
"label\n"
"string.text"
msgid "Properties..."
msgstr "Eigenschaften..."

#: printersetupdialog.ui
msgctxt ""
"printersetupdialog.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Printer"
msgstr "Drucker"

#: querydeletedialog.ui
msgctxt ""
"querydeletedialog.ui\n"
"QueryDeleteDialog\n"
"title\n"
"string.text"
msgid "Confirm Delete"
msgstr "Löschen bestätigen"

#: querydeletedialog.ui
msgctxt ""
"querydeletedialog.ui\n"
"QueryDeleteDialog\n"
"text\n"
"string.text"
msgid "Are you sure you want to delete the selected data?"
msgstr "Sind Sie sicher, dass Sie die ausgewählten Daten löschen möchten?"

#: querydeletedialog.ui
msgctxt ""
"querydeletedialog.ui\n"
"QueryDeleteDialog\n"
"secondary_text\n"
"string.text"
msgid "Entry: %s"
msgstr "Eintrag: %s"

#: querydeletedialog.ui
msgctxt ""
"querydeletedialog.ui\n"
"yes\n"
"label\n"
"string.text"
msgid "_Delete"
msgstr "_Löschen"

#: querydeletedialog.ui
msgctxt ""
"querydeletedialog.ui\n"
"all\n"
"label\n"
"string.text"
msgid "Delete _All"
msgstr "_Alle löschen"

#: querydeletedialog.ui
msgctxt ""
"querydeletedialog.ui\n"
"no\n"
"label\n"
"string.text"
msgid "Do _Not Delete"
msgstr "_Nicht löschen"

#: restartdialog.ui
msgctxt ""
"restartdialog.ui\n"
"RestartDialog\n"
"title\n"
"string.text"
msgid "Restart %PRODUCTNAME"
msgstr "%PRODUCTNAME neu starten"

#: restartdialog.ui
msgctxt ""
"restartdialog.ui\n"
"yes\n"
"label\n"
"string.text"
msgid "Restart Now"
msgstr "Jetzt neu starten"

#: restartdialog.ui
msgctxt ""
"restartdialog.ui\n"
"no\n"
"label\n"
"string.text"
msgid "Restart Later"
msgstr "Später neu starten"

#: restartdialog.ui
msgctxt ""
"restartdialog.ui\n"
"reason_java\n"
"label\n"
"string.text"
msgid "For the selected Java runtime environment to work properly, %PRODUCTNAME must be restarted."
msgstr "Damit die ausgewählte Java-Laufzeitumgebung richtig funktioniert, muss %PRODUCTNAME neu gestartet werden."

#: restartdialog.ui
msgctxt ""
"restartdialog.ui\n"
"reason_mailmerge_install\n"
"label\n"
"string.text"
msgid "For mail merge to work properly, %PRODUCTNAME must be restarted."
msgstr "Damit der Seriendruck richtig funktioniert, muss %PRODUCTNAME neu gestartet werden."

#: restartdialog.ui
msgctxt ""
"restartdialog.ui\n"
"reason_pdf\n"
"label\n"
"string.text"
msgid "For the modified default print job format to take effect, %PRODUCTNAME must be restarted."
msgstr "Damit die geänderten Druckauftrag-Standardeinstellungen verwendet werden können, muss %PRODUCTNAME neu gestartet werden."

#: restartdialog.ui
msgctxt ""
"restartdialog.ui\n"
"reason_bibliography_install\n"
"label\n"
"string.text"
msgid "For the bibliography to work properly, %PRODUCTNAME must be restarted."
msgstr "Damit das Literaturverzeichnis richtig funktioniert, muss %PRODUCTNAME neu gestartet werden."

#: restartdialog.ui
msgctxt ""
"restartdialog.ui\n"
"reason_assigning_folders\n"
"label\n"
"string.text"
msgid "For the assigned folders and archives to take effect, %PRODUCTNAME must be restarted."
msgstr "Um die zugewiesenen Ordner und Archive wirksam werden zu lassen, muss %PRODUCTNAME neu gestartet werden."

#: restartdialog.ui
msgctxt ""
"restartdialog.ui\n"
"reason_assigning_javaparameters\n"
"label\n"
"string.text"
msgid "For the assigned Java parameters to take effect, %PRODUCTNAME must be restarted."
msgstr "Um die zugewiesenen Java-Parameter wirksam werden zu lassen, muss %PRODUCTNAME neu gestartet werden."

#: restartdialog.ui
msgctxt ""
"restartdialog.ui\n"
"reason_adding_path\n"
"label\n"
"string.text"
msgid "For the added path to take effect, %PRODUCTNAME must be restarted."
msgstr "Um den hinzugefügten Pfad wirksam werden zu lassen, muss %PRODUCTNAME neu gestartet werden."

#: restartdialog.ui
msgctxt ""
"restartdialog.ui\n"
"reason_language_change\n"
"label\n"
"string.text"
msgid "For the updated language settings to take effect, %PRODUCTNAME must be restarted."
msgstr "Um die aktualisierten Spracheinstellungen wirksam werden zu lassen, muss %PRODUCTNAME neu gestartet werden."

#: restartdialog.ui
msgctxt ""
"restartdialog.ui\n"
"reason_exp_features\n"
"label\n"
"string.text"
msgid "For the modified experimental features to take effect, %PRODUCTNAME must be restarted."
msgstr "Um die geänderten experimentellen Funktionen wirksam werden zu lassen, muss %PRODUCTNAME neu gestartet werden."

#: restartdialog.ui
msgctxt ""
"restartdialog.ui\n"
"reason_extension_install\n"
"label\n"
"string.text"
msgid "For the extension to work properly, %PRODUCTNAME must be restarted."
msgstr "Damit die Extension richtig funktioniert, muss %PRODUCTNAME neu gestartet werden."

#: restartdialog.ui
msgctxt ""
"restartdialog.ui\n"
"reason_opengl\n"
"label\n"
"string.text"
msgid "For the OpenGL changes to take effect, %PRODUCTNAME must be restarted."
msgstr "Damit die OpenGL-Änderungen wirksam werden, muss %PRODUCTNAME neu gestartet werden."

#: restartdialog.ui
msgctxt ""
"restartdialog.ui\n"
"label\n"
"label\n"
"string.text"
msgid "Do you want to restart %PRODUCTNAME now?"
msgstr "Möchten Sie %PRODUCTNAME jetzt neu starten?"

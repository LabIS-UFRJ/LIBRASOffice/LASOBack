#. extracted from helpcontent2/source/text/scalc/02
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-11-20 13:02+0100\n"
"PO-Revision-Date: 2013-05-30 13:57+0000\n"
"Last-Translator: Andras <timar74@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1369922235.0\n"

#: 02130000.xhp
msgctxt ""
"02130000.xhp\n"
"tit\n"
"help.text"
msgid "Number format: Currency"
msgstr "Számformátum: pénznem"

#: 02130000.xhp
msgctxt ""
"02130000.xhp\n"
"hd_id3152892\n"
"1\n"
"help.text"
msgid "<link href=\"text/scalc/02/02130000.xhp\" name=\"Number format: Currency\">Number format: Currency</link>"
msgstr "<link href=\"text/scalc/02/02130000.xhp\" name=\"Számformátum: pénznem\">Számformátum: pénznem</link>"

#: 02130000.xhp
msgctxt ""
"02130000.xhp\n"
"par_id3148837\n"
"2\n"
"help.text"
msgid "<ahelp hid=\".uno:NumberFormatCurrency\" visibility=\"visible\">Applies the default currency format to the selected cells.</ahelp>"
msgstr "<ahelp hid=\".uno:NumberFormatCurrency\" visibility=\"visible\">A kijelölt cellákra az alapértelmezett pénzformátumot alkalmazza.</ahelp>"

#: 02130000.xhp
msgctxt ""
"02130000.xhp\n"
"par_id3155267\n"
"help.text"
msgid "<image src=\"cmd/sc_currencyfield.png\" id=\"img_id3159096\"><alt id=\"alt_id3159096\">Icon</alt></image>"
msgstr "<image src=\"cmd/sc_currencyfield.png\" id=\"img_id3159096\"><alt id=\"alt_id3159096\">Ikon</alt></image>"

#: 02130000.xhp
msgctxt ""
"02130000.xhp\n"
"par_id3150214\n"
"3\n"
"help.text"
msgid "Number Format: Currency"
msgstr "Számformátum: pénznem"

#: 02130000.xhp
msgctxt ""
"02130000.xhp\n"
"par_id3146776\n"
"4\n"
"help.text"
msgid "<link href=\"text/shared/01/05020300.xhp\" name=\"Format - Cell - Numbers\">Format - Cell - Numbers</link>."
msgstr "<link href=\"text/shared/01/05020300.xhp\" name=\"Formátum - Cellák - Számok\">Formátum - Cellák - Számok</link>"

#: 02140000.xhp
msgctxt ""
"02140000.xhp\n"
"tit\n"
"help.text"
msgid "Number format: Percent"
msgstr "Számformátum: százalék"

#: 02140000.xhp
msgctxt ""
"02140000.xhp\n"
"hd_id3156329\n"
"1\n"
"help.text"
msgid "<link href=\"text/scalc/02/02140000.xhp\" name=\"Number format: Percent\">Number format: Percent</link>"
msgstr "<link href=\"text/scalc/02/02140000.xhp\" name=\"Számformátum: százalék\">Számformátum: százalék</link>"

#: 02140000.xhp
msgctxt ""
"02140000.xhp\n"
"par_id3155629\n"
"2\n"
"help.text"
msgid "<ahelp hid=\".uno:NumberFormatPercent\">Applies the percentage format to the selected cells.</ahelp>"
msgstr "<ahelp hid=\".uno:NumberFormatPercent\">A kijelölt cellákra a százalékformátumot alkalmazza.</ahelp>"

#: 02140000.xhp
msgctxt ""
"02140000.xhp\n"
"par_id3153968\n"
"help.text"
msgid "<image id=\"img_id3150869\" src=\"cmd/sc_numberformatpercent.png\" width=\"5.64mm\" height=\"5.64mm\"><alt id=\"alt_id3150869\">Icon</alt></image>"
msgstr "<image id=\"img_id3150869\" src=\"cmd/sc_numberformatpercent.png\" width=\"5.64mm\" height=\"5.64mm\"><alt id=\"alt_id3150869\">Ikon</alt></image>"

#: 02140000.xhp
msgctxt ""
"02140000.xhp\n"
"par_id3151114\n"
"3\n"
"help.text"
msgid "Number Format: Percent"
msgstr "Számformátum: százalék"

#: 02140000.xhp
msgctxt ""
"02140000.xhp\n"
"bm_id3149260\n"
"help.text"
msgid "<bookmark_value>percentage calculations</bookmark_value>"
msgstr "<bookmark_value>százalékszámítások</bookmark_value>"

#: 02140000.xhp
msgctxt ""
"02140000.xhp\n"
"par_id3149260\n"
"5\n"
"help.text"
msgid "You can also enter a percentage sign (%) after a number in a cell:"
msgstr "Egy százalékjelet (%) is beírhat a cellába a szám után:"

#: 02140000.xhp
msgctxt ""
"02140000.xhp\n"
"par_id3155411\n"
"6\n"
"help.text"
msgid "1% corresponds to 0.01"
msgstr "Az 1% jelentése: 0,01."

#: 02140000.xhp
msgctxt ""
"02140000.xhp\n"
"par_id3145749\n"
"7\n"
"help.text"
msgid "1 + 16% corresponds to 116% or 1.16"
msgstr "Az 1 + 16% jelentése: 116% vagy 1,16."

#: 02140000.xhp
msgctxt ""
"02140000.xhp\n"
"par_id3148575\n"
"8\n"
"help.text"
msgid "1%% corresponds to 0.0001"
msgstr "Az 1%% jelentése: 0,0001."

#: 02140000.xhp
msgctxt ""
"02140000.xhp\n"
"par_id3159153\n"
"10\n"
"help.text"
msgid "<link href=\"text/shared/01/05020300.xhp\" name=\"Format - Cell - Numbers\">Format - Cell - Numbers</link>"
msgstr "<link href=\"text/shared/01/05020300.xhp\" name=\"Formátum - Cellák - Számok\">Formátum - Cellák - Számok</link>"

#: 02150000.xhp
msgctxt ""
"02150000.xhp\n"
"tit\n"
"help.text"
msgid "Number format: Default"
msgstr "Számformátum: általános"

#: 02150000.xhp
msgctxt ""
"02150000.xhp\n"
"hd_id3149182\n"
"1\n"
"help.text"
msgid "<link href=\"text/scalc/02/02150000.xhp\" name=\"Number format: Default\">Number format: Default</link>"
msgstr "<link href=\"text/scalc/02/02150000.xhp\" name=\"Számformátum: általános\">Számformátum: általános</link>"

#: 02150000.xhp
msgctxt ""
"02150000.xhp\n"
"par_id3163802\n"
"2\n"
"help.text"
msgid "<ahelp hid=\".uno:NumberFormatStandard\" visibility=\"visible\">Applies the default number format to the selected cells.</ahelp>"
msgstr "<ahelp hid=\".uno:NumberFormatStandard\" visibility=\"visible\">A kijelölt cellákra az alapértelmezett számformátumot alkalmazza.</ahelp>"

#: 02150000.xhp
msgctxt ""
"02150000.xhp\n"
"par_id3155922\n"
"help.text"
msgid "<image src=\"cmd/sc_numberformatstandard.png\" id=\"img_id3156024\"><alt id=\"alt_id3156024\">Icon</alt></image>"
msgstr "<image src=\"cmd/sc_numberformatstandard.png\" id=\"img_id3156024\"><alt id=\"alt_id3156024\">Ikon</alt></image>"

#: 02150000.xhp
msgctxt ""
"02150000.xhp\n"
"par_id3153361\n"
"3\n"
"help.text"
msgid "Number Format: Standard"
msgstr "Számformátum: általános"

#: 02150000.xhp
msgctxt ""
"02150000.xhp\n"
"par_id3154908\n"
"4\n"
"help.text"
msgid "<link href=\"text/shared/01/05020300.xhp\" name=\"Format - Cell - Numbers\">Format - Cell - Numbers</link>."
msgstr "<link href=\"text/shared/01/05020300.xhp\" name=\"Formátum - Cellák - Számok\">Formátum - Cellák - Számok</link>"

#: 02160000.xhp
msgctxt ""
"02160000.xhp\n"
"tit\n"
"help.text"
msgid "Number Format: Add Decimal Place"
msgstr "Számformátum: tizedesjegy hozzáadása"

#: 02160000.xhp
msgctxt ""
"02160000.xhp\n"
"hd_id3150275\n"
"1\n"
"help.text"
msgid "<link href=\"text/scalc/02/02160000.xhp\" name=\"Number Format: Add Decimal Place\">Number Format: Add Decimal Place</link>"
msgstr "<link href=\"text/scalc/02/02160000.xhp\" name=\"Számformátum: tizedesjegy hozzáadása\">Számformátum: tizedesjegy hozzáadása</link>"

#: 02160000.xhp
msgctxt ""
"02160000.xhp\n"
"par_id3150792\n"
"2\n"
"help.text"
msgid "<ahelp hid=\".uno:NumberFormatIncDecimals\">Adds one decimal place to the numbers in the selected cells.</ahelp>"
msgstr "<ahelp hid=\".uno:NumberFormatIncDecimals\">Hozzáad egy tizedesjegyet a kijelölt cellákban lévő számokhoz.</ahelp>"

#: 02160000.xhp
msgctxt ""
"02160000.xhp\n"
"par_id3145787\n"
"help.text"
msgid "<image id=\"img_id3145271\" src=\"cmd/sc_numberformatincdecimals.png\" width=\"5.64mm\" height=\"5.64mm\"><alt id=\"alt_id3145271\">Icon</alt></image>"
msgstr "<image id=\"img_id3145271\" src=\"cmd/sc_numberformatincdecimals.png\" width=\"5.64mm\" height=\"5.64mm\"><alt id=\"alt_id3145271\">Ikon</alt></image>"

#: 02160000.xhp
msgctxt ""
"02160000.xhp\n"
"par_id3149262\n"
"3\n"
"help.text"
msgid "Number Format: Add Decimal Place"
msgstr "Számformátum: tizedesjegy hozzáadása"

#: 02170000.xhp
msgctxt ""
"02170000.xhp\n"
"tit\n"
"help.text"
msgid "Number Format: Delete Decimal Place"
msgstr "Számformátum: tizedesjegy törlése"

#: 02170000.xhp
msgctxt ""
"02170000.xhp\n"
"hd_id3149164\n"
"1\n"
"help.text"
msgid "<link href=\"text/scalc/02/02170000.xhp\" name=\"Number Format: Delete Decimal Place\">Number Format: Delete Decimal Place</link>"
msgstr "<link href=\"text/scalc/02/02170000.xhp\" name=\"Számformátum: tizedesjegy törlése\">Számformátum: tizedesjegy törlése</link>"

#: 02170000.xhp
msgctxt ""
"02170000.xhp\n"
"par_id3147264\n"
"2\n"
"help.text"
msgid "<ahelp hid=\".uno:NumberFormatDecDecimals\">Removes one decimal place from the numbers in the selected cells.</ahelp>"
msgstr "<ahelp hid=\".uno:NumberFormatDecDecimals\">Eltávolít egy tizedesjegyet a kijelölt cellákban lévő számokról.</ahelp>"

#: 02170000.xhp
msgctxt ""
"02170000.xhp\n"
"par_id3145173\n"
"help.text"
msgid "<image id=\"img_id3153192\" src=\"cmd/sc_numberformatdecdecimals.png\" width=\"5.64mm\" height=\"5.64mm\"><alt id=\"alt_id3153192\">Icon</alt></image>"
msgstr "<image id=\"img_id3153192\" src=\"cmd/sc_numberformatdecdecimals.png\" width=\"5.64mm\" height=\"5.64mm\"><alt id=\"alt_id3153192\">Ikon</alt></image>"

#: 02170000.xhp
msgctxt ""
"02170000.xhp\n"
"par_id3154686\n"
"3\n"
"help.text"
msgid "Number Format: Delete Decimal Place"
msgstr "Számformátum: tizedesjegy törlése"

#: 06010000.xhp
msgctxt ""
"06010000.xhp\n"
"tit\n"
"help.text"
msgid "Sheet Area"
msgstr "Munkalapterület"

#: 06010000.xhp
msgctxt ""
"06010000.xhp\n"
"bm_id3156326\n"
"help.text"
msgid "<bookmark_value>formula bar; sheet area names</bookmark_value><bookmark_value>sheet area names</bookmark_value><bookmark_value>showing; cell references</bookmark_value><bookmark_value>cell references; showing</bookmark_value>"
msgstr "<bookmark_value>képlet eszköztár; munkalapterület-nevek</bookmark_value><bookmark_value>munkalapterület-nevek</bookmark_value><bookmark_value>megjelenítés; cellahivatkozások</bookmark_value><bookmark_value>cellahivatkozások; megjelenítés</bookmark_value>"

#: 06010000.xhp
msgctxt ""
"06010000.xhp\n"
"hd_id3156326\n"
"1\n"
"help.text"
msgid "<link href=\"text/scalc/02/06010000.xhp\" name=\"Name Box\">Name Box</link>"
msgstr "<link href=\"text/scalc/02/06010000.xhp\" name=\"Névdoboz\">Névdoboz</link>"

#: 06010000.xhp
msgctxt ""
"06010000.xhp\n"
"par_id3149656\n"
"2\n"
"help.text"
msgid "<ahelp hid=\"HID_INSWIN_POS\">Displays the reference for the current cell, the range of the selected cells, or the name of the area. You can also select a range of cells, and then type a name for that range into the <emph>Name Box</emph>.</ahelp>"
msgstr "<ahelp hid=\"HID_INSWIN_POS\">Megjeleníti a jelenlegi cellára, a kijelölt cellatartományra vagy a terület nevére mutató hivatkozást. Egy cellatartományt is kiválaszthat, majd megadhat egy nevet a tartományhoz a <emph>Névdoboz</emph> mezőben.</ahelp>"

#: 06010000.xhp
msgctxt ""
"06010000.xhp\n"
"par_id3163710\n"
"help.text"
msgid "<image id=\"img_id3152576\" src=\"res/helpimg/calcein.png\" width=\"1.2398inch\" height=\"0.2398inch\" localize=\"true\"><alt id=\"alt_id3152576\">Combo box sheet area</alt></image>"
msgstr "<image id=\"img_id3152576\" src=\"res/helpimg/calcein.png\" width=\"1.2398inch\" height=\"0.2398inch\" localize=\"true\"><alt id=\"alt_id3152576\">Kombinált lista munkalapterülete</alt></image>"

#: 06010000.xhp
msgctxt ""
"06010000.xhp\n"
"par_id3151118\n"
"4\n"
"help.text"
msgid "Name Box"
msgstr "Névdoboz"

#: 06010000.xhp
msgctxt ""
"06010000.xhp\n"
"par_id3152596\n"
"6\n"
"help.text"
msgid "To jump to a particular cell, or to select a cell range, type the cell reference, or cell range reference in this box, for example, F1, or A1:C4."
msgstr "Egy megadott cellára való ugráshoz vagy egy cellatartomány kijelöléséhez írja be ebbe a mezőbe a cellahivatkozást vagy a cellatartomány-hivatkozást. Például: F1 vagy A1:C4."

#: 06030000.xhp
msgctxt ""
"06030000.xhp\n"
"tit\n"
"help.text"
msgid "Sum"
msgstr "Összeg"

#: 06030000.xhp
msgctxt ""
"06030000.xhp\n"
"bm_id3157909\n"
"help.text"
msgid "<bookmark_value>functions;sum function icon</bookmark_value>         <bookmark_value>formula bar;sum function</bookmark_value>         <bookmark_value>sum icon</bookmark_value>         <bookmark_value>AutoSum button, see sum icon</bookmark_value>"
msgstr "<bookmark_value>függvények;összegzőfüggvény ikonja</bookmark_value>         <bookmark_value>képlet eszköztár;összegzőfüggvény</bookmark_value>         <bookmark_value>összegzés ikon</bookmark_value>         <bookmark_value>automatikus összegzés gomb, lásd összegzés ikon</bookmark_value>"

#: 06030000.xhp
msgctxt ""
"06030000.xhp\n"
"hd_id3157909\n"
"1\n"
"help.text"
msgid "<link href=\"text/scalc/02/06030000.xhp\" name=\"Sum\">Sum</link>"
msgstr "<link href=\"text/scalc/02/06030000.xhp\" name=\"Összeg\">Összeg</link>"

#: 06030000.xhp
msgctxt ""
"06030000.xhp\n"
"par_id3150543\n"
"2\n"
"help.text"
msgid "<ahelp hid=\"HID_INSWIN_SUMME\">Inserts the sum of a cell range into the current cell, or inserts sum values into selected cells. Click in a cell, click this icon, and optionally adjust the cell range. Or select some cells into which the sum values will be inserted, then click the icon.</ahelp>"
msgstr "<ahelp hid=\"HID_INSWIN_SUMME\">Beszúrja egy cellatartomány összegét az aktuális cellába, vagy beszúrja az összegértékeket a kijelölt cellákba. Kattintson egy cellára, kattintson erre az ikonra, majd szükség szerint állítsa be a cellatartományt. Ki is jelölheti azokat a cellákat, amelyekbe az összegértékeket be kívánja szúrni, ezután kattintson az ikonra ezek beszúrásához.</ahelp>"

#: 06030000.xhp
msgctxt ""
"06030000.xhp\n"
"par_id3153770\n"
"help.text"
msgid "<image id=\"img_id3147434\" src=\"cmd/sc_autosum.png\" width=\"0.1665in\" height=\"0.1665in\"><alt id=\"alt_id3147434\">Icon</alt></image>"
msgstr "<image id=\"img_id3147434\" src=\"cmd/sc_autosum.png\" width=\"0.1665in\" height=\"0.1665in\"><alt id=\"alt_id3147434\">Ikon</alt></image>"

#: 06030000.xhp
msgctxt ""
"06030000.xhp\n"
"par_id3152577\n"
"15\n"
"help.text"
msgid "Sum"
msgstr "Összeg"

#: 06030000.xhp
msgctxt ""
"06030000.xhp\n"
"par_id3156444\n"
"16\n"
"help.text"
msgid "$[officename] automatically suggests a cell range, provided that the spreadsheet contains data. If the cell range already contains a sum function, you can combine it with the new one to yield the total sum of the range. If the range contains filters, the Subtotal function is inserted instead of the Sum function."
msgstr "A $[officename] automatikusan felajánl egy cellatartományt, feltéve, hogy a táblázat tartalmaz adatokat. Ha a cellatartomány már tartalmaz összegfüggvényt, akkor azt egyesítheti az újjal a tartomány teljes összegének kiszámításához. Ha a tartomány szűrőket tartalmaz, akkor az összegfüggvény helyett részösszegfüggvény kerül beszúrásra."

#: 06030000.xhp
msgctxt ""
"06030000.xhp\n"
"par_id3153189\n"
"3\n"
"help.text"
msgid "Click the <emph>Accept</emph> icon (green check mark) to use the formula displayed in the input line."
msgstr "A beviteli sorban megjelenő képlet használatához kattintson az <emph>Elfogadás</emph> ikonra (zöld pipa)."

#: 06040000.xhp
msgctxt ""
"06040000.xhp\n"
"tit\n"
"help.text"
msgid "Function"
msgstr "Függvény"

#: 06040000.xhp
msgctxt ""
"06040000.xhp\n"
"bm_id3150084\n"
"help.text"
msgid "<bookmark_value>formula bar; functions</bookmark_value><bookmark_value>functions; formula bar icon</bookmark_value>"
msgstr "<bookmark_value>képlet eszköztár; függvények</bookmark_value><bookmark_value>függvények; képlet eszköztár ikonja</bookmark_value>"

#: 06040000.xhp
msgctxt ""
"06040000.xhp\n"
"hd_id3150084\n"
"1\n"
"help.text"
msgid "<link href=\"text/scalc/02/06040000.xhp\" name=\"Function\">Function</link>"
msgstr "<link href=\"text/scalc/02/06040000.xhp\" name=\"Függvény\">Függvény</link>"

#: 06040000.xhp
msgctxt ""
"06040000.xhp\n"
"par_id3151245\n"
"2\n"
"help.text"
msgid "<ahelp hid=\"HID_INSWIN_FUNC\">Adds a formula to the current cell. Click this icon, and then enter the formula in the <emph>Input line</emph>.</ahelp>"
msgstr "<ahelp hid=\"HID_INSWIN_FUNC\">Egy képletet ad hozzá az aktuális cellához. Kattintson erre az ikonra, majd írja be a képletet a <emph>Beviteli sorba</emph>.</ahelp>"

#: 06040000.xhp
msgctxt ""
"06040000.xhp\n"
"par_id3153360\n"
"3\n"
"help.text"
msgid "This icon is only available when the <emph>Input line</emph> box is not active."
msgstr "Ez az ikon csak akkor érhető el, ha a <emph>Beviteli sor</emph> mező nem aktív."

#: 06040000.xhp
msgctxt ""
"06040000.xhp\n"
"par_id3153770\n"
"help.text"
msgid "<image id=\"img_id3145785\" src=\"sc/imglst/sc26049.png\" width=\"5.64mm\" height=\"5.64mm\"><alt id=\"alt_id3145785\">Icon</alt></image>"
msgstr "<image id=\"img_id3145785\" src=\"sc/imglst/sc26049.png\" width=\"5.64mm\" height=\"5.64mm\"><alt id=\"alt_id3145785\">Ikon</alt></image>"

#: 06040000.xhp
msgctxt ""
"06040000.xhp\n"
"par_id3153951\n"
"4\n"
"help.text"
msgid "Function"
msgstr "Függvény"

#: 06050000.xhp
msgctxt ""
"06050000.xhp\n"
"tit\n"
"help.text"
msgid "Input line"
msgstr "Beviteli sor"

#: 06050000.xhp
msgctxt ""
"06050000.xhp\n"
"hd_id3153821\n"
"1\n"
"help.text"
msgid "<link href=\"text/scalc/02/06050000.xhp\" name=\"Input line\">Input line</link>"
msgstr "<link href=\"text/scalc/02/06050000.xhp\" name=\"Beviteli sor\">Beviteli sor</link>"

#: 06050000.xhp
msgctxt ""
"06050000.xhp\n"
"par_id3155922\n"
"2\n"
"help.text"
msgid "<ahelp hid=\"HID_INSWIN_INPUT\">Enter the formula that you want to add to the current cell. You can also click the <link href=\"text/scalc/01/04060000.xhp\" name=\"Function Wizard\">Function Wizard</link> icon to insert a predefined function into the formula.</ahelp>"
msgstr "<ahelp hid=\"HID_INSWIN_INPUT\">Írja be az aktuális cellába felvenni kívánt képletet. Ezenkívül egy előre meghatározott függvény képletbe való beszúrásához használhatja a <link href=\"text/scalc/01/04060000.xhp\" name=\"Függvénytündér\">Függvénytündér</link> ikont is.</ahelp>"

#: 06060000.xhp
msgctxt ""
"06060000.xhp\n"
"tit\n"
"help.text"
msgid "Cancel"
msgstr "Mégse"

#: 06060000.xhp
msgctxt ""
"06060000.xhp\n"
"bm_id3154514\n"
"help.text"
msgid "<bookmark_value>formula bar; canceling inputs</bookmark_value><bookmark_value>functions; canceling input icon</bookmark_value>"
msgstr "<bookmark_value>képlet eszköztár; bevitelek törlése</bookmark_value><bookmark_value>függvények; bevitelek törlése ikon</bookmark_value>"

#: 06060000.xhp
msgctxt ""
"06060000.xhp\n"
"hd_id3154514\n"
"1\n"
"help.text"
msgid "<link href=\"text/scalc/02/06060000.xhp\" name=\"Cancel\">Cancel</link>"
msgstr "<link href=\"text/scalc/02/06060000.xhp\" name=\"Mégse\">Mégse</link>"

#: 06060000.xhp
msgctxt ""
"06060000.xhp\n"
"par_id3153823\n"
"2\n"
"help.text"
msgid "<ahelp hid=\"HID_INSWIN_CANCEL\">Clears the contents of the <emph>Input line</emph>, or cancels the changes that you made to an existing formula.</ahelp>"
msgstr "<ahelp hid=\"HID_INSWIN_CANCEL\">Törli a <emph>Beviteli sor</emph> tartalmát, vagy visszavonja egy már létező képleten történt változtatásokat.</ahelp>"

#: 06060000.xhp
msgctxt ""
"06060000.xhp\n"
"par_id3156281\n"
"help.text"
msgid "<image id=\"img_id3156422\" src=\"svx/res/nu02.png\" width=\"0.2228inch\" height=\"0.2228inch\"><alt id=\"alt_id3156422\">Icon</alt></image>"
msgstr "<image id=\"img_id3156422\" src=\"svx/res/nu02.png\" width=\"0.2228inch\" height=\"0.2228inch\"><alt id=\"alt_id3156422\">Ikon</alt></image>"

#: 06060000.xhp
msgctxt ""
"06060000.xhp\n"
"par_id3153970\n"
"3\n"
"help.text"
msgid "Cancel"
msgstr "Mégse"

#: 06070000.xhp
msgctxt ""
"06070000.xhp\n"
"tit\n"
"help.text"
msgid "Accept"
msgstr "Elfogadás"

#: 06070000.xhp
msgctxt ""
"06070000.xhp\n"
"bm_id3143267\n"
"help.text"
msgid "<bookmark_value>formula bar; accepting inputs</bookmark_value><bookmark_value>functions; accepting input icon</bookmark_value>"
msgstr "<bookmark_value>képlet eszköztár; bevitelek elfogadása</bookmark_value><bookmark_value>függvények; bevitelek elfogadása ikon</bookmark_value>"

#: 06070000.xhp
msgctxt ""
"06070000.xhp\n"
"hd_id3143267\n"
"1\n"
"help.text"
msgid "<link href=\"text/scalc/02/06070000.xhp\" name=\"Accept\">Accept</link>"
msgstr "<link href=\"text/scalc/02/06070000.xhp\" name=\"Elfogadás\">Elfogadás</link>"

#: 06070000.xhp
msgctxt ""
"06070000.xhp\n"
"par_id3151245\n"
"2\n"
"help.text"
msgid "<ahelp hid=\"HID_INSWIN_OK\">Accepts the contents of the <emph>Input line</emph>, and then inserts the contents into the current cell.</ahelp>"
msgstr "<ahelp hid=\"HID_INSWIN_OK\">Elfogadja a <emph>Beviteli sor</emph> tartalmát, és beírja a tartalmat az aktuális cellába.</ahelp>"

#: 06070000.xhp
msgctxt ""
"06070000.xhp\n"
"par_id3150769\n"
"help.text"
msgid "<image id=\"img_id3156422\" src=\"svx/res/nu01.png\" width=\"5.64mm\" height=\"5.64mm\"><alt id=\"alt_id3156422\">Icon</alt></image>"
msgstr "<image id=\"img_id3156422\" src=\"svx/res/nu01.png\" width=\"5.64mm\" height=\"5.64mm\"><alt id=\"alt_id3156422\">Ikon</alt></image>"

#: 06070000.xhp
msgctxt ""
"06070000.xhp\n"
"par_id3125864\n"
"3\n"
"help.text"
msgid "Accept"
msgstr "Elfogadás"

#: 06080000.xhp
msgctxt ""
"06080000.xhp\n"
"tit\n"
"help.text"
msgid "Theme Selection"
msgstr "Témaválasztás"

#: 06080000.xhp
msgctxt ""
"06080000.xhp\n"
"hd_id3153087\n"
"1\n"
"help.text"
msgid "<link href=\"text/scalc/02/06080000.xhp\" name=\"Theme Selection\">Theme Selection</link>"
msgstr "<link href=\"text/scalc/02/06080000.xhp\" name=\"Témaválasztás\">Témaválasztás</link>"

#: 06080000.xhp
msgctxt ""
"06080000.xhp\n"
"par_id3154515\n"
"2\n"
"help.text"
msgid "<variable id=\"thementext\"><ahelp hid=\".uno:ChooseDesign\">Applies a formatting style to the selected cells.</ahelp></variable> The styles include font, border, and background color information."
msgstr "<variable id=\"thementext\"><ahelp hid=\".uno:ChooseDesign\">Formázási stílust alkalmaz a kijelölt cellákra.</ahelp></variable> A stílus betűkészlet-, szegély- és háttérszín-információkat foglal magában."

#: 06080000.xhp
msgctxt ""
"06080000.xhp\n"
"par_id3155132\n"
"help.text"
msgid "<image id=\"img_id3145785\" src=\"cmd/sc_choosedesign.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3145785\">Icon</alt></image>"
msgstr "<image id=\"img_id3145785\" src=\"cmd/sc_choosedesign.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3145785\">Ikon</alt></image>"

#: 06080000.xhp
msgctxt ""
"06080000.xhp\n"
"par_id3153953\n"
"4\n"
"help.text"
msgid "Choose Themes"
msgstr "Témaválasztás"

#: 06080000.xhp
msgctxt ""
"06080000.xhp\n"
"par_id3147127\n"
"5\n"
"help.text"
msgid "<ahelp hid=\"HID_DLGSTYLES_LISTBOX\">Click the formatting theme that you want to apply, and then click <emph>OK</emph>.</ahelp>"
msgstr "<ahelp hid=\"HID_DLGSTYLES_LISTBOX\">Kattintson az alkalmazni kívánt formázási témára, majd kattintson az <emph>OK</emph> gombra.</ahelp>"

#: 08010000.xhp
msgctxt ""
"08010000.xhp\n"
"tit\n"
"help.text"
msgid "Position in document"
msgstr "Elhelyezkedés a dokumentumban"

#: 08010000.xhp
msgctxt ""
"08010000.xhp\n"
"hd_id3145119\n"
"1\n"
"help.text"
msgid "<link href=\"text/scalc/02/08010000.xhp\" name=\"Position in document\">Position in document</link>"
msgstr "<link href=\"text/scalc/02/08010000.xhp\" name=\"Elhelyezkedés a dokumentumban\">Elhelyezkedés a dokumentumban</link>"

#: 08010000.xhp
msgctxt ""
"08010000.xhp\n"
"par_id3147265\n"
"2\n"
"help.text"
msgid "<ahelp hid=\".uno:StatusDocPos\">Displays the number of the current sheet and the total number of sheets in the spreadsheet.</ahelp>"
msgstr "<ahelp hid=\".uno:StatusDocPos\">Az aktuális munkalap számát és a munkafüzetben lévő munkalapok számát jeleníti meg.</ahelp>"

#: 08080000.xhp
msgctxt ""
"08080000.xhp\n"
"tit\n"
"help.text"
msgid "Standard Formula, Date/Time, Error Warning"
msgstr "Szabványos képlet, Dátum/Idő, Hibafigyelmeztetés"

#: 08080000.xhp
msgctxt ""
"08080000.xhp\n"
"bm_id3147335\n"
"help.text"
msgid "<bookmark_value>formulas;status bar</bookmark_value>"
msgstr "<bookmark_value>képletek;állapotsor</bookmark_value>"

#: 08080000.xhp
msgctxt ""
"08080000.xhp\n"
"hd_id3147335\n"
"1\n"
"help.text"
msgid "<link href=\"text/scalc/02/08080000.xhp\" name=\"Standard Formula, Date/Time, Error Warning\">Standard Formula, Date/Time, Error Warning</link>"
msgstr "<link href=\"text/scalc/02/08080000.xhp\" name=\"Szabványos képlet, Dátum/Idő, Hibafigyelmeztetés\">Szabványos képlet, Dátum/Idő, Hibafigyelmeztetés</link>"

#: 08080000.xhp
msgctxt ""
"08080000.xhp\n"
"par_id3150791\n"
"2\n"
"help.text"
msgid "<ahelp hid=\".uno:StateTableCell\">Displays information about the current document. By default, the SUM of the contents of the selected cells is displayed.</ahelp>"
msgstr "<ahelp hid=\".uno:StateTableCell\">Információt jelenít meg az aktuális dokumentumról. Alapértelmezés szerint a kijelölt cellák összege (SZUM) kerül megjelenítésre.</ahelp>"

#: 08080000.xhp
msgctxt ""
"08080000.xhp\n"
"par_id3155061\n"
"3\n"
"help.text"
msgid "To change the default formula that is displayed, right-click the field, and then choose the formula that you want. The available formulas are: Average, count of values (COUNTA), count of numbers (COUNT), Maximum, Minimum, Sum, or None."
msgstr "A megjelenített alapértelmezett képlet módosításához kattintson a jobb egérgombbal a mezőre, majd válassza ki a kívánt képletet. Az elérhető képletek: Átlag, értékek száma (DARAB2), számok száma (DARAB), Maximum, Minimum, Összeg vagy Nincs."

#: 08080000.xhp
msgctxt ""
"08080000.xhp\n"
"par_id3153969\n"
"4\n"
"help.text"
msgid "<link href=\"text/scalc/05/02140000.xhp\" name=\"Error codes\">Error codes</link>"
msgstr "<link href=\"text/scalc/05/02140000.xhp\" name=\"Hibakódok\">Hibakódok</link>"

#: 10050000.xhp
msgctxt ""
"10050000.xhp\n"
"tit\n"
"help.text"
msgid "Zoom In"
msgstr "Nagyítás"

#: 10050000.xhp
msgctxt ""
"10050000.xhp\n"
"bm_id3148491\n"
"help.text"
msgid "<bookmark_value>page views; increasing scales</bookmark_value><bookmark_value>increasing scales in page view</bookmark_value><bookmark_value>zooming;enlarging page views</bookmark_value>"
msgstr "<bookmark_value>oldalnézetek; méretezés növelése</bookmark_value><bookmark_value>méretezés növelése oldalnézetben</bookmark_value><bookmark_value>nagyítás;oldalnézetek</bookmark_value>"

#: 10050000.xhp
msgctxt ""
"10050000.xhp\n"
"hd_id3148491\n"
"1\n"
"help.text"
msgid "<link href=\"text/scalc/02/10050000.xhp\" name=\"Zoom In\">Zoom In</link>"
msgstr "<link href=\"text/scalc/02/10050000.xhp\" name=\"Nagyítás\">Nagyítás</link>"

#: 10050000.xhp
msgctxt ""
"10050000.xhp\n"
"par_id3145069\n"
"2\n"
"help.text"
msgid "<ahelp hid=\".uno:ZoomIn\">Enlarges the screen display of the current document. The current zoom factor is displayed on the <emph>Status Bar</emph>.</ahelp>"
msgstr "<ahelp hid=\".uno:ZoomIn\">Kinagyítja az aktuális dokumentum megjelenített képét. Az aktuális nagyítási tényező az <emph>Állapotsoron</emph> jelenik meg.</ahelp>"

#: 10050000.xhp
msgctxt ""
"10050000.xhp\n"
"par_id3145171\n"
"4\n"
"help.text"
msgid "The maximum zoom factor is 400%."
msgstr "A maximális nagyítás mértéke 400%."

#: 10050000.xhp
msgctxt ""
"10050000.xhp\n"
"par_id3155854\n"
"help.text"
msgid "<image id=\"img_id3151116\" src=\"cmd/sc_zoomin.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3151116\">Icon</alt></image>"
msgstr "<image id=\"img_id3151116\" src=\"cmd/sc_zoomin.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3151116\">Ikon</alt></image>"

#: 10050000.xhp
msgctxt ""
"10050000.xhp\n"
"par_id3145273\n"
"3\n"
"help.text"
msgid "Zoom In"
msgstr "Nagyítás"

#: 10060000.xhp
msgctxt ""
"10060000.xhp\n"
"tit\n"
"help.text"
msgid "Zoom Out"
msgstr "Kicsinyítés"

#: 10060000.xhp
msgctxt ""
"10060000.xhp\n"
"bm_id3153561\n"
"help.text"
msgid "<bookmark_value>page views;reducing scales</bookmark_value><bookmark_value>zooming;reducing page views</bookmark_value>"
msgstr "<bookmark_value>oldalnézetek;méretezés csökkentése</bookmark_value><bookmark_value>nagyítás;oldalnézetek csökkentése</bookmark_value>"

#: 10060000.xhp
msgctxt ""
"10060000.xhp\n"
"hd_id3153561\n"
"1\n"
"help.text"
msgid "<link href=\"text/scalc/02/10060000.xhp\" name=\"Zoom Out\">Zoom Out</link>"
msgstr "<link href=\"text/scalc/02/10060000.xhp\" name=\"Kicsinyítés\">Kicsinyítés</link>"

#: 10060000.xhp
msgctxt ""
"10060000.xhp\n"
"par_id3151246\n"
"2\n"
"help.text"
msgid "<ahelp hid=\".uno:ZoomOut\">Reduces the screen display of the current document. The current zoom factor is displayed on the <emph>Status Bar</emph>.</ahelp>"
msgstr "<ahelp hid=\".uno:ZoomOut\">Kicsinyíti az aktuális dokumentum megjelenített képét. Az aktuális nagyítási tényező az <emph>Állapotsoron</emph> jelenik meg.</ahelp>"

#: 10060000.xhp
msgctxt ""
"10060000.xhp\n"
"par_id3150398\n"
"4\n"
"help.text"
msgid "The minimum zoom factor is 20%."
msgstr "A maximális kicsinyítés mértéke 20%."

#: 10060000.xhp
msgctxt ""
"10060000.xhp\n"
"par_id3153770\n"
"help.text"
msgid "<image id=\"img_id3155131\" src=\"cmd/sc_zoomout.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3155131\">Icon</alt></image>"
msgstr "<image id=\"img_id3155131\" src=\"cmd/sc_zoomout.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3155131\">Ikon</alt></image>"

#: 10060000.xhp
msgctxt ""
"10060000.xhp\n"
"par_id3150440\n"
"3\n"
"help.text"
msgid "Zooming Out"
msgstr "Kicsinyítés"

#: 18010000.xhp
msgctxt ""
"18010000.xhp\n"
"tit\n"
"help.text"
msgid "Insert"
msgstr "Beszúrás"

#: 18010000.xhp
msgctxt ""
"18010000.xhp\n"
"bm_id3156329\n"
"help.text"
msgid "<bookmark_value>inserting; objects, toolbar icon</bookmark_value>"
msgstr "<bookmark_value>beszúrás; objektumok; eszköztár ikon</bookmark_value>"

#: 18010000.xhp
msgctxt ""
"18010000.xhp\n"
"hd_id3156329\n"
"1\n"
"help.text"
msgid "<link href=\"text/scalc/02/18010000.xhp\" name=\"Insert\">Insert</link>"
msgstr "<link href=\"text/scalc/02/18010000.xhp\" name=\"Beszúrás\">Beszúrás</link>"

#: 18010000.xhp
msgctxt ""
"18010000.xhp\n"
"par_id3147336\n"
"2\n"
"help.text"
msgid "<ahelp hid=\".uno:InsertCtrl\">Click the arrow next to the icon to open the <emph>Insert </emph>toolbar, where you can add graphics and special characters to the current sheet.</ahelp>"
msgstr "<ahelp hid=\".uno:InsertCtrl\">Kattintson az ikon melletti nyílra a <emph>Beszúrás</emph> eszköztár megnyitásához, amelynek segítségével képeket és különleges karaktereket adhat az aktuális munkalaphoz.</ahelp>"

#: 18010000.xhp
msgctxt ""
"18010000.xhp\n"
"par_id3148664\n"
"3\n"
"help.text"
msgid "Tools bar icon:"
msgstr "Eszközök eszköztár ikonja:"

#: 18010000.xhp
msgctxt ""
"18010000.xhp\n"
"par_id3150767\n"
"help.text"
msgid "<image id=\"img_id3156423\" src=\"cmd/sc_insertgraphic.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3156423\">Icon</alt></image>"
msgstr "<image id=\"img_id3156423\" src=\"cmd/sc_insertgraphic.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3156423\">Ikon</alt></image>"

#: 18010000.xhp
msgctxt ""
"18010000.xhp\n"
"par_id3146120\n"
"4\n"
"help.text"
msgid "Insert"
msgstr "Beszúrás"

#: 18010000.xhp
msgctxt ""
"18010000.xhp\n"
"par_id3153188\n"
"6\n"
"help.text"
msgid "You can select the following icons:"
msgstr "A következő ikonokat választhatja ki:"

#: 18010000.xhp
msgctxt ""
"18010000.xhp\n"
"hd_id4283883\n"
"help.text"
msgid "<link href=\"text/shared/01/04160500.xhp\" name=\"Floating Frame\">Floating Frame</link>"
msgstr "<link href=\"text/shared/01/04160500.xhp\" name=\"Úszó keret\">Úszó keret</link>"

#: 18010000.xhp
msgctxt ""
"18010000.xhp\n"
"hd_id3149410\n"
"8\n"
"help.text"
msgid "<link href=\"text/shared/01/04100000.xhp\" name=\"Special Character\">Special Character</link>"
msgstr "<link href=\"text/shared/01/04100000.xhp\" name=\"Különleges karakter\">Különleges karakter</link>"

#: 18010000.xhp
msgctxt ""
"18010000.xhp\n"
"hd_id3151117\n"
"7\n"
"help.text"
msgid "<link href=\"text/shared/01/04140000.xhp\" name=\"From File\">From File</link>"
msgstr "<link href=\"text/shared/01/04140000.xhp\" name=\"Fájlból\">Fájlból</link>"

#: 18010000.xhp
msgctxt ""
"18010000.xhp\n"
"hd_id3633533\n"
"help.text"
msgid "<link href=\"text/shared/01/04160300.xhp\" name=\"Formula\">Formula</link>"
msgstr "<link href=\"text/shared/01/04160300.xhp\" name=\"Képlet\">Képlet</link>"

#: 18010000.xhp
msgctxt ""
"18010000.xhp\n"
"par_idN10769\n"
"help.text"
msgid "<link href=\"text/schart/01/wiz_chart_type.xhp\" name=\"Insert Chart\">Chart</link>"
msgstr "<link href=\"text/schart/01/wiz_chart_type.xhp\" name=\"Diagram beszúrása\">Diagram beszúrása</link>"

#: 18010000.xhp
msgctxt ""
"18010000.xhp\n"
"hd_id7581408\n"
"help.text"
msgid "<link href=\"text/shared/01/04150100.xhp\" name=\"OLE Object\">OLE Object</link>"
msgstr "<link href=\"text/shared/01/04150100.xhp\" name=\"OLE-objektum\">OLE-objektum</link>"

#: 18020000.xhp
msgctxt ""
"18020000.xhp\n"
"tit\n"
"help.text"
msgid "Insert Cells"
msgstr "Cellák beszúrása"

#: 18020000.xhp
msgctxt ""
"18020000.xhp\n"
"bm_id3150275\n"
"help.text"
msgid "<bookmark_value>inserting; cells, toolbar icon</bookmark_value>"
msgstr "<bookmark_value>beszúrás; cellák, eszköztár ikonja</bookmark_value>"

#: 18020000.xhp
msgctxt ""
"18020000.xhp\n"
"hd_id3150275\n"
"1\n"
"help.text"
msgid "<link href=\"text/scalc/02/18020000.xhp\" name=\"Insert Cells\">Insert Cells</link>"
msgstr "<link href=\"text/scalc/02/18020000.xhp\" name=\"Cellák beszúrása\">Cellák beszúrása</link>"

#: 18020000.xhp
msgctxt ""
"18020000.xhp\n"
"par_id3156024\n"
"2\n"
"help.text"
msgid "<ahelp hid=\".uno:InsCellsCtrl\">Click the arrow next to the icon to open the <emph>Insert Cells </emph>toolbar, where you can insert cells, rows, and columns into the current sheet.</ahelp>"
msgstr "<ahelp hid=\".uno:InsCellsCtrl\">Kattintson az ikon melletti nyílra a <emph>Cellák beszúrása</emph> eszköztár megnyitásához, amelynek segítségével cellákat, sorokat és oszlopokat szúrhat be az aktuális munkalapba.</ahelp>"

#: 18020000.xhp
msgctxt ""
"18020000.xhp\n"
"par_id3150398\n"
"3\n"
"help.text"
msgid "Tools bar icon:"
msgstr "Eszközök eszköztár ikonja:"

#: 18020000.xhp
msgctxt ""
"18020000.xhp\n"
"par_id3150767\n"
"5\n"
"help.text"
msgid "You can select the following icons:"
msgstr "A következő ikonokat választhatja ki:"

#: 18020000.xhp
msgctxt ""
"18020000.xhp\n"
"hd_id3150439\n"
"6\n"
"help.text"
msgid "<link href=\"text/scalc/01/04020000.xhp\" name=\"Insert Cells Down\">Insert Cells Down</link>"
msgstr "<link href=\"text/scalc/01/04020000.xhp\" name=\"Cellák beszúrása lefelé\">Cellák beszúrása lefelé</link>"

#: 18020000.xhp
msgctxt ""
"18020000.xhp\n"
"hd_id3146119\n"
"7\n"
"help.text"
msgid "<link href=\"text/scalc/01/04020000.xhp\" name=\"Insert Cells Right\">Insert Cells Right</link>"
msgstr "<link href=\"text/scalc/01/04020000.xhp\" name=\"Cellák beszúrása jobbra\">Cellák beszúrása jobbra</link>"

#: 18020000.xhp
msgctxt ""
"18020000.xhp\n"
"hd_id3153190\n"
"8\n"
"help.text"
msgid "<link href=\"text/scalc/01/04020000.xhp\" name=\"Rows\">Rows</link>"
msgstr "<link href=\"text/scalc/01/04020000.xhp\" name=\"Sorok\">Sorok</link>"

#: 18020000.xhp
msgctxt ""
"18020000.xhp\n"
"hd_id3153726\n"
"9\n"
"help.text"
msgid "<link href=\"text/scalc/01/04020000.xhp\" name=\"Columns\">Columns</link>"
msgstr "<link href=\"text/scalc/01/04020000.xhp\" name=\"Oszlopok\">Oszlopok</link>"

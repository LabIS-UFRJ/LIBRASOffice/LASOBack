#. extracted from sw/source/ui/fldui
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-04-22 23:40+0200\n"
"PO-Revision-Date: 2016-04-29 06:46+0000\n"
"Last-Translator: Pau Iranzo <paugnu@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.7\n"
"X-POOTLE-MTIME: 1461912384.000000\n"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_DATEFLD\n"
"string.text"
msgid "Date"
msgstr "Data"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_TIMEFLD\n"
"string.text"
msgid "Time"
msgstr "Hora"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_FILENAMEFLD\n"
"string.text"
msgid "File name"
msgstr "Nom de fitxer"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_DBNAMEFLD\n"
"string.text"
msgid "Database Name"
msgstr "Nom de la base de dades"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_CHAPTERFLD\n"
"string.text"
msgid "Chapter"
msgstr "Capítol"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_PAGENUMBERFLD\n"
"string.text"
msgid "Page numbers"
msgstr "Números de pàgina"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_DOCSTATFLD\n"
"string.text"
msgid "Statistics"
msgstr "Estadístiques"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_AUTHORFLD\n"
"string.text"
msgid "Author"
msgstr "Autor"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_TEMPLNAMEFLD\n"
"string.text"
msgid "Templates"
msgstr "Plantilles"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_EXTUSERFLD\n"
"string.text"
msgid "Sender"
msgstr "Remitent"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_SETFLD\n"
"string.text"
msgid "Set variable"
msgstr "Defineix la variable"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_GETFLD\n"
"string.text"
msgid "Show variable"
msgstr "Mostra la variable"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_FORMELFLD\n"
"string.text"
msgid "Insert Formula"
msgstr "Insereix la fórmula"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_INPUTFLD\n"
"string.text"
msgid "Input field"
msgstr "Camp d'entrada"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_SETINPUTFLD\n"
"string.text"
msgid "Input field (variable)"
msgstr "Camp d'entrada (variable)"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_USRINPUTFLD\n"
"string.text"
msgid "Input field (user)"
msgstr "Camp d'entrada (usuari)"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_CONDTXTFLD\n"
"string.text"
msgid "Conditional text"
msgstr "Text condicional"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_DDEFLD\n"
"string.text"
msgid "DDE field"
msgstr "Camp DDE"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_MACROFLD\n"
"string.text"
msgid "Execute macro"
msgstr "Executa la macro"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_SEQFLD\n"
"string.text"
msgid "Number range"
msgstr "Interval de nombres"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_SETREFPAGEFLD\n"
"string.text"
msgid "Set page variable"
msgstr "Determina la variable de pàgina"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_GETREFPAGEFLD\n"
"string.text"
msgid "Show page variable"
msgstr "Mostra la variable de la pàgina"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_INTERNETFLD\n"
"string.text"
msgid "Load URL"
msgstr "Carrega l'URL"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_JUMPEDITFLD\n"
"string.text"
msgid "Placeholder"
msgstr "Espai reservat"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_COMBINED_CHARS\n"
"string.text"
msgid "Combine characters"
msgstr "Combina caràcters"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_DROPDOWN\n"
"string.text"
msgid "Input list"
msgstr "Llista d'entrada"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_SETREFFLD\n"
"string.text"
msgid "Set Reference"
msgstr "Defineix la referència"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_GETREFFLD\n"
"string.text"
msgid "Insert Reference"
msgstr "Insereix una referència"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_DBFLD\n"
"string.text"
msgid "Mail merge fields"
msgstr "Camps de combinació de correu"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_DBNEXTSETFLD\n"
"string.text"
msgid "Next record"
msgstr "Registre següent"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_DBNUMSETFLD\n"
"string.text"
msgid "Any record"
msgstr "Qualsevol registre"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_DBSETNUMBERFLD\n"
"string.text"
msgid "Record number"
msgstr "Número de registre"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_PREVPAGEFLD\n"
"string.text"
msgid "Previous page"
msgstr "Pàgina anterior"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_NEXTPAGEFLD\n"
"string.text"
msgid "Next page"
msgstr "Pàgina següent"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_HIDDENTXTFLD\n"
"string.text"
msgid "Hidden text"
msgstr "Text amagat"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_USERFLD\n"
"string.text"
msgid "User Field"
msgstr "Camp d'usuari"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_POSTITFLD\n"
"string.text"
msgid "Note"
msgstr "Nota"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_SCRIPTFLD\n"
"string.text"
msgid "Script"
msgstr "Script"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_AUTHORITY\n"
"string.text"
msgid "Bibliography entry"
msgstr "Entrada bibliogràfica"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_HIDDENPARAFLD\n"
"string.text"
msgid "Hidden Paragraph"
msgstr "Paràgraf amagat"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_DOCINFOFLD\n"
"string.text"
msgid "DocInformation"
msgstr "Informació del document"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_DATE_STD\n"
"string.text"
msgid "Date"
msgstr "Data"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_DATE_FIX\n"
"string.text"
msgid "Date (fixed)"
msgstr "Data (fixa)"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_TIME_STD\n"
"string.text"
msgid "Time"
msgstr "Hora"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_TIME_FIX\n"
"string.text"
msgid "Time (fixed)"
msgstr "Hora (fixa)"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_STAT_TABLE\n"
"string.text"
msgid "Tables"
msgstr "Taules"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_STAT_CHAR\n"
"string.text"
msgid "Characters"
msgstr "Caràcters"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_STAT_WORD\n"
"string.text"
msgid "Words"
msgstr "Paraules"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_STAT_PARA\n"
"string.text"
msgid "Paragraphs"
msgstr "Paràgrafs"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_STAT_GRF\n"
"string.text"
msgid "Image"
msgstr "Imatge"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_STAT_OBJ\n"
"string.text"
msgid "Objects"
msgstr "Objectes"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_STAT_PAGE\n"
"string.text"
msgid "Pages"
msgstr "Pàgines"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_DDE_HOT\n"
"string.text"
msgid "DDE automatic"
msgstr "DDE automàtic"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_DDE_NORMAL\n"
"string.text"
msgid "DDE manual"
msgstr "DDE manual"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_EU_FIRMA\n"
"string.text"
msgid "Company"
msgstr "Empresa"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_EU_VORNAME\n"
"string.text"
msgid "First Name"
msgstr "Nom"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_EU_NAME\n"
"string.text"
msgid "Last Name"
msgstr "Cognoms"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_EU_ABK\n"
"string.text"
msgid "Initials"
msgstr "Inicials"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_EU_STRASSE\n"
"string.text"
msgid "Street"
msgstr "Carrer"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_EU_LAND\n"
"string.text"
msgid "Country"
msgstr "País"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_EU_PLZ\n"
"string.text"
msgid "Zip code"
msgstr "Codi postal"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_EU_ORT\n"
"string.text"
msgid "City"
msgstr "Ciutat"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_EU_TITEL\n"
"string.text"
msgid "Title"
msgstr "Títol"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_EU_POS\n"
"string.text"
msgid "Position"
msgstr "Posició"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_EU_TELPRIV\n"
"string.text"
msgid "Tel. (Home)"
msgstr "Tel. (particular)"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_EU_TELFIRMA\n"
"string.text"
msgid "Tel. (Work)"
msgstr "Tel. (feina)"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_EU_FAX\n"
"string.text"
msgid "FAX"
msgstr "Fax"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_EU_EMAIL\n"
"string.text"
msgid "E-mail"
msgstr "Correu electrònic"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_EU_STATE\n"
"string.text"
msgid "State"
msgstr "Comarca"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_PAGEREF_OFF\n"
"string.text"
msgid "off"
msgstr "desactivat"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FLD_PAGEREF_ON\n"
"string.text"
msgid "on"
msgstr "activat"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_FF_NAME\n"
"string.text"
msgid "File name"
msgstr "Nom de fitxer"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_FF_NAME_NOEXT\n"
"string.text"
msgid "File name without extension"
msgstr "Nom del fitxer sense extensió"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_FF_PATHNAME\n"
"string.text"
msgid "Path/File name"
msgstr "Nom del camí/Fitxer"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_FF_PATH\n"
"string.text"
msgid "Path"
msgstr "Camí"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_FF_UI_NAME\n"
"string.text"
msgid "Style"
msgstr "Estil"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_FF_UI_RANGE\n"
"string.text"
msgid "Category"
msgstr "Categoria"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_CHAPTER_NAME\n"
"string.text"
msgid "Chapter name"
msgstr "Nom del capítol"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_CHAPTER_NO\n"
"string.text"
msgid "Chapter number"
msgstr "Número de capítol"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_CHAPTER_NO_NOSEPARATOR\n"
"string.text"
msgid "Chapter number without separator"
msgstr "Número de capítol sense separador"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_CHAPTER_NAMENO\n"
"string.text"
msgid "Chapter number and name"
msgstr "Número i nom del capítol"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_NUM_ROMAN\n"
"string.text"
msgid "Roman (I II III)"
msgstr "Romans (I II III)"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_NUM_SROMAN\n"
"string.text"
msgid "Roman (i ii iii)"
msgstr "Romans (i ii iii)"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_NUM_ARABIC\n"
"string.text"
msgid "Arabic (1 2 3)"
msgstr "Àrabs (1 2 3)"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_NUM_PAGEDESC\n"
"string.text"
msgid "As Page Style"
msgstr "Com a estil de la pàgina"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_NUM_PAGESPECIAL\n"
"string.text"
msgid "Text"
msgstr "Text"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_AUTHOR_NAME\n"
"string.text"
msgid "Name"
msgstr "Nom"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_AUTHOR_SCUT\n"
"string.text"
msgid "Initials"
msgstr "Inicials"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_SETVAR_SYS\n"
"string.text"
msgid "System"
msgstr "Sistema"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_SETVAR_TEXT\n"
"string.text"
msgid "Text"
msgstr "Text"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_GETVAR_NAME\n"
"string.text"
msgid "Name"
msgstr "Nom"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_GETVAR_TEXT\n"
"string.text"
msgid "Text"
msgstr "Text"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_USERVAR_CMD\n"
"string.text"
msgid "Formula"
msgstr "Fórmula"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_USERVAR_TEXT\n"
"string.text"
msgid "Text"
msgstr "Text"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_DBFLD_DB\n"
"string.text"
msgid "Database"
msgstr "Base de dades"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_DBFLD_SYS\n"
"string.text"
msgid "System"
msgstr "Sistema"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_REG_AUTHOR\n"
"string.text"
msgid "Author"
msgstr "Autor"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_REG_TIME\n"
"string.text"
msgid "Time"
msgstr "Hora"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_REG_DATE\n"
"string.text"
msgid "Date"
msgstr "Data"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_REF_TEXT\n"
"string.text"
msgid "Reference"
msgstr "Referència"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_REF_PAGE\n"
"string.text"
msgid "Page"
msgstr "Pàgina"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_REF_CHAPTER\n"
"string.text"
msgid "Chapter"
msgstr "Capítol"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_REF_UPDOWN\n"
"string.text"
msgid "Above/Below"
msgstr "A sobre/A sota"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_REF_PAGE_PGDSC\n"
"string.text"
msgid "As Page Style"
msgstr "Com a estil de la pàgina"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_REF_ONLYNUMBER\n"
"string.text"
msgid "Category and Number"
msgstr "Categoria i número"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_REF_ONLYCAPTION\n"
"string.text"
msgid "Caption Text"
msgstr "Text de la llegenda"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_REF_ONLYSEQNO\n"
"string.text"
msgid "Numbering"
msgstr "Numeració"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_REF_NUMBER\n"
"string.text"
msgid "Number"
msgstr "Número"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_REF_NUMBER_NO_CONTEXT\n"
"string.text"
msgid "Number (no context)"
msgstr "Número (sense context)"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_REF_NUMBER_FULL_CONTEXT\n"
"string.text"
msgid "Number (full context)"
msgstr "Número (amb tot el context)"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_MARK_TEXT\n"
"string.text"
msgid "Text"
msgstr "Text"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_MARK_TABLE\n"
"string.text"
msgid "Table"
msgstr "Taula"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_MARK_FRAME\n"
"string.text"
msgid "Frame"
msgstr "Marc"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_MARK_GRAFIC\n"
"string.text"
msgid "Image"
msgstr "Imatge"

#: fldui.src
msgctxt ""
"fldui.src\n"
"FMT_MARK_OLE\n"
"string.text"
msgid "Object"
msgstr "Objecte"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_ALL\n"
"string.text"
msgid "All"
msgstr "Tot"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_INSERT\n"
"string.text"
msgid "Insert"
msgstr "Insereix"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_COND\n"
"string.text"
msgid "~Condition"
msgstr "~Condició"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_TEXT\n"
"string.text"
msgid "Then, Else"
msgstr "Aleshores, si no"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_DDE_CMD\n"
"string.text"
msgid "DDE Statement"
msgstr "Expressió DDE"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_INSTEXT\n"
"string.text"
msgid "Hidden t~ext"
msgstr "T~ext amagat"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_MACNAME\n"
"string.text"
msgid "~Macro name"
msgstr "Nom de la ~macro"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_PROMPT\n"
"string.text"
msgid "~Reference"
msgstr "R~eferència"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_COMBCHRS_FT\n"
"string.text"
msgid "Ch~aracters"
msgstr "C~aràcters"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_SETNO\n"
"string.text"
msgid "Record number"
msgstr "Número de registre"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_OFFSET\n"
"string.text"
msgid "O~ffset"
msgstr "D~esplaçament"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_VALUE\n"
"string.text"
msgid "Value"
msgstr "Valor"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_FORMULA\n"
"string.text"
msgid "Formula"
msgstr "Fórmula"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_URLPROMPT\n"
"string.text"
msgid "~URL"
msgstr "~URL"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_ALL_DATABASE\n"
"string.text"
msgid "<All>"
msgstr "<Tot>"

#: fldui.src
msgctxt ""
"fldui.src\n"
"STR_CUSTOM\n"
"string.text"
msgid "Custom"
msgstr "Personalitzat"

#. extracted from svtools/source/contnr
msgid ""
msgstr ""
"Project-Id-Version: OpenOffice.org\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2014-11-21 22:01+0100\n"
"PO-Revision-Date: 2013-01-24 22:37+0100\n"
"Last-Translator: Goran Rakic <grakic@devbase.net>\n"
"Language-Team: Serbian <dev@sr.openoffice.org>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"

#: fileview.src
#, fuzzy
msgctxt ""
"fileview.src\n"
"STR_SVT_FILEVIEW_COLUMN_TITLE\n"
"string.text"
msgid "Title"
msgstr "Naslov"

#: fileview.src
#, fuzzy
msgctxt ""
"fileview.src\n"
"STR_SVT_FILEVIEW_COLUMN_SIZE\n"
"string.text"
msgid "Size"
msgstr "Veličina"

#: fileview.src
msgctxt ""
"fileview.src\n"
"STR_SVT_FILEVIEW_COLUMN_DATE\n"
"string.text"
msgid "Date modified"
msgstr "Datum izmene"

#: fileview.src
#, fuzzy
msgctxt ""
"fileview.src\n"
"STR_SVT_FILEVIEW_COLUMN_TYPE\n"
"string.text"
msgid "Type"
msgstr "Vrsta"

#: fileview.src
msgctxt ""
"fileview.src\n"
"STR_SVT_FILEVIEW_ERR_MAKEFOLDER\n"
"string.text"
msgid "Could not create the folder %1."
msgstr "Nije moguće napraviti fasciklu %1."

#: fileview.src
msgctxt ""
"fileview.src\n"
"STR_SVT_BYTES\n"
"string.text"
msgid "Bytes"
msgstr "B"

#: fileview.src
msgctxt ""
"fileview.src\n"
"STR_SVT_KB\n"
"string.text"
msgid "KB"
msgstr "KB"

#: fileview.src
msgctxt ""
"fileview.src\n"
"STR_SVT_MB\n"
"string.text"
msgid "MB"
msgstr "MB"

#: fileview.src
msgctxt ""
"fileview.src\n"
"STR_SVT_GB\n"
"string.text"
msgid "GB"
msgstr "GB"

#: fileview.src
msgctxt ""
"fileview.src\n"
"RID_FILEVIEW_CONTEXTMENU\n"
"MID_FILEVIEW_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr "~Obriši"

#: fileview.src
msgctxt ""
"fileview.src\n"
"RID_FILEVIEW_CONTEXTMENU\n"
"MID_FILEVIEW_RENAME\n"
"menuitem.text"
msgid "~Rename"
msgstr "~Preimenuj"

#: svcontnr.src
msgctxt ""
"svcontnr.src\n"
"STR_SVT_ACC_DESC_TABLISTBOX\n"
"string.text"
msgid "Row: %1, Column: %2"
msgstr "Red: %1, Kolona: %2"

#: svcontnr.src
msgctxt ""
"svcontnr.src\n"
"STR_SVT_ACC_DESC_FILEVIEW\n"
"string.text"
msgid ", Type: %1, URL: %2"
msgstr ", Vrsta: %1, URL: %2"

#: svcontnr.src
msgctxt ""
"svcontnr.src\n"
"STR_SVT_ACC_DESC_FOLDER\n"
"string.text"
msgid "Folder"
msgstr "Fascikla"

#: svcontnr.src
msgctxt ""
"svcontnr.src\n"
"STR_SVT_ACC_DESC_FILE\n"
"string.text"
msgid "File"
msgstr "Datoteka"

#: svcontnr.src
msgctxt ""
"svcontnr.src\n"
"STR_SVT_ACC_EMPTY_FIELD\n"
"string.text"
msgid "Empty Field"
msgstr "Prazno polje"

#: templwin.src
#, fuzzy
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"Title\n"
"itemlist.text"
msgid "Title"
msgstr "Naslov"

#: templwin.src
#, fuzzy
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"By\n"
"itemlist.text"
msgid "By"
msgstr "Kao"

#: templwin.src
#, fuzzy
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"Date\n"
"itemlist.text"
msgid "Date"
msgstr "Datum"

#: templwin.src
#, fuzzy
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"Keywords\n"
"itemlist.text"
msgid "Keywords"
msgstr "Ključne reči"

#: templwin.src
#, fuzzy
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"Description\n"
"itemlist.text"
msgid "Description"
msgstr "Opis"

#: templwin.src
#, fuzzy
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"Type\n"
"itemlist.text"
msgid "Type"
msgstr "Vrsta"

#: templwin.src
#, fuzzy
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"Modified on\n"
"itemlist.text"
msgid "Modified on"
msgstr "Promenjen na"

#: templwin.src
#, fuzzy
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"Modified by\n"
"itemlist.text"
msgid "Modified by"
msgstr "Promenio"

#: templwin.src
#, fuzzy
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"Printed on\n"
"itemlist.text"
msgid "Printed on"
msgstr "Odštampan na"

#: templwin.src
#, fuzzy
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"Printed by\n"
"itemlist.text"
msgid "Printed by"
msgstr "Odštampao"

#: templwin.src
#, fuzzy
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"Subject\n"
"itemlist.text"
msgid "Subject"
msgstr "Tema"

#: templwin.src
#, fuzzy
msgctxt ""
"templwin.src\n"
"STRARY_SVT_DOCINFO\n"
"Size\n"
"itemlist.text"
msgid "Size"
msgstr "Veličina"

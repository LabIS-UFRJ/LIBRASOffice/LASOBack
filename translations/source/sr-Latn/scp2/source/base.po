#. extracted from scp2/source/base
msgid ""
msgstr ""
"Project-Id-Version: OpenOffice.org\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2014-11-21 22:01+0100\n"
"PO-Revision-Date: 2012-06-18 20:01+0200\n"
"Last-Translator: Goran Rakic <grakic@devbase.net>\n"
"Language-Team: Serbian <http://gitorious.org/ooo-sr>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-Project-Style: openoffice\n"

#: folderitem_base.ulf
msgctxt ""
"folderitem_base.ulf\n"
"STR_FI_TOOLTIP_BASE\n"
"LngText.text"
msgid "Manage databases, create queries and reports to track and manage your information by using Base."
msgstr "Upravljajte bazama podataka, napravite upite i izveštaje za praćenje podataka u programu Baza."

#: module_base.ulf
msgctxt ""
"module_base.ulf\n"
"STR_NAME_MODULE_PRG_BASE\n"
"LngText.text"
msgid "%PRODUCTNAME Base"
msgstr "%PRODUCTNAME Baze"

#: module_base.ulf
msgctxt ""
"module_base.ulf\n"
"STR_DESC_MODULE_PRG_BASE\n"
"LngText.text"
msgid "Create and edit databases by using %PRODUCTNAME Base."
msgstr "Napravite baze podataka i uređujte ih u %PRODUCTNAME Bazi."

#: module_base.ulf
msgctxt ""
"module_base.ulf\n"
"STR_NAME_MODULE_PRG_BASE_BIN\n"
"LngText.text"
msgid "Program Module"
msgstr "Programski modul"

#: module_base.ulf
msgctxt ""
"module_base.ulf\n"
"STR_DESC_MODULE_PRG_BASE_BIN\n"
"LngText.text"
msgid "The application %PRODUCTNAME Base"
msgstr "Program %PRODUCTNAME Baze"

#: module_base.ulf
msgctxt ""
"module_base.ulf\n"
"STR_NAME_MODULE_PRG_BASE_HELP\n"
"LngText.text"
msgid "%PRODUCTNAME Base Help"
msgstr "Pomoć za %PRODUCTNAME Baze"

#: module_base.ulf
msgctxt ""
"module_base.ulf\n"
"STR_DESC_MODULE_PRG_BASE_HELP\n"
"LngText.text"
msgid "Help about %PRODUCTNAME Base"
msgstr "Pomoć za %PRODUCTNAME Baze"

#: postgresqlsdbc.ulf
msgctxt ""
"postgresqlsdbc.ulf\n"
"STR_NAME_MODULE_OPTIONAL_EXTENSIONS_POSTGRESQLSDBC\n"
"LngText.text"
msgid "PostgreSQL Connector"
msgstr "Povezivanje na PostgreSQL"

#: postgresqlsdbc.ulf
#, fuzzy
msgctxt ""
"postgresqlsdbc.ulf\n"
"STR_DESC_MODULE_OPTIONAL_EXTENSIONS_POSTGRESQLSDBC\n"
"LngText.text"
msgid "PostgreSQL Connector"
msgstr "Povezivanje na PostgreSQL"

#: registryitem_base.ulf
msgctxt ""
"registryitem_base.ulf\n"
"STR_REG_VAL_OO_DATABASE\n"
"LngText.text"
msgid "OpenDocument Database"
msgstr "ODF baza podataka"

#. extracted from helpcontent2/source/text/shared/07
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-12-11 12:58+0100\n"
"PO-Revision-Date: 2013-05-24 13:20+0000\n"
"Last-Translator: system user <>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: tg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 2.7\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1369401635.000000\n"

#: 09000000.xhp
msgctxt ""
"09000000.xhp\n"
"tit\n"
"help.text"
msgid "Web Pages"
msgstr "Веб Саҳифаҳо"

#: 09000000.xhp
msgctxt ""
"09000000.xhp\n"
"hd_id3156014\n"
"1\n"
"help.text"
msgid "<link href=\"text/shared/07/09000000.xhp\" name=\"Web Pages\">Web Pages</link>"
msgstr "<link href=\"text/shared/07/09000000.xhp\" name=\"Web Pages\">Веб Саҳифаҳо</link>"

#: 09000000.xhp
msgctxt ""
"09000000.xhp\n"
"par_id3146946\n"
"2\n"
"help.text"
msgid "To create a new web page for the Internet, open a new <emph>HTML Document</emph> by choosing <emph>File - New</emph>."
msgstr "Барои сохтани Веб саҳифаи Интернетӣ <emph>Ҳуҷҷати HTML</emph>и нав <emph>Дафтар - Нав</emph>ро интихоб кунед."

#: 09000000.xhp
#, fuzzy
msgctxt ""
"09000000.xhp\n"
"par_id3143284\n"
"3\n"
"help.text"
msgid "A tool for creating new web pages is the Web Layout mode, which you enable with <emph>View - Web</emph>."
msgstr "Асбоб барои сохтани Веб саҳифа режими Веб мебошад, ки онро бо фармони <emph>Намоиш - Тарҳи Веб</emph> фаъол мегардонед."

#: 09000000.xhp
msgctxt ""
"09000000.xhp\n"
"hd_id3147285\n"
"8\n"
"help.text"
msgid "Creating a New Web Page"
msgstr "Сохтани Веб Саҳифаи Нав"

#: 09000000.xhp
#, fuzzy
msgctxt ""
"09000000.xhp\n"
"par_id3150808\n"
"9\n"
"help.text"
msgid "Switch to the web layout mode by choosing <emph>View - Web</emph> or by opening a new HTML document."
msgstr "Ба Тарҳи Веб гузаштан таввасути фармони <emph>Намоиш - Тарҳи Веб</emph> ё худ бо кушодани дафтари нави HTML муяссар мегардад."

#: 09000000.xhp
msgctxt ""
"09000000.xhp\n"
"par_id3145136\n"
"15\n"
"help.text"
msgid "To create an HTML page from your $[officename] document, save the page using one of the \"HTML Document\" file types."
msgstr "Барои сохтани ҳуҷҷати HTML дар ҳуҷҷати $[officename], саҳифаро бо истифода аз яке типҳои Ҳуҷҷати HTML\"HTML Document\" сабт кунед."

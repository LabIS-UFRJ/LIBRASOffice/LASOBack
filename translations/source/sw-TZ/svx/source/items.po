#. extracted from svx/source/items
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2015-12-11 20:19+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: sw_TZ\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1449865163.000000\n"

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCTX\n"
"ERRCTX_SVX_LINGU_THESAURUS&ERRCODE_RES_MASK\n"
"string.text"
msgid "$(ERR) executing the thesaurus."
msgstr "$(ERR) inachakata thesauri."

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCTX\n"
"ERRCTX_SVX_LINGU_SPELLING&ERRCODE_RES_MASK\n"
"string.text"
msgid "$(ERR) executing the spellcheck."
msgstr "$(ERR) inachakata kagualahaja."

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCTX\n"
"ERRCTX_SVX_LINGU_HYPHENATION&ERRCODE_RES_MASK\n"
"string.text"
msgid "$(ERR) executing the hyphenation."
msgstr "$(ERR) uchakataji wa uunganishaji."

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCTX\n"
"ERRCTX_SVX_LINGU_DICTIONARY&ERRCODE_RES_MASK\n"
"string.text"
msgid "$(ERR) creating a dictionary."
msgstr "$(ERR) kamusi inaundwa."

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCTX\n"
"ERRCTX_SVX_BACKGROUND&ERRCODE_RES_MASK\n"
"string.text"
msgid "$(ERR) setting background attribute."
msgstr "$(ERR) usetishaji wa sifa usuli."

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCTX\n"
"ERRCTX_SVX_IMPORT_GRAPHIC&ERRCODE_RES_MASK\n"
"string.text"
msgid "$(ERR) loading the graphics."
msgstr "$(ERR) michoro inapakiwa."

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCODE\n"
"ERRCODE_SVX_LINGU_THESAURUSNOTEXISTS&ERRCODE_RES_MASK\n"
"string.text"
msgid ""
"No thesaurus available for the current language.\n"
"Please check your installation and install the desired language."
msgstr ""
"Hakuna thesauri iliyopo kwa lugha ya sasa.\n"
"Tafadhali kagua usakinishaji na sakinisha lugha utakayo"

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCODE\n"
"ERRCODE_SVX_LINGU_LANGUAGENOTEXISTS&ERRCODE_RES_MASK\n"
"string.text"
msgid ""
"$(ARG1) is not supported by the spellcheck function or is not presently active.\n"
"Please check your installation and, if necessary, install the required language module\n"
" or activate it under 'Tools - Options -  Language Settings - Writing Aids'."
msgstr ""
"$(ARG1) haina mwega kwa amali ya ukaguzitahajia au sasa hivi haiko hai.\n"
"Tafadhali kagua usakinishaji wako na kama lazima, sakinisha moduli inayohitajika\n"
"au ianzishe kwenye 'Zana - Chaguzi - Vipimo Lugha - Misaada Uandishi'."

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCODE\n"
"ERRCODE_SVX_LINGU_LINGUNOTEXISTS&ERRCODE_RES_MASK\n"
"string.text"
msgid "Spellcheck is not available."
msgstr "Kikagualahaja hakipo."

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCODE\n"
"ERRCODE_SVX_LINGU_HYPHENNOTEXISTS&ERRCODE_RES_MASK\n"
"string.text"
msgid "Hyphenation not available."
msgstr "Uunganishaji haupatikani."

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCODE\n"
"ERRCODE_SVX_LINGU_DICT_NOTREADABLE&ERRCODE_RES_MASK\n"
"string.text"
msgid "The custom dictionary $(ARG1) cannot be read."
msgstr "Kamusi ya kienyeji $(ARG1) haikuwezekana kusomeka."

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCODE\n"
"ERRCODE_SVX_LINGU_DICT_NOTWRITEABLE&ERRCODE_RES_MASK\n"
"string.text"
msgid "The custom dictionary $(ARG1) cannot be created."
msgstr "Kamusi ya kienyeji $(ARG1) haikuwezekana kuundwa."

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCODE\n"
"ERRCODE_SVX_GRAPHIC_NOTREADABLE&ERRCODE_RES_MASK\n"
"string.text"
msgid "The graphic $(ARG1) could not be found."
msgstr "Mchoro $(ARG1) haukuweza kupatikana."

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCODE\n"
"ERRCODE_SVX_GRAPHIC_WRONG_FILEFORMAT&ERRCODE_RES_MASK\n"
"string.text"
msgid "An unlinked graphic could not be loaded."
msgstr "Michoro isiyounganishwa haikuweza kupakiwa"

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCODE\n"
"ERRCODE_SVX_LINGU_NOLANGUAGE&ERRCODE_RES_MASK\n"
"string.text"
msgid "A language has not been fixed for the selected term."
msgstr "Lugha haikurekebishwa kwa istilahi iliyoteuliwa."

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCODE\n"
"(ERRCODE_SVX_FORMS_NOIOSERVICES | ERRCODE_CLASS_READ) & ERRCODE_RES_MASK\n"
"string.text"
msgid "The form layer wasn't loaded as the required IO-services (com.sun.star.io.*) could not be instantiated."
msgstr ""

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCODE\n"
"(ERRCODE_SVX_FORMS_NOIOSERVICES | ERRCODE_CLASS_WRITE) & ERRCODE_RES_MASK\n"
"string.text"
msgid "The form layer wasn't written as the required IO services (com.sun.star.io.*) could not be instantiated."
msgstr ""

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCODE\n"
"(ERRCODE_SVX_FORMS_READWRITEFAILED | ERRCODE_CLASS_READ) & ERRCODE_RES_MASK\n"
"string.text"
msgid "An error occurred while reading the form controls. The form layer has not been loaded."
msgstr "Hitilafu imetokea vidhibiti fomu vilipokua vinasomwa. Tabaka la fomu halikuweza kupakiwa"

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCODE\n"
"(ERRCODE_SVX_FORMS_READWRITEFAILED | ERRCODE_CLASS_WRITE) & ERRCODE_RES_MASK\n"
"string.text"
msgid "An error occurred while writing the form controls. The form layer has not been saved."
msgstr "Hitilafu imetokea wakati wa kuandika vidhibiti fomu. Tabaka la fomu halijahifadhiwa"

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCODE\n"
"(ERRCODE_SVX_BULLETITEM_NOBULLET | ERRCODE_CLASS_READ) & ERRCODE_RES_MASK\n"
"string.text"
msgid "An error occurred while reading one of the bullets. Not all of the bullets were loaded."
msgstr "Hitilafu imetokea wakati tobo mojawapo ilipokua inasomwa. Sio tobo zote zimepakiwa."

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCODE\n"
"ERRCODE_SVX_MODIFIED_VBASIC_STORAGE & ERRCODE_RES_MASK\n"
"string.text"
msgid "All changes to the Basic Code are lost. The original VBA Macro Code is saved instead."
msgstr ""

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCODE\n"
"ERRCODE_SVX_VBASIC_STORAGE_EXIST & ERRCODE_RES_MASK\n"
"string.text"
msgid "The original VBA Basic Code contained in the document will not be saved."
msgstr "Misimbo VBA asili iliyoko kwenye andiko haitaweza kuhifadhiwa."

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCODE\n"
"ERRCODE_SVX_WRONGPASS & ERRCODE_RES_MASK\n"
"string.text"
msgid "The password is incorrect. The document cannot be opened."
msgstr "The password is incorrect. The document cannot be opened."

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCODE\n"
"ERRCODE_SVX_READ_FILTER_CRYPT & ERRCODE_RES_MASK\n"
"string.text"
msgid "The encryption method used in this document is not supported. Only Microsoft Office 97/2000 compatible password encryption is supported."
msgstr "The encryption method used in this document is not supported. Only Microsoft Office 97/2000 compatible password encryption is supported."

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCODE\n"
"ERRCODE_SVX_READ_FILTER_PPOINT & ERRCODE_RES_MASK\n"
"string.text"
msgid "The loading of password-encrypted Microsoft PowerPoint presentations is not supported."
msgstr "The loading of password-encrypted Microsoft PowerPoint presentations is not supported."

#: svxerr.src
msgctxt ""
"svxerr.src\n"
"RID_SVXERRCODE\n"
"ERRCODE_SVX_EXPORT_FILTER_CRYPT & ERRCODE_RES_MASK\n"
"string.text"
msgid ""
"Password protection is not supported when documents are saved in a Microsoft Office format.\n"
"Do you want to save the document without password protection?"
msgstr ""
"Password protection is not supported when documents are saved in a Microsoft Office format.\n"
"Do you want to save the document without password protection"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Scale\n"
"itemlist.text"
msgid "Scale"
msgstr "Skeli"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Brush\n"
"itemlist.text"
msgid "Brush"
msgstr "Brashi"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Tab stops\n"
"itemlist.text"
msgid "Tab stops"
msgstr "Tab stops"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Character\n"
"itemlist.text"
msgid "Character"
msgstr "Kiwambo"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Font\n"
"itemlist.text"
msgid "Font"
msgstr "Fonti"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Font posture\n"
"itemlist.text"
msgid "Font posture"
msgstr "Mkao fonti"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Font weight\n"
"itemlist.text"
msgid "Font weight"
msgstr "Uzito fonti"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Shadowed\n"
"itemlist.text"
msgid "Shadowed"
msgstr "Iliyovulishwa"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Individual words\n"
"itemlist.text"
msgid "Individual words"
msgstr "Maneno binasfi"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Outline\n"
"itemlist.text"
msgid "Outline"
msgstr "Outline"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Strikethrough\n"
"itemlist.text"
msgid "Strikethrough"
msgstr "Strikethrough"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Underline\n"
"itemlist.text"
msgid "Underline"
msgstr "Pigia mstari"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Font size\n"
"itemlist.text"
msgid "Font size"
msgstr "Saizi fonti"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Rel. Font size\n"
"itemlist.text"
msgid "Rel. Font size"
msgstr "Saizi Fonti Linganifu"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Font color\n"
"itemlist.text"
msgid "Font color"
msgstr "Font color"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Kerning\n"
"itemlist.text"
msgid "Kerning"
msgstr "Kerning"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Effects\n"
"itemlist.text"
msgid "Effects"
msgstr "Athari"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Language\n"
"itemlist.text"
msgid "Language"
msgstr "Lugha"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Position\n"
"itemlist.text"
msgid "Position"
msgstr "Position"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Character blinking\n"
"itemlist.text"
msgid "Character blinking"
msgstr ""

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Character set color\n"
"itemlist.text"
msgid "Character set color"
msgstr "Seti rangi kiwambo"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Overline\n"
"itemlist.text"
msgid "Overline"
msgstr ""

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Paragraph\n"
"itemlist.text"
msgid "Paragraph"
msgstr "Aya"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Alignment\n"
"itemlist.text"
msgid "Alignment"
msgstr "Alignment"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Line spacing\n"
"itemlist.text"
msgid "Line spacing"
msgstr "Line spacing"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Page Break\n"
"itemlist.text"
msgid "Page Break"
msgstr "Mkato Ukurasa"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Hyphenation\n"
"itemlist.text"
msgid "Hyphenation"
msgstr "Hyphenation"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Do not split paragraph\n"
"itemlist.text"
msgid "Do not split paragraph"
msgstr "Usivunje aya"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Orphans\n"
"itemlist.text"
msgid "Orphans"
msgstr "Madirisha"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Widows\n"
"itemlist.text"
msgid "Widows"
msgstr "Yatima"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Paragraph spacing\n"
"itemlist.text"
msgid "Paragraph spacing"
msgstr ""

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Paragraph indent\n"
"itemlist.text"
msgid "Paragraph indent"
msgstr ""

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Indent\n"
"itemlist.text"
msgid "Indent"
msgstr "Indent"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Spacing\n"
"itemlist.text"
msgid "Spacing"
msgstr "Unafasishaji"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Page\n"
"itemlist.text"
msgid "Page"
msgstr "Page"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Page Style\n"
"itemlist.text"
msgid "Page Style"
msgstr "Page Style"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Keep with next paragraph\n"
"itemlist.text"
msgid "Keep with next paragraph"
msgstr "Bakia na aya inayofuata"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Blinking\n"
"itemlist.text"
msgid "Blinking"
msgstr "Upepeso"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Register-true\n"
"itemlist.text"
msgid "Register-true"
msgstr "Sajili-kweli"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Character background\n"
"itemlist.text"
msgid "Character background"
msgstr "Usuli kiwambo"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Asian font\n"
"itemlist.text"
msgid "Asian font"
msgstr "Fonti ya kiasia"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Size of Asian font\n"
"itemlist.text"
msgid "Size of Asian font"
msgstr "Ukubwa wa fonti ya Kiasia"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Language of Asian font\n"
"itemlist.text"
msgid "Language of Asian font"
msgstr "Lugha ya fonti za Kiasia"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Posture of Asian font\n"
"itemlist.text"
msgid "Posture of Asian font"
msgstr "Mkao wa fonti za Kiasia"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Weight of Asian font\n"
"itemlist.text"
msgid "Weight of Asian font"
msgstr "Uzito wa fonti za Kiasia"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"CTL\n"
"itemlist.text"
msgid "CTL"
msgstr "CTL"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Size of complex scripts\n"
"itemlist.text"
msgid "Size of complex scripts"
msgstr "Saizi ya hati changamano"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Language of complex scripts\n"
"itemlist.text"
msgid "Language of complex scripts"
msgstr "Lugha ya hati changamano"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Posture of complex scripts\n"
"itemlist.text"
msgid "Posture of complex scripts"
msgstr "Mkao wa hati changamani"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Weight of complex scripts\n"
"itemlist.text"
msgid "Weight of complex scripts"
msgstr "Uzito wa hati changamano"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Double-lined\n"
"itemlist.text"
msgid "Double-lined"
msgstr "Double-lined"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Emphasis mark\n"
"itemlist.text"
msgid "Emphasis mark"
msgstr "Alama ya kutia maanani"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Text spacing\n"
"itemlist.text"
msgid "Text spacing"
msgstr "Unafasishaji matini"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Hanging punctuation\n"
"itemlist.text"
msgid "Hanging punctuation"
msgstr "Utuzaji ning'nivu"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Forbidden characters\n"
"itemlist.text"
msgid "Forbidden characters"
msgstr "Viwambo haramu"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Rotation\n"
"itemlist.text"
msgid "Rotation"
msgstr "Mzunguko"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Character scaling\n"
"itemlist.text"
msgid "Character scaling"
msgstr ""

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Relief\n"
"itemlist.text"
msgid "Relief"
msgstr "Relief"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_ATTR_NAMES\n"
"Vertical text alignment\n"
"itemlist.text"
msgid "Vertical text alignment"
msgstr "Upangaji wima matini"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_SEARCHCMD_FIND\n"
"string.text"
msgid "Search"
msgstr "Tafuta"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_SEARCHCMD_FIND_ALL\n"
"string.text"
msgid "Find All"
msgstr "Fafuta yote"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_SEARCHCMD_REPLACE\n"
"string.text"
msgid "Replace"
msgstr "Badilisha"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_SEARCHCMD_REPLACE_ALL\n"
"string.text"
msgid "Replace all"
msgstr "Badilisha yote"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_SEARCHSTYL_CHAR\n"
"string.text"
msgid "Character Style"
msgstr "Character Style"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_SEARCHSTYL_PARA\n"
"string.text"
msgid "Paragraph Style"
msgstr "Paragraph Style"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_SEARCHSTYL_FRAME\n"
"string.text"
msgid "Frame Style"
msgstr "Frame Style"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_SEARCHSTYL_PAGE\n"
"string.text"
msgid "Page Style"
msgstr "Page Style"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_SEARCHIN_FORMULA\n"
"string.text"
msgid "Formula"
msgstr "Fomula"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_SEARCHIN_VALUE\n"
"string.text"
msgid "Value"
msgstr "Value"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_SEARCHIN_NOTE\n"
"string.text"
msgid "Note"
msgstr "Note"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_BRUSHSTYLE_NULL\n"
"string.text"
msgid "None"
msgstr "Bila"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_BRUSHSTYLE_SOLID\n"
"string.text"
msgid "Solid"
msgstr "Solid"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_BRUSHSTYLE_HORZ\n"
"string.text"
msgid "Horizontal"
msgstr "Mlalo"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_BRUSHSTYLE_VERT\n"
"string.text"
msgid "Vertical"
msgstr "Wima"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_BRUSHSTYLE_CROSS\n"
"string.text"
msgid "Grid"
msgstr "Gridi"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_BRUSHSTYLE_DIAGCROSS\n"
"string.text"
msgid "Diamond"
msgstr "Almasi"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_BRUSHSTYLE_UPDIAG\n"
"string.text"
msgid "Diagonal up"
msgstr "Mshazari kwenda juu"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_BRUSHSTYLE_DOWNDIAG\n"
"string.text"
msgid "Diagonal down"
msgstr "Mshazari kwenda chini"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_BRUSHSTYLE_BITMAP\n"
"string.text"
msgid "Image"
msgstr "Taswira"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_ORI_STANDARD\n"
"string.text"
msgid "Default orientation"
msgstr "Default orientation"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_ORI_TOPBOTTOM\n"
"string.text"
msgid "From top to bottom"
msgstr "Kutoka juu kwenda chini"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_ORI_BOTTOMTOP\n"
"string.text"
msgid "Bottom to Top"
msgstr "Chini kwenda Juu"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_ORI_STACKED\n"
"string.text"
msgid "Stacked"
msgstr "Stacked"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_BOXINF_TABLE_TRUE\n"
"string.text"
msgid "Table"
msgstr "Jedwali"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_BOXINF_TABLE_FALSE\n"
"string.text"
msgid "Not Table"
msgstr "Sio Jedwali"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_BOXINF_DIST_TRUE\n"
"string.text"
msgid "Spacing enabled"
msgstr "Unafasishaji umewezeshwa"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_BOXINF_DIST_FALSE\n"
"string.text"
msgid "Spacing disabled"
msgstr "Unafasishaji haujawezeshwa"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_BOXINF_MDIST_TRUE\n"
"string.text"
msgid "Keep spacing interval"
msgstr "Tunza umbali wa unafasishaji"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_BOXINF_MDIST_FALSE\n"
"string.text"
msgid "Allowed to fall short of spacing interval"
msgstr "Inaruhusiwa kupungukiwa kwa nafasi"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_MARGIN_LEFT\n"
"string.text"
msgid "Left margin: "
msgstr "Pambizo kushoto: "

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_MARGIN_TOP\n"
"string.text"
msgid "Top margin: "
msgstr "Pambizo juu: "

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_MARGIN_RIGHT\n"
"string.text"
msgid "Right margin: "
msgstr "Pambizo kulia: "

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_MARGIN_BOTTOM\n"
"string.text"
msgid "Bottom margin: "
msgstr "Pambizo chini: "

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_PAGE_COMPLETE\n"
"string.text"
msgid "Page Description: "
msgstr "Maelezo Ukurasa: "

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_PAGE_NUM_CHR_UPPER\n"
"string.text"
msgid "Capitals"
msgstr "Herufi kubwa"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_PAGE_NUM_CHR_LOWER\n"
"string.text"
msgid "Lowercase"
msgstr "Lowercase"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_PAGE_NUM_ROM_UPPER\n"
"string.text"
msgid "Uppercase Roman"
msgstr "Herufi kubwa za Kiroma"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_PAGE_NUM_ROM_LOWER\n"
"string.text"
msgid "Lowercase Roman"
msgstr "Herufi ndogo za Kiroma"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_PAGE_NUM_ARABIC\n"
"string.text"
msgid "Arabic"
msgstr "Kiarabu"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_PAGE_NUM_NONE\n"
"string.text"
msgid "None"
msgstr "Bila"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_PAGE_LAND_TRUE\n"
"string.text"
msgid "Landscape"
msgstr "Landscape"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_PAGE_LAND_FALSE\n"
"string.text"
msgid "Portrait"
msgstr "Portrait"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_PAGE_USAGE_LEFT\n"
"string.text"
msgid "Left"
msgstr "Kushoto"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_PAGE_USAGE_RIGHT\n"
"string.text"
msgid "Right"
msgstr "Kulia"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_PAGE_USAGE_ALL\n"
"string.text"
msgid "All"
msgstr "All"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_PAGE_USAGE_MIRROR\n"
"string.text"
msgid "Mirrored"
msgstr "Iliyoakisiwa"

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_AUTHOR_COMPLETE\n"
"string.text"
msgid "Author: "
msgstr "Mtunzi: "

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_DATE_COMPLETE\n"
"string.text"
msgid "Date: "
msgstr "Tarehe: "

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_TEXT_COMPLETE\n"
"string.text"
msgid "Text: "
msgstr "Matini: "

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_BACKGROUND_COLOR\n"
"string.text"
msgid "Background color: "
msgstr "Rangi usuli: "

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_PATTERN_COLOR\n"
"string.text"
msgid "Pattern color: "
msgstr "Rangi ruwaza: "

#: svxitems.src
msgctxt ""
"svxitems.src\n"
"RID_SVXITEMS_BRUSH_CHAR\n"
"string.text"
msgid "Character background"
msgstr "Usuli kiwambo"

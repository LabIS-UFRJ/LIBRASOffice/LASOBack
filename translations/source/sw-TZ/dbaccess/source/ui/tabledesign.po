#. extracted from dbaccess/source/ui/tabledesign
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-03-11 22:49+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: sw_TZ\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1457736593.000000\n"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABLEDESIGN_DBFIELDTYPES\n"
"string.text"
msgid "Unknown;Text;Number;Date/Time;Date;Time;Yes/No;Currency;Memo;Counter;Image;Text (fix);Decimal;Binary (fix);Binary;BigInt;Double;Float;Real;Integer;Small Integer;Tiny Integer;SQL Null;Object;Distinct;Structure;Field;BLOB;CLOB;REF;OTHER;Bit (fix)"
msgstr "Unknown;Text;Number;Date/Time;Date;Time;Yes/No;Currency;Memo;Counter;Image;Text(fix);Decimal;Binary(fix);Binary;BigInt;Double;Float;Real;Integer;Small Integer;Tiny Integer;SQL Null;Object;Distinct;Structure;Field;BLOB;CLOB;REF;OTHER"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABLEDESIGN_UNDO_PRIMKEY\n"
"string.text"
msgid "Insert/remove primary key"
msgstr "Chomeka/ondoa ufunguo awali"

#: table.src
msgctxt ""
"table.src\n"
"STR_VALUE_YES\n"
"string.text"
msgid "Yes"
msgstr "Yes"

#: table.src
msgctxt ""
"table.src\n"
"STR_VALUE_NO\n"
"string.text"
msgid "No"
msgstr "Hapana"

#: table.src
msgctxt ""
"table.src\n"
"STR_VALUE_ASC\n"
"string.text"
msgid "Ascending"
msgstr "Ascending"

#: table.src
msgctxt ""
"table.src\n"
"STR_VALUE_DESC\n"
"string.text"
msgid "Descending"
msgstr "Descending"

#: table.src
msgctxt ""
"table.src\n"
"STR_VALUE_NONE\n"
"string.text"
msgid "<none>"
msgstr "<bila>"

#: table.src
msgctxt ""
"table.src\n"
"STR_TAB_FIELD_NAME\n"
"string.text"
msgid "Field name"
msgstr "Jina la Uga"

#: table.src
#, fuzzy
msgctxt ""
"table.src\n"
"STR_TAB_FIELD_COLUMN_NAME\n"
"string.text"
msgid "Field Name"
msgstr "Jina la Uga"

#: table.src
msgctxt ""
"table.src\n"
"STR_TAB_FIELD_DATATYPE\n"
"string.text"
msgid "Field ~type"
msgstr "Aina ya Uga"

#: table.src
#, fuzzy
msgctxt ""
"table.src\n"
"STR_TAB_FIELD_COLUMN_DATATYPE\n"
"string.text"
msgid "Field Type"
msgstr "Aina ya Uga"

#: table.src
msgctxt ""
"table.src\n"
"STR_TAB_FIELD_LENGTH\n"
"string.text"
msgid "Field length"
msgstr "Urefu wa uga"

#: table.src
msgctxt ""
"table.src\n"
"STR_TAB_HELP_TEXT\n"
"string.text"
msgid "Description"
msgstr "Maelezo"

#: table.src
#, fuzzy
msgctxt ""
"table.src\n"
"STR_COLUMN_DESCRIPTION\n"
"string.text"
msgid "Column Description"
msgstr "Hariri Maelezo ya Safu-wima"

#: table.src
msgctxt ""
"table.src\n"
"STR_TAB_FIELD_NULLABLE\n"
"string.text"
msgid "Input required"
msgstr "Ingizo linatakiwa"

#: table.src
msgctxt ""
"table.src\n"
"STR_FIELD_AUTOINCREMENT\n"
"string.text"
msgid "~AutoValue"
msgstr "ThamaniOtomati"

#: table.src
msgctxt ""
"table.src\n"
"STR_TAB_PROPERTIES\n"
"string.text"
msgid "Field Properties"
msgstr "Sifa za Uga"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABPAGE_GENERAL\n"
"string.text"
msgid "General"
msgstr "Jumla"

#: table.src
msgctxt ""
"table.src\n"
"STR_TAB_TABLE_DESCRIPTION\n"
"string.text"
msgid "Description:"
msgstr "Maelezo:"

#: table.src
msgctxt ""
"table.src\n"
"STR_TAB_TABLE_PROPERTIES\n"
"string.text"
msgid "Table properties"
msgstr "Sifa za jedwali"

#: table.src
msgctxt ""
"table.src\n"
"RID_TABLEDESIGNROWPOPUPMENU\n"
"SID_CUT\n"
"menuitem.text"
msgid "Cu~t"
msgstr ""

#: table.src
msgctxt ""
"table.src\n"
"RID_TABLEDESIGNROWPOPUPMENU\n"
"SID_COPY\n"
"menuitem.text"
msgid "~Copy"
msgstr ""

#: table.src
msgctxt ""
"table.src\n"
"RID_TABLEDESIGNROWPOPUPMENU\n"
"SID_PASTE\n"
"menuitem.text"
msgid "~Paste"
msgstr ""

#: table.src
msgctxt ""
"table.src\n"
"RID_TABLEDESIGNROWPOPUPMENU\n"
"SID_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr ""

#: table.src
msgctxt ""
"table.src\n"
"RID_TABLEDESIGNROWPOPUPMENU\n"
"SID_TABLEDESIGN_INSERTROWS\n"
"menuitem.text"
msgid "Insert Rows"
msgstr "Insert Rows"

#: table.src
msgctxt ""
"table.src\n"
"RID_TABLEDESIGNROWPOPUPMENU\n"
"SID_TABLEDESIGN_TABED_PRIMARYKEY\n"
"menuitem.text"
msgid "Primary Key"
msgstr "Ufunguo Msingi"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABED_UNDO_CELLMODIFIED\n"
"string.text"
msgid "Modify cell"
msgstr "Rekebisha seli"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABED_UNDO_ROWDELETED\n"
"string.text"
msgid "Delete row"
msgstr "Futa safu-mlalo"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABED_UNDO_TYPE_CHANGED\n"
"string.text"
msgid "Modify field type"
msgstr "Rekebisha Aina ya Uga"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABED_UNDO_ROWINSERTED\n"
"string.text"
msgid "Insert row"
msgstr "Chomeka safu-mlalo"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABED_UNDO_NEWROWINSERTED\n"
"string.text"
msgid "Insert new row"
msgstr "Chomeka safu-mlalo mpya"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABED_UNDO_PRIMKEY\n"
"string.text"
msgid "Insert/remove primary key"
msgstr "Chomeka/ondoa ufunguo awali"

#: table.src
msgctxt ""
"table.src\n"
"STR_DEFAULT_VALUE\n"
"string.text"
msgid "~Default value"
msgstr "Thamani msingi"

#: table.src
msgctxt ""
"table.src\n"
"STR_FIELD_REQUIRED\n"
"string.text"
msgid "~Entry required"
msgstr "Ingizo linatakiwa"

#: table.src
msgctxt ""
"table.src\n"
"STR_TEXT_LENGTH\n"
"string.text"
msgid "~Length"
msgstr "Urefu"

#: table.src
msgctxt ""
"table.src\n"
"STR_NUMERIC_TYPE\n"
"string.text"
msgid "~Type"
msgstr "~Type"

#: table.src
msgctxt ""
"table.src\n"
"STR_LENGTH\n"
"string.text"
msgid "~Length"
msgstr "Urefu"

#: table.src
msgctxt ""
"table.src\n"
"STR_SCALE\n"
"string.text"
msgid "Decimal ~places"
msgstr "Sehemu za desimali"

#: table.src
msgctxt ""
"table.src\n"
"STR_FORMAT\n"
"string.text"
msgid "Format example"
msgstr "Mfano wa fomati"

#: table.src
msgctxt ""
"table.src\n"
"STR_HELP_BOOL_DEFAULT\n"
"string.text"
msgid ""
"Select a value that is to appear in all new records as default.\n"
"If the field is not to have a default value, select the empty string."
msgstr ""
"Teua thamani ambazo zitaonekana katika rekodi mpya zote kama msingi.\n"
"kama uga hautakuwa na thamani msingi, teua utungo mtupu."

#: table.src
msgctxt ""
"table.src\n"
"STR_HELP_DEFAULT_VALUE\n"
"string.text"
msgid ""
"Enter a default value for this field.\n"
"\n"
"When you later enter data in the table, this string will be used in each new record for the field selected. It should, therefore, correspond to the cell format that needs to be entered below."
msgstr ""
"Ingiza thamani msingi kwa uga huu.\n"
"\n"
"Wakati utakapoingiza data baadaye katika jedwali, utungo huu utakuwa unatumika katika kila rekodi kwa uga ulioteuliwa. Kwa hiyo, ziwe linganifu kwa fomati ya seli ambazo zinaitajika kuwa zimeingizwa chini."

#: table.src
msgctxt ""
"table.src\n"
"STR_HELP_FIELD_REQUIRED\n"
"string.text"
msgid "Activate this option if this field cannot contain NULL values, i.e. the user must always enter data."
msgstr "Amilisha chaguo hili kama uga huu hauwezi kujumuisha thamani NUNGE, k.v. daima mtumiaji lazima aingize data."

#: table.src
msgctxt ""
"table.src\n"
"STR_HELP_TEXT_LENGTH\n"
"string.text"
msgid "Enter the maximum text length permitted."
msgstr "Ingiza kiwango cha juu cha urefu wa matini ulioruhusiwa."

#: table.src
msgctxt ""
"table.src\n"
"STR_HELP_NUMERIC_TYPE\n"
"string.text"
msgid "Enter the number format."
msgstr "Ingiza fomati ya namba."

#: table.src
msgctxt ""
"table.src\n"
"STR_HELP_LENGTH\n"
"string.text"
msgid ""
"Determine the length data can have in this field.\n"
"\n"
"If decimal fields, then the maximum length of the number to be entered, if binary fields, then the length of the data block.\n"
"The value will be corrected accordingly when it exceeds the maximum for this database."
msgstr ""
"Ukilia urefu wa data zinazowezakana katika uga huu.\n"
"\n"
"Kama nyuga desimali, basi urefu wa juu wa namba zitakazoingizwa, kama nyuga jozi, bazi urefu wa duta data.\n"
"Thamani zitakuwa zimekusanywa wakati itakapozidi ukubwa kwa hifadhidata hii."

#: table.src
msgctxt ""
"table.src\n"
"STR_HELP_SCALE\n"
"string.text"
msgid "Specify the number of decimal places permitted in this field."
msgstr "Bainisha sehemu za desimali za namba zilizoruhusiwa katika uga huu."

#: table.src
msgctxt ""
"table.src\n"
"STR_HELP_FORMAT_CODE\n"
"string.text"
msgid "This is where you see how the data would be displayed in the current format (use the button on the right to modify the format)."
msgstr "Hapa ndipo utaona jinsi data zitakavyo zinzwa katika fomati ya sasa(tumia kitufe kushoto kwako kurekebisha fomati)."

#: table.src
msgctxt ""
"table.src\n"
"STR_HELP_FORMAT_BUTTON\n"
"string.text"
msgid "This is where you determine the output format of the data."
msgstr "Hapa ndipo utaukilia fomati ya zao ya data."

#: table.src
msgctxt ""
"table.src\n"
"STR_HELP_AUTOINCREMENT\n"
"string.text"
msgid ""
"Choose if this field should contain AutoIncrement values.\n"
"\n"
"You can not enter data in fields of this type. An intrinsic value will be assigned to each new record automatically (resulting from the increment of the previous record)."
msgstr ""
"Chagua kama uga huu utajumuhisha thamani za Nyongeza Otomati.\n"
"\n"
"Hauwezi kuingiza data katika nyuga za aina hii. Maana dhati zitapangiwa kwa kila rekodi mpya kiotomati (matokeo kutoka nyongeza za rekodi za awali)."

#: table.src
msgctxt ""
"table.src\n"
"STR_BUTTON_FORMAT\n"
"string.text"
msgid "~..."
msgstr ""

#: table.src
msgctxt ""
"table.src\n"
"STR_TABLEDESIGN_DUPLICATE_NAME\n"
"string.text"
msgid "The table cannot be saved because column name \"$column$\" was assigned twice."
msgstr "Jedwali haliwezekani kuhifadhiwa kwa sababu jina la safu-wima \"$column$\" ilipangiwa mara mbili."

#: table.src
msgctxt ""
"table.src\n"
"STR_TBL_COLUMN_IS_KEYCOLUMN\n"
"string.text"
msgid "The column \"$column$\" belongs to the primary key. If the column is deleted, the primary key will also be deleted. Do you really want to continue?"
msgstr "safu-wima \"$column$\" ni kwa ufunguo msingi. Kama safu-wima itafutwa, ufunguo awali vilevile utakuwa umefutwa. Unataka kweli kuundelea?"

#: table.src
msgctxt ""
"table.src\n"
"STR_TBL_COLUMN_IS_KEYCOLUMN_TITLE\n"
"string.text"
msgid "Primary Key Affected"
msgstr "Primary Key Affected"

#: table.src
msgctxt ""
"table.src\n"
"STR_COLUMN_NAME\n"
"string.text"
msgid "Column"
msgstr "Safu-wima"

#: table.src
msgctxt ""
"table.src\n"
"STR_QRY_CONTINUE\n"
"string.text"
msgid "Continue anyway?"
msgstr "Vyovyote endelea?"

#: table.src
msgctxt ""
"table.src\n"
"STR_STAT_WARNING\n"
"string.text"
msgid "Warning!"
msgstr "Tahadhari!"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABLEDESIGN_CONNECTION_MISSING\n"
"string.text"
msgid "The table could not be saved due to problems connecting to the database."
msgstr "Jedwali halikuwezekana kuhifadhiwa kutokana na tatizo la unganisho kwa hifadhidata."

#: table.src
msgctxt ""
"table.src\n"
"STR_TABLEDESIGN_DATASOURCE_DELETED\n"
"string.text"
msgid "The table filter could not be adjusted because the data source has been deleted."
msgstr "Chujio la jedwali halikuwezekana kurekebishwa kwa sababu chanzo data limekwisha futwa."

#: table.src
#, fuzzy
msgctxt ""
"table.src\n"
"STR_QUERY_SAVE_TABLE_EDIT_INDEXES\n"
"string.text"
msgid ""
"Before you can edit the indexes of a table, you have to save it.\n"
"Do you want to save the changes now?"
msgstr ""
"Kabla hujahariri vielezo vya jedwali, unatakiwa kihifadhi.\n"
"Unataka kuhifadhi mabadiliko sasa?"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABLEDESIGN_NO_PRIM_KEY_HEAD\n"
"string.text"
msgid "No primary key"
msgstr "Hakuna ufunguo msingi"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABLEDESIGN_NO_PRIM_KEY\n"
"string.text"
msgid ""
"A unique index or primary key is required for data record identification in this database.\n"
"You can only enter data into this table when one of these two structural conditions has been met.\n"
"\n"
"Should a primary key be created now?"
msgstr ""
"Kielezo pekee au ufunguo msingi zinaitajika kwa ajili ya utambulishi wa rekodi za data katika hifadhidata hii.\n"
"Unaweza kuingiza data tu katika jedwali hili wakati moja wapo ya hali ya muundo miwili zimekwisha kutana.\n"
"\n"
"Ufunguo msingi uundwe sasa?"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABLEDESIGN_TITLE\n"
"string.text"
msgid " - %PRODUCTNAME Base: Table Design"
msgstr " - %PRODUCTNAME Base: Table Design"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABLEDESIGN_ALTER_ERROR\n"
"string.text"
msgid "The column \"$column$\" could not be changed. Should the column instead be deleted and the new format appended?"
msgstr "Safu-wima \"$column$\" haiwezekani kubadilishwa. Tunaweza kufuta safu-wima na badala yake fomati mpya kuongezwa mwishoni?"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABLEDESIGN_SAVE_ERROR\n"
"string.text"
msgid "Error while saving the table design"
msgstr ""

#: table.src
msgctxt ""
"table.src\n"
"STR_TABLEDESIGN_COULD_NOT_DROP_COL\n"
"string.text"
msgid "The column $column$ could not be deleted."
msgstr ""

#: table.src
msgctxt ""
"table.src\n"
"STR_AUTOINCREMENT_VALUE\n"
"string.text"
msgid "A~uto-increment statement"
msgstr "Maelezo nyongeza-otomati"

#: table.src
msgctxt ""
"table.src\n"
"STR_HELP_AUTOINCREMENT_VALUE\n"
"string.text"
msgid ""
"Enter an SQL statement for the auto-increment field.\n"
"\n"
"This statement will be directly transferred to the database when the table is created."
msgstr ""
"Ingiza maelezo ya SQL kwa ajili ya uga wa nyongeza-otomati.\n"
"\n"
"Maelezo haya yatahamishiwa moja kwa moja kwenye Hifadhidata wakati jedwali litakapoundwa."

#: table.src
msgctxt ""
"table.src\n"
"STR_NO_TYPE_INFO_AVAILABLE\n"
"string.text"
msgid ""
"No type information could be retrieved from the database.\n"
"The table design mode is not available for this data source."
msgstr ""
"Hakuna taarifa zinazoweza kiitishwa kutoka hifadhidata.\n"
"Modi mbuni jedwali haipo kwa ajili ya chanzo data."

#: table.src
msgctxt ""
"table.src\n"
"STR_CHANGE_COLUMN_NAME\n"
"string.text"
msgid "change field name"
msgstr "change field name"

#: table.src
msgctxt ""
"table.src\n"
"STR_CHANGE_COLUMN_TYPE\n"
"string.text"
msgid "change field type"
msgstr "change field type"

#: table.src
msgctxt ""
"table.src\n"
"STR_CHANGE_COLUMN_DESCRIPTION\n"
"string.text"
msgid "change field description"
msgstr "change field description"

#: table.src
msgctxt ""
"table.src\n"
"STR_CHANGE_COLUMN_ATTRIBUTE\n"
"string.text"
msgid "change field attribute"
msgstr "change field attribute"

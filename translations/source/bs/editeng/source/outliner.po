#. extracted from editeng/source/outliner
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-11-20 13:02+0100\n"
"PO-Revision-Date: 2014-01-16 10:47+0000\n"
"Last-Translator: vljubovic <vedran-liste@linux.org.ba>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: bs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1389869258.0\n"

#: outliner.src
msgctxt ""
"outliner.src\n"
"RID_OUTLUNDO_HEIGHT\n"
"string.text"
msgid "Move"
msgstr "Pomjeri"

#: outliner.src
msgctxt ""
"outliner.src\n"
"RID_OUTLUNDO_DEPTH\n"
"string.text"
msgid "Indent"
msgstr "Uvuci"

#: outliner.src
msgctxt ""
"outliner.src\n"
"RID_OUTLUNDO_EXPAND\n"
"string.text"
msgid "Show subpoints"
msgstr "Prikaži pod-tačke"

#: outliner.src
msgctxt ""
"outliner.src\n"
"RID_OUTLUNDO_COLLAPSE\n"
"string.text"
msgid "Collapse"
msgstr "Zatvori"

#: outliner.src
msgctxt ""
"outliner.src\n"
"RID_OUTLUNDO_ATTR\n"
"string.text"
msgid "Apply attributes"
msgstr "Primijeni atribute"

#: outliner.src
msgctxt ""
"outliner.src\n"
"RID_OUTLUNDO_INSERT\n"
"string.text"
msgid "Insert"
msgstr "Ubaci"

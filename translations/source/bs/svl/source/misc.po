#. extracted from svl/source/misc
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-11-20 13:02+0100\n"
"PO-Revision-Date: 2014-01-27 19:22+0000\n"
"Last-Translator: vljubovic <vedran-liste@linux.org.ba>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: bs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1390850548.0\n"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_OCTSTREAM\n"
"string.text"
msgid "Binary file"
msgstr "Binarna datoteka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_PDF\n"
"string.text"
msgid "PDF file"
msgstr "PDF datoteka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_RTF\n"
"string.text"
msgid "RTF File"
msgstr "RTF datoteka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_MSWORD\n"
"string.text"
msgid "MS-Word document"
msgstr "MS Word dokument"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_STARCALC\n"
"string.text"
msgid "%PRODUCTNAME Spreadsheet"
msgstr "%PRODUCTNAME tablični proračun"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_STARCHART\n"
"string.text"
msgid "%PRODUCTNAME Chart"
msgstr "%PRODUCTNAME grafikon"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_STARDRAW\n"
"string.text"
msgid "%PRODUCTNAME Drawing"
msgstr "%PRODUCTNAME crtež"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_STARIMAGE\n"
"string.text"
msgid "%PRODUCTNAME Image"
msgstr "%PRODUCTNAME slika"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_STARMATH\n"
"string.text"
msgid "%PRODUCTNAME Formula"
msgstr "%PRODUCTNAME formula"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_STARWRITER\n"
"string.text"
msgid "%PRODUCTNAME Text"
msgstr "%PRODUCTNAME tekst"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_ZIP\n"
"string.text"
msgid "ZIP file"
msgstr "ZIP datoteka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_JAR\n"
"string.text"
msgid "JAR file"
msgstr "JAR datoteka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_AUDIO_AIFF\n"
"string.text"
msgid "Audio file"
msgstr "Audio datoteka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_AUDIO_BASIC\n"
"string.text"
msgid "Audio file"
msgstr "Audio datoteka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_AUDIO_MIDI\n"
"string.text"
msgid "Audio file"
msgstr "Audio datoteka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_AUDIO_VORBIS\n"
"string.text"
msgid "Audio file"
msgstr "Audio datoteka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_AUDIO_WAV\n"
"string.text"
msgid "Audio file"
msgstr "Audio datoteka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_AUDIO_WEBM\n"
"string.text"
msgid "Audio file"
msgstr "Audio datoteka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_IMAGE_GIF\n"
"string.text"
msgid "Image"
msgstr "Slika"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_IMAGE_JPEG\n"
"string.text"
msgid "Image"
msgstr "Slika"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_IMAGE_PCX\n"
"string.text"
msgid "Image"
msgstr "Slika"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_IMAGE_BMP\n"
"string.text"
msgid "Bitmap"
msgstr "Bitmapa"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_TEXT_HTML\n"
"string.text"
msgid "HTML document"
msgstr "HTML dokument"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_TEXT_PLAIN\n"
"string.text"
msgid "Text file"
msgstr "Tekstualna datoteka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_TEXT_URL\n"
"string.text"
msgid "Bookmark"
msgstr "Zabilješka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_TEXT_VCARD\n"
"string.text"
msgid "vCard file"
msgstr "vCard datoteka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_VIDEO_MSVIDEO\n"
"string.text"
msgid "Video file"
msgstr "Video datoteka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_VIDEO_THEORA\n"
"string.text"
msgid "Video file"
msgstr "Video datoteka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_VIDEO_VDO\n"
"string.text"
msgid "Video file"
msgstr "Video datoteka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_VIDEO_WEBM\n"
"string.text"
msgid "Video file"
msgstr "Video datoteka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_X_STARMAIL\n"
"string.text"
msgid "Message"
msgstr "Poruka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_X_VRML\n"
"string.text"
msgid "VRML file"
msgstr "VRML datoteka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_STARIMPRESS\n"
"string.text"
msgid "%PRODUCTNAME Presentation"
msgstr "%PRODUCTNAME prezentacija"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_IMPRESSPACKED\n"
"string.text"
msgid "%PRODUCTNAME Presentation (packed)"
msgstr "%PRODUCTNAME prezentacija (zapakovana)"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_STARHELP\n"
"string.text"
msgid "%PRODUCTNAME Help"
msgstr "Pomoć za %PRODUCTNAME"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_CNT_FSYSBOX\n"
"string.text"
msgid "Workplace"
msgstr "Radno mjesto"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_CNT_FSYSFLD\n"
"string.text"
msgid "Folder"
msgstr "Direktorij"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_GAL\n"
"string.text"
msgid "Gallery"
msgstr "Galerija"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_GAL_THEME\n"
"string.text"
msgid "Gallery theme"
msgstr "Galerija tema"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_STARW_GLOB\n"
"string.text"
msgid "%PRODUCTNAME Master Document"
msgstr "%PRODUCTNAME master dokument"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_SDM\n"
"string.text"
msgid "Message"
msgstr "Poruka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_SMD\n"
"string.text"
msgid "Message"
msgstr "Poruka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_STARW_WEB\n"
"string.text"
msgid "%PRODUCTNAME Writer/Web"
msgstr "%PRODUCTNAME Writer/Web"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_SCHEDULE\n"
"string.text"
msgid "Tasks & Events"
msgstr "Zadaci i događaji"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_SCHEDULE_EVT\n"
"string.text"
msgid "%PRODUCTNAME Events View"
msgstr "%PRODUCTNAME pogled događaja"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_SCHEDULE_TASK\n"
"string.text"
msgid "%PRODUCTNAME Task View"
msgstr "%PRODUCTNAME pogled zadataka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_SCHEDULE_FEVT\n"
"string.text"
msgid "%PRODUCTNAME Event"
msgstr "%PRODUCTNAME događaj"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_SCHEDULE_FTASK\n"
"string.text"
msgid "%PRODUCTNAME Task"
msgstr "%PRODUCTNAME zadatak"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_FRAMESET\n"
"string.text"
msgid "Frameset Document"
msgstr "Dokument sa skupom okvira"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_MACRO\n"
"string.text"
msgid "Macro file"
msgstr "Makro datoteka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_CNT_SFSYSFOLDER\n"
"string.text"
msgid "System folder"
msgstr "Sistemski direktorij"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_TEMPLATE\n"
"string.text"
msgid "%PRODUCTNAME Template"
msgstr "Šablon programa %PRODUCTNAME"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_IMAGE_GENERIC\n"
"string.text"
msgid "Image"
msgstr "Slika"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_MSEXCEL\n"
"string.text"
msgid "MS Excel document"
msgstr "MS Excel dokument"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_MSEXCEL_TEMPL\n"
"string.text"
msgid "MS Excel Template"
msgstr "MS Excel šablon"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_MSPPOINT\n"
"string.text"
msgid "MS PowerPoint document"
msgstr "MS PowerPoint dokument"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_TEXT_VCALENDAR\n"
"string.text"
msgid "vCalendar-file"
msgstr "vCalendar datoteka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_TEXT_ICALENDAR\n"
"string.text"
msgid "iCalendar-File"
msgstr "iCalendar datoteka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_TEXT_XMLICALENDAR\n"
"string.text"
msgid "XML-iCalendar-File"
msgstr "XML iCalendar datoteka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_TEXT_CDE_CALENDAR_APP\n"
"string.text"
msgid "CDE-Calendar-File"
msgstr "CDE kalendar datoteka"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_INET_MSG_RFC822\n"
"string.text"
msgid "message/rfc822"
msgstr "message/rfc822"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_INET_MULTI_ALTERNATIVE\n"
"string.text"
msgid "multipart/alternative"
msgstr "multipart/alternative"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_INET_MULTI_DIGEST\n"
"string.text"
msgid "multipart/digest"
msgstr "multipart/digest"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_INET_MULTI_PARALLEL\n"
"string.text"
msgid "multipart/parallel"
msgstr "multipart/parallel"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_INET_MULTI_RELATED\n"
"string.text"
msgid "multipart/related"
msgstr "multipart/related"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_INET_MULTI_MIXED\n"
"string.text"
msgid "multipart/mixed"
msgstr "multipart/mixed"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_SXCALC\n"
"string.text"
msgid "OpenOffice.org 1.0 Spreadsheet"
msgstr "OpenOffice.org 1.0 Proračunske tablice"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_SXCHART\n"
"string.text"
msgid "OpenOffice.org 1.0 Chart"
msgstr "OpenOffice.org 1.0 grafikon"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_SXDRAW\n"
"string.text"
msgid "OpenOffice.org 1.0 Drawing"
msgstr "OpenOffice.org 1.0 Crtež"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_SXMATH\n"
"string.text"
msgid "OpenOffice.org 1.0 Formula"
msgstr "OpenOffice.org 1.0 Formula"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_SXWRITER\n"
"string.text"
msgid "OpenOffice.org 1.0 Text Document"
msgstr "OpenOffice.org 1.0 Tekstualni dokument"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_SXIMPRESS\n"
"string.text"
msgid "OpenOffice.org 1.0 Presentation"
msgstr "OpenOffice.org 1.0 prezentacija"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_SXGLOBAL\n"
"string.text"
msgid "OpenOffice.org 1.0 Master Document"
msgstr "OpenOffice.org 1.0 master dokument"

#: mediatyp.src
msgctxt ""
"mediatyp.src\n"
"STR_SVT_MIMETYPE_APP_SXIPACKED\n"
"string.text"
msgid "OpenOffice.org 1.0 Presentation (packed)"
msgstr "OpenOffice.org 1.0 prezentacija (zapakovana)"

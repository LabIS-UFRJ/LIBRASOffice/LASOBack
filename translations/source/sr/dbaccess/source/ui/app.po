#. extracted from dbaccess/source/ui/app
msgid ""
msgstr ""
"Project-Id-Version: OpenOffice.org\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2014-11-21 22:01+0100\n"
"PO-Revision-Date: 2012-06-18 08:30+0200\n"
"Last-Translator: Goran Rakic <grakic@devbase.net>\n"
"Language-Team: Serbian <http://gitorious.org/ooo-sr>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-Project-Style: openoffice\n"

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_NEW_FORM\n"
"string.text"
msgid "Create Form in Design View..."
msgstr "Направи образац у графичком уређивачу..."

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_NEW_FORM_AUTO\n"
"string.text"
msgid "Use Wizard to Create Form..."
msgstr "Помоћник за израду обрасца..."

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_NEW_REPORT_AUTO\n"
"string.text"
msgid "Use Wizard to Create Report..."
msgstr "Помоћник за израду извештаја..."

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_NEW_REPORT\n"
"string.text"
msgid "Create Report in Design View..."
msgstr "Направи извештај у графичком уређивачу..."

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_NEW_QUERY\n"
"string.text"
msgid "Create Query in Design View..."
msgstr "Направи упит у графичком уређивачу..."

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_NEW_QUERY_SQL\n"
"string.text"
msgid "Create Query in SQL View..."
msgstr "Направи упит у SQL приказу..."

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_NEW_QUERY_AUTO\n"
"string.text"
msgid "Use Wizard to Create Query..."
msgstr "Помоћник за израду упита..."

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_NEW_TABLE\n"
"string.text"
msgid "Create Table in Design View..."
msgstr "Направи табелу у графичком уређивачу..."

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_NEW_TABLE_AUTO\n"
"string.text"
msgid "Use Wizard to Create Table..."
msgstr "Помоћник за израду табеле..."

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_NEW_VIEW\n"
"string.text"
msgid "Create View..."
msgstr "Направи поглед..."

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_FORMS_CONTAINER\n"
"string.text"
msgid "Forms"
msgstr "Обрасци"

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_REPORTS_CONTAINER\n"
"string.text"
msgid "Reports"
msgstr "Извештаји"

#: app.src
msgctxt ""
"app.src\n"
"RID_MENU_APP_NEW\n"
"SID_APP_NEW_FORM\n"
"menuitem.text"
msgid "Form..."
msgstr "Образац..."

#: app.src
msgctxt ""
"app.src\n"
"RID_MENU_APP_NEW\n"
"SID_APP_NEW_REPORT\n"
"menuitem.text"
msgid "Report..."
msgstr "Извештај..."

#: app.src
msgctxt ""
"app.src\n"
"RID_MENU_APP_NEW\n"
"SID_DB_NEW_VIEW_SQL\n"
"menuitem.text"
msgid "View (Simple)..."
msgstr "Поглед (једноставан)..."

#: app.src
msgctxt ""
"app.src\n"
"RID_MENU_APP_EDIT\n"
"SID_DB_APP_PASTE_SPECIAL\n"
"menuitem.text"
msgid "Paste Special..."
msgstr "Уметни посебан..."

#: app.src
msgctxt ""
"app.src\n"
"RID_MENU_APP_EDIT\n"
"SID_DB_APP_DELETE\n"
"menuitem.text"
msgid "Delete"
msgstr "Обриши"

#: app.src
msgctxt ""
"app.src\n"
"RID_MENU_APP_EDIT\n"
"SID_DB_APP_RENAME\n"
"menuitem.text"
msgid "Rename"
msgstr "Преименуј"

#: app.src
msgctxt ""
"app.src\n"
"RID_MENU_APP_EDIT\n"
"SID_DB_APP_EDIT\n"
"menuitem.text"
msgid "Edit"
msgstr "Измени"

#: app.src
msgctxt ""
"app.src\n"
"RID_MENU_APP_EDIT\n"
"SID_DB_APP_EDIT_SQL_VIEW\n"
"menuitem.text"
msgid "Edit in SQL View..."
msgstr "Уреди у SQL приказу..."

#: app.src
msgctxt ""
"app.src\n"
"RID_MENU_APP_EDIT\n"
"SID_DB_APP_OPEN\n"
"menuitem.text"
msgid "Open"
msgstr "Отвори"

#: app.src
msgctxt ""
"app.src\n"
"RID_MENU_APP_EDIT\n"
"SID_DB_APP_CONVERTTOVIEW\n"
"menuitem.text"
msgid "Create as View"
msgstr "Направи као поглед"

#: app.src
msgctxt ""
"app.src\n"
"RID_MENU_APP_EDIT\n"
"SID_FORM_CREATE_REPWIZ_PRE_SEL\n"
"menuitem.text"
msgid "Form Wizard..."
msgstr "Помоћник за образац..."

#: app.src
#, fuzzy
msgctxt ""
"app.src\n"
"RID_MENU_APP_EDIT\n"
"SID_APP_NEW_REPORT_PRE_SEL\n"
"menuitem.text"
msgid "Report..."
msgstr "Извештај..."

#: app.src
msgctxt ""
"app.src\n"
"RID_MENU_APP_EDIT\n"
"SID_REPORT_CREATE_REPWIZ_PRE_SEL\n"
"menuitem.text"
msgid "Report Wizard..."
msgstr "Помоћник за извештај..."

#: app.src
msgctxt ""
"app.src\n"
"RID_MENU_APP_EDIT\n"
"SID_SELECTALL\n"
"menuitem.text"
msgid "Select All"
msgstr "Изабери све"

#: app.src
msgctxt ""
"app.src\n"
"RID_MENU_APP_EDIT.MN_PROPS\n"
"SID_DB_APP_DSPROPS\n"
"menuitem.text"
msgid "Properties..."
msgstr "Својства..."

#: app.src
msgctxt ""
"app.src\n"
"RID_MENU_APP_EDIT.MN_PROPS\n"
"SID_DB_APP_DSCONNECTION_TYPE\n"
"menuitem.text"
msgid "Connection Type..."
msgstr "Врста везе..."

#: app.src
msgctxt ""
"app.src\n"
"RID_MENU_APP_EDIT.MN_PROPS\n"
"SID_DB_APP_DSADVANCED_SETTINGS\n"
"menuitem.text"
msgid "Advanced Settings..."
msgstr "Напредна подешавања..."

#: app.src
msgctxt ""
"app.src\n"
"RID_MENU_APP_EDIT\n"
"MN_PROPS\n"
"menuitem.text"
msgid "~Database"
msgstr "~База података"

#: app.src
msgctxt ""
"app.src\n"
"STR_QUERY_DELETE_DATASOURCE\n"
"string.text"
msgid "Do you want to delete the data source '%1'?"
msgstr "Желите ли да обришете извор података „%1“?"

#: app.src
msgctxt ""
"app.src\n"
"STR_APP_TITLE\n"
"string.text"
msgid " - %PRODUCTNAME Base"
msgstr " - %PRODUCTNAME База"

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_REPORTS_HELP_TEXT_WIZARD\n"
"string.text"
msgid "The wizard will guide you through the steps necessary to create a report."
msgstr "Овај помоћник ће вас провести кроз неопходне кораке за израду извештаја."

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_FORMS_HELP_TEXT\n"
"string.text"
msgid "Create a form by specifying the record source, controls, and control properties."
msgstr "Направите образац наводећи извор записа, контроле за унос и њихова својства."

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_REPORT_HELP_TEXT\n"
"string.text"
msgid "Create a report by specifying the record source, controls, and control properties."
msgstr "Направите извештај наводећи извор записа, контроле и својства контроле."

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_FORMS_HELP_TEXT_WIZARD\n"
"string.text"
msgid "The wizard will guide you through the steps necessary to create a form."
msgstr "Овај помоћник ће вас провести кроз неопходне кораке за израду обрасца."

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_QUERIES_HELP_TEXT\n"
"string.text"
msgid "Create a query by specifying the filters, input tables, field names, and properties for sorting or grouping."
msgstr "Направите упит наводећи филтере, улазне табеле, имена поља и својства ређања или груписања."

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_QUERIES_HELP_TEXT_SQL\n"
"string.text"
msgid "Create a query entering an SQL statement directly."
msgstr "Направите упит директним уносом SQL израза."

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_QUERIES_HELP_TEXT_WIZARD\n"
"string.text"
msgid "The wizard will guide you through the steps necessary to create a query."
msgstr "Овај помоћник ће вас провести кроз неопходне кораке за израду упита."

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_TABLES_HELP_TEXT_DESIGN\n"
"string.text"
msgid "Create a table by specifying the field names and properties, as well as the data types."
msgstr "Направите табелу наводећи имена поља, њихова својства и врсту података."

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_TABLES_HELP_TEXT_WIZARD\n"
"string.text"
msgid "Choose from a selection of business and personal table samples, which you customize to create a table."
msgstr "Изаберите из скупа примера пословних и личних табела и прилагодите их да направите нову табелу."

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_VIEWS_HELP_TEXT_DESIGN\n"
"string.text"
msgid "Create a view by specifying the tables and field names you would like to have visible."
msgstr "Направите поглед наводећи имена табела и поља која ће бити видљива."

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_VIEWS_HELP_TEXT_WIZARD\n"
"string.text"
msgid "Opens the view wizard"
msgstr "Отвара помоћника за поглед"

#: app.src
msgctxt ""
"app.src\n"
"STR_DATABASE\n"
"string.text"
msgid "Database"
msgstr "База података"

#: app.src
msgctxt ""
"app.src\n"
"STR_TASKS\n"
"string.text"
msgid "Tasks"
msgstr "Задаци"

#: app.src
msgctxt ""
"app.src\n"
"STR_DESCRIPTION\n"
"string.text"
msgid "Description"
msgstr "Опис"

#: app.src
msgctxt ""
"app.src\n"
"STR_PREVIEW\n"
"string.text"
msgid "Preview"
msgstr "Преглед"

#: app.src
msgctxt ""
"app.src\n"
"STR_DISABLEPREVIEW\n"
"string.text"
msgid "Disable Preview"
msgstr "Искључи преглед"

#: app.src
msgctxt ""
"app.src\n"
"STR_QUERY_CLOSEDOCUMENTS\n"
"string.text"
msgid ""
"The connection type has been altered.\n"
"For the changes to take effect, all forms, reports, queries and tables must be closed.\n"
"\n"
"Do you want to close all documents now?"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_MENU_APP_PREVIEW\n"
"SID_DB_APP_DISABLE_PREVIEW\n"
"menuitem.text"
msgid "None"
msgstr "Без прегледа"

#: app.src
msgctxt ""
"app.src\n"
"RID_MENU_APP_PREVIEW\n"
"SID_DB_APP_VIEW_DOCINFO_PREVIEW\n"
"menuitem.text"
msgid "Document Information"
msgstr "Подаци о документу"

#: app.src
msgctxt ""
"app.src\n"
"RID_MENU_APP_PREVIEW\n"
"SID_DB_APP_VIEW_DOC_PREVIEW\n"
"menuitem.text"
msgid "Document"
msgstr "Документ"

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_FORM\n"
"string.text"
msgid "Form"
msgstr "Образац"

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_REPORT\n"
"string.text"
msgid "Report"
msgstr "Извештај"

#: app.src
msgctxt ""
"app.src\n"
"STR_FRM_LABEL\n"
"string.text"
msgid "F~orm name"
msgstr "Име о~брасца"

#: app.src
msgctxt ""
"app.src\n"
"STR_RPT_LABEL\n"
"string.text"
msgid "~Report name"
msgstr "~Назив извештаја"

#: app.src
msgctxt ""
"app.src\n"
"STR_FOLDER_LABEL\n"
"string.text"
msgid "F~older name"
msgstr "Име ~фасцикле"

#: app.src
msgctxt ""
"app.src\n"
"STR_SUB_DOCS_WITH_SCRIPTS\n"
"string.text"
msgid "The document contains forms or reports with embedded macros."
msgstr "Документ садржи обрасце или извештаје са уграђеним макроима."

#: app.src
msgctxt ""
"app.src\n"
"STR_SUB_DOCS_WITH_SCRIPTS_DETAIL\n"
"string.text"
msgid ""
"Macros should be embedded into the database document itself.\n"
"\n"
"You can continue to use your document as before, however, you are encouraged to migrate your macros. The menu item 'Tools / Migrate Macros ...' will assist you with this.\n"
"\n"
"Note that you won't be able to embed macros into the database document itself until this migration is done. "
msgstr ""
"Макрои треба да буду уграђени у документ базе података.\n"
"\n"
"Можете наставити да користите документ као и раније, ипак препоручљиво је да пренесете макрое. За пренос одаберите „Алатке > Пренеси макрое...“ у менију.\n"
"\n"
"Док не обавите пренос нећете бити у могућности да уградите макрое у документ базе."

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_EMBEDDED_DATABASE\n"
"string.text"
msgid "Embedded database"
msgstr "Уграђена база података"

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_NO_DIFF_CAT\n"
"string.text"
msgid "You cannot select different categories."
msgstr "Није могућ избор различитих категорија."

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_UNSUPPORTED_OBJECT_TYPE\n"
"string.text"
msgid "Unsupported object type found ($type$)."
msgstr "Пронађен неподржан тип објекта ($type$)."

#: app.src
msgctxt ""
"app.src\n"
"STR_PAGETITLE_GENERAL\n"
"string.text"
msgid "Advanced Properties"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_PAGETITLE_ADVANCED\n"
"string.text"
msgid "Additional Settings"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_PAGETITLE_CONNECTION\n"
"string.text"
msgid "Connection settings"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_TBL_LABEL\n"
"string.text"
msgid "~Table Name"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_VW_LABEL\n"
"string.text"
msgid "~Name of table view"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_QRY_LABEL\n"
"string.text"
msgid "~Query name"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_TITLE_RENAME\n"
"string.text"
msgid "Rename to"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_TITLE_PASTE_AS\n"
"string.text"
msgid "Insert as"
msgstr ""

#. extracted from officecfg/registry/data/org/openoffice
msgid ""
msgstr ""
"Project-Id-Version: LibreOffice 4.3.1.0.0+\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2014-11-21 22:01+0100\n"
"PO-Revision-Date: 2014-07-11 13:43+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"

#: Setup.xcu
msgctxt ""
"Setup.xcu\n"
"..Setup.Office.Factories.com.sun.star.sdb.FormDesign\n"
"ooSetupFactoryUIName\n"
"value.text"
msgid "Base: Database Form"
msgstr ""

#: Setup.xcu
msgctxt ""
"Setup.xcu\n"
"..Setup.Office.Factories.com.sun.star.sdb.TextReportDesign\n"
"ooSetupFactoryUIName\n"
"value.text"
msgid "Base: Report Design"
msgstr ""

#: Setup.xcu
msgctxt ""
"Setup.xcu\n"
"..Setup.Office.Factories.com.sun.star.sdb.RelationDesign\n"
"ooSetupFactoryUIName\n"
"value.text"
msgid "Base: Relation Design"
msgstr ""

#: Setup.xcu
msgctxt ""
"Setup.xcu\n"
"..Setup.Office.Factories.com.sun.star.sdb.QueryDesign\n"
"ooSetupFactoryUIName\n"
"value.text"
msgid "Base: Query Design"
msgstr ""

#: Setup.xcu
msgctxt ""
"Setup.xcu\n"
"..Setup.Office.Factories.com.sun.star.sdb.ViewDesign\n"
"ooSetupFactoryUIName\n"
"value.text"
msgid "Base: View Design"
msgstr ""

#: Setup.xcu
msgctxt ""
"Setup.xcu\n"
"..Setup.Office.Factories.com.sun.star.sdb.TableDesign\n"
"ooSetupFactoryUIName\n"
"value.text"
msgid "Base: Table Design"
msgstr ""

#: Setup.xcu
msgctxt ""
"Setup.xcu\n"
"..Setup.Office.Factories.com.sun.star.sdb.DataSourceBrowser\n"
"ooSetupFactoryUIName\n"
"value.text"
msgid "Base: Data View"
msgstr ""

#: Setup.xcu
msgctxt ""
"Setup.xcu\n"
"..Setup.Office.Factories.com.sun.star.sdb.TableDataView\n"
"ooSetupFactoryUIName\n"
"value.text"
msgid "Base: Table Data View"
msgstr ""

#: Setup.xcu
msgctxt ""
"Setup.xcu\n"
"..Setup.Office.Factories.com.sun.star.report.ReportDefinition\n"
"ooSetupFactoryUIName\n"
"value.text"
msgid "Base: Oracle Report Builder"
msgstr ""

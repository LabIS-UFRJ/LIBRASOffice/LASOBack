#. extracted from sc/source/ui/miscdlgs
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2014-11-18 11:23+0100\n"
"PO-Revision-Date: 2015-03-22 22:09+0000\n"
"Last-Translator: Xosé <xosecalvo@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 2.5.1\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1427062163.000000\n"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"STR_CHG_INSERT_COLS\n"
"string.text"
msgid "Column inserted"
msgstr "Columna inserida"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"STR_CHG_INSERT_ROWS\n"
"string.text"
msgid "Row inserted "
msgstr "Fila inserida "

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"STR_CHG_INSERT_TABS\n"
"string.text"
msgid "Sheet inserted "
msgstr "Folla inserida "

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"STR_CHG_DELETE_COLS\n"
"string.text"
msgid "Column deleted"
msgstr "Columna eliminada"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"STR_CHG_DELETE_ROWS\n"
"string.text"
msgid "Row deleted"
msgstr "Fila eliminada"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"STR_CHG_DELETE_TABS\n"
"string.text"
msgid "Sheet deleted"
msgstr "Folla eliminada"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"STR_CHG_MOVE\n"
"string.text"
msgid "Range moved"
msgstr "Intervalo movido"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"STR_CHG_CONTENT\n"
"string.text"
msgid "Changed contents"
msgstr "Contido alterado"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"STR_CHG_CONTENT_WITH_CHILD\n"
"string.text"
msgid "Changed contents"
msgstr "Contido alterado"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"STR_CHG_CHILD_CONTENT\n"
"string.text"
msgid "Changed to "
msgstr "Alterado para "

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"STR_CHG_CHILD_ORGCONTENT\n"
"string.text"
msgid "Original"
msgstr "Orixinal"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"STR_CHG_REJECT\n"
"string.text"
msgid "Changes rejected"
msgstr "Modificacións rexeitadas"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"STR_CHG_ACCEPTED\n"
"string.text"
msgid "Accepted"
msgstr "Aceptado"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"STR_CHG_REJECTED\n"
"string.text"
msgid "Rejected"
msgstr "Rexeitado"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"STR_CHG_NO_ENTRY\n"
"string.text"
msgid "No Entry"
msgstr "Ningunha entrada"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"STR_CHG_EMPTY\n"
"string.text"
msgid "<empty>"
msgstr "<baleiro>"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"RID_POPUP_CHANGES\n"
"SC_CHANGES_COMMENT\n"
"menuitem.text"
msgid "Edit Comment..."
msgstr "Editar comentario..."

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"RID_POPUP_CHANGES.SC_SUB_SORT\n"
"SC_SORT_ACTION\n"
"menuitem.text"
msgid "Action"
msgstr "Acción"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"RID_POPUP_CHANGES.SC_SUB_SORT\n"
"SC_SORT_POSITION\n"
"menuitem.text"
msgid "Position"
msgstr "Posición"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"RID_POPUP_CHANGES.SC_SUB_SORT\n"
"SC_SORT_AUTHOR\n"
"menuitem.text"
msgid "Author"
msgstr "Autor"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"RID_POPUP_CHANGES.SC_SUB_SORT\n"
"SC_SORT_DATE\n"
"menuitem.text"
msgid "Date"
msgstr "Data"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"RID_POPUP_CHANGES.SC_SUB_SORT\n"
"SC_SORT_COMMENT\n"
"menuitem.text"
msgid "Description"
msgstr "Descrición"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"RID_POPUP_CHANGES\n"
"SC_SUB_SORT\n"
"menuitem.text"
msgid "Sorting"
msgstr "Ordenar"

#: conflictsdlg.src
msgctxt ""
"conflictsdlg.src\n"
"STR_TITLE_CONFLICT\n"
"string.text"
msgid "Conflict"
msgstr "Conflito"

#: conflictsdlg.src
msgctxt ""
"conflictsdlg.src\n"
"STR_TITLE_AUTHOR\n"
"string.text"
msgid "Author"
msgstr "Autor"

#: conflictsdlg.src
msgctxt ""
"conflictsdlg.src\n"
"STR_TITLE_DATE\n"
"string.text"
msgid "Date"
msgstr "Data"

#: conflictsdlg.src
msgctxt ""
"conflictsdlg.src\n"
"STR_UNKNOWN_USER_CONFLICT\n"
"string.text"
msgid "Unknown User"
msgstr "Usuario descoñecido"

#: retypepassdlg.src
msgctxt ""
"retypepassdlg.src\n"
"STR_NOT_PROTECTED\n"
"string.text"
msgid "Not protected"
msgstr "Sen protección"

#: retypepassdlg.src
msgctxt ""
"retypepassdlg.src\n"
"STR_NOT_PASS_PROTECTED\n"
"string.text"
msgid "Not password-protected"
msgstr "Sen protección con contrasinal"

#: retypepassdlg.src
msgctxt ""
"retypepassdlg.src\n"
"STR_HASH_BAD\n"
"string.text"
msgid "Hash incompatible"
msgstr "Hash incompatíbel"

#: retypepassdlg.src
msgctxt ""
"retypepassdlg.src\n"
"STR_HASH_GOOD\n"
"string.text"
msgid "Hash compatible"
msgstr "Hash compatíbel"

#: retypepassdlg.src
msgctxt ""
"retypepassdlg.src\n"
"STR_HASH_REGENERATED\n"
"string.text"
msgid "Hash re-generated"
msgstr "Hash rexerado"

#: retypepassdlg.src
msgctxt ""
"retypepassdlg.src\n"
"STR_RETYPE\n"
"string.text"
msgid "Re-type"
msgstr "Reescribir"

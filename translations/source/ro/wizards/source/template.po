#. extracted from wizards/source/template
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-11-20 13:02+0100\n"
"PO-Revision-Date: 2014-01-30 12:29+0000\n"
"Last-Translator: Ákos <nagy.akos@libreoffice.ro>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2);;\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1391084985.0\n"

#: template.src
msgctxt ""
"template.src\n"
"SAMPLES\n"
"string.text"
msgid "In order to use the full functionality of this sample, create a document that is based on this template."
msgstr "Pentru a folosi întreaga funcționalitate a acestui eșantion, creați un document bazat pe acest șablon."

#: template.src
msgctxt ""
"template.src\n"
"SAMPLES + 1\n"
"string.text"
msgid "Remarks"
msgstr "Remărci"

#: template.src
msgctxt ""
"template.src\n"
"STYLES\n"
"string.text"
msgid "Theme Selection"
msgstr "Selecție temă"

#: template.src
msgctxt ""
"template.src\n"
"STYLES + 1\n"
"string.text"
msgid "Error while saving the document to the clipboard! The following action cannot be undone."
msgstr "Eroare la salvarea documentului în clipboard! Acțiunea următoare nu poate fi anulată."

#: template.src
msgctxt ""
"template.src\n"
"STYLES + 2\n"
"string.text"
msgid "~Cancel"
msgstr "~Renunță"

#: template.src
msgctxt ""
"template.src\n"
"STYLES + 3\n"
"string.text"
msgid "~OK"
msgstr "~OK"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME\n"
"string.text"
msgid "(Standard)"
msgstr "(Standard)"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 1\n"
"string.text"
msgid "Autumn Leaves"
msgstr "Autumn Leaves"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 2\n"
"string.text"
msgid "Be"
msgstr "Be"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 3\n"
"string.text"
msgid "Black and White"
msgstr "Black and White"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 4\n"
"string.text"
msgid "Blackberry Bush"
msgstr "Blackberry Bush"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 5\n"
"string.text"
msgid "Blue Jeans"
msgstr "Blue Jeans"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 6\n"
"string.text"
msgid "Fifties Diner"
msgstr "Fifties Diner"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 7\n"
"string.text"
msgid "Glacier"
msgstr "Glacier"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 8\n"
"string.text"
msgid "Green Grapes"
msgstr "Green Grapes"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 9\n"
"string.text"
msgid "Marine"
msgstr "Albastru marin"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 10\n"
"string.text"
msgid "Millennium"
msgstr "Millennium"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 11\n"
"string.text"
msgid "Nature"
msgstr "Nature"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 12\n"
"string.text"
msgid "Neon"
msgstr "Neon"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 13\n"
"string.text"
msgid "Night"
msgstr "Night"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 14\n"
"string.text"
msgid "PC Nostalgia"
msgstr "PC Nostalgia"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 15\n"
"string.text"
msgid "Pastel"
msgstr "Pastel"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 16\n"
"string.text"
msgid "Pool Party"
msgstr "Pool Party"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 17\n"
"string.text"
msgid "Pumpkin"
msgstr "Pumpkin"

#: template.src
msgctxt ""
"template.src\n"
"AgendaDlgName\n"
"string.text"
msgid "Minutes Template"
msgstr "Șablon subiecte"

#: template.src
msgctxt ""
"template.src\n"
"AgendaDlgNoCancel\n"
"string.text"
msgid "An option must be confirmed."
msgstr "O opțiune trebuie confirmată."

#: template.src
msgctxt ""
"template.src\n"
"AgendaDlgFrame\n"
"string.text"
msgid "Minutes Type"
msgstr "Tip subiecte"

#: template.src
msgctxt ""
"template.src\n"
"AgendaDlgButton1\n"
"string.text"
msgid "Results Minutes"
msgstr "Subiecte rezultate"

#: template.src
msgctxt ""
"template.src\n"
"AgendaDlgButton2\n"
"string.text"
msgid "Evaluation Minutes"
msgstr "Subiecte de evaluare"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceNoTextmark\n"
"string.text"
msgid "The bookmark 'Recipient' is missing."
msgstr "Semnul de carte 'Destinatar' lipsește."

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceNoTextmark+1\n"
"string.text"
msgid "Form letter fields can not be included."
msgstr "Câmpurile de litere ale formularului nu pot fi incluse."

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceMsgError\n"
"string.text"
msgid "An error has occurred."
msgstr "A apărut o eroare."

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceDialog\n"
"string.text"
msgid "Addressee"
msgstr "Destinatar"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceDialog+1\n"
"string.text"
msgid "One recipient"
msgstr "Un destinatar"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceDialog+2\n"
"string.text"
msgid "Several recipients (address database)"
msgstr "Mai mulți destinatari (bază de date adrese)"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceDialog+3\n"
"string.text"
msgid "Use of This Template"
msgstr "Utilizarea acestui șablon"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields\n"
"string.text"
msgid "Click placeholder and overwrite"
msgstr "Clic pe simbol de substituire și suprascrie"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+1\n"
"string.text"
msgid "Company"
msgstr "Societate"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+2\n"
"string.text"
msgid "Department"
msgstr "Departament"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+3\n"
"string.text"
msgid "First Name"
msgstr "Prenume"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+4\n"
"string.text"
msgid "Last Name"
msgstr "Nume"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+5\n"
"string.text"
msgid "Street"
msgstr "Stradă"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+6\n"
"string.text"
msgid "Country"
msgstr "Țară"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+7\n"
"string.text"
msgid "ZIP/Postal Code"
msgstr "Cod Poștal (Zip )/Oraș"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+8\n"
"string.text"
msgid "City"
msgstr "Localitate"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+9\n"
"string.text"
msgid "Title"
msgstr "Titlu"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+10\n"
"string.text"
msgid "Position"
msgstr "Poziție"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+11\n"
"string.text"
msgid "Form of Address"
msgstr "Forma adresei"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+12\n"
"string.text"
msgid "Initials"
msgstr "Inițiale"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+13\n"
"string.text"
msgid "Salutation"
msgstr "Introducere"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+14\n"
"string.text"
msgid "Home Phone"
msgstr "Telefon de acasă"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+15\n"
"string.text"
msgid "Work Phone"
msgstr "Telefon de serviciu"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+16\n"
"string.text"
msgid "Fax"
msgstr "Fax"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+17\n"
"string.text"
msgid "E-Mail"
msgstr "E-mail"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+18\n"
"string.text"
msgid "URL"
msgstr "URL"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+19\n"
"string.text"
msgid "Notes"
msgstr "Note"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+20\n"
"string.text"
msgid "Alt. Field 1"
msgstr "Câmp alt. 1"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+21\n"
"string.text"
msgid "Alt. Field 2"
msgstr "Câmp alt. 2"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+22\n"
"string.text"
msgid "Alt. Field 3"
msgstr "Câmp alt. 3"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+23\n"
"string.text"
msgid "Alt. Field 4"
msgstr "Câmp alt. 4"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+24\n"
"string.text"
msgid "ID"
msgstr "ID"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+25\n"
"string.text"
msgid "State"
msgstr "Stat"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+26\n"
"string.text"
msgid "Office Phone"
msgstr "Telefon de la birou"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+27\n"
"string.text"
msgid "Pager"
msgstr "Pager"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+28\n"
"string.text"
msgid "Mobile Phone"
msgstr "Telefon mobil"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+29\n"
"string.text"
msgid "Other Phone"
msgstr "Alt telefon"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+30\n"
"string.text"
msgid "Calendar URL"
msgstr "URL calendar"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+31\n"
"string.text"
msgid "Invite"
msgstr "Invită"

#: template.src
msgctxt ""
"template.src\n"
"TextField\n"
"string.text"
msgid "User data field is not defined!"
msgstr "Câmpul de date al utilizatorului nu este definit!"

#: template.src
msgctxt ""
"template.src\n"
"Newsletter\n"
"string.text"
msgid "General layout"
msgstr "Aspect general"

#: template.src
msgctxt ""
"template.src\n"
"Newsletter + 1\n"
"string.text"
msgid "Default layout"
msgstr "Aspect implicit"

#: template.src
msgctxt ""
"template.src\n"
"Newsletter + 2\n"
"string.text"
msgid "Commemorative publication layout"
msgstr "Aspect publicație comemorativă"

#: template.src
msgctxt ""
"template.src\n"
"Newsletter + 3\n"
"string.text"
msgid "Brochure layout"
msgstr "Aspect broșură"

#: template.src
msgctxt ""
"template.src\n"
"Newsletter + 10\n"
"string.text"
msgid "Format"
msgstr "Formatare"

#: template.src
msgctxt ""
"template.src\n"
"Newsletter + 11\n"
"string.text"
msgid "Single-sided"
msgstr "O singură față"

#: template.src
msgctxt ""
"template.src\n"
"Newsletter + 12\n"
"string.text"
msgid "Double-sided"
msgstr "Față-verso"

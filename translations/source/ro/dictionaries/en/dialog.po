#. extracted from dictionaries/en/dialog
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-11-20 13:01+0100\n"
"PO-Revision-Date: 2014-01-30 11:38+0000\n"
"Last-Translator: Ákos <nagy.akos@libreoffice.ro>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2);;\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1391081939.0\n"

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"spelling\n"
"property.text"
msgid "Grammar checking"
msgstr "Verificarea gramaticii"

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"hlp_grammar\n"
"property.text"
msgid "Check more grammar errors."
msgstr "Caută mai multe erori gramaticale."

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"grammar\n"
"property.text"
msgid "Possible mistakes"
msgstr "Posibile greșeli"

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"hlp_cap\n"
"property.text"
msgid "Check missing capitalization of sentences."
msgstr "Verificarea literei mari la început de propoziție."

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"cap\n"
"property.text"
msgid "Capitalization"
msgstr "Capitalizare"

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"hlp_dup\n"
"property.text"
msgid "Check repeated words."
msgstr "Verifică repetarea cuvintelor."

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"dup\n"
"property.text"
msgid "Word duplication"
msgstr "Duplicări de cuvinte"

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"hlp_pair\n"
"property.text"
msgid "Check missing or extra parentheses and quotation marks."
msgstr "Verificarea lipsa sau plusul semnelor de citație sau a parantezelor."

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"pair\n"
"property.text"
msgid "Parentheses"
msgstr "Paranteze"

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"punctuation\n"
"property.text"
msgid "Punctuation"
msgstr "Punctuație"

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"hlp_spaces\n"
"property.text"
msgid "Check single spaces between words."
msgstr "Verificarea spațiilor între cuvinte."

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"spaces\n"
"property.text"
msgid "Word spacing"
msgstr "Spații"

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"hlp_mdash\n"
"property.text"
msgid "Force unspaced em dash instead of spaced en dash."
msgstr "Utilizarea semnului de dialog cu spații în loc de semul de pauză."

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"mdash\n"
"property.text"
msgid "Em dash"
msgstr "Linie de pauză"

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"hlp_ndash\n"
"property.text"
msgid "Force spaced en dash instead of unspaced em dash."
msgstr "Folosirea liniei de dialog cu spații în locul liniei de pauză."

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"ndash\n"
"property.text"
msgid "En dash"
msgstr "Linie de dialog"

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"hlp_quotation\n"
"property.text"
msgid "Check double quotation marks: \"x\" → “x”"
msgstr "Schimbarea semnelor de citație: \"x\" → “x”"

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"quotation\n"
"property.text"
msgid "Quotation marks"
msgstr "Semne de citație"

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"hlp_times\n"
"property.text"
msgid "Check true multiplication sign: 5x5 → 5×5"
msgstr "Sem de înmulțire adevărat: 5x5 → 5×5"

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"times\n"
"property.text"
msgid "Multiplication sign"
msgstr "Semn de înmulțire"

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"hlp_spaces2\n"
"property.text"
msgid "Check single spaces between sentences."
msgstr "Între propoziții să fie un singur spațiu."

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"spaces2\n"
"property.text"
msgid "Sentence spacing"
msgstr "Spații între propoziții"

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"hlp_spaces3\n"
"property.text"
msgid "Check more than two extra space characters between words and sentences."
msgstr "Verifică spațiile multiple între cuvinte sau propoziții."

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"spaces3\n"
"property.text"
msgid "More spaces"
msgstr "Mai multe spații"

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"hlp_minus\n"
"property.text"
msgid "Change hyphen characters to real minus signs."
msgstr "Utilizarea semnului minus în loc de cratimă."

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"minus\n"
"property.text"
msgid "Minus sign"
msgstr "Semn minus"

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"hlp_apostrophe\n"
"property.text"
msgid "Change typewriter apostrophe, single quotation marks and correct double primes."
msgstr "În loc de citațiile drepte utilizarea citațiilor tipografice."

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"apostrophe\n"
"property.text"
msgid "Apostrophe"
msgstr "Apostrof"

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"hlp_ellipsis\n"
"property.text"
msgid "Change three dots with ellipsis."
msgstr "Schimbarea a trei puncte pe semn tipografic."

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"ellipsis\n"
"property.text"
msgid "Ellipsis"
msgstr "Elipsă"

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"others\n"
"property.text"
msgid "Others"
msgstr "Alții"

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"hlp_metric\n"
"property.text"
msgid "Measurement conversion from °F, mph, ft, in, lb, gal and miles."
msgstr "Schimbare unităților de măsură (°F, mph, ft, in, lb, gal și miles)."

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"metric\n"
"property.text"
msgid "Convert to metric (°C, km/h, m, kg, l)"
msgstr "Convertire la unități metrice (°C, km/h, m, kg, l)"

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"hlp_numsep\n"
"property.text"
msgid "Common (1000000 → 1,000,000) or ISO (1000000 → 1 000 000)."
msgstr "Comun (1000000 → 1,000,000) sau ISO (1000000 → 1 000 000)."

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"numsep\n"
"property.text"
msgid "Thousand separation of large numbers"
msgstr "Separarea miilor la numere mari"

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"hlp_nonmetric\n"
"property.text"
msgid "Measurement conversion from °C; km/h; cm, m, km; kg; l."
msgstr "Convertirea de la °C; km/h; cm, m, km; kg; l."

#: en_en_US.properties
msgctxt ""
"en_en_US.properties\n"
"nonmetric\n"
"property.text"
msgid "Convert to non-metric (°F, mph, ft, lb, gal)"
msgstr "Convertire la non-metrice (°F, mph, ft, lb, gal)"

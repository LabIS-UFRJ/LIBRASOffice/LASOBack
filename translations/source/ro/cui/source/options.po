#. extracted from cui/source/options
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-12-22 20:36+0000\n"
"Last-Translator: Jobava <jobaval10n@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2);;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1482438990.000000\n"

#: connpooloptions.src
msgctxt ""
"connpooloptions.src\n"
"RID_SVXSTR_DRIVER_NAME\n"
"string.text"
msgid "Driver name"
msgstr "Nume driver"

#: connpooloptions.src
msgctxt ""
"connpooloptions.src\n"
"RID_SVXSTR_POOLED_FLAG\n"
"string.text"
msgid "Pool"
msgstr "Memorare temporară"

#: connpooloptions.src
msgctxt ""
"connpooloptions.src\n"
"RID_SVXSTR_POOL_TIMEOUT\n"
"string.text"
msgid "Timeout"
msgstr "Timp de așteptare"

#: connpooloptions.src
msgctxt ""
"connpooloptions.src\n"
"RID_SVXSTR_YES\n"
"string.text"
msgid "Yes"
msgstr "Da"

#: connpooloptions.src
msgctxt ""
"connpooloptions.src\n"
"RID_SVXSTR_NO\n"
"string.text"
msgid "No"
msgstr "Nu"

#: dbregister.src
msgctxt ""
"dbregister.src\n"
"RID_SVXSTR_TYPE\n"
"string.text"
msgid "Registered name"
msgstr "Nume înregistrat"

#: dbregister.src
msgctxt ""
"dbregister.src\n"
"RID_SVXSTR_PATH\n"
"string.text"
msgid "Database file"
msgstr "Fișier bază de date"

#: doclinkdialog.src
msgctxt ""
"doclinkdialog.src\n"
"STR_LINKEDDOC_DOESNOTEXIST\n"
"string.text"
msgid ""
"The file\n"
"$file$\n"
"does not exist."
msgstr ""
"Fișierul\n"
"$file$\n"
" nu există."

#: doclinkdialog.src
msgctxt ""
"doclinkdialog.src\n"
"STR_LINKEDDOC_NO_SYSTEM_FILE\n"
"string.text"
msgid ""
"The file\n"
"$file$\n"
"does not exist in the local file system."
msgstr ""
"Fișierul\n"
"$file$\n"
"nu există pe sistemul local de fișiere."

#: doclinkdialog.src
msgctxt ""
"doclinkdialog.src\n"
"STR_NAME_CONFLICT\n"
"string.text"
msgid ""
"The name '$file$' is already used for another database.\n"
"Please choose a different name."
msgstr ""
"Există deja o bază de date numită „$file$”.\n"
"Alegeți un nume diferit."

#: doclinkdialog.src
msgctxt ""
"doclinkdialog.src\n"
"RID_SVXSTR_QUERY_DELETE_CONFIRM\n"
"string.text"
msgid "Do you want to delete the entry?"
msgstr "Doriți să ștergeți intrarea?"

#: optchart.src
msgctxt ""
"optchart.src\n"
"RID_SVXSTR_DIAGRAM_ROW\n"
"string.text"
msgid "Data Series $(ROW)"
msgstr "Serie de date $(ROW)"

#: optchart.src
msgctxt ""
"optchart.src\n"
"RID_OPTSTR_COLOR_CHART_DELETE\n"
"string.text"
msgid "Chart Color Deletion"
msgstr "Ștergere culoare diagramă"

#: optcolor.src
msgctxt ""
"optcolor.src\n"
"RID_SVXSTR_COLOR_CONFIG_DELETE\n"
"string.text"
msgid "Do you really want to delete the color scheme?"
msgstr "Sunteți sigur că doriți să ștergeți această schemă de culori?"

#: optcolor.src
msgctxt ""
"optcolor.src\n"
"RID_SVXSTR_COLOR_CONFIG_DELETE_TITLE\n"
"string.text"
msgid "Color Scheme Deletion"
msgstr "Eliminare schemă de culori"

#: optcolor.src
msgctxt ""
"optcolor.src\n"
"RID_SVXSTR_COLOR_CONFIG_SAVE1\n"
"string.text"
msgid "Save scheme"
msgstr "Salvează schema"

#: optcolor.src
msgctxt ""
"optcolor.src\n"
"RID_SVXSTR_COLOR_CONFIG_SAVE2\n"
"string.text"
msgid "Name of color scheme"
msgstr "Numele schemei de culori"

#: optdict.src
msgctxt ""
"optdict.src\n"
"RID_SVXSTR_OPT_DOUBLE_DICTS\n"
"string.text"
msgid ""
"The specified name already exists.\n"
"Please enter a new name."
msgstr ""
"Numele specificat există deja.\n"
"Introduceți un nume nou."

#: optdict.src
msgctxt ""
"optdict.src\n"
"STR_MODIFY\n"
"string.text"
msgid "~Replace"
msgstr "~Înlocuire"

#: optdict.src
msgctxt ""
"optdict.src\n"
"RID_SVXSTR_CONFIRM_SET_LANGUAGE\n"
"string.text"
msgid "Do you want to change the '%1' dictionary language?"
msgstr "Doriți să modificați dicționarul de limbaj '%1'?"

#: optfltr.src
msgctxt ""
"optfltr.src\n"
"RID_SVXSTR_HEADER1\n"
"string.text"
msgid "[L]"
msgstr "[Î]"

#: optfltr.src
msgctxt ""
"optfltr.src\n"
"RID_SVXSTR_HEADER2\n"
"string.text"
msgid "[S]"
msgstr "[S]"

#: optfltr.src
msgctxt ""
"optfltr.src\n"
"RID_SVXSTR_CHG_MATH\n"
"string.text"
msgid "MathType to %PRODUCTNAME Math or reverse"
msgstr "MathType → %PRODUCTNAME Math sau invers"

#: optfltr.src
msgctxt ""
"optfltr.src\n"
"RID_SVXSTR_CHG_WRITER\n"
"string.text"
msgid "WinWord to %PRODUCTNAME Writer or reverse"
msgstr "WinWord → %PRODUCTNAME Writer sau invers"

#: optfltr.src
msgctxt ""
"optfltr.src\n"
"RID_SVXSTR_CHG_CALC\n"
"string.text"
msgid "Excel to %PRODUCTNAME Calc or reverse"
msgstr "Excel → %PRODUCTNAME Calc sau invers"

#: optfltr.src
msgctxt ""
"optfltr.src\n"
"RID_SVXSTR_CHG_IMPRESS\n"
"string.text"
msgid "PowerPoint to %PRODUCTNAME Impress or reverse"
msgstr "PowerPoint → %PRODUCTNAME Impress sau invers"

#: optfltr.src
msgctxt ""
"optfltr.src\n"
"RID_SVXSTR_CHG_SMARTART\n"
"string.text"
msgid "SmartArt to %PRODUCTNAME shapes or reverse"
msgstr "SmartArt pentru forme și inverse %PRODUCTNAME"

#: optinet2.src
msgctxt ""
"optinet2.src\n"
"RID_SVXSTR_OPT_PROXYPORTS\n"
"string.text"
msgid ""
"Invalid value!\n"
"\n"
"The maximum value for a port number is 65535."
msgstr ""
"Valoare invalidă!\n"
"\n"
"Valoarea maximă pentru un număr de port este 65535."

#: optjava.src
msgctxt ""
"optjava.src\n"
"RID_SVXSTR_JRE_NOT_RECOGNIZED\n"
"string.text"
msgid ""
"The folder you selected does not contain a Java runtime environment.\n"
"Please select a different folder."
msgstr ""
"Dosarul pe care l-ați selectat nu conține un mediu de execuție Java.\n"
"Selectați un alt dosar."

#: optjava.src
msgctxt ""
"optjava.src\n"
"RID_SVXSTR_JRE_FAILED_VERSION\n"
"string.text"
msgid ""
"The Java runtime environment you selected is not the required version.\n"
"Please select a different folder."
msgstr ""
"Versiunea mediului de execuție Java selectată este prea veche.\n"
"Selectați alt dosar."

#: optjava.src
msgctxt ""
"optjava.src\n"
"RID_SVXSTR_OPTIONS_RESTART\n"
"string.text"
msgid "Please restart %PRODUCTNAME now so the new or modified values can take effect."
msgstr "Reporniți %PRODUCTNAME acum pentru ca valorile noi sau modificate să aibă efect."

#: optjava.src
msgctxt ""
"optjava.src\n"
"RID_SVXSTR_JAVA_START_PARAM\n"
"string.text"
msgid "Edit Parameter"
msgstr "Editează parametrul"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_SPELL\n"
"string.text"
msgid "Spelling"
msgstr "Ortografie"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_HYPH\n"
"string.text"
msgid "Hyphenation"
msgstr "Silabisire"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_THES\n"
"string.text"
msgid "Thesaurus"
msgstr "Tezaur"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_GRAMMAR\n"
"string.text"
msgid "Grammar"
msgstr "Gramatică"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_CAPITAL_WORDS\n"
"string.text"
msgid "Check uppercase words"
msgstr "Verifică cuvintele cu majuscule"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_WORDS_WITH_DIGITS\n"
"string.text"
msgid "Check words with numbers "
msgstr "Verifică cuvintele cu numere"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_SPELL_SPECIAL\n"
"string.text"
msgid "Check special regions"
msgstr "Verifică regiunile speciale"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_SPELL_AUTO\n"
"string.text"
msgid "Check spelling as you type"
msgstr "Verifică ortografia în timp ce tastez"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_GRAMMAR_AUTO\n"
"string.text"
msgid "Check grammar as you type"
msgstr "Verifică gramatica în timp ce tastez"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_NUM_MIN_WORDLEN\n"
"string.text"
msgid "Minimal number of characters for hyphenation: "
msgstr "Număr minim de caractere pentru silabisire:"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_NUM_PRE_BREAK\n"
"string.text"
msgid "Characters before line break: "
msgstr "Caractere înainte de întreruperea de linie:"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_NUM_POST_BREAK\n"
"string.text"
msgid "Characters after line break: "
msgstr "Caractere după întreruperea de linie:"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_HYPH_AUTO\n"
"string.text"
msgid "Hyphenate without inquiry"
msgstr "Silabisește fără interogare"

#: optlingu.src
msgctxt ""
"optlingu.src\n"
"RID_SVXSTR_HYPH_SPECIAL\n"
"string.text"
msgid "Hyphenate special regions"
msgstr "Silabisire în regiuni speciale"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_CONFIG_DIR\n"
"string.text"
msgid "Configuration"
msgstr "Configurație"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_WORK_PATH\n"
"string.text"
msgid "My Documents"
msgstr "Documentele Mele"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_GRAPHICS_PATH\n"
"string.text"
msgid "Images"
msgstr "Imagini"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_BITMAP_PATH\n"
"string.text"
msgid "Icons"
msgstr "Iconițe"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_PALETTE_PATH\n"
"string.text"
msgid "Palettes"
msgstr "Palete"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_BACKUP_PATH\n"
"string.text"
msgid "Backups"
msgstr "Copii de siguranță"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_MODULES_PATH\n"
"string.text"
msgid "Modules"
msgstr "Module"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_TEMPLATE_PATH\n"
"string.text"
msgid "Templates"
msgstr "Șabloane"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_GLOSSARY_PATH\n"
"string.text"
msgid "AutoText"
msgstr "Text automat"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_DICTIONARY_PATH\n"
"string.text"
msgid "Dictionaries"
msgstr "Dictionare"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_HELP_DIR\n"
"string.text"
msgid "Help"
msgstr "Ajutor"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_GALLERY_DIR\n"
"string.text"
msgid "Gallery"
msgstr "Galerie"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_STORAGE_DIR\n"
"string.text"
msgid "Message Storage"
msgstr "Stocare mesaje"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_TEMP_PATH\n"
"string.text"
msgid "Temporary files"
msgstr "Fișiere temporare"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_PLUGINS_PATH\n"
"string.text"
msgid "Plug-ins"
msgstr "Module"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_FAVORITES_DIR\n"
"string.text"
msgid "Folder Bookmarks"
msgstr "Semne de carte a directoarelor"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_FILTER_PATH\n"
"string.text"
msgid "Filters"
msgstr "Filtre"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_ADDINS_PATH\n"
"string.text"
msgid "Add-ins"
msgstr "Add-inuri"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_USERCONFIG_PATH\n"
"string.text"
msgid "User Configuration"
msgstr "Configurare utilizator"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_USERDICTIONARY_DIR\n"
"string.text"
msgid "User-defined dictionaries"
msgstr "Dicționare definite de utilizator"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_CLASSIFICATION_PATH\n"
"string.text"
msgid "Classification"
msgstr "Clasificare"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_AUTOCORRECT_DIR\n"
"string.text"
msgid "AutoCorrect"
msgstr "Formatare automată"

#: optpath.src
msgctxt ""
"optpath.src\n"
"RID_SVXSTR_KEY_LINGUISTIC_DIR\n"
"string.text"
msgid "Writing aids"
msgstr "Ajutor la scriere"

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_SEARCHTERM\n"
"string.text"
msgid "Search term"
msgstr "Caută termen"

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_SELECTEDPERSONA\n"
"string.text"
msgid "Selected Theme: "
msgstr "Tema selectată: "

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_SEARCHING\n"
"string.text"
msgid "Searching, please wait..."
msgstr "Așteptați, se caută..."

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_SEARCHERROR\n"
"string.text"
msgid "Cannot open %1, please try again later."
msgstr "Nu se poate deschide %1, încercați mai târziu."

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_NORESULTS\n"
"string.text"
msgid "No results found."
msgstr "Niciun rezultat găsit."

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_APPLYPERSONA\n"
"string.text"
msgid "Applying Theme..."
msgstr "Aplică tema..."

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_PERSONA_CATEGORIES\n"
"LibreOffice\n"
"itemlist.text"
msgid "LibreOffice"
msgstr "LibreOffice"

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_PERSONA_CATEGORIES\n"
"Abstract\n"
"itemlist.text"
msgid "Abstract"
msgstr "Rezumat"

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_PERSONA_CATEGORIES\n"
"Color\n"
"itemlist.text"
msgid "Color"
msgstr "Culoare"

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_PERSONA_CATEGORIES\n"
"Music\n"
"itemlist.text"
msgid "Music"
msgstr "Muzică"

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_PERSONA_CATEGORIES\n"
"Nature\n"
"itemlist.text"
msgid "Nature"
msgstr "Natură"

#: personalization.src
msgctxt ""
"personalization.src\n"
"RID_SVXSTR_PERSONA_CATEGORIES\n"
"Solid\n"
"itemlist.text"
msgid "Solid"
msgstr "Uniform"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"%PRODUCTNAME\n"
"itemlist.text"
msgid "%PRODUCTNAME"
msgstr "%PRODUCTNAME"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"User Data\n"
"itemlist.text"
msgid "User Data"
msgstr "Datele utilizatorului"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"General\n"
"itemlist.text"
msgid "General"
msgstr "General"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"Memory\n"
"itemlist.text"
msgid "Memory"
msgstr "Memorie"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"View\n"
"itemlist.text"
msgid "View"
msgstr "Vizualizare"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"Print\n"
"itemlist.text"
msgid "Print"
msgstr "Tipărire"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"Paths\n"
"itemlist.text"
msgid "Paths"
msgstr "Căi"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"Fonts\n"
"itemlist.text"
msgid "Fonts"
msgstr "Fonturi"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"Security\n"
"itemlist.text"
msgid "Security"
msgstr "Securitate"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"Personalization\n"
"itemlist.text"
msgid "Personalization"
msgstr "Personalizare"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"Application Colors\n"
"itemlist.text"
msgid "Application Colors"
msgstr "Culori aplicație"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"Accessibility\n"
"itemlist.text"
msgid "Accessibility"
msgstr "Accesibilitate"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"Advanced\n"
"itemlist.text"
msgid "Advanced"
msgstr "Avansat"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"Basic IDE Options\n"
"itemlist.text"
msgid "Basic IDE Options"
msgstr "Opțiuni de bază IDE"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"Online Update\n"
"itemlist.text"
msgid "Online Update"
msgstr "Actualizare online"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_GENERAL_OPTIONS\n"
"OpenCL\n"
"itemlist.text"
msgid "OpenCL"
msgstr "OpenCL"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_LANGUAGE_OPTIONS\n"
"Language Settings\n"
"itemlist.text"
msgid "Language Settings"
msgstr "Configurări limbă"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_LANGUAGE_OPTIONS\n"
"Languages\n"
"itemlist.text"
msgid "Languages"
msgstr "Limbi"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_LANGUAGE_OPTIONS\n"
"Writing Aids\n"
"itemlist.text"
msgid "Writing Aids"
msgstr "Asistenți pentru scriere"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_LANGUAGE_OPTIONS\n"
"Searching in Japanese\n"
"itemlist.text"
msgid "Searching in Japanese"
msgstr "Se caută în japoneză"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_LANGUAGE_OPTIONS\n"
"Asian Layout\n"
"itemlist.text"
msgid "Asian Layout"
msgstr "Aranjament asiatic"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_LANGUAGE_OPTIONS\n"
"Complex Text Layout\n"
"itemlist.text"
msgid "Complex Text Layout"
msgstr "Aranjare complexă a textului"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_INET_DLG\n"
"Internet\n"
"itemlist.text"
msgid "Internet"
msgstr "Internet"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_INET_DLG\n"
"Proxy\n"
"itemlist.text"
msgid "Proxy"
msgstr "Proxy"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_INET_DLG\n"
"E-mail\n"
"itemlist.text"
msgid "E-mail"
msgstr "E-mail"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"%PRODUCTNAME Writer\n"
"itemlist.text"
msgid "%PRODUCTNAME Writer"
msgstr "%PRODUCTNAME Writer"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"General\n"
"itemlist.text"
msgid "General"
msgstr "General"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"View\n"
"itemlist.text"
msgid "View"
msgstr "Vizualizare"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"Formatting Aids\n"
"itemlist.text"
msgid "Formatting Aids"
msgstr "Ajutoare formatare"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"Grid\n"
"itemlist.text"
msgid "Grid"
msgstr "Grilă"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"Basic Fonts (Western)\n"
"itemlist.text"
msgid "Basic Fonts (Western)"
msgstr "Fonturi de bază (vestice)"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"Basic Fonts (Asian)\n"
"itemlist.text"
msgid "Basic Fonts (Asian)"
msgstr "Fonturi de bază (asiatice)"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"Basic Fonts (CTL)\n"
"itemlist.text"
msgid "Basic Fonts (CTL)"
msgstr "Fonturi de bază (CTL)"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"Print\n"
"itemlist.text"
msgid "Print"
msgstr "Tipărire"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"Table\n"
"itemlist.text"
msgid "Table"
msgstr "Tabel"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"Changes\n"
"itemlist.text"
msgid "Changes"
msgstr "Modificări"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"Comparison\n"
"itemlist.text"
msgid "Comparison"
msgstr "Comparație"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"Compatibility\n"
"itemlist.text"
msgid "Compatibility"
msgstr "Compatibilitate"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"AutoCaption\n"
"itemlist.text"
msgid "AutoCaption"
msgstr "Legendă automată"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_EDITOPTIONS\n"
"Mail Merge E-mail\n"
"itemlist.text"
msgid "Mail Merge E-mail"
msgstr "Trimite e-mail în serie"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_ONLINEOPTIONS\n"
"%PRODUCTNAME Writer/Web\n"
"itemlist.text"
msgid "%PRODUCTNAME Writer/Web"
msgstr "%PRODUCTNAME Writer/Web"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_ONLINEOPTIONS\n"
"View\n"
"itemlist.text"
msgid "View"
msgstr "Vizualizare"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_ONLINEOPTIONS\n"
"Formatting Aids\n"
"itemlist.text"
msgid "Formatting Aids"
msgstr "Ajutoare formatare"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_ONLINEOPTIONS\n"
"Grid\n"
"itemlist.text"
msgid "Grid"
msgstr "Grilă"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_ONLINEOPTIONS\n"
"Print\n"
"itemlist.text"
msgid "Print"
msgstr "Tipărire"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_ONLINEOPTIONS\n"
"Table\n"
"itemlist.text"
msgid "Table"
msgstr "Tabel"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SW_ONLINEOPTIONS\n"
"Background\n"
"itemlist.text"
msgid "Background"
msgstr "Fundal"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SM_EDITOPTIONS\n"
"%PRODUCTNAME Math\n"
"itemlist.text"
msgid "%PRODUCTNAME Math"
msgstr "%PRODUCTNAME Math"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SM_EDITOPTIONS\n"
"Settings\n"
"itemlist.text"
msgid "Settings"
msgstr "Configurări"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SC_EDITOPTIONS\n"
"%PRODUCTNAME Calc\n"
"itemlist.text"
msgid "%PRODUCTNAME Calc"
msgstr "%PRODUCTNAME Calc"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SC_EDITOPTIONS\n"
"General\n"
"itemlist.text"
msgid "General"
msgstr "General"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SC_EDITOPTIONS\n"
"Defaults\n"
"itemlist.text"
msgid "Defaults"
msgstr "Implicite"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SC_EDITOPTIONS\n"
"View\n"
"itemlist.text"
msgid "View"
msgstr "Vizualizare"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SC_EDITOPTIONS\n"
"Calculate\n"
"itemlist.text"
msgid "Calculate"
msgstr "Calculează"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SC_EDITOPTIONS\n"
"Formula\n"
"itemlist.text"
msgid "Formula"
msgstr "Formulă"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SC_EDITOPTIONS\n"
"Sort Lists\n"
"itemlist.text"
msgid "Sort Lists"
msgstr "Sortare liste"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SC_EDITOPTIONS\n"
"Changes\n"
"itemlist.text"
msgid "Changes"
msgstr "Modificări"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SC_EDITOPTIONS\n"
"Compatibility\n"
"itemlist.text"
msgid "Compatibility"
msgstr "Compatibilitate"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SC_EDITOPTIONS\n"
"Grid\n"
"itemlist.text"
msgid "Grid"
msgstr "Grilă"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SC_EDITOPTIONS\n"
"Print\n"
"itemlist.text"
msgid "Print"
msgstr "Tipărire"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SD_EDITOPTIONS\n"
"%PRODUCTNAME Impress\n"
"itemlist.text"
msgid "%PRODUCTNAME Impress"
msgstr "%PRODUCTNAME Impress"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SD_EDITOPTIONS\n"
"General\n"
"itemlist.text"
msgid "General"
msgstr "General"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SD_EDITOPTIONS\n"
"View\n"
"itemlist.text"
msgid "View"
msgstr "Vizualizare"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SD_EDITOPTIONS\n"
"Grid\n"
"itemlist.text"
msgid "Grid"
msgstr "Grilă"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SD_EDITOPTIONS\n"
"Print\n"
"itemlist.text"
msgid "Print"
msgstr "Tipărire"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SD_GRAPHIC_OPTIONS\n"
"%PRODUCTNAME Draw\n"
"itemlist.text"
msgid "%PRODUCTNAME Draw"
msgstr "%PRODUCTNAME Draw"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SD_GRAPHIC_OPTIONS\n"
"General\n"
"itemlist.text"
msgid "General"
msgstr "General"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SD_GRAPHIC_OPTIONS\n"
"View\n"
"itemlist.text"
msgid "View"
msgstr "Vizualizare"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SD_GRAPHIC_OPTIONS\n"
"Grid\n"
"itemlist.text"
msgid "Grid"
msgstr "Grilă"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SD_GRAPHIC_OPTIONS\n"
"Print\n"
"itemlist.text"
msgid "Print"
msgstr "Tipărire"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SCH_EDITOPTIONS\n"
"Charts\n"
"itemlist.text"
msgid "Charts"
msgstr "Grafice"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SCH_EDITOPTIONS\n"
"Default Colors\n"
"itemlist.text"
msgid "Default Colors"
msgstr "Culori implicite"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_FILTER_DLG\n"
"Load/Save\n"
"itemlist.text"
msgid "Load/Save"
msgstr "Încarcă/Salvează"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_FILTER_DLG\n"
"General\n"
"itemlist.text"
msgid "General"
msgstr "General"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_FILTER_DLG\n"
"VBA Properties\n"
"itemlist.text"
msgid "VBA Properties"
msgstr "Proprietăți VBA"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_FILTER_DLG\n"
"Microsoft Office\n"
"itemlist.text"
msgid "Microsoft Office"
msgstr "Microsoft Office"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_FILTER_DLG\n"
"HTML Compatibility\n"
"itemlist.text"
msgid "HTML Compatibility"
msgstr "Compatibilitate HTML"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SB_STARBASEOPTIONS\n"
"%PRODUCTNAME Base\n"
"itemlist.text"
msgid "%PRODUCTNAME Base"
msgstr "%PRODUCTNAME Base"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SB_STARBASEOPTIONS\n"
"Connections\n"
"itemlist.text"
msgid "Connections"
msgstr "Conexiuni"

#: treeopt.src
msgctxt ""
"treeopt.src\n"
"SID_SB_STARBASEOPTIONS\n"
"Databases\n"
"itemlist.text"
msgid "Databases"
msgstr "Baze de date"

#. extracted from svx/source/fmcomp
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-03-09 20:48+0100\n"
"PO-Revision-Date: 2013-05-23 23:56+0000\n"
"Last-Translator: system user <>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ks\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Pootle 2.7\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1369353415.000000\n"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_ROWS\n"
"SID_FM_DELETEROWS\n"
"menuitem.text"
msgid "Delete Rows"
msgstr "سطوركریو خئارج"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_ROWS\n"
"SID_FM_RECORD_SAVE\n"
"menuitem.text"
msgid "Save Record"
msgstr "ریکاڈ تھئیویومحفوظ  "

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_ROWS\n"
"SID_FM_RECORD_UNDO\n"
"menuitem.text"
msgid "Undo: Data entry"
msgstr "انڈو: ڈاٹا دئخلئ"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_INSERTCOL\n"
"SID_FM_EDIT\n"
"menuitem.text"
msgid "Text Box"
msgstr "ٹیکسٹ باکس"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_INSERTCOL\n"
"SID_FM_CHECKBOX\n"
"menuitem.text"
msgid "Check Box"
msgstr "چیک باکس"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_INSERTCOL\n"
"SID_FM_COMBOBOX\n"
"menuitem.text"
msgid "Combo Box"
msgstr "کومبو باکس"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_INSERTCOL\n"
"SID_FM_LISTBOX\n"
"menuitem.text"
msgid "List Box"
msgstr "لسٹ باکس"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_INSERTCOL\n"
"SID_FM_DATEFIELD\n"
"menuitem.text"
msgid "Date Field"
msgstr "شعبئی تئریخ"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_INSERTCOL\n"
"SID_FM_TIMEFIELD\n"
"menuitem.text"
msgid "Time Field"
msgstr "وقت شعبہ"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_INSERTCOL\n"
"SID_FM_NUMERICFIELD\n"
"menuitem.text"
msgid "Numeric Field"
msgstr "شمئرشعبہ"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_INSERTCOL\n"
"SID_FM_CURRENCYFIELD\n"
"menuitem.text"
msgid "Currency Field"
msgstr "کرنسی ہیوند  حصئ"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_INSERTCOL\n"
"SID_FM_PATTERNFIELD\n"
"menuitem.text"
msgid "Pattern Field"
msgstr "نمونئ شعبہ"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_INSERTCOL\n"
"SID_FM_FORMATTEDFIELD\n"
"menuitem.text"
msgid "Formatted Field"
msgstr "شعبئ وضع شدہ"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_INSERTCOL\n"
"SID_FM_TWOFIELDS_DATE_N_TIME\n"
"menuitem.text"
msgid "Date and Time Field"
msgstr "تئریخ تئ دوہ شعبہ"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS\n"
"SID_FM_INSERTCOL\n"
"menuitem.text"
msgid "Insert ~Column"
msgstr "کالم كریو  دئخل"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS\n"
"SID_FM_CHANGECOL\n"
"menuitem.text"
msgid "~Replace with"
msgstr "۔سعتھ بدلئویو"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS\n"
"SID_FM_DELETECOL\n"
"menuitem.text"
msgid "Delete Column"
msgstr "کالم كریو خئرج"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS\n"
"SID_FM_HIDECOL\n"
"menuitem.text"
msgid "~Hide Column"
msgstr "۔کالم  تھٲویو ژور۪ی"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_SHOWCOLS\n"
"SID_FM_SHOWCOLS_MORE\n"
"menuitem.text"
msgid "~More..."
msgstr "۔مزید"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS.SID_FM_SHOWCOLS\n"
"SID_FM_SHOWALLCOLS\n"
"menuitem.text"
msgid "~All"
msgstr "۔سئری"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS\n"
"SID_FM_SHOWCOLS\n"
"menuitem.text"
msgid "~Show Columns"
msgstr "۔کالمزہئویو"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_COLS\n"
"SID_FM_SHOW_PROPERTY_BROWSER\n"
"menuitem.text"
msgid "Column..."
msgstr "کالم۔۔۔"

#: gridctrl.src
msgctxt ""
"gridctrl.src\n"
"RID_SVXMNU_CELL\n"
"SID_COPY\n"
"menuitem.text"
msgid "~Copy"
msgstr ""

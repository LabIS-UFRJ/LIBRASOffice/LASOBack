#. extracted from dbaccess/source/ui/dlg
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-08-25 12:34+0200\n"
"PO-Revision-Date: 2016-07-05 01:02+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ks\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1467680578.000000\n"

#: AutoControls.src
msgctxt ""
"AutoControls.src\n"
"STR_DBASE_PATH_OR_FILE\n"
"string.text"
msgid "Path to the dBASE files"
msgstr "dBASE فائلس پاتھ "

#: AutoControls.src
msgctxt ""
"AutoControls.src\n"
"STR_FLAT_PATH_OR_FILE\n"
"string.text"
msgid "Path to the text files"
msgstr "مواد  فائلن ہ۪یوند  پاتھ"

#: AutoControls.src
msgctxt ""
"AutoControls.src\n"
"STR_CALC_PATH_OR_FILE\n"
"string.text"
msgid "Path to the spreadsheet document"
msgstr " سپریڈ شیٹ دستاویزك  پاتھ"

#: AutoControls.src
msgctxt ""
"AutoControls.src\n"
"STR_NAME_OF_ODBC_DATASOURCE\n"
"string.text"
msgid "Name of the ODBC data source on your system"
msgstr "   ڈاٹاسورسك  نام ODBC  تہندس سسٹمس پ۪یٹھ"

#: AutoControls.src
msgctxt ""
"AutoControls.src\n"
"STR_MYSQL_DATABASE_NAME\n"
"string.text"
msgid "Name of the MySQL database"
msgstr " MySQL ڈاٹابیسك ناو"

#: AutoControls.src
msgctxt ""
"AutoControls.src\n"
"STR_ORACLE_DATABASE_NAME\n"
"string.text"
msgid "Name of the Oracle database"
msgstr "ا وریکل ڈاٹابیسك ناو"

#: AutoControls.src
msgctxt ""
"AutoControls.src\n"
"STR_MSACCESS_MDB_FILE\n"
"string.text"
msgid "Microsoft Access database file"
msgstr "مائکروسوفٹ  ایکس   ڈاٹابیس فائل"

#: AutoControls.src
msgctxt ""
"AutoControls.src\n"
"STR_NO_ADDITIONAL_SETTINGS\n"
"string.text"
msgid "No more settings are necessary. To verify that the connection is working, click the '%test' button."
msgstr "زیادہ سیٹینگس چھنئ ضُرت۔ویریفائے کرو   رئبطئ چھا چلان'%test' بٹن۔"

#: AutoControls.src
msgctxt ""
"AutoControls.src\n"
"STR_COMMONURL\n"
"string.text"
msgid "Datasource URL (e.g. postgresql://host:port/database)"
msgstr ""

#: AutoControls.src
msgctxt ""
"AutoControls.src\n"
"STR_HOSTNAME\n"
"string.text"
msgid "~Host name"
msgstr "~ہوسٹ ناو "

#: AutoControls.src
msgctxt ""
"AutoControls.src\n"
"STR_MOZILLA_PROFILE_NAME\n"
"string.text"
msgid "~Mozilla profile name"
msgstr "موزیلا  پروفائل ناو "

#: AutoControls.src
msgctxt ""
"AutoControls.src\n"
"STR_THUNDERBIRD_PROFILE_NAME\n"
"string.text"
msgid "~Thunderbird profile name"
msgstr "تھنڈ ر بڈ  پروفائل ناو"

#: AutoControls.src
msgctxt ""
"AutoControls.src\n"
"STR_ADD_TABLES\n"
"string.text"
msgid "Add Tables"
msgstr ""

#: AutoControls.src
msgctxt ""
"AutoControls.src\n"
"STR_ADD_TABLE_OR_QUERY\n"
"string.text"
msgid "Add Table or Query"
msgstr ""

#: ConnectionPage.src
msgctxt ""
"ConnectionPage.src\n"
"STR_CONNECTION_TEST\n"
"string.text"
msgid "Connection Test"
msgstr "تعلقات  ٹیسٹ  "

#: ConnectionPage.src
msgctxt ""
"ConnectionPage.src\n"
"STR_CONNECTION_SUCCESS\n"
"string.text"
msgid "The connection was established successfully."
msgstr "رئبطئ رود  کامیاب  ۔"

#: ConnectionPage.src
msgctxt ""
"ConnectionPage.src\n"
"STR_CONNECTION_NO_SUCCESS\n"
"string.text"
msgid "The connection could not be established."
msgstr "رئبطئ گئو ناكام"

#: ConnectionPage.src
msgctxt ""
"ConnectionPage.src\n"
"STR_JDBCDRIVER_SUCCESS\n"
"string.text"
msgid "The JDBC driver was loaded successfully."
msgstr " ڈرائیور گئو  کامیئبی سان لوڈ JDBC "

#: ConnectionPage.src
msgctxt ""
"ConnectionPage.src\n"
"STR_JDBCDRIVER_NO_SUCCESS\n"
"string.text"
msgid "The JDBC driver could not be loaded."
msgstr "ڈرائیور ہ۪یوك نئ لوڈ  كریتھ۔JDBC "

#: ConnectionPage.src
msgctxt ""
"ConnectionPage.src\n"
"STR_MSACCESS_FILTERNAME\n"
"string.text"
msgid "MS Access file"
msgstr "ایكسیس فائلMS "

#: ConnectionPage.src
msgctxt ""
"ConnectionPage.src\n"
"STR_MSACCESS_2007_FILTERNAME\n"
"string.text"
msgid "MS Access 2007 file"
msgstr ""

#: ConnectionPage.src
msgctxt ""
"ConnectionPage.src\n"
"STR_FIREBIRD_FILTERNAME\n"
"string.text"
msgid "Firebird Database"
msgstr ""

#: UserAdmin.src
msgctxt ""
"UserAdmin.src\n"
"STR_QUERY_USERADMIN_DELETE_USER\n"
"string.text"
msgid "Do you really want to delete the user?"
msgstr "تہہ چھئو یژھان استیمال كرن وۄل خئرج كرُن؟"

#: UserAdmin.src
msgctxt ""
"UserAdmin.src\n"
"STR_USERADMIN_NOT_AVAILABLE\n"
"string.text"
msgid "The database does not support user administration."
msgstr "ڈاٹابیس  چُھنئ استیمال كرن وئلس تعاوُن كران۔"

#: UserAdmin.src
msgctxt ""
"UserAdmin.src\n"
"STR_ERROR_PASSWORDS_NOT_IDENTICAL\n"
"string.text"
msgid "The passwords do not match. Please enter the password again."
msgstr "خفیہ لفظ جھُنئ رلان۔ برائے مہربانی خفیہ لفظ كریو دوبارئ دئخل  ۔"

#: dbadmin.src
msgctxt ""
"dbadmin.src\n"
"STR_DATABASE_TYPE_CHANGE\n"
"string.text"
msgid "Database properties"
msgstr "ڈاٹا بیس   پروپرٹیس"

#: dbadmin.src
#, fuzzy
msgctxt ""
"dbadmin.src\n"
"STR_PARENTTITLE_GENERAL\n"
"string.text"
msgid "Data Source Properties: #"
msgstr "ڈاٹاسورس پروپرٹیس: # "

#: dbadmin.src
msgctxt ""
"dbadmin.src\n"
"STR_ERR_USE_CONNECT_TO\n"
"string.text"
msgid "Please choose 'Connect to an existing database' to connect to an existing database instead."
msgstr ""

#: dbadmin.src
#, fuzzy
msgctxt ""
"dbadmin.src\n"
"STR_COULD_NOT_LOAD_ODBC_LIB\n"
"string.text"
msgid "Could not load the program library #lib# or it is corrupted. The ODBC data source selection is not available."
msgstr "پروگرام   فائل  ہیوك نئ  لوڈ  كریتھ #lib# کرپٹیٹ چھئ یا ڈاٹاسورس   سلیکشن  چھُنئ موجود ODBC "

#: dbadmin.src
#, fuzzy
msgctxt ""
"dbadmin.src\n"
"STR_UNSUPPORTED_DATASOURCE_TYPE\n"
"string.text"
msgid ""
"This kind of data source is not supported on this platform.\n"
"You are allowed to change the settings, but you probably will not be able to connect to the database."
msgstr ""
"  یتھ پلیٹ   فورمس پیٹھ چھُنئ معاون ڈاٹا سورسس      \n"
"تہہ ہیكیو سیٹینگس بدلئیویتھ مگر تہہ نئ شاید رئبطئ كریتھ۔ ڈاٹا بیسس سعتھ"

#: dbadmin.src
msgctxt ""
"dbadmin.src\n"
"STR_AUTOTEXT_FIELD_SEP_NONE\n"
"string.text"
msgid "{None}"
msgstr "كہین نئ"

#. EM Dec 2002: 'Space' refers to what you get when you hit the space bar on your keyboard.
#: dbadmin.src
msgctxt ""
"dbadmin.src\n"
"STR_AUTOFIELDSEPARATORLIST\n"
"string.text"
msgid ";\t59\t,\t44\t:\t58\t{Tab}\t9\t{Space}\t32"
msgstr ""

#: dbadmin.src
msgctxt ""
"dbadmin.src\n"
"STR_AUTODELIMITER_MISSING\n"
"string.text"
msgid "#1 must be set."
msgstr "#1 پزئ سیٹ كرُن"

#: dbadmin.src
msgctxt ""
"dbadmin.src\n"
"STR_AUTODELIMITER_MUST_DIFFER\n"
"string.text"
msgid "#1 and #2 must be different."
msgstr "#1 and #2   گژھن مختلف آسن"

#: dbadmin.src
msgctxt ""
"dbadmin.src\n"
"STR_AUTONO_WILDCARDS\n"
"string.text"
msgid "Wildcards such as ?,* are not allowed in #1."
msgstr " وائلڈکارڈس کی مثلند ?,* چُھنئ   اجازت ن #1"

#: dbadmin2.src
msgctxt ""
"dbadmin2.src\n"
"STR_ENTER_CONNECTION_PASSWORD\n"
"string.text"
msgid "A password is needed to connect to the data source \"$name$\"."
msgstr "خفیہ لفظ چھئ  ڈ اٹاسورس س سعتھ زریی بنئاونس ضُرت  \"$name$\"."

#: dbadmin2.src
msgctxt ""
"dbadmin2.src\n"
"STR_ASK_FOR_DIRECTORY_CREATION\n"
"string.text"
msgid ""
"The directory\n"
"\n"
"$path$\n"
"\n"
"does not exist. Should it be created?"
msgstr ""
"ڈائریکٹری\n"
"\n"
"$path$\n"
"\n"
"چھنئ موجود٫ تہہ چھئی یژھان بنئاون؟"

#: dbadmin2.src
msgctxt ""
"dbadmin2.src\n"
"STR_COULD_NOT_CREATE_DIRECTORY\n"
"string.text"
msgid "The directory $name$ could not be created."
msgstr "ڈایریكٹری $name$ ہیچ نئ بنئاویتھ"

#: dbadmin2.src
msgctxt ""
"dbadmin2.src\n"
"STR_ALREADYEXISTOVERWRITE\n"
"string.text"
msgid "The file already exists. Overwrite?"
msgstr "فائلچھئ گوڈئی موجود ۔اوررائٹ؟"

#: dbadmin2.src
msgctxt ""
"dbadmin2.src\n"
"STR_NEW_FOLDER\n"
"string.text"
msgid "Folder"
msgstr "فولڈ ر "

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_DBWIZARDTITLE\n"
"string.text"
msgid "Database Wizard"
msgstr "ڈاٹا بیس ویزاڈ"

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_PAGETITLE_INTROPAGE\n"
"string.text"
msgid "Select database"
msgstr "سلیکٹ ڈاٹا بیس "

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_PAGETITLE_DBASE\n"
"string.text"
msgid "Set up dBASE connection"
msgstr "سیٹ اپ BASE  زریی"

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_PAGETITLE_TEXT\n"
"string.text"
msgid "Set up a connection to text files"
msgstr "  ٹیکسٹ فائلن  سعتھ كریو ریبطئ سیٹ اپ"

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_PAGETITLE_MSACCESS\n"
"string.text"
msgid "Set up Microsoft Access connection"
msgstr "مائکروسوفٹ ایکسیس زریی كریو سیٹ اپ "

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_PAGETITLE_LDAP\n"
"string.text"
msgid "Set up LDAP connection"
msgstr "سیٹ اپLDAP زریی"

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_PAGETITLE_ADO\n"
"string.text"
msgid "Set up ADO connection"
msgstr "سیٹ اپ ADO زریی"

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_PAGETITLE_JDBC\n"
"string.text"
msgid "Set up JDBC connection"
msgstr "سیٹ اپ JDBزریی"

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_PAGETITLE_ORACLE\n"
"string.text"
msgid "Set up Oracle database connection"
msgstr "  اوریکل ڈاٹا  بیس زریی كریو   سیٹ اپ  "

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_PAGETITLE_MYSQL\n"
"string.text"
msgid "Set up MySQL connection"
msgstr "سیٹ اپ MySQLزریی"

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_PAGETITLE_ODBC\n"
"string.text"
msgid "Set up ODBC connection"
msgstr "سیٹ اپ ODBC زریی"

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_PAGETITLE_SPREADSHEET\n"
"string.text"
msgid "Set up Spreadsheet connection"
msgstr "سپریڈ شیٹ  زریی كریوسیٹ اپ "

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_PAGETITLE_AUTHENTIFICATION\n"
"string.text"
msgid "Set up user authentication"
msgstr "   استیمال کرن وۄل سیٹ اپ کریو تصدیق"

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_PAGETITLE_MYSQL_NATIVE\n"
"string.text"
msgid "Set up MySQL server data"
msgstr ""

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_PAGETITLE_FINAL\n"
"string.text"
msgid "Save and proceed"
msgstr "محفوظ  كریو تئ بروٹھ پكیو"

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_DATABASEDEFAULTNAME\n"
"string.text"
msgid "New Database"
msgstr "نئو ڈاٹا بیس"

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_MYSQLJDBC_HEADERTEXT\n"
"string.text"
msgid "Set up connection to a MySQL database using JDBC"
msgstr "JDBC  استیمال كریتھ كریو MySQL سعتھ رئبطئ سیٹ اپ"

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_MYSQLJDBC_HELPTEXT\n"
"string.text"
msgid ""
"Please enter the required information to connect to a MySQL database using JDBC. Note that a JDBC driver class must be installed on your system and registered with %PRODUCTNAME.\n"
"Please contact your system administrator if you are unsure about the following settings."
msgstr ""

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_MYSQL_DRIVERCLASSTEXT\n"
"string.text"
msgid "MySQL JDBC d~river class:"
msgstr "MySQL JDBC dریور کلاس: "

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_MYSQL_DEFAULT\n"
"string.text"
msgid "Default: 3306"
msgstr "ڈی فالٹ: 3306"

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_DBASE_HEADERTEXT\n"
"string.text"
msgid "Set up a connection to dBASE files"
msgstr " dBASE  فایلن سعتھ كریو رئبطئ قئیم"

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_DBASE_HELPTEXT\n"
"string.text"
msgid "Select the folder where the dBASE files are stored."
msgstr "فولڈر ژئریو یتیتھdBASE   فایلس چھئ موجود۔"

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_TEXT_HEADERTEXT\n"
"string.text"
msgid "Set up a connection to text files"
msgstr "  ٹیکسٹ فائلن  سعتھ كریو ریبطئ سیٹ اپ"

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_TEXT_HELPTEXT\n"
"string.text"
msgid "Select the folder where the CSV (Comma Separated Values) text files are stored. %PRODUCTNAME Base will open these files in read-only mode."
msgstr ""

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_TEXT_PATH_OR_FILE\n"
"string.text"
msgid "Path to text files"
msgstr "پاتھ ٹیکس فائلس "

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_MSACCESS_HEADERTEXT\n"
"string.text"
msgid "Set up a connection to a Microsoft Access database"
msgstr "  مائکرو سوفٹ ایکسیس ڈاٹا بیسس منز كریو  رئبطئ سیٹ اپ"

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_MSACCESS_HELPTEXT\n"
"string.text"
msgid "Please select the Microsoft Access file you want to access."
msgstr "برائےمہربانی مائکروسوفٹ ایکسیس فائل  ژئریو یس تُہ یژھان چھئو ۔"

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_ADO_HEADERTEXT\n"
"string.text"
msgid "Set up a connection to an ADO database"
msgstr "  ADO ڈاٹابیس كریو رئبطئ سیٹ اپ"

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_ADO_HELPTEXT\n"
"string.text"
msgid ""
"Please enter the URL of the ADO data source you want to connect to.\n"
"Click 'Browse' to configure provider-specific settings.\n"
"Please contact your system administrator if you are unsure about the following settings."
msgstr ""

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_ODBC_HEADERTEXT\n"
"string.text"
msgid "Set up a connection to an ODBC database"
msgstr " ODBC ڈاٹابیسس سعتھ كریو رئبطئ سیٹ اپ "

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_ODBC_HELPTEXT\n"
"string.text"
msgid ""
"Enter the name of the ODBC database you want to connect to.\n"
"Click 'Browse...' to select an ODBC database that is already registered in %PRODUCTNAME.\n"
"Please contact your system administrator if you are unsure about the following settings."
msgstr ""

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_JDBC_HEADERTEXT\n"
"string.text"
msgid "Set up a connection to a JDBC database"
msgstr " JDBCڈاٹابیسس سعتھ كریو رئبطئ سیٹ اپ"

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_JDBC_HELPTEXT\n"
"string.text"
msgid ""
"Please enter the required information to connect to a JDBC database.\n"
"Please contact your system administrator if you are unsure about the following settings."
msgstr ""

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_ORACLE_HEADERTEXT\n"
"string.text"
msgid "Set up a connection to an Oracle database"
msgstr "اوریکل ڈاٹابیس سعتھ كریو رئبطئ سیٹ  اپ  "

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_ORACLE_DEFAULT\n"
"string.text"
msgid "Default: 1521"
msgstr "ڈی فالٹ: 1521"

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_ORACLE_DRIVERCLASSTEXT\n"
"string.text"
msgid "Oracle JDBC ~driver class"
msgstr "اوریکل JDBC ڈرائیور کلاس"

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_ORACLE_HELPTEXT\n"
"string.text"
msgid ""
"Please enter the required information to connect to an Oracle database. Note that a JDBC Driver Class must be installed on your system and registered with %PRODUCTNAME.\n"
"Please contact your system administrator if you are unsure about the following settings."
msgstr ""

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_SPREADSHEET_HEADERTEXT\n"
"string.text"
msgid "Set up a connection to spreadsheets"
msgstr "سپریڈشیٹس سعتھ كریو رئبطئ سیٹ اپ"

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_SPREADSHEET_HELPTEXT\n"
"string.text"
msgid ""
"Click 'Browse...' to select a %PRODUCTNAME spreadsheet or Microsoft Excel workbook.\n"
"%PRODUCTNAME will open this file in read-only mode."
msgstr ""

#: dbadminsetup.src
msgctxt ""
"dbadminsetup.src\n"
"STR_SPREADSHEETPATH\n"
"string.text"
msgid "~Location and file name"
msgstr "چاے تئ  فائل ناو"

#: directsql.src
msgctxt ""
"directsql.src\n"
"STR_COMMAND_EXECUTED_SUCCESSFULLY\n"
"string.text"
msgid "Command successfully executed."
msgstr "كماںڈ آی انجام آسئنی سان     ۔ "

#: directsql.src
msgctxt ""
"directsql.src\n"
"STR_DIRECTSQL_CONNECTIONLOST\n"
"string.text"
msgid "The connection to the database has been lost. This dialog will be closed."
msgstr "ڈاٹابیسس سعتھ ژھیون رئبطئ   ۔یہ كتھ باتھ گژھ۪ی بند  ۔"

#: indexdialog.src
msgctxt ""
"indexdialog.src\n"
"STR_TAB_INDEX_SORTORDER\n"
"string.text"
msgid "Sort order"
msgstr "سورٹ  آرڈر"

#: indexdialog.src
msgctxt ""
"indexdialog.src\n"
"STR_TAB_INDEX_FIELD\n"
"string.text"
msgid "Index field"
msgstr "  فہرست شبعہ"

#: indexdialog.src
msgctxt ""
"indexdialog.src\n"
"STR_ORDER_ASCENDING\n"
"string.text"
msgid "Ascending"
msgstr "بوئنئ پیٹھئ ہیور تام"

#: indexdialog.src
msgctxt ""
"indexdialog.src\n"
"STR_ORDER_DESCENDING\n"
"string.text"
msgid "Descending"
msgstr "بۄن كُن ترتیب دیون"

#: indexdialog.src
msgctxt ""
"indexdialog.src\n"
"STR_CONFIRM_DROP_INDEX\n"
"string.text"
msgid "Do you really want to delete the index '$name$'?"
msgstr " تہہ چھئو  فہرست '$name$'  یژھان خئرج كرن ؟  "

#: indexdialog.src
msgctxt ""
"indexdialog.src\n"
"STR_LOGICAL_INDEX_NAME\n"
"string.text"
msgid "index"
msgstr "فہرست"

#: indexdialog.src
msgctxt ""
"indexdialog.src\n"
"STR_NEED_INDEX_FIELDS\n"
"string.text"
msgid "The index must contain at least one field."
msgstr "فہرستس منز گژھ۪ی آسُن كم از كم  اكھ     شبعہ     "

#: indexdialog.src
msgctxt ""
"indexdialog.src\n"
"STR_INDEX_NAME_ALREADY_USED\n"
"string.text"
msgid "There is already another index named \"$name$\"."
msgstr "یم ناوك فہرست چھئ گُڈٲی موجود۔\"$name$\""

#: indexdialog.src
msgctxt ""
"indexdialog.src\n"
"STR_INDEXDESIGN_DOUBLE_COLUMN_NAME\n"
"string.text"
msgid "In an index definition, no table column may occur more than once. However, you have entered column \"$name$\" twice."
msgstr "فہرستچئ خلاصس منز   ، كہین جدول  کالم ہیك۪ی نئ یتھ اكئ ہوتئ زیادو لٹ۪ی۔۔البتئ تہہ چھئو كرمُت  کالم  دئخل دوی  پھیرئ ,\"$name$\" "

#: paramdialog.src
msgctxt ""
"paramdialog.src\n"
"STR_COULD_NOT_CONVERT_PARAM\n"
"string.text"
msgid "The entry could not be converted to a valid value for the \"$name$\" parameter"
msgstr ""

#: sqlmessage.src
msgctxt ""
"sqlmessage.src\n"
"STR_EXCEPTION_STATUS\n"
"string.text"
msgid "SQL Status"
msgstr ""

#: sqlmessage.src
msgctxt ""
"sqlmessage.src\n"
"STR_EXCEPTION_ERRORCODE\n"
"string.text"
msgid "Error code"
msgstr ""

#: sqlmessage.src
msgctxt ""
"sqlmessage.src\n"
"STR_EXPLAN_STRINGCONVERSION_ERROR\n"
"string.text"
msgid "A frequent reason for this error is an inappropriate character set setting for the language of your database. Check the setting by choosing Edit - Database - Properties."
msgstr "اكھ  وجئ یمئ غلطی ہ۪یوند چھُ حرف سیٹ اپ سیٹینگس یم زبان خعطرئ چھئ تہندئ ڈاٹا بیث منز ۔ ثیٹنگس كریو  جانچ ایڈیٹس منز گژھیتھ۔ ڈاٹابیس پراپرٹیز۔"

#: sqlmessage.src
msgctxt ""
"sqlmessage.src\n"
"STR_EXCEPTION_ERROR\n"
"string.text"
msgid "Error"
msgstr "غلطی"

#: sqlmessage.src
msgctxt ""
"sqlmessage.src\n"
"STR_EXCEPTION_WARNING\n"
"string.text"
msgid "Warning"
msgstr "آگہی "

#: sqlmessage.src
msgctxt ""
"sqlmessage.src\n"
"STR_EXCEPTION_INFO\n"
"string.text"
msgid "Information"
msgstr "مولومات"

#: sqlmessage.src
msgctxt ""
"sqlmessage.src\n"
"STR_EXCEPTION_DETAILS\n"
"string.text"
msgid "Details"
msgstr "تفصیل"

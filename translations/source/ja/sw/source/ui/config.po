#. extracted from sw/source/ui/config
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-03-09 20:49+0100\n"
"PO-Revision-Date: 2016-12-11 01:34+0000\n"
"Last-Translator: Naruhiko Ogasawara <naruoga@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1481420072.000000\n"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"ST_SCRIPT_ASIAN\n"
"string.text"
msgid "Asian"
msgstr "アジア諸言語"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"ST_SCRIPT_CTL\n"
"string.text"
msgid "CTL"
msgstr "複合文字言語 (CTL)"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"ST_SCRIPT_WESTERN\n"
"string.text"
msgid "Western"
msgstr "西洋諸言語"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PRODUCTNAME\n"
"string.text"
msgid "%PRODUCTNAME %s"
msgstr "%PRODUCTNAME %s"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_CONTENTS\n"
"string.text"
msgid "Contents"
msgstr "内容"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PAGE_BACKGROUND\n"
"string.text"
msgid "Page ba~ckground"
msgstr "ページの背景(~C)"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PICTURES\n"
"string.text"
msgid "P~ictures and other graphic objects"
msgstr "画像やグラフィックオブジェクト(~I)"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_HIDDEN\n"
"string.text"
msgid "Hidden te~xt"
msgstr "隠しテキスト(~X)"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_TEXT_PLACEHOLDERS\n"
"string.text"
msgid "~Text placeholders"
msgstr "テキストのプレースホルダー(~T)"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_FORM_CONTROLS\n"
"string.text"
msgid "Form control~s"
msgstr "フォームコントロール(~S)"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_COLOR\n"
"string.text"
msgid "Color"
msgstr "色"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PRINT_BLACK\n"
"string.text"
msgid "Print text in blac~k"
msgstr "文字を黒で印刷(~K)"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PAGES_TEXT\n"
"string.text"
msgid "Pages"
msgstr "ページ"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PRINT_BLANK\n"
"string.text"
msgid "Print ~automatically inserted blank pages"
msgstr "自動的に挿入された空白ページを印刷(~A)"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_ONLY_PAPER\n"
"string.text"
msgid "~Use only paper tray from printer preferences"
msgstr "プリンター設定の給紙トレイのみ使用(~U)"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PRINT\n"
"string.text"
msgid "Print"
msgstr "印刷"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_NONE\n"
"string.text"
msgid "None (document only)"
msgstr "なし (ドキュメントのみ)"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_COMMENTS_ONLY\n"
"string.text"
msgid "Comments only"
msgstr "コメントのみ"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PLACE_END\n"
"string.text"
msgid "Place at end of document"
msgstr "ドキュメントの末尾に置く"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PLACE_PAGE\n"
"string.text"
msgid "Place at end of page"
msgstr "ページの末尾に置く"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_COMMENTS\n"
"string.text"
msgid "~Comments"
msgstr "コメント(~C)"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PAGE_SIDES\n"
"string.text"
msgid "Page sides"
msgstr "ページの左右"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_ALL_PAGES\n"
"string.text"
msgid "All pages"
msgstr "すべてのページ"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_BACK_PAGES\n"
"string.text"
msgid "Back sides / left pages"
msgstr "裏面/左ページ"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_FONT_PAGES\n"
"string.text"
msgid "Front sides / right pages"
msgstr "表面/右ページ"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_INCLUDE\n"
"string.text"
msgid "Include"
msgstr "印刷に含める"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_BROCHURE\n"
"string.text"
msgid "Broch~ure"
msgstr "パンフレット(~U)"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_LEFT_SCRIPT\n"
"string.text"
msgid "Left-to-right script"
msgstr "左から右へ （横書き）"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_RIGHT_SCRIPT\n"
"string.text"
msgid "Right-to-left script"
msgstr "右から左へ （縦書き）"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_RANGE_COPIES\n"
"string.text"
msgid "Range and copies"
msgstr "範囲と部数"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_ALLPAGES\n"
"string.text"
msgid "~All pages"
msgstr "すべてのページ(~A)"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_SOMEPAGES\n"
"string.text"
msgid "Pa~ges"
msgstr "指定ページ(~G)"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_SELECTION\n"
"string.text"
msgid "~Selection"
msgstr "選択した部分(~S)"

#: optdlg.src
msgctxt ""
"optdlg.src\n"
"STR_PRINTOPTUI_PLACE_MARGINS\n"
"string.text"
msgid "Place in margins"
msgstr "マージンに配置"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Millimeter\n"
"itemlist.text"
msgid "Millimeter"
msgstr "ミリメートル"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Centimeter\n"
"itemlist.text"
msgid "Centimeter"
msgstr "センチメートル"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Meter\n"
"itemlist.text"
msgid "Meter"
msgstr "メートル"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Kilometer\n"
"itemlist.text"
msgid "Kilometer"
msgstr "キロメートル"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Inch\n"
"itemlist.text"
msgid "Inch"
msgstr "インチ"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Foot\n"
"itemlist.text"
msgid "Foot"
msgstr "フィート"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Miles\n"
"itemlist.text"
msgid "Miles"
msgstr "マイル"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Pica\n"
"itemlist.text"
msgid "Pica"
msgstr "パイカ"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Point\n"
"itemlist.text"
msgid "Point"
msgstr "ポイント"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Char\n"
"itemlist.text"
msgid "Char"
msgstr "文字"

#: optload.src
msgctxt ""
"optload.src\n"
"STR_ARR_METRIC\n"
"Line\n"
"itemlist.text"
msgid "Line"
msgstr "行"

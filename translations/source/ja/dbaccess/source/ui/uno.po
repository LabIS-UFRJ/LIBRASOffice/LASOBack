#. extracted from dbaccess/source/ui/uno
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-11-20 13:02+0100\n"
"PO-Revision-Date: 2012-07-02 15:46+0200\n"
"Last-Translator: Ikuya <ikuya@fruitsbasket.info>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"

#: copytablewizard.src
msgctxt ""
"copytablewizard.src\n"
"STR_CTW_NO_VIEWS_SUPPORT\n"
"string.text"
msgid "The destination database does not support views."
msgstr "宛先データベースはビューをサポートしていません。"

#: copytablewizard.src
msgctxt ""
"copytablewizard.src\n"
"STR_CTW_NO_PRIMARY_KEY_SUPPORT\n"
"string.text"
msgid "The destination database does not support primary keys."
msgstr "宛先データベースはプライマリキーをサポートしていません。"

#: copytablewizard.src
msgctxt ""
"copytablewizard.src\n"
"STR_CTW_INVALID_DATA_ACCESS_DESCRIPTOR\n"
"string.text"
msgid "no data access descriptor found, or no data access descriptor able to provide all necessary information"
msgstr "データアクセス記述子が見つからないか、必要な情報をすべて提供できるデータアクセス記述子がありません"

#: copytablewizard.src
msgctxt ""
"copytablewizard.src\n"
"STR_CTW_ONLY_TABLES_AND_QUERIES_SUPPORT\n"
"string.text"
msgid "Only tables and queries are supported at the moment."
msgstr "現在サポートされているのは、テーブルとクエリーだけです。"

#: copytablewizard.src
msgctxt ""
"copytablewizard.src\n"
"STR_CTW_COPY_SOURCE_NEEDS_BOOKMARKS\n"
"string.text"
msgid "The copy source's result set must support bookmarks."
msgstr "コピー元の結果セットがブックマークをサポートしている必要があります。"

#: copytablewizard.src
msgctxt ""
"copytablewizard.src\n"
"STR_CTW_UNSUPPORTED_COLUMN_TYPE\n"
"string.text"
msgid "Unsupported source column type ($type$) at column position $pos$."
msgstr "列位置 $pos$ のソース列の種類 ($type$) はサポートされていません。"

#: copytablewizard.src
msgctxt ""
"copytablewizard.src\n"
"STR_CTW_ILLEGAL_PARAMETER_COUNT\n"
"string.text"
msgid "Illegal number of initialization parameters."
msgstr "初期化パラメーターの数が正しくありません。"

#: copytablewizard.src
msgctxt ""
"copytablewizard.src\n"
"STR_CTW_ERROR_DURING_INITIALIZATION\n"
"string.text"
msgid "An error occurred during initialization."
msgstr "初期化中にエラーが発生しました。"

#: copytablewizard.src
msgctxt ""
"copytablewizard.src\n"
"STR_CTW_ERROR_UNSUPPORTED_SETTING\n"
"string.text"
msgid "Unsupported setting in the copy source descriptor: $name$."
msgstr "コピー元の記述子にサポートされていない設定があります: $name$"

#: copytablewizard.src
msgctxt ""
"copytablewizard.src\n"
"STR_CTW_ERROR_NO_QUERY\n"
"string.text"
msgid "To copy a query, your connection must be able to provide queries."
msgstr "クエリーをコピーするには、接続がクエリーを提供できるようになっている必要があります。"

#: copytablewizard.src
msgctxt ""
"copytablewizard.src\n"
"STR_CTW_ERROR_INVALID_INTERACTIONHANDLER\n"
"string.text"
msgid "The given interaction handler is invalid."
msgstr "指定されたインタラクションハンドラーは無効です。"

#: dbinteraction.src
msgctxt ""
"dbinteraction.src\n"
"STR_REMEMBERPASSWORD_SESSION\n"
"string.text"
msgid "~Remember password until end of session"
msgstr "セッション終了時までパスワードを記憶(~R)"

#: dbinteraction.src
msgctxt ""
"dbinteraction.src\n"
"STR_REMEMBERPASSWORD_PERSISTENT\n"
"string.text"
msgid "~Remember password"
msgstr "パスワードを記憶(~R)"

#. extracted from sd/source/ui/view
msgid ""
msgstr ""
"Project-Id-Version: view\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-04-16 21:40+0200\n"
"PO-Revision-Date: 2016-10-15 16:29+0000\n"
"Last-Translator: bormant <bormant@mail.ru>\n"
"Language-Team: Russian <l10n@ru.libreoffice.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.7\n"
"X-POOTLE-MTIME: 1476548978.000000\n"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS\n"
"STR_IMPRESS_PRINT_UI_GROUP_NAME\n"
"string.text"
msgid "%PRODUCTNAME %s"
msgstr "%PRODUCTNAME %s"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS\n"
"STR_IMPRESS_PRINT_UI_PRINT_CONTENT\n"
"string.text"
msgid "Print content"
msgstr "Что печатать"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS\n"
"STR_IMPRESS_PRINT_UI_PRINT_GROUP\n"
"string.text"
msgid "Print"
msgstr "Печать"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS\n"
"STR_IMPRESS_PRINT_UI_CONTENT\n"
"string.text"
msgid "Document"
msgstr "Документ"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_CONTENT_CHOICES\n"
"Slides\n"
"itemlist.text"
msgid "Slides"
msgstr "Слайды"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_CONTENT_CHOICES\n"
"Handouts\n"
"itemlist.text"
msgid "Handouts"
msgstr "Тезисы"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_CONTENT_CHOICES\n"
"Notes\n"
"itemlist.text"
msgid "Notes"
msgstr "Примечания"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_CONTENT_CHOICES\n"
"Outline\n"
"itemlist.text"
msgid "Outline"
msgstr "Структура"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS\n"
"STR_IMPRESS_PRINT_UI_SLIDESPERPAGE\n"
"string.text"
msgid "Slides per page"
msgstr "Слайдов на страницу"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_SLIDESPERPAGE_CHOICES\n"
"According to layout\n"
"itemlist.text"
msgid "According to layout"
msgstr "По умолчанию"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_SLIDESPERPAGE_CHOICES\n"
"1\n"
"itemlist.text"
msgid "1"
msgstr "1"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_SLIDESPERPAGE_CHOICES\n"
"2\n"
"itemlist.text"
msgid "2"
msgstr "2"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_SLIDESPERPAGE_CHOICES\n"
"3\n"
"itemlist.text"
msgid "3"
msgstr "3"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_SLIDESPERPAGE_CHOICES\n"
"4\n"
"itemlist.text"
msgid "4"
msgstr "4"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_SLIDESPERPAGE_CHOICES\n"
"6\n"
"itemlist.text"
msgid "6"
msgstr "6"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_SLIDESPERPAGE_CHOICES\n"
"9\n"
"itemlist.text"
msgid "9"
msgstr "9"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS\n"
"STR_IMPRESS_PRINT_UI_ORDER\n"
"string.text"
msgid "Order"
msgstr "Порядок"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_ORDER_CHOICES\n"
"Left to right, then down\n"
"itemlist.text"
msgid "Left to right, then down"
msgstr "Слева направо, затем вниз"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_ORDER_CHOICES\n"
"Top to bottom, then right\n"
"itemlist.text"
msgid "Top to bottom, then right"
msgstr "Сверху вниз, затем направо"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS\n"
"STR_IMPRESS_PRINT_UI_INCLUDE_CONTENT\n"
"string.text"
msgid "~Contents"
msgstr "Содержимое"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS\n"
"STR_IMPRESS_PRINT_UI_IS_PRINT_NAME\n"
"string.text"
msgid "~Slide name"
msgstr "Имя слайда"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS\n"
"STR_DRAW_PRINT_UI_IS_PRINT_NAME\n"
"string.text"
msgid "P~age name"
msgstr "Название страницы"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS\n"
"STR_IMPRESS_PRINT_UI_IS_PRINT_DATE\n"
"string.text"
msgid "~Date and time"
msgstr "Дата и время"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS\n"
"STR_IMPRESS_PRINT_UI_IS_PRINT_HIDDEN\n"
"string.text"
msgid "Hidden pages"
msgstr "Скрытые страницы"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS\n"
"STR_IMPRESS_PRINT_UI_OUTPUT_OPTIONS_GROUP\n"
"string.text"
msgid "Output options"
msgstr "Параметры вывода"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS\n"
"STR_IMPRESS_PRINT_UI_QUALITY\n"
"string.text"
msgid "Color"
msgstr "Цвет"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_QUALITY_CHOICES\n"
"Original colors\n"
"itemlist.text"
msgid "Original colors"
msgstr "Исходные цвета"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_QUALITY_CHOICES\n"
"Grayscale\n"
"itemlist.text"
msgid "Grayscale"
msgstr "Оттенки серого"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_QUALITY_CHOICES\n"
"Black & white\n"
"itemlist.text"
msgid "Black & white"
msgstr "Чёрно-белое"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS\n"
"STR_IMPRESS_PRINT_UI_PAGE_OPTIONS\n"
"string.text"
msgid "~Size"
msgstr "Размер"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_PAGE_OPTIONS_CHOICES\n"
"Original size\n"
"itemlist.text"
msgid "Original size"
msgstr "Исходный размер"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_PAGE_OPTIONS_CHOICES\n"
"Fit to printable page\n"
"itemlist.text"
msgid "Fit to printable page"
msgstr "Подогнать по размеру страницы"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_PAGE_OPTIONS_CHOICES\n"
"Distribute on multiple sheets of paper\n"
"itemlist.text"
msgid "Distribute on multiple sheets of paper"
msgstr "Печать плакатом"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_PAGE_OPTIONS_CHOICES\n"
"Tile sheet of paper with repeated slides\n"
"itemlist.text"
msgid "Tile sheet of paper with repeated slides"
msgstr "Печать мозаикой"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_PAGE_OPTIONS_CHOICES_DRAW\n"
"Original size\n"
"itemlist.text"
msgid "Original size"
msgstr "Исходный размер"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_PAGE_OPTIONS_CHOICES_DRAW\n"
"Fit to printable page\n"
"itemlist.text"
msgid "Fit to printable page"
msgstr "Подогнать по размеру страницы"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_PAGE_OPTIONS_CHOICES_DRAW\n"
"Distribute on multiple sheets of paper\n"
"itemlist.text"
msgid "Distribute on multiple sheets of paper"
msgstr "Печать плакатом"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_PAGE_OPTIONS_CHOICES_DRAW\n"
"Tile sheet of paper with repeated pages\n"
"itemlist.text"
msgid "Tile sheet of paper with repeated pages"
msgstr "Печать мозаикой"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS\n"
"STR_IMPRESS_PRINT_UI_BROCHURE\n"
"string.text"
msgid "Brochure"
msgstr "Брошюра"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS\n"
"STR_IMPRESS_PRINT_UI_PAGE_SIDES\n"
"string.text"
msgid "Page sides"
msgstr "Стороны страниц"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS\n"
"STR_IMPRESS_PRINT_UI_BROCHURE_INCLUDE\n"
"string.text"
msgid "Include"
msgstr "Вывести"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_BROCHURE_INCLUDE_LIST\n"
"All pages\n"
"itemlist.text"
msgid "All pages"
msgstr "Все страницы"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_BROCHURE_INCLUDE_LIST\n"
"Front sides / right pages\n"
"itemlist.text"
msgid "Front sides / right pages"
msgstr "Лицевые стороны / правые страницы"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_BROCHURE_INCLUDE_LIST\n"
"Back sides / left pages\n"
"itemlist.text"
msgid "Back sides / left pages"
msgstr "Оборотные стороны / левые страницы"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS\n"
"STR_IMPRESS_PRINT_UI_PAPER_TRAY_GROUP\n"
"string.text"
msgid "Paper tray"
msgstr "Подача бумаги"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS\n"
"STR_IMPRESS_PRINT_UI_PAPER_TRAY\n"
"string.text"
msgid "~Use only paper tray from printer preferences"
msgstr "Подача бумаги согласно настройкам принтера"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS\n"
"STR_IMPRESS_PRINT_UI_PAGE_RANGE\n"
"string.text"
msgid "Print range"
msgstr "Область печати"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_PAGE_RANGE_CHOICE\n"
"~All slides\n"
"itemlist.text"
msgid "~All slides"
msgstr "~Все слайды"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_PAGE_RANGE_CHOICE\n"
"~Slides\n"
"itemlist.text"
msgid "~Slides"
msgstr "Слайды"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_IMPRESS_PRINT_UI_PAGE_RANGE_CHOICE\n"
"Se~lection\n"
"itemlist.text"
msgid "Se~lection"
msgstr "Выделение"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_DRAW_PRINT_UI_PAGE_RANGE_CHOICE\n"
"~All pages\n"
"itemlist.text"
msgid "~All pages"
msgstr "Все страницы"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_DRAW_PRINT_UI_PAGE_RANGE_CHOICE\n"
"Pa~ges\n"
"itemlist.text"
msgid "Pa~ges"
msgstr "Страницы"

#: DocumentRenderer.src
msgctxt ""
"DocumentRenderer.src\n"
"STR_IMPRESS_PRINT_UI_OPTIONS.STR_DRAW_PRINT_UI_PAGE_RANGE_CHOICE\n"
"Se~lection\n"
"itemlist.text"
msgid "Se~lection"
msgstr "Выделение"

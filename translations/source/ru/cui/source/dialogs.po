#. extracted from cui/source/dialogs
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-12-24 14:36+0000\n"
"Last-Translator: bormant <bormant@mail.ru>\n"
"Language-Team: Russian <l10n@ru.libreoffice.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1482590183.000000\n"

#: cuires.src
msgctxt ""
"cuires.src\n"
"RID_SVXSTR_ERR_TEXTNOTFOUND\n"
"string.text"
msgid "No alternatives found."
msgstr "Не найдено альтернатив."

#: cuires.src
msgctxt ""
"cuires.src\n"
"RID_SVXSTR_SELECT_FILE_IFRAME\n"
"string.text"
msgid "Select File for Floating Frame"
msgstr "Выбрать файл для фрейма"

#: cuires.src
msgctxt ""
"cuires.src\n"
"RID_SVXSTR_ALLFUNCTIONS\n"
"string.text"
msgid "All categories"
msgstr "Все категории"

#: cuires.src
msgctxt ""
"cuires.src\n"
"RID_SVXSTR_MYMACROS\n"
"string.text"
msgid "My Macros"
msgstr "Мои макросы"

#: cuires.src
msgctxt ""
"cuires.src\n"
"RID_SVXSTR_PRODMACROS\n"
"string.text"
msgid "%PRODUCTNAME Macros"
msgstr "Макросы %PRODUCTNAME"

#: cuires.src
msgctxt ""
"cuires.src\n"
"RID_SVXSTR_SELECTOR_ADD_COMMANDS\n"
"string.text"
msgid "Add Commands"
msgstr "Добавить команды"

#: cuires.src
msgctxt ""
"cuires.src\n"
"RID_SVXSTR_SELECTOR_RUN\n"
"string.text"
msgid "Run"
msgstr "Выполнить"

#: cuires.src
msgctxt ""
"cuires.src\n"
"RID_SVXSTR_ROW\n"
"string.text"
msgid "Insert Rows"
msgstr "Вставить строки"

#. PPI is pixel per inch, %1 is a number
#: cuires.src
msgctxt ""
"cuires.src\n"
"RID_SVXSTR_PPI\n"
"string.text"
msgid "(%1 PPI)"
msgstr "(%1 PPI)"

#: cuires.src
msgctxt ""
"cuires.src\n"
"RID_SVXSTR_COL\n"
"string.text"
msgid "Insert Columns"
msgstr "Вставить столбцы"

#: cuires.src
msgctxt ""
"cuires.src\n"
"RID_SVXSTR_AUTO_ENTRY\n"
"string.text"
msgid "Automatic"
msgstr "Автоматически"

#: cuires.src
msgctxt ""
"cuires.src\n"
"RID_SVXSTR_EDIT_GRAPHIC\n"
"string.text"
msgid "Link"
msgstr "Ссылка"

#: cuires.src
msgctxt ""
"cuires.src\n"
"RID_SVXSTR_LOADACCELCONFIG\n"
"string.text"
msgid "Load Keyboard Configuration"
msgstr "Загрузить настройки клавиатуры"

#: cuires.src
msgctxt ""
"cuires.src\n"
"RID_SVXSTR_SAVEACCELCONFIG\n"
"string.text"
msgid "Save Keyboard Configuration"
msgstr "Сохранить настройки клавиатуры"

#: cuires.src
msgctxt ""
"cuires.src\n"
"RID_SVXSTR_FILTERNAME_CFG\n"
"string.text"
msgid "Configuration (*.cfg)"
msgstr "Настройки (*.cfg)"

#: cuires.src
msgctxt ""
"cuires.src\n"
"RID_SVXSTR_HYPDLG_ERR_LERR_NOENTRIES\n"
"string.text"
msgid "Targets do not exist in the document."
msgstr "Нет доступных для ссылки элементов в документе."

#: cuires.src
msgctxt ""
"cuires.src\n"
"RID_SVXSTR_HYPDLG_ERR_LERR_DOCNOTOPEN\n"
"string.text"
msgid "Couldn't open the document."
msgstr "Невозможно открыть документ."

#: cuires.src
msgctxt ""
"cuires.src\n"
"RID_SVXSTR_EDITHINT\n"
"string.text"
msgid "[Enter text here]"
msgstr "[Введите текст]"

#: cuires.src
msgctxt ""
"cuires.src\n"
"RID_SVXSTR_HANGUL\n"
"string.text"
msgid "Hangul"
msgstr "Хангыль"

#: cuires.src
msgctxt ""
"cuires.src\n"
"RID_SVXSTR_HANJA\n"
"string.text"
msgid "Hanja"
msgstr "Ханджа"

#: fmsearch.src
msgctxt ""
"fmsearch.src\n"
"RID_STR_SEARCH_ANYWHERE\n"
"string.text"
msgid "anywhere in the field"
msgstr "где-либо в поле"

#: fmsearch.src
msgctxt ""
"fmsearch.src\n"
"RID_STR_SEARCH_BEGINNING\n"
"string.text"
msgid "beginning of field"
msgstr "в начале поля"

#: fmsearch.src
msgctxt ""
"fmsearch.src\n"
"RID_STR_SEARCH_END\n"
"string.text"
msgid "end of field"
msgstr "в конце поля"

#: fmsearch.src
msgctxt ""
"fmsearch.src\n"
"RID_STR_SEARCH_WHOLE\n"
"string.text"
msgid "entire field"
msgstr "все поле"

#: fmsearch.src
msgctxt ""
"fmsearch.src\n"
"RID_STR_FROM_TOP\n"
"string.text"
msgid "From top"
msgstr "Сверху"

#: fmsearch.src
msgctxt ""
"fmsearch.src\n"
"RID_STR_FROM_BOTTOM\n"
"string.text"
msgid "From bottom"
msgstr "Снизу"

#: fmsearch.src
msgctxt ""
"fmsearch.src\n"
"RID_STR_SEARCH_NORECORD\n"
"string.text"
msgid "No records corresponding to your data found."
msgstr "Не найдены записи, соответствующие запросу."

#: fmsearch.src
msgctxt ""
"fmsearch.src\n"
"RID_STR_SEARCH_GENERAL_ERROR\n"
"string.text"
msgid "An unknown error occurred. The search could not be finished."
msgstr "Неизвестная ошибка. Невозможно завершить поиск."

#: fmsearch.src
msgctxt ""
"fmsearch.src\n"
"RID_STR_OVERFLOW_FORWARD\n"
"string.text"
msgid "Overflow, search continued at the beginning"
msgstr "Переполнение, поиск будет продолжен с начала"

#: fmsearch.src
msgctxt ""
"fmsearch.src\n"
"RID_STR_OVERFLOW_BACKWARD\n"
"string.text"
msgid "Overflow, search continued at the end"
msgstr "Переполнение, поиск будет продолжен с конца"

#: fmsearch.src
msgctxt ""
"fmsearch.src\n"
"RID_STR_SEARCH_COUNTING\n"
"string.text"
msgid "counting records"
msgstr "подсчёт записей"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_NOFILES\n"
"string.text"
msgid "<No Files>"
msgstr "<без файлов>"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERYPROPS_OBJECT\n"
"string.text"
msgid "Object;Objects"
msgstr "Объект;Объекты"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_READONLY\n"
"string.text"
msgid "(read-only)"
msgstr "(только чтение)"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_ALLFILES\n"
"string.text"
msgid "<All Files>"
msgstr "<Все файлы>"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_ID_EXISTS\n"
"string.text"
msgid "This ID already exists..."
msgstr "Этот ID уже существует..."

#: hyperdlg.src
msgctxt ""
"hyperdlg.src\n"
"RID_SVXSTR_HYPDLG_CLOSEBUT\n"
"string.text"
msgid "Close"
msgstr "~Закрыть"

#: hyperdlg.src
msgctxt ""
"hyperdlg.src\n"
"RID_SVXSTR_HYPDLG_MACROACT1\n"
"string.text"
msgid "Mouse over object"
msgstr "Мышь над объектом"

#: hyperdlg.src
msgctxt ""
"hyperdlg.src\n"
"RID_SVXSTR_HYPDLG_MACROACT2\n"
"string.text"
msgid "Trigger hyperlink"
msgstr "Переход по гиперссылке"

#: hyperdlg.src
msgctxt ""
"hyperdlg.src\n"
"RID_SVXSTR_HYPDLG_MACROACT3\n"
"string.text"
msgid "Mouse leaves object"
msgstr "Мышь покидает объект"

#: hyperdlg.src
msgctxt ""
"hyperdlg.src\n"
"RID_SVXSTR_HYPDLG_NOVALIDFILENAME\n"
"string.text"
msgid "Please type in a valid file name."
msgstr "Введите правильное имя файла."

#: hyperdlg.src
msgctxt ""
"hyperdlg.src\n"
"RID_SVXSTR_HYPERDLG_HLINETTP\n"
"string.text"
msgid "Internet"
msgstr "Интернет"

#: hyperdlg.src
msgctxt ""
"hyperdlg.src\n"
"RID_SVXSTR_HYPERDLG_HLINETTP_HELP\n"
"string.text"
msgid "This is where you create a link to a Web page or FTP server connection."
msgstr "Здесь вы создаёте ссылку на веб-страницу или подключение к серверу FTP."

#: hyperdlg.src
msgctxt ""
"hyperdlg.src\n"
"RID_SVXSTR_HYPERDLG_HLMAILTP\n"
"string.text"
msgid "Mail"
msgstr "Почта"

#: hyperdlg.src
msgctxt ""
"hyperdlg.src\n"
"RID_SVXSTR_HYPERDLG_HLMAILTP_HELP\n"
"string.text"
msgid "This is where you create a link to an e-mail address."
msgstr "Здесь вы создаёте ссылку на адрес электронной почты."

#: hyperdlg.src
msgctxt ""
"hyperdlg.src\n"
"RID_SVXSTR_HYPERDLG_HLDOCTP\n"
"string.text"
msgid "Document"
msgstr "Документ"

#: hyperdlg.src
msgctxt ""
"hyperdlg.src\n"
"RID_SVXSTR_HYPERDLG_HLDOCTP_HELP\n"
"string.text"
msgid "This is where you create a link to an existing document or a target within a document."
msgstr "Здесь вы создаёте ссылку на существующий документ или на определённое место в нём."

#: hyperdlg.src
msgctxt ""
"hyperdlg.src\n"
"RID_SVXSTR_HYPERDLG_HLDOCNTP\n"
"string.text"
msgid "New Document"
msgstr "Создать документ"

#: hyperdlg.src
msgctxt ""
"hyperdlg.src\n"
"RID_SVXSTR_HYPERDLG_HLDOCNTP_HELP\n"
"string.text"
msgid "This is where you create a new document to which the new link points."
msgstr "Здесь вы создаёте новый документ, на который указывает новая ссылка."

#: hyperdlg.src
msgctxt ""
"hyperdlg.src\n"
"RID_SVXSTR_HYPERDLG_FORM_BUTTON\n"
"string.text"
msgid "Button"
msgstr "Кнопка"

#: hyperdlg.src
msgctxt ""
"hyperdlg.src\n"
"RID_SVXSTR_HYPERDLG_FROM_TEXT\n"
"string.text"
msgid "Text"
msgstr "Текст"

#: hyperdlg.src
msgctxt ""
"hyperdlg.src\n"
"RID_SVXSTR_HYPERDLG_QUERYOVERWRITE\n"
"string.text"
msgid "The file already exists. Overwrite?"
msgstr "Этот файл уже существует. Переписать его?"

#: multipat.src
msgctxt ""
"multipat.src\n"
"RID_MULTIPATH_DBL_ERR\n"
"string.text"
msgid "The path %1 already exists."
msgstr "Путь %1 уже существует."

#: multipat.src
msgctxt ""
"multipat.src\n"
"RID_SVXSTR_ARCHIVE_TITLE\n"
"string.text"
msgid "Select Archives"
msgstr "Выбор архивов"

#: multipat.src
msgctxt ""
"multipat.src\n"
"RID_SVXSTR_ARCHIVE_HEADLINE\n"
"string.text"
msgid "Archives"
msgstr "Архивы"

#: multipat.src
msgctxt ""
"multipat.src\n"
"RID_SVXSTR_MULTIFILE_DBL_ERR\n"
"string.text"
msgid "The file %1 already exists."
msgstr "Файл %1 уже существует."

#: multipat.src
msgctxt ""
"multipat.src\n"
"RID_SVXSTR_ADD_IMAGE\n"
"string.text"
msgid "Add Image"
msgstr "Добавить изображение"

#: passwdomdlg.src
msgctxt ""
"passwdomdlg.src\n"
"RID_SVXSTR_PASSWD_MUST_BE_CONFIRMED\n"
"string.text"
msgid "Password must be confirmed"
msgstr "Необходимо подтвердить пароль"

#: passwdomdlg.src
msgctxt ""
"passwdomdlg.src\n"
"RID_SVXSTR_ONE_PASSWORD_MISMATCH\n"
"string.text"
msgid "The confirmation password did not match the password. Set the password again by entering the same password in both boxes."
msgstr "Подтверждение и пароль не совпадают. Попробуйте снова, введя в оба поля одинаковые пароли."

#: passwdomdlg.src
msgctxt ""
"passwdomdlg.src\n"
"RID_SVXSTR_TWO_PASSWORDS_MISMATCH\n"
"string.text"
msgid "The confirmation passwords did not match the original passwords. Set the passwords again."
msgstr "Подтверждающие пароли не совпадают с оригнальными. Задайте пароли снова."

#: passwdomdlg.src
msgctxt ""
"passwdomdlg.src\n"
"RID_SVXSTR_INVALID_STATE_FOR_OK_BUTTON\n"
"string.text"
msgid "Please enter a password to open or to modify, or check the open read-only option to continue."
msgstr "Для продолжения введите пароль открытия или изменения либо укажите «только для чтения»."

#: passwdomdlg.src
msgctxt ""
"passwdomdlg.src\n"
"RID_SVXSTR_INVALID_STATE_FOR_OK_BUTTON_V2\n"
"string.text"
msgid "Set the password by entering the same password in both boxes."
msgstr "Задайте пароль, введя его в оба поля."

#: screenshotannotationdlg.src
msgctxt ""
"screenshotannotationdlg.src\n"
"RID_SVXSTR_SAVE_SCREENSHOT_AS\n"
"string.text"
msgid "Save Screenshot As..."
msgstr "Сохранить снимок как..."

#: scriptdlg.src
msgctxt ""
"scriptdlg.src\n"
"RID_SVXSTR_DELQUERY\n"
"string.text"
msgid "Do you want to delete the following object?"
msgstr "Вы действительно хотите удалить этот объект?"

#: scriptdlg.src
msgctxt ""
"scriptdlg.src\n"
"RID_SVXSTR_DELQUERY_TITLE\n"
"string.text"
msgid "Confirm Deletion"
msgstr "Подтвердить удаление"

#: scriptdlg.src
msgctxt ""
"scriptdlg.src\n"
"RID_SVXSTR_DELFAILED\n"
"string.text"
msgid "The selected object could not be deleted."
msgstr "Выбранный объект не может быть удалён."

#: scriptdlg.src
msgctxt ""
"scriptdlg.src\n"
"RID_SVXSTR_DELFAILEDPERM\n"
"string.text"
msgid " You do not have permission to delete this object."
msgstr " У вас не хватает прав для удаления данного объекта."

#: scriptdlg.src
msgctxt ""
"scriptdlg.src\n"
"RID_SVXSTR_DELFAILED_TITLE\n"
"string.text"
msgid "Error Deleting Object"
msgstr "Ошибка при удалении объекта"

#: scriptdlg.src
msgctxt ""
"scriptdlg.src\n"
"RID_SVXSTR_CREATEFAILED\n"
"string.text"
msgid "The object could not be created."
msgstr "Объект не может быть создан."

#: scriptdlg.src
msgctxt ""
"scriptdlg.src\n"
"RID_SVXSTR_CREATEFAILEDDUP\n"
"string.text"
msgid " Object with the same name already exists."
msgstr " Объект с таким именем уже существует."

#: scriptdlg.src
msgctxt ""
"scriptdlg.src\n"
"RID_SVXSTR_CREATEFAILEDPERM\n"
"string.text"
msgid " You do not have permission to create this object."
msgstr " У вас не хватает прав для создания данного объекта."

#: scriptdlg.src
msgctxt ""
"scriptdlg.src\n"
"RID_SVXSTR_CREATEFAILED_TITLE\n"
"string.text"
msgid "Error Creating Object"
msgstr "Ошибка при создании объекта"

#: scriptdlg.src
msgctxt ""
"scriptdlg.src\n"
"RID_SVXSTR_RENAMEFAILED\n"
"string.text"
msgid "The object could not be renamed."
msgstr "Объект не может быть переименован."

#: scriptdlg.src
msgctxt ""
"scriptdlg.src\n"
"RID_SVXSTR_RENAMEFAILEDPERM\n"
"string.text"
msgid " You do not have permission to rename this object."
msgstr " У вас не хватает прав для переименования данного объекта."

#: scriptdlg.src
msgctxt ""
"scriptdlg.src\n"
"RID_SVXSTR_RENAMEFAILED_TITLE\n"
"string.text"
msgid "Error Renaming Object"
msgstr "Ошибка при переименовании объекта"

#: scriptdlg.src
msgctxt ""
"scriptdlg.src\n"
"RID_SVXSTR_ERROR_TITLE\n"
"string.text"
msgid "%PRODUCTNAME Error"
msgstr "Ошибка %PRODUCTNAME"

#: scriptdlg.src
msgctxt ""
"scriptdlg.src\n"
"RID_SVXSTR_ERROR_LANG_NOT_SUPPORTED\n"
"string.text"
msgid "The scripting language %LANGUAGENAME is not supported."
msgstr "Язык сценариев %LANGUAGENAME не поддерживается."

#: scriptdlg.src
msgctxt ""
"scriptdlg.src\n"
"RID_SVXSTR_ERROR_RUNNING\n"
"string.text"
msgid "An error occurred while running the %LANGUAGENAME script %SCRIPTNAME."
msgstr "Ошибка при выполнении сценария %LANGUAGENAME %SCRIPTNAME."

#: scriptdlg.src
msgctxt ""
"scriptdlg.src\n"
"RID_SVXSTR_EXCEPTION_RUNNING\n"
"string.text"
msgid "An exception occurred while running the %LANGUAGENAME script %SCRIPTNAME."
msgstr "Исключение при выполнении сценария %LANGUAGENAME %SCRIPTNAME."

#: scriptdlg.src
msgctxt ""
"scriptdlg.src\n"
"RID_SVXSTR_ERROR_AT_LINE\n"
"string.text"
msgid "An error occurred while running the %LANGUAGENAME script %SCRIPTNAME at line: %LINENUMBER."
msgstr "Ошибка при выполнении сценария %LANGUAGENAME %SCRIPTNAME в строке: %LINENUMBER."

#: scriptdlg.src
msgctxt ""
"scriptdlg.src\n"
"RID_SVXSTR_EXCEPTION_AT_LINE\n"
"string.text"
msgid "An exception occurred while running the %LANGUAGENAME script %SCRIPTNAME at line: %LINENUMBER."
msgstr "Исключение при выполнении сценария %LANGUAGENAME %SCRIPTNAME в строке: %LINENUMBER."

#: scriptdlg.src
msgctxt ""
"scriptdlg.src\n"
"RID_SVXSTR_FRAMEWORK_ERROR_RUNNING\n"
"string.text"
msgid "A Scripting Framework error occurred while running the %LANGUAGENAME script %SCRIPTNAME."
msgstr "Ошибка сценария при выполнении сценария %LANGUAGENAME %SCRIPTNAME."

#: scriptdlg.src
msgctxt ""
"scriptdlg.src\n"
"RID_SVXSTR_FRAMEWORK_ERROR_AT_LINE\n"
"string.text"
msgid "A Scripting Framework error occurred while running the %LANGUAGENAME script %SCRIPTNAME at line: %LINENUMBER."
msgstr "Ошибка сценария при выполнении сценария %LANGUAGENAME %SCRIPTNAME в строке: %LINENUMBER."

#: scriptdlg.src
msgctxt ""
"scriptdlg.src\n"
"RID_SVXSTR_ERROR_TYPE_LABEL\n"
"string.text"
msgid "Type:"
msgstr "Тип:"

#: scriptdlg.src
msgctxt ""
"scriptdlg.src\n"
"RID_SVXSTR_ERROR_MESSAGE_LABEL\n"
"string.text"
msgid "Message:"
msgstr "Сообщение:"

#: svuidlg.src
msgctxt ""
"svuidlg.src\n"
"STR_AUTOLINK\n"
"string.text"
msgid "Automatic"
msgstr "Автоматически"

#: svuidlg.src
msgctxt ""
"svuidlg.src\n"
"STR_MANUALLINK\n"
"string.text"
msgid "Manual"
msgstr "Вручную"

#: svuidlg.src
msgctxt ""
"svuidlg.src\n"
"STR_BROKENLINK\n"
"string.text"
msgid "Not available"
msgstr "Недоступно"

#: svuidlg.src
msgctxt ""
"svuidlg.src\n"
"STR_CLOSELINKMSG\n"
"string.text"
msgid "Are you sure you want to remove the selected link?"
msgstr "Вы действительно хотите удалить выделенные связи?"

#: svuidlg.src
msgctxt ""
"svuidlg.src\n"
"STR_CLOSELINKMSG_MULTI\n"
"string.text"
msgid "Are you sure you want to remove the selected link?"
msgstr "Вы действительно хотите удалить выделенные связи?"

#: svuidlg.src
msgctxt ""
"svuidlg.src\n"
"STR_WAITINGLINK\n"
"string.text"
msgid "Waiting"
msgstr "Ожидание"

#. extracted from sw/source/ui/app
msgid ""
msgstr ""
"Project-Id-Version: app\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2017-10-17 14:37+0000\n"
"Last-Translator: bormant <bormant@mail.ru>\n"
"Language-Team: Russian <ru@li.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1508251065.000000\n"

#: app.src
msgctxt ""
"app.src\n"
"STR_PRINT_MERGE_MACRO\n"
"string.text"
msgid "Print form letters"
msgstr "Печать сформированных писем"

#: app.src
msgctxt ""
"app.src\n"
"STR_PAGE_COUNT_MACRO\n"
"string.text"
msgid "Changing the page count"
msgstr "Изменение количества страниц"

#: app.src
msgctxt ""
"app.src\n"
"STR_PARAGRAPHSTYLEFAMILY\n"
"string.text"
msgid "Paragraph Styles"
msgstr "Стили абзацев"

#: app.src
msgctxt ""
"app.src\n"
"STR_CHARACTERSTYLEFAMILY\n"
"string.text"
msgid "Character Styles"
msgstr "Стили символов"

#: app.src
msgctxt ""
"app.src\n"
"STR_FRAMESTYLEFAMILY\n"
"string.text"
msgid "Frame Styles"
msgstr "Стили врезок"

#: app.src
msgctxt ""
"app.src\n"
"STR_PAGESTYLEFAMILY\n"
"string.text"
msgid "Page Styles"
msgstr "Стили страниц"

#: app.src
msgctxt ""
"app.src\n"
"STR_LISTSTYLEFAMILY\n"
"string.text"
msgid "List Styles"
msgstr "Стили списков"

#: app.src
msgctxt ""
"app.src\n"
"STR_TABLESTYLEFAMILY\n"
"string.text"
msgid "Table Styles"
msgstr "Стили таблиц"

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"All Styles\n"
"itemlist.text"
msgid "All Styles"
msgstr "Все стили"

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"Hidden Styles\n"
"itemlist.text"
msgid "Hidden Styles"
msgstr "Скрытые стили"

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"Applied Styles\n"
"itemlist.text"
msgid "Applied Styles"
msgstr "Используемые стили"

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"Custom Styles\n"
"itemlist.text"
msgid "Custom Styles"
msgstr "Стили пользователя"

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"Automatic\n"
"itemlist.text"
msgid "Automatic"
msgstr "Автоматически"

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"Text Styles\n"
"itemlist.text"
msgid "Text Styles"
msgstr "Стили текста"

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"Chapter Styles\n"
"itemlist.text"
msgid "Chapter Styles"
msgstr "Стили глав"

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"List Styles\n"
"itemlist.text"
msgid "List Styles"
msgstr "Стили списков"

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"Index Styles\n"
"itemlist.text"
msgid "Index Styles"
msgstr "Стили указателей"

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"Special Styles\n"
"itemlist.text"
msgid "Special Styles"
msgstr "Специальные стили"

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"HTML Styles\n"
"itemlist.text"
msgid "HTML Styles"
msgstr "Стили HTML"

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"Conditional Styles\n"
"itemlist.text"
msgid "Conditional Styles"
msgstr "Условные стили"

#: app.src
msgctxt ""
"app.src\n"
"RID_CHARACTERSTYLEFAMILY\n"
"All\n"
"itemlist.text"
msgid "All"
msgstr "Все"

#: app.src
msgctxt ""
"app.src\n"
"RID_CHARACTERSTYLEFAMILY\n"
"Hidden Styles\n"
"itemlist.text"
msgid "Hidden Styles"
msgstr "Скрытые стили"

#: app.src
msgctxt ""
"app.src\n"
"RID_CHARACTERSTYLEFAMILY\n"
"Applied Styles\n"
"itemlist.text"
msgid "Applied Styles"
msgstr "Используемые стили"

#: app.src
msgctxt ""
"app.src\n"
"RID_CHARACTERSTYLEFAMILY\n"
"Custom Styles\n"
"itemlist.text"
msgid "Custom Styles"
msgstr "Стили пользователя"

#: app.src
msgctxt ""
"app.src\n"
"RID_FRAMESTYLEFAMILY\n"
"All\n"
"itemlist.text"
msgid "All"
msgstr "Все"

#: app.src
msgctxt ""
"app.src\n"
"RID_FRAMESTYLEFAMILY\n"
"Hidden Styles\n"
"itemlist.text"
msgid "Hidden Styles"
msgstr "Скрытые стили"

#: app.src
msgctxt ""
"app.src\n"
"RID_FRAMESTYLEFAMILY\n"
"Applied Styles\n"
"itemlist.text"
msgid "Applied Styles"
msgstr "Используемые стили"

#: app.src
msgctxt ""
"app.src\n"
"RID_FRAMESTYLEFAMILY\n"
"Custom Styles\n"
"itemlist.text"
msgid "Custom Styles"
msgstr "Стили пользователя"

#: app.src
msgctxt ""
"app.src\n"
"RID_PAGESTYLEFAMILY\n"
"All\n"
"itemlist.text"
msgid "All"
msgstr "Все"

#: app.src
msgctxt ""
"app.src\n"
"RID_PAGESTYLEFAMILY\n"
"Hidden Styles\n"
"itemlist.text"
msgid "Hidden Styles"
msgstr "Скрытые стили"

#: app.src
msgctxt ""
"app.src\n"
"RID_PAGESTYLEFAMILY\n"
"Applied Styles\n"
"itemlist.text"
msgid "Applied Styles"
msgstr "Используемые стили"

#: app.src
msgctxt ""
"app.src\n"
"RID_PAGESTYLEFAMILY\n"
"Custom Styles\n"
"itemlist.text"
msgid "Custom Styles"
msgstr "Стили пользователя"

#: app.src
msgctxt ""
"app.src\n"
"RID_LISTSTYLEFAMILY\n"
"All\n"
"itemlist.text"
msgid "All"
msgstr "Все"

#: app.src
msgctxt ""
"app.src\n"
"RID_LISTSTYLEFAMILY\n"
"Hidden Styles\n"
"itemlist.text"
msgid "Hidden Styles"
msgstr "Скрытые стили"

#: app.src
msgctxt ""
"app.src\n"
"RID_LISTSTYLEFAMILY\n"
"Applied Styles\n"
"itemlist.text"
msgid "Applied Styles"
msgstr "Используемые стили"

#: app.src
msgctxt ""
"app.src\n"
"RID_LISTSTYLEFAMILY\n"
"Custom Styles\n"
"itemlist.text"
msgid "Custom Styles"
msgstr "Стили пользователя"

#: app.src
msgctxt ""
"app.src\n"
"RID_TABLESTYLEFAMILY\n"
"All\n"
"itemlist.text"
msgid "All"
msgstr "Все"

#: app.src
msgctxt ""
"app.src\n"
"RID_TABLESTYLEFAMILY\n"
"Hidden Styles\n"
"itemlist.text"
msgid "Hidden Styles"
msgstr "Скрытые стили"

#: app.src
msgctxt ""
"app.src\n"
"RID_TABLESTYLEFAMILY\n"
"Applied Styles\n"
"itemlist.text"
msgid "Applied Styles"
msgstr "Используемые стили"

#: app.src
msgctxt ""
"app.src\n"
"RID_TABLESTYLEFAMILY\n"
"Custom Styles\n"
"itemlist.text"
msgid "Custom Styles"
msgstr "Стили пользователя"

#: app.src
msgctxt ""
"app.src\n"
"STR_ENV_TITLE\n"
"string.text"
msgid "Envelope"
msgstr "Конверт"

#: app.src
msgctxt ""
"app.src\n"
"STR_LAB_TITLE\n"
"string.text"
msgid "Labels"
msgstr "Этикетка"

#: app.src
msgctxt ""
"app.src\n"
"STR_HUMAN_SWDOC_NAME\n"
"string.text"
msgid "Text"
msgstr "Текст"

#: app.src
msgctxt ""
"app.src\n"
"STR_WRITER_DOCUMENT_FULLTYPE\n"
"string.text"
msgid "%PRODUCTNAME %PRODUCTVERSION Text Document"
msgstr "Текстовый документ (%PRODUCTNAME %PRODUCTVERSION)"

#: app.src
msgctxt ""
"app.src\n"
"STR_CANTOPEN\n"
"string.text"
msgid "Cannot open document."
msgstr "Невозможно открыть документ."

#: app.src
msgctxt ""
"app.src\n"
"STR_CANTCREATE\n"
"string.text"
msgid "Can't create document."
msgstr "Невозможно создать документ."

#: app.src
msgctxt ""
"app.src\n"
"STR_DLLNOTFOUND\n"
"string.text"
msgid "Filter not found."
msgstr "Фильтр не найден."

#: app.src
msgctxt ""
"app.src\n"
"STR_UNBENANNT\n"
"string.text"
msgid "Untitled"
msgstr "Без имени"

#: app.src
msgctxt ""
"app.src\n"
"STR_LOAD_GLOBAL_DOC\n"
"string.text"
msgid "Name and Path of Master Document"
msgstr "Имя и путь составного документа"

#: app.src
msgctxt ""
"app.src\n"
"STR_LOAD_HTML_DOC\n"
"string.text"
msgid "Name and Path of the HTML Document"
msgstr "Имя и путь к документу HTML"

#: app.src
msgctxt ""
"app.src\n"
"STR_JAVA_EDIT\n"
"string.text"
msgid "Edit Script"
msgstr "Редактировать сценарий"

#: app.src
msgctxt ""
"app.src\n"
"STR_REMOVE_WARNING\n"
"string.text"
msgid "The following characters are not valid and have been removed: "
msgstr "Следующие некорректные символы были удалены: "

#: app.src
msgctxt ""
"app.src\n"
"STR_BOOKMARK_NAME\n"
"string.text"
msgid "Name"
msgstr "Имя"

#: app.src
msgctxt ""
"app.src\n"
"STR_BOOKMARK_TEXT\n"
"string.text"
msgid "Text"
msgstr "Текст"

#: app.src
msgctxt ""
"app.src\n"
"SW_STR_NONE\n"
"string.text"
msgid "[None]"
msgstr "[Нет]"

#: app.src
msgctxt ""
"app.src\n"
"STR_CAPTION_BEGINNING\n"
"string.text"
msgid "Start"
msgstr "Запуск"

#: app.src
msgctxt ""
"app.src\n"
"STR_CAPTION_END\n"
"string.text"
msgid "End"
msgstr "Конце"

#: app.src
msgctxt ""
"app.src\n"
"STR_CAPTION_ABOVE\n"
"string.text"
msgid "Above"
msgstr "Сверху"

#: app.src
msgctxt ""
"app.src\n"
"STR_CAPTION_BELOW\n"
"string.text"
msgid "Below"
msgstr "Снизу"

#: app.src
msgctxt ""
"app.src\n"
"SW_STR_READONLY\n"
"string.text"
msgid "read-only"
msgstr "только для чтения"

#: app.src
msgctxt ""
"app.src\n"
"STR_READONLY_PATH\n"
"string.text"
msgid "The 'AutoText' directories are read-only. Do you want to call the path settings dialog?"
msgstr "Каталог 'AutoText' предназначен только для чтения. Вызвать диалог задания пути?"

#: app.src
msgctxt ""
"app.src\n"
"STR_ERROR_PASSWD\n"
"string.text"
msgid "Invalid password"
msgstr "Неправильный пароль"

#: app.src
msgctxt ""
"app.src\n"
"STR_FMT_STD\n"
"string.text"
msgid "(none)"
msgstr "(Нет)"

#: app.src
msgctxt ""
"app.src\n"
"STR_DOC_STAT\n"
"string.text"
msgid "Statistics"
msgstr "Статистика"

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_W4WREAD\n"
"string.text"
msgid "Importing document..."
msgstr "Импорт документа..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_W4WWRITE\n"
"string.text"
msgid "Exporting document..."
msgstr "Экспорт документа..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_SWGREAD\n"
"string.text"
msgid "Loading document..."
msgstr "Загрузка документа..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_SWGWRITE\n"
"string.text"
msgid "Saving document..."
msgstr "Сохранение документа..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_REFORMAT\n"
"string.text"
msgid "Repagination..."
msgstr "Разбивка на страницы..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_AUTOFORMAT\n"
"string.text"
msgid "Formatting document automatically..."
msgstr "Автоматическое форматирование документа..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_IMPGRF\n"
"string.text"
msgid "Importing images..."
msgstr "Импорт изображений..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_SEARCH\n"
"string.text"
msgid "Search..."
msgstr "Поиск..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_FORMAT\n"
"string.text"
msgid "Formatting..."
msgstr "Форматирование..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_PRINT\n"
"string.text"
msgid "Printing..."
msgstr "Печать..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_LAYOUTINIT\n"
"string.text"
msgid "Converting..."
msgstr "Преобразование..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_LETTER\n"
"string.text"
msgid "Letter"
msgstr "Письмо"

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_SPELL\n"
"string.text"
msgid "Spellcheck..."
msgstr "Проверка орфографии..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_HYPHEN\n"
"string.text"
msgid "Hyphenation..."
msgstr "Расстановка переносов..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_TOX_INSERT\n"
"string.text"
msgid "Inserting Index..."
msgstr "Вставка указателя..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_TOX_UPDATE\n"
"string.text"
msgid "Updating Index..."
msgstr "Обновление указателя..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_SUMMARY\n"
"string.text"
msgid "Creating abstract..."
msgstr "Создание реферата..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_SWGPRTOLENOTIFY\n"
"string.text"
msgid "Adapt Objects..."
msgstr "Адаптировать объекты..."

#: app.src
msgctxt ""
"app.src\n"
"STR_TABLE_DEFNAME\n"
"string.text"
msgid "Table"
msgstr "Таблица"

#: app.src
msgctxt ""
"app.src\n"
"STR_GRAPHIC_DEFNAME\n"
"string.text"
msgid "Image"
msgstr "Изображение"

#: app.src
msgctxt ""
"app.src\n"
"STR_OBJECT_DEFNAME\n"
"string.text"
msgid "Object"
msgstr "Объект"

#: app.src
msgctxt ""
"app.src\n"
"STR_FRAME_DEFNAME\n"
"string.text"
msgid "Frame"
msgstr "Врезка"

#: app.src
msgctxt ""
"app.src\n"
"STR_SHAPE_DEFNAME\n"
"string.text"
msgid "Shape"
msgstr "Фигура"

#: app.src
msgctxt ""
"app.src\n"
"STR_REGION_DEFNAME\n"
"string.text"
msgid "Section"
msgstr "Раздел"

#: app.src
msgctxt ""
"app.src\n"
"STR_NUMRULE_DEFNAME\n"
"string.text"
msgid "Numbering"
msgstr "Нумерация"

#: app.src
msgctxt ""
"app.src\n"
"STR_EMPTYPAGE\n"
"string.text"
msgid "blank page"
msgstr "Пустая страница"

#: app.src
msgctxt ""
"app.src\n"
"STR_ABSTRACT_TITLE\n"
"string.text"
msgid "Abstract: "
msgstr "Абстрактное представление: "

#: app.src
msgctxt ""
"app.src\n"
"STR_FDLG_TEMPLATE_BUTTON\n"
"string.text"
msgid "Style"
msgstr "Стиль"

#: app.src
msgctxt ""
"app.src\n"
"STR_FDLG_TEMPLATE_NAME\n"
"string.text"
msgid "separated by: "
msgstr "отделять по: "

#: app.src
msgctxt ""
"app.src\n"
"STR_FDLG_OUTLINE_LEVEL\n"
"string.text"
msgid "Outline: Level "
msgstr "Структура: уровень "

#: app.src
msgctxt ""
"app.src\n"
"STR_FDLG_STYLE\n"
"string.text"
msgid "Style: "
msgstr "Текущий стиль: "

#: app.src
msgctxt ""
"app.src\n"
"STR_PAGEOFFSET\n"
"string.text"
msgid "Page number: "
msgstr "Номер страницы: "

#: app.src
msgctxt ""
"app.src\n"
"STR_PAGEBREAK\n"
"string.text"
msgid "Break before new page"
msgstr "Разрыв перед новой страницей"

#: app.src
msgctxt ""
"app.src\n"
"STR_WESTERN_FONT\n"
"string.text"
msgid "Western text: "
msgstr "Западный текст: "

#: app.src
msgctxt ""
"app.src\n"
"STR_CJK_FONT\n"
"string.text"
msgid "Asian text: "
msgstr "Восточноазиатский текст: "

#: app.src
msgctxt ""
"app.src\n"
"STR_REDLINE_UNKNOWN_AUTHOR\n"
"string.text"
msgid "Unknown Author"
msgstr "<анонимный>"

#: app.src
msgctxt ""
"app.src\n"
"STR_DELETE_NOTE_AUTHOR\n"
"string.text"
msgid "Delete ~All Comments by $1"
msgstr "Удалить все комментарии ~от $1"

#: app.src
msgctxt ""
"app.src\n"
"STR_HIDE_NOTE_AUTHOR\n"
"string.text"
msgid "H~ide All Comments by $1"
msgstr "Скрыть все комментарии от $1"

#: app.src
msgctxt ""
"app.src\n"
"STR_DONT_ASK_AGAIN\n"
"string.text"
msgid "~Do not show warning again"
msgstr "Больше не показывать предупреждение"

#: app.src
msgctxt ""
"app.src\n"
"STR_OUTLINE_NUMBERING\n"
"string.text"
msgid "Outline Numbering"
msgstr "Структура нумерации"

#: app.src
msgctxt ""
"app.src\n"
"STR_STATUSBAR_WORDCOUNT_NO_SELECTION\n"
"string.text"
msgid "%1 words, %2 characters"
msgstr "%1 слов, %2 символов"

#: app.src
msgctxt ""
"app.src\n"
"STR_STATUSBAR_WORDCOUNT\n"
"string.text"
msgid "%1 words, %2 characters selected"
msgstr "%1 слов, %2 выделенных символов"

#: app.src
msgctxt ""
"app.src\n"
"STR_CONVERT_TEXT_TABLE\n"
"string.text"
msgid "Convert Text to Table"
msgstr "Преобразовать текст в таблицу"

#: app.src
msgctxt ""
"app.src\n"
"STR_ADD_AUTOFORMAT_TITLE\n"
"string.text"
msgid "Add AutoFormat"
msgstr "Добавить автоформат"

#: app.src
msgctxt ""
"app.src\n"
"STR_ADD_AUTOFORMAT_LABEL\n"
"string.text"
msgid "Name"
msgstr "Название"

#: app.src
msgctxt ""
"app.src\n"
"STR_DEL_AUTOFORMAT_TITLE\n"
"string.text"
msgid "Delete AutoFormat"
msgstr "Удалить автоформат"

#: app.src
msgctxt ""
"app.src\n"
"STR_DEL_AUTOFORMAT_MSG\n"
"string.text"
msgid "The following AutoFormat entry will be deleted:"
msgstr "Следующий автоформат будет удалён:"

#: app.src
msgctxt ""
"app.src\n"
"STR_RENAME_AUTOFORMAT_TITLE\n"
"string.text"
msgid "Rename AutoFormat"
msgstr "Переименовать автоформат"

#: app.src
msgctxt ""
"app.src\n"
"STR_BTN_AUTOFORMAT_CLOSE\n"
"string.text"
msgid "~Close"
msgstr "Закрыть"

#: app.src
msgctxt ""
"app.src\n"
"STR_JAN\n"
"string.text"
msgid "Jan"
msgstr "Янв"

#: app.src
msgctxt ""
"app.src\n"
"STR_FEB\n"
"string.text"
msgid "Feb"
msgstr "Фев"

#: app.src
msgctxt ""
"app.src\n"
"STR_MAR\n"
"string.text"
msgid "Mar"
msgstr "Мар"

#: app.src
msgctxt ""
"app.src\n"
"STR_NORTH\n"
"string.text"
msgid "North"
msgstr "Север"

#: app.src
msgctxt ""
"app.src\n"
"STR_MID\n"
"string.text"
msgid "Mid"
msgstr "Середина"

#: app.src
msgctxt ""
"app.src\n"
"STR_SOUTH\n"
"string.text"
msgid "South"
msgstr "Юг"

#: app.src
msgctxt ""
"app.src\n"
"STR_SUM\n"
"string.text"
msgid "Sum"
msgstr "Сумма"

#: app.src
msgctxt ""
"app.src\n"
"STR_INVALID_AUTOFORMAT_NAME\n"
"string.text"
msgid ""
"You have entered an invalid name.\n"
"The desired AutoFormat could not be created. \n"
"Try again using a different name."
msgstr ""
"Введено неправильное название.\n"
"Невозможно создать желаемый автоформат.\n"
"Используйте другое название."

#: app.src
msgctxt ""
"app.src\n"
"STR_NUMERIC\n"
"string.text"
msgid "Numeric"
msgstr "Числовой"

#: app.src
msgctxt ""
"app.src\n"
"STR_ROW\n"
"string.text"
msgid "Rows"
msgstr "Строки"

#: app.src
msgctxt ""
"app.src\n"
"STR_COL\n"
"string.text"
msgid "Column"
msgstr "Столбец"

#: app.src
msgctxt ""
"app.src\n"
"STR_SIMPLE\n"
"string.text"
msgid "Plain"
msgstr "Обычная"

#: app.src
msgctxt ""
"app.src\n"
"STR_AUTHMRK_EDIT\n"
"string.text"
msgid "Edit Bibliography Entry"
msgstr "Правка библиографической ссылки"

#: app.src
msgctxt ""
"app.src\n"
"STR_AUTHMRK_INSERT\n"
"string.text"
msgid "Insert Bibliography Entry"
msgstr "Вставить библиографическую ссылку"

#: app.src
msgctxt ""
"app.src\n"
"STR_ACCESS_PAGESETUP_SPACING\n"
"string.text"
msgid "Spacing between %1 and %2"
msgstr "Интервал между %1 и %2"

#: app.src
msgctxt ""
"app.src\n"
"STR_ACCESS_COLUMN_WIDTH\n"
"string.text"
msgid "Column %1 Width"
msgstr "Ширина колонки %1"

#: app.src
msgctxt ""
"app.src\n"
"STR_CAPTION_TABLE\n"
"string.text"
msgid "%PRODUCTNAME Writer Table"
msgstr "Таблица %PRODUCTNAME Writer"

#: app.src
msgctxt ""
"app.src\n"
"STR_CAPTION_FRAME\n"
"string.text"
msgid "%PRODUCTNAME Writer Frame"
msgstr "Фрейм %PRODUCTNAME Writer"

#: app.src
msgctxt ""
"app.src\n"
"STR_CAPTION_GRAPHIC\n"
"string.text"
msgid "%PRODUCTNAME Writer Image"
msgstr "Изображение %PRODUCTNAME Writer"

#: app.src
msgctxt ""
"app.src\n"
"STR_CAPTION_OLE\n"
"string.text"
msgid "Other OLE Objects"
msgstr "Другие объекты OLE"

#: app.src
msgctxt ""
"app.src\n"
"STR_WRONG_TABLENAME\n"
"string.text"
msgid "The name of the table must not contain spaces."
msgstr "Имя таблицы не должно содержать пробелов."

#: app.src
msgctxt ""
"app.src\n"
"STR_ERR_TABLE_MERGE\n"
"string.text"
msgid "Selected table cells are too complex to merge."
msgstr "Выбранные ячейки таблицы слишком сложны для объединения."

#: app.src
msgctxt ""
"app.src\n"
"STR_SRTERR\n"
"string.text"
msgid "Cannot sort selection"
msgstr "Невозможно сортировать выбранное"

#: error.src
msgctxt ""
"error.src\n"
"STR_COMCORE_READERROR\n"
"string.text"
msgid "Read Error"
msgstr "Ошибка чтения"

#: error.src
msgctxt ""
"error.src\n"
"STR_COMCORE_CANT_SHOW\n"
"string.text"
msgid "Image cannot be displayed."
msgstr "Невозможно показать изображение."

#: error.src
msgctxt ""
"error.src\n"
"STR_ERROR_CLPBRD_READ\n"
"string.text"
msgid "Error reading from the clipboard."
msgstr "Ошибка чтения из буфера обмена."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_SWG_FILE_FORMAT_ERROR )\n"
"string.text"
msgid "File format error found."
msgstr "Ошибка формата файла."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_SWG_READ_ERROR )\n"
"string.text"
msgid "Error reading file."
msgstr "Ошибка чтения файла."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_SW6_INPUT_FILE )\n"
"string.text"
msgid "Input file error."
msgstr "Ошибка ввода файла."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_SW6_NOWRITER_FILE )\n"
"string.text"
msgid "This is not a %PRODUCTNAME Writer file."
msgstr "Этот файл не является файлом %PRODUCTNAME Writer."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_SW6_UNEXPECTED_EOF )\n"
"string.text"
msgid "Unexpected end of file."
msgstr "Неожиданный конец файла."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_SW6_PASSWD )\n"
"string.text"
msgid "Password-protected files cannot be opened."
msgstr "Не поддерживаются файлы, защищённые паролями."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_WW6_NO_WW6_FILE_ERR )\n"
"string.text"
msgid "This is not a valid WinWord6 file."
msgstr "Этот файл не является файлом WinWord6."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_WW6_FASTSAVE_ERR )\n"
"string.text"
msgid "This file was saved with WinWord in 'Fast Save' mode. Please unmark the WinWord option 'Allow Fast Saves' and save the file again."
msgstr "Этот файл сохранён из WinWord в режиме «Быстрое сохранение». Пожалуйста, отключите параметр «Разрешить быстрое сохранение» и заново сохраните этот файл из WinWord."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_FORMAT_ROWCOL )\n"
"string.text"
msgid "File format error found at $(ARG1)(row,col)."
msgstr "Ошибка формата файла в позиции $(ARG1)(строка, столбец)."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_SWG_NEW_VERSION )\n"
"string.text"
msgid "File has been written in a newer version."
msgstr "Файл записан в более новой версии."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_WW8_NO_WW8_FILE_ERR )\n"
"string.text"
msgid "This is not a valid WinWord97 file."
msgstr "Этот файл не является файлом WinWord97."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_FORMAT_FILE_ROWCOL )\n"
"string.text"
msgid "Format error discovered in the file in sub-document $(ARG1) at $(ARG2)(row,col)."
msgstr "Ошибка формата файла в поддокументе $(ARG1) в позиции $(ARG2)(строка, столбец)."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_WRITE , ERR_SWG_WRITE_ERROR )\n"
"string.text"
msgid "Error writing file."
msgstr "Ошибка записи файла."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_WRITE , ERR_SWG_OLD_GLOSSARY )\n"
"string.text"
msgid "Wrong AutoText document version."
msgstr "Неправильная версия документа автотекста."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_WRITE , ERR_WRITE_ERROR_FILE )\n"
"string.text"
msgid "Error in writing sub-document $(ARG1)."
msgstr "Ошибка записи поддокумента $(ARG1)."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_SWG_INTERNAL_ERROR )\n"
"string.text"
msgid "Internal error in %PRODUCTNAME Writer file format."
msgstr "Внутренняя ошибка формата файла %PRODUCTNAME Writer."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_WRITE , ERR_SWG_INTERNAL_ERROR )\n"
"string.text"
msgid "Internal error in %PRODUCTNAME Writer file format."
msgstr "Внутренняя ошибка формата файла %PRODUCTNAME Writer."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_LOCKING , ERR_TXTBLOCK_NEWFILE_ERROR )\n"
"string.text"
msgid "$(ARG1) has changed."
msgstr "$(ARG1) изменился."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_PATH , ERR_AUTOPATH_ERROR )\n"
"string.text"
msgid "$(ARG1) does not exist."
msgstr "$(ARG1) не существует."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_NONE , ERR_TBLSPLIT_ERROR )\n"
"string.text"
msgid "Cells cannot be further split."
msgstr "Невозможно далее разбивать ячейки."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_NONE , ERR_TBLINSCOL_ERROR )\n"
"string.text"
msgid "Additional columns cannot be inserted."
msgstr "Невозможно вставить дополнительные столбцы."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_NONE , ERR_TBLDDECHG_ERROR )\n"
"string.text"
msgid "The structure of a linked table cannot be modified."
msgstr "Невозможно изменить структуру связанной таблицы."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"WARN_CODE ( ERRCODE_CLASS_READ , WARN_SWG_NO_DRAWINGS )\n"
"string.text"
msgid "No drawings could be read."
msgstr "Невозможно прочесть рисунки."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"WARN_CODE ( ERRCODE_CLASS_READ , WARN_WW6_FASTSAVE_ERR )\n"
"string.text"
msgid "This file was saved with WinWord in 'Fast Save' mode. Please unmark the WinWord option 'Allow Fast Saves' and save the file again."
msgstr "Этот файл сохранён из WinWord в режиме «Быстрое сохранение». Пожалуйста, отключите параметр «Разрешить быстрое сохранение» и заново сохраните этот файл из WinWord."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"WARN_CODE ( ERRCODE_CLASS_READ , WARN_SWG_FEATURES_LOST )\n"
"string.text"
msgid "Not all attributes could be read."
msgstr "Невозможно прочесть все атрибуты."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"WARN_CODE ( ERRCODE_CLASS_WRITE , WARN_SWG_FEATURES_LOST )\n"
"string.text"
msgid "Not all attributes could be recorded."
msgstr "Невозможно записать все атрибуты."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"WARN_CODE ( ERRCODE_CLASS_READ , WARN_SWG_OLE )\n"
"string.text"
msgid "Some OLE objects could only be loaded as images."
msgstr "Загрузка некоторых объектов OLE возможна только в виде изображений."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"WARN_CODE ( ERRCODE_CLASS_WRITE , WARN_SWG_OLE )\n"
"string.text"
msgid "Some OLE objects could only be saved as images."
msgstr "Сохранение некоторых объектов OLE возможно только в виде изображений."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"WARN_CODE ( ERRCODE_CLASS_READ , WARN_SWG_POOR_LOAD )\n"
"string.text"
msgid "Document could not be completely loaded."
msgstr "Невозможно полностью загрузить документ."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"WARN_CODE ( ERRCODE_CLASS_WRITE , WARN_SWG_POOR_LOAD )\n"
"string.text"
msgid "Document could not be completely saved."
msgstr "Невозможно полностью сохранить документ."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"WARN_CODE ( ERRCODE_CLASS_WRITE , WARN_SWG_HTML_NO_MACROS)\n"
"string.text"
msgid ""
"This HTML document contains %PRODUCTNAME Basic macros.\n"
"They were not saved with the current export settings."
msgstr ""
"Эта документ HTML содержит макросы %PRODUCTNAME Basic.\n"
"Они не были сохранены из-за настроек указанных для экспорта документов."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_WRITE , WARN_WRITE_ERROR_FILE )\n"
"string.text"
msgid "Error in writing sub-document $(ARG1)."
msgstr "Ошибка записи поддокумента $(ARG1)."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_WRITE , WARN_FORMAT_FILE_ROWCOL )\n"
"string.text"
msgid "Format error discovered in the file in sub-document $(ARG1) at $(ARG2)(row,col)."
msgstr "Ошибка формата файла в поддокументе $(ARG1) в позиции $(ARG2)(строка, столбец)."

#: mn.src
msgctxt ""
"mn.src\n"
"MN_ANNOTATION_BUTTON\n"
"FN_REPLY\n"
"menuitem.text"
msgid "Reply"
msgstr "Ответить"

#: mn.src
msgctxt ""
"mn.src\n"
"MN_ANNOTATION_BUTTON\n"
"FN_DELETE_COMMENT\n"
"menuitem.text"
msgid "Delete ~Comment"
msgstr "Удалить комментарий"

#: mn.src
msgctxt ""
"mn.src\n"
"MN_ANNOTATION_BUTTON\n"
"FN_DELETE_NOTE_AUTHOR\n"
"menuitem.text"
msgid "Delete ~All Comments by $1"
msgstr "Удалить все комментарии ~от $1"

#: mn.src
msgctxt ""
"mn.src\n"
"MN_ANNOTATION_BUTTON\n"
"FN_DELETE_ALL_NOTES\n"
"menuitem.text"
msgid "~Delete All Comments"
msgstr "Удалить ~все комментарии"

#: mn.src
msgctxt ""
"mn.src\n"
"MN_ANNOTATION_BUTTON\n"
"FN_FORMAT_ALL_NOTES\n"
"menuitem.text"
msgid "~Format All Comments..."
msgstr "~Формат всех комментариев..."

#: mn.src
msgctxt ""
"mn.src\n"
"MN_HEADERFOOTER_BUTTON\n"
"FN_HEADERFOOTER_BORDERBACK\n"
"menuitem.text"
msgid "Border and Background..."
msgstr "Обрамление и заливка..."

#: mn.src
msgctxt ""
"mn.src\n"
"MN_PAGEBREAK_BUTTON\n"
"FN_PAGEBREAK_EDIT\n"
"menuitem.text"
msgid "Edit Page Break..."
msgstr "Править разрыв страницы..."

#: mn.src
msgctxt ""
"mn.src\n"
"MN_PAGEBREAK_BUTTON\n"
"FN_PAGEBREAK_DELETE\n"
"menuitem.text"
msgid "Delete Page Break"
msgstr "Удалить разрыв страницы"

#. extracted from sfx2/uiconfig/ui
msgid ""
msgstr ""
"Project-Id-Version: ui\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2017-04-11 22:26+0200\n"
"PO-Revision-Date: 2017-05-30 10:55+0000\n"
"Last-Translator: bormant <bormant@mail.ru>\n"
"Language-Team: Russian <l10n@ru.libreoffice.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1496141715.000000\n"

#: alienwarndialog.ui
msgctxt ""
"alienwarndialog.ui\n"
"AlienWarnDialog\n"
"title\n"
"string.text"
msgid "Confirm File Format"
msgstr "Подтверждение формата файла"

#: alienwarndialog.ui
msgctxt ""
"alienwarndialog.ui\n"
"AlienWarnDialog\n"
"text\n"
"string.text"
msgid "This document may contain formatting or content that cannot be saved in the currently selected file format “%FORMATNAME”."
msgstr "Документ может включать в себя форматирование или содержимое, которое невозможно сохранить в выбранном формате «%FORMATNAME»."

#: alienwarndialog.ui
msgctxt ""
"alienwarndialog.ui\n"
"AlienWarnDialog\n"
"secondary_text\n"
"string.text"
msgid "Use the default ODF file format to be sure that the document is saved correctly."
msgstr "Используйте формат ODF по умолчанию, если хотите быть уверенным в том, что документ сохранится корректно."

#: alienwarndialog.ui
msgctxt ""
"alienwarndialog.ui\n"
"cancel\n"
"label\n"
"string.text"
msgid "Use %DEFAULTEXTENSION _Format"
msgstr "Использовать формат %DEFAULTEXTENSION"

#: alienwarndialog.ui
msgctxt ""
"alienwarndialog.ui\n"
"save\n"
"label\n"
"string.text"
msgid "_Use %FORMATNAME Format"
msgstr "Использовать формат %FORMATNAME"

#: alienwarndialog.ui
msgctxt ""
"alienwarndialog.ui\n"
"ask\n"
"label\n"
"string.text"
msgid "_Ask when not saving in ODF or default format"
msgstr "Спрашивать при сохранении в формате, отличном от ODF или по умолчанию"

#: bookmarkdialog.ui
msgctxt ""
"bookmarkdialog.ui\n"
"BookmarkDialog\n"
"title\n"
"string.text"
msgid "Add to Bookmarks"
msgstr "Добавить закладку"

#: bookmarkdialog.ui
msgctxt ""
"bookmarkdialog.ui\n"
"alttitle\n"
"label\n"
"string.text"
msgid "Rename Bookmark"
msgstr "Переименовать закладку"

#: bookmarkdialog.ui
msgctxt ""
"bookmarkdialog.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Bookmark:"
msgstr "Закладка:"

#: checkin.ui
msgctxt ""
"checkin.ui\n"
"CheckinDialog\n"
"title\n"
"string.text"
msgid "Check-In"
msgstr "Выгрузить"

#: checkin.ui
msgctxt ""
"checkin.ui\n"
"MajorVersion\n"
"label\n"
"string.text"
msgid "New major version"
msgstr "Новая основная версия"

#: checkin.ui
msgctxt ""
"checkin.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Version comment:"
msgstr "Комментарий к версии:"

#: cmisinfopage.ui
msgctxt ""
"cmisinfopage.ui\n"
"name\n"
"label\n"
"string.text"
msgid "Name"
msgstr "Название"

#: cmisinfopage.ui
msgctxt ""
"cmisinfopage.ui\n"
"type\n"
"label\n"
"string.text"
msgid "Type"
msgstr "Тип"

#: cmisinfopage.ui
msgctxt ""
"cmisinfopage.ui\n"
"value\n"
"label\n"
"string.text"
msgid "Value"
msgstr "Значение"

#: cmisline.ui
msgctxt ""
"cmisline.ui\n"
"type\n"
"label\n"
"string.text"
msgid "Type"
msgstr "Тип"

#: cmisline.ui
msgctxt ""
"cmisline.ui\n"
"yes\n"
"label\n"
"string.text"
msgid "Yes"
msgstr "Да"

#: cmisline.ui
msgctxt ""
"cmisline.ui\n"
"no\n"
"label\n"
"string.text"
msgid "No"
msgstr "Нет"

#: custominfopage.ui
msgctxt ""
"custominfopage.ui\n"
"add\n"
"label\n"
"string.text"
msgid "Add _Property"
msgstr "Добавить свойство"

#: custominfopage.ui
msgctxt ""
"custominfopage.ui\n"
"name\n"
"label\n"
"string.text"
msgid "Name"
msgstr "Введите имя объекта"

#: custominfopage.ui
msgctxt ""
"custominfopage.ui\n"
"type\n"
"label\n"
"string.text"
msgid "Type"
msgstr "Тип"

#: custominfopage.ui
msgctxt ""
"custominfopage.ui\n"
"value\n"
"label\n"
"string.text"
msgid "Value"
msgstr "Значение"

#: descriptioninfopage.ui
msgctxt ""
"descriptioninfopage.ui\n"
"label27\n"
"label\n"
"string.text"
msgid "_Title:"
msgstr "_Заглавие:"

#: descriptioninfopage.ui
msgctxt ""
"descriptioninfopage.ui\n"
"label28\n"
"label\n"
"string.text"
msgid "_Subject:"
msgstr "_Тема:"

#: descriptioninfopage.ui
msgctxt ""
"descriptioninfopage.ui\n"
"label29\n"
"label\n"
"string.text"
msgid "_Keywords:"
msgstr "_Ключевые слова:"

#: descriptioninfopage.ui
msgctxt ""
"descriptioninfopage.ui\n"
"label30\n"
"label\n"
"string.text"
msgid "_Comments:"
msgstr "Комментарии:"

#: documentfontspage.ui
msgctxt ""
"documentfontspage.ui\n"
"embedFonts\n"
"label\n"
"string.text"
msgid "_Embed fonts in the document"
msgstr "Внедрять шрифты в документ"

#: documentfontspage.ui
msgctxt ""
"documentfontspage.ui\n"
"fontEmbeddingLabel\n"
"label\n"
"string.text"
msgid "Font Embedding"
msgstr "Внедрение шрифтов"

#: documentinfopage.ui
msgctxt ""
"documentinfopage.ui\n"
"label13\n"
"label\n"
"string.text"
msgid "_Created:"
msgstr "Соз_дан:"

#: documentinfopage.ui
msgctxt ""
"documentinfopage.ui\n"
"label14\n"
"label\n"
"string.text"
msgid "_Modified:"
msgstr "И_зменён:"

#: documentinfopage.ui
msgctxt ""
"documentinfopage.ui\n"
"label15\n"
"label\n"
"string.text"
msgid "_Digitally signed:"
msgstr "Ци_фровая подпись:"

#: documentinfopage.ui
msgctxt ""
"documentinfopage.ui\n"
"label16\n"
"label\n"
"string.text"
msgid "Last pri_nted:"
msgstr "Напе_чатан:"

#: documentinfopage.ui
msgctxt ""
"documentinfopage.ui\n"
"label17\n"
"label\n"
"string.text"
msgid "Total _editing time:"
msgstr "Вре_мя редактирования:"

#: documentinfopage.ui
msgctxt ""
"documentinfopage.ui\n"
"label18\n"
"label\n"
"string.text"
msgid "Re_vision number:"
msgstr "Редакция:"

#: documentinfopage.ui
msgctxt ""
"documentinfopage.ui\n"
"showsigned\n"
"label\n"
"string.text"
msgid "Multiply signed document"
msgstr "Документ с множественными подписями"

#: documentinfopage.ui
msgctxt ""
"documentinfopage.ui\n"
"userdatacb\n"
"label\n"
"string.text"
msgid "_Apply user data"
msgstr "Применить данные пользователя"

#: documentinfopage.ui
msgctxt ""
"documentinfopage.ui\n"
"thumbnailsavecb\n"
"label\n"
"string.text"
msgid "Save preview image with this document"
msgstr "Сохранить эскиз в документе"

#: documentinfopage.ui
msgctxt ""
"documentinfopage.ui\n"
"reset\n"
"label\n"
"string.text"
msgid "Reset Properties"
msgstr "Восстановить свойства"

#: documentinfopage.ui
msgctxt ""
"documentinfopage.ui\n"
"signature\n"
"label\n"
"string.text"
msgid "Di_gital Signature..."
msgstr "Цифров_ые подписи..."

#: documentinfopage.ui
msgctxt ""
"documentinfopage.ui\n"
"label11\n"
"label\n"
"string.text"
msgid "_Size:"
msgstr "Ра_змер:"

#: documentinfopage.ui
msgctxt ""
"documentinfopage.ui\n"
"showsize\n"
"label\n"
"string.text"
msgid "unknown"
msgstr "неизвестно"

#: documentinfopage.ui
msgctxt ""
"documentinfopage.ui\n"
"label8\n"
"label\n"
"string.text"
msgid "_Location:"
msgstr "Располо_жение:"

#: documentinfopage.ui
msgctxt ""
"documentinfopage.ui\n"
"label7\n"
"label\n"
"string.text"
msgid "_Type:"
msgstr "Т_ип:"

#: documentinfopage.ui
msgctxt ""
"documentinfopage.ui\n"
"changepass\n"
"label\n"
"string.text"
msgid "Change _Password"
msgstr "Изменить парол_ь"

#: documentinfopage.ui
msgctxt ""
"documentinfopage.ui\n"
"templateft\n"
"label\n"
"string.text"
msgid "Template:"
msgstr "Шаблон:"

#: documentpropertiesdialog.ui
msgctxt ""
"documentpropertiesdialog.ui\n"
"DocumentPropertiesDialog\n"
"title\n"
"string.text"
msgid "Properties of “%1”"
msgstr "Свойства «%1»"

#: documentpropertiesdialog.ui
msgctxt ""
"documentpropertiesdialog.ui\n"
"general\n"
"label\n"
"string.text"
msgid "General "
msgstr "Общие "

#: documentpropertiesdialog.ui
msgctxt ""
"documentpropertiesdialog.ui\n"
"description\n"
"label\n"
"string.text"
msgid "Description"
msgstr "Описание"

#: documentpropertiesdialog.ui
msgctxt ""
"documentpropertiesdialog.ui\n"
"customprops\n"
"label\n"
"string.text"
msgid "Custom Properties"
msgstr "Свойства пользователя"

#: documentpropertiesdialog.ui
msgctxt ""
"documentpropertiesdialog.ui\n"
"cmisprops\n"
"label\n"
"string.text"
msgid "CMIS Properties"
msgstr "Параметры CMIS"

#: documentpropertiesdialog.ui
msgctxt ""
"documentpropertiesdialog.ui\n"
"security\n"
"label\n"
"string.text"
msgid "Security"
msgstr "Безопасность"

#: editdocumentdialog.ui
msgctxt ""
"editdocumentdialog.ui\n"
"EditDocumentDialog\n"
"title\n"
"string.text"
msgid "Confirm editing of document"
msgstr "Подтвердите правку документа"

#: editdocumentdialog.ui
msgctxt ""
"editdocumentdialog.ui\n"
"EditDocumentDialog\n"
"text\n"
"string.text"
msgid "Are you sure you want to edit the document?"
msgstr "Действительно редактировать документ?"

#: editdocumentdialog.ui
msgctxt ""
"editdocumentdialog.ui\n"
"EditDocumentDialog\n"
"secondary_text\n"
"string.text"
msgid "The original file can be signed without editing the document. Existing signatures on the document will be lost in case of saving an edited version."
msgstr "Возможно подписать исходный файл без его изменения. При сохранении изменённого документа имеющиеся в нём подписи будут утрачены."

#: editdocumentdialog.ui
msgctxt ""
"editdocumentdialog.ui\n"
"edit\n"
"label\n"
"string.text"
msgid "Edit Document"
msgstr "Редактировать документ"

#: editdocumentdialog.ui
msgctxt ""
"editdocumentdialog.ui\n"
"cancel\n"
"label\n"
"string.text"
msgid "Cancel"
msgstr "Отменить"

#: editdurationdialog.ui
msgctxt ""
"editdurationdialog.ui\n"
"EditDurationDialog\n"
"title\n"
"string.text"
msgid "Edit Duration"
msgstr "Изменить длительность"

#: editdurationdialog.ui
msgctxt ""
"editdurationdialog.ui\n"
"negative\n"
"label\n"
"string.text"
msgid "_Negative"
msgstr "Отрицательная"

#: editdurationdialog.ui
msgctxt ""
"editdurationdialog.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "_Years:"
msgstr "Годы:"

#: editdurationdialog.ui
msgctxt ""
"editdurationdialog.ui\n"
"label\n"
"label\n"
"string.text"
msgid "_Months:"
msgstr "Месяцы:"

#: editdurationdialog.ui
msgctxt ""
"editdurationdialog.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "_Days:"
msgstr "Дни:"

#: editdurationdialog.ui
msgctxt ""
"editdurationdialog.ui\n"
"label4\n"
"label\n"
"string.text"
msgid "H_ours:"
msgstr "Часы:"

#: editdurationdialog.ui
msgctxt ""
"editdurationdialog.ui\n"
"label5\n"
"label\n"
"string.text"
msgid "Min_utes:"
msgstr "Минуты:"

#: editdurationdialog.ui
msgctxt ""
"editdurationdialog.ui\n"
"label6\n"
"label\n"
"string.text"
msgid "_Seconds:"
msgstr "Секунды:"

#: editdurationdialog.ui
msgctxt ""
"editdurationdialog.ui\n"
"label7\n"
"label\n"
"string.text"
msgid "Millise_conds:"
msgstr "Миллисекунды:"

#: editdurationdialog.ui
msgctxt ""
"editdurationdialog.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Duration"
msgstr "Длительность"

#: errorfindemaildialog.ui
msgctxt ""
"errorfindemaildialog.ui\n"
"ErrorFindEmailDialog\n"
"title\n"
"string.text"
msgid "No e-mail configuration"
msgstr "Электронная почта не настроена"

#: errorfindemaildialog.ui
msgctxt ""
"errorfindemaildialog.ui\n"
"ErrorFindEmailDialog\n"
"text\n"
"string.text"
msgid "%PRODUCTNAME was unable to find a working e-mail configuration."
msgstr "%PRODUCTNAME не смог обнаружить настройки электронной почты."

#: errorfindemaildialog.ui
msgctxt ""
"errorfindemaildialog.ui\n"
"ErrorFindEmailDialog\n"
"secondary_text\n"
"string.text"
msgid "Please save this document locally instead and attach it from within your e-mail client."
msgstr "Пожалуйста, сохраните документ на локальном компьютере и используйте его как вложение в клиенте электронной почты."

#: floatingrecord.ui
msgctxt ""
"floatingrecord.ui\n"
"FloatingRecord\n"
"title\n"
"string.text"
msgid "Record Macro"
msgstr "Запись макроса"

#: helpbookmarkpage.ui
msgctxt ""
"helpbookmarkpage.ui\n"
"display\n"
"label\n"
"string.text"
msgid "_Display"
msgstr "Показать"

#: helpbookmarkpage.ui
msgctxt ""
"helpbookmarkpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "_Bookmarks"
msgstr "Закладки"

#: helpcontrol.ui
msgctxt ""
"helpcontrol.ui\n"
"contents\n"
"label\n"
"string.text"
msgid "Contents"
msgstr "Содержимое"

#: helpcontrol.ui
msgctxt ""
"helpcontrol.ui\n"
"index\n"
"label\n"
"string.text"
msgid "Index"
msgstr "Указатель"

#: helpcontrol.ui
msgctxt ""
"helpcontrol.ui\n"
"find\n"
"label\n"
"string.text"
msgid "Find"
msgstr "Найти"

#: helpcontrol.ui
msgctxt ""
"helpcontrol.ui\n"
"bookmarks\n"
"label\n"
"string.text"
msgid "Bookmarks"
msgstr "Закладки"

#: helpindexpage.ui
msgctxt ""
"helpindexpage.ui\n"
"display\n"
"label\n"
"string.text"
msgid "_Display"
msgstr "Показать"

#: helpindexpage.ui
msgctxt ""
"helpindexpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "_Search term"
msgstr "Искомое понятие"

#: helpsearchpage.ui
msgctxt ""
"helpsearchpage.ui\n"
"display\n"
"label\n"
"string.text"
msgid "_Display"
msgstr "Показать"

#: helpsearchpage.ui
msgctxt ""
"helpsearchpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "_Search term"
msgstr "Искомое понятие"

#: helpsearchpage.ui
msgctxt ""
"helpsearchpage.ui\n"
"completewords\n"
"label\n"
"string.text"
msgid "_Complete words only"
msgstr "Только слово целиком"

#: helpsearchpage.ui
msgctxt ""
"helpsearchpage.ui\n"
"headings\n"
"label\n"
"string.text"
msgid "Find in _headings only"
msgstr "_Искать только в заголовках"

#: inputdialog.ui
msgctxt ""
"inputdialog.ui\n"
"label\n"
"label\n"
"string.text"
msgid "Height:"
msgstr "Высота:"

#: licensedialog.ui
msgctxt ""
"licensedialog.ui\n"
"LicenseDialog\n"
"title\n"
"string.text"
msgid "Licensing and Legal information"
msgstr "Лицензирование и правовая информация"

#: licensedialog.ui
msgctxt ""
"licensedialog.ui\n"
"show\n"
"label\n"
"string.text"
msgid "_Show License"
msgstr "Показать лицензию"

#: licensedialog.ui
msgctxt ""
"licensedialog.ui\n"
"label\n"
"label\n"
"string.text"
msgid ""
"%PRODUCTNAME is made available subject to the terms of the Mozilla Public License, v. 2.0. A copy of the MPL can be obtained at http://mozilla.org/MPL/2.0/.\n"
"\n"
"Third Party Code Additional copyright notices and license terms applicable to portions of the Software are set forth in the LICENSE.html file; choose Show License to see exact details in English.\n"
"\n"
"All trademarks and registered trademarks mentioned herein are the property of their respective owners.\n"
"\n"
"Copyright © 2000–2017 LibreOffice contributors. All rights reserved.\n"
"\n"
"This product was created by %OOOVENDOR, based on OpenOffice.org, which is Copyright 2000, 2011 Oracle and/or its affiliates. %OOOVENDOR acknowledges all community members, please see http://www.libreoffice.org/ for more details."
msgstr ""
"%PRODUCTNAME доступен в соответствии с условиями Mozilla Public License версии 2.0. Копию лицензии MPL можно найти на http://mozilla.org/MPL/2.0/.\n"
"\n"
"Дополнительные уведомления об авторских правах и условия лицензии на код третьих лиц, применяемые к отдельным частям программного обеспечения, изложены в файле LICENSE.html; выберите Показать лицензию для просмотра подробностей на английском языке.\n"
"\n"
"Все товарные знаки, упомянутые здесь, являются собственностью их владельцев.\n"
"\n"
"Copyright © 2000-2017 разработчики LibreOffice. Все права защищены.\n"
"\n"
"Этот продукт был создан %OOOVENDOR на основе OpenOffice.org, Copyright 2000, 2011 Oracle и/или её аффилированные лица. %OOOVENDOR признателен всем участникам сообщества, подробности смотрите по адресу http://www.libreoffice.org/ ."

#: linkeditdialog.ui
msgctxt ""
"linkeditdialog.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "_Application:"
msgstr "Приложение:"

#: linkeditdialog.ui
msgctxt ""
"linkeditdialog.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "_File:"
msgstr "Файл:"

#: linkeditdialog.ui
msgctxt ""
"linkeditdialog.ui\n"
"label4\n"
"label\n"
"string.text"
msgid "_Category:"
msgstr "Категория:"

#: linkeditdialog.ui
msgctxt ""
"linkeditdialog.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Modify Link"
msgstr "Изменить ссылку"

#: loadtemplatedialog.ui
msgctxt ""
"loadtemplatedialog.ui\n"
"LoadTemplateDialog\n"
"title\n"
"string.text"
msgid "New"
msgstr "Создать"

#: loadtemplatedialog.ui
msgctxt ""
"loadtemplatedialog.ui\n"
"fromfile\n"
"label\n"
"string.text"
msgid "From File..."
msgstr "Из файла..."

#: loadtemplatedialog.ui
msgctxt ""
"loadtemplatedialog.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Categories"
msgstr "Категории"

#: loadtemplatedialog.ui
msgctxt ""
"loadtemplatedialog.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Templates"
msgstr "Шаблоны"

#: loadtemplatedialog.ui
msgctxt ""
"loadtemplatedialog.ui\n"
"text\n"
"label\n"
"string.text"
msgid "Te_xt"
msgstr "Текст"

#: loadtemplatedialog.ui
msgctxt ""
"loadtemplatedialog.ui\n"
"frame\n"
"label\n"
"string.text"
msgid "_Frame"
msgstr "Врезки"

#: loadtemplatedialog.ui
msgctxt ""
"loadtemplatedialog.ui\n"
"pages\n"
"label\n"
"string.text"
msgid "_Pages"
msgstr "Страницы"

#: loadtemplatedialog.ui
msgctxt ""
"loadtemplatedialog.ui\n"
"numbering\n"
"label\n"
"string.text"
msgid "N_umbering"
msgstr "Нумерация"

#: loadtemplatedialog.ui
msgctxt ""
"loadtemplatedialog.ui\n"
"overwrite\n"
"label\n"
"string.text"
msgid "_Overwrite"
msgstr "Перезаписать"

#: loadtemplatedialog.ui
msgctxt ""
"loadtemplatedialog.ui\n"
"alttitle\n"
"label\n"
"string.text"
msgid "Load Styles"
msgstr "Загрузить стили"

#: loadtemplatedialog.ui
msgctxt ""
"loadtemplatedialog.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "Pre_view"
msgstr "Просмотр"

#: managestylepage.ui
msgctxt ""
"managestylepage.ui\n"
"nameft\n"
"label\n"
"string.text"
msgid "_Name:"
msgstr "Имя:"

#: managestylepage.ui
msgctxt ""
"managestylepage.ui\n"
"nextstyleft\n"
"label\n"
"string.text"
msgid "Ne_xt style:"
msgstr "Следующий стиль:"

#: managestylepage.ui
msgctxt ""
"managestylepage.ui\n"
"linkedwithft\n"
"label\n"
"string.text"
msgid "Inherit from:"
msgstr "Наследует из:"

#: managestylepage.ui
msgctxt ""
"managestylepage.ui\n"
"categoryft\n"
"label\n"
"string.text"
msgid "_Category:"
msgstr "Категория:"

#: managestylepage.ui
msgctxt ""
"managestylepage.ui\n"
"editstyle\n"
"label\n"
"string.text"
msgid "Edit Style"
msgstr "Изменить стиль"

#: managestylepage.ui
msgctxt ""
"managestylepage.ui\n"
"editlinkstyle\n"
"label\n"
"string.text"
msgid "Edit Style"
msgstr "Изменить стиль"

#: managestylepage.ui
msgctxt ""
"managestylepage.ui\n"
"autoupdate\n"
"label\n"
"string.text"
msgid "_AutoUpdate"
msgstr "Автообновление"

#: managestylepage.ui
msgctxt ""
"managestylepage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Style"
msgstr "Стиль"

#: managestylepage.ui
msgctxt ""
"managestylepage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Contains"
msgstr "Параметры"

#: newstyle.ui
msgctxt ""
"newstyle.ui\n"
"CreateStyleDialog\n"
"title\n"
"string.text"
msgid "Create Style"
msgstr "Создать стиль"

#: newstyle.ui
msgctxt ""
"newstyle.ui\n"
"stylename-atkobject\n"
"AtkObject::accessible-name\n"
"string.text"
msgid "Style Name"
msgstr "Имя стиля"

#: newstyle.ui
msgctxt ""
"newstyle.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Style Name"
msgstr "Имя стиля"

#: notebookbar.ui
msgctxt ""
"notebookbar.ui\n"
"label9\n"
"label\n"
"string.text"
msgid "File"
msgstr "Файл"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"printer\n"
"label\n"
"string.text"
msgid "_Printer"
msgstr "Принтера"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"file\n"
"label\n"
"string.text"
msgid "Print to _file"
msgstr "Печати в файл"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"label4\n"
"label\n"
"string.text"
msgid "Settings for:"
msgstr "Настройки для:"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"reducetrans\n"
"label\n"
"string.text"
msgid "_Reduce transparency"
msgstr "Сокра_тить прозрачность"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"reducegrad\n"
"label\n"
"string.text"
msgid "Reduce _gradient"
msgstr "Сократить _градиент"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"converttogray\n"
"label\n"
"string.text"
msgid "Con_vert colors to grayscale"
msgstr "Преобразовать цвета в оттенки серого"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"reducetransauto\n"
"label\n"
"string.text"
msgid "Auto_matically"
msgstr "Автоматически"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"reducetransnone\n"
"label\n"
"string.text"
msgid "_No transparency"
msgstr "_Без прозрачности"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"reducegradstripes\n"
"label\n"
"string.text"
msgid "Gradient _stripes:"
msgstr "Градиентные полосы:"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"reducegradcolor\n"
"label\n"
"string.text"
msgid "Intermediate _color"
msgstr "Промежуточный цвет"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"reducebitmapoptimal\n"
"label\n"
"string.text"
msgid "_High print quality"
msgstr "_Высокое качество печати"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"reducebitmapnormal\n"
"label\n"
"string.text"
msgid "N_ormal print quality"
msgstr "Нормальное качество печати"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"reducebitmapresol\n"
"label\n"
"string.text"
msgid "Reso_lution:"
msgstr "Разрешение:"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"reducebitmapdpi\n"
"0\n"
"stringlist.text"
msgid "72 DPI"
msgstr "72 dpi"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"reducebitmapdpi\n"
"1\n"
"stringlist.text"
msgid "96 DPI"
msgstr "96 dpi"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"reducebitmapdpi\n"
"2\n"
"stringlist.text"
msgid "150 DPI (Fax)"
msgstr "150 dpi (факс)"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"reducebitmapdpi\n"
"3\n"
"stringlist.text"
msgid "200 DPI (default)"
msgstr "200 dpi (по умолчанию)"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"reducebitmapdpi\n"
"4\n"
"stringlist.text"
msgid "300 DPI"
msgstr "300 dpi"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"reducebitmapdpi\n"
"5\n"
"stringlist.text"
msgid "600 DPI"
msgstr "600 dpi"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"reducebitmaptrans\n"
"label\n"
"string.text"
msgid "Include transparent objects"
msgstr "Включить прозрачные объекты"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"reducebitmap\n"
"label\n"
"string.text"
msgid "Reduce _bitmaps"
msgstr "Сократить растровые _изображения"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"pdf\n"
"label\n"
"string.text"
msgid "_PDF as standard print job format"
msgstr "_Задания печати в формате PDF"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Reduce Print Data"
msgstr "Сократить данные печати"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"papersize\n"
"label\n"
"string.text"
msgid "P_aper size"
msgstr "Размер бумаги"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"paperorient\n"
"label\n"
"string.text"
msgid "Pap_er orientation"
msgstr "Ориентация бумаги"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"trans\n"
"label\n"
"string.text"
msgid "_Transparency"
msgstr "Прозрачность"

#: optprintpage.ui
msgctxt ""
"optprintpage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Printer Warnings"
msgstr "Предупреждения принтера"

#: password.ui
msgctxt ""
"password.ui\n"
"PasswordDialog\n"
"title\n"
"string.text"
msgid "Enter Password"
msgstr "Введите пароль"

#: password.ui
msgctxt ""
"password.ui\n"
"userft\n"
"label\n"
"string.text"
msgid "User:"
msgstr "Пользователь:"

#: password.ui
msgctxt ""
"password.ui\n"
"pass1ft\n"
"label\n"
"string.text"
msgid "Password:"
msgstr "Пароль:"

#: password.ui
msgctxt ""
"password.ui\n"
"confirm1ft\n"
"label\n"
"string.text"
msgid "Confirm:"
msgstr "Подтвердить:"

#: password.ui
msgctxt ""
"password.ui\n"
"pass1ed-atkobject\n"
"AtkObject::accessible-name\n"
"string.text"
msgid "Password"
msgstr "Пароль"

#: password.ui
msgctxt ""
"password.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Password"
msgstr "Пароль"

#: password.ui
msgctxt ""
"password.ui\n"
"pass2ft\n"
"label\n"
"string.text"
msgid "Password:"
msgstr "Пароль:"

#: password.ui
msgctxt ""
"password.ui\n"
"confirm2ft\n"
"label\n"
"string.text"
msgid "Confirm:"
msgstr "Подтвердить:"

#: password.ui
msgctxt ""
"password.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Second Password"
msgstr "Второй пароль"

#: printeroptionsdialog.ui
msgctxt ""
"printeroptionsdialog.ui\n"
"PrinterOptionsDialog\n"
"title\n"
"string.text"
msgid "Printer Options"
msgstr "Параметры печати"

#: querysavedialog.ui
msgctxt ""
"querysavedialog.ui\n"
"QuerySaveDialog\n"
"title\n"
"string.text"
msgid "Save Document?"
msgstr "Сохранить документ?"

#: querysavedialog.ui
msgctxt ""
"querysavedialog.ui\n"
"QuerySaveDialog\n"
"text\n"
"string.text"
msgid "Save changes to document “$(DOC)” before closing?"
msgstr "Сохранить изменения документа «$(DOC)» перед закрытием?"

#: querysavedialog.ui
msgctxt ""
"querysavedialog.ui\n"
"QuerySaveDialog\n"
"secondary_text\n"
"string.text"
msgid "Your changes will be lost if you don’t save them."
msgstr "Изменения будут утеряны, если их не сохранить."

#: querysavedialog.ui
msgctxt ""
"querysavedialog.ui\n"
"discard\n"
"label\n"
"string.text"
msgid "_Don’t Save"
msgstr "_Не сохранять"

#: safemodequerydialog.ui
msgctxt ""
"safemodequerydialog.ui\n"
"SafeModeQueryDialog\n"
"title\n"
"string.text"
msgid "Enter Safe Mode"
msgstr "Безопасный режим"

#: safemodequerydialog.ui
msgctxt ""
"safemodequerydialog.ui\n"
"restart\n"
"label\n"
"string.text"
msgid "_Restart"
msgstr "Перезапустить"

#: safemodequerydialog.ui
msgctxt ""
"safemodequerydialog.ui\n"
"label\n"
"label\n"
"string.text"
msgid "Are you sure you want to restart %PRODUCTNAME and enter the Safe Mode?"
msgstr "Действительно перезапустить %PRODUCTNAME в безопасном режиме?"

#: saveastemplatedlg.ui
msgctxt ""
"saveastemplatedlg.ui\n"
"SaveAsTemplateDialog\n"
"title\n"
"string.text"
msgid "Save As Template"
msgstr "Сохранить как шаблон"

#: saveastemplatedlg.ui
msgctxt ""
"saveastemplatedlg.ui\n"
"create_label\n"
"label\n"
"string.text"
msgid "Template _Name"
msgstr "Имя шаблона"

#: saveastemplatedlg.ui
msgctxt ""
"saveastemplatedlg.ui\n"
"select_label\n"
"label\n"
"string.text"
msgid "Template _Category"
msgstr "Категория шаблона"

#: saveastemplatedlg.ui
msgctxt ""
"saveastemplatedlg.ui\n"
"defaultcb\n"
"label\n"
"string.text"
msgid "_Set as default template"
msgstr "Установить шаблоном по умолчанию"

#: saveastemplatedlg.ui
msgctxt ""
"saveastemplatedlg.ui\n"
"categorylist\n"
"0\n"
"stringlist.text"
msgid "None"
msgstr "Нет"

#: searchdialog.ui
msgctxt ""
"searchdialog.ui\n"
"SearchDialog\n"
"title\n"
"string.text"
msgid "Find on this Page"
msgstr "Найти на странице"

#: searchdialog.ui
msgctxt ""
"searchdialog.ui\n"
"search\n"
"label\n"
"string.text"
msgid "_Find"
msgstr "_Поиск"

#: searchdialog.ui
msgctxt ""
"searchdialog.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "_Search for:"
msgstr "Найти:"

#: searchdialog.ui
msgctxt ""
"searchdialog.ui\n"
"matchcase\n"
"label\n"
"string.text"
msgid "Ma_tch case"
msgstr "Учитывать регистр"

#: searchdialog.ui
msgctxt ""
"searchdialog.ui\n"
"wholewords\n"
"label\n"
"string.text"
msgid "Whole wor_ds only"
msgstr "Слово целиком"

#: searchdialog.ui
msgctxt ""
"searchdialog.ui\n"
"backwards\n"
"label\n"
"string.text"
msgid "Bac_kwards"
msgstr "В обратном порядке"

#: searchdialog.ui
msgctxt ""
"searchdialog.ui\n"
"wrap\n"
"label\n"
"string.text"
msgid "Wrap _around"
msgstr "Весь документ"

#: securityinfopage.ui
msgctxt ""
"securityinfopage.ui\n"
"readonly\n"
"label\n"
"string.text"
msgid "_Open file read-only"
msgstr "Открывать только для чтения"

#: securityinfopage.ui
msgctxt ""
"securityinfopage.ui\n"
"recordchanges\n"
"label\n"
"string.text"
msgid "Record _changes"
msgstr "Записывать изменения"

#: securityinfopage.ui
msgctxt ""
"securityinfopage.ui\n"
"protect\n"
"label\n"
"string.text"
msgid "Protect..."
msgstr "Защитить..."

#: securityinfopage.ui
msgctxt ""
"securityinfopage.ui\n"
"unprotect\n"
"label\n"
"string.text"
msgid "_Unprotect..."
msgstr "Снять защиту..."

#: securityinfopage.ui
msgctxt ""
"securityinfopage.ui\n"
"label47\n"
"label\n"
"string.text"
msgid "File Sharing Options"
msgstr "Настройки совместного использования файла"

#: startcenter.ui
msgctxt ""
"startcenter.ui\n"
"clear_all\n"
"label\n"
"string.text"
msgid "Clear Recent Documents"
msgstr "Очистить недавние документы"

#: startcenter.ui
msgctxt ""
"startcenter.ui\n"
"filter_writer\n"
"label\n"
"string.text"
msgid "Writer Templates"
msgstr "Шаблоны Writer"

#: startcenter.ui
msgctxt ""
"startcenter.ui\n"
"filter_calc\n"
"label\n"
"string.text"
msgid "Calc Templates"
msgstr "Шаблоны Calc"

#: startcenter.ui
msgctxt ""
"startcenter.ui\n"
"filter_impress\n"
"label\n"
"string.text"
msgid "Impress Templates"
msgstr "Шаблоны Impress"

#: startcenter.ui
msgctxt ""
"startcenter.ui\n"
"filter_draw\n"
"label\n"
"string.text"
msgid "Draw Templates"
msgstr "Шаблоны Draw"

#: startcenter.ui
msgctxt ""
"startcenter.ui\n"
"manage\n"
"label\n"
"string.text"
msgid "Manage Templates"
msgstr "Управление шаблонами"

#: startcenter.ui
msgctxt ""
"startcenter.ui\n"
"open_all\n"
"label\n"
"string.text"
msgid "_Open File"
msgstr "_Открыть файл"

#: startcenter.ui
msgctxt ""
"startcenter.ui\n"
"open_remote\n"
"label\n"
"string.text"
msgid "Remote File_s"
msgstr "Файлы на сервере"

#: startcenter.ui
msgctxt ""
"startcenter.ui\n"
"open_recent\n"
"label\n"
"string.text"
msgid "_Recent Files"
msgstr "Недавние файлы"

#: startcenter.ui
msgctxt ""
"startcenter.ui\n"
"templates_all\n"
"label\n"
"string.text"
msgid "T_emplates"
msgstr "_Шаблоны"

#: startcenter.ui
msgctxt ""
"startcenter.ui\n"
"create_label\n"
"label\n"
"string.text"
msgid "Create:"
msgstr "Создать:"

#: startcenter.ui
msgctxt ""
"startcenter.ui\n"
"writer_all\n"
"label\n"
"string.text"
msgid "_Writer Document"
msgstr "_Документ Writer"

#: startcenter.ui
msgctxt ""
"startcenter.ui\n"
"calc_all\n"
"label\n"
"string.text"
msgid "_Calc Spreadsheet"
msgstr "_Таблицу Calc"

#: startcenter.ui
msgctxt ""
"startcenter.ui\n"
"impress_all\n"
"label\n"
"string.text"
msgid "_Impress Presentation"
msgstr "_Презентацию Impress"

#: startcenter.ui
msgctxt ""
"startcenter.ui\n"
"draw_all\n"
"label\n"
"string.text"
msgid "_Draw Drawing"
msgstr "_Рисунок Draw"

#: startcenter.ui
msgctxt ""
"startcenter.ui\n"
"math_all\n"
"label\n"
"string.text"
msgid "_Math Formula"
msgstr "_Формулу Math"

#: startcenter.ui
msgctxt ""
"startcenter.ui\n"
"database_all\n"
"label\n"
"string.text"
msgid "_Base Database"
msgstr "_Базу данных Base"

#: startcenter.ui
msgctxt ""
"startcenter.ui\n"
"althelplabel\n"
"label\n"
"string.text"
msgid "He_lp"
msgstr "Справка"

#: startcenter.ui
msgctxt ""
"startcenter.ui\n"
"extensions\n"
"label\n"
"string.text"
msgid "E_xtensions"
msgstr "Расшир_ения"

#: startcenter.ui
msgctxt ""
"startcenter.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Application"
msgstr "Приложение"

#: startcenter.ui
msgctxt ""
"startcenter.ui\n"
"all_recent_label\n"
"label\n"
"string.text"
msgid "Recent Files List"
msgstr "Недавние файлы"

#: startcenter.ui
msgctxt ""
"startcenter.ui\n"
"local_view_label\n"
"label\n"
"string.text"
msgid "Templates List"
msgstr "Список шаблонов"

#: templatecategorydlg.ui
msgctxt ""
"templatecategorydlg.ui\n"
"TemplatesCategoryDialog\n"
"title\n"
"string.text"
msgid "Select Category"
msgstr "Выбор категории"

#: templatecategorydlg.ui
msgctxt ""
"templatecategorydlg.ui\n"
"select_label\n"
"label\n"
"string.text"
msgid "Select from Existing Category"
msgstr "Выбрать существующую категорию"

#: templatecategorydlg.ui
msgctxt ""
"templatecategorydlg.ui\n"
"create_label\n"
"label\n"
"string.text"
msgid "or Create a New Category"
msgstr "или создать новую"

#: templatecategorydlg.ui
msgctxt ""
"templatecategorydlg.ui\n"
"categorylist\n"
"0\n"
"stringlist.text"
msgid "None"
msgstr "Нет"

#: templatedlg.ui
msgctxt ""
"templatedlg.ui\n"
"TemplateDialog\n"
"title\n"
"string.text"
msgid "Templates"
msgstr "Шаблоны"

#: templatedlg.ui
msgctxt ""
"templatedlg.ui\n"
"search_filter\n"
"tooltip_text\n"
"string.text"
msgid "Search"
msgstr "Поиск"

#: templatedlg.ui
msgctxt ""
"templatedlg.ui\n"
"search_filter\n"
"placeholder_text\n"
"string.text"
msgid "Search..."
msgstr "Поиск…"

#: templatedlg.ui
msgctxt ""
"templatedlg.ui\n"
"filter_application\n"
"tooltip_text\n"
"string.text"
msgid "Filter by Application"
msgstr "Фильтр по приложению"

#: templatedlg.ui
msgctxt ""
"templatedlg.ui\n"
"filter_folder\n"
"tooltip_text\n"
"string.text"
msgid "Filter by Category"
msgstr "Фильтр по категории"

#: templatedlg.ui
msgctxt ""
"templatedlg.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Filter"
msgstr "Фильтр"

#: templatedlg.ui
msgctxt ""
"templatedlg.ui\n"
"thumbnailviewlabel\n"
"label\n"
"string.text"
msgid "Template List"
msgstr "Список шаблонов"

#: templatedlg.ui
msgctxt ""
"templatedlg.ui\n"
"action_menu\n"
"tooltip_text\n"
"string.text"
msgid "Settings"
msgstr "Настройки"

#: templatedlg.ui
msgctxt ""
"templatedlg.ui\n"
"online_link\n"
"tooltip_text\n"
"string.text"
msgid "Browse online templates"
msgstr "Обзор шаблонов онлайн"

#: templatedlg.ui
msgctxt ""
"templatedlg.ui\n"
"hidedialogcb\n"
"label\n"
"string.text"
msgid "Show this dialog at startup"
msgstr "Показывать при запуске"

#: templatedlg.ui
msgctxt ""
"templatedlg.ui\n"
"move_btn\n"
"label\n"
"string.text"
msgid "Move"
msgstr "Переместить"

#: templatedlg.ui
msgctxt ""
"templatedlg.ui\n"
"move_btn\n"
"tooltip_text\n"
"string.text"
msgid "Move Templates"
msgstr "Переместить шаблоны"

#: templatedlg.ui
msgctxt ""
"templatedlg.ui\n"
"export_btn\n"
"label\n"
"string.text"
msgid "Export"
msgstr "Экспорт"

#: templatedlg.ui
msgctxt ""
"templatedlg.ui\n"
"export_btn\n"
"tooltip_text\n"
"string.text"
msgid "Export Templates"
msgstr "Экспорт шаблонов"

#: templatedlg.ui
msgctxt ""
"templatedlg.ui\n"
"import_btn\n"
"label\n"
"string.text"
msgid "Import"
msgstr "Импорт"

#: templatedlg.ui
msgctxt ""
"templatedlg.ui\n"
"import_btn\n"
"tooltip_text\n"
"string.text"
msgid "Import Templates"
msgstr "Импорт шаблонов"

#: templatedlg.ui
msgctxt ""
"templatedlg.ui\n"
"applist\n"
"0\n"
"stringlist.text"
msgid "All Applications"
msgstr "Все приложения"

#: templatedlg.ui
msgctxt ""
"templatedlg.ui\n"
"applist\n"
"1\n"
"stringlist.text"
msgid "Documents"
msgstr "Документы"

#: templatedlg.ui
msgctxt ""
"templatedlg.ui\n"
"applist\n"
"2\n"
"stringlist.text"
msgid "Spreadsheets"
msgstr "Эл. таблицы"

#: templatedlg.ui
msgctxt ""
"templatedlg.ui\n"
"applist\n"
"3\n"
"stringlist.text"
msgid "Presentations"
msgstr "Презентации"

#: templatedlg.ui
msgctxt ""
"templatedlg.ui\n"
"applist\n"
"4\n"
"stringlist.text"
msgid "Drawings"
msgstr "Рисунки"

#: templatedlg.ui
msgctxt ""
"templatedlg.ui\n"
"folderlist\n"
"0\n"
"stringlist.text"
msgid "All Categories"
msgstr "Все категории"

#: versioncommentdialog.ui
msgctxt ""
"versioncommentdialog.ui\n"
"VersionCommentDialog\n"
"title\n"
"string.text"
msgid "Insert Version Comment"
msgstr "Вставить комментарий к версии"

#: versioncommentdialog.ui
msgctxt ""
"versioncommentdialog.ui\n"
"timestamp\n"
"label\n"
"string.text"
msgid "Date and time: "
msgstr "Дата и время: "

#: versioncommentdialog.ui
msgctxt ""
"versioncommentdialog.ui\n"
"author\n"
"label\n"
"string.text"
msgid "Saved by: "
msgstr "Сохранено: "

#: versionscmis.ui
msgctxt ""
"versionscmis.ui\n"
"show\n"
"label\n"
"string.text"
msgid "_Show..."
msgstr "Показать..."

#: versionscmis.ui
msgctxt ""
"versionscmis.ui\n"
"compare\n"
"label\n"
"string.text"
msgid "_Compare"
msgstr "Сравнить"

#: versionscmis.ui
msgctxt ""
"versionscmis.ui\n"
"datetime\n"
"label\n"
"string.text"
msgid "Date and time"
msgstr "Дата и время"

#: versionscmis.ui
msgctxt ""
"versionscmis.ui\n"
"savedby\n"
"label\n"
"string.text"
msgid "Saved by"
msgstr "Сохранено"

#: versionscmis.ui
msgctxt ""
"versionscmis.ui\n"
"comments\n"
"label\n"
"string.text"
msgid "Comments"
msgstr "Комментарии"

#: versionscmis.ui
msgctxt ""
"versionscmis.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Existing Versions"
msgstr "Имеющиеся версии"

#: versionsofdialog.ui
msgctxt ""
"versionsofdialog.ui\n"
"show\n"
"label\n"
"string.text"
msgid "_Show..."
msgstr "Показать..."

#: versionsofdialog.ui
msgctxt ""
"versionsofdialog.ui\n"
"compare\n"
"label\n"
"string.text"
msgid "_Compare"
msgstr "Сравнить"

#: versionsofdialog.ui
msgctxt ""
"versionsofdialog.ui\n"
"cmis\n"
"label\n"
"string.text"
msgid "CMIS"
msgstr "CMIS"

#: versionsofdialog.ui
msgctxt ""
"versionsofdialog.ui\n"
"save\n"
"label\n"
"string.text"
msgid "Save _New Version"
msgstr "Сохранить новую версию"

#: versionsofdialog.ui
msgctxt ""
"versionsofdialog.ui\n"
"always\n"
"label\n"
"string.text"
msgid "_Always save a new version on closing"
msgstr "Всегда сохранять новую версию при закрытии"

#: versionsofdialog.ui
msgctxt ""
"versionsofdialog.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "New Versions"
msgstr "Новые версии"

#: versionsofdialog.ui
msgctxt ""
"versionsofdialog.ui\n"
"datetime\n"
"label\n"
"string.text"
msgid "Date and time"
msgstr "Дата и время"

#: versionsofdialog.ui
msgctxt ""
"versionsofdialog.ui\n"
"savedby\n"
"label\n"
"string.text"
msgid "Saved by"
msgstr "Сохранено"

#: versionsofdialog.ui
msgctxt ""
"versionsofdialog.ui\n"
"comments\n"
"label\n"
"string.text"
msgid "Comments"
msgstr "Комментарии"

#: versionsofdialog.ui
msgctxt ""
"versionsofdialog.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Existing Versions"
msgstr "Имеющиеся версии"

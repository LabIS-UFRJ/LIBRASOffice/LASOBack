#. extracted from reportdesign/source/core/resource
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.freedesktop.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-05-28 18:08+0200\n"
"PO-Revision-Date: 2011-10-22 23:06+0000\n"
"Last-Translator: Andras <timar74@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: bn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1319324817.0\n"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_DETAIL\n"
"string.text"
msgid "Detail"
msgstr "বিস্তারিত"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_PAGE_HEADER\n"
"string.text"
msgid "Page Header"
msgstr "পৃষ্ঠার শীর্ষচরণ"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_PAGE_FOOTER\n"
"string.text"
msgid "Page Footer"
msgstr "পৃষ্ঠার পাদচরণ"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_GROUP_HEADER\n"
"string.text"
msgid "Group Header"
msgstr "গ্রুপের শীর্ষচরণ"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_GROUP_FOOTER\n"
"string.text"
msgid "Group Footer"
msgstr "গ্রুপের পাদচরণ"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_REPORT_HEADER\n"
"string.text"
msgid "Report Header"
msgstr "প্রতিবেদন শীর্ষচরণ"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_REPORT_FOOTER\n"
"string.text"
msgid "Report Footer"
msgstr "প্রতিবেদন পাদচরণ"

#: strings.src
#, fuzzy
msgctxt ""
"strings.src\n"
"RID_STR_PROPERTY_CHANGE_NOT_ALLOWED\n"
"string.text"
msgid "The name '#1' already exists and cannot be assigned again."
msgstr "'#1' নামটি ইতিমধ্যেই বিদ্যমান এবং পুনরায় বরাদ্দ করা যাবে না।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_ERROR_WRONG_ARGUMENT\n"
"string.text"
msgid "You tried to set an illegal argument. Please have a look at '#1' for valid arguments."
msgstr "আপনি একটি অবৈধ আর্গুমেন্ট নির্ধারণ করার চেষ্টা করেছেন। অনুগ্রহ করে কার্যকর আর্গুমেন্টের জন্য '#1' দেখুন।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_ARGUMENT_IS_NULL\n"
"string.text"
msgid "The element is invalid."
msgstr "উপাদানটি অকার্যকর।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_FIXEDTEXT\n"
"string.text"
msgid "Label field"
msgstr "লেবেলের ক্ষেত্র"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_FORMATTEDFIELD\n"
"string.text"
msgid "Formatted field"
msgstr "বিন্যাসিত ক্ষেত্র"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_IMAGECONTROL\n"
"string.text"
msgid "Image control"
msgstr "চিত্র কন্ট্রোল"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_REPORT\n"
"string.text"
msgid "Report"
msgstr "প্রতিবেদন"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_SHAPE\n"
"string.text"
msgid "Shape"
msgstr "আকৃতি"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_FIXEDLINE\n"
"string.text"
msgid "Fixed line"
msgstr "নির্ধারিত লাইন"

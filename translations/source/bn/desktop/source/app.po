#. extracted from desktop/source/app
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-04-22 23:40+0200\n"
"PO-Revision-Date: 2015-05-12 00:14+0000\n"
"Last-Translator: system user <>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: bn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 2.7\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1431389697.000000\n"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_RECOVER_QUERY\n"
"string.text"
msgid "Should the file \"$1\" be restored?"
msgstr "\"$1\" ফাইলটি পুনরুদ্ধার করা হবে কি?"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_RECOVER_TITLE\n"
"string.text"
msgid "File Recovery"
msgstr "ফাইল পুনরুদ্ধার"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_CANNOT_START\n"
"string.text"
msgid "The application cannot be started. "
msgstr "অ্যাপ্লিকেশনটি আরম্ভ করা যাচ্ছে না।"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_DIR_MISSING\n"
"string.text"
msgid "The configuration directory \"$1\" could not be found."
msgstr "কনফিগারেশন ডিরেক্টরি \"$1\" খুঁজে পাওয়া যায়নি।"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_PATH_INVALID\n"
"string.text"
msgid "The installation path is invalid."
msgstr "ইনস্টলেশন পাথটি অকার্যকর।"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_NO_PATH\n"
"string.text"
msgid "The installation path is not available."
msgstr "ইনস্টলেশন পাথটি বিদ্যমান নেই।"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_INTERNAL\n"
"string.text"
msgid "An internal error occurred."
msgstr "একটি অভ্যন্তরীণ ত্রুটি ঘটেছে।"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_FILE_CORRUPT\n"
"string.text"
msgid "The configuration file \"$1\" is corrupt."
msgstr "কনফিগারেশন ফাইল \"$1\" বিকৃত হয়েছে।"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_FILE_MISSING\n"
"string.text"
msgid "The configuration file \"$1\" was not found."
msgstr "কনফিগারেশন ফাইল \"$1\" খুঁজে পাওয়া যায়নি।"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_NO_SUPPORT\n"
"string.text"
msgid "The configuration file \"$1\" does not support the current version."
msgstr "কনফিগারেশন ফাইল \"$1\" বর্তমান সংস্করণ সমর্থন করেনা।"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_LANGUAGE_MISSING\n"
"string.text"
msgid "The user interface language cannot be determined."
msgstr "ব্যবহারকারীর ইন্টারফেসের ভাষা নির্ধারণ করা যাচ্ছেনা।"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_USERINSTALL_FAILED\n"
"string.text"
msgid "User installation could not be completed. "
msgstr ""

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_NO_CFG_SERVICE\n"
"string.text"
msgid "The configuration service is not available."
msgstr "কনফিগারেশন সার্ভিস বিদ্যমান নেই।"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_MULTISESSION\n"
"string.text"
msgid "You have another instance running in a different terminal session. Close that instance and then try again."
msgstr ""

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_ASK_START_SETUP_MANUALLY\n"
"string.text"
msgid "Start the setup application to repair the installation from the CD or the folder containing the installation packages."
msgstr "সিডি অথবা ইনস্টলেশন প্যাকেজ ধারণকারী ফোল্ডার থেকে ইনস্টলেশন মেরামত করতে সেটআপ অ্যাপ্লিকেশন আরম্ভ করুন।"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_CONFIG_ERR_SETTINGS_INCOMPLETE\n"
"string.text"
msgid "The startup settings for accessing the central configuration are incomplete. "
msgstr "কেন্দ্রীয় কনফিগারেশনে প্রবেশাধিকারের প্রারম্ভিক সেটিংস অসম্পূর্ণ।"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_CONFIG_ERR_CANNOT_CONNECT\n"
"string.text"
msgid "A connection to the central configuration could not be established. "
msgstr "কেন্দ্রীয় কনফিগারেশনে কোনো সংযোগ প্রতিষ্ঠিত করা যায়নি।"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_CONFIG_ERR_RIGHTS_MISSING\n"
"string.text"
msgid "You cannot access the central configuration because of missing access rights. "
msgstr "আপনি কেন্দ্রীয় কনফিগারেশনে সন্নিবেশ করতে পারবেন না কারণ আপনি প্রবেশাধিকার হারিয়েছেন।"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_CONFIG_ERR_ACCESS_GENERAL\n"
"string.text"
msgid "A general error occurred while accessing your central configuration. "
msgstr "আপনার কেন্দ্রীয় কনফিগারেশনে সন্নিবেশ করার সময় সাধারণ একটি ত্রুটি ঘটেছে।"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_CONFIG_ERR_NO_WRITE_ACCESS\n"
"string.text"
msgid "The changes to your personal settings cannot be stored centrally because of missing access rights. "
msgstr "আপনার ব্যক্তিগত সেটিংসমূহের পরিবর্তন কেন্দ্রীয়ভাবে সংরক্ষণ করা যাবে না কারণ আপনি প্রবেশাধিকার হারিয়েছেন।"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_CFG_DATAACCESS\n"
"string.text"
msgid ""
"%PRODUCTNAME cannot be started due to an error in accessing the %PRODUCTNAME configuration data.\n"
"\n"
"Please contact your system administrator."
msgstr ""
"%PRODUCTNAME কনফিগারেশন ডাটায় প্রবেশে একটি ত্রুটির কারণে %PRODUCTNAME আরম্ভ করা যাচ্ছে না।\n"
"\n"
"অনুগ্রহ করে আপনার সিস্টেম প্রশাসকের সাথে যোগাযোগ করুন।"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_INTERNAL_ERRMSG\n"
"string.text"
msgid "The following internal error has occurred: "
msgstr "নিম্নোক্ত অভ্যন্তরীণ ত্রুটি ঘটেছে:"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_LO_MUST_BE_RESTARTED\n"
"string.text"
msgid "%PRODUCTNAME must unfortunately be manually restarted once after installation or update."
msgstr ""

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_QUERY_USERDATALOCKED\n"
"string.text"
msgid ""
"Either another instance of %PRODUCTNAME is accessing your personal settings or your personal settings are locked.\n"
"Simultaneous access can lead to inconsistencies in your personal settings. Before continuing, you should make sure user '$u' closes %PRODUCTNAME on host '$h'.\n"
"\n"
"Do you really want to continue?"
msgstr ""

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_TITLE_USERDATALOCKED\n"
"string.text"
msgid "%PRODUCTNAME %PRODUCTVERSION"
msgstr "%PRODUCTNAME %PRODUCTVERSION"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_ERR_PRINTDISABLED\n"
"string.text"
msgid "Printing is disabled. No documents can be printed."
msgstr "মুদ্রণ নিষ্ক্রিয় করা আছে। কোনো নথি মুদ্রণ করা যাবেনা।"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_TITLE_EXPIRED\n"
"string.text"
msgid "%PRODUCTNAME %PRODUCTVERSION"
msgstr "%PRODUCTNAME %PRODUCTVERSION"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_NO_PATHSET_SERVICE\n"
"string.text"
msgid "The path manager is not available.\n"
msgstr "পাথ ব্যবস্থাপক সহজলভ্য নয়।\n"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOSTRAP_ERR_NOTENOUGHDISKSPACE\n"
"string.text"
msgid ""
"%PRODUCTNAME user installation could not be completed due to insufficient free disk space. Please free more disc space at the following location and restart %PRODUCTNAME:\n"
"\n"
msgstr ""
"%PRODUCTNAME ব্যবহারকারীর ইনস্টলেশন ডিস্কে অপর্যাপ্ত ফাঁকা স্থান থাকার কারণে শেষ করা যাবে না। অনুগ্রহ করে নিম্নলিখিত অবস্থানে আরও ডিস্কের স্থান ফাঁকা করুন এবং %PRODUCTNAME পুনরায় আরম্ভ করুন:\n"
"\n"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOSTRAP_ERR_NOACCESSRIGHTS\n"
"string.text"
msgid ""
"%PRODUCTNAME user installation could not be processed due to missing access rights. Please make sure that you have sufficient access rights for the following location and restart %PRODUCTNAME:\n"
"\n"
msgstr ""
"%PRODUCTNAME ব্যবহারকারীর ইনস্টলেশন প্রবেশাধিকার না থাকার কারণে আগানো যাচ্ছে না। অনুগ্রহ করে নিশ্চিত করুন যে, নিম্নলিখিত অবস্থানের জন্য যথেষ্ট প্রবেশাধিকার আপনার রয়েছে এবং %PRODUCTNAME পুনরায় আরম্ভ করুন:\n"
"\n"

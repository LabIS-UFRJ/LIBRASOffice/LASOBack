#. extracted from reportdesign/source/ui/report
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-03-12 01:35+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: sq\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1457746503.000000\n"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_BRWTITLE_PROPERTIES\n"
"string.text"
msgid "Properties: "
msgstr "Vetitë: "

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_BRWTITLE_NO_PROPERTIES\n"
"string.text"
msgid "No Control marked"
msgstr "Nuk është zgjedhur asnjë kontroll"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_BRWTITLE_MULTISELECT\n"
"string.text"
msgid "Multiselection"
msgstr "Zgjedhje e shumëfishtë"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_PROPTITLE_IMAGECONTROL\n"
"string.text"
msgid "Image Control"
msgstr "Kontrolli i fotografisë"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_PROPTITLE_FIXEDTEXT\n"
"string.text"
msgid "Label field"
msgstr "Fushë për emërim"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_PROPTITLE_FIXEDLINE\n"
"string.text"
msgid "Line"
msgstr "Vija"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_PROPTITLE_FORMATTED\n"
"string.text"
msgid "Formatted Field"
msgstr "Fusha e Formatuar"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_PROPTITLE_SHAPE\n"
"string.text"
msgid "Shape"
msgstr "Trajtë"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_PROPTITLE_REPORT\n"
"string.text"
msgid "Report"
msgstr "Raport"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_PROPTITLE_SECTION\n"
"string.text"
msgid "Section"
msgstr "Seksion"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_PROPTITLE_FUNCTION\n"
"string.text"
msgid "Function"
msgstr "Funksioni"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_PROPTITLE_GROUP\n"
"string.text"
msgid "Group"
msgstr "Grupi"

#: report.src
msgctxt ""
"report.src\n"
"STR_SHOW_RULER\n"
"string.text"
msgid "Show ruler"
msgstr "Shfaqe vizoren"

#: report.src
msgctxt ""
"report.src\n"
"STR_SHOW_GRID\n"
"string.text"
msgid "Show grid"
msgstr "Shfaqe rrjetën"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_CHANGEPOSITION\n"
"string.text"
msgid "Change Object"
msgstr "Ndrysho objektin"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_MOVE_GROUP\n"
"string.text"
msgid "Move Group(s)"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_CONDITIONAL_FORMATTING\n"
"string.text"
msgid "Conditional Formatting"
msgstr "Formatizim i Kushtëzuar"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_REMOVE_REPORTHEADERFOOTER\n"
"string.text"
msgid "Remove report header / report footer"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_ADD_REPORTHEADERFOOTER\n"
"string.text"
msgid "Add report header / report footer"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_REMOVE_PAGEHEADERFOOTER\n"
"string.text"
msgid "Remove page header / page footer"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_ADD_PAGEHEADERFOOTER\n"
"string.text"
msgid "Add page header / page footer"
msgstr ""

#. The # character is used for replacing
#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_PROPERTY\n"
"string.text"
msgid "Change property '#'"
msgstr "Vendos vetinë '#'"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_ADD_GROUP_HEADER\n"
"string.text"
msgid "Add group header "
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_REMOVE_GROUP_HEADER\n"
"string.text"
msgid "Remove group header "
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_ADD_GROUP_FOOTER\n"
"string.text"
msgid "Add group footer "
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_REMOVE_GROUP_FOOTER\n"
"string.text"
msgid "Remove group footer "
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_ADDFUNCTION\n"
"string.text"
msgid "Add function"
msgstr "Funksion eksponencial"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_DELETEFUNCTION\n"
"string.text"
msgid "Delete function"
msgstr "Funksion eksponencial"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_DESIGN_VIEW\n"
"string.text"
msgid "Design"
msgstr "Dizajni"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_PREVIEW_VIEW\n"
"string.text"
msgid "Preview"
msgstr "Shikim paraprak"

#: report.src
msgctxt ""
"report.src\n"
"STR_RPT_TITLE\n"
"string.text"
msgid "Report #"
msgstr "Raporti #"

#: report.src
msgctxt ""
"report.src\n"
"STR_RPT_LABEL\n"
"string.text"
msgid "~Report name"
msgstr "Emri i ~raportit"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_REMOVE_GROUP\n"
"string.text"
msgid "Delete Group"
msgstr "Fshi grup volumi"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_APPEND_GROUP\n"
"string.text"
msgid "Add Group"
msgstr "Emri i grupit"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_REMOVE_SELECTION\n"
"string.text"
msgid "Delete Selection"
msgstr "Fshij përzgjedhjen"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_REMOVE_FUNCTION\n"
"string.text"
msgid "Delete Function"
msgstr "Fshij funksionin"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_CHANGE_SIZE\n"
"string.text"
msgid "Change Size"
msgstr "Change Font Size"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_PASTE\n"
"string.text"
msgid "Paste"
msgstr "Ngjit"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_INSERT_CONTROL\n"
"string.text"
msgid "Insert Control"
msgstr "Shto kontrollin"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_DELETE_CONTROL\n"
"string.text"
msgid "Delete Control"
msgstr "Shiriti i kontrollit"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_GROUP\n"
"string.text"
msgid "Insert Group"
msgstr "Shto grupin"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_UNGROUP\n"
"string.text"
msgid "Delete Group"
msgstr "Fshi grup volumi"

#. Please try to avoid spaces in the name. It is used as a programmatic one.
#: report.src
msgctxt ""
"report.src\n"
"RID_STR_GROUPHEADER\n"
"string.text"
msgid "GroupHeader"
msgstr ""

#. Please try to avoid spaces in the name. It is used as a programmatic one.
#: report.src
msgctxt ""
"report.src\n"
"RID_STR_GROUPFOOTER\n"
"string.text"
msgid "GroupFooter"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT\n"
"SID_SORTINGANDGROUPING\n"
"menuitem.text"
msgid "Sorting and Grouping..."
msgstr "Renditja dhe grupimi..."

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT\n"
"SID_ATTR_CHAR_COLOR_BACKGROUND\n"
"menuitem.text"
msgid "Background Color..."
msgstr "Ngjyra e sfondit..."

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT\n"
"SID_RULER\n"
"menuitem.text"
msgid "Ruler..."
msgstr "Vizore..."

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT\n"
"SID_GRID_VISIBLE\n"
"menuitem.text"
msgid "Grid..."
msgstr "Rrjeta..."

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT.SID_ARRANGEMENU\n"
"SID_FRAME_TO_TOP\n"
"menuitem.text"
msgid "~Bring to Front"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT.SID_ARRANGEMENU\n"
"SID_FRAME_UP\n"
"menuitem.text"
msgid "Bring ~Forward"
msgstr "Sjelle ~përpara"

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT.SID_ARRANGEMENU\n"
"SID_FRAME_DOWN\n"
"menuitem.text"
msgid "Send Back~ward"
msgstr "Dërgoje p~rapa"

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT.SID_ARRANGEMENU\n"
"SID_FRAME_TO_BOTTOM\n"
"menuitem.text"
msgid "~Send to Back"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT.SID_ARRANGEMENU\n"
"SID_OBJECT_HEAVEN\n"
"menuitem.text"
msgid "~To Foreground"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT.SID_ARRANGEMENU\n"
"SID_OBJECT_HELL\n"
"menuitem.text"
msgid "T~o Background"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT\n"
"SID_ARRANGEMENU\n"
"menuitem.text"
msgid "~Arrange"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT.SID_OBJECT_ALIGN\n"
"SID_OBJECT_ALIGN_LEFT\n"
"menuitem.text"
msgid "~Left"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT.SID_OBJECT_ALIGN\n"
"SID_OBJECT_ALIGN_CENTER\n"
"menuitem.text"
msgid "~Centered"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT.SID_OBJECT_ALIGN\n"
"SID_OBJECT_ALIGN_RIGHT\n"
"menuitem.text"
msgid "~Right"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT.SID_OBJECT_ALIGN\n"
"SID_OBJECT_ALIGN_UP\n"
"menuitem.text"
msgid "~Top"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT.SID_OBJECT_ALIGN\n"
"SID_OBJECT_ALIGN_MIDDLE\n"
"menuitem.text"
msgid "C~enter"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT.SID_OBJECT_ALIGN\n"
"SID_OBJECT_ALIGN_DOWN\n"
"menuitem.text"
msgid "~Bottom"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT\n"
"SID_OBJECT_ALIGN\n"
"menuitem.text"
msgid "A~lignment"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT.SID_OBJECT_RESIZING\n"
"SID_OBJECT_SMALLESTWIDTH\n"
"menuitem.text"
msgid "~Fit to smallest width"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT.SID_OBJECT_RESIZING\n"
"SID_OBJECT_GREATESTWIDTH\n"
"menuitem.text"
msgid "~Fit to greatest width"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT.SID_OBJECT_RESIZING\n"
"SID_OBJECT_SMALLESTHEIGHT\n"
"menuitem.text"
msgid "~Fit to smallest height"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT.SID_OBJECT_RESIZING\n"
"SID_OBJECT_GREATESTHEIGHT\n"
"menuitem.text"
msgid "~Fit to greatest height"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT\n"
"SID_OBJECT_RESIZING\n"
"menuitem.text"
msgid "~Object Resizing"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT.SID_SECTION_SHRINK_MENU\n"
"SID_SECTION_SHRINK\n"
"menuitem.text"
msgid "Shrink"
msgstr "Tkurr"

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT.SID_SECTION_SHRINK_MENU\n"
"SID_SECTION_SHRINK_TOP\n"
"menuitem.text"
msgid "Shrink from top"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT.SID_SECTION_SHRINK_MENU\n"
"SID_SECTION_SHRINK_BOTTOM\n"
"menuitem.text"
msgid "Shrink from bottom"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT\n"
"SID_SECTION_SHRINK_MENU\n"
"menuitem.text"
msgid "Section"
msgstr "Seksion"

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT\n"
"SID_DISTRIBUTION\n"
"menuitem.text"
msgid "Distribution..."
msgstr "Shpërndarje..."

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT\n"
"SID_SHOW_PROPERTYBROWSER\n"
"menuitem.text"
msgid "Properties..."
msgstr "Vetitë..."

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT\n"
"SID_CUT\n"
"menuitem.text"
msgid "Cu~t"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_MENU_REPORT\n"
"SID_COPY\n"
"menuitem.text"
msgid "~Copy"
msgstr ""

#: report.src
#, fuzzy
msgctxt ""
"report.src\n"
"RID_MENU_REPORT\n"
"SID_PASTE\n"
"menuitem.text"
msgid "~Paste"
msgstr "Ngjit"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_FIELDSELECTION\n"
"string.text"
msgid "Add field:"
msgstr "Shto fushën:"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_FILTER\n"
"string.text"
msgid "Filter"
msgstr "Filtër"

#: report.src
msgctxt ""
"report.src\n"
"RID_APP_TITLE\n"
"string.text"
msgid " - %PRODUCTNAME Base Report"
msgstr " - Raport %PRODUCTNAME Base"

#: report.src
msgctxt ""
"report.src\n"
"RID_APP_NEW_DOC\n"
"string.text"
msgid "Untitled"
msgstr "Pa titull"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_ALIGNMENT\n"
"string.text"
msgid "Change Alignment"
msgstr "Ndërro rreshtimin"

#. # will be replaced with a name.
#: report.src
msgctxt ""
"report.src\n"
"RID_STR_HEADER\n"
"string.text"
msgid "# Header"
msgstr "# Kreu"

#. # will be replaced with a name.
#: report.src
msgctxt ""
"report.src\n"
"RID_STR_FOOTER\n"
"string.text"
msgid "# Footer"
msgstr "# Fundi i faqes"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_IMPORT_GRAPHIC\n"
"string.text"
msgid "Insert graphics"
msgstr "Fut figura"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_DELETE\n"
"string.text"
msgid "Delete"
msgstr "Fshije"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_FUNCTION\n"
"string.text"
msgid "Function"
msgstr "Funksioni"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_COULD_NOT_CREATE_REPORT\n"
"string.text"
msgid "An error occurred while creating the report."
msgstr "Ndodhi një gabim duke krijuar çelësin."

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_CAUGHT_FOREIGN_EXCEPTION\n"
"string.text"
msgid "An exception of type $type$ was caught."
msgstr "Ndodhi një përjashtim i tipit $type$."

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_CHANGEFONT\n"
"string.text"
msgid "Change font"
msgstr "Ndrysho fontin"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_CHANGEPAGE\n"
"string.text"
msgid "Change page attributes"
msgstr "Ndrysho numrin e faqes"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_PAGEHEADERFOOTER_INSERT\n"
"string.text"
msgid "Insert Page Header/Footer"
msgstr "Shto ndërprerësin manual të faqes"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_PAGEHEADERFOOTER_DELETE\n"
"string.text"
msgid "Delete Page Header/Footer"
msgstr "Shto ndërprerësin manual të faqes"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_COLUMNHEADERFOOTER_INSERT\n"
"string.text"
msgid "Insert Column Header/Footer"
msgstr "Shto manualisht kornizën e kolonës së vetme"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_COLUMNHEADERFOOTER_DELETE\n"
"string.text"
msgid "Delete Column Header/Footer"
msgstr "Shto manualisht kornizën e kolonës së vetme"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_REPORTHEADERFOOTER_INSERT\n"
"string.text"
msgid "Insert Report Header/Footer"
msgstr "Shto ndërprerësin manual të faqes"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_REPORTHEADERFOOTER_DELETE\n"
"string.text"
msgid "Delete Report Header/Footer"
msgstr "Shto ndërprerësin manual të faqes"

#: report.src
msgctxt ""
"report.src\n"
"RID_ERR_NO_COMMAND\n"
"string.text"
msgid "The report can not be executed unless it is bound to content."
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_ERR_NO_OBJECTS\n"
"string.text"
msgid "The report can not be executed unless at least one object has been inserted."
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_UNDO_SHRINK\n"
"string.text"
msgid "Shrink Section"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_DETAIL\n"
"string.text"
msgid "Detail"
msgstr "Hollësi"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_PAGE_HEADER\n"
"string.text"
msgid "Page Header"
msgstr "Kreu i faqes"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_PAGE_FOOTER\n"
"string.text"
msgid "Page Footer"
msgstr ""

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_REPORT_HEADER\n"
"string.text"
msgid "Report Header"
msgstr "Zona e kryefaqes"

#: report.src
msgctxt ""
"report.src\n"
"RID_STR_REPORT_FOOTER\n"
"string.text"
msgid "Report Footer"
msgstr "Zona e fundfaqes"

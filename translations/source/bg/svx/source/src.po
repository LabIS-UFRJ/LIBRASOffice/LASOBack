#. extracted from svx/source/src
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2017-02-16 15:13+0000\n"
"Last-Translator: Mihail Balabanov <m.balabanov@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1487258004.000000\n"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRCTX\n"
"ERRCTX_ERROR\n"
"string.text"
msgid "Error"
msgstr "Грешка"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRCTX\n"
"ERRCTX_WARNING\n"
"string.text"
msgid "Warning"
msgstr "Предупреждение"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRCTX\n"
"ERRCTX_SFX_LOADTEMPLATE\n"
"string.text"
msgid "$(ERR) loading the template $(ARG1)"
msgstr "$(ERR) при зареждане на шаблона $(ARG1)"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRCTX\n"
"ERRCTX_SFX_SAVEDOC\n"
"string.text"
msgid "$(ERR) saving the document $(ARG1)"
msgstr "$(ERR) при записване на документа $(ARG1)"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRCTX\n"
"ERRCTX_SFX_SAVEASDOC\n"
"string.text"
msgid "$(ERR) saving the document $(ARG1)"
msgstr "$(ERR) при записване на документа $(ARG1)"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRCTX\n"
"ERRCTX_SFX_DOCINFO\n"
"string.text"
msgid "$(ERR) displaying doc. information for document $(ARG1)"
msgstr "$(ERR) при показване на свойствата на документа $(ARG1)"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRCTX\n"
"ERRCTX_SFX_DOCTEMPLATE\n"
"string.text"
msgid "$(ERR) writing document $(ARG1) as template"
msgstr "$(ERR) при записване на документа $(ARG1) като шаблон"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRCTX\n"
"ERRCTX_SFX_MOVEORCOPYCONTENTS\n"
"string.text"
msgid "$(ERR) copying or moving document contents"
msgstr "$(ERR) при копиране или местене на съдържание на документ"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRCTX\n"
"ERRCTX_SFX_DOCMANAGER\n"
"string.text"
msgid "$(ERR) starting the Document Manager"
msgstr "$(ERR) при стартиране на диспечера за документи"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRCTX\n"
"ERRCTX_SFX_OPENDOC\n"
"string.text"
msgid "$(ERR) loading document $(ARG1)"
msgstr "$(ERR) при зареждане на документа $(ARG1)"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRCTX\n"
"ERRCTX_SFX_NEWDOCDIRECT\n"
"string.text"
msgid "$(ERR) creating a new document"
msgstr "$(ERR) при създаване на нов документ"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRCTX\n"
"ERRCTX_SFX_NEWDOC\n"
"string.text"
msgid "$(ERR) creating a new document"
msgstr "$(ERR) при създаване на нов документ"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRCTX\n"
"ERRCTX_SFX_CREATEOBJSH\n"
"string.text"
msgid "$(ERR) expanding entry"
msgstr "$(ERR) при разширяване на запис"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRCTX\n"
"ERRCTX_SFX_LOADBASIC\n"
"string.text"
msgid "$(ERR) loading BASIC of document $(ARG1)"
msgstr "$(ERR) при зареждане на код на BASIC от документа $(ARG1)"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRCTX\n"
"ERRCTX_SFX_SEARCHADDRESS\n"
"string.text"
msgid "$(ERR) searching for an address"
msgstr "$(ERR) при търсене на адрес"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_CLASS_ABORT\n"
"string.text"
msgid "Abort"
msgstr "Прекратяване"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_CLASS_NOTEXISTS\n"
"string.text"
msgid "Nonexistent object"
msgstr "Несъществуващ обект"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_CLASS_ALREADYEXISTS\n"
"string.text"
msgid "Object already exists"
msgstr "Обектът вече съществува"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_CLASS_ACCESS\n"
"string.text"
msgid "Object not accessible"
msgstr "Обектът е недостъпен"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_CLASS_PATH\n"
"string.text"
msgid "Inadmissible path"
msgstr "Недопустим път"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_CLASS_LOCKING\n"
"string.text"
msgid "Locking problem"
msgstr "Проблем със заключването"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_CLASS_PARAMETER\n"
"string.text"
msgid "Wrong parameter"
msgstr "Грешен параметър"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_CLASS_SPACE\n"
"string.text"
msgid "Resource exhausted"
msgstr "Недостиг на ресурс"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_CLASS_NOTSUPPORTED\n"
"string.text"
msgid "Action not supported"
msgstr "Действието не се поддържа"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_CLASS_READ\n"
"string.text"
msgid "Read Error"
msgstr "Грешка при четене"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_CLASS_WRITE\n"
"string.text"
msgid "Write Error"
msgstr "Грешка при запис"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_CLASS_UNKNOWN\n"
"string.text"
msgid "unknown"
msgstr "неизвестно"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_CLASS_VERSION\n"
"string.text"
msgid "Version Incompatibility"
msgstr "Несъвместими версии"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_CLASS_GENERAL\n"
"string.text"
msgid "General Error"
msgstr "Обща грешка"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_CLASS_FORMAT\n"
"string.text"
msgid "Incorrect format"
msgstr "Некоректен формат"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_CLASS_CREATE\n"
"string.text"
msgid "Error creating object"
msgstr "Грешка при създаване на обект"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_CLASS_SBX\n"
"string.text"
msgid "Inadmissible value or data type"
msgstr "Недопустима стойност или тип данни"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_CLASS_RUNTIME\n"
"string.text"
msgid "BASIC runtime error"
msgstr "Грешка при изпълнение на BASIC"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_CLASS_COMPILER\n"
"string.text"
msgid "BASIC syntax error"
msgstr "Синтактична грешка в код на BASIC"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"1\n"
"string.text"
msgid "General Error"
msgstr "Обща грешка"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_GENERAL\n"
"string.text"
msgid "General input/output error."
msgstr "Обща входно-изходна грешка."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_MISPLACEDCHAR\n"
"string.text"
msgid "Invalid file name."
msgstr "Невалидно име на файл."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_NOTEXISTS\n"
"string.text"
msgid "Nonexistent file."
msgstr "Несъществуващ файл."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_ALREADYEXISTS\n"
"string.text"
msgid "File already exists."
msgstr "Файлът вече съществува."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_NOTADIRECTORY\n"
"string.text"
msgid "The object is not a directory."
msgstr "Обектът не е директория."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_NOTAFILE\n"
"string.text"
msgid "The object is not a file."
msgstr "Обектът не е файл."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_INVALIDDEVICE\n"
"string.text"
msgid "The specified device is invalid."
msgstr "Зададеното устройство е невалидно."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_ACCESSDENIED\n"
"string.text"
msgid ""
"The object cannot be accessed\n"
"due to insufficient user rights."
msgstr ""
"Няма достъп до обекта поради\n"
"недостатъчни права на потребителя."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_LOCKVIOLATION\n"
"string.text"
msgid "Sharing violation while accessing the object."
msgstr "Нарушение при споделяне по време на достъп до обект."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_OUTOFSPACE\n"
"string.text"
msgid "No more space on device."
msgstr "Няма повече място на устройството."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_ISWILDCARD\n"
"string.text"
msgid ""
"This operation cannot be run on\n"
"files containing wildcards."
msgstr ""
"Операцията е недопустима върху\n"
"файлови имена със заместващи знаци."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_NOTSUPPORTED\n"
"string.text"
msgid "This operation is not supported on this operating system."
msgstr "Операцията не се поддържа от операционната система."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_TOOMANYOPENFILES\n"
"string.text"
msgid "There are too many files open."
msgstr "Отворени са прекалено много файлове."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_CANTREAD\n"
"string.text"
msgid "Data could not be read from the file."
msgstr "Не бе възможно прочитането на данните от файла."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_CANTWRITE\n"
"string.text"
msgid "The file could not be written."
msgstr "Не бе възможно записването на файла."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_OUTOFMEMORY\n"
"string.text"
msgid "The operation could not be run due to insufficient memory."
msgstr "Извършването на операцията бе невъзможно поради недостиг на памет."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_CANTSEEK\n"
"string.text"
msgid "The seek operation could not be run."
msgstr "Неуспешна операция търсене."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_CANTTELL\n"
"string.text"
msgid "The tell operation could not be run."
msgstr "Неуспешна операция tell."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_WRONGVERSION\n"
"string.text"
msgid "Incorrect file version."
msgstr "Некоректна версия на файл."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_WRONGFORMAT\n"
"string.text"
msgid "Incorrect file format."
msgstr "Некоректен формат на файл."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_INVALIDCHAR\n"
"string.text"
msgid "The file name contains invalid characters."
msgstr "Името на файла съдържа невалидни знаци."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_UNKNOWN\n"
"string.text"
msgid "An unknown I/O error has occurred."
msgstr "Възникнала е непозната входно/изходна грешка."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_INVALIDACCESS\n"
"string.text"
msgid "An invalid attempt was made to access the file."
msgstr "Извършен е невалиден опит за достъп до файла."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_CANTCREATE\n"
"string.text"
msgid "The file could not be created."
msgstr "Създаването на файла бе невъзможно."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_INVALIDPARAMETER\n"
"string.text"
msgid "The operation was started under an invalid parameter."
msgstr "Операцията е започната с невалиден параметър."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_ABORT\n"
"string.text"
msgid "The operation on the file was aborted."
msgstr "Операцията върху файла е прекратена."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_NOTEXISTSPATH\n"
"string.text"
msgid "Path to the file does not exist."
msgstr "Пътят към файла не съществува."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_RECURSIVE\n"
"string.text"
msgid "An object cannot be copied into itself."
msgstr "Обект не може да бъде копиран върху себе си."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_NOSTDTEMPLATE\n"
"string.text"
msgid "The default template could not be opened."
msgstr "Не бе възможно отварянето на подразбирания шаблон."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_TEMPLATENOTFOUND\n"
"string.text"
msgid "The specified template could not be found."
msgstr "Зададеният шаблон не бе намерен."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_NOTATEMPLATE\n"
"string.text"
msgid "The file cannot be used as template."
msgstr "Файлът не може да бъде използван като шаблон."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_CANTREADDOCINFO\n"
"string.text"
msgid ""
"Document information could not be read from the file because\n"
"the document information format is unknown or because document information does not\n"
"exist."
msgstr "Свойствата на документа не могат да бъдат прочетени от файла, защото са в непознат формат или не съществуват."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_ALREADYOPEN\n"
"string.text"
msgid "This document has already been opened for editing."
msgstr "Документът вече е отворен за редактиране."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_WRONGPASSWORD\n"
"string.text"
msgid "The wrong password has been entered."
msgstr "Въведена е грешна парола."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_DOLOADFAILED\n"
"string.text"
msgid "Error reading file."
msgstr "Грешка при четене на файл."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_DOCUMENTREADONLY\n"
"string.text"
msgid "The document was opened as read-only."
msgstr "Документът е отворен само за четене."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_OLEGENERAL\n"
"string.text"
msgid "General OLE Error."
msgstr "Обща грешка на OLE."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_INET_NAME_RESOLVE\n"
"string.text"
msgid "The host name $(ARG1) could not be resolved."
msgstr "Името на машината $(ARG1) не може да бъде определено."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_INET_CONNECT\n"
"string.text"
msgid "Could not establish Internet connection to $(ARG1)."
msgstr "Не е възможно да се установи интернет връзка с $(ARG1)."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_INET_READ\n"
"string.text"
msgid ""
"Error reading data from the Internet.\n"
"Server error message: $(ARG1)."
msgstr ""
"Грешка при получаване на данни от Интернет.\n"
"Съобщение за грешка от сървъра: $(ARG1)."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_INET_WRITE\n"
"string.text"
msgid ""
"Error transferring data to the Internet.\n"
"Server error message: $(ARG1)."
msgstr ""
"Грешка при изпращане на данни към Интернет.\n"
"Съобщение за грешка от сървъра: $(ARG1)."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_INET_GENERAL\n"
"string.text"
msgid "General Internet error has occurred."
msgstr "Възникна обща грешка при работа с Интернет."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_INET_OFFLINE\n"
"string.text"
msgid "The requested Internet data is not available in the cache and cannot be transmitted as the Online mode has not be activated."
msgstr "Търсените данни от Интернет не са налице в кеша и не могат да бъдат прехвърлени, тъй като като не е активен режимът Онлайн."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFXMSG_STYLEREPLACE\n"
"string.text"
msgid "Should the $(ARG1) Style be replaced?"
msgstr "Желаете ли стилът \"$(ARG1)\" да бъде заменен?"

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_NOFILTER\n"
"string.text"
msgid "A filter has not been found."
msgstr "Не е намерен филтър."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_CANTFINDORIGINAL\n"
"string.text"
msgid "The original could not be determined."
msgstr "Определянето на оригинала бе невъзможно."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_CANTCREATECONTENT\n"
"string.text"
msgid "The contents could not be created."
msgstr "Създаването на съдържание бе невъзможно."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_CANTCREATELINK\n"
"string.text"
msgid "The link could not be created."
msgstr "Създаването на връзка бе невъзможно."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_WRONGBMKFORMAT\n"
"string.text"
msgid "The link format is invalid."
msgstr "Форматът на връзката е невалиден."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_WRONGICONFILE\n"
"string.text"
msgid "The configuration of the icon display is invalid."
msgstr "Конфигурацията за визуализиране на икони е невалидна."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_CANTWRITEICONFILE\n"
"string.text"
msgid "The configuration of the icon display can not be saved."
msgstr "Конфигурацията за визуализиране на икони не може да бъде записана."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_CANTDELICONFILE\n"
"string.text"
msgid "The configuration of the icon display could not be deleted."
msgstr "Конфигурацията за визуализиране на икони не може да бъде изтрита."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_CANTRENAMECONTENT\n"
"string.text"
msgid "Contents cannot be renamed."
msgstr "Съдържанието не може да бъде преименувано."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_INVALIDBMKPATH\n"
"string.text"
msgid "The bookmark folder is invalid."
msgstr "Папката с показалци е невалидна."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_CANTWRITEURLCFGFILE\n"
"string.text"
msgid "The configuration of the URLs to be saved locally could not be saved."
msgstr "Конфигурацията за локално записване на URL не може да бъде записана."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_WRONGURLCFGFORMAT\n"
"string.text"
msgid "The configuration format of the URLs to be saved locally is invalid."
msgstr "Конфигурацията за локално записване на URL е невалидна."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_NODOCUMENT\n"
"string.text"
msgid "This action cannot be applied to a document that does not exist."
msgstr "Това действие не може да бъде извършено с несъществуващ документ."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_INVALIDLINK\n"
"string.text"
msgid "The link refers to an invalid target."
msgstr "Връзката съдържа обръщение към невалидна цел."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_INVALIDTRASHPATH\n"
"string.text"
msgid "The Recycle Bin path is invalid."
msgstr "Пътят до Кошчето е невалиден."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_NOTRESTORABLE\n"
"string.text"
msgid "The entry could not be restored."
msgstr "Записът не може да бъде възстановен."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_NAMETOOLONG\n"
"string.text"
msgid "The file name is too long for the target file system."
msgstr "Името на файла е твърде дълго за целевата файлова система."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_CONSULTUSER\n"
"string.text"
msgid "The details for running the function are incomplete."
msgstr "Параметрите са недостатъчни за стартиране на функцията."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_INVALIDSYNTAX\n"
"string.text"
msgid "The input syntax is invalid."
msgstr "Входният синтаксис е невалиден."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_CANTCREATEFOLDER\n"
"string.text"
msgid "The input syntax is invalid."
msgstr "Входният синтаксис е невалиден."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_CANTRENAMEFOLDER\n"
"string.text"
msgid "The input syntax is invalid."
msgstr "Входният синтаксис е невалиден."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_WRONG_CDF_FORMAT\n"
"string.text"
msgid "The channel document has an invalid format."
msgstr "Документът за канал е в невалиден формат."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_EMPTY_SERVER\n"
"string.text"
msgid "The server must not be empty."
msgstr "Сървърът не трябва да е празен."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_NO_ABOBOX\n"
"string.text"
msgid "A subscription folder is required to install a Channel."
msgstr "За инсталиране на канал е нужна папка за абонамент."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_NOTSTORABLEINBINARYFORMAT\n"
"string.text"
msgid ""
"This document contains attributes that cannot be saved in the selected format.\n"
"Please save the document in a %PRODUCTNAME %PRODUCTVERSION file format."
msgstr ""
"Документът съдържа атрибути, които не могат да бъдат записани в избрания формат.\n"
"Моля, запишете документа във файлов формат на %PRODUCTNAME %PRODUCTVERSION."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_TARGETFILECORRUPTED\n"
"string.text"
msgid "The file $(FILENAME) cannot be saved. Please check your system settings. You can find an automatically generated backup copy of this file in folder $(PATH) named $(BACKUPNAME)."
msgstr "Файлът $(FILENAME) не може да бъде записан. Моля, проверете системните настройки. Автоматично генерирано резервно копие на файла можете да намерите в папката $(PATH) под името $(BACKUPNAME)."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_NOMOREDOCUMENTSALLOWED\n"
"string.text"
msgid "The maximum number of documents that can be opened at the same time has been reached. You need to close one or more documents before you can open a new document."
msgstr "Достигнат е максималният брой едновременно отворени документи. Трябва да затворите един или повече документи, за да отворите нов."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_CANTCREATEBACKUP\n"
"string.text"
msgid "Could not create backup copy."
msgstr "Не бе възможно да се създаде резервно копие."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_MACROS_SUPPORT_DISABLED\n"
"string.text"
msgid ""
"An attempt was made to execute a macro.\n"
"For security reasons, macro support is disabled."
msgstr ""
"Направен е опит за стартиране на макрос.\n"
"Поради мерки за сигурност поддръжката на макроси е забранена."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_DOCUMENT_MACRO_DISABLED_MAC\n"
"string.text"
msgid ""
"This document contains macros.\n"
"\n"
"Macros may contain viruses. Execution of macros is disabled due to the current macro security setting in %PRODUCTNAME - Preferences - %PRODUCTNAME - Security.\n"
"\n"
"Therefore, some functionality may not be available."
msgstr ""
"Този документ съдържа макроси.\n"
"\n"
"Макросите може да съдържат вируси. Стартирането на макроси е забранено поради текущите настройки за сигурност на макроси в %PRODUCTNAME - Preferences - %PRODUCTNAME - Сигурност.\n"
"\n"
"Затова може да липсва функционалност."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_DOCUMENT_MACRO_DISABLED\n"
"string.text"
msgid ""
"This document contains macros.\n"
"\n"
"Macros may contain viruses. Execution of macros is disabled due to the current macro security setting in Tools - Options - %PRODUCTNAME - Security.\n"
"\n"
"Therefore, some functionality may not be available."
msgstr ""
"Този документ съдържа макроси.\n"
"\n"
"Макросите може да съдържат вируси. Стартирането на макроси е забранено поради текущите настройки за сигурност на макроси в Инструменти - Настройки - %PRODUCTNAME - Сигурност.\n"
"\n"
"Затова може да липсва функционалност."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_BROKENSIGNATURE\n"
"string.text"
msgid ""
"The digitally signed document content and/or macros do not match the current document signature.\n"
"\n"
"This could be the result of document manipulation or of structural document damage due to data transmission.\n"
"\n"
"We recommend that you do not trust the content of the current document.\n"
"Execution of macros is disabled for this document.\n"
" "
msgstr ""
"Цифрово подписаното съдържание и/или макросите в документа не съответстват на текущия му подпис.\n"
"\n"
"Това може да е в резултат на манипулиране на документа или повреди в структурата му, възникнали по време на трансфер на данни.\n"
"\n"
"Препоръчваме да не се доверявате на съдържанието на текущия документ.\n"
"Стартирането на макроси за този документ е забранено.\n"
" "

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_INCOMPLETE_ENCRYPTION\n"
"string.text"
msgid ""
"The encrypted document contains unexpected non-encrypted streams.\n"
"\n"
"This could be the result of document manipulation.\n"
"\n"
"We recommend that you do not trust the content of the current document.\n"
"Execution of macros is disabled for this document.\n"
" "
msgstr ""
"Шифрованият документ съдържа неочаквани нешифровани потоци.\n"
"\n"
"Това може да е в резултат на манипулиране на документа.\n"
"\n"
"Препоръчваме да не се доверявате на съдържанието на текущия документ.\n"
"Стартирането на макроси за този документ е забранено.\n"
" "

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_INVALIDLENGTH\n"
"string.text"
msgid "Invalid data length."
msgstr "Грешна дължина на данните."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_CURRENTDIR\n"
"string.text"
msgid "Function not possible: path contains current directory."
msgstr "Действието е невъзможно: пътят съдържа текущата директория."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_NOTSAMEDEVICE\n"
"string.text"
msgid "Function not possible: device (drive) not identical."
msgstr "Действието е невъзможно: устройствата не са еднакви."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_DEVICENOTREADY\n"
"string.text"
msgid "Device (drive) not ready."
msgstr "Устройството не е готово."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_BADCRC\n"
"string.text"
msgid "Wrong check amount."
msgstr "Грешна контролна сума."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_IO_WRITEPROTECTED\n"
"string.text"
msgid "Function not possible: write protected."
msgstr "Действието е невъзможно: защита срещу запис."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_SHARED_NOPASSWORDCHANGE\n"
"string.text"
msgid ""
"The password of a shared spreadsheet cannot be set or changed.\n"
"Deactivate sharing mode first."
msgstr ""
"Паролата за споделената електронна таблица не може да бъде зададена или сменена.\n"
"Първо деактивирайте споделения режим."

#: errtxt.src
msgctxt ""
"errtxt.src\n"
"RID_ERRHDL\n"
"ERRCODE_SFX_FORMAT_ROWCOL\n"
"string.text"
msgid "File format error found at $(ARG1)(row,col)."
msgstr "Открита е грешка във формата на файла на позиция $(ARG1)(row,col)."

#. extracted from svx/source/form
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-03-11 08:05+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: gu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1457683535.000000\n"

#: datanavi.src
#, fuzzy
msgctxt ""
"datanavi.src\n"
"RID_STR_QRY_REMOVE_MODEL\n"
"string.text"
msgid ""
"Deleting the model '$MODELNAME' affects all controls currently bound to this model.\n"
"Do you really want to delete this model?"
msgstr ""
"મોડેલ '$MODELNAME' કાઢી નાંખવાનું આ મોડેલ સાથે સંકળાયેલ બધા નિયંત્રણોને અસર કરે છે.\n"
"શું તમે ખરેખર આ મોડેલ કાઢી નાંખવા માંગો છો?"

#: datanavi.src
#, fuzzy
msgctxt ""
"datanavi.src\n"
"RID_STR_QRY_REMOVE_INSTANCE\n"
"string.text"
msgid ""
"Deleting the instance '$INSTANCENAME' affects all controls currently bound to this instance.\n"
"Do you really want to delete this instance?"
msgstr ""
"દષ્ટાંત '$INSTANCENAME' કાઢી નાંખવાનું આ દષ્ટાંત સાથે સંકળાયેલ બધા નિયંત્રણોને અસર કરે છે.\n"
"શું તમે ખરેખર આ દષ્ટાંત કાઢી નાંખવા માંગો છો?"

#: datanavi.src
#, fuzzy
msgctxt ""
"datanavi.src\n"
"RID_STR_QRY_REMOVE_ELEMENT\n"
"string.text"
msgid ""
"Deleting the element '$ELEMENTNAME' affects all controls currently bound to this element.\n"
"Do you really want to delete this element?"
msgstr ""
"ઘટક '$ELEMENTNAME' કાઢી નાંખવાનું આ ઘટક સાથે સંકળાયેલ બધા નિયંત્રણોને અસર કરે છે.\n"
"શું તમે ખરેખર આ ઘટક કાઢી નાંખવા માંગો છો?"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_QRY_REMOVE_ATTRIBUTE\n"
"string.text"
msgid "Do you really want to delete the attribute '$ATTRIBUTENAME'?"
msgstr "શું તમે ખરેખર આ ગુણધર્મ '$ATTRIBUTENAME' ને દૂર કરવા માંગો છો?"

#: datanavi.src
#, fuzzy
msgctxt ""
"datanavi.src\n"
"RID_STR_QRY_REMOVE_SUBMISSION\n"
"string.text"
msgid ""
"Deleting the submission '$SUBMISSIONNAME' affects all controls currently bound to this submission.\n"
"\n"
"Do you really want to delete this submission?"
msgstr ""
"જમાવટ '$SUBMISSIONNAME' ને કાઢી નાંખવાનું આ જમાવટ સાથે વર્તમાનમાં સંકળાયેલ બધા નિયંત્રણોને અસર કરે છે.\n"
"\n"
"શું તમે આ જમાવટ કાઢી નાંખવા માંગો છો?"

#: datanavi.src
#, fuzzy
msgctxt ""
"datanavi.src\n"
"RID_STR_QRY_REMOVE_BINDING\n"
"string.text"
msgid ""
"Deleting the binding '$BINDINGNAME' affects all controls currently bound to this binding.\n"
"\n"
"Do you really want to delete this binding?"
msgstr ""
"બાઈન્ડીંગ '$BINDINGNAME' ને કાઢી નાંખવાનું આ બાઈન્ડીંગ સાથે સંકળાયેલ બધા નિયંત્રણોને અસર કરે છે.\n"
"\n"
"શું તમે ખરેખર આ બાઈન્ડીંગ કાઢી નાંખવા માંગો છો?"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_INVALID_XMLNAME\n"
"string.text"
msgid "The name '%1' is not valid in XML. Please enter a different name."
msgstr "આ '%1' યોગ્ય XML નામ નથી.  મહેરબાની કરી અલગ નામ દાખલ કરો."

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_INVALID_XMLPREFIX\n"
"string.text"
msgid "The prefix '%1' is not valid in XML. Please enter a different prefix."
msgstr "આ પ્રત્યય '%1' યોગ્ય XML નામ નથી. મહેરબાની કરી અલગ પ્રત્યય દાખલ કરો."

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DOUBLE_MODELNAME\n"
"string.text"
msgid "The name '%1' already exists. Please enter a new name."
msgstr "આ '%1' નામ પહેલેથી અ મહેરબાની કરી અલગ પ્રત્યય દાખલ કરો."

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_EMPTY_SUBMISSIONNAME\n"
"string.text"
msgid "The submission must have a name."
msgstr "આ સબમિશન નું નામ હાેવુ જ જોઇએ."

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_MENU_DATANAVIGATOR\n"
"TBI_ITEM_ADD\n"
"menuitem.text"
msgid "Add Item"
msgstr "વસ્તુ ઉમેરો"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_MENU_DATANAVIGATOR\n"
"TBI_ITEM_ADD_ELEMENT\n"
"menuitem.text"
msgid "Add Element"
msgstr "ઘટક ઉમેરો"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_MENU_DATANAVIGATOR\n"
"TBI_ITEM_ADD_ATTRIBUTE\n"
"menuitem.text"
msgid "Add Attribute"
msgstr "લક્ષણ ઉમેરો"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_MENU_DATANAVIGATOR\n"
"TBI_ITEM_EDIT\n"
"menuitem.text"
msgid "Edit"
msgstr "ફેરફાર કરો"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_MENU_DATANAVIGATOR\n"
"TBI_ITEM_REMOVE\n"
"menuitem.text"
msgid "Delete"
msgstr "કાઢો"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_METHOD_POST\n"
"string.text"
msgid "Post"
msgstr "પોસ્ટ"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_METHOD_PUT\n"
"string.text"
msgid "Put"
msgstr "રાખવું"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_METHOD_GET\n"
"string.text"
msgid "Get"
msgstr "મેળવો"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_REPLACE_NONE\n"
"string.text"
msgid "None"
msgstr "કંઈ નહિં"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_REPLACE_INST\n"
"string.text"
msgid "Instance"
msgstr "નમૂનો"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_REPLACE_DOC\n"
"string.text"
msgid "Document"
msgstr "દસ્તાવેજ"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_SUBM_PARENT\n"
"string.text"
msgid "Submission: "
msgstr "સબમીશન: "

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_SUBM_ID\n"
"string.text"
msgid "ID: "
msgstr "ઓળખ: "

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_SUBM_BIND\n"
"string.text"
msgid "Binding: "
msgstr "બાંઘવું: "

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_SUBM_REF\n"
"string.text"
msgid "Reference: "
msgstr "અનુસંઘાન: "

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_SUBM_ACTION\n"
"string.text"
msgid "Action: "
msgstr "ક્રિયા: "

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_SUBM_METHOD\n"
"string.text"
msgid "Method: "
msgstr "રીત: "

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_SUBM_REPLACE\n"
"string.text"
msgid "Replace: "
msgstr "બદલાવો: "

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_ADD_ELEMENT\n"
"string.text"
msgid "Add Element"
msgstr "ઘટક ઉમેરો"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_EDIT_ELEMENT\n"
"string.text"
msgid "Edit Element"
msgstr "ઘટકમાં ફેરફાર કરો"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_REMOVE_ELEMENT\n"
"string.text"
msgid "Delete Element"
msgstr "ભાગ દુર કરો"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_ADD_ATTRIBUTE\n"
"string.text"
msgid "Add Attribute"
msgstr "લક્ષણ ઉમેરો"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_EDIT_ATTRIBUTE\n"
"string.text"
msgid "Edit Attribute"
msgstr "ગુણધર્મો સુઘારો"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_REMOVE_ATTRIBUTE\n"
"string.text"
msgid "Delete Attribute"
msgstr "ગુણધર્મો કાઢી નાંખો"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_ADD_BINDING\n"
"string.text"
msgid "Add Binding"
msgstr "બાંધનાર ઉમેરો"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_EDIT_BINDING\n"
"string.text"
msgid "Edit Binding"
msgstr "બાંધનાર સુઘારો"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_REMOVE_BINDING\n"
"string.text"
msgid "Delete Binding"
msgstr "બાંધનાર દુર કરો"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_ADD_SUBMISSION\n"
"string.text"
msgid "Add Submission"
msgstr "જમાવટ ઉમેરો"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_EDIT_SUBMISSION\n"
"string.text"
msgid "Edit Submission"
msgstr "સબમીશન સુધારો"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_REMOVE_SUBMISSION\n"
"string.text"
msgid "Delete Submission"
msgstr "સબમીશન દુર કરો"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_ELEMENT\n"
"string.text"
msgid "Element"
msgstr "ઘટક"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_ATTRIBUTE\n"
"string.text"
msgid "Attribute"
msgstr "ગુણધર્મ"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_BINDING\n"
"string.text"
msgid "Binding"
msgstr "બાંધવું"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_BINDING_EXPR\n"
"string.text"
msgid "Binding expression"
msgstr "બાઈન્ડીંગ સમીકરણ"

#: filtnav.src
msgctxt ""
"filtnav.src\n"
"RID_FM_FILTER_MENU\n"
"SID_FM_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr "કાઢો (~D)"

#: filtnav.src
msgctxt ""
"filtnav.src\n"
"RID_FM_FILTER_MENU\n"
"SID_FM_FILTER_EDIT\n"
"menuitem.text"
msgid "~Edit"
msgstr "ફેરફાર કરો (~E)"

#: filtnav.src
msgctxt ""
"filtnav.src\n"
"RID_FM_FILTER_MENU\n"
"SID_FM_FILTER_IS_NULL\n"
"menuitem.text"
msgid "~Is Null"
msgstr "Null છે (~I)"

#: filtnav.src
msgctxt ""
"filtnav.src\n"
"RID_FM_FILTER_MENU\n"
"SID_FM_FILTER_IS_NOT_NULL\n"
"menuitem.text"
msgid "I~s not Null"
msgstr "Null નથી (~s)"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU.SID_FM_NEW\n"
"SID_FM_NEW_FORM\n"
"menuitem.text"
msgid "Form"
msgstr "ફોર્મ"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU.SID_FM_NEW\n"
"SID_FM_NEW_HIDDEN\n"
"menuitem.text"
msgid "Hidden Control"
msgstr "છૂપાયેલુ નિયંત્રણ"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU\n"
"SID_FM_NEW\n"
"menuitem.text"
msgid "~New"
msgstr "નવું(~N)"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU\n"
"SID_FM_CHANGECONTROLTYPE\n"
"menuitem.text"
msgid "Replace with"
msgstr "બદલો"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU\n"
"SID_CUT\n"
"menuitem.text"
msgid "Cu~t"
msgstr ""

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU\n"
"SID_COPY\n"
"menuitem.text"
msgid "~Copy"
msgstr ""

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU\n"
"SID_PASTE\n"
"menuitem.text"
msgid "~Paste"
msgstr ""

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU\n"
"SID_FM_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr "કાઢો (~D)"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU\n"
"SID_FM_TAB_DIALOG\n"
"menuitem.text"
msgid "Tab Order..."
msgstr "ટેબનો ક્રમ..."

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU\n"
"SID_FM_RENAME_OBJECT\n"
"menuitem.text"
msgid "~Rename"
msgstr "ફરી નામ આપો(~R)"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU\n"
"SID_FM_SHOW_PROPERTY_BROWSER\n"
"menuitem.text"
msgid "Propert~ies"
msgstr "ગુણધર્મો(~i)"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU\n"
"SID_FM_OPEN_READONLY\n"
"menuitem.text"
msgid "Open in Design Mode"
msgstr "ભાત સ્થિતિમાં ખોલો"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU\n"
"SID_FM_AUTOCONTROLFOCUS\n"
"menuitem.text"
msgid "Automatic Control Focus"
msgstr "આપોઆપ નિયંત્રણ પર પ્રકાશ"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_EDIT\n"
"menuitem.text"
msgid "~Text Box"
msgstr "લખાણ પેટી(~T)"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_BUTTON\n"
"menuitem.text"
msgid "~Button"
msgstr "બટન (~B)"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_FIXEDTEXT\n"
"menuitem.text"
msgid "La~bel field"
msgstr "કોષ્ટક ક્ષેત્ર (~b)"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_GROUPBOX\n"
"menuitem.text"
msgid "G~roup Box"
msgstr "જૂથ પેટી (~r)"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_LISTBOX\n"
"menuitem.text"
msgid "L~ist Box"
msgstr "યાદીની પેટી (~i)"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_CHECKBOX\n"
"menuitem.text"
msgid "~Check Box"
msgstr "ચકાસો પેટી (~C)"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_RADIOBUTTON\n"
"menuitem.text"
msgid "~Radio Button"
msgstr "રેડિયો બટન (~R)"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_COMBOBOX\n"
"menuitem.text"
msgid "Combo Bo~x"
msgstr "કોમ્બો બોક્સ(~x)"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_IMAGEBUTTON\n"
"menuitem.text"
msgid "I~mage Button"
msgstr "ચિત્ર બટન(~m)"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_FILECONTROL\n"
"menuitem.text"
msgid "~File Selection"
msgstr "ફાઇલ પસંદગી (~F)"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_DATE\n"
"menuitem.text"
msgid "~Date Field"
msgstr "તારીખ ક્ષેત્ર (~D)"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_TIME\n"
"menuitem.text"
msgid "Tim~e Field"
msgstr "સમય ક્ષેત્ર (~e)"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_NUMERIC\n"
"menuitem.text"
msgid "~Numerical Field"
msgstr "આંકડાકીય ક્ષેત્ર (~N)"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_CURRENCY\n"
"menuitem.text"
msgid "C~urrency Field"
msgstr "ચલણ ક્ષેત્ર (~u)"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_PATTERN\n"
"menuitem.text"
msgid "~Pattern Field"
msgstr "ભાત ક્ષેત્ર (~P)"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_IMAGECONTROL\n"
"menuitem.text"
msgid "Ima~ge Control"
msgstr "ચિત્રનાં નિયંત્રણો (~g)"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_FORMATTED\n"
"menuitem.text"
msgid "Fo~rmatted Field"
msgstr "બંધારણ ઘડાયેલ ક્ષેત્ર (~r)"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_SCROLLBAR\n"
"menuitem.text"
msgid "Scroll bar"
msgstr "ખસેડવાની પટ્ટી"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_SPINBUTTON\n"
"menuitem.text"
msgid "Spin Button"
msgstr "સ્પીન બટન"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_NAVIGATIONBAR\n"
"menuitem.text"
msgid "Navigation Bar"
msgstr "સંશોધક પટ્ટી"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_ERR_CONTEXT_ADDFORM\n"
"string.text"
msgid "Error while creating form"
msgstr "ફોર્મ દષ્ટાંતતી વખતે ભૂલ"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_ERR_FIELDREQUIRED\n"
"string.text"
msgid "Input required in field '#'. Please enter a value."
msgstr "'#' ક્ષેત્રમાં ઈનપુટ જરુરી છે. મહેરબાની કરીને કિંમત દાખલ કરો."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_ERR_DUPLICATE_NAME\n"
"string.text"
msgid ""
"Entry already exists.\n"
"Please choose another name."
msgstr ""
"પ્રવેશ પહેલાથી જ હાજર છે.\n"
"મહેરબાની કરીને અન્ય નામ પસંદ કરો."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_FORMS\n"
"string.text"
msgid "Forms"
msgstr "ફોર્મો"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_NO_PROPERTIES\n"
"string.text"
msgid "No control selected"
msgstr "કોઈ નિયંત્રણ પસંદ થયુ નથી"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPERTIES_CONTROL\n"
"string.text"
msgid "Properties: "
msgstr "ગુણધર્મો: "

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPERTIES_FORM\n"
"string.text"
msgid "Form Properties"
msgstr "ફોર્મના ગુણધર્મો"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_FMEXPLORER\n"
"string.text"
msgid "Form Navigator"
msgstr "ફોર્મ સંશોધક"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_FORM\n"
"string.text"
msgid "Form"
msgstr "ફોર્મ"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_TABWIN_PREFIX\n"
"Table\n"
"itemlist.text"
msgid "Table"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_TABWIN_PREFIX\n"
"Query\n"
"itemlist.text"
msgid "Query"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_TABWIN_PREFIX\n"
"SQL\n"
"itemlist.text"
msgid "SQL"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_STDFORMNAME\n"
"string.text"
msgid "Form"
msgstr "ફોર્મ"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_HIDDEN\n"
"string.text"
msgid "Hidden Control"
msgstr "છૂપાયેલુ નિયંત્રણ"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_CONTROL\n"
"string.text"
msgid "Control"
msgstr "નિયંત્રણ"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_REC_TEXT\n"
"string.text"
msgid "Record"
msgstr "રેકોર્ડ"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_REC_FROM_TEXT\n"
"string.text"
msgid "of"
msgstr "નું"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_FIELDSELECTION\n"
"string.text"
msgid "Add field:"
msgstr "ક્ષેત્ર ઉમેરો:"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_WRITEERROR\n"
"string.text"
msgid "Error writing data to database"
msgstr "ડેટાબેઝમાં માહિતી લખતી વખતે ભૂલ"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SYNTAXERROR\n"
"string.text"
msgid "Syntax error in query expression"
msgstr "પ્રશ્ર્ન સમીકરણમાં બંધારણની ભૂલ"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_DELETECONFIRM_RECORD\n"
"string.text"
msgid "You intend to delete 1 record."
msgstr "તમે ૧ રેકોર્ડ કાઢી નાંખવાની ઈચ્છા ધરાવો છો."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_DELETECONFIRM_RECORDS\n"
"string.text"
msgid "# records will be deleted."
msgstr "# રેકોર્ડો નીકળી જશે."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_DELETECONFIRM\n"
"string.text"
msgid ""
"If you click Yes, you won't be able to undo this operation.\n"
"Do you want to continue anyway?"
msgstr ""
"જો હા ક્લિક કરો, તો તમે આ પ્રક્રિયા રદ કરવા સમર્થ હશો નહિં!\n"
"શું તમે ગમે તે રીતે ચાલુ રાખવા માંગો છો?"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_ERR_NO_ELEMENT\n"
"string.text"
msgid "Choose an entry from the list or enter a text corresponding to one of the list items."
msgstr "યાદીમાંથી દાખલો પસંદ કરો અથવા યાદી વસ્તુને અનુરુપ લખાણ દાખલ કરો."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_GROUPBOX\n"
"string.text"
msgid "Frame element"
msgstr "ચોકઠાનો ભાગ"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_NAVIGATION\n"
"string.text"
msgid "Navigation"
msgstr "શોધખોળ"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_NAVIGATIONBAR\n"
"string.text"
msgid "Navigation bar"
msgstr "માર્ગદર્શક પટ્ટી"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_COLUMN\n"
"string.text"
msgid "Col"
msgstr "સ્તંભ"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_UNDO_PROPERTY\n"
"string.text"
msgid "Set property '#'"
msgstr "ગુણધર્મો  '#' સુયોજીત કરો"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_UNDO_CONTAINER_INSERT\n"
"string.text"
msgid "Insert in container"
msgstr "સંગ્રાહકમાં દાખલ કરો"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_UNDO_CONTAINER_REMOVE\n"
"string.text"
msgid "Delete #"
msgstr "# કાઢી નાંખો"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_UNDO_CONTAINER_REMOVE_MULTIPLE\n"
"string.text"
msgid "Delete # objects"
msgstr "# વસ્તુઓ કાઢી નાંખો"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_UNDO_CONTAINER_REPLACE\n"
"string.text"
msgid "Replace a container element"
msgstr "સંગ્રાહકનાં ભાગોને બદલો"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_UNDO_DELETE_LOGICAL\n"
"string.text"
msgid "Delete structure"
msgstr "માળખુ કાઢી નાંખો"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_UNDO_MODEL_REPLACE\n"
"string.text"
msgid "Replace Control"
msgstr "નિયંત્રણ બદલો"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_DATE\n"
"string.text"
msgid "Date"
msgstr "તારીખ"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_TIME\n"
"string.text"
msgid "Time"
msgstr "સમય"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_PUSHBUTTON\n"
"string.text"
msgid "Push Button"
msgstr "બટનને દબાવો"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_RADIOBUTTON\n"
"string.text"
msgid "Option Button"
msgstr "વિકલ્પ બટન"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_CHECKBOX\n"
"string.text"
msgid "Check Box"
msgstr "ચૅક બોક્સ"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_FIXEDTEXT\n"
"string.text"
msgid "Label Field"
msgstr "લેબલ ક્ષેત્ર"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_GROUPBOX\n"
"string.text"
msgid "Group Box"
msgstr "સમૂહ પેટી"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_EDIT\n"
"string.text"
msgid "Text Box"
msgstr "લખાણ પેટી"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_FORMATTED\n"
"string.text"
msgid "Formatted Field"
msgstr "ફોર્મેટેડ ક્ષેત્ર"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_LISTBOX\n"
"string.text"
msgid "List Box"
msgstr "યાદીની પેટી"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_COMBOBOX\n"
"string.text"
msgid "Combo Box"
msgstr "કોમ્બો બોક્સ"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_IMAGEBUTTON\n"
"string.text"
msgid "Image Button"
msgstr "ચિત્ર બટન"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_IMAGECONTROL\n"
"string.text"
msgid "Image Control"
msgstr "ચિત્રનું નિયંત્રણ"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_FILECONTROL\n"
"string.text"
msgid "File Selection"
msgstr "ફાઈલની પસંદગી"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_DATEFIELD\n"
"string.text"
msgid "Date Field"
msgstr "તારીખની ક્ષેત્ર"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_TIMEFIELD\n"
"string.text"
msgid "Time Field"
msgstr "સમય ક્ષેત્ર"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_NUMERICFIELD\n"
"string.text"
msgid "Numeric Field"
msgstr "સંખ્યાની ક્ષેત્ર"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_CURRENCYFIELD\n"
"string.text"
msgid "Currency Field"
msgstr "નાણા ક્ષેત્ર"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_PATTERNFIELD\n"
"string.text"
msgid "Pattern Field"
msgstr "ભાત ક્ષેત્ર"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_DBGRID\n"
"string.text"
msgid "Table Control "
msgstr "કોષ્ટક નિયંત્રણ "

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_SCROLLBAR\n"
"string.text"
msgid "Scrollbar"
msgstr "સરકપટ્ટી"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_SPINBUTTON\n"
"string.text"
msgid "Spin Button"
msgstr "સ્પીન બટન"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_NAVBAR\n"
"string.text"
msgid "Navigation Bar"
msgstr "સંશોધક પટ્ટી"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_MULTISELECT\n"
"string.text"
msgid "Multiselection"
msgstr "ઘણી પસંદગીઓ"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_NODATACONTROLS\n"
"string.text"
msgid "No data-related controls in the current form!"
msgstr "આ ફોર્મમાં માહિતી સંબંધિત નિયંત્રણો નથી!"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_POSTFIX_DATE\n"
"string.text"
msgid " (Date)"
msgstr " (તારીખ)"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_POSTFIX_TIME\n"
"string.text"
msgid " (Time)"
msgstr " (સમય)"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_FILTER_NAVIGATOR\n"
"string.text"
msgid "Filter navigator"
msgstr "ગાળક શાેધક"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_FILTER_FILTER_FOR\n"
"string.text"
msgid "Filter for"
msgstr "ગાળક માટે"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_FILTER_FILTER_OR\n"
"string.text"
msgid "Or"
msgstr "અથવા"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_NOCONTROLS_FOR_EXTERNALDISPLAY\n"
"string.text"
msgid "Valid bound controls which can be used in the table view do not exist in the current form."
msgstr "યોગ્ય સીમીત નિયંત્રણો કે જે કોષ્ટકનાં દેખાવમાં વપરાય છે તે વર્તમાન ફોર્મમાં હયાત નથી."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_AUTOFIELD\n"
"string.text"
msgid "<AutoField>"
msgstr "<આપમેળે ક્ષેત્ર>"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"LIKE\n"
"itemlist.text"
msgid "LIKE"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"NOT\n"
"itemlist.text"
msgid "NOT"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"EMPTY\n"
"itemlist.text"
msgid "EMPTY"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"TRUE\n"
"itemlist.text"
msgid "TRUE"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"FALSE\n"
"itemlist.text"
msgid "FALSE"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"IS\n"
"itemlist.text"
msgid "IS"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"BETWEEN\n"
"itemlist.text"
msgid "BETWEEN"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"OR\n"
"itemlist.text"
msgid "OR"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"AND\n"
"itemlist.text"
msgid "AND"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"Average\n"
"itemlist.text"
msgid "Average"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"Count\n"
"itemlist.text"
msgid "Count"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"Maximum\n"
"itemlist.text"
msgid "Maximum"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"Minimum\n"
"itemlist.text"
msgid "Minimum"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"Sum\n"
"itemlist.text"
msgid "Sum"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"Every\n"
"itemlist.text"
msgid "Every"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"Any\n"
"itemlist.text"
msgid "Any"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"Some\n"
"itemlist.text"
msgid "Some"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"STDDEV_POP\n"
"itemlist.text"
msgid "STDDEV_POP"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"STDDEV_SAMP\n"
"itemlist.text"
msgid "STDDEV_SAMP"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"VAR_SAMP\n"
"itemlist.text"
msgid "VAR_SAMP"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"VAR_POP\n"
"itemlist.text"
msgid "VAR_POP"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"Collect\n"
"itemlist.text"
msgid "Collect"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"Fusion\n"
"itemlist.text"
msgid "Fusion"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"Intersection\n"
"itemlist.text"
msgid "Intersection"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_ERROR\n"
"string.text"
msgid "Syntax error in SQL statement"
msgstr "SQL વાક્યમાં બંધારણની ભૂલ"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_VALUE_NO_LIKE\n"
"string.text"
msgid "The value #1 cannot be used with LIKE."
msgstr "કિંમત #1 એ LIKE સાથે વાપરી શકાશે નહિં."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_FIELD_NO_LIKE\n"
"string.text"
msgid "LIKE cannot be used with this field."
msgstr "LIKE આ ક્ષેત્ર સાથે વાપરી શકાતુ નથી."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_ACCESS_DAT_NO_VALID\n"
"string.text"
msgid "The value entered is not a valid date. Please enter a date in a valid format, for example, MM/DD/YY."
msgstr "દાખલ કરવામાં આવેલ કિંમત યોગ્ય તારીખ નથી. મહેરબાની કરીને તારીખ યોગ્ય રીતે દાખલ કરો, જેમકે,મહીનો/દિવસ/વષૅ."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_INT_NO_VALID\n"
"string.text"
msgid "The field cannot be compared with an integer."
msgstr "ક્ષેત્ર પૂર્ણાંક સાથે સરખાવી શકાયુ નહિં."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_TABLE\n"
"string.text"
msgid "The database does not contain a table named \"#\"."
msgstr "ડેટાબેઝ \"#\" નામવાળું કોષ્ટક સમાવતું નથી."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_TABLE_OR_QUERY\n"
"string.text"
msgid "The database does contain neither a table nor a query named \"#\"."
msgstr "ડેટાબેઝ ક્યાં તો કોષ્ટક કે પ્રશ્ન કે જે \"#\" નામવાળું હોય તે સમાવતું નથી."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_TABLE_EXISTS\n"
"string.text"
msgid "The database already contains a table or view with name \"#\"."
msgstr "ડેટાબેઝ પહેલાથી જ કોષ્ટક અથવા દેખાવ \"#\" નામવાળું સમાવે છે."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_QUERY_EXISTS\n"
"string.text"
msgid "The database already contains a query with name \"#\"."
msgstr "ડેટાબેઝ પહેલાથી જ \"#\" નામવાળો પ્રશ્ન સમાવે છે."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_COLUMN\n"
"string.text"
msgid "The column \"#1\" is unknown in the table \"#2\"."
msgstr "સ્તંભ \"#1\" એ કોષ્ટક \"#2\" માં અજ્ઞાત છે."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_REAL_NO_VALID\n"
"string.text"
msgid "The field cannot be compared with a floating point number."
msgstr "ક્ષેત્ર દશાંશ બિંદુુવાળી સંખ્યા સાથે સરખાવી શકાયુ નહિં."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_CRIT_NO_COMPARE\n"
"string.text"
msgid "The entered criterion cannot be compared with this field."
msgstr "આપેલ માપદંડ આ ક્ષેત્ર સાથે સરખાવી શકાયો નહિં."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_DATANAVIGATOR\n"
"string.text"
msgid "Data Navigator"
msgstr "માહીતી માર્ગદર્શક"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_READONLY_VIEW\n"
"string.text"
msgid " (read-only)"
msgstr " (માત્ર-વાંચી શકાય તેવું)"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_ALREADYEXISTOVERWRITE\n"
"string.text"
msgid "The file already exists. Overwrite?"
msgstr "ફાઇલ એ પહેલેથી અસ્તિત્વ ધરાવે છે. શું ઉપર લખવા માંગો છો?"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_OBJECT_LABEL\n"
"string.text"
msgid "#object# label"
msgstr "#object# લેબલ"

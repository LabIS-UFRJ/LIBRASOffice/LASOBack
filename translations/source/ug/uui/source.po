#. extracted from uui/source
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-11-10 19:33+0100\n"
"PO-Revision-Date: 2016-01-02 16:39+0000\n"
"Last-Translator: Abduqadir Abliz <Sahran@live.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ug\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Pootle 2.7\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1451752775.000000\n"

#: alreadyopen.src
msgctxt ""
"alreadyopen.src\n"
"STR_ALREADYOPEN_TITLE\n"
"string.text"
msgid "Document in Use"
msgstr "پۈتۈك ئىشلىتىلىۋاتىدۇ"

#: alreadyopen.src
msgctxt ""
"alreadyopen.src\n"
"STR_ALREADYOPEN_MSG\n"
"string.text"
msgid ""
"Document file '$(ARG1)' is locked for editing by yourself on a different system since $(ARG2)\n"
"\n"
"Open document read-only, or ignore own file locking and open the document for editing.\n"
"\n"
msgstr ""
"'$(ARG1)' پۈتۈك ھۆججىتى $(ARG2) دا ئوخشاش بولمىغان سىستېمىدا ئۆزىڭىز تەھرىرلەشنى قۇلۇپلىدىڭىز.\n"
"\n"
"پۈتۈكنى ئوقۇشقىلا بولىدىغان ھالەتتە ئاچالايسىز ياكى ئۆزىڭىزنىڭ قۇلۇپىغا پەرۋا قىلماي، پۈتۈكنى ئېچىپ تەھرىرىلىسىڭىز بولىدۇ.\n"
"\n"

#: alreadyopen.src
msgctxt ""
"alreadyopen.src\n"
"STR_ALREADYOPEN_READONLY_BTN\n"
"string.text"
msgid "Open ~Read-Only"
msgstr "ئوقۇشقىلا بولىدىغان ئاچ(~R)"

#: alreadyopen.src
msgctxt ""
"alreadyopen.src\n"
"STR_ALREADYOPEN_OPEN_BTN\n"
"string.text"
msgid "~Open"
msgstr "ئاچ(~O)"

#: alreadyopen.src
msgctxt ""
"alreadyopen.src\n"
"STR_ALREADYOPEN_SAVE_MSG\n"
"string.text"
msgid ""
"Document file '$(ARG1)' is locked for editing by yourself on a different system since $(ARG2)\n"
"\n"
"Close document on other system and retry saving or ignore own file locking and save current document.\n"
"\n"
msgstr ""
"'$(ARG1)' پۈتۈك ھۆججىتى $(ARG2) دا ئوخشاش بولمىغان سىستېمىدا ئۆزىڭىز تەھرىرلەشنى قۇلۇپلىدىڭىز.\n"
"\n"
"باشقا سىستېمىدا پۈتۈكنى يېپىپ، قايتا ساقلاڭ ياكى ئۆزىڭىزسالغان قۇلۇپقا پەرۋا قىلماي، نۆۋەتتىكى پۈتۈكنى ساقلاڭ.\n"
"\n"

#: alreadyopen.src
msgctxt ""
"alreadyopen.src\n"
"STR_ALREADYOPEN_RETRY_SAVE_BTN\n"
"string.text"
msgid "~Retry Saving"
msgstr "ساقلاشنى قايتا سىنا(~R)"

#: alreadyopen.src
msgctxt ""
"alreadyopen.src\n"
"STR_ALREADYOPEN_SAVE_BTN\n"
"string.text"
msgid "~Save"
msgstr "ساقلا(~S)"

#: filechanged.src
msgctxt ""
"filechanged.src\n"
"STR_FILECHANGED_TITLE\n"
"string.text"
msgid "Document Has Been Changed by Others"
msgstr "پۈتۈكنى باشقىلار ئۆزگەرتتى"

#: filechanged.src
msgctxt ""
"filechanged.src\n"
"STR_FILECHANGED_MSG\n"
"string.text"
msgid ""
"The file has been changed since it was opened for editing in %PRODUCTNAME. Saving your version of the document will overwrite changes made by others.\n"
"\n"
"Do you want to save anyway?\n"
"\n"
msgstr ""
"%PRODUCTNAME دا ھۆججەت ئېچىلىپ تەھرىرلەندى، شۇڭلاشقا ئۇ ھۆججەت ئۆزگەرتىلدى. سىز ئۆزگەرتكەندىن كېيىنكى پۈتۈك باشقىلارنىڭ ئۆزگەرتىشىنى قاپلىۋېتىدۇ..\n"
"\n"
"ساقلاۋېرەمسىز؟\n"
"\n"

#: filechanged.src
msgctxt ""
"filechanged.src\n"
"STR_FILECHANGED_SAVEANYWAY_BTN\n"
"string.text"
msgid "~Save Anyway"
msgstr "ساقلاۋەر(~S)"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_KEEP_PASSWORD\n"
"string.text"
msgid "~Remember password until end of session"
msgstr "سۆزلىشىش ئاخىرلاشقۇچە ئىم ئەستە تۇت(~R)"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_SAVE_PASSWORD\n"
"string.text"
msgid "~Remember password"
msgstr "ئىم ئەستە تۇت(~R)"

#: ids.src
msgctxt ""
"ids.src\n"
"STR_WARNING_BROKENSIGNATURE_TITLE\n"
"string.text"
msgid "Invalid Document Signature"
msgstr "ئىناۋەتسىز پۈتۈك ئىمزاسى"

#: ids.src
msgctxt ""
"ids.src\n"
"STR_WARNING_INCOMPLETE_ENCRYPTION_TITLE\n"
"string.text"
msgid "Non-Encrypted Streams"
msgstr "شىفىرلانمىغان سانلىق مەلۇمات ئېقىمى"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_ABORT & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The operation executed on $(ARG1) was aborted."
msgstr "$(ARG1) دا ئىجرا بولىدىغان مەشغۇلات توختىتىلدى."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_ACCESSDENIED & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Access to $(ARG1) was denied."
msgstr "$(ARG1) زىيارەت رەت قىلىندى."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_ALREADYEXISTS & ERRCODE_RES_MASK)\n"
"string.text"
msgid "$(ARG1) already exists."
msgstr "$(ARG1) مەۋجۇد."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_TARGETALREADYEXISTS & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Target already exists."
msgstr "نىشان مەۋجۇد."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_MODULESIZEEXCEEDED & ERRCODE_RES_MASK)\n"
"string.text"
msgid ""
"You are about to save/export a password protected basic library containing module(s) \n"
"$(ARG1)\n"
"which are too large to store in binary format. If you wish users that don't have access to the library password to be able to run macros in those module(s) you must split those modules into a number of smaller modules. Do you wish to continue to save/export this library?"
msgstr ""
"سىز ئىم بىلەن قوغدالغان basic ئامبىرىغا ساقلاش/چىقىرىش مەشغۇلاتى ئېلىپ بارماقچى. بۇ ئامبار ئۆز ئىچىگە ئالغان \n"
"$(ARG1)\n"
"بۆلەك بەك چوڭ، ئىككىلىك سىستېمىدا ساقلىيالمايدۇ. ئەگەر سىز ئامبارنى ئىم بىلمەيدىغان زىيارەت قىلىش ھوقۇقى يوق ئىشلەتكۈچلەرمۇ بۇ بۆلەكلەردىكى ماكرونى ئىجرا قىلىشنى ئۈمىد قىلسىڭىز ئۇ ھالدا بۇ بۆلەكلەرنى كۆپلىگەن كىچىك بۆلەكلەرگە پارچىلاڭ. بۇ ئامبارنى يەنىلا ساقلاپ/چىقىرامسىز؟"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_BADCRC & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The data from $(ARG1) has an incorrect checksum."
msgstr "$(ARG1) سانلىق مەلۇماتنىڭ تەكشۈرۈش يىغىندىسى خاتا."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_CANTCREATE & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The object $(ARG1) cannot be created in directory $(ARG2)."
msgstr "$(ARG2) مۇندەرىجىدە $(ARG1) ئوبيېكتنى قۇرالمىدى."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_CANTREAD & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Data of $(ARG1) could not be read."
msgstr "$(ARG1) نىڭ سانلىق مەلۇماتىنى ئوقۇيالمىدى."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_CANTSEEK & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The seek operation on $(ARG1) could not be performed."
msgstr "$(ARG1) دا seek مەشغۇلاتىنى ئىجرا قىلالمىدى."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_CANTTELL & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The tell operation on $(ARG1) could not be performed."
msgstr "$(ARG1) دا tell مەشغۇلاتىنى ئىجرا قىلالمىدى."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_CANTWRITE & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Data for $(ARG1) could not be written."
msgstr "$(ARG1) غا سانلىق مەلۇمات يازالمىدى."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_CURRENTDIR & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Action impossible: $(ARG1) is the current directory."
msgstr "مەشغۇلات قىلالمىدى: $(ARG1) نۆۋەتتىكى مۇندەرىجە."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTREADY & ERRCODE_RES_MASK)\n"
"string.text"
msgid "$(ARG1) is not ready."
msgstr "$(ARG1) تېخى تەييارلانمىدى."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTSAMEDEVICE & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Action impossible: $(ARG1) and $(ARG2) are different devices (drives)."
msgstr "مەشغۇلاتنى ئىجرا قىلالمىدى: $(ARG1) بىلەن $(ARG2) ئوخشىمىغان ئۈسكۈنە (قوزغاتقۇچ)"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_GENERAL & ERRCODE_RES_MASK)\n"
"string.text"
msgid "General input/output error while accessing $(ARG1)."
msgstr "$(ARG1) زىيارەت قىلغاندا كىرگۈزۈش/چىقىرىش خاتالىقى كۆرۈلدى."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_INVALIDACCESS & ERRCODE_RES_MASK)\n"
"string.text"
msgid "An attempt was made to access $(ARG1) in an invalid way."
msgstr "ئىناۋەتسىز ئۇسۇلدا $(ARG1) نى زىيارەت قىلىشقا ئۇرۇندى."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_INVALIDCHAR & ERRCODE_RES_MASK)\n"
"string.text"
msgid "$(ARG1) contains invalid characters."
msgstr "$(ARG1) دا ئىناۋەتسىز ھەرپ بار."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_INVALIDDEVICE & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The device (drive) $(ARG1) is invalid."
msgstr "بۇ ئۈسكۈنە (قوزغاتقۇچ) $(ARG1) ئىناۋەتسىز."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_INVALIDLENGTH & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The data from $(ARG1) has an invalid length."
msgstr "$(ARG1) دىكى سانلىق مەلۇمات ئۇزۇنلۇقى ئىناۋەتسىز."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_INVALIDPARAMETER & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The operation on $(ARG1) was started with an invalid parameter."
msgstr "ئىناۋەتسىز پارامېتىر ئىشلىتىپ $(ARG1) دىكى مەشغۇلات قوزغىتىلدى."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_ISWILDCARD & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The operation cannot be performed because $(ARG1) contains wildcards."
msgstr "مەشغۇلاتنى ئىجرا قىلالمايدۇ، چۈنكى  $(ARG1) دا ئورتاق بەلگە بار."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_LOCKVIOLATION & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Error during shared access to $(ARG1)."
msgstr "$(ARG1) نى بىرلا ۋاقىتتا زىيارەت قىلغاندا خاتالىق كۆرۈلدى."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_MISPLACEDCHAR & ERRCODE_RES_MASK)\n"
"string.text"
msgid "$(ARG1) contains misplaced characters."
msgstr "$(ARG1) دا ئورنى يۆتكەلگەن ھەرپ بار."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NAMETOOLONG & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The name $(ARG1) contains too many characters."
msgstr "$(ARG1) ئاتىدا ھەرپ بەك كۆپ."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTEXISTS & ERRCODE_RES_MASK)\n"
"string.text"
msgid "$(ARG1) does not exist."
msgstr "$(ARG1) مەۋجۇد ئەمەس."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTEXISTSPATH & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The path $(ARG1) does not exist."
msgstr "يول $(ARG1) مەۋجۇد ئەمەس."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTSUPPORTED & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The operation on $(ARG1) is not supported on this operating system."
msgstr "$(ARG1) بۇ مەشغۇلاتنى نۆۋەتتىكى مەشغۇلات سىستېمىسى قوللىمايدۇ."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTADIRECTORY & ERRCODE_RES_MASK)\n"
"string.text"
msgid "$(ARG1) is not a directory."
msgstr "$(ARG1) مۇندەرىجە ئەمەس."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTAFILE & ERRCODE_RES_MASK)\n"
"string.text"
msgid "$(ARG1) is not a file."
msgstr "$(ARG1) ھۆججەت ئەمەس."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_OUTOFSPACE & ERRCODE_RES_MASK)\n"
"string.text"
msgid "There is no space left on device $(ARG1)."
msgstr "$(ARG1) ئۈسكۈنىدە يېتەرلىك بوشلۇق يوق."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_TOOMANYOPENFILES & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The operation on $(ARG1) cannot be performed because too many files are already open."
msgstr "بەك كۆپ ھۆججەت ئېچىلغانلىقتىن $(ARG1) دىكى مەشغۇلاتنى ئىجرا قىلالمايدۇ."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_OUTOFMEMORY & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The operation on $(ARG1) cannot be performed because there is no more memory available."
msgstr "ئەسلەك يېتىشمىگەنلىكتىن $(ARG1) دىكى مەشغۇلاتنى ئىجرا قىلالمايدۇ."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_PENDING & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The operation on $(ARG1) cannot continue because more data is pending."
msgstr "نۇرغۇن سانلىق مەلۇمات بېكىتىلمىگەن ھالەتتە بولغانلىقتىن $(ARG1) دىكى مەشغۇلاتنى داۋاملىق ئىجار قىلالمايدۇ."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_RECURSIVE & ERRCODE_RES_MASK)\n"
"string.text"
msgid "$(ARG1) cannot be copied into itself."
msgstr "$(ARG1) ئۆزىنى ئۆزىگە كۆچۈرەلمەيدۇ."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_UNKNOWN & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Unknown input/output error while accessing $(ARG1)."
msgstr "$(ARG1) زىيارەت قىلغاندا نامەلۇم كىرگۈزۈش/چىقىرىش خاتالىقى كۆرۈلدى."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_WRITEPROTECTED & ERRCODE_RES_MASK)\n"
"string.text"
msgid "$(ARG1) is write protected."
msgstr "$(ARG1) يېزىشتىن قوغدالغان."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_WRONGFORMAT & ERRCODE_RES_MASK)\n"
"string.text"
msgid "$(ARG1) is not in the correct format."
msgstr "$(ARG1) پىچىمى خاتا."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_WRONGVERSION & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The version of $(ARG1) is not correct."
msgstr "$(ARG1) نەشرى خاتا."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTEXISTS_VOLUME & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Drive $(ARG1) does not exist."
msgstr "قوزغاتقۇچ $(ARG1) مەۋجۇد ئەمەس."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTEXISTS_FOLDER & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Folder $(ARG1) does not exist."
msgstr "مۇندەرىجە $(ARG1) مەۋجۇد ئەمەس."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_WRONGJAVA & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The installed Java version is not supported."
msgstr "قاچىلانغان Java نەشرىنى قوللىمايدۇ."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_WRONGJAVA_VERSION & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The installed Java version $(ARG1) is not supported."
msgstr "قاچىلانغان Java نەشرى$(ARG1) نى قوللىمايدۇ."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_WRONGJAVA_MIN & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The installed Java version is not supported, at least version $(ARG1) is required."
msgstr "قاچىلانغان Java نەشرىنى قوللىمايدۇ، ئەڭ تۆۋەن بولغاندىمۇ $(ARG1) نەشرىنى قاچىلاش لازىم."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_WRONGJAVA_VERSION_MIN & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The installed Java version $(ARG1) is not supported, at least version $(ARG2) is required."
msgstr "قاچىلانغان Java نەشرى$(ARG1) نى قوللىمايدۇ، ئەڭ تۆۋەن بولغاندىمۇ $(ARG2) نەشرىنى قاچىلاش لازىم."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_BADPARTNERSHIP & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The data associated with the partnership is corrupted."
msgstr "بۇ شېرىككە مۇناسىۋەتلىك سانلىق مەلۇمات بۇزۇلغان."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_BADPARTNERSHIP_NAME & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The data associated with the partnership $(ARG1) is corrupted."
msgstr "بۇ $(ARG1) شېرىككە مۇناسىۋەتلىك سانلىق مەلۇمات بۇزۇلغان."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTREADY_VOLUME & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Volume $(ARG1) is not ready."
msgstr "$(ARG1) ئەن تەييارلانمىدى."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTREADY_REMOVABLE & ERRCODE_RES_MASK)\n"
"string.text"
msgid "$(ARG1) is not ready; please insert a storage medium."
msgstr "$(ARG1) تەييارلانمىدى؛ ساقلاش ۋاسىتىسى قىستۇرۇڭ."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_NOTREADY_VOLUME_REMOVABLE & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Volume $(ARG1) is not ready; please insert a storage medium."
msgstr "$(ARG1) ئەن تەييارلانمىدى؛ ساقلاش ۋاسىتىسى قىستۇرۇڭ."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_WRONGMEDIUM & ERRCODE_RES_MASK)\n"
"string.text"
msgid "Please insert disk $(ARG1)."
msgstr "$(ARG1) دىسكىنى قىستۇرۇڭ."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_CANTCREATE_NONAME & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The object cannot be created in directory $(ARG1)."
msgstr "ئوبيېكتنى $(ARG1) مۇندەرىجىدە قۇرالمىدى."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_UNSUPPORTEDOVERWRITE & ERRCODE_RES_MASK)\n"
"string.text"
msgid "%PRODUCTNAME cannot keep files from being overwritten when this transmission protocol is used. Do you want to continue anyway?"
msgstr "بۇ يوللاش كېلىشىمىنى ئىشلەتكەندە %PRODUCTNAME ھۆججەت قاپلىنىشىنىڭ ئالدىنى ئالالمايدۇ. داۋاملاشتۇرۇۋېرەمسىز؟"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_BROKENPACKAGE & ERRCODE_RES_MASK)\n"
"string.text"
msgid ""
"The file '$(ARG1)' is corrupt and therefore cannot be opened. %PRODUCTNAME can try to repair the file.\n"
"\n"
"The corruption could be the result of document manipulation or of structural document damage due to data transmission.\n"
"\n"
"We recommend that you do not trust the content of the repaired document.\n"
"Execution of macros is disabled for this document.\n"
"\n"
"Should %PRODUCTNAME repair the file?\n"
msgstr ""
"بۇ '$(ARG1)' ھۆججەت بۇزۇلغان، شۇڭلاشقا ئاچالمايدۇ. %PRODUCTNAME بۇ ھۆججەتنى ئوڭشاشنى سىنىيالايدۇ .\n"
"\n"
"بۇزۇلۇش سەۋەبى پۈتۈككە نىسبەتەن ئېلىپ بېرىلغان مەشغۇلات نامۇۋاپىق ياكى سانلىق مەلۇمات يوللاشتىكى ھۆججەت قۇرۇلمىسى بۇزۇلغان بولۇشى مۇمكىن.\n"
"\n"
"نۆۋەتتىكى پۈتۈكنىڭ مەزمۇنىغا ئىشەنمەسلىكىڭىزنى تەۋسىيە قىلىمىز.\n"
"بۇ پۈتۈكنىڭ ماكروسى ئىجرا قىلىشتىن چەكلەندى.\n"
"\n"
"%PRODUCTNAME ھۆججەتنى ئوڭشىسۇنمۇ؟\n"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_IO_BROKENPACKAGE_CANTREPAIR & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The file '$(ARG1)' could not be repaired and therefore cannot be opened."
msgstr "'$(ARG1)' ھۆججەتنى ئوڭشىيالمىدى، شۇڭلاشقا بۇ ھۆجقەتنى ئاچالمايدۇ."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_CONFIGURATION_BROKENDATA_NOREMOVE & ERRCODE_RES_MASK)\n"
"string.text"
msgid ""
"Configuration data in '$(ARG1)' is corrupted. Without this data some functions may not operate correctly.\n"
"Do you want to continue startup of %PRODUCTNAME without the corrupted configuration data?"
msgstr ""
"'$(ARG1)' دىكى سەپلىمە سانلىق مەلۇماتى بۇزۇلغان. بۇ سانلىق مەلۇمات يوق، بەزى ئىقتىدارلار نورمال ئىجرا بولماسلىقى مۇمكىن.\n"
"بۇزۇلغان سەپلىمە سانلىق مەلۇماتى يوق ئەھۋالدا %PRODUCTNAME نى قوزغىتىۋېرەمسىز؟"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_CONFIGURATION_BROKENDATA_WITHREMOVE & ERRCODE_RES_MASK)\n"
"string.text"
msgid ""
"The personal configuration file '$(ARG1)' is corrupted and must be deleted to continue. Some of your personal settings may be lost.\n"
"Do you want to continue startup of %PRODUCTNAME without the corrupted configuration data?"
msgstr ""
"'$(ARG1)' شەخسىي سەپلىمە سانلىق مەلۇماتى بۇزۇلغان. ئۇنى ئۆچۈرگەندىلا ئاندىن داۋاملاشتۇرغىلى بولىدۇ. بەزى شەخسىي تەڭشەك يوقالغان بولۇشى مۇمكىن.\n"
"بۇزۇلغان سەپلىمە سانلىق مەلۇماتى يوق ئەھۋالدا %PRODUCTNAME نى قوزغىتىۋېرەمسىز؟"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_CONFIGURATION_BACKENDMISSING & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The configuration data source '$(ARG1)' is unavailable. Without this data some functions may not operate correctly."
msgstr "'$(ARG1)' سانلىق مەلۇمات مەنبەسى سەپلەشنى ئىشلەتكىلى بولمايدۇ. بۇ سانلىق مەلۇمات يوق، بەزى ئىقتىدارلار نورمال ئىجرا بولماسلىقى مۇمكىن."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_CONFIGURATION_BACKENDMISSING_WITHRECOVER & ERRCODE_RES_MASK)\n"
"string.text"
msgid ""
"The configuration data source '$(ARG1)' is unavailable. Without this data some functions may not operate correctly.\n"
"Do you want to continue startup of %PRODUCTNAME without the missing configuration data?"
msgstr ""
"'$(ARG1)' سانلىق مەلۇمات مەنبەسى سەپلەشنى ئىشلەتكىلى بولمايدۇ. بۇ سانلىق مەلۇمات يوق، بەزى ئىقتىدارلار نورمال ئىجرا بولماسلىقى مۇمكىن.\n"
"سەپلىمە سانلىق مەلۇماتى يوق ئەھۋالدا %PRODUCTNAME نى قوزغىتىۋېرەمسىز؟"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_INVALID_XFORMS_SUBMISSION_DATA & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The form contains invalid data. Do you still want to continue?"
msgstr "بۇ جەدۋەل ئىناۋەتسىز سانلىق مەلۇماتنى ئۆز ئىچىگە ئالغان. داۋاملاشتۇرىۋېرەمسىز؟"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_LOCKING_LOCKED & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The file $(ARG1) is locked by another user. Currently, another write access to this file cannot be granted."
msgstr "بۇ $(ARG1) ھۆججەتنى باشقا ئىشلەتكۈچى قۇلۇپلىغان. نۆۋەتتە، بۇ ھۆججەتكە يەنە بىر يېزىش زىيارەت ھوقۇقىغا يول قويۇلمايدۇ."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_LOCKING_LOCKED_SELF & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The file $(ARG1) is locked by yourself. Currently, another write access to this file cannot be granted."
msgstr "بۇ $(ARG1) ھۆججەتنى ئۆزىڭىز قۇلۇپلىغان. نۆۋەتتە، بۇ ھۆججەتكە يەنە بىر يېزىش زىيارەت ھوقۇقىغا يول قويۇلمايدۇ."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_LOCKING_NOT_LOCKED & ERRCODE_RES_MASK)\n"
"string.text"
msgid "The file $(ARG1) is currently not locked by yourself."
msgstr "بۇ $(ARG1) ھۆججەتنى نۆۋەتتە ئۆزىڭىز قۇلۇپلىمىغان."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_LOCKING_LOCK_EXPIRED & ERRCODE_RES_MASK)\n"
"string.text"
msgid ""
"The previously obtained lock for file $(ARG1) has expired.\n"
"This can happen due to problems on the server managing the file lock. It cannot be guaranteed that write operations on this file will not overwrite changes done by other users!"
msgstr ""
"ئىلگىرى ئېرىشكەن $(ARG1) ھۆججەت قۇلۇپىنىڭ ۋاقتى ئۆتتى.\n"
"بۇ خىل ئەھۋال باشقۇرۇش سەۋەبىدىن مۇلازىمىتىردىكى ھۆججەت قۇلۇپلانغان بولۇشى مۇمكىن. بۇ ھۆججەتنىڭ يېزىش مەشغۇلاتى باشقا ئىشلەتكۈچىنىڭ ئۆزگەرتىشىنى قاپلىۋېتىشكە كاپالەتلىك قىلالمايدۇ."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_UNKNOWNAUTH_UNTRUSTED)\n"
"string.text"
msgid ""
"Unable to verify the identity of $(ARG1) site.\n"
"\n"
"Before accepting this certificate, you should examine this site's certificate carefully. Are you willing to accept this certificate for the purpose of identifying the Web site $(ARG1)?"
msgstr ""
"$(ARG1) بېكەتنىڭ سالاھىيىتىنى دەلىللىيەلمىدى. \n"
"\n"
"بۇ گۇۋاھنامىنى قوبۇل قىلىشتىن ئىلگىرى، بۇ بېكەتنىڭ گۇۋاھنامىسىنى تەپسىلىي تەكشۈرۈڭ. سىز بۇ گۇۋاھنامىنى قوبۇل قىلىپ $(ARG1) تور بېكەتنى تونۇماقچىمۇ؟"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_SSLWARN_EXPIRED_1)\n"
"string.text"
msgid ""
"$(ARG1) is a site that uses a security certificate to encrypt data during transmission, but its certificate expired on $(ARG2).\n"
"\n"
"You should check to make sure that your computer's time is correct."
msgstr ""
"$(ARG1) بېكەت يوللاش جەريانىدا بىخەتەرلىك دەلىللەپ شىفىرلىق سانلىق مەلۇمات ئىشلىتىدىغان تور بېكەت، ئەمما بۇ بېكەتنىڭ گۇۋاھنامىسى $(ARG2) ۋاقتى ئۆتىدۇ. \n"
"\n"
"كومپيۇتېر ۋاقتىڭىزنىڭ توغرىلىقىنى جەزملەڭ."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_SSLWARN_DOMAINMISMATCH_1)\n"
"string.text"
msgid ""
"You have attempted to establish a connection with $(ARG1). However, the security certificate presented belongs to $(ARG2). It is possible, though unlikely, that someone may be trying to intercept your communication with this web site.\n"
"\n"
"If you suspect the certificate shown does not belong to $(ARG1), please cancel the connection and notify the site administrator.\n"
"\n"
"Would you like to continue anyway?"
msgstr ""
"سىز$(ARG1) غا ئۇلىنىشنى سىناۋاتىسىز. ئەمما بىخەتەرلىك گۇۋاھنامىسى $(ARG2) غا تەۋە. مۇمكىنچىلىكى ئانچە چوڭ بولمىسىمۇ ئەمما باشقىلار سىزنىڭ تور بېكەت بىلەن بولغان ئالاقىڭىزنى ئۈزۈشكە ئۇرۇنۇۋاتىدۇ. \n"
"\n"
"ئەگەر كۆرسىتىلگەن گۇۋاھنامىنىڭ $(ARG1) غا تەۋە ئىكەنلىكىدىن گۇمانلانسىڭىز بۇ ئۇلىنىشتىن ۋاز كېچىپ تور بېكەت باشقۇرغۇچىغا مەلۇم قىلىڭ.\n"
"\n"
"داۋاملاشتۇرۇۋېرەمسىز؟"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_SSLWARN_INVALID_1)\n"
"string.text"
msgid ""
"The certificate could not be validated. You should examine this site's certificate carefully.\n"
"\n"
"If you suspect the certificate shown, please cancel the connection and notify the site administrator."
msgstr ""
"بۇ گۇۋاھنامىنى دەلىللىيەلمىدى. بۇ تور بېكەتنىڭ گۇۋاھنامىسىنى تەپسىلىي تەڭشۈرۈڭ.\n"
"\n"
"ئەگەر كۆرۈنگەن گۇۋاھنامىدىن گۇمانلانسىڭىز، ئۇلىنىشتىن ۋاز كېچىپ بۇ بېكەتنىڭ باشقۇرغۇچىسىغا مەلۇم قىلىڭ."

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(TITLE_UUI_SSLWARN_DOMAINMISMATCH)\n"
"string.text"
msgid "Security Warning: Domain Name Mismatch"
msgstr "بىخەتەرلىك ئاگاھلاندۇرۇش: دائىرە ئاتى ماسلاشمىدى"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(TITLE_UUI_SSLWARN_EXPIRED)\n"
"string.text"
msgid "Security Warning: Server Certificate Expired"
msgstr "بىخەتەرلىك ئاگاھلاندۇرۇش: مۇلازىمىتىر گۇۋاھنامىسىنىڭ ۋاقتى ئۆتكەن"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(TITLE_UUI_SSLWARN_INVALID)\n"
"string.text"
msgid "Security Warning: Server Certificate Invalid"
msgstr "بىخەتەرلىك ئاگاھلاندۇرۇش: مۇلازىمىتىر گۇۋاھنامىسى ئىناۋەتسىز"

#: ids.src
msgctxt ""
"ids.src\n"
"RID_UUI_ERRHDL\n"
"(ERRCODE_UUI_CANNOT_ACTIVATE_FACTORY & ERRCODE_RES_MASK)\n"
"string.text"
msgid ""
"Component cannot be loaded, possibly broken or incomplete installation.\n"
"Full error message:\n"
"\n"
" $(ARG1)."
msgstr ""
"بۆلەكنى يۈكلىيەلمەيدۇ، ئورنىتىش بۇزۇلغان ياكى تولۇق ئەمەس.\n"
"تولۇق خاتالىق ئۇچۇرى:\n"
"\n"
"$(ARG1)."

#: lockfailed.src
msgctxt ""
"lockfailed.src\n"
"STR_LOCKFAILED_TITLE\n"
"string.text"
msgid "Document Could Not Be Locked"
msgstr "پۈتۈكنى قۇلۇپلىيالمايدۇ"

#: lockfailed.src
msgctxt ""
"lockfailed.src\n"
"STR_LOCKFAILED_MSG\n"
"string.text"
msgid "The file could not be locked for exclusive access by %PRODUCTNAME, due to missing permission to create a lock file on that file location."
msgstr "%PRODUCTNAME ئۆز ئالدىغا يالغۇر زىيارەت قىلىشقا ئىشلىتىش ئۈچۈن بۇ ھۆججەتنى قۇلۇپلىيالمىدى، بۇ ھۆججەت تۇرۇشلۇق ئورۇندا ھۆججەت قۇلۇپلاش ھوقۇقىنىڭ كەم بولۇشىدىن بولۇشى مۇمكىن."

#: lockfailed.src
msgctxt ""
"lockfailed.src\n"
"STR_LOCKFAILED_DONTSHOWAGAIN\n"
"string.text"
msgid "~Do not show this message again"
msgstr "بۇ ئۇچۇرنى قايتا كۆرسەتمە(~D)"

#: nameclashdlg.src
msgctxt ""
"nameclashdlg.src\n"
"STR_RENAME_OR_REPLACE\n"
"string.text"
msgid ""
"A file with the name \"%NAME\" already exists in the location \"%FOLDER\".\n"
"Choose Replace to overwrite the existing file or provide a new name."
msgstr ""
"«%NAME» ئاتلىق ھۆججەت «%FOLDER» مەۋجۇد!\n"
"ئالماشتۇرنى تاللىسىڭىز ئەسلىدىكى ھۆججەتنى قاپلىۋېتىدۇ، ئۇنداق بولمىسا يېڭى ھۆججەت ئاتىنى كىرگۈزۈڭ."

#: nameclashdlg.src
msgctxt ""
"nameclashdlg.src\n"
"STR_NAME_CLASH_RENAME_ONLY\n"
"string.text"
msgid ""
"A file with the name \"%NAME\" already exists in the location \"%FOLDER\".\n"
"Please enter a new name."
msgstr ""
"«%NAME» ئاتلىق ھۆججەت «%FOLDER» دا مەۋجۇد!\n"
"يېڭى بىر ھۆججەت ئاتىنى كىرگۈزۈڭ."

#: nameclashdlg.src
msgctxt ""
"nameclashdlg.src\n"
"STR_SAME_NAME_USED\n"
"string.text"
msgid "Please provide a different file name!"
msgstr "پەرقلىق ھۆججەت ئاتىدىن بىرنى كىرگۈزۈڭ!"

#: openlocked.src
msgctxt ""
"openlocked.src\n"
"STR_OPENLOCKED_TITLE\n"
"string.text"
msgid "Document in Use"
msgstr "ھۆججەت ئىشلىتىلىۋاتىدۇ"

#: openlocked.src
msgctxt ""
"openlocked.src\n"
"STR_OPENLOCKED_MSG\n"
"string.text"
msgid ""
"Document file '$(ARG1)' is locked for editing by:\n"
"\n"
"$(ARG2)\n"
"\n"
"Open document read-only or open a copy of the document for editing.\n"
"\n"
msgstr ""
"'$(ARG1)' پۈتۈك ھۆججىتىنىڭ تەھرىرلەش ئىقتىدارىنى قۇلۇپلىغۇچى:\n"
"\n"
"$(ARG2)\n"
"\n"
"پۈتۈكنى ئوقۇشقىلا بولىدىغان ھالەتتە ئېچىڭ ياكى كۆچۈرمە نۇسخىسىدىن بىرنى ئېچىپ تەھرىرلەڭ.\n"
"\n"

#: openlocked.src
msgctxt ""
"openlocked.src\n"
"STR_OPENLOCKED_OPENREADONLY_BTN\n"
"string.text"
msgid "Open ~Read-Only"
msgstr "ئوقۇشقىلا بولىدىغان ئاچ(~R)"

#: openlocked.src
msgctxt ""
"openlocked.src\n"
"STR_OPENLOCKED_OPENCOPY_BTN\n"
"string.text"
msgid "Open ~Copy"
msgstr "كۆچۈرمە نۇسخا ئاچ(~C)"

#: openlocked.src
msgctxt ""
"openlocked.src\n"
"STR_UNKNOWNUSER\n"
"string.text"
msgid "Unknown User"
msgstr "نامەلۇم ئىشلەتكۈچى"

#: passworddlg.src
msgctxt ""
"passworddlg.src\n"
"STR_ENTER_PASSWORD_TO_OPEN\n"
"string.text"
msgid "Enter password to open file: \n"
msgstr "ھۆججەت ئېچىش ئۈچۈن ئىم كىرگۈزۈڭ: \n"

#: passworddlg.src
msgctxt ""
"passworddlg.src\n"
"STR_ENTER_PASSWORD_TO_MODIFY\n"
"string.text"
msgid "Enter password to modify file: \n"
msgstr "ھۆججەت ئۆزگەرتىش ئۈچۈن ئىم كىرگۈزۈڭ: \n"

#: passworddlg.src
msgctxt ""
"passworddlg.src\n"
"STR_ENTER_SIMPLE_PASSWORD\n"
"string.text"
msgid "Enter password: "
msgstr "ئىم كىرگۈزۈڭ: "

#: passworddlg.src
msgctxt ""
"passworddlg.src\n"
"STR_CONFIRM_SIMPLE_PASSWORD\n"
"string.text"
msgid "Confirm password: "
msgstr "جەزملەش ئىم: "

#: passworddlg.src
msgctxt ""
"passworddlg.src\n"
"STR_TITLE_CREATE_PASSWORD\n"
"string.text"
msgid "Set Password"
msgstr "ئىم تەڭشەك"

#: passworddlg.src
msgctxt ""
"passworddlg.src\n"
"STR_TITLE_ENTER_PASSWORD\n"
"string.text"
msgid "Enter Password"
msgstr "ئىم كىرگۈزۈڭ"

#: passworddlg.src
msgctxt ""
"passworddlg.src\n"
"STR_PASSWORD_MISMATCH\n"
"string.text"
msgid "The confirmation password did not match the password. Set the password again by entering the same password in both boxes."
msgstr "جەزملەش ئىم بىلەن ئىم ماس كەلمىدى. ئىككى كاتەكچىگە ئىمنى ئوخشاش كىرگۈزۈپ ئىمنى تەڭشەڭ."

#: passworderrs.src
msgctxt ""
"passworderrs.src\n"
"STR_ERROR_PASSWORD_TO_OPEN_WRONG\n"
"string.text"
msgid "The password is incorrect. The file cannot be opened."
msgstr "ئىم خاتا، ھۆججەتنى ئاچالمىدى."

#: passworderrs.src
msgctxt ""
"passworderrs.src\n"
"STR_ERROR_PASSWORD_TO_MODIFY_WRONG\n"
"string.text"
msgid "The password is incorrect. The file cannot be modified."
msgstr "ئىم خاتا، ھۆججەتنى تۈزىتەلمىدى."

#: passworderrs.src
msgctxt ""
"passworderrs.src\n"
"STR_ERROR_MASTERPASSWORD_WRONG\n"
"string.text"
msgid "The master password is incorrect."
msgstr "ئاساسىي ئىم خاتا."

#: passworderrs.src
msgctxt ""
"passworderrs.src\n"
"STR_ERROR_SIMPLE_PASSWORD_WRONG\n"
"string.text"
msgid "The password is incorrect."
msgstr "ئىم خاتا."

#: passworderrs.src
msgctxt ""
"passworderrs.src\n"
"STR_ERROR_PASSWORDS_NOT_IDENTICAL\n"
"string.text"
msgid "The password confirmation does not match."
msgstr "جەزملەنگەن ئىم ماس كەلمىدى."

#: trylater.src
msgctxt ""
"trylater.src\n"
"STR_TRYLATER_TITLE\n"
"string.text"
msgid "Document in Use"
msgstr "ھۆججەت ئىشلىتىلىۋاتىدۇ"

#: trylater.src
msgctxt ""
"trylater.src\n"
"STR_TRYLATER_MSG\n"
"string.text"
msgid ""
"Document file '$(ARG1)' is locked for editing by:\n"
"\n"
"$(ARG2)\n"
"\n"
"Try again later to save document or save a copy of that document.\n"
"\n"
msgstr ""
"'$(ARG1)' پۈتۈك ھۆججىتى تەھرىرلەشتىن ساقلىنىش ئۈچۈن \n"
" \n"
"$(ARG2)\n"
"\n"
"تەرىپىدىن قۇلۇپلاندى، سەل تۇرۇپ ھۆججەت ساقلاشنى سىناپ بېقىڭ ياكى بۇ ھۆججەتنىڭ كۆپەيتىلمىسىدىن بىرنى ساقلاڭ.\n"
"\n"

#: trylater.src
msgctxt ""
"trylater.src\n"
"STR_TRYLATER_RETRYSAVING_BTN\n"
"string.text"
msgid "~Retry Saving"
msgstr "ساقلاشنى قايتا سىنا(~R)"

#: trylater.src
msgctxt ""
"trylater.src\n"
"STR_TRYLATER_SAVEAS_BTN\n"
"string.text"
msgid "~Save As..."
msgstr "باشقا ئاتتا ساقلا(~S)…"

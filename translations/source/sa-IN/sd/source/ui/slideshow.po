#. extracted from sd/source/ui/slideshow
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2014-05-02 00:04+0200\n"
"PO-Revision-Date: 2014-09-29 06:10+0000\n"
"Last-Translator: babitashinde123 <babitashinde123@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: sa_IN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Pootle 2.5.1\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1411971044.000000\n"

#: slideshow.src
msgctxt ""
"slideshow.src\n"
"RID_SLIDESHOW_CONTEXTMENU\n"
"CM_NEXT_SLIDE\n"
"menuitem.text"
msgid "~Next"
msgstr "अग्रिमः"

#: slideshow.src
msgctxt ""
"slideshow.src\n"
"RID_SLIDESHOW_CONTEXTMENU\n"
"CM_PREV_SLIDE\n"
"menuitem.text"
msgid "~Previous"
msgstr "पूर्वम्"

#: slideshow.src
msgctxt ""
"slideshow.src\n"
"RID_SLIDESHOW_CONTEXTMENU.CM_GOTO\n"
"CM_FIRST_SLIDE\n"
"menuitem.text"
msgid "~First Slide"
msgstr "प्रथमावसर्पिणी"

#: slideshow.src
msgctxt ""
"slideshow.src\n"
"RID_SLIDESHOW_CONTEXTMENU.CM_GOTO\n"
"CM_LAST_SLIDE\n"
"menuitem.text"
msgid "~Last Slide"
msgstr "अंतिेमावसर्पिणी"

#: slideshow.src
msgctxt ""
"slideshow.src\n"
"RID_SLIDESHOW_CONTEXTMENU\n"
"CM_GOTO\n"
"menuitem.text"
msgid "~Go to Slide"
msgstr "अवसर्पिणीं गच्छ"

#: slideshow.src
#, fuzzy
msgctxt ""
"slideshow.src\n"
"RID_SLIDESHOW_CONTEXTMENU\n"
"CM_PEN_MODE\n"
"menuitem.text"
msgid "Mouse pointer as ~Pen"
msgstr "मौस्सूचकः लेखनी इव"

#: slideshow.src
#, fuzzy
msgctxt ""
"slideshow.src\n"
"RID_SLIDESHOW_CONTEXTMENU.CM_WIDTH_PEN\n"
"CM_WIDTH_PEN_VERY_THIN\n"
"menuitem.text"
msgid "~Very thin"
msgstr "अतिदृढः"

#: slideshow.src
#, fuzzy
msgctxt ""
"slideshow.src\n"
"RID_SLIDESHOW_CONTEXTMENU.CM_WIDTH_PEN\n"
"CM_WIDTH_PEN_THIN\n"
"menuitem.text"
msgid "~Thin"
msgstr "कृशः"

#: slideshow.src
#, fuzzy
msgctxt ""
"slideshow.src\n"
"RID_SLIDESHOW_CONTEXTMENU.CM_WIDTH_PEN\n"
"CM_WIDTH_PEN_NORMAL\n"
"menuitem.text"
msgid "~Normal"
msgstr "सामान्यम्"

#: slideshow.src
msgctxt ""
"slideshow.src\n"
"RID_SLIDESHOW_CONTEXTMENU.CM_WIDTH_PEN\n"
"CM_WIDTH_PEN_THICK\n"
"menuitem.text"
msgid "~Thick"
msgstr "घणम् (~T)"

#: slideshow.src
#, fuzzy
msgctxt ""
"slideshow.src\n"
"RID_SLIDESHOW_CONTEXTMENU.CM_WIDTH_PEN\n"
"CM_WIDTH_PEN_VERY_THICK\n"
"menuitem.text"
msgid "~Very Thick"
msgstr "अतिदृढः"

#: slideshow.src
msgctxt ""
"slideshow.src\n"
"RID_SLIDESHOW_CONTEXTMENU\n"
"CM_WIDTH_PEN\n"
"menuitem.text"
msgid "~Pen Width"
msgstr "लेखनीविस्तारः (~P)"

#: slideshow.src
#, fuzzy
msgctxt ""
"slideshow.src\n"
"RID_SLIDESHOW_CONTEXTMENU\n"
"CM_COLOR_PEN\n"
"menuitem.text"
msgid "~Change pen Color..."
msgstr "अक्षरवर्णं परिवर्तय"

#: slideshow.src
msgctxt ""
"slideshow.src\n"
"RID_SLIDESHOW_CONTEXTMENU\n"
"CM_ERASE_ALLINK\n"
"menuitem.text"
msgid "~Erase all ink on Slide"
msgstr "स्लैड् मध्ये सर्वाणि इङ्क् अवमर्श (~E)"

#: slideshow.src
msgctxt ""
"slideshow.src\n"
"RID_SLIDESHOW_CONTEXTMENU.CM_SCREEN\n"
"CM_SCREEN_BLACK\n"
"menuitem.text"
msgid "~Black"
msgstr "श्यामम्"

#: slideshow.src
msgctxt ""
"slideshow.src\n"
"RID_SLIDESHOW_CONTEXTMENU.CM_SCREEN\n"
"CM_SCREEN_WHITE\n"
"menuitem.text"
msgid "~White"
msgstr "श्वेतम्"

#: slideshow.src
msgctxt ""
"slideshow.src\n"
"RID_SLIDESHOW_CONTEXTMENU\n"
"CM_SCREEN\n"
"menuitem.text"
msgid "~Screen"
msgstr "पटः"

#: slideshow.src
msgctxt ""
"slideshow.src\n"
"RID_SLIDESHOW_CONTEXTMENU\n"
"CM_EDIT_PRESENTATION\n"
"menuitem.text"
msgid "E~dit Presentation"
msgstr ""

#: slideshow.src
msgctxt ""
"slideshow.src\n"
"RID_SLIDESHOW_CONTEXTMENU\n"
"CM_ENDSHOW\n"
"menuitem.text"
msgid "~End Show"
msgstr "प्रदर्शनं समापय"

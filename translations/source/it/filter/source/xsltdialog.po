#. extracted from filter/source/xsltdialog
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2014-05-02 00:04+0200\n"
"PO-Revision-Date: 2014-08-02 19:07+0000\n"
"Last-Translator: Valter <valtermura@gmail.com>\n"
"Language-Team: Italian <l10n@it.libreoffice.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1407006420.000000\n"

#: xmlfilterdialogstrings.src
msgctxt ""
"xmlfilterdialogstrings.src\n"
"STR_COLUMN_HEADER_NAME\n"
"string.text"
msgid "Name"
msgstr "Nome"

#: xmlfilterdialogstrings.src
msgctxt ""
"xmlfilterdialogstrings.src\n"
"STR_COLUMN_HEADER_TYPE\n"
"string.text"
msgid "Type"
msgstr "Tipo"

#: xmlfilterdialogstrings.src
msgctxt ""
"xmlfilterdialogstrings.src\n"
"STR_UNKNOWN_APPLICATION\n"
"string.text"
msgid "Unknown"
msgstr "Sconosciuto"

#: xmlfilterdialogstrings.src
msgctxt ""
"xmlfilterdialogstrings.src\n"
"STR_IMPORT_ONLY\n"
"string.text"
msgid "import filter"
msgstr "Importa filtro"

#: xmlfilterdialogstrings.src
msgctxt ""
"xmlfilterdialogstrings.src\n"
"STR_IMPORT_EXPORT\n"
"string.text"
msgid "import/export filter"
msgstr "Importa/esporta filtro"

#: xmlfilterdialogstrings.src
msgctxt ""
"xmlfilterdialogstrings.src\n"
"STR_EXPORT_ONLY\n"
"string.text"
msgid "export filter"
msgstr "Esporta filtro"

#: xmlfilterdialogstrings.src
msgctxt ""
"xmlfilterdialogstrings.src\n"
"STR_WARN_DELETE\n"
"string.text"
msgid "Do you really want to delete the XML Filter '%s'? This action cannot be undone."
msgstr "Vuoi davvero eliminare il filtro XML '%s'? L'azione è irreversibile."

#: xmlfilterdialogstrings.src
msgctxt ""
"xmlfilterdialogstrings.src\n"
"STR_ERROR_FILTER_NAME_EXISTS\n"
"string.text"
msgid "An XML filter with the name '%s' already exists. Please enter a different name."
msgstr "Un filtro XML dal nome '%s' esiste già. Indica un nome diverso."

#: xmlfilterdialogstrings.src
msgctxt ""
"xmlfilterdialogstrings.src\n"
"STR_ERROR_TYPE_NAME_EXISTS\n"
"string.text"
msgid "The name for the user interface '%s1' is already used by the XML filter '%s2'. Please enter a different name."
msgstr "Il nome per l'interfaccia utente '%s1' è già utilizzato dal filtro XML '%s2'. Indica un nome diverso."

#: xmlfilterdialogstrings.src
msgctxt ""
"xmlfilterdialogstrings.src\n"
"STR_ERROR_EXPORT_XSLT_NOT_FOUND\n"
"string.text"
msgid "The XSLT for export cannot be found. Please enter a valid path."
msgstr "Impossibile trovare l'XSLT per l'esportazione. Indica un percorso valido."

#: xmlfilterdialogstrings.src
msgctxt ""
"xmlfilterdialogstrings.src\n"
"STR_ERROR_IMPORT_XSLT_NOT_FOUND\n"
"string.text"
msgid "The XSLT for import cannot be found. Please enter a valid path."
msgstr "Impossibile trovare l'XSLT. Indica un percorso valido."

#: xmlfilterdialogstrings.src
msgctxt ""
"xmlfilterdialogstrings.src\n"
"STR_ERROR_IMPORT_TEMPLATE_NOT_FOUND\n"
"string.text"
msgid "The given import template cannot be found. Please enter a valid path."
msgstr "Impossibile trovare il modello d'importazione richiesto. Indica un percorso valido."

#: xmlfilterdialogstrings.src
msgctxt ""
"xmlfilterdialogstrings.src\n"
"STR_NOT_SPECIFIED\n"
"string.text"
msgid "Not specified"
msgstr "Non specificato"

#: xmlfilterdialogstrings.src
msgctxt ""
"xmlfilterdialogstrings.src\n"
"STR_DEFAULT_FILTER_NAME\n"
"string.text"
msgid "New Filter"
msgstr "Filtro nuovo"

#: xmlfilterdialogstrings.src
msgctxt ""
"xmlfilterdialogstrings.src\n"
"STR_DEFAULT_UI_NAME\n"
"string.text"
msgid "Untitled"
msgstr "Senza nome"

#: xmlfilterdialogstrings.src
msgctxt ""
"xmlfilterdialogstrings.src\n"
"STR_UNDEFINED_FILTER\n"
"string.text"
msgid "undefined filter"
msgstr "Filtro non definito"

#: xmlfilterdialogstrings.src
msgctxt ""
"xmlfilterdialogstrings.src\n"
"STR_FILTER_HAS_BEEN_SAVED\n"
"string.text"
msgid "The XML filter '%s' has been saved as package '%s'. "
msgstr "Il filtro XML '%s' è stato salvato come pacchetto '%s'."

#: xmlfilterdialogstrings.src
msgctxt ""
"xmlfilterdialogstrings.src\n"
"STR_FILTERS_HAVE_BEEN_SAVED\n"
"string.text"
msgid "%s XML filters have been saved in the package '%s'."
msgstr "I filtri %s XML sono stati salvati nel pacchetto '%s'."

#: xmlfilterdialogstrings.src
msgctxt ""
"xmlfilterdialogstrings.src\n"
"STR_FILTER_PACKAGE\n"
"string.text"
msgid "XSLT filter package"
msgstr "Pacchetto filtri XSLT"

#: xmlfilterdialogstrings.src
msgctxt ""
"xmlfilterdialogstrings.src\n"
"STR_FILTER_INSTALLED\n"
"string.text"
msgid "The XML filter '%s' has been installed successfully."
msgstr "L'installazione del filtro XML '%s' è terminata."

#: xmlfilterdialogstrings.src
msgctxt ""
"xmlfilterdialogstrings.src\n"
"STR_FILTERS_INSTALLED\n"
"string.text"
msgid "%s XML filters have been installed successfully."
msgstr "L'installazione dei filtri XML %s è terminata."

#: xmlfilterdialogstrings.src
msgctxt ""
"xmlfilterdialogstrings.src\n"
"STR_NO_FILTERS_FOUND\n"
"string.text"
msgid "No XML filter could be installed because the package '%s' does not contain any XML filters."
msgstr "Impossibile installare i filtri XML perché il pacchetto '%s' non contiene alcun filtro XML."

#: xmlfilterdialogstrings.src
msgctxt ""
"xmlfilterdialogstrings.src\n"
"STR_XML_FILTER_LISTBOX\n"
"string.text"
msgid "XML Filter List"
msgstr "Elenco filtro XML"

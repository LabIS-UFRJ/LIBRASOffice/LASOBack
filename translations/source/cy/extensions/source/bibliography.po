#. extracted from extensions/source/bibliography
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-11-15 17:28+0000\n"
"Last-Translator: Rhoslyn Prys <rprys@yahoo.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: cy\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n==2) ? 1 : 0;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.7\n"
"X-POOTLE-MTIME: 1479230920.000000\n"

#: bib.src
msgctxt ""
"bib.src\n"
"RID_BIB_STR_FIELDSELECTION\n"
"string.text"
msgid "Field selection:"
msgstr "Dewis maes:"

#: bib.src
msgctxt ""
"bib.src\n"
"RID_BIB_STR_TABWIN_PREFIX\n"
"string.text"
msgid "Table;Query;Sql;Sql [Native]"
msgstr "Table;Query;Sql;Sql [Native]"

#: bib.src
msgctxt ""
"bib.src\n"
"RID_BIB_STR_FRAME_TITLE\n"
"string.text"
msgid "Bibliography Database"
msgstr "Cronfa Ddata Llyfryddiaeth"

#: bib.src
msgctxt ""
"bib.src\n"
"RID_MAP_QUESTION\n"
"string.text"
msgid "Do you want to edit the column arrangement?"
msgstr "Hoffech chi olygu trefniant y colofnau?"

#: bib.src
msgctxt ""
"bib.src\n"
"RID_BIB_STR_NONE\n"
"string.text"
msgid "<none>"
msgstr "<dim>"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_ERROR_PREFIX\n"
"string.text"
msgid "The following column names could not be assigned:\n"
msgstr "Methu neilltuo'r enwau colofn ganlynol:\n"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_ARTICLE\n"
"string.text"
msgid "Article"
msgstr "Erthygl"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_BOOK\n"
"string.text"
msgid "Book"
msgstr "Llyfr"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_BOOKLET\n"
"string.text"
msgid "Brochures"
msgstr "Llyfryn"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_CONFERENCE\n"
"string.text"
msgid "Conference proceedings article (BiBTeX)"
msgstr "Erthygl gweithrediadau cynhadledd (BiBTeX)"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_INBOOK\n"
"string.text"
msgid "Book excerpt"
msgstr "Dyfyniad o lyfr"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_INCOLLECTION\n"
"string.text"
msgid "Book excerpt with title"
msgstr "Dyfyniad o lyfr gyda theitl"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_INPROCEEDINGS\n"
"string.text"
msgid "Conference proceedings article"
msgstr "Erthygl gweithrediadau cynhadledd"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_JOURNAL\n"
"string.text"
msgid "Journal"
msgstr "Siwrnal"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_MANUAL\n"
"string.text"
msgid "Techn. documentation"
msgstr "Dogfennaeth dechnegol"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_MASTERSTHESIS\n"
"string.text"
msgid "Thesis"
msgstr "Thesis"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_MISC\n"
"string.text"
msgid "Miscellaneous"
msgstr "Amrywiol"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_PHDTHESIS\n"
"string.text"
msgid "Dissertation"
msgstr "Traethawd"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_PROCEEDINGS\n"
"string.text"
msgid "Conference proceedings"
msgstr "Gweithrediadau cynhadledd"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_TECHREPORT\n"
"string.text"
msgid "Research report"
msgstr "Adroddiad ymchwil"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_UNPUBLISHED\n"
"string.text"
msgid "Unpublished"
msgstr "Heb ei gyhoeddi"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_EMAIL\n"
"string.text"
msgid "E-mail"
msgstr "E-bost"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_WWW\n"
"string.text"
msgid "WWW document"
msgstr "Dogfen WWW"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_CUSTOM1\n"
"string.text"
msgid "User-defined1"
msgstr "Defnyddiwr diffinedig1"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_CUSTOM2\n"
"string.text"
msgid "User-defined2"
msgstr "Defnyddiwr diffinedig2"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_CUSTOM3\n"
"string.text"
msgid "User-defined3"
msgstr "Defnyddiwr diffinedig3"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_CUSTOM4\n"
"string.text"
msgid "User-defined4"
msgstr "Defnyddiwr diffinedig4"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_CUSTOM5\n"
"string.text"
msgid "User-defined5"
msgstr "Defnyddiwr diffinedig5"

#: sections.src
msgctxt ""
"sections.src\n"
"ST_TYPE_TITLE\n"
"string.text"
msgid "General"
msgstr "Cyffredinol"

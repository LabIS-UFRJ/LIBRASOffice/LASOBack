#. extracted from sfx2/source/doc
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2017-01-13 11:03+0000\n"
"Last-Translator: Rhoslyn Prys <rprys@yahoo.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: cy\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n==2) ? 1 : 0;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1484305396.000000\n"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_TEMPLATE_FILTER\n"
"string.text"
msgid "Templates"
msgstr "Templedi"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_SAVEDOC\n"
"string.text"
msgid "~Save"
msgstr "C~adw"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_SAVEASDOC\n"
"string.text"
msgid "Save ~As..."
msgstr "Cadw ~Fel..."

#: doc.src
msgctxt ""
"doc.src\n"
"STR_SAVEACOPY\n"
"string.text"
msgid "Save a Copy..."
msgstr "Cadw Copi..."

#: doc.src
msgctxt ""
"doc.src\n"
"STR_CLOSEDOC\n"
"string.text"
msgid "~Close"
msgstr "~Cau"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_OPEN\n"
"string.text"
msgid "Open"
msgstr "Agor"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_EDIT_TEMPLATE\n"
"string.text"
msgid "Edit"
msgstr "Golygu"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_DEFAULT_TEMPLATE\n"
"string.text"
msgid "Set As Default"
msgstr "Gosod fel y Rhagosodedig"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_RESET_DEFAULT\n"
"string.text"
msgid "Reset Default"
msgstr "Ailosod y Rhagosodiad"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_DELETE\n"
"string.text"
msgid "Delete"
msgstr "Dileu"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_RENAME\n"
"string.text"
msgid "Rename"
msgstr "Ailenwi"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_CATEGORY_RENAME\n"
"string.text"
msgid "Rename Category"
msgstr "Ailenwi Categori"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_PROPERTIES\n"
"string.text"
msgid "Properties"
msgstr "Priodweddau"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_RENAME_TEMPLATE\n"
"string.text"
msgid "Enter New Name: "
msgstr "Rhowch Enw Newydd: "

#: doc.src
msgctxt ""
"doc.src\n"
"STR_TEMPLATE_TOOLTIP\n"
"string.text"
msgid ""
"Title: $1\n"
"Category: $2"
msgstr ""
"Teitl: $1\n"
"Categori: $2"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_TEMPLATE_SELECTION\n"
"string.text"
msgid "Select a Template"
msgstr "Dewis Templed"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_AUTOMATICVERSION\n"
"string.text"
msgid "Automatically saved version"
msgstr "Fersiwn wedi ei gadw'n awtomatig"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_DOCTYPENAME_SW\n"
"string.text"
msgid "Text Document"
msgstr "Dogfen Testun"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_DOCTYPENAME_SWWEB\n"
"string.text"
msgid "HTML Document"
msgstr "Dogfen HTML"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_DOCTYPENAME_SWGLOB\n"
"string.text"
msgid "Master Document"
msgstr "Prif Ddogfen"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_DOCTYPENAME_SC\n"
"string.text"
msgid "Spreadsheet"
msgstr "Taenlen"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_DOCTYPENAME_SI\n"
"string.text"
msgid "Presentation"
msgstr "Cyflwyniad"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_DOCTYPENAME_SD\n"
"string.text"
msgid "Drawing"
msgstr "Lluniadu"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_DOCTYPENAME_MESSAGE\n"
"string.text"
msgid "Message"
msgstr "Neges"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_EXPORTBUTTON\n"
"string.text"
msgid "Export"
msgstr "Allforio"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_LABEL_FILEFORMAT\n"
"string.text"
msgid "File format:"
msgstr "Fformat ffeil:"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_HIDDENINFO_CONTAINS\n"
"string.text"
msgid ""
"This document contains:\n"
"\n"
msgstr ""
"Mae'r ddogfen yn cynnwys:\n"
"\n"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_HIDDENINFO_RECORDCHANGES\n"
"string.text"
msgid "Recorded changes"
msgstr "Newidiadau recordiwyd"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_HIDDENINFO_NOTES\n"
"string.text"
msgid "Notes"
msgstr "Nodiadau"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_HIDDENINFO_DOCVERSIONS\n"
"string.text"
msgid "Document versions"
msgstr "Fersiwn dogfen"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_HIDDENINFO_FIELDS\n"
"string.text"
msgid "Fields"
msgstr "Meysydd"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_HIDDENINFO_LINKDATA\n"
"string.text"
msgid "Linked data..."
msgstr "Data cysylltiedig..."

#: doc.src
msgctxt ""
"doc.src\n"
"STR_HIDDENINFO_CONTINUE_SAVING\n"
"string.text"
msgid "Do you want to continue saving the document?"
msgstr "Hoffech chi barhau i gadw'r ddogfen?"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_HIDDENINFO_CONTINUE_PRINTING\n"
"string.text"
msgid "Do you want to continue printing the document?"
msgstr "Hoffech chi barhau i argraffu'r ddogfen?"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_HIDDENINFO_CONTINUE_SIGNING\n"
"string.text"
msgid "Do you want to continue signing the document?"
msgstr "Hoffech chi barhau i lofnodi'r ddogfen?"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_HIDDENINFO_CONTINUE_CREATEPDF\n"
"string.text"
msgid "Do you want to continue creating a PDF file?"
msgstr "Hoffech chi barhau i greu ffeil PDF?"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_NEW_FILENAME_SAVE\n"
"string.text"
msgid "If you do not want to overwrite the original document, you should save your work under a new filename."
msgstr "Os nad ydych am drosysgrifo'r ddogfen wreiddiol, dylech gadw eich gwaith o dan enw ffeil newydd."

#: doc.src
msgctxt ""
"doc.src\n"
"STR_ERROR_DELETE_TEMPLATE_DIR\n"
"string.text"
msgid "Some template files are protected and can not be deleted."
msgstr "Mae rhai ffeiliau templed wedi eu diogelu ac nid oes modd eu dileu."

#. pb: %1 == a number [1-4]
#: doc.src
msgctxt ""
"doc.src\n"
"STR_DOCINFO_INFOFIELD\n"
"string.text"
msgid "Info %1"
msgstr "Gwybodaeth %1"

#. Used in the title of a shared document.
#: doc.src
msgctxt ""
"doc.src\n"
"STR_SHARED\n"
"string.text"
msgid " (shared)"
msgstr " (rhanedig)"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_XMLSEC_ODF12_EXPECTED\n"
"string.text"
msgid "The document format version is set to ODF 1.1 (OpenOffice.org 2.x) in Tools-Options-Load/Save-General. Signing documents requires ODF 1.2 (OpenOffice.org 3.x)."
msgstr "Mae fersiwn fformat y ddogfen wedi ei gosod i ODF 1.1 (OpenOffice.org 2.x) yn Offer-Dewisiada-Llwytho/Cadw-Cyffredinol. Mae llofnodi dogfennau'n gofyn am ODF 1.2 (OpenOffice.org 3.x)."

#: doc.src
msgctxt ""
"doc.src\n"
"STR_XMLSEC_QUERY_SAVESIGNEDBEFORESIGN\n"
"string.text"
msgid ""
"The document has to be saved before it can be signed. Saving the document removes all present signatures.\n"
"Do you want to save the document?"
msgstr ""
"Mae'n rhaid cadw'r ddogfen hon cyn ei llofnodi. Bydd cadw'r ddogfen yn tynnu'r holl lofnodion presennol.\n"
"Hoffech chi gadw'r ddogfen hon?"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_QRYTEMPL_MESSAGE\n"
"string.text"
msgid "The template '$(ARG1)' on which this document is based, has been modified. Do you want to update style based formatting according to the modified template?"
msgstr "Mae templed '$(ARG1)' y mae'r ddogfen hon wedi ei seilio arni, wedi ei newid. Hoffech chi ddiweddaru'r fformat sy'n seiliedig ar fformatio yn ôl y templed diwygiedig?"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_QRYTEMPL_UPDATE_BTN\n"
"string.text"
msgid "~Update Styles"
msgstr "~Diweddaru Arddull"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_QRYTEMPL_KEEP_BTN\n"
"string.text"
msgid "~Keep Old Styles"
msgstr "~Cadw'r Hen Arddull"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_ACTION_SORT_NAME\n"
"string.text"
msgid "Sort by name"
msgstr "Trefnu yn ôl enw"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_ACTION_REFRESH\n"
"string.text"
msgid "Refresh"
msgstr "Adnewyddu"

#. leave ending space
#: doc.src
msgctxt ""
"doc.src\n"
"STR_ACTION_DEFAULT\n"
"string.text"
msgid "Reset Default Template "
msgstr "Ailosod y Templed Rhagosodedig "

#: doc.src
msgctxt ""
"doc.src\n"
"STR_MOVE_NEW\n"
"string.text"
msgid "New folder"
msgstr "Ffolder newydd"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_CATEGORY_NEW\n"
"string.text"
msgid "New Category"
msgstr "Categori Newydd"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_CATEGORY_DELETE\n"
"string.text"
msgid "Delete Category"
msgstr "Dileu Categori"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_CATEGORY_SELECT\n"
"string.text"
msgid "Select Category"
msgstr "Dewis Categori"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_MSG_EXPORT_SUCCESS\n"
"string.text"
msgid "$1 templates successfully exported."
msgstr "$1 templed wedi eu hallforio'n llwyddiannus."

#: doc.src
msgctxt ""
"doc.src\n"
"STR_MSG_QUERY_COPY\n"
"string.text"
msgid "$1 could not be moved to the category \"$2\". Do you want to copy the template instead?"
msgstr "Nid oedd modd symud $1 i gategori \"$2\". Hoffech chi gopïo'r templed yn lle hynny?"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_CREATE_ERROR\n"
"string.text"
msgid "Cannot create category: $1"
msgstr "Methu creu categori: $1"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_ERROR_SAVEAS\n"
"string.text"
msgid "Cannot save template: $1"
msgstr "Methu cadw templed: $1"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_INPUT_NEW\n"
"string.text"
msgid "Enter category name:"
msgstr "Rhowch enw categori:"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_REPOSITORY_LOCAL\n"
"string.text"
msgid "Local"
msgstr "Lleol"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_REPOSITORY_NEW\n"
"string.text"
msgid "New Repository"
msgstr "Storfa Newydd"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_MSG_ERROR_LOCAL_MOVE\n"
"string.text"
msgid ""
"Error moving the following templates to $1.\n"
"$2"
msgstr ""
"Gwall wrth symud y templedi canlynol i $1.\n"
"$2"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_MSG_ERROR_REMOTE_MOVE\n"
"string.text"
msgid ""
"Error moving the following templates from repository $1 to folder $2.\n"
"$3"
msgstr ""
"Gwall wrth symud y templedi canlynol o storfa $1 i ffolder $2.\n"
"$3"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_MSG_ERROR_EXPORT\n"
"string.text"
msgid ""
"Error exporting the following templates:\n"
"$1"
msgstr ""
"Gwall wrth allforio'r templedi canlynol:\n"
"$1"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_MSG_ERROR_IMPORT\n"
"string.text"
msgid ""
"Error importing the following templates to $1:\n"
"$2"
msgstr ""
"Gwall wrth fewnforio'r templedi canlynol i $1:\n"
"$2"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_MSG_ERROR_DELETE_TEMPLATE\n"
"string.text"
msgid ""
"The following templates cannot be deleted:\n"
"$1"
msgstr ""
"Nid oes modd dileu'r templedi canlynol:\n"
"$1"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_MSG_ERROR_DELETE_FOLDER\n"
"string.text"
msgid ""
"The following folders cannot be deleted:\n"
"$1"
msgstr ""
"Nid oes modd dileu'r ffolderi canlynol:\n"
"$1"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_MSG_ERROR_REPOSITORY_NAME\n"
"string.text"
msgid ""
"Failed to create repository \"$1\".\n"
"A repository with this name may already exist."
msgstr ""
"Wedi methu creu storfa \"$1\".\n"
"Efallai bod storfa gyda'r enw hwnnw'n bodoli eisoes."

#: doc.src
msgctxt ""
"doc.src\n"
"STR_MSG_ERROR_SELECT_FOLDER\n"
"string.text"
msgid "Select the destination folder(s) to save the template."
msgstr "Dewiswch y ffolder(i) i gadw'r templed."

#: doc.src
msgctxt ""
"doc.src\n"
"STR_INPUT_TEMPLATE_NEW\n"
"string.text"
msgid "Enter template name:"
msgstr "Rhowch enw'r templed:"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_QMSG_SEL_FOLDER_DELETE\n"
"string.text"
msgid "Do you want to delete the selected folders?"
msgstr "Hoffech chi ddileu'r ffolderi hyn?"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_QMSG_TEMPLATE_OVERWRITE\n"
"string.text"
msgid "A template named $1 already exist in $2. Do you want to overwrite it?"
msgstr "Mae templed o'r enw $1 yn bodoli eisoes $2. Ydych chi am ei throsysgrifo?"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_QMSG_SEL_TEMPLATE_DELETE\n"
"string.text"
msgid "Do you want to delete the selected templates?"
msgstr "Hoffech chi ddileu'r templedi hyn?"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"My Templates\n"
"itemlist.text"
msgid "My Templates"
msgstr "Templedi"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"Business Correspondence\n"
"itemlist.text"
msgid "Business Correspondence"
msgstr "Gohebiaeth Busnes"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"Other Business Documents\n"
"itemlist.text"
msgid "Other Business Documents"
msgstr "Dogfennau Busnes Eraill"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"Personal Correspondence and Documents\n"
"itemlist.text"
msgid "Personal Correspondence and Documents"
msgstr "Gohebiaeth a Dogfennau Personol"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"Forms and Contracts\n"
"itemlist.text"
msgid "Forms and Contracts"
msgstr "Ffurflenni a Chontractau"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"Finances\n"
"itemlist.text"
msgid "Finances"
msgstr "Cyllid"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"Education\n"
"itemlist.text"
msgid "Education"
msgstr "Addysg"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"Presentation Backgrounds\n"
"itemlist.text"
msgid "Presentation Backgrounds"
msgstr "Cefndir Cyflwyniadau"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"Presentations\n"
"itemlist.text"
msgid "Presentations"
msgstr "Cyflwyniadau"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"Miscellaneous\n"
"itemlist.text"
msgid "Miscellaneous"
msgstr "Amrywiol"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"Labels\n"
"itemlist.text"
msgid "Labels"
msgstr "Labeli"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"Styles\n"
"itemlist.text"
msgid "Styles"
msgstr "Arddulliau"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"RID_CNT_STR_WAITING\n"
"string.text"
msgid "The templates are being initialized for first-time usage."
msgstr "Mae'r templedi'n cael eu cychwyn ar gyfer defnydd cyntaf."

#: templatelocnames.src
msgctxt ""
"templatelocnames.src\n"
"STR_TEMPLATE_NAME1\n"
"string.text"
msgid "Abstract Green"
msgstr "Gwyrdd Haniaethol"

#: templatelocnames.src
msgctxt ""
"templatelocnames.src\n"
"STR_TEMPLATE_NAME2\n"
"string.text"
msgid "Abstract Red"
msgstr "Coch Haniaethol"

#: templatelocnames.src
msgctxt ""
"templatelocnames.src\n"
"STR_TEMPLATE_NAME3\n"
"string.text"
msgid "Abstract Yellow"
msgstr "Melyn Haniaethol"

#: templatelocnames.src
msgctxt ""
"templatelocnames.src\n"
"STR_TEMPLATE_NAME4\n"
"string.text"
msgid "Bright Blue"
msgstr "Glas Llachar"

#: templatelocnames.src
msgctxt ""
"templatelocnames.src\n"
"STR_TEMPLATE_NAME5\n"
"string.text"
msgid "DNA"
msgstr "DNA"

#: templatelocnames.src
msgctxt ""
"templatelocnames.src\n"
"STR_TEMPLATE_NAME6\n"
"string.text"
msgid "Inspiration"
msgstr "Ysbrydoliaeth"

#: templatelocnames.src
msgctxt ""
"templatelocnames.src\n"
"STR_TEMPLATE_NAME7\n"
"string.text"
msgid "Lush Green"
msgstr "Gwyrdd Dwfn"

#: templatelocnames.src
msgctxt ""
"templatelocnames.src\n"
"STR_TEMPLATE_NAME8\n"
"string.text"
msgid "Metropolis"
msgstr "Metropolis"

#: templatelocnames.src
msgctxt ""
"templatelocnames.src\n"
"STR_TEMPLATE_NAME9\n"
"string.text"
msgid "Sunset"
msgstr "Machlud Haul"

#: templatelocnames.src
msgctxt ""
"templatelocnames.src\n"
"STR_TEMPLATE_NAME10\n"
"string.text"
msgid "Vintage"
msgstr "Hynafol"

#. extracted from formula/uiconfig/ui
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2017-02-23 07:00+0000\n"
"Last-Translator: VenetoABC <veneto.abc@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: vec\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1487833211.000000\n"

#: formuladialog.ui
msgctxt ""
"formuladialog.ui\n"
"array\n"
"label\n"
"string.text"
msgid "Array"
msgstr "Matrise"

#: formuladialog.ui
msgctxt ""
"formuladialog.ui\n"
"back\n"
"label\n"
"string.text"
msgid "<< _Back"
msgstr "<< _Indrio"

#: formuladialog.ui
msgctxt ""
"formuladialog.ui\n"
"next\n"
"label\n"
"string.text"
msgid "_Next >>"
msgstr "_Seguente >>"

#: formuladialog.ui
msgctxt ""
"formuladialog.ui\n"
"function\n"
"label\n"
"string.text"
msgid "Functions"
msgstr "Funsion"

#: formuladialog.ui
msgctxt ""
"formuladialog.ui\n"
"struct\n"
"label\n"
"string.text"
msgid "Structure"
msgstr "Strutura"

#: formuladialog.ui
msgctxt ""
"formuladialog.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Function result"
msgstr "Rezultà funsion"

#: formuladialog.ui
msgctxt ""
"formuladialog.ui\n"
"formula\n"
"label\n"
"string.text"
msgid "For_mula"
msgstr "Fòr_muła"

#: formuladialog.ui
msgctxt ""
"formuladialog.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Result"
msgstr "Rezultato"

#: formuladialog.ui
msgctxt ""
"formuladialog.ui\n"
"ed_formula-atkobject\n"
"AtkObject::accessible-name\n"
"string.text"
msgid "Formula"
msgstr "Formula"

#: formuladialog.ui
msgctxt ""
"formuladialog.ui\n"
"RB_REF\n"
"tooltip_text\n"
"string.text"
msgid "Maximize"
msgstr "Ingrandisi"

#: functionpage.ui
msgctxt ""
"functionpage.ui\n"
"label_search\n"
"label\n"
"string.text"
msgid "_Search"
msgstr "S_erca"

#: functionpage.ui
msgctxt ""
"functionpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "_Category"
msgstr "_Categorìa"

#: functionpage.ui
msgctxt ""
"functionpage.ui\n"
"category\n"
"0\n"
"stringlist.text"
msgid "Last Used"
msgstr "Doparà da poco"

#: functionpage.ui
msgctxt ""
"functionpage.ui\n"
"category\n"
"1\n"
"stringlist.text"
msgid "All"
msgstr "Tuto"

#: functionpage.ui
msgctxt ""
"functionpage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "_Function"
msgstr "_Funsion"

#: parameter.ui
msgctxt ""
"parameter.ui\n"
"editdesc\n"
"label\n"
"string.text"
msgid "Function not known"
msgstr "Funsion mìa conosuda"

#: parameter.ui
msgctxt ""
"parameter.ui\n"
"RB_ARG1\n"
"tooltip_text\n"
"string.text"
msgid "Select"
msgstr "Sełesiona"

#: parameter.ui
msgctxt ""
"parameter.ui\n"
"RB_ARG2\n"
"tooltip_text\n"
"string.text"
msgid "Select"
msgstr "Sełesiona"

#: parameter.ui
msgctxt ""
"parameter.ui\n"
"RB_ARG3\n"
"tooltip_text\n"
"string.text"
msgid "Select"
msgstr "Sełesiona"

#: parameter.ui
msgctxt ""
"parameter.ui\n"
"RB_ARG4\n"
"tooltip_text\n"
"string.text"
msgid "Select"
msgstr "Sełesiona"

#: structpage.ui
msgctxt ""
"structpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "_Structure"
msgstr "_Strutura"

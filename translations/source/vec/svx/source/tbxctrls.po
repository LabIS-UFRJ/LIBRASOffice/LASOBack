#. extracted from svx/source/tbxctrls
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2017-03-23 05:58+0000\n"
"Last-Translator: VenetoABC <veneto.abc@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: vec\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1490248682.000000\n"

#: colrctrl.src
msgctxt ""
"colrctrl.src\n"
"STR_COLORTABLE\n"
"string.text"
msgid "Color Palette"
msgstr "Tavołosa de i cołori"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_PERSPECTIVE\n"
"string.text"
msgid "~Perspective"
msgstr "~Prospetiva"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_PARALLEL\n"
"string.text"
msgid "P~arallel"
msgstr "P~arałeło"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_DIRECTION + DIRECTION_NW\n"
"string.text"
msgid "Extrusion North-West"
msgstr "Estruzion nord-ovest"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_DIRECTION + DIRECTION_N\n"
"string.text"
msgid "Extrusion North"
msgstr "Estruzion nord"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_DIRECTION + DIRECTION_NE\n"
"string.text"
msgid "Extrusion North-East"
msgstr "Estruzion nord-est"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_DIRECTION + DIRECTION_W\n"
"string.text"
msgid "Extrusion West"
msgstr "Estruzion ovest"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_DIRECTION + DIRECTION_NONE\n"
"string.text"
msgid "Extrusion Backwards"
msgstr "Estruzion indrìo"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_DIRECTION + DIRECTION_E\n"
"string.text"
msgid "Extrusion East"
msgstr "Estruzion est"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_DIRECTION + DIRECTION_SW\n"
"string.text"
msgid "Extrusion South-West"
msgstr "Estruzion sud-ovest"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_DIRECTION + DIRECTION_S\n"
"string.text"
msgid "Extrusion South"
msgstr "Estruzion sud"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_DIRECTION + DIRECTION_SE\n"
"string.text"
msgid "Extrusion South-East"
msgstr "Estruzion sud-est"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_BRIGHT\n"
"string.text"
msgid "~Bright"
msgstr "~Łuzente"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_NORMAL\n"
"string.text"
msgid "~Normal"
msgstr "~Normałe"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_DIM\n"
"string.text"
msgid "~Dim"
msgstr "~Ofuscar"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_WIREFRAME\n"
"string.text"
msgid "~Wire Frame"
msgstr "~Fil de fero"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_MATTE\n"
"string.text"
msgid "~Matt"
msgstr "~Opaco"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_PLASTIC\n"
"string.text"
msgid "~Plastic"
msgstr "~Plastega"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_METAL\n"
"string.text"
msgid "Me~tal"
msgstr "Me~tàłico"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_EXTRUSION_COLOR\n"
"string.text"
msgid "Extrusion Color"
msgstr "Cołor estruzion"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_DEPTH_0\n"
"string.text"
msgid "~0 cm"
msgstr "~0 cm"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_DEPTH_1\n"
"string.text"
msgid "~1 cm"
msgstr "~1 cm"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_DEPTH_2\n"
"string.text"
msgid "~2.5 cm"
msgstr "~2,5 cm"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_DEPTH_3\n"
"string.text"
msgid "~5 cm"
msgstr "~5 cm"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_DEPTH_4\n"
"string.text"
msgid "10 ~cm"
msgstr "10 ~cm"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_DEPTH_0_INCH\n"
"string.text"
msgid "0 inch"
msgstr "0 pòłisi"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_DEPTH_1_INCH\n"
"string.text"
msgid "0.~5 inch"
msgstr "0,~5 pòłisi"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_DEPTH_2_INCH\n"
"string.text"
msgid "~1 inch"
msgstr "~1 pòłise"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_DEPTH_3_INCH\n"
"string.text"
msgid "~2 inch"
msgstr "~2 pòłisi"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_DEPTH_4_INCH\n"
"string.text"
msgid "~4 inch"
msgstr "~4 pòłisi"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_CUSTOM\n"
"string.text"
msgid "~Custom..."
msgstr "~Parsonałiza"

#: extrusioncontrols.src
msgctxt ""
"extrusioncontrols.src\n"
"RID_SVXSTR_INFINITY\n"
"string.text"
msgid "~Infinity"
msgstr "~Infinìo"

#: fontworkgallery.src
msgctxt ""
"fontworkgallery.src\n"
"RID_SVXSTR_ALIGN_LEFT\n"
"string.text"
msgid "~Left Align"
msgstr "~Łineasion a sanca"

#: fontworkgallery.src
msgctxt ""
"fontworkgallery.src\n"
"RID_SVXSTR_ALIGN_CENTER\n"
"string.text"
msgid "~Center"
msgstr "~Sentrałe"

#: fontworkgallery.src
msgctxt ""
"fontworkgallery.src\n"
"RID_SVXSTR_ALIGN_RIGHT\n"
"string.text"
msgid "~Right Align"
msgstr "Łineasion a ~drita"

#: fontworkgallery.src
msgctxt ""
"fontworkgallery.src\n"
"RID_SVXSTR_ALIGN_WORD\n"
"string.text"
msgid "~Word Justify"
msgstr "Justìfega ~parołe"

#: fontworkgallery.src
msgctxt ""
"fontworkgallery.src\n"
"RID_SVXSTR_ALIGN_STRETCH\n"
"string.text"
msgid "S~tretch Justify"
msgstr "Justìfega ~caràtari"

#: fontworkgallery.src
msgctxt ""
"fontworkgallery.src\n"
"RID_SVXSTR_CHARS_SPACING_VERY_TIGHT\n"
"string.text"
msgid "~Very Tight"
msgstr "Stre~to asè"

#: fontworkgallery.src
msgctxt ""
"fontworkgallery.src\n"
"RID_SVXSTR_CHARS_SPACING_TIGHT\n"
"string.text"
msgid "~Tight"
msgstr "~Streto"

#: fontworkgallery.src
msgctxt ""
"fontworkgallery.src\n"
"RID_SVXSTR_CHARS_SPACING_NORMAL\n"
"string.text"
msgid "~Normal"
msgstr "~Normałe"

#: fontworkgallery.src
msgctxt ""
"fontworkgallery.src\n"
"RID_SVXSTR_CHARS_SPACING_LOOSE\n"
"string.text"
msgid "~Loose"
msgstr "Ł~argo"

#: fontworkgallery.src
msgctxt ""
"fontworkgallery.src\n"
"RID_SVXSTR_CHARS_SPACING_VERY_LOOSE\n"
"string.text"
msgid "Very ~Loose"
msgstr "Łar~go asè"

#: fontworkgallery.src
msgctxt ""
"fontworkgallery.src\n"
"RID_SVXSTR_CHARS_SPACING_CUSTOM\n"
"string.text"
msgid "~Custom..."
msgstr "~Parsonałiza..."

#: fontworkgallery.src
msgctxt ""
"fontworkgallery.src\n"
"RID_SVXSTR_CHARS_SPACING_KERN_PAIRS\n"
"string.text"
msgid "~Kern Character Pairs"
msgstr "~Crenadura copia de caràtari"

#: grafctrl.src
msgctxt ""
"grafctrl.src\n"
"RID_SVXSTR_UNDO_GRAFMODE\n"
"string.text"
msgid "Image Mode"
msgstr "Modo imàjine"

#: grafctrl.src
msgctxt ""
"grafctrl.src\n"
"RID_SVXSTR_UNDO_GRAFRED\n"
"string.text"
msgid "Red"
msgstr "Roso"

#: grafctrl.src
msgctxt ""
"grafctrl.src\n"
"RID_SVXSTR_UNDO_GRAFGREEN\n"
"string.text"
msgid "Green"
msgstr "Verde"

#: grafctrl.src
msgctxt ""
"grafctrl.src\n"
"RID_SVXSTR_UNDO_GRAFBLUE\n"
"string.text"
msgid "Blue"
msgstr "Blè"

#: grafctrl.src
msgctxt ""
"grafctrl.src\n"
"RID_SVXSTR_UNDO_GRAFLUMINANCE\n"
"string.text"
msgid "Brightness"
msgstr "Łuzentesa"

#: grafctrl.src
msgctxt ""
"grafctrl.src\n"
"RID_SVXSTR_UNDO_GRAFCONTRAST\n"
"string.text"
msgid "Contrast"
msgstr "Contrasto"

#: grafctrl.src
msgctxt ""
"grafctrl.src\n"
"RID_SVXSTR_UNDO_GRAFGAMMA\n"
"string.text"
msgid "Gamma"
msgstr "Gama"

#: grafctrl.src
msgctxt ""
"grafctrl.src\n"
"RID_SVXSTR_UNDO_GRAFTRANSPARENCY\n"
"string.text"
msgid "Transparency"
msgstr "Trasparensa"

#: grafctrl.src
msgctxt ""
"grafctrl.src\n"
"RID_SVXSTR_GRAFCROP\n"
"string.text"
msgid "Crop"
msgstr "Retaja"

#: lboxctrl.src
msgctxt ""
"lboxctrl.src\n"
"RID_SVXSTR_NUM_UNDO_ACTIONS\n"
"string.text"
msgid "Actions to undo: $(ARG1)"
msgstr "Asion da desfar: $(ARG1)"

#: lboxctrl.src
msgctxt ""
"lboxctrl.src\n"
"RID_SVXSTR_NUM_UNDO_ACTION\n"
"string.text"
msgid "Actions to undo: $(ARG1)"
msgstr "Asion da desfar: $(ARG1)"

#: lboxctrl.src
msgctxt ""
"lboxctrl.src\n"
"RID_SVXSTR_NUM_REDO_ACTIONS\n"
"string.text"
msgid "Actions to redo: $(ARG1)"
msgstr "Asion da refar: $(ARG1)"

#: lboxctrl.src
msgctxt ""
"lboxctrl.src\n"
"RID_SVXSTR_NUM_REDO_ACTION\n"
"string.text"
msgid "Actions to redo: $(ARG1)"
msgstr "Asion da refar: $(ARG1)"

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_NOFILL\n"
"string.text"
msgid "No Fill"
msgstr "Nesuna inpienasion"

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_TRANSPARENT\n"
"string.text"
msgid "Transparent"
msgstr "Trasparente"

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_FILLPATTERN\n"
"string.text"
msgid "Pattern"
msgstr "Modeło"

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_FRAME\n"
"string.text"
msgid "Borders"
msgstr "Bordi"

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_FRAME_STYLE\n"
"string.text"
msgid "Border Style"
msgstr "Stiłe bordo"

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_FRAME_COLOR\n"
"string.text"
msgid "Border Color"
msgstr "Cołor bordo"

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_LINECOLOR\n"
"string.text"
msgid "Line Color"
msgstr "Cołor łinea"

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_FILLCOLOR\n"
"string.text"
msgid "Fill Color"
msgstr "Cołor de inpienasion"

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_MORENUMBERING\n"
"string.text"
msgid "More Numbering..."
msgstr "Altri ełenchi numerài..."

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_MOREBULLETS\n"
"string.text"
msgid "More Bullets..."
msgstr "Altri ełenchi puntài..."

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVX_STYLE_MENU\n"
"RID_SVX_UPDATE_STYLE\n"
"menuitem.text"
msgid "Update to Match Selection"
msgstr "Ajorna par abinar ła sełesion"

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVX_STYLE_MENU\n"
"RID_SVX_MODIFY_STYLE\n"
"menuitem.text"
msgid "Edit Style..."
msgstr "Modìfega stiłe..."

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVX_PRESET_MENU\n"
"RID_SVX_PRESET_RENAME\n"
"menuitem.text"
msgid "Rename"
msgstr "Renòmena"

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVX_PRESET_MENU\n"
"RID_SVX_PRESET_DELETE\n"
"menuitem.text"
msgid "Delete"
msgstr "Ełìmina"

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_EXTRAS_CHARBACKGROUND\n"
"string.text"
msgid "Highlight Color"
msgstr "Cołor evidensiasion"

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_BACKGROUND\n"
"string.text"
msgid "Background"
msgstr "Sfondo"

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_AUTOMATIC\n"
"string.text"
msgid "Automatic"
msgstr "Automàtego"

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_BY_AUTHOR\n"
"string.text"
msgid "By author"
msgstr "Par autor"

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_PAGES\n"
"string.text"
msgid "Pages"
msgstr "Pàjine"

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_CLEARFORM\n"
"string.text"
msgid "Clear formatting"
msgstr "Scanseła formatasion"

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_MORE_STYLES\n"
"string.text"
msgid "More Styles..."
msgstr "Altri stiłi..."

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_MORE\n"
"string.text"
msgid "More Options..."
msgstr "Altre opsion..."

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_TEXTCOLOR\n"
"string.text"
msgid "Font color"
msgstr "Cołor de'l caràtare"

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_CHARFONTNAME\n"
"string.text"
msgid "Font Name"
msgstr "Nome de'l caràtare"

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_CHARFONTNAME_NOTAVAILABLE\n"
"string.text"
msgid "Font Name. The current font is not available and will be substituted."
msgstr "Nome de'l caràtare. El caràtare corente no'l ze desponìbiłe e el venjarà sostituìo."

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_CUSTOM_PAL\n"
"string.text"
msgid "custom"
msgstr "parsonałizà"

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_DOC_COLORS\n"
"string.text"
msgid "Document colors"
msgstr "Cołori predefinìi"

#: tbcontrl.src
msgctxt ""
"tbcontrl.src\n"
"RID_SVXSTR_DOC_COLOR_PREFIX\n"
"string.text"
msgid "Document Color"
msgstr "Cołor de'l documento"

#: tbunosearchcontrollers.src
msgctxt ""
"tbunosearchcontrollers.src\n"
"RID_SVXSTR_FINDBAR_FIND\n"
"string.text"
msgid "Find"
msgstr "Trova"

#: tbunosearchcontrollers.src
msgctxt ""
"tbunosearchcontrollers.src\n"
"RID_SVXSTR_FINDBAR_MATCHCASE\n"
"string.text"
msgid "Match Case"
msgstr "Maiùscołe/minùscołe"

#: tbunosearchcontrollers.src
msgctxt ""
"tbunosearchcontrollers.src\n"
"RID_SVXSTR_FINDBAR_SEARCHFORMATTED\n"
"string.text"
msgid "Formatted Display"
msgstr "Vizuałizasion formatà"

#. extracted from basctl/source/dlged
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-04-22 23:40+0200\n"
"PO-Revision-Date: 2017-01-12 08:52+0000\n"
"Last-Translator: VenetoABC <veneto.abc@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: vec\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1484211137.000000\n"

#: dlgresid.src
msgctxt ""
"dlgresid.src\n"
"RID_STR_BRWTITLE_PROPERTIES\n"
"string.text"
msgid "Properties: "
msgstr "Propietà: "

#: dlgresid.src
msgctxt ""
"dlgresid.src\n"
"RID_STR_BRWTITLE_NO_PROPERTIES\n"
"string.text"
msgid "No Control marked"
msgstr "Nesun controło sełesionà"

#: dlgresid.src
msgctxt ""
"dlgresid.src\n"
"RID_STR_BRWTITLE_MULTISELECT\n"
"string.text"
msgid "Multiselection"
msgstr "Sełesion mùltipla"

#: dlgresid.src
msgctxt ""
"dlgresid.src\n"
"RID_STR_DEF_LANG\n"
"string.text"
msgid "[Default Language]"
msgstr "[Łengua predefinìa]"

#: dlgresid.src
msgctxt ""
"dlgresid.src\n"
"RID_STR_CREATE_LANG\n"
"string.text"
msgid "<Press 'Add' to create language resources>"
msgstr "<Struca 'Zonta' par crear resorse de łengua>"

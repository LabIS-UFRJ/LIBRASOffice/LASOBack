#. extracted from vcl/source/src
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2017-03-21 05:55+0000\n"
"Last-Translator: VenetoABC <veneto.abc@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: vec\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1490075756.000000\n"

#: app.src
msgctxt ""
"app.src\n"
"SV_APP_CPUTHREADS\n"
"string.text"
msgid "CPU Threads: "
msgstr "Thread CPU: "

#: app.src
msgctxt ""
"app.src\n"
"SV_APP_OSVERSION\n"
"string.text"
msgid "OS Version: "
msgstr "Varsion SO: "

#: app.src
msgctxt ""
"app.src\n"
"SV_APP_UIRENDER\n"
"string.text"
msgid "UI Render: "
msgstr "Reza intarfasa: "

#: app.src
msgctxt ""
"app.src\n"
"SV_APP_GL\n"
"string.text"
msgid "GL"
msgstr "GL"

#: app.src
msgctxt ""
"app.src\n"
"SV_APP_DEFAULT\n"
"string.text"
msgid "default"
msgstr "predefinìo"

#: app.src
msgctxt ""
"app.src\n"
"SV_APP_VCLBACKEND\n"
"string.text"
msgid "VCL: "
msgstr "VCL: "

#: app.src
msgctxt ""
"app.src\n"
"SV_APP_LAYOUT_ENGINE\n"
"string.text"
msgid "Layout Engine: "
msgstr "Motor de layout:"

#: app.src
msgctxt ""
"app.src\n"
"SV_APP_LAYOUT_NEW\n"
"string.text"
msgid "new"
msgstr "novo"

#: app.src
msgctxt ""
"app.src\n"
"SV_APP_LAYOUT_OLD\n"
"string.text"
msgid "old"
msgstr "vecio"

#. This is used on buttons for platforms other than windows, there should be a ~ mnemonic in this string
#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_OK\n"
"string.text"
msgid "~OK"
msgstr "O~K"

#. This is used on buttons for platforms other than windows, there should be a ~ mnemonic in this string
#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_CANCEL\n"
"string.text"
msgid "~Cancel"
msgstr "~Anuła"

#. This is used on buttons for Windows, there should be no ~ mnemonic in this string
#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_OK_NOMNEMONIC\n"
"string.text"
msgid "OK"
msgstr "OK"

#. This is used on buttons for Windows, there should be no ~ mnemonic in this string
#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_CANCEL_NOMNEMONIC\n"
"string.text"
msgid "Cancel"
msgstr "Anuła"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_YES\n"
"string.text"
msgid "~Yes"
msgstr "~Sì"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_NO\n"
"string.text"
msgid "~No"
msgstr "~No"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_RETRY\n"
"string.text"
msgid "~Retry"
msgstr "~Reprova"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_HELP\n"
"string.text"
msgid "~Help"
msgstr "Guid~a"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_CLOSE\n"
"string.text"
msgid "~Close"
msgstr "~Sara su"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_MORE\n"
"string.text"
msgid "~More"
msgstr "~Altro"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_LESS\n"
"string.text"
msgid "~Less"
msgstr "~Manco"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_IGNORE\n"
"string.text"
msgid "~Ignore"
msgstr "~Injora"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_ABORT\n"
"string.text"
msgid "~Abort"
msgstr "~Intaronpi"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_RESET\n"
"string.text"
msgid "R~eset"
msgstr "Reprìs~tena"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_ADD\n"
"string.text"
msgid "~Add"
msgstr "~Zonta"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_DELETE\n"
"string.text"
msgid "~Delete"
msgstr "~Ełìmina"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_REMOVE\n"
"string.text"
msgid "~Remove"
msgstr "~Cava via"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_NEW\n"
"string.text"
msgid "~New"
msgstr "~Novo"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_EDIT\n"
"string.text"
msgid "~Edit"
msgstr "~Modìfega"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_APPLY\n"
"string.text"
msgid "~Apply"
msgstr "~Àplega"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_SAVE\n"
"string.text"
msgid "~Save"
msgstr "~Salva"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_OPEN\n"
"string.text"
msgid "~Open"
msgstr "~Verzi"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_UNDO\n"
"string.text"
msgid "~Undo"
msgstr "Desf~a"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_PASTE\n"
"string.text"
msgid "~Paste"
msgstr "~Incoła"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_NEXT\n"
"string.text"
msgid "~Next"
msgstr "~Seguente"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_GO_UP\n"
"string.text"
msgid "~Up"
msgstr "~Su"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_GO_DOWN\n"
"string.text"
msgid "Do~wn"
msgstr "~Zo"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_CLEAR\n"
"string.text"
msgid "~Clear"
msgstr "~Scanseła"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_PLAY\n"
"string.text"
msgid "~Play"
msgstr "Re~produzi"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_FIND\n"
"string.text"
msgid "~Find"
msgstr "Ca~ta"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_STOP\n"
"string.text"
msgid "~Stop"
msgstr "~Ferma"

#: btntext.src
msgctxt ""
"btntext.src\n"
"SV_BUTTONTEXT_CONNECT\n"
"string.text"
msgid "C~onnect"
msgstr "~Cołega"

#: fpicker.src
msgctxt ""
"fpicker.src\n"
"STR_FPICKER_AUTO_EXTENSION\n"
"string.text"
msgid "~Automatic file name extension"
msgstr "~Estension automàtega de'l nome de'l file"

#: fpicker.src
msgctxt ""
"fpicker.src\n"
"STR_FPICKER_PASSWORD\n"
"string.text"
msgid "Save with pass~word"
msgstr "Salva co pass~word"

#: fpicker.src
msgctxt ""
"fpicker.src\n"
"STR_FPICKER_FILTER_OPTIONS\n"
"string.text"
msgid "~Edit filter settings"
msgstr "~Modìfega inpostasion filtro"

#: fpicker.src
msgctxt ""
"fpicker.src\n"
"STR_FPICKER_READONLY\n"
"string.text"
msgid "~Read-only"
msgstr "So~ła łetura"

#: fpicker.src
msgctxt ""
"fpicker.src\n"
"STR_FPICKER_INSERT_AS_LINK\n"
"string.text"
msgid "Insert as ~Link"
msgstr "Insarisi cofà cołe~gamento"

#: fpicker.src
msgctxt ""
"fpicker.src\n"
"STR_FPICKER_SHOW_PREVIEW\n"
"string.text"
msgid "Pr~eview"
msgstr "Ant~eprima"

#: fpicker.src
msgctxt ""
"fpicker.src\n"
"STR_FPICKER_PLAY\n"
"string.text"
msgid "~Play"
msgstr "Re~produzi"

#: fpicker.src
msgctxt ""
"fpicker.src\n"
"STR_FPICKER_VERSION\n"
"string.text"
msgid "~Version:"
msgstr "~Varsion:"

#: fpicker.src
msgctxt ""
"fpicker.src\n"
"STR_FPICKER_TEMPLATES\n"
"string.text"
msgid "S~tyles:"
msgstr "St~iłi:"

#: fpicker.src
msgctxt ""
"fpicker.src\n"
"STR_FPICKER_IMAGE_TEMPLATE\n"
"string.text"
msgid "Frame Style: "
msgstr "Stiłe de'l recuadro: "

#: fpicker.src
msgctxt ""
"fpicker.src\n"
"STR_FPICKER_SELECTION\n"
"string.text"
msgid "~Selection"
msgstr "~Sełesion"

#: fpicker.src
msgctxt ""
"fpicker.src\n"
"STR_FPICKER_FOLDER_DEFAULT_TITLE\n"
"string.text"
msgid "Select Path"
msgstr "Sełesiona parcorso"

#: fpicker.src
msgctxt ""
"fpicker.src\n"
"STR_FPICKER_FOLDER_DEFAULT_DESCRIPTION\n"
"string.text"
msgid "Please select a folder."
msgstr "Sełesiona na carteła."

#: fpicker.src
msgctxt ""
"fpicker.src\n"
"STR_FPICKER_ALREADYEXISTOVERWRITE_PRIMARY\n"
"string.text"
msgid "A file named \"$filename$\" already exists. Do you want to replace it?"
msgstr "Un file che'l se ciama \"$filename$\" l'eziste za. Vutu renpiasarlo?"

#: fpicker.src
msgctxt ""
"fpicker.src\n"
"STR_FPICKER_ALREADYEXISTOVERWRITE_SECONDARY\n"
"string.text"
msgid "The file already exists in \"$dirname$\". Replacing it will overwrite its contents."
msgstr "Inte \"$dirname$\" A eziste za el file. Se se ło renpiasa A se ghe sorascrivarà el contenjùo."

#: fpicker.src
msgctxt ""
"fpicker.src\n"
"STR_FPICKER_ALLFORMATS\n"
"string.text"
msgid "All Formats"
msgstr "Tuti i formati"

#: fpicker.src
msgctxt ""
"fpicker.src\n"
"STR_FPICKER_OPEN\n"
"string.text"
msgid "Open"
msgstr "Verzi"

#: fpicker.src
msgctxt ""
"fpicker.src\n"
"STR_FPICKER_SAVE\n"
"string.text"
msgid "Save"
msgstr "Salva"

#: fpicker.src
msgctxt ""
"fpicker.src\n"
"STR_FPICKER_TYPE\n"
"string.text"
msgid "File ~type"
msgstr "~Tipo de file"

#: helptext.src
msgctxt ""
"helptext.src\n"
"SV_HELPTEXT_CLOSE\n"
"string.text"
msgid "Close"
msgstr "Sara su"

#: helptext.src
msgctxt ""
"helptext.src\n"
"SV_HELPTEXT_CLOSEDOCUMENT\n"
"string.text"
msgid "Close Document"
msgstr "Sara su el documento"

#: helptext.src
msgctxt ""
"helptext.src\n"
"SV_HELPTEXT_MINIMIZE\n"
"string.text"
msgid "Minimize"
msgstr "Reduzi"

#: helptext.src
msgctxt ""
"helptext.src\n"
"SV_HELPTEXT_MAXIMIZE\n"
"string.text"
msgid "Maximize"
msgstr "Ingrandisi"

#: helptext.src
msgctxt ""
"helptext.src\n"
"SV_HELPTEXT_RESTORE\n"
"string.text"
msgid "Restore"
msgstr "Reprìstena"

#: helptext.src
msgctxt ""
"helptext.src\n"
"SV_HELPTEXT_ROLLDOWN\n"
"string.text"
msgid "Drop down"
msgstr "Cascada"

#: helptext.src
msgctxt ""
"helptext.src\n"
"SV_HELPTEXT_ROLLUP\n"
"string.text"
msgid "Roll up"
msgstr "Scondi"

#: helptext.src
msgctxt ""
"helptext.src\n"
"SV_HELPTEXT_HELP\n"
"string.text"
msgid "Help"
msgstr "Juto"

#: helptext.src
msgctxt ""
"helptext.src\n"
"SV_HELPTEXT_ALWAYSVISIBLE\n"
"string.text"
msgid "Always visible"
msgstr "Senpre vizìbiłe"

#: helptext.src
msgctxt ""
"helptext.src\n"
"SV_HELPTEXT_FADEIN\n"
"string.text"
msgid "Show"
msgstr "Mostra"

#: helptext.src
msgctxt ""
"helptext.src\n"
"SV_HELPTEXT_FADEOUT\n"
"string.text"
msgid "Hide"
msgstr "Scondi"

#: helptext.src
msgctxt ""
"helptext.src\n"
"SV_HELPTEXT_SPLITFLOATING\n"
"string.text"
msgid "Floating"
msgstr "Mòbiłe"

#: helptext.src
msgctxt ""
"helptext.src\n"
"SV_HELPTEXT_SPLITFIXED\n"
"string.text"
msgid "Stick"
msgstr "Ferma"

#: helptext.src
msgctxt ""
"helptext.src\n"
"SV_SHORTCUT_HELP\n"
"string.text"
msgid "Help"
msgstr "Juto"

#: helptext.src
msgctxt ""
"helptext.src\n"
"SV_SHORTCUT_CONTEXTHELP\n"
"string.text"
msgid "Context Help"
msgstr "Guida contestuałe"

#: helptext.src
msgctxt ""
"helptext.src\n"
"SV_SHORTCUT_ACTIVEHELP\n"
"string.text"
msgid "Extended Tips"
msgstr "Guida estendesta"

#: helptext.src
msgctxt ""
"helptext.src\n"
"SV_SHORTCUT_DOCKUNDOCK\n"
"string.text"
msgid "Dock/Undock Windows"
msgstr "Taca/Destaca fenestra"

#: helptext.src
msgctxt ""
"helptext.src\n"
"SV_SHORTCUT_NEXTSUBWINDOW\n"
"string.text"
msgid "To Next Toolbar/Window"
msgstr "Fenestra/zbara strumenti seguente"

#: helptext.src
msgctxt ""
"helptext.src\n"
"SV_SHORTCUT_PREVSUBWINDOW\n"
"string.text"
msgid "To Previous Toolbar/Window"
msgstr "Fenestra/zbara strumenti presedente"

#: helptext.src
msgctxt ""
"helptext.src\n"
"SV_SHORTCUT_TODOCUMENT\n"
"string.text"
msgid "To Document"
msgstr "A'l documento"

#: helptext.src
msgctxt ""
"helptext.src\n"
"SV_SHORTCUT_MENUBAR\n"
"string.text"
msgid "To Menu Bar"
msgstr "A ła zbara menù"

#: helptext.src
msgctxt ""
"helptext.src\n"
"SV_SHORTCUT_SPLITTER\n"
"string.text"
msgid "Split window separator"
msgstr "Dividi separador de fenestra"

#: menu.src
msgctxt ""
"menu.src\n"
"SV_RESID_STRING_NOSELECTIONPOSSIBLE\n"
"string.text"
msgid "<No selection possible>"
msgstr "<Mìa posìbiłe sełesionar>"

#: menu.src
msgctxt ""
"menu.src\n"
"SV_RESID_MENU_EDIT\n"
"SV_MENU_EDIT_UNDO\n"
"menuitem.text"
msgid "~Undo"
msgstr "Desf~a"

#: menu.src
msgctxt ""
"menu.src\n"
"SV_RESID_MENU_EDIT\n"
"SV_MENU_EDIT_CUT\n"
"menuitem.text"
msgid "Cu~t"
msgstr "~Taja"

#: menu.src
msgctxt ""
"menu.src\n"
"SV_RESID_MENU_EDIT\n"
"SV_MENU_EDIT_COPY\n"
"menuitem.text"
msgid "~Copy"
msgstr "~Copia"

#: menu.src
msgctxt ""
"menu.src\n"
"SV_RESID_MENU_EDIT\n"
"SV_MENU_EDIT_PASTE\n"
"menuitem.text"
msgid "~Paste"
msgstr "~Incoła"

#: menu.src
msgctxt ""
"menu.src\n"
"SV_RESID_MENU_EDIT\n"
"SV_MENU_EDIT_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr "~Ełìmena"

#: menu.src
msgctxt ""
"menu.src\n"
"SV_RESID_MENU_EDIT\n"
"SV_MENU_EDIT_SELECTALL\n"
"menuitem.text"
msgid "Select ~All"
msgstr "Sełesiona ~tuto"

#: menu.src
msgctxt ""
"menu.src\n"
"SV_RESID_MENU_EDIT\n"
"SV_MENU_EDIT_INSERTSYMBOL\n"
"menuitem.text"
msgid "~Special Character..."
msgstr "Caràtare ~spesiałe..."

#: menu.src
msgctxt ""
"menu.src\n"
"SV_MENU_MAC_SERVICES\n"
"string.text"
msgid "Services"
msgstr "Servisi"

#: menu.src
msgctxt ""
"menu.src\n"
"SV_MENU_MAC_HIDEAPP\n"
"string.text"
msgid "Hide %PRODUCTNAME"
msgstr "Scondi %PRODUCTNAME"

#: menu.src
msgctxt ""
"menu.src\n"
"SV_MENU_MAC_HIDEALL\n"
"string.text"
msgid "Hide Others"
msgstr "Scondi altri"

#: menu.src
msgctxt ""
"menu.src\n"
"SV_MENU_MAC_SHOWALL\n"
"string.text"
msgid "Show All"
msgstr "Mostra tuto"

#: menu.src
msgctxt ""
"menu.src\n"
"SV_MENU_MAC_QUITAPP\n"
"string.text"
msgid "Quit %PRODUCTNAME"
msgstr "Va fora da %PRODUCTNAME"

#: print.src
msgctxt ""
"print.src\n"
"SV_PRINT_NATIVE_STRINGS\n"
"Preview\n"
"itemlist.text"
msgid "Preview"
msgstr "Anteprima"

#: print.src
msgctxt ""
"print.src\n"
"SV_PRINT_NATIVE_STRINGS\n"
"Page number\n"
"itemlist.text"
msgid "Page number"
msgstr "Nùmaro de pàjina"

#: print.src
msgctxt ""
"print.src\n"
"SV_PRINT_NATIVE_STRINGS\n"
"Number of pages\n"
"itemlist.text"
msgid "Number of pages"
msgstr "Nùmaro de pàjine"

#: print.src
msgctxt ""
"print.src\n"
"SV_PRINT_NATIVE_STRINGS\n"
"More\n"
"itemlist.text"
msgid "More"
msgstr "Altro"

#: print.src
msgctxt ""
"print.src\n"
"SV_PRINT_NATIVE_STRINGS\n"
"Print selection only\n"
"itemlist.text"
msgid "Print selection only"
msgstr "Stanpa soło ła sełesion"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"A0\n"
"itemlist.text"
msgid "A0"
msgstr "A0"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"A1\n"
"itemlist.text"
msgid "A1"
msgstr "A1"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"A2\n"
"itemlist.text"
msgid "A2"
msgstr "A2"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"A3\n"
"itemlist.text"
msgid "A3"
msgstr "A3"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"A4\n"
"itemlist.text"
msgid "A4"
msgstr "A4"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"A5\n"
"itemlist.text"
msgid "A5"
msgstr "A5"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"B4 (ISO)\n"
"itemlist.text"
msgid "B4 (ISO)"
msgstr "B4 (ISO)"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"B5 (ISO)\n"
"itemlist.text"
msgid "B5 (ISO)"
msgstr "B5 (ISO)"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"Letter\n"
"itemlist.text"
msgid "Letter"
msgstr "Létara"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"Legal\n"
"itemlist.text"
msgid "Legal"
msgstr "Łegałe"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"Tabloid\n"
"itemlist.text"
msgid "Tabloid"
msgstr "Tabloid"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"User Defined\n"
"itemlist.text"
msgid "User Defined"
msgstr "Parsonałizà"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"B6 (ISO)\n"
"itemlist.text"
msgid "B6 (ISO)"
msgstr "B6 (ISO)"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"C4 Envelope\n"
"itemlist.text"
msgid "C4 Envelope"
msgstr "Busta C4"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"C5 Envelope\n"
"itemlist.text"
msgid "C5 Envelope"
msgstr "Busta C5"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"C6 Envelope\n"
"itemlist.text"
msgid "C6 Envelope"
msgstr "Busta C6"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"C6/5 Envelope\n"
"itemlist.text"
msgid "C6/5 Envelope"
msgstr "Busta C6/5"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"DL Envelope\n"
"itemlist.text"
msgid "DL Envelope"
msgstr "Busta DL"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"Dia Slide\n"
"itemlist.text"
msgid "Dia Slide"
msgstr "Diapozitiva Dia"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"C\n"
"itemlist.text"
msgid "C"
msgstr "C"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"D\n"
"itemlist.text"
msgid "D"
msgstr "D"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"E\n"
"itemlist.text"
msgid "E"
msgstr "E"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"Executive\n"
"itemlist.text"
msgid "Executive"
msgstr "Ezegudivo"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"Long Bond\n"
"itemlist.text"
msgid "Long Bond"
msgstr "Long Bond"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"#8 (Monarch) Envelope\n"
"itemlist.text"
msgid "#8 (Monarch) Envelope"
msgstr "Busta #8 (Monarch)"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"#6 3/4 (Personal) Envelope\n"
"itemlist.text"
msgid "#6 3/4 (Personal) Envelope"
msgstr "Busta #6 3/4 (Parsonałe)"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"#9 Envelope\n"
"itemlist.text"
msgid "#9 Envelope"
msgstr "Busta #9"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"#10 Envelope\n"
"itemlist.text"
msgid "#10 Envelope"
msgstr "Busta #10"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"#11 Envelope\n"
"itemlist.text"
msgid "#11 Envelope"
msgstr "Busta #11"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"#12 Envelope\n"
"itemlist.text"
msgid "#12 Envelope"
msgstr "Busta #12"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"16 Kai\n"
"itemlist.text"
msgid "16 Kai"
msgstr "16 Kai"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"32 Kai\n"
"itemlist.text"
msgid "32 Kai"
msgstr "32 Kai"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"Big 32 Kai\n"
"itemlist.text"
msgid "Big 32 Kai"
msgstr "32 Kai grando"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"B4 (JIS)\n"
"itemlist.text"
msgid "B4 (JIS)"
msgstr "B4 (JIS)"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"B5 (JIS)\n"
"itemlist.text"
msgid "B5 (JIS)"
msgstr "B5 (JIS)"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"B6 (JIS)\n"
"itemlist.text"
msgid "B6 (JIS)"
msgstr "B6 (JIS)"

#: print.src
msgctxt ""
"print.src\n"
"RID_STR_PAPERNAMES\n"
"Japanese Postcard\n"
"itemlist.text"
msgid "Japanese Postcard"
msgstr "Cartołina japoneze"

#: print.src
msgctxt ""
"print.src\n"
"SV_PRINT_NOPAGES\n"
"string.text"
msgid "No pages"
msgstr "Njanca na pàjina"

#: print.src
msgctxt ""
"print.src\n"
"SV_PRINT_TOFILE_TXT\n"
"string.text"
msgid "Print to File..."
msgstr "Stanpa so file..."

#: print.src
msgctxt ""
"print.src\n"
"SV_PRINT_DEFPRT_TXT\n"
"string.text"
msgid "Default printer"
msgstr "Stanpadora predefinìa"

#: print.src
msgctxt ""
"print.src\n"
"SV_PRINT_PRINTPREVIEW_TXT\n"
"string.text"
msgid "Print preview"
msgstr "Anteprima de stanpa"

#: print.src
msgctxt ""
"print.src\n"
"SV_PRINT_QUERYFAXNUMBER_TXT\n"
"string.text"
msgid "Please enter the fax number"
msgstr "Meti rento el nùmaro de fax"

#: print.src
msgctxt ""
"print.src\n"
"SV_PRINT_INVALID_TXT\n"
"string.text"
msgid "<ignore>"
msgstr "<injora>"

#: print.src
msgctxt ""
"print.src\n"
"SV_PRINT_CUSTOM_TXT\n"
"string.text"
msgid "Custom"
msgstr "Parsonałizà"

#: stdtext.src
msgctxt ""
"stdtext.src\n"
"SV_STDTEXT_SERVICENOTAVAILABLE\n"
"string.text"
msgid ""
"The component (%s) could not be loaded.\n"
"Please start setup with the repair option."
msgstr ""
"No ze mìa posìbiłe cargar el conponente (%s).\n"
"Retaca danovo el programa de instałasion co l'opsion de reparasion."

#: stdtext.src
msgctxt ""
"stdtext.src\n"
"SV_STDTEXT_DONTASKAGAIN\n"
"string.text"
msgid "Do not show this question again."
msgstr "No stà mìa pì mostrar uncora 'sta domanda."

#: stdtext.src
msgctxt ""
"stdtext.src\n"
"SV_STDTEXT_DONTWARNAGAIN\n"
"string.text"
msgid "Do not show warning again."
msgstr "No stà mìa pì mostrar uncora 'sto avizo."

#: stdtext.src
msgctxt ""
"stdtext.src\n"
"SV_ACCESSERROR_NO_FONTS\n"
"string.text"
msgid "No fonts could be found on the system."
msgstr "No ze mìa stà trovà caràtari inte'l sistema."

#: stdtext.src
msgctxt ""
"stdtext.src\n"
"SV_STDTEXT_ABOUT\n"
"string.text"
msgid "About %PRODUCTNAME"
msgstr "Informasion so %PRODUCTNAME"

#: stdtext.src
msgctxt ""
"stdtext.src\n"
"SV_STDTEXT_PREFERENCES\n"
"string.text"
msgid "Preferences..."
msgstr "Prefarense..."

#: stdtext.src
msgctxt ""
"stdtext.src\n"
"SV_EDIT_WARNING_STR\n"
"string.text"
msgid "The inserted text exceeded the maximum length of this text field. The text was truncated."
msgstr "El testo insarìo el ze masa łungo par 'sto canpo de testo. El testo el ze stà troncà."

#: stdtext.src
msgctxt ""
"stdtext.src\n"
"SV_MAC_SCREENNNAME\n"
"string.text"
msgid "Screen %d"
msgstr "Schermo %d"

#: stdtext.src
msgctxt ""
"stdtext.src\n"
"SV_STDTEXT_ALLFILETYPES\n"
"string.text"
msgid "Any type"
msgstr "Tuti i tipi"

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"mm\n"
"itemlist.text"
msgid "mm"
msgstr "mm"

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"cm\n"
"itemlist.text"
msgid "cm"
msgstr "cm"

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"m\n"
"itemlist.text"
msgid "m"
msgstr "m"

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"km\n"
"itemlist.text"
msgid "km"
msgstr "km"

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"twips\n"
"itemlist.text"
msgid "twips"
msgstr "twips"

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"twip\n"
"itemlist.text"
msgid "twip"
msgstr "twip"

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"pt\n"
"itemlist.text"
msgid "pt"
msgstr "pt"

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"pc\n"
"itemlist.text"
msgid "pc"
msgstr "pc"

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"\"\n"
"itemlist.text"
msgid "\""
msgstr "\""

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"in\n"
"itemlist.text"
msgid "in"
msgstr "in"

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"inch\n"
"itemlist.text"
msgid "inch"
msgstr "pòłeze"

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"'\n"
"itemlist.text"
msgid "'"
msgstr "'"

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"ft\n"
"itemlist.text"
msgid "ft"
msgstr "ft"

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"foot\n"
"itemlist.text"
msgid "foot"
msgstr "pie"

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"feet\n"
"itemlist.text"
msgid "feet"
msgstr "pie"

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"miles\n"
"itemlist.text"
msgid "miles"
msgstr "mejare"

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"mile\n"
"itemlist.text"
msgid "mile"
msgstr "mejara"

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"ch\n"
"itemlist.text"
msgid "ch"
msgstr "car"

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"line\n"
"itemlist.text"
msgid "line"
msgstr "łinea"

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"pixels\n"
"itemlist.text"
msgid "pixels"
msgstr "pixel"

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"pixel\n"
"itemlist.text"
msgid "pixel"
msgstr "pixel"

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"°\n"
"itemlist.text"
msgid "°"
msgstr "°"

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"sec\n"
"itemlist.text"
msgid "sec"
msgstr "sec"

#: units.src
msgctxt ""
"units.src\n"
"SV_FUNIT_STRINGS\n"
"ms\n"
"itemlist.text"
msgid "ms"
msgstr "ms"

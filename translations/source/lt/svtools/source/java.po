#. extracted from svtools/source/java
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2017-03-06 09:27+0000\n"
"Last-Translator: Modestas Rimkus <modestas.rimkus@gmail.com>\n"
"Language-Team: Lithuanian <komp_lt@konferencijos.lt>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-Project-Style: openoffice\n"
"X-POOTLE-MTIME: 1488792470.000000\n"

#: javaerror.src
msgctxt ""
"javaerror.src\n"
"STR_WARNING_JAVANOTFOUND\n"
"string.text"
msgid "%PRODUCTNAME requires a Java runtime environment (JRE) to perform this task. Please install a JRE and restart %PRODUCTNAME."
msgstr "Šiam veiksmui atlikti „%PRODUCTNAME“ programai reikalinga „Java“ programų vykdymo terpė (JRE). Įdiekite JRE ir iš naujo paleiskite „%PRODUCTNAME“."

#: javaerror.src
msgctxt ""
"javaerror.src\n"
"STR_WARNING_JAVANOTFOUND_MAC\n"
"string.text"
msgid "%PRODUCTNAME requires Oracle's Java Development Kit (JDK) on Mac OS X 10.10 or greater to perform this task. Please install them and restart %PRODUCTNAME."
msgstr "Šiai užduočiai atlikti „%PRODUCTNAME“ reikalingas „Oracle“ Javos komplektas (JDK), veikiantis 10.10 ar vėlesnės versijos „Mac OS X“ sistemoje. Įdiekite reikiamą komplektą ir paleiskite „%PRODUCTNAME“ iš naujo."

#: javaerror.src
msgctxt ""
"javaerror.src\n"
"STR_WARNING_INVALIDJAVASETTINGS_MAC\n"
"string.text"
msgid "The %PRODUCTNAME configuration has been changed. Under %PRODUCTNAME - Preferences - %PRODUCTNAME - Advanced, select the Java runtime environment you want to have used by %PRODUCTNAME."
msgstr "„%PRODUCTNAME“ sąranka buvo pakeista. Lange „%PRODUCTNAME → Parinktys → %PRODUCTNAME → Kitos parinktys“ parinkite „Java“ programų vykdymo terpę, kurią turėtų naudoti „%PRODUCTNAME“ programa."

#: javaerror.src
msgctxt ""
"javaerror.src\n"
"STR_WARNING_INVALIDJAVASETTINGS\n"
"string.text"
msgid "The %PRODUCTNAME configuration has been changed. Under Tools - Options - %PRODUCTNAME - Advanced, select the Java runtime environment you want to have used by %PRODUCTNAME."
msgstr "„%PRODUCTNAME“ sąranka buvo pakeista. Lange „Priemonės → Parinktys → %PRODUCTNAME →Kitos parinktys“ pasirinkite „Java“ programų vykdymo terpę, kurią turėtų naudoti „%PRODUCTNAME“ programa."

#: javaerror.src
msgctxt ""
"javaerror.src\n"
"STR_ERROR_JVMCREATIONFAILED_MAC\n"
"string.text"
msgid "%PRODUCTNAME requires a Java runtime environment (JRE) to perform this task. The selected JRE is defective. Please select another version or install a new JRE and select it under %PRODUCTNAME - Preferences - %PRODUCTNAME - Advanced."
msgstr "Šiam veiksmui atlikti „%PRODUCTNAME“ programai reikalinga „Java“ programų vykdymo terpė (JRE). Parinkta „Java“ terpė yra sugadinta. Parinkite kitą JRE versiją arba įdiekite kitą „Java“ terpę ir parinkite ją lange „%PRODUCTNAME → Parinktys → %PRODUCTNAME → Kitos parinktys“."

#: javaerror.src
msgctxt ""
"javaerror.src\n"
"STR_ERROR_JVMCREATIONFAILED\n"
"string.text"
msgid "%PRODUCTNAME requires a Java runtime environment (JRE) to perform this task. The selected JRE is defective. Please select another version or install a new JRE and select it under Tools - Options - %PRODUCTNAME - Advanced."
msgstr "Šiam veiksmui atlikti „%PRODUCTNAME“ programai reikalinga „Java“ programų vykdymo terpė (JRE). Parinkta „Java“ terpė yra sugadinta. Parinkite kitą JRE versiją arba įdiekite kitą „Java“ terpę ir parinkite ją lange „Priemonės → Parinktys → %PRODUCTNAME → Kitos parinktys“."

#: javaerror.src
msgctxt ""
"javaerror.src\n"
"STR_WARNING_JAVANOTFOUND_TITLE\n"
"string.text"
msgid "JRE Required"
msgstr "Reikalinga JRE"

#: javaerror.src
msgctxt ""
"javaerror.src\n"
"STR_WARNING_INVALIDJAVASETTINGS_TITLE\n"
"string.text"
msgid "Select JRE"
msgstr "JRE parinkimas"

#: javaerror.src
msgctxt ""
"javaerror.src\n"
"STR_ERROR_JVMCREATIONFAILED_TITLE\n"
"string.text"
msgid "JRE is Defective"
msgstr "JRE sugadinta"

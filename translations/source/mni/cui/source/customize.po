#. extracted from cui/source/customize
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-04-17 04:30+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: mni\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1460867426.000000\n"

#: acccfg.src
msgctxt ""
"acccfg.src\n"
"RID_SVXPAGE_CONFIGGROUPBOX\n"
"STR_BASICMACROS\n"
"string.text"
msgid "BASIC Macros"
msgstr "BASIC মেক্রোজ"

#: acccfg.src
#, fuzzy
msgctxt ""
"acccfg.src\n"
"RID_SVXPAGE_CONFIGGROUPBOX\n"
"STR_GROUP_STYLES\n"
"string.text"
msgid "Styles"
msgstr ""
"#-#-#-#-#  index.po (PACKAGE VERSION)  #-#-#-#-#\n"
"স্তাইলশিং\n"
"#-#-#-#-#  doc.po (PACKAGE VERSION)  #-#-#-#-#\n"
"মওংশিং\n"
"#-#-#-#-#  dialog.po (PACKAGE VERSION)  #-#-#-#-#\n"
"মওংশিং"

#: cfg.src
msgctxt ""
"cfg.src\n"
"TEXT_RENAME\n"
"#define.text"
msgid "Rename..."
msgstr "মিং ওনবা..."

#: cfg.src
#, fuzzy
msgctxt ""
"cfg.src\n"
"TEXT_DELETE_NODOTS\n"
"#define.text"
msgid "Delete"
msgstr ""
"#-#-#-#-#  utlui.po (PACKAGE VERSION)  #-#-#-#-#\n"
"মুত্থতলু\n"
"#-#-#-#-#  config.po (PACKAGE VERSION)  #-#-#-#-#\n"
"মু্ত্থত্পা\n"
"#-#-#-#-#  fldui.po (PACKAGE VERSION)  #-#-#-#-#\n"
"মুত্থতলু\n"
"#-#-#-#-#  app.po (PACKAGE VERSION)  #-#-#-#-#\n"
"মুত্থত্পা\n"
"#-#-#-#-#  navipi.po (PACKAGE VERSION)  #-#-#-#-#\n"
"মুত্থতলু\n"
"#-#-#-#-#  appl.po (PACKAGE VERSION)  #-#-#-#-#\n"
"মুত্থত্পা\n"
"#-#-#-#-#  app.po (PACKAGE VERSION)  #-#-#-#-#\n"
"মুত্থত্পা"

#: cfg.src
msgctxt ""
"cfg.src\n"
"MODIFY_TOOLBAR_CONTENT\n"
"ID_DEFAULT_COMMAND\n"
"menuitem.text"
msgid "Restore Default Command"
msgstr "দিফোল্ত কমান্দ রিস্তোর তৌরো"

#: cfg.src
msgctxt ""
"cfg.src\n"
"MODIFY_TOOLBAR_CONTENT\n"
"ID_CHANGE_SYMBOL\n"
"menuitem.text"
msgid "Change Icon..."
msgstr "আইকোন হোংদোকপা..."

#: cfg.src
msgctxt ""
"cfg.src\n"
"MODIFY_TOOLBAR_CONTENT\n"
"ID_RESET_SYMBOL\n"
"menuitem.text"
msgid "Reset Icon"
msgstr "আইকোন রিসেত তৌবা"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_NEW_MENU\n"
"string.text"
msgid "New Menu %n"
msgstr "অনৌবা মেন্য়ু %n"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_NEW_TOOLBAR\n"
"string.text"
msgid "New Toolbar %n"
msgstr "অনৌবা তুলবার %n"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_MOVE_MENU\n"
"string.text"
msgid "Move Menu"
msgstr "মেন্য়ু মফম হোংদোকপা"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_ADD_SUBMENU\n"
"string.text"
msgid "Add Submenu"
msgstr "Add Sub-menu"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_SUBMENU_NAME\n"
"string.text"
msgid "Submenu name"
msgstr "সবমেন্য়ু মমিং"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_MENU_ADDCOMMANDS_DESCRIPTION\n"
"string.text"
msgid "To add a command to a menu, select the category and then the command. You can also drag the command to the Commands list of the Menus tab page in the Customize dialog."
msgstr ""
"মেন্য়ুদা কমান্দ হাপচিন্নবা, কাংলুপ খল্লো অদুগা কমান্দ খল্লো. অদোম্না \n"
" কস্তমাইজ দাইলোগতা মেন্য়ুগী তেব লামাযগী কমান্দ পরিংদা কমান্দ চিংশিনবা যাগনি."

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_IMPORT_ICON_ERROR\n"
"string.text"
msgid "The files listed below could not be imported. The file format could not be interpreted."
msgstr ""
"মখাগী পরিং অসিদা পীরিবা ফাইলশিং অসি পুশিল্লকপা যারোই.\n"
" ফাইল ফোর্মেত খঙদোকপা ঙম্মোই."

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_DELETE_ICON_CONFIRM\n"
"string.text"
msgid "Are you sure to delete the image?"
msgstr "ইমেজ অসি মুত্থৎপা শোযদ্রব্রা?"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_REPLACE_ICON_WARNING\n"
"string.text"
msgid ""
"The icon %ICONNAME is already contained in the image list.\n"
"Would you like to replace the existing icon?"
msgstr ""
"আইকোন %ICONNAME অসি হান্নদগী ইমেজগী পরিংদা যাওরে.\n"
" লৈরিবা আইকোন অসি মহুৎ শিন্দোকপা পাম্বিব্রা?"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_REPLACE_ICON_CONFIRM\n"
"string.text"
msgid "Confirm Icon Replacement"
msgstr "আইকোন মহুৎ শিন্দোকপা যারব্রা"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_YESTOALL\n"
"string.text"
msgid "Yes to All"
msgstr "পুম্নমক্তা হোই"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_PRODUCTNAME_TOOLBARS\n"
"string.text"
msgid "%PRODUCTNAME %MODULENAME Toolbars"
msgstr "%PRODUCTNAME %MODULENAME তুলবারশিং"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_PRODUCTNAME_CONTEXTMENUS\n"
"string.text"
msgid "%PRODUCTNAME %MODULENAME Context Menus"
msgstr ""

#: cfg.src
#, fuzzy
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_TOOLBAR\n"
"string.text"
msgid "Toolbar"
msgstr "~তুলবারশিং"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_TOOLBAR_CONTENT\n"
"string.text"
msgid "Toolbar Content"
msgstr "তুলবারগী হীরম"

#: cfg.src
#, fuzzy
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_COMMANDS\n"
"string.text"
msgid "Commands"
msgstr "থৌদাৱা"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_COMMAND\n"
"string.text"
msgid "Command"
msgstr "থৌদাৱা"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_TOOLBAR_NAME\n"
"string.text"
msgid "Toolbar Name"
msgstr "তুবার মমিং"

#: cfg.src
#, fuzzy
msgctxt ""
"cfg.src\n"
"RID_SXVSTR_CONFIRM_DELETE_TOOLBAR\n"
"string.text"
msgid "There are no more commands on the toolbar. Do you want to delete the toolbar?"
msgstr "তুলবারদা অহেনবা কমান্দ অমত্তা লৈত্রে. অদোম্না তুলবার মুত্থৎপা পামলব্রা?"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_CONFIRM_MENU_RESET\n"
"string.text"
msgid "The menu configuration for %SAVE IN SELECTION% will be reset to the default settings. Do you want to continue?"
msgstr ""

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_CONFIRM_TOOLBAR_RESET\n"
"string.text"
msgid "The toolbar configuration for %SAVE IN SELECTION% will be reset to the default settings. Do you want to continue?"
msgstr ""

#: cfg.src
#, fuzzy
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_CONFIRM_RESTORE_DEFAULT\n"
"string.text"
msgid "This will delete all changes previously made to this toolbar. Do you really want to reset the toolbar?"
msgstr ""
"মসিনা মমাংদা তুলবার অসিদা হোংদোকখিবা পুম্নমক মুত্থৎলগনি. অদোম্না তুলবার অসি \n"
" রিসেত তৌবা তশেংনা পামলব্রা?"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_MNUCFG_ALREADY_INCLUDED\n"
"string.text"
msgid "Function is already included in this popup."
msgstr "ফঙ্কসন অসি হান্নদগী পোপঅপ অসিদা যাওরে."

#: cfg.src
#, fuzzy
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_LABEL_NEW_NAME\n"
"string.text"
msgid "~New name"
msgstr "অনৌবা মিং"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_RENAME_MENU\n"
"string.text"
msgid "Rename Menu"
msgstr "মেন্য়ু মমিং ওনবা"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_RENAME_TOOLBAR\n"
"string.text"
msgid "Rename Toolbar"
msgstr "তুলবার মমিং ওনবা"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_STARTAPP\n"
"string.text"
msgid "Start Application"
msgstr "এপ্লকেসন হৌবা"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_CLOSEAPP\n"
"string.text"
msgid "Close Application"
msgstr "এপ্লিকেসন লোইবা"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_NEWDOC\n"
"string.text"
msgid "New Document"
msgstr "অনৌবা দোকুমেন্ত"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_CLOSEDOC\n"
"string.text"
msgid "Document closed"
msgstr "দোকুমেন্ত লোইশিল্লে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_PREPARECLOSEDOC\n"
"string.text"
msgid "Document is going to be closed"
msgstr "দোকুমেন্ত অসি লোইশিনগদৌরে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_OPENDOC\n"
"string.text"
msgid "Open Document"
msgstr "অহাংবা দোকুমেন্ত "

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_SAVEDOC\n"
"string.text"
msgid "Save Document"
msgstr "দোকুমেন্ত সেভ তৌবা"

#: macropg.src
#, fuzzy
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_SAVEASDOC\n"
"string.text"
msgid "Save Document As"
msgstr "দোকুমেন্ত সেভ তৌবা"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_SAVEDOCDONE\n"
"string.text"
msgid "Document has been saved"
msgstr "দোকুমেন্ত সেভ তৌরে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_SAVEASDOCDONE\n"
"string.text"
msgid "Document has been saved as"
msgstr "দোকুমেন্ত অসিগুম্না সেভ তৌরে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_ACTIVATEDOC\n"
"string.text"
msgid "Activate Document"
msgstr "দোকুমেন্ত শিনবাংহনবা"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_DEACTIVATEDOC\n"
"string.text"
msgid "Deactivate Document"
msgstr "দোকুমেন্ত শিনবাংহনদবা"

#: macropg.src
#, fuzzy
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_PRINTDOC\n"
"string.text"
msgid "Print Document"
msgstr "লমাইগী শক্তম নমথোকপা"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_MODIFYCHANGED\n"
"string.text"
msgid "'Modified' status was changed"
msgstr "'শেমদোক্লবা' স্তেতস হোংদোকখ্রে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_MAILMERGE\n"
"string.text"
msgid "Printing of form letters started"
msgstr "ফোর্ম লেতরশিং নমথোকপা হৌরে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_MAILMERGE_END\n"
"string.text"
msgid "Printing of form letters finished"
msgstr "ফোর্ম লেতরশিং নমথোকপা লোইরে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_FIELDMERGE\n"
"string.text"
msgid "Merging of form fields started"
msgstr "ফোর্ম ফিল্দশিং পুনশিনবা হৌরে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_FIELDMERGE_FINISHED\n"
"string.text"
msgid "Merging of form fields finished"
msgstr "ফোর্ম ফিল্দশিং পুনশিনবা লোইরে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_PAGECOUNTCHANGE\n"
"string.text"
msgid "Changing the page count"
msgstr "লমায় মশিং থীবা হোংদোক্লি"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_SUBCOMPONENT_OPENED\n"
"string.text"
msgid "Loaded a sub component"
msgstr "সব কম্পোনেন্ত অমা লোদ তৌরে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_SUBCOMPONENT_CLOSED\n"
"string.text"
msgid "Closed a sub component"
msgstr "সব কম্পোনেন্ত অমা লোইশিল্লে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_APPROVEPARAMETER\n"
"string.text"
msgid "Fill parameters"
msgstr "পেরামিতরশিং মেনশিনবা"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_ACTIONPERFORMED\n"
"string.text"
msgid "Execute action"
msgstr "হৌবা মতমদা"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_AFTERUPDATE\n"
"string.text"
msgid "After updating"
msgstr "অপদেত তৌরবা মতুং"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_BEFOREUPDATE\n"
"string.text"
msgid "Before updating"
msgstr "অপদেত তৌদ্রিঙৈগী মমাংদা"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_APPROVEROWCHANGE\n"
"string.text"
msgid "Before record action"
msgstr "রেকোর্দ থবক তৌদ্রিঙৈগী মমাং"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_ROWCHANGE\n"
"string.text"
msgid "After record action"
msgstr "রেকোর্দ থবক তৌরবা মতুং"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_CONFIRMDELETE\n"
"string.text"
msgid "Confirm deletion"
msgstr "মুত্থত্পদা চুমলবা"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_ERROROCCURRED\n"
"string.text"
msgid "Error occurred"
msgstr "অশোয়বা থোক্লে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_ADJUSTMENTVALUECHANGED\n"
"string.text"
msgid "While adjusting"
msgstr "চান্নহল্লিঙৈগী মতমদা"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_FOCUSGAINED\n"
"string.text"
msgid "When receiving focus"
msgstr "ফোকস ফংবা মতমদা"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_FOCUSLOST\n"
"string.text"
msgid "When losing focus"
msgstr "ফোকস মাংখিবা মতমদা"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_ITEMSTATECHANGED\n"
"string.text"
msgid "Item status changed"
msgstr "আইতেমগী ফিবম হোংলে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_KEYTYPED\n"
"string.text"
msgid "Key pressed"
msgstr "কি নম্লে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_KEYUP\n"
"string.text"
msgid "Key released"
msgstr "কি থাদোক্লে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_LOADED\n"
"string.text"
msgid "When loading"
msgstr "লোদ তৌবা মতমদা"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_RELOADING\n"
"string.text"
msgid "Before reloading"
msgstr "হন্নালোদ তৌদ্রিঙৈগী মমাংদা"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_RELOADED\n"
"string.text"
msgid "When reloading"
msgstr "হন্নালোদ তৌরিঙৈগী মতমদা"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_MOUSEDRAGGED\n"
"string.text"
msgid "Mouse moved while key pressed"
msgstr "কি নম্লিঙৈগী মতমদা মাউস লেংঙে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_MOUSEENTERED\n"
"string.text"
msgid "Mouse inside"
msgstr "মাউস মনুংদা"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_MOUSEEXITED\n"
"string.text"
msgid "Mouse outside"
msgstr "মাউস মপান্দা"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_MOUSEMOVED\n"
"string.text"
msgid "Mouse moved"
msgstr "মাউস লেংঙে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_MOUSEPRESSED\n"
"string.text"
msgid "Mouse button pressed"
msgstr "মাউসকী বতন নম্লে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_MOUSERELEASED\n"
"string.text"
msgid "Mouse button released"
msgstr "মাউসকী বতন থাদোক্লে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_POSITIONING\n"
"string.text"
msgid "Before record change"
msgstr "রেকোর্দ হোংদ্রিঙৈগী মমাং"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_POSITIONED\n"
"string.text"
msgid "After record change"
msgstr "রেকোর্দ হোংলবা মতুং"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_RESETTED\n"
"string.text"
msgid "After resetting"
msgstr "রিসেত তৌরবা মতুং"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_APPROVERESETTED\n"
"string.text"
msgid "Prior to reset"
msgstr "রিসেত তৌদ্রিঙৈগী মমাং"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_APPROVEACTIONPERFORMED\n"
"string.text"
msgid "Approve action"
msgstr "হৌদ্রিঙৈগী মমাং"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_SUBMITTED\n"
"string.text"
msgid "Before submitting"
msgstr "পিশিদ্রিঙৈগী মমাং"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_TEXTCHANGED\n"
"string.text"
msgid "Text modified"
msgstr "তেক্স শেমদোক্লে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_UNLOADING\n"
"string.text"
msgid "Before unloading"
msgstr "অনলোদ তৌদ্রিঙৈগী মমাং"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_UNLOADED\n"
"string.text"
msgid "When unloading"
msgstr "অনলোদ তৌবা মতমদা"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_CHANGED\n"
"string.text"
msgid "Changed"
msgstr "হোংলবা "

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_CREATEDOC\n"
"string.text"
msgid "Document created"
msgstr "দোকুমেন্ত শেমগৎলে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_LOADDOCFINISHED\n"
"string.text"
msgid "Document loading finished"
msgstr "দোকুমেন্ত লোদ তৌবা লোইরে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_SAVEDOCFAILED\n"
"string.text"
msgid "Saving of document failed"
msgstr "দোকুমেন্ত সেভ তৌবা মায পাক্ত্রে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_SAVEASDOCFAILED\n"
"string.text"
msgid "'Save as' has failed"
msgstr "'অসিগুম্না সেভ তৌবা' মায পাক্ত্রে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_COPYTODOC\n"
"string.text"
msgid "Storing or exporting copy of document"
msgstr "দোকুমেন্তগী কোপি স্তোর তৌরি নত্রগা থাদোক্লি"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_COPYTODOCDONE\n"
"string.text"
msgid "Document copy has been created"
msgstr "দোকুমেন্ত কোপি শেমগৎলে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_COPYTODOCFAILED\n"
"string.text"
msgid "Creating of document copy failed"
msgstr "দোকুমেন্ত কোপি শেমগৎপা মায পাক্ত্রে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_VIEWCREATED\n"
"string.text"
msgid "View created"
msgstr "মীৎযেং শেমগৎলে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_PREPARECLOSEVIEW\n"
"string.text"
msgid "View is going to be closed"
msgstr "মীৎযেং অসি লোইশিনগদৌরে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_CLOSEVIEW\n"
"string.text"
msgid "View closed"
msgstr "মীৎযেং লোইশিল্লে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_TITLECHANGED\n"
"string.text"
msgid "Document title changed"
msgstr "দোকুমেন্ত মমিং হোংদোক্লে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_MODECHANGED\n"
"string.text"
msgid "Document mode changed"
msgstr "দোকুমেন্ত মোদ হোংদোক্লে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_VISAREACHANGED\n"
"string.text"
msgid "Visible area changed"
msgstr "উবা ঙমবা এরিযা হোংদোক্লে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_STORAGECHANGED\n"
"string.text"
msgid "Document has got a new storage"
msgstr "দোকুমেন্তগী অনৌবা স্তোরেজ অম ফংলে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_LAYOUT_FINISHED\n"
"string.text"
msgid "Document layout finished"
msgstr "দোকুমেন্ত লেআউত লোইরে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_SELECTIONCHANGED\n"
"string.text"
msgid "Selection changed"
msgstr "অখনবা হোংদোক্লে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_DOUBLECLICK\n"
"string.text"
msgid "Double click"
msgstr "দবল ক্লিক"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_RIGHTCLICK\n"
"string.text"
msgid "Right click"
msgstr "যেৎথংবা ক্লিক"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_CALCULATE\n"
"string.text"
msgid "Formulas calculated"
msgstr "ফোর্মুলা হোৎলে"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_CONTENTCHANGED\n"
"string.text"
msgid "Content changed"
msgstr "হীরম হোংদোক্লে"

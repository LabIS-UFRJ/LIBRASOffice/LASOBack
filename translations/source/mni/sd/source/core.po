#. extracted from sd/source/core
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-12-01 12:12+0100\n"
"PO-Revision-Date: 2016-07-05 03:35+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: mni\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1467689749.000000\n"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_LAYER_BCKGRND\n"
"string.text"
msgid "Background"
msgstr "হৌরকফম"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_LAYER_BCKGRNDOBJ\n"
"string.text"
msgid "Background objects"
msgstr "হৌরকফমগী পোত্শকশিং"

#: glob.src
#, fuzzy
msgctxt ""
"glob.src\n"
"STR_LAYER_LAYOUT\n"
"string.text"
msgid "Layout"
msgstr "উত্পা"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_LAYER_CONTROLS\n"
"string.text"
msgid "Controls"
msgstr "কন্ত্রোলশিং"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_LAYER_MEASURELINES\n"
"string.text"
msgid "Dimension Lines"
msgstr "মাইকৈ তাকপা পরিংশিং"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PAGE\n"
"string.text"
msgid "Slide"
msgstr "স্লাইদ"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PAGE_NAME\n"
"string.text"
msgid "Page"
msgstr "লমাই"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_SLIDE_NAME\n"
"string.text"
msgid "Slide"
msgstr "স্লাইদ"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_MASTERSLIDE_NAME\n"
"string.text"
msgid "Master Slide"
msgstr ""

#: glob.src
msgctxt ""
"glob.src\n"
"STR_MASTERPAGE_NAME\n"
"string.text"
msgid "Master Page"
msgstr ""

#: glob.src
msgctxt ""
"glob.src\n"
"STR_MASTERPAGE\n"
"string.text"
msgid "Background"
msgstr "হৌরকফম"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_NOTES\n"
"string.text"
msgid "(Notes)"
msgstr "(খংজিনগদবশিং)"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_HANDOUT\n"
"string.text"
msgid "Handouts"
msgstr "হেন্দআউতস"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PRESOBJ_MPTITLE\n"
"string.text"
msgid "Click to edit the title text format"
msgstr "মিংথোলগী তেক্সত ফোর্মেত সেমদোক-শাদোক তৌলবগীদমক ক্লিক তৌ"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PRESOBJ_MPOUTLINE\n"
"string.text"
msgid "Click to edit the outline text format"
msgstr "মপান্থোংগী ওইবা শক্তম তেক্সত ফোর্মেত শেমদোক-শাদোক তৌনবগীদমক ক্লিক তৌ"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PRESOBJ_MPOUTLLAYER2\n"
"string.text"
msgid "Second Outline Level"
msgstr "অনীশুবা মপান্থোংগী ওইবা শক্তমগী থাক"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PRESOBJ_MPOUTLLAYER3\n"
"string.text"
msgid "Third Outline Level"
msgstr "অহুমশুবা মপান্থোংগী ওইবা শক্তমগী থাক"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PRESOBJ_MPOUTLLAYER4\n"
"string.text"
msgid "Fourth Outline Level"
msgstr "মরীশুবা মপান্থোংগী ওইবা শক্তমগী থাক"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PRESOBJ_MPOUTLLAYER5\n"
"string.text"
msgid "Fifth Outline Level"
msgstr "মঙাশুবা মপান্থোংগী ওইবা শক্তমগী থাক"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PRESOBJ_MPOUTLLAYER6\n"
"string.text"
msgid "Sixth Outline Level"
msgstr "তরুকশুবা মপান্থোংগী ওইবা শক্তমগী থাক"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PRESOBJ_MPOUTLLAYER7\n"
"string.text"
msgid "Seventh Outline Level"
msgstr "তরেতশুবা মপান্থোংগী ওইবা শক্তমগী থাক"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PRESOBJ_MPOUTLLAYER8\n"
"string.text"
msgid "Eighth Outline Level"
msgstr "নিপালশুবা মপান্থোংগী ওইবা শক্তমগী থাক"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PRESOBJ_MPOUTLLAYER9\n"
"string.text"
msgid "Ninth Outline Level"
msgstr "মাপলশুবা মপান্থোংগী ওইবা শক্তমগী থাক"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PRESOBJ_MPNOTESTITLE\n"
"string.text"
msgid "Click to move the slide"
msgstr "শ্লাইদ মফম হোংনবগীদমক ক্লিক তৌ"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PRESOBJ_MPNOTESTEXT\n"
"string.text"
msgid "Click to edit the notes format"
msgstr "খঙজিনগদবশিং ফোর্মেত শেমদোক-শাদোক তৌনবগীদমক ক্লিক তৌ"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PRESOBJ_TITLE\n"
"string.text"
msgid "Click to add Title"
msgstr ""

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PRESOBJ_OUTLINE\n"
"string.text"
msgid "Click to add Text"
msgstr ""

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PRESOBJ_TEXT\n"
"string.text"
msgid "Click to add Text"
msgstr ""

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PRESOBJ_NOTESTEXT\n"
"string.text"
msgid "Click to add Notes"
msgstr ""

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PRESOBJ_GRAPHIC\n"
"string.text"
msgid "Double-click to add an Image"
msgstr ""

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PRESOBJ_OBJECT\n"
"string.text"
msgid "Double-click to add an Object"
msgstr ""

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PRESOBJ_CHART\n"
"string.text"
msgid "Double-click to add a Chart"
msgstr ""

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PRESOBJ_ORGCHART\n"
"string.text"
msgid "Double-click to add an Organization Chart"
msgstr ""

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PRESOBJ_TABLE\n"
"string.text"
msgid "Double-click to add a Spreadsheet"
msgstr ""

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PREVIEWVIEWSHELL\n"
"string.text"
msgid "Preview Window"
msgstr "ৱিন্দোগী হান্নগী শক্তম"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_STDOBJECTBARSHELL\n"
"string.text"
msgid "Document Mode"
msgstr "দোকুমেন্ত মখল"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_GLUEPOINTSOBJECTBARSHELL\n"
"string.text"
msgid "Glue Points Mode"
msgstr "গ্লু বিন্দুশিংগী মখল"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_LAYOUT_DEFAULT_NAME\n"
"string.text"
msgid "Default"
msgstr "দিফোল্ত "

#: glob.src
msgctxt ""
"glob.src\n"
"STR_LAYOUT_DEFAULT_TITLE_NAME\n"
"string.text"
msgid "Title"
msgstr "মিংথোল"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_STANDARD_STYLESHEET_NAME\n"
"string.text"
msgid "Default"
msgstr "দিফোল্ত "

#: glob.src
msgctxt ""
"glob.src\n"
"STR_UNDO_MOVEPAGES\n"
"string.text"
msgid "Move slides"
msgstr "স্লাইদশিংগী মফম হোংবা"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_NOT_ENOUGH_MEMORY\n"
"string.text"
msgid ""
"Not enough memory!\n"
"The action will be aborted."
msgstr ""
"চপ চাবা নিংসিংবা লৈতবা!\n"
"থবক্তু কান্নররোয়."

#: glob.src
msgctxt ""
"glob.src\n"
"STR_POOLSHEET_MEASURE\n"
"string.text"
msgid "Dimension Line"
msgstr "মাইকৈ তাকপা পরিং"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_POOLSHEET_OBJNOLINENOFILL\n"
"string.text"
msgid "Object with no fill and no line"
msgstr ""

#: glob.src
msgctxt ""
"glob.src\n"
"STR_POOLSHEET_OBJWITHARROW\n"
"string.text"
msgid "Object with arrow"
msgstr "তেনজৈগা লোয়ননা পোত্শক"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_POOLSHEET_OBJWITHSHADOW\n"
"string.text"
msgid "Object with shadow"
msgstr "মমিগা লোয়ননা পোত্শক"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_POOLSHEET_OBJWITHOUTFILL\n"
"string.text"
msgid "Object without fill"
msgstr "খনহন্দবা পোত্শক"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_POOLSHEET_TEXT\n"
"string.text"
msgid "Text"
msgstr "তেক্সত"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_POOLSHEET_TEXTBODY\n"
"string.text"
msgid "Text body"
msgstr "তেক্সতকী বোদি"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_POOLSHEET_TEXTBODY_JUSTIFY\n"
"string.text"
msgid "Text body justified"
msgstr "তেক্সত বোদি চুমথোক্লবা"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_POOLSHEET_TEXTBODY_INDENT\n"
"string.text"
msgid "First line indent"
msgstr "অহানবা পরিং ইন্দেন্ত"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_POOLSHEET_TITLE\n"
"string.text"
msgid "Title"
msgstr "মিংথোল"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_POOLSHEET_TITLE1\n"
"string.text"
msgid "Title1"
msgstr "মিংথোল 1"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_POOLSHEET_TITLE2\n"
"string.text"
msgid "Title2"
msgstr "মিংথোল 2"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_POOLSHEET_HEADLINE\n"
"string.text"
msgid "Heading"
msgstr "মকোক্কী মমিং"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_POOLSHEET_HEADLINE1\n"
"string.text"
msgid "Heading1"
msgstr "মকোক্কী মমিং1"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_POOLSHEET_HEADLINE2\n"
"string.text"
msgid "Heading2"
msgstr "মকোক্কী মমিং 2"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_EMPTY_STYLESHEET_NAME\n"
"string.text"
msgid "Blank template"
msgstr "অহাংবা নোমুনা"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PSEUDOSHEET_TITLE\n"
"string.text"
msgid "Title"
msgstr "মিংথোল"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PSEUDOSHEET_SUBTITLE\n"
"string.text"
msgid "Subtitle"
msgstr "সবতাইতল(মথংগী মমিং)"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PSEUDOSHEET_OUTLINE\n"
"string.text"
msgid "Outline"
msgstr "মপান্থোংগী ওইবা শক্তম"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PSEUDOSHEET_BACKGROUNDOBJECTS\n"
"string.text"
msgid "Background objects"
msgstr "হৌরকফমগী পোত্শকশিং"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PSEUDOSHEET_BACKGROUND\n"
"string.text"
msgid "Background"
msgstr "হৌরকফম"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_PSEUDOSHEET_NOTES\n"
"string.text"
msgid "Notes"
msgstr "খংজিনগদবশিং"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_TOOL_PANEL_SHELL\n"
"string.text"
msgid "Tool Panel"
msgstr ""

#: glob.src
msgctxt ""
"glob.src\n"
"STR_TASKPANEVIEWSHELL\n"
"string.text"
msgid "Tasks"
msgstr "থবকশিং"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_TASKPANELAYOUTMENU\n"
"string.text"
msgid "Layout"
msgstr "উত্পা"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_POWERPOINT_IMPORT\n"
"string.text"
msgid "PowerPoint Import"
msgstr "পাৱার পোইন্ত পুশিল্লকপা"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_POOLSHEET_ARROW\n"
"string.text"
msgid "Arrow"
msgstr "তেনজৈ"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_LOAD_DOC\n"
"string.text"
msgid "Load Document"
msgstr "দোকুমেন্ত লোদ তৌবা"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_SAVE_DOC\n"
"string.text"
msgid "Save Document"
msgstr "দোকুমেন্ত সেভ তৌবা"

#: glob.src
msgctxt ""
"glob.src\n"
"RID_SD_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_FORMAT_ROWCOL )\n"
"string.text"
msgid "File format error found at $(ARG1)(row,col)."
msgstr "$(ARG1)(পরিং,কলম)দা ফাইল ফোর্মেত অশোয়বা ফংলে"

#: glob.src
msgctxt ""
"glob.src\n"
"RID_SD_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_FORMAT_FILE_ROWCOL )\n"
"string.text"
msgid "Format error discovered in the file in sub-document $(ARG1) at position $(ARG2)(row,col)."
msgstr " $(ARG1) সবদোকুমেন্ত ফাইলদুদা ফোর্মেত অশোয়বা পুথোক্লে $(ARG2)(পরিং,কলম) মফমদা"

#: glob.src
msgctxt ""
"glob.src\n"
"RID_SD_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , WARN_FORMAT_FILE_ROWCOL )\n"
"string.text"
msgid "Format error discovered in the file in sub-document $(ARG1) at position $(ARG2)(row,col)."
msgstr " $(ARG1) সবদোকুমেন্ত ফাইলদুদা ফোর্মেত অশোয়বা পুথোক্লে $(ARG2)(পরিং,কলম) মফমদা"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_POOLSHEET_BANDED_CELL\n"
"string.text"
msgid "Banding cell"
msgstr ""

#: glob.src
msgctxt ""
"glob.src\n"
"STR_POOLSHEET_HEADER\n"
"string.text"
msgid "Header"
msgstr "হেদর"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_POOLSHEET_TOTAL\n"
"string.text"
msgid "Total line"
msgstr ""

#: glob.src
#, fuzzy
msgctxt ""
"glob.src\n"
"STR_POOLSHEET_FIRST_COLUMN\n"
"string.text"
msgid "First column"
msgstr "অহানবা মচু"

#: glob.src
#, fuzzy
msgctxt ""
"glob.src\n"
"STR_POOLSHEET_LAST_COLUMN\n"
"string.text"
msgid "Last column"
msgstr "~ওইথংবা কলম"

#: glob.src
msgctxt ""
"glob.src\n"
"STR_ENTER_PIN\n"
"string.text"
msgid "Enter PIN:"
msgstr ""

#: glob.src
msgctxt ""
"glob.src\n"
"STR_DEAUTHORISE_CLIENT\n"
"string.text"
msgid "Remove client authorisation"
msgstr ""

#: glob.src
msgctxt ""
"glob.src\n"
"STR_SHRINK_FONT_SIZE\n"
"string.text"
msgid "Shrink font size"
msgstr ""

#: glob.src
msgctxt ""
"glob.src\n"
"STR_GROW_FONT_SIZE\n"
"string.text"
msgid "Grow font size"
msgstr ""

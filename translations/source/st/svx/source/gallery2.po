#. extracted from svx/source/gallery2
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2015-12-11 20:04+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: st\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1449864262.000000\n"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_ACTUALIZE_PROGRESS\n"
"string.text"
msgid "Update"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_FOPENERROR\n"
"string.text"
msgid "This file cannot be opened"
msgstr "Faele ena e ka se bulwe"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_NOTHEME\n"
"string.text"
msgid "Invalid Theme Name!"
msgstr "Lebitso la Sehloho sa Taba le sa Sebetseng!"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_EXTFORMAT1_UI\n"
"string.text"
msgid "Wave - Sound File"
msgstr "Isa Hodimo le Tlase - Faele ya Modumo"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_EXTFORMAT2_UI\n"
"string.text"
msgid "Audio Interchange File Format"
msgstr "Sebopeho sa Faele se Fetohang sa Odiyo"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_EXTFORMAT3_UI\n"
"string.text"
msgid "AU - Sound File"
msgstr "AU - Faele ya Modumo"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_FILTER\n"
"string.text"
msgid "Graphics filter"
msgstr "Filthara ya ditshwantsho"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_LENGTH\n"
"string.text"
msgid "Length:"
msgstr "Botelele:"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_SIZE\n"
"string.text"
msgid "Size:"
msgstr "Boholo:"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_DELETEDD\n"
"string.text"
msgid "Do you want to delete the linked file?"
msgstr "Na o batla ho hlakola faele e kopantsweng?"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_SGIERROR\n"
"string.text"
msgid ""
"This file cannot be opened.\n"
"Do you want to enter a different search path? "
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_NEWTHEME\n"
"string.text"
msgid "New Theme"
msgstr "Sehloho se Setjha sa Taba"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_BROWSER\n"
"string.text"
msgid "~Organizer..."
msgstr "~Mohlophisi..."

#: gallery.src
#, fuzzy
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_THEMEERR\n"
"string.text"
msgid ""
"This theme name already exists.\n"
"Please choose a different one."
msgstr "Lebitso lena la laeborari le se ntse le le teng. Ka kopo kgetha lebitso le leng le fapaneng."

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_IMPORTTHEME\n"
"string.text"
msgid "I~mport..."
msgstr "A~mohela..."

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_CREATETHEME\n"
"string.text"
msgid "New Theme..."
msgstr "Sehlooho se Setjha sa Taba..."

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_DIALOGID\n"
"string.text"
msgid "Assign ID"
msgstr "Abela ID"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_TITLE\n"
"string.text"
msgid "Title"
msgstr "Sehlooho"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_PATH\n"
"string.text"
msgid "Path"
msgstr "Tselana"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_ICONVIEW\n"
"string.text"
msgid "Icon View"
msgstr "Pono ya Letshwao"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_LISTVIEW\n"
"string.text"
msgid "Detailed View"
msgstr "Pono e Nang le Dintlha"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY1\n"
"MN_ACTUALIZE\n"
"menuitem.text"
msgid "Update"
msgstr ""

#: gallery.src
#, fuzzy
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY1\n"
"MN_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr "~Hlakola"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY1\n"
"MN_RENAME\n"
"menuitem.text"
msgid "~Rename"
msgstr "~Reha hape"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY1\n"
"MN_ASSIGN_ID\n"
"menuitem.text"
msgid "Assign ~ID"
msgstr "Abela ~ID"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY1\n"
"MN_PROPERTIES\n"
"menuitem.text"
msgid "Propert~ies..."
msgstr "Dipha~hlo..."

#: gallery.src
#, fuzzy
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY2\n"
"MN_ADD\n"
"menuitem.text"
msgid "~Insert"
msgstr "~Qapa"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY2\n"
"MN_BACKGROUND\n"
"menuitem.text"
msgid "Insert as Bac~kground"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY2\n"
"MN_PREVIEW\n"
"menuitem.text"
msgid "~Preview"
msgstr "~Ponelopele"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY2\n"
"MN_TITLE\n"
"menuitem.text"
msgid "~Title"
msgstr "~Sehloho"

#: gallery.src
#, fuzzy
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY2\n"
"MN_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr "~Hlakola"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY2\n"
"MN_COPYCLIPBOARD\n"
"menuitem.text"
msgid "~Copy"
msgstr ""

#: gallery.src
#, fuzzy
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY2\n"
"MN_PASTECLIPBOARD\n"
"menuitem.text"
msgid "~Insert"
msgstr "~Qapa"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_3D\n"
"string.text"
msgid "3D Effects"
msgstr "Diphello tsa 3D"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_ANIMATIONS\n"
"string.text"
msgid "Animations"
msgstr "Diketsahatso"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_BULLETS\n"
"string.text"
msgid "Bullets"
msgstr "Dipulete"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_OFFICE\n"
"string.text"
msgid "Office"
msgstr "Ofisi"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_FLAGS\n"
"string.text"
msgid "Flags"
msgstr "Difolaga"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_FLOWCHARTS\n"
"string.text"
msgid "Flow Charts"
msgstr "Ditjhate tse Elellang"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_EMOTICONS\n"
"string.text"
msgid "Emoticons"
msgstr "Ditlhaku tsa khipoto"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_PHOTOS\n"
"string.text"
msgid "Pictures"
msgstr "Dinepe"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_BACKGROUNDS\n"
"string.text"
msgid "Backgrounds"
msgstr "Tse leng ka morao"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_HOMEPAGE\n"
"string.text"
msgid "Homepage"
msgstr "Leqephe la lapeng"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_INTERACTION\n"
"string.text"
msgid "Interaction"
msgstr "Kenelelano"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_MAPS\n"
"string.text"
msgid "Maps"
msgstr "Dimmapa"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_PEOPLE\n"
"string.text"
msgid "People"
msgstr "Batho"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_SURFACES\n"
"string.text"
msgid "Surfaces"
msgstr "Bokahodimo"

#: galtheme.src
#, fuzzy
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_COMPUTERS\n"
"string.text"
msgid "Computers"
msgstr "Dikhomphuthara"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_DIAGRAMS\n"
"string.text"
msgid "Diagrams"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_ENVIRONMENT\n"
"string.text"
msgid "Environment"
msgstr ""

#: galtheme.src
#, fuzzy
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_FINANCE\n"
"string.text"
msgid "Finance"
msgstr "Matlotlo"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_TRANSPORT\n"
"string.text"
msgid "Transport"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_TXTSHAPES\n"
"string.text"
msgid "Textshapes"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_SOUNDS\n"
"string.text"
msgid "Sounds"
msgstr "Medumo"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_SYMBOLS\n"
"string.text"
msgid "Symbols"
msgstr "Matshwao"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_MYTHEME\n"
"string.text"
msgid "My Theme"
msgstr "Sehloho saka sa Taba"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_ARROWS\n"
"string.text"
msgid "Arrows"
msgstr "Marungwana"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_BALLOONS\n"
"string.text"
msgid "Balloons"
msgstr "Dipalune"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_KEYBOARD\n"
"string.text"
msgid "Keyboard"
msgstr "Khipoto"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_TIME\n"
"string.text"
msgid "Time"
msgstr "Nako"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_PRESENTATION\n"
"string.text"
msgid "Presentation"
msgstr "Nehelano"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_CALENDAR\n"
"string.text"
msgid "Calendar"
msgstr "Khalendara"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_NAVIGATION\n"
"string.text"
msgid "Navigation"
msgstr "Taolo"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_COMMUNICATION\n"
"string.text"
msgid "Communication"
msgstr "Puisano"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_FINANCES\n"
"string.text"
msgid "Finances"
msgstr "Matlotlo"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_COMPUTER\n"
"string.text"
msgid "Computers"
msgstr "Dikhomphuthara"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_CLIMA\n"
"string.text"
msgid "Climate"
msgstr "Klelaemete"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_EDUCATION\n"
"string.text"
msgid "School & University"
msgstr "Sekolo & Yunivesithi"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_TROUBLE\n"
"string.text"
msgid "Problem Solving"
msgstr "Ho Rarolla Bothata"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_SCREENBEANS\n"
"string.text"
msgid "Screen Beans"
msgstr "Dinawa tsa Sekirini"

#. extracted from svx/source/toolbars
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.freedesktop.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-05-23 12:06+0200\n"
"PO-Revision-Date: 2011-04-06 02:17+0200\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: st\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVX_EXTRUSION_BAR\n"
"string.text"
msgid "Extrusion"
msgstr "Kgatello"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_ON_OFF\n"
"string.text"
msgid "Apply Extrusion On/Off"
msgstr "Sebedisa Kgatello Sebetsa/Timile"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_ROTATE_DOWN\n"
"string.text"
msgid "Tilt Down"
msgstr "Sekamisetsa Tlase"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_ROTATE_UP\n"
"string.text"
msgid "Tilt Up"
msgstr "Sekamisetsa Hodimo"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_ROTATE_LEFT\n"
"string.text"
msgid "Tilt Left"
msgstr "Sekamisetsa ho le Letshehadi"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_ROTATE_RIGHT\n"
"string.text"
msgid "Tilt Right"
msgstr "Sekamisetsa ho le Letona"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_DEPTH\n"
"string.text"
msgid "Change Extrusion Depth"
msgstr "Fetola Botebo ba Kgatello"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_ORIENTATION\n"
"string.text"
msgid "Change Orientation"
msgstr "Fetola Hlophiso"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_PROJECTION\n"
"string.text"
msgid "Change Projection Type"
msgstr "Fetola Mofuta wa Porojeke"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_LIGHTING\n"
"string.text"
msgid "Change Lighting"
msgstr "Fetola ho Laeta"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_BRIGHTNESS\n"
"string.text"
msgid "Change Brightness"
msgstr "Fetola ho Kganya"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_SURFACE\n"
"string.text"
msgid "Change Extrusion Surface"
msgstr "Fetola Bokahodimo ba Kgatello"

#: extrusionbar.src
msgctxt ""
"extrusionbar.src\n"
"RID_SVXSTR_UNDO_APPLY_EXTRUSION_COLOR\n"
"string.text"
msgid "Change Extrusion Color"
msgstr "Fetola Mmala wa Kgatello"

#: fontworkbar.src
msgctxt ""
"fontworkbar.src\n"
"RID_SVX_FONTWORK_BAR\n"
"string.text"
msgid "Fontwork"
msgstr "Mosebetsi wa fonto"

#: fontworkbar.src
msgctxt ""
"fontworkbar.src\n"
"RID_SVXSTR_UNDO_APPLY_FONTWORK_SHAPE\n"
"string.text"
msgid "Apply Fontwork Shape"
msgstr "Sebedisa Sebopeho sa Mosebetsi wa fonto"

#: fontworkbar.src
msgctxt ""
"fontworkbar.src\n"
"RID_SVXSTR_UNDO_APPLY_FONTWORK_SAME_LETTER_HEIGHT\n"
"string.text"
msgid "Apply Fontwork Same Letter Heights"
msgstr "Sebedisa Bophahamo ba Leletere le Tshwanang la Mosebetsi wa fonto"

#: fontworkbar.src
msgctxt ""
"fontworkbar.src\n"
"RID_SVXSTR_UNDO_APPLY_FONTWORK_ALIGNMENT\n"
"string.text"
msgid "Apply Fontwork Alignment"
msgstr "Sebedisa ho Lokisa ha Mosebetsi wa fonto"

#: fontworkbar.src
msgctxt ""
"fontworkbar.src\n"
"RID_SVXSTR_UNDO_APPLY_FONTWORK_CHARACTER_SPACING\n"
"string.text"
msgid "Apply Fontwork Character Spacing"
msgstr "Sebedisa ho Etsa Sebaka sa Tlhaku ha Mosebetsi wa fonto"

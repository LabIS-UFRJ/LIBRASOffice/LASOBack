#. extracted from svx/source/stbctrls
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2017-04-11 22:26+0200\n"
"PO-Revision-Date: 2015-05-13 12:33+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: sat\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1431520414.000000\n"

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_INSERT_HELPTEXT\n"
"string.text"
msgid "Insert mode. Click to change to overwrite mode."
msgstr ""

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_OVERWRITE_HELPTEXT\n"
"string.text"
msgid "Overwrite mode. Click to change to insert mode."
msgstr ""

#. To be shown in the status bar when in overwrite mode, please try to make it not longer than the word 'Overwrite'.
#: stbctrls.src
#, fuzzy
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_OVERWRITE_TEXT\n"
"string.text"
msgid "Overwrite"
msgstr "दोà ¹डà¤< (~O)"

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXMENU_SELECTION\n"
"SELECTION_STANDARD\n"
"menuitem.text"
msgid "Standard selection"
msgstr "Standard selection"

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXMENU_SELECTION\n"
"SELECTION_EXTENDED\n"
"menuitem.text"
msgid "Extending selection"
msgstr ""

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXMENU_SELECTION\n"
"SELECTION_ADDED\n"
"menuitem.text"
msgid "Adding selection"
msgstr ""

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXMENU_SELECTION\n"
"SELECTION_BLOCK\n"
"menuitem.text"
msgid "Block selection"
msgstr ""

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_XMLSEC_SIG_OK\n"
"string.text"
msgid "Digital Signature: The document signature is OK."
msgstr "एलेख सोही: दोलिल सुही ठिका."

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_XMLSEC_SIG_OK_NO_VERIFY\n"
"string.text"
msgid "Digital Signature: The document signature is OK, but the certificates could not be validated."
msgstr "एलेख सोही: दोलिल सुही ठिका मेनखान पोरमान बाङ बाताव दाड़ेयाक् आ."

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_XMLSEC_SIG_NOT_OK\n"
"string.text"
msgid "Digital Signature: The document signature does not match the document content. We strongly recommend you to do not trust this document."
msgstr "एलेख सोही: दोलिल सुही बाय मिलाक् दोलिल बिसोय सांव आले  लाहा आम काना ले आम नोवा दोलिल रे ओलोम पतियाव आ.                                                     एलेख सोही: दोलिल सुही बाय मिलाक् दोलिल बिसोय सांव आले  लाहा आम काना ले आम नोवा दोलिल रे ओलोम पतियाव आ.                                                     "

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_XMLSEC_NO_SIG\n"
"string.text"
msgid "Digital Signature: The document is not signed."
msgstr "एलेख सोही: दोलिल बाङ सुही आकान."

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_XMLSEC_SIG_CERT_OK_PARTIAL_SIG\n"
"string.text"
msgid "Digital Signature: The document signature and the certificate are OK, but not all parts of the document are signed."
msgstr "एलेख एल सुही: दोलिल सुही आर पोरमान साकाम दो  OK गेया, मेनखान दोलिल रेयाक् जोतो हा़टिञ दो बाय सुही या."

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_DOC_MODIFIED_YES\n"
"string.text"
msgid "The document has been modified. Double-click to save the document."
msgstr "दोलिल दो बोदोलाकाना. दोलिल सांचाव ला़गित् बार धाव ओताय मे."

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_DOC_MODIFIED_NO\n"
"string.text"
msgid "The document has not been modified since the last save."
msgstr "दोलिल दो मुचा़त् सांचाव खोन बाय बोदोल आ़गू आकाना."

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_DOC_LOAD\n"
"string.text"
msgid "Loading document..."
msgstr "दस्तावेज लोड़ कर रहा है...दोलिल लादेयेत् आ ..."

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_FIT_SLIDE\n"
"string.text"
msgid "Fit slide to current window."
msgstr ""

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_WARN_MISSING_SMARTART\n"
"string.text"
msgid "Could not load all SmartArts. Saving in Microsoft Office 2010 or later would avoid this issue."
msgstr ""

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_ZOOMTOOL_HINT\n"
"string.text"
msgid "Zoom level. Right-click to change zoom level or click to open Zoom dialog."
msgstr ""

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_ZOOM_IN\n"
"string.text"
msgid "Zoom In"
msgstr ""

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_ZOOM_OUT\n"
"string.text"
msgid "Zoom Out"
msgstr ""

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_ZOOM_25\n"
"string.text"
msgid "25%"
msgstr ""

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_ZOOM_50\n"
"string.text"
msgid "50%"
msgstr ""

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_ZOOM_75\n"
"string.text"
msgid "75%"
msgstr ""

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_ZOOM_100\n"
"string.text"
msgid "100%"
msgstr ""

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_ZOOM_150\n"
"string.text"
msgid "150%"
msgstr ""

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_ZOOM_200\n"
"string.text"
msgid "200%"
msgstr ""

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_ZOOM_WHOLE_PAGE\n"
"string.text"
msgid "Entire Page"
msgstr "जोतो साहटा"

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_ZOOM_PAGE_WIDTH\n"
"string.text"
msgid "Page Width"
msgstr "ओसार साहटा"

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXSTR_ZOOM_OPTIMAL_VIEW\n"
"string.text"
msgid "Optimal View"
msgstr ""

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SIDEBAR_EMPTY_PANEL_TEXT\n"
"string.text"
msgid "Properties for the task that you are performing are not available for the current selection"
msgstr ""

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXMNU_ZOOM\n"
"ZOOM_WHOLE_PAGE\n"
"menuitem.text"
msgid "Entire Page"
msgstr "जोतो साहटा"

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXMNU_ZOOM\n"
"ZOOM_PAGE_WIDTH\n"
"menuitem.text"
msgid "Page Width"
msgstr "ओसार साहटा"

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXMNU_ZOOM\n"
"ZOOM_OPTIMAL\n"
"menuitem.text"
msgid "Optimal View"
msgstr ""

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXMNU_ZOOM\n"
"ZOOM_50\n"
"menuitem.text"
msgid "50%"
msgstr ""

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXMNU_ZOOM\n"
"ZOOM_75\n"
"menuitem.text"
msgid "75%"
msgstr ""

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXMNU_ZOOM\n"
"ZOOM_100\n"
"menuitem.text"
msgid "100%"
msgstr ""

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXMNU_ZOOM\n"
"ZOOM_150\n"
"menuitem.text"
msgid "150%"
msgstr ""

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXMNU_ZOOM\n"
"ZOOM_200\n"
"menuitem.text"
msgid "200%"
msgstr ""

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXMNU_PSZ_FUNC\n"
"PSZ_FUNC_AVG\n"
"menuitem.text"
msgid "Average"
msgstr "ताला कोरे मेनाक्बाराबारी तेयाक्"

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXMNU_PSZ_FUNC\n"
"PSZ_FUNC_COUNT2\n"
"menuitem.text"
msgid "CountA"
msgstr "लेखा A"

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXMNU_PSZ_FUNC\n"
"PSZ_FUNC_COUNT\n"
"menuitem.text"
msgid "Count"
msgstr "एलखालेखा"

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXMNU_PSZ_FUNC\n"
"PSZ_FUNC_MAX\n"
"menuitem.text"
msgid "Maximum"
msgstr "माराङ होचोढेर उतार"

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXMNU_PSZ_FUNC\n"
"PSZ_FUNC_MIN\n"
"menuitem.text"
msgid "Minimum"
msgstr "कोम उता़रकोम उता़र "

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXMNU_PSZ_FUNC\n"
"PSZ_FUNC_SUM\n"
"menuitem.text"
msgid "Sum"
msgstr "जोड़एलेख"

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXMNU_PSZ_FUNC\n"
"PSZ_FUNC_SELECTION_COUNT\n"
"menuitem.text"
msgid "Selection count"
msgstr "Selection count"

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXMNU_PSZ_FUNC\n"
"PSZ_FUNC_NONE\n"
"menuitem.text"
msgid "None"
msgstr "ओका हों बाङकोय हों बाङ"

#: stbctrls.src
msgctxt ""
"stbctrls.src\n"
"RID_SVXMNU_XMLSECSTATBAR\n"
"XMLSEC_CALL\n"
"menuitem.text"
msgid "Digital Signatures..."
msgstr "एलेख सुही..."

#. extracted from reportdesign/source/core/resource
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.freedesktop.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-05-28 18:08+0200\n"
"PO-Revision-Date: 2013-06-18 09:20+0300\n"
"Last-Translator: Harri Pitkänen <hatapitk@iki.fi>\n"
"Language-Team: Finnish <discuss@fi.libreoffice.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_DETAIL\n"
"string.text"
msgid "Detail"
msgstr "Tiedot"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_PAGE_HEADER\n"
"string.text"
msgid "Page Header"
msgstr "Sivun ylätunniste"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_PAGE_FOOTER\n"
"string.text"
msgid "Page Footer"
msgstr "Sivun alatunniste"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_GROUP_HEADER\n"
"string.text"
msgid "Group Header"
msgstr "Ryhmän ylätunniste"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_GROUP_FOOTER\n"
"string.text"
msgid "Group Footer"
msgstr "Ryhmän alatunniste"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_REPORT_HEADER\n"
"string.text"
msgid "Report Header"
msgstr "Raportin ylätunniste"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_REPORT_FOOTER\n"
"string.text"
msgid "Report Footer"
msgstr "Raportin alatunniste"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_PROPERTY_CHANGE_NOT_ALLOWED\n"
"string.text"
msgid "The name '#1' already exists and cannot be assigned again."
msgstr "Nimi '#1' on jo olemassa, eikä sitä voi käyttää uudelleen."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_ERROR_WRONG_ARGUMENT\n"
"string.text"
msgid "You tried to set an illegal argument. Please have a look at '#1' for valid arguments."
msgstr "Yritit asettaa sopimattoman argumentin. '#1' esittää kelvolliset argumentit."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_ARGUMENT_IS_NULL\n"
"string.text"
msgid "The element is invalid."
msgstr "Osatekijä on virheellinen."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_FIXEDTEXT\n"
"string.text"
msgid "Label field"
msgstr "Selitekenttä"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_FORMATTEDFIELD\n"
"string.text"
msgid "Formatted field"
msgstr "Muotoiltu kenttä"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_IMAGECONTROL\n"
"string.text"
msgid "Image control"
msgstr "Kuvan ohjaus"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_REPORT\n"
"string.text"
msgid "Report"
msgstr "Raportti"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_SHAPE\n"
"string.text"
msgid "Shape"
msgstr "Kuvio"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_FIXEDLINE\n"
"string.text"
msgid "Fixed line"
msgstr "Muuttumaton rivi"

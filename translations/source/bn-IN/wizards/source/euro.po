#. extracted from wizards/source/euro
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-04-22 23:40+0200\n"
"PO-Revision-Date: 2015-05-12 01:00+0000\n"
"Last-Translator: system user <>\n"
"Language-Team: Bengali (India) <anubad@lists.ankur.org.in>\n"
"Language: bn_IN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 2.7\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1431392457.000000\n"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_ZERO\n"
"string.text"
msgid "~Cancel"
msgstr "‍বাতিল (~C)"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_ZERO + 1\n"
"string.text"
msgid "~Help"
msgstr "সহায়তা (~H)"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_ZERO + 2\n"
"string.text"
msgid "<<~Back"
msgstr "<< ফিরে যান (‍‍‍‍‍‍‍‍~B)"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_ZERO + 3\n"
"string.text"
msgid "~Convert"
msgstr "রূপান্তর (~C)"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_ZERO + 4\n"
"string.text"
msgid "Note: Currency amounts from external links and currency conversion factors in formulas cannot be converted."
msgstr "নোট: সূত্রের বাহ্যিক লিংক থেকে মুদ্রার পরিমাণ এবং মুদ্রা রূপান্তর সম্পর্কিত বিষয়সমূহ রূপান্তর করা যাবে না।"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_ZERO + 5\n"
"string.text"
msgid "First, unprotect all sheets."
msgstr "প্রথমে, সবগুলো শীট অরক্ষিত করে নিন।"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_ZERO + 6\n"
"string.text"
msgid "Currencies:"
msgstr "মুদ্রাসমূহ:"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_ZERO + 7\n"
"string.text"
msgid "C~ontinue>>"
msgstr "চালিয়ে যান (~o) >>"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_ZERO + 8\n"
"string.text"
msgid "C~lose"
msgstr "বন্ধ করুন (~l)"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_CONVERTER\n"
"string.text"
msgid "~Entire document"
msgstr "সমগ্র নথি (~E)"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_CONVERTER + 1\n"
"string.text"
msgid "Selection"
msgstr "নির্বাচন"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_CONVERTER + 2\n"
"string.text"
msgid "Cell S~tyles"
msgstr "ঘরের শৈলীসমূহ (‍~t)"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_CONVERTER + 3\n"
"string.text"
msgid "Currency cells in the current ~sheet"
msgstr "বর্তমান শীটের মুদ্রার ঘরসমূহ (~s)"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_CONVERTER + 4\n"
"string.text"
msgid "Currency cells in the entire ~document"
msgstr "সমগ্র নথিতে মুদ্রার ঘরসমূহ (~d)"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_CONVERTER + 5\n"
"string.text"
msgid "~Selected range"
msgstr "নির্বাচিত পরিসর (‍‍~S)"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_CONVERTER + 6\n"
"string.text"
msgid "Select Cell Styles"
msgstr "ঘরের শৈলী নির্বাচন করুন"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_CONVERTER + 7\n"
"string.text"
msgid "Select currency cells"
msgstr "মুদ্রার ঘরগুলো নির্বাচন করুন"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_CONVERTER + 8\n"
"string.text"
msgid "Currency ranges:"
msgstr "মুদ্রার পরিসর:"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_CONVERTER + 9\n"
"string.text"
msgid "Templates:"
msgstr "ফর্মাসমূহ:"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_AUTOPILOT\n"
"string.text"
msgid "Extent"
msgstr "বিস্তৃতি"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_AUTOPILOT + 1\n"
"string.text"
msgid "~Single %PRODUCTNAME Calc document"
msgstr "একক %PRODUCTNAME ক্যালক নথি (~S)"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_AUTOPILOT + 2\n"
"string.text"
msgid "Complete ~directory"
msgstr "সম্পূর্ণ ডিরেক্টরি (~d)"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_AUTOPILOT + 3\n"
"string.text"
msgid "Source Document:"
msgstr "উৎস নথি:"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_AUTOPILOT + 4\n"
"string.text"
msgid "Source directory:"
msgstr "উৎস ডিরেক্টরি:"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_AUTOPILOT + 5\n"
"string.text"
msgid "~Including subfolders"
msgstr "সাব-ফোল্ডারসমূহ সহ (~I)"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_AUTOPILOT + 6\n"
"string.text"
msgid "Target directory:"
msgstr "গন্তব্য ডিরেক্টরি:"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_AUTOPILOT + 7\n"
"string.text"
msgid "Temporarily unprotect sheet without query"
msgstr "কোয়েরি না করে অস্থায়ীভাবে শীট অরক্ষিত করুন"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_AUTOPILOT + 10\n"
"string.text"
msgid "Also convert fields and tables in text documents"
msgstr "নথির ক্ষেত্র এবং সারণিগুলো রূপান্তর করুন"

#: euro.src
msgctxt ""
"euro.src\n"
"STATUSLINE\n"
"string.text"
msgid "Conversion status: "
msgstr "রূপান্তরের অবস্থা:"

#: euro.src
msgctxt ""
"euro.src\n"
"STATUSLINE + 1\n"
"string.text"
msgid "Conversion status of the cell templates:"
msgstr "ঘরের ফর্মাগুলোর রূপান্তরের অবস্থা:"

#: euro.src
msgctxt ""
"euro.src\n"
"STATUSLINE + 2\n"
"string.text"
msgid "Registration of the relevant ranges: Sheet %1Number%1 of %2TotPageCount%2"
msgstr "প্রাসঙ্গিক পরিসর নিবন্ধন: শীট %1Number%1 , মোট %2TotPageCount%2"

#: euro.src
msgctxt ""
"euro.src\n"
"STATUSLINE + 3\n"
"string.text"
msgid "Entry of the ranges to be converted..."
msgstr "রূপান্তরের জন্য পরিসর অন্তর্ভুক্তি..."

#: euro.src
msgctxt ""
"euro.src\n"
"STATUSLINE + 4\n"
"string.text"
msgid "Sheet protection for each sheet will be restored..."
msgstr "প্রতিটি শীটের জন্য শীট-নিরাপত্তা পূনরায় কার্যকর করা হবে..."

#: euro.src
msgctxt ""
"euro.src\n"
"STATUSLINE + 5\n"
"string.text"
msgid "Conversion of the currency units in the cell templates..."
msgstr "ঘরের ফর্মাগুলোতে মুদ্রার এককের রূপান্তর..."

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES\n"
"string.text"
msgid "~Finish"
msgstr "শেষ (~F)"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 1\n"
"string.text"
msgid "Select directory"
msgstr "ডিরেক্টরি নির্বাচন করুন"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 2\n"
"string.text"
msgid "Select file"
msgstr "ফাইল নির্বাচন করুন"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 3\n"
"string.text"
msgid "Select target directory"
msgstr "গন্তব্য ডিরেক্টরি নির্বাচন করুন"

#: euro.src
#, fuzzy
msgctxt ""
"euro.src\n"
"MESSAGES + 4\n"
"string.text"
msgid "non-existent"
msgstr "বিদ্যমান নয়"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 5\n"
"string.text"
msgid "Euro Converter"
msgstr "ইউরো রূপান্তরকারী"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 6\n"
"string.text"
msgid "Should protected spreadsheets be temporarily unprotected?"
msgstr "সুরক্ষিত শীটগুলো কি অস্থায়ীভাবে অরক্ষিত করা হবে?"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 7\n"
"string.text"
msgid "Enter the password to unprotect the table %1TableName%1"
msgstr "%1TableName%1 টেবিলটি অরক্ষিত করার জন্য পাসওয়ার্ড দিন"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 8\n"
"string.text"
msgid "Wrong Password!"
msgstr "ভুল পাসওয়ার্ড!"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 9\n"
"string.text"
msgid "Protected Sheet"
msgstr "সুরক্ষিত শীট"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 10\n"
"string.text"
msgid "Warning!"
msgstr "সতর্কতা!"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 11\n"
"string.text"
msgid "Protection for the sheets will not be removed."
msgstr "সবগুলো শীট অরক্ষিত হবে না।"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 12\n"
"string.text"
msgid "Sheet cannot be unprotected"
msgstr "শীট অরক্ষিত করা যাবে না"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 13\n"
"string.text"
msgid "The Wizard cannot edit this document as cell formats cannot be modified in documents containing protected spreadsheets."
msgstr "উইজার্ড এই নথি সম্পাদনা করতে পারবেনা কারণ যেসব নথিতে সুরক্ষিত স্প্রেডশীট আছে তাতে ঘরের ফরম্যাট পরিবর্তন করা যাচ্ছে না।"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 14\n"
"string.text"
msgid "Please note that the Euro Converter will, otherwise, not be able to edit this document!"
msgstr "মনে রাখবেন, ইউরো রূপান্তরক পারবে, অন্যথায় এই নথি সম্পাদনা করতে পারবেনা!"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 15\n"
"string.text"
msgid "Please choose a currency to be converted first!"
msgstr "অনুগ্রহ করে প্রথমে রূপান্তরের জন্য একটি মুদ্রা নির্বাচন করুন!"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 16\n"
"string.text"
msgid "Password:"
msgstr "পাসওয়ার্ড:"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 17\n"
"string.text"
msgid "OK"
msgstr "ঠিক আছে"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 18\n"
"string.text"
msgid "Cancel"
msgstr "বাতিল"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 19\n"
"string.text"
msgid "Please select a %PRODUCTNAME Calc document for editing!"
msgstr "অনুগ্রহ করে সম্পাদনার জন্য একটি %PRODUCTNAME ক্যালক নথি নির্বাচন করুন!"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 20\n"
"string.text"
msgid "'<1>' is not a directory!"
msgstr "'<1>' কোন ডিরেক্টরি নয়!"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 21\n"
"string.text"
msgid "Document is read-only!"
msgstr "নথিটি শুধুমাত্র পড়ার জন্য!"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 22\n"
"string.text"
msgid "The '<1>' file already exists.<CR>Do you want to overwrite it?"
msgstr "'<1>' ফাইলটি ইতোমধ্যে আছে।<CR>আপনি কি এটির উপরিলিখন করতে চান?"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 23\n"
"string.text"
msgid "Do you really want to terminate conversion at this point?"
msgstr "আপনি কি সত্যিই এই পর্যায়ে রূপান্তর বন্ধ করতে চান?"

#: euro.src
msgctxt ""
"euro.src\n"
"MESSAGES + 24\n"
"string.text"
msgid "Cancel Wizard"
msgstr "উইজার্ড বাতিল করুন"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES\n"
"string.text"
msgid "Portuguese Escudo"
msgstr "পর্তুগিজ এস্কুডো"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 1\n"
"string.text"
msgid "Dutch Guilder"
msgstr "ডাচ গিল্ডার"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 2\n"
"string.text"
msgid "French Franc"
msgstr "ফরাসি ফ্র্যাঙ্ক"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 3\n"
"string.text"
msgid "Spanish Peseta"
msgstr "স্পেনীয় পেসেতা"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 4\n"
"string.text"
msgid "Italian Lira"
msgstr "ইতালীয় লিরা"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 5\n"
"string.text"
msgid "German Mark"
msgstr "জার্মান মার্ক"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 6\n"
"string.text"
msgid "Belgian Franc"
msgstr "বেলজিয়ান ফ্রাংক"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 7\n"
"string.text"
msgid "Irish Punt"
msgstr "আইরিশ পান্ট"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 8\n"
"string.text"
msgid "Luxembourg Franc"
msgstr "লুক্সেমবার্গ ফ্রাংক"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 9\n"
"string.text"
msgid "Austrian Schilling"
msgstr "অস্ট্রিয়ান শিলিং"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 10\n"
"string.text"
msgid "Finnish Mark"
msgstr "ফিনিশ মার্ক"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 11\n"
"string.text"
msgid "Greek Drachma"
msgstr "গ্রীক ড্রাচমা"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 12\n"
"string.text"
msgid "Slovenian Tolar"
msgstr "স্লোভেনিয়ান টলার"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 13\n"
"string.text"
msgid "Cypriot Pound"
msgstr "সাইপ্রাস পাউন্ড"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 14\n"
"string.text"
msgid "Maltese Lira"
msgstr "মাল্টা লিরা"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 15\n"
"string.text"
msgid "Slovak Koruna"
msgstr "স্লোভাক করুনা"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 16\n"
"string.text"
msgid "Estonian Kroon"
msgstr "এসটোনিয়ান ক্রণ"

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 17\n"
"string.text"
msgid "Latvian Lats"
msgstr ""

#: euro.src
msgctxt ""
"euro.src\n"
"CURRENCIES + 18\n"
"string.text"
msgid "Lithuanian Litas"
msgstr ""

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_LASTPAGE\n"
"string.text"
msgid "Progress"
msgstr "অগ্রগতি"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_LASTPAGE + 1\n"
"string.text"
msgid "Retrieving the relevant documents..."
msgstr "প্রাসঙ্গিক নথিগুলো আহরণ করা হচ্ছে..."

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_LASTPAGE + 2\n"
"string.text"
msgid "Converting the documents..."
msgstr "নথিগুলো রূপান্তর করা হচ্ছে..."

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_LASTPAGE + 3\n"
"string.text"
msgid "Settings:"
msgstr "সেটিংসমূহ:"

#: euro.src
msgctxt ""
"euro.src\n"
"STEP_LASTPAGE + 4\n"
"string.text"
msgid "Sheet is always unprotected"
msgstr "শীটটি সবসময় অরক্ষিত থাকে"

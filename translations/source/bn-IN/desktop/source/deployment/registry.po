#. extracted from desktop/source/deployment/registry
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-11-20 13:01+0100\n"
"PO-Revision-Date: 2012-09-07 15:05+0000\n"
"Last-Translator: runab <runa.misc@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: bn_IN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1347030304.0\n"

#: dp_registry.src
msgctxt ""
"dp_registry.src\n"
"RID_STR_REGISTERING_PACKAGE\n"
"string.text"
msgid "Enabling: "
msgstr "সক্রিয় করা হচ্ছে:"

#: dp_registry.src
msgctxt ""
"dp_registry.src\n"
"RID_STR_REVOKING_PACKAGE\n"
"string.text"
msgid "Disabling: "
msgstr "নিষ্ক্রিয় করা হচ্ছে:"

#: dp_registry.src
msgctxt ""
"dp_registry.src\n"
"RID_STR_CANNOT_DETECT_MEDIA_TYPE\n"
"string.text"
msgid "Cannot detect media-type: "
msgstr "মিডিয়া-টাইপ সনাক্ত করা যাচ্ছেনা:"

#: dp_registry.src
msgctxt ""
"dp_registry.src\n"
"RID_STR_UNSUPPORTED_MEDIA_TYPE\n"
"string.text"
msgid "This media-type is not supported: "
msgstr "এই মিডিয়া-টাইপটি সমর্থিত নয়:"

#: dp_registry.src
msgctxt ""
"dp_registry.src\n"
"RID_STR_ERROR_WHILE_REGISTERING\n"
"string.text"
msgid "An error occurred while enabling: "
msgstr "সক্রিয় করার সময় একটি ত্রুটি দেখা দিয়েছে:"

#: dp_registry.src
msgctxt ""
"dp_registry.src\n"
"RID_STR_ERROR_WHILE_REVOKING\n"
"string.text"
msgid "An error occurred while disabling: "
msgstr "নিষ্ক্রিয় করার সময় একটি ত্রুটি দেখা দিয়েছে:"

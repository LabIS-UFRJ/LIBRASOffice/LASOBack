#. extracted from sc/source/ui/dbgui
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-12-01 12:11+0100\n"
"PO-Revision-Date: 2017-01-02 05:04+0000\n"
"Last-Translator: Cheng-Chia Tseng <pswo10680@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1483333491.000000\n"

#: pvfundlg.src
msgctxt ""
"pvfundlg.src\n"
"SCSTR_DPFUNCLISTBOX\n"
"Sum\n"
"itemlist.text"
msgid "Sum"
msgstr "總和"

#: pvfundlg.src
msgctxt ""
"pvfundlg.src\n"
"SCSTR_DPFUNCLISTBOX\n"
"Count\n"
"itemlist.text"
msgid "Count"
msgstr "計數"

#: pvfundlg.src
msgctxt ""
"pvfundlg.src\n"
"SCSTR_DPFUNCLISTBOX\n"
"Average\n"
"itemlist.text"
msgid "Average"
msgstr "平均"

#: pvfundlg.src
msgctxt ""
"pvfundlg.src\n"
"SCSTR_DPFUNCLISTBOX\n"
"Median\n"
"itemlist.text"
msgid "Median"
msgstr "中位數"

#: pvfundlg.src
msgctxt ""
"pvfundlg.src\n"
"SCSTR_DPFUNCLISTBOX\n"
"Max\n"
"itemlist.text"
msgid "Max"
msgstr "最大"

#: pvfundlg.src
msgctxt ""
"pvfundlg.src\n"
"SCSTR_DPFUNCLISTBOX\n"
"Min\n"
"itemlist.text"
msgid "Min"
msgstr "最小"

#: pvfundlg.src
msgctxt ""
"pvfundlg.src\n"
"SCSTR_DPFUNCLISTBOX\n"
"Product\n"
"itemlist.text"
msgid "Product"
msgstr "乘積"

#: pvfundlg.src
msgctxt ""
"pvfundlg.src\n"
"SCSTR_DPFUNCLISTBOX\n"
"Count (Numbers only)\n"
"itemlist.text"
msgid "Count (Numbers only)"
msgstr "計數 (僅計數字)"

#: pvfundlg.src
msgctxt ""
"pvfundlg.src\n"
"SCSTR_DPFUNCLISTBOX\n"
"StDev (Sample)\n"
"itemlist.text"
msgid "StDev (Sample)"
msgstr "標準差 (樣本)"

#: pvfundlg.src
msgctxt ""
"pvfundlg.src\n"
"SCSTR_DPFUNCLISTBOX\n"
"StDevP (Population)\n"
"itemlist.text"
msgid "StDevP (Population)"
msgstr "母標準差 (母群體)"

#: pvfundlg.src
msgctxt ""
"pvfundlg.src\n"
"SCSTR_DPFUNCLISTBOX\n"
"Var (Sample)\n"
"itemlist.text"
msgid "Var (Sample)"
msgstr "變異數 (樣本)"

#: pvfundlg.src
msgctxt ""
"pvfundlg.src\n"
"SCSTR_DPFUNCLISTBOX\n"
"VarP (Population)\n"
"itemlist.text"
msgid "VarP (Population)"
msgstr "母變異數 (母群體)"

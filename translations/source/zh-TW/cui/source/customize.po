#. extracted from cui/source/customize
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2017-10-04 13:08+0000\n"
"Last-Translator: Cheng-Chia Tseng <pswo10680@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1507122534.000000\n"

#: acccfg.src
msgctxt ""
"acccfg.src\n"
"RID_SVXPAGE_CONFIGGROUPBOX\n"
"STR_BASICMACROS\n"
"string.text"
msgid "BASIC Macros"
msgstr "BASIC 巨集"

#: acccfg.src
msgctxt ""
"acccfg.src\n"
"RID_SVXPAGE_CONFIGGROUPBOX\n"
"STR_GROUP_STYLES\n"
"string.text"
msgid "Styles"
msgstr "樣式"

#: cfg.src
msgctxt ""
"cfg.src\n"
"TEXT_RENAME\n"
"#define.text"
msgid "Rename..."
msgstr "重新命名..."

#: cfg.src
msgctxt ""
"cfg.src\n"
"TEXT_DELETE_NODOTS\n"
"#define.text"
msgid "Delete"
msgstr "刪除"

#: cfg.src
msgctxt ""
"cfg.src\n"
"MODIFY_TOOLBAR_CONTENT\n"
"ID_DEFAULT_COMMAND\n"
"menuitem.text"
msgid "Restore Default Command"
msgstr "還原預設指令"

#: cfg.src
msgctxt ""
"cfg.src\n"
"MODIFY_TOOLBAR_CONTENT\n"
"ID_CHANGE_SYMBOL\n"
"menuitem.text"
msgid "Change Icon..."
msgstr "變更圖示..."

#: cfg.src
msgctxt ""
"cfg.src\n"
"MODIFY_TOOLBAR_CONTENT\n"
"ID_RESET_SYMBOL\n"
"menuitem.text"
msgid "Reset Icon"
msgstr "重設圖示"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_NEW_MENU\n"
"string.text"
msgid "New Menu %n"
msgstr "新選單 %n"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_NEW_TOOLBAR\n"
"string.text"
msgid "New Toolbar %n"
msgstr "新工具列 %n"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_MOVE_MENU\n"
"string.text"
msgid "Move Menu"
msgstr "移動選單"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_ADD_SUBMENU\n"
"string.text"
msgid "Add Submenu"
msgstr "加入子選單"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_SUBMENU_NAME\n"
"string.text"
msgid "Submenu name"
msgstr "子選單名稱"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_MENU_ADDCOMMANDS_DESCRIPTION\n"
"string.text"
msgid "To add a command to a menu, select the category and then the command. You can also drag the command to the Commands list of the Menus tab page in the Customize dialog."
msgstr "若要將指令加入選單，請先選取種類再選取指令。也可以將指令拖曳至 [自訂] 對話方塊中 [選單] 標籤頁的 [指令] 清單。"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_IMPORT_ICON_ERROR\n"
"string.text"
msgid "The files listed below could not be imported. The file format could not be interpreted."
msgstr "無法匯入以下所列的檔案。無法解譯檔案格式。"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_DELETE_ICON_CONFIRM\n"
"string.text"
msgid "Are you sure to delete the image?"
msgstr "確定要刪除影像？"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_REPLACE_ICON_WARNING\n"
"string.text"
msgid ""
"The icon %ICONNAME is already contained in the image list.\n"
"Would you like to replace the existing icon?"
msgstr ""
"影像清單中已包含 %ICONNAME 圖示。\n"
"是否要取代現有的圖示？"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_REPLACE_ICON_CONFIRM\n"
"string.text"
msgid "Confirm Icon Replacement"
msgstr "確認取代圖示"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_YESTOALL\n"
"string.text"
msgid "Yes to All"
msgstr "全部皆是"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_PRODUCTNAME_TOOLBARS\n"
"string.text"
msgid "%PRODUCTNAME %MODULENAME Toolbars"
msgstr "%PRODUCTNAME %MODULENAME 工具列"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_PRODUCTNAME_CONTEXTMENUS\n"
"string.text"
msgid "%PRODUCTNAME %MODULENAME Context Menus"
msgstr "%PRODUCTNAME %MODULENAME 右鍵功能表"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_TOOLBAR\n"
"string.text"
msgid "Toolbar"
msgstr "工具列"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_TOOLBAR_CONTENT\n"
"string.text"
msgid "Toolbar Content"
msgstr "工具列內容"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_COMMANDS\n"
"string.text"
msgid "Commands"
msgstr "指令"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_COMMAND\n"
"string.text"
msgid "Command"
msgstr "指令"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_TOOLBAR_NAME\n"
"string.text"
msgid "Toolbar Name"
msgstr "工具列名稱"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SXVSTR_CONFIRM_DELETE_TOOLBAR\n"
"string.text"
msgid "There are no more commands on the toolbar. Do you want to delete the toolbar?"
msgstr "工具列上沒有其他指令。您是否想要刪除該工具列？"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_CONFIRM_MENU_RESET\n"
"string.text"
msgid "The menu configuration for %SAVE IN SELECTION% will be reset to the default settings. Do you want to continue?"
msgstr "%SAVE IN SELECTION% 的選單組態將重設回預設設定。您是否想繼續？"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_CONFIRM_TOOLBAR_RESET\n"
"string.text"
msgid "The toolbar configuration for %SAVE IN SELECTION% will be reset to the default settings. Do you want to continue?"
msgstr "%SAVE IN SELECTION% 的工具列組態將重設回預設設定。您是否想繼續？"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_CONFIRM_RESTORE_DEFAULT\n"
"string.text"
msgid "This will delete all changes previously made to this toolbar. Do you really want to reset the toolbar?"
msgstr "這會刪除所有先前對此工具列所作的變更。您是否真想重設該工具列？"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_MNUCFG_ALREADY_INCLUDED\n"
"string.text"
msgid "Function is already included in this popup."
msgstr "功能已包含在這個彈出式選單中。"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_LABEL_NEW_NAME\n"
"string.text"
msgid "~New name"
msgstr "新名稱(~N)"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_RENAME_MENU\n"
"string.text"
msgid "Rename Menu"
msgstr "重新命名選單"

#: cfg.src
msgctxt ""
"cfg.src\n"
"RID_SVXSTR_RENAME_TOOLBAR\n"
"string.text"
msgid "Rename Toolbar"
msgstr "重新命名工具列"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_STARTAPP\n"
"string.text"
msgid "Start Application"
msgstr "啟動應用程式"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_CLOSEAPP\n"
"string.text"
msgid "Close Application"
msgstr "關閉應用程式"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_NEWDOC\n"
"string.text"
msgid "New Document"
msgstr "新文件"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_CLOSEDOC\n"
"string.text"
msgid "Document closed"
msgstr "文件已關閉"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_PREPARECLOSEDOC\n"
"string.text"
msgid "Document is going to be closed"
msgstr "文件即將關閉"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_OPENDOC\n"
"string.text"
msgid "Open Document"
msgstr "開啟文件"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_SAVEDOC\n"
"string.text"
msgid "Save Document"
msgstr "儲存文件"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_SAVEASDOC\n"
"string.text"
msgid "Save Document As"
msgstr "另存文件"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_SAVEDOCDONE\n"
"string.text"
msgid "Document has been saved"
msgstr "文件已儲存"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_SAVEASDOCDONE\n"
"string.text"
msgid "Document has been saved as"
msgstr "文件已另存"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_ACTIVATEDOC\n"
"string.text"
msgid "Activate Document"
msgstr "使用文件"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_DEACTIVATEDOC\n"
"string.text"
msgid "Deactivate Document"
msgstr "停用文件"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_PRINTDOC\n"
"string.text"
msgid "Print Document"
msgstr "列印文件"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_MODIFYCHANGED\n"
"string.text"
msgid "'Modified' status was changed"
msgstr "已變更「已修改」狀態"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_MAILMERGE\n"
"string.text"
msgid "Printing of form letters started"
msgstr "列印套印信件已啟動"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_MAILMERGE_END\n"
"string.text"
msgid "Printing of form letters finished"
msgstr "列印套印信件已完成"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_FIELDMERGE\n"
"string.text"
msgid "Merging of form fields started"
msgstr "合併表單欄位已啟動"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_FIELDMERGE_FINISHED\n"
"string.text"
msgid "Merging of form fields finished"
msgstr "合併表單欄位已完成"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_PAGECOUNTCHANGE\n"
"string.text"
msgid "Changing the page count"
msgstr "變更頁數"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_SUBCOMPONENT_OPENED\n"
"string.text"
msgid "Loaded a sub component"
msgstr "已載入子元件"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_SUBCOMPONENT_CLOSED\n"
"string.text"
msgid "Closed a sub component"
msgstr "已關閉子元件"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_APPROVEPARAMETER\n"
"string.text"
msgid "Fill parameters"
msgstr "填入參數"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_ACTIONPERFORMED\n"
"string.text"
msgid "Execute action"
msgstr "執行動作"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_AFTERUPDATE\n"
"string.text"
msgid "After updating"
msgstr "更新後"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_BEFOREUPDATE\n"
"string.text"
msgid "Before updating"
msgstr "更新前"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_APPROVEROWCHANGE\n"
"string.text"
msgid "Before record action"
msgstr "記錄動作前"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_ROWCHANGE\n"
"string.text"
msgid "After record action"
msgstr "記錄動作後"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_CONFIRMDELETE\n"
"string.text"
msgid "Confirm deletion"
msgstr "確認刪除"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_ERROROCCURRED\n"
"string.text"
msgid "Error occurred"
msgstr "發生錯誤"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_ADJUSTMENTVALUECHANGED\n"
"string.text"
msgid "While adjusting"
msgstr "調整時"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_FOCUSGAINED\n"
"string.text"
msgid "When receiving focus"
msgstr "接收焦點時"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_FOCUSLOST\n"
"string.text"
msgid "When losing focus"
msgstr "失去焦點時"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_ITEMSTATECHANGED\n"
"string.text"
msgid "Item status changed"
msgstr "項目狀態已變更"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_KEYTYPED\n"
"string.text"
msgid "Key pressed"
msgstr "已按下按鍵"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_KEYUP\n"
"string.text"
msgid "Key released"
msgstr "已放開按鍵"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_LOADED\n"
"string.text"
msgid "When loading"
msgstr "載入時"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_RELOADING\n"
"string.text"
msgid "Before reloading"
msgstr "重新載入前"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_RELOADED\n"
"string.text"
msgid "When reloading"
msgstr "重新載入時"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_MOUSEDRAGGED\n"
"string.text"
msgid "Mouse moved while key pressed"
msgstr "按下按鍵時已移動滑鼠"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_MOUSEENTERED\n"
"string.text"
msgid "Mouse inside"
msgstr "滑鼠移入"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_MOUSEEXITED\n"
"string.text"
msgid "Mouse outside"
msgstr "滑鼠移出"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_MOUSEMOVED\n"
"string.text"
msgid "Mouse moved"
msgstr "滑鼠移動"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_MOUSEPRESSED\n"
"string.text"
msgid "Mouse button pressed"
msgstr "已按下滑鼠按鈕"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_MOUSERELEASED\n"
"string.text"
msgid "Mouse button released"
msgstr "已放開滑鼠按鈕"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_POSITIONING\n"
"string.text"
msgid "Before record change"
msgstr "記錄指標變更前"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_POSITIONED\n"
"string.text"
msgid "After record change"
msgstr "記錄指標變更後"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_RESETTED\n"
"string.text"
msgid "After resetting"
msgstr "重新設定後"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_APPROVERESETTED\n"
"string.text"
msgid "Prior to reset"
msgstr "重設前"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_APPROVEACTIONPERFORMED\n"
"string.text"
msgid "Approve action"
msgstr "核准動作"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_SUBMITTED\n"
"string.text"
msgid "Before submitting"
msgstr "提交前"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_TEXTCHANGED\n"
"string.text"
msgid "Text modified"
msgstr "文字已修改"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_UNLOADING\n"
"string.text"
msgid "Before unloading"
msgstr "卸載前"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_UNLOADED\n"
"string.text"
msgid "When unloading"
msgstr "卸載時"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_CHANGED\n"
"string.text"
msgid "Changed"
msgstr "已變更"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_CREATEDOC\n"
"string.text"
msgid "Document created"
msgstr "文件已建立"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_LOADDOCFINISHED\n"
"string.text"
msgid "Document loading finished"
msgstr "文件載入已完成"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_SAVEDOCFAILED\n"
"string.text"
msgid "Saving of document failed"
msgstr "儲存文件失敗"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_SAVEASDOCFAILED\n"
"string.text"
msgid "'Save as' has failed"
msgstr "「另存新檔」失敗"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_COPYTODOC\n"
"string.text"
msgid "Storing or exporting copy of document"
msgstr "排序或匯出文件複本"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_COPYTODOCDONE\n"
"string.text"
msgid "Document copy has been created"
msgstr "已建立文件複本"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_COPYTODOCFAILED\n"
"string.text"
msgid "Creating of document copy failed"
msgstr "建立文件複本失敗"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_VIEWCREATED\n"
"string.text"
msgid "View created"
msgstr "檢視已建立"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_PREPARECLOSEVIEW\n"
"string.text"
msgid "View is going to be closed"
msgstr "檢視即將關閉"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_CLOSEVIEW\n"
"string.text"
msgid "View closed"
msgstr "檢視已關閉"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_TITLECHANGED\n"
"string.text"
msgid "Document title changed"
msgstr "文件標題已變更"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_MODECHANGED\n"
"string.text"
msgid "Document mode changed"
msgstr "文件模式已變更"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_VISAREACHANGED\n"
"string.text"
msgid "Visible area changed"
msgstr "可見區塊已變更"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_STORAGECHANGED\n"
"string.text"
msgid "Document has got a new storage"
msgstr "文件已有新的儲存位置"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_LAYOUT_FINISHED\n"
"string.text"
msgid "Document layout finished"
msgstr "文件版面配置已完成"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_SELECTIONCHANGED\n"
"string.text"
msgid "Selection changed"
msgstr "選取的內容已變更"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_DOUBLECLICK\n"
"string.text"
msgid "Double click"
msgstr "連按兩下"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_RIGHTCLICK\n"
"string.text"
msgid "Right click"
msgstr "按一下滑鼠右鍵"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_CALCULATE\n"
"string.text"
msgid "Formulas calculated"
msgstr "公式已計算"

#: macropg.src
msgctxt ""
"macropg.src\n"
"RID_SVXSTR_EVENT_CONTENTCHANGED\n"
"string.text"
msgid "Content changed"
msgstr "內容已變更"

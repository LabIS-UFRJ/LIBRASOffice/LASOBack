#. extracted from sd/source/ui/accessibility
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-04-22 23:40+0200\n"
"PO-Revision-Date: 2016-09-10 02:01+0000\n"
"Last-Translator: Cheng-Chia Tseng <pswo10680@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1473472909.000000\n"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_D_DRAWVIEW_N\n"
"string.text"
msgid "Drawing View"
msgstr "繪圖檢視"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_D_DRAWVIEW_D\n"
"string.text"
msgid "This is where you create and edit drawings."
msgstr "在此您能夠建立和編輯繪圖內容。"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_I_DRAWVIEW_N\n"
"string.text"
msgid "Drawing View"
msgstr "繪圖檢視"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_I_DRAWVIEW_D\n"
"string.text"
msgid "This is where you create and edit slides."
msgstr "在此您能夠建立和編輯投影片。"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_I_OUTLINEVIEW_N\n"
"string.text"
msgid "Outline View"
msgstr "大綱檢視"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_I_OUTLINEVIEW_D\n"
"string.text"
msgid "This is where you enter or edit text in list form."
msgstr "在此您能夠以列表形式輸入和編輯文字。"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_I_SLIDEVIEW_N\n"
"string.text"
msgid "Slides View"
msgstr "投影片檢視"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_I_SLIDEVIEW_D\n"
"string.text"
msgid "This is where you sort slides."
msgstr "在此您能夠排序投影片。"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_I_NOTESVIEW_N\n"
"string.text"
msgid "Notes View"
msgstr "備註檢視"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_I_NOTESVIEW_D\n"
"string.text"
msgid "This is where you enter and view notes."
msgstr "在此您能夠輸入和檢視備註。"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_I_HANDOUTVIEW_N\n"
"string.text"
msgid "Handout View"
msgstr "講義檢視"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_I_HANDOUTVIEW_D\n"
"string.text"
msgid "This is where you decide on the layout for handouts."
msgstr "在此您能夠編輯講義的版面配置。"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_TITLE_N\n"
"string.text"
msgid "PresentationTitle"
msgstr "簡報題名"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_OUTLINER_N\n"
"string.text"
msgid "PresentationOutliner"
msgstr "簡報大鋼管裡器"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_SUBTITLE_N\n"
"string.text"
msgid "PresentationSubtitle"
msgstr "簡報副題"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_PAGE_N\n"
"string.text"
msgid "PresentationPage"
msgstr "簡報頁面"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_NOTES_N\n"
"string.text"
msgid "PresentationNotes"
msgstr "簡報備註"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_HANDOUT_N\n"
"string.text"
msgid "Handout"
msgstr "講義"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_UNKNOWN_N\n"
"string.text"
msgid "UnknownAccessiblePresentationShape"
msgstr "未知的可存取簡報外型"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_TITLE_D\n"
"string.text"
msgid "PresentationTitleShape"
msgstr "簡報題名外型"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_OUTLINER_D\n"
"string.text"
msgid "PresentationOutlinerShape"
msgstr "簡報大綱管理器外型"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_SUBTITLE_D\n"
"string.text"
msgid "PresentationSubtitleShape"
msgstr "簡報副題外型"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_PAGE_D\n"
"string.text"
msgid "PresentationPageShape"
msgstr "簡報頁面外型"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_NOTES_D\n"
"string.text"
msgid "PresentationNotesShape"
msgstr "簡報備註外型"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_HANDOUT_D\n"
"string.text"
msgid "PresentationHandoutShape"
msgstr "簡報講義外型"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_UNKNOWN_D\n"
"string.text"
msgid "Unknown accessible presentation shape"
msgstr "未知的可存取簡報外型"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_FOOTER_N\n"
"string.text"
msgid "PresentationFooter"
msgstr "簡報頁尾"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_FOOTER_D\n"
"string.text"
msgid "PresentationFooterShape"
msgstr "簡報頁尾外型"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_HEADER_N\n"
"string.text"
msgid "PresentationHeader"
msgstr "簡報頁首"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_HEADER_D\n"
"string.text"
msgid "PresentationHeaderShape"
msgstr "簡報頁首外型"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_DATE_N\n"
"string.text"
msgid "PresentationDateAndTime"
msgstr "簡報日期和時間"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_DATE_D\n"
"string.text"
msgid "PresentationDateAndTimeShape"
msgstr "簡報日期和時間外型"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_NUMBER_N\n"
"string.text"
msgid "PresentationPageNumber"
msgstr "簡報頁碼"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_NUMBER_D\n"
"string.text"
msgid "PresentationPageNumberShape"
msgstr "簡報頁碼外形"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_D_PRESENTATION\n"
"string.text"
msgid "%PRODUCTNAME Presentation"
msgstr "%PRODUCTNAME 簡報"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_TITLE_N_STYLE\n"
"string.text"
msgid "Title"
msgstr "題名"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_OUTLINER_N_STYLE\n"
"string.text"
msgid "Outliner"
msgstr "大綱管理器"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_SUBTITLE_N_STYLE\n"
"string.text"
msgid "Subtitle"
msgstr "副題"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_PAGE_N_STYLE\n"
"string.text"
msgid "Page"
msgstr "頁面"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_NOTES_N_STYLE\n"
"string.text"
msgid "Notes"
msgstr "備註"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_HANDOUT_N_STYLE\n"
"string.text"
msgid "Handout"
msgstr "講義"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_UNKNOWN_N_STYLE\n"
"string.text"
msgid "Unknown Accessible Presentation Shape"
msgstr "未知的可存取簡報外型"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_FOOTER_N_STYLE\n"
"string.text"
msgid "Footer"
msgstr "頁尾"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_HEADER_N_STYLE\n"
"string.text"
msgid "Header"
msgstr "頁首"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_DATE_N_STYLE\n"
"string.text"
msgid "Date"
msgstr "日期"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_P_NUMBER_N_STYLE\n"
"string.text"
msgid "Number"
msgstr "數字"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_I_PREVIEW_N\n"
"string.text"
msgid "Preview View"
msgstr "預覽檢視"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_I_PREVIEW_D\n"
"string.text"
msgid "This is where you print preview pages."
msgstr "這裡是您列印預覽頁面之處。"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_I_PREVIEW_SUFFIX\n"
"string.text"
msgid "(Preview mode)"
msgstr "(預覽模式)"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"SID_SD_A11Y_D_PRESENTATION_READONLY\n"
"string.text"
msgid "(read-only)"
msgstr "(唯讀)"

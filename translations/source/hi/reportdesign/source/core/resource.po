#. extracted from reportdesign/source/core/resource
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.freedesktop.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-05-28 18:08+0200\n"
"PO-Revision-Date: 2011-04-05 20:37+0200\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_DETAIL\n"
"string.text"
msgid "Detail"
msgstr "विवरण"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_PAGE_HEADER\n"
"string.text"
msgid "Page Header"
msgstr "पृष्ठ शीर्षिका"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_PAGE_FOOTER\n"
"string.text"
msgid "Page Footer"
msgstr "पृष्ठ पादिका"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_GROUP_HEADER\n"
"string.text"
msgid "Group Header"
msgstr "समूह शीर्षिका"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_GROUP_FOOTER\n"
"string.text"
msgid "Group Footer"
msgstr "समूह पादिका"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_REPORT_HEADER\n"
"string.text"
msgid "Report Header"
msgstr "रिपोर्ट शीर्षिका"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_REPORT_FOOTER\n"
"string.text"
msgid "Report Footer"
msgstr "रिपोर्ट पादिका"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_PROPERTY_CHANGE_NOT_ALLOWED\n"
"string.text"
msgid "The name '#1' already exists and cannot be assigned again."
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_ERROR_WRONG_ARGUMENT\n"
"string.text"
msgid "You tried to set an illegal argument. Please have a look at '#1' for valid arguments."
msgstr "आपने अवैध तर्क सेट करने की कोशिश किया. कृपया '#1' पर वैध तर्क पर एक दृष्टि दें."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_ARGUMENT_IS_NULL\n"
"string.text"
msgid "The element is invalid."
msgstr "तत्व अवैध है."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_FIXEDTEXT\n"
"string.text"
msgid "Label field"
msgstr "लेबल क्षेत्र"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_FORMATTEDFIELD\n"
"string.text"
msgid "Formatted field"
msgstr "प्रारूपित क्षेत्र"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_IMAGECONTROL\n"
"string.text"
msgid "Image control"
msgstr "चित्र नियंत्रण"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_REPORT\n"
"string.text"
msgid "Report"
msgstr "रिपोर्ट"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_SHAPE\n"
"string.text"
msgid "Shape"
msgstr "आकार"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_FIXEDLINE\n"
"string.text"
msgid "Fixed line"
msgstr "स्थिर पंक्ति"

#. extracted from desktop/source/deployment/registry/script
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-04-22 23:40+0200\n"
"PO-Revision-Date: 2017-01-19 19:31+0000\n"
"Last-Translator: Khaled <khaledhosny@eglug.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1484854275.000000\n"

#: dp_script.src
msgctxt ""
"dp_script.src\n"
"RID_STR_BASIC_LIB\n"
"string.text"
msgid "%PRODUCTNAME Basic Library"
msgstr "مكتبة %PRODUCTNAME بيسك"

#: dp_script.src
msgctxt ""
"dp_script.src\n"
"RID_STR_DIALOG_LIB\n"
"string.text"
msgid "Dialog Library"
msgstr "مكتبة مربعات الحوار"

#: dp_script.src
msgctxt ""
"dp_script.src\n"
"RID_STR_CANNOT_DETERMINE_LIBNAME\n"
"string.text"
msgid "The library name could not be determined."
msgstr "تعذر تحديد اسم المكتبة."

#: dp_script.src
msgctxt ""
"dp_script.src\n"
"RID_STR_LIBNAME_ALREADY_EXISTS\n"
"string.text"
msgid "This library name already exists. Please choose a different name."
msgstr "اسم المكتبة مستخدم بالفعل. الرجاء اختيار اسم مختلف."

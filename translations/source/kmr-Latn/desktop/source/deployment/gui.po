#. extracted from desktop/source/deployment/gui
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-12-01 12:11+0100\n"
"PO-Revision-Date: 2016-07-05 00:24+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: kmr-Latn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1467678255.000000\n"

#: dp_gui_dialog.src
msgctxt ""
"dp_gui_dialog.src\n"
"RID_STR_ADD_PACKAGES\n"
"string.text"
msgid "Add Extension(s)"
msgstr "Pêvekê(an) lê zêde bike"

#: dp_gui_dialog.src
msgctxt ""
"dp_gui_dialog.src\n"
"RID_CTX_ITEM_REMOVE\n"
"string.text"
msgid "~Remove"
msgstr "~Rakirin"

#: dp_gui_dialog.src
msgctxt ""
"dp_gui_dialog.src\n"
"RID_CTX_ITEM_ENABLE\n"
"string.text"
msgid "~Enable"
msgstr "Çala~kkirin"

#: dp_gui_dialog.src
msgctxt ""
"dp_gui_dialog.src\n"
"RID_CTX_ITEM_DISABLE\n"
"string.text"
msgid "~Disable"
msgstr "~Neçalakkirin"

#: dp_gui_dialog.src
msgctxt ""
"dp_gui_dialog.src\n"
"RID_CTX_ITEM_CHECK_UPDATE\n"
"string.text"
msgid "~Update..."
msgstr "~Rojanekirin..."

#: dp_gui_dialog.src
msgctxt ""
"dp_gui_dialog.src\n"
"RID_STR_ADDING_PACKAGES\n"
"string.text"
msgid "Adding %EXTENSION_NAME"
msgstr "%EXTENSION_NAME tê lêzêdekirin"

#: dp_gui_dialog.src
msgctxt ""
"dp_gui_dialog.src\n"
"RID_STR_REMOVING_PACKAGES\n"
"string.text"
msgid "Removing %EXTENSION_NAME"
msgstr "%EXTENSION_NAME tê rakirin"

#: dp_gui_dialog.src
msgctxt ""
"dp_gui_dialog.src\n"
"RID_STR_ENABLING_PACKAGES\n"
"string.text"
msgid "Enabling %EXTENSION_NAME"
msgstr "%EXTENSION_NAME tê çalakkirin"

#: dp_gui_dialog.src
msgctxt ""
"dp_gui_dialog.src\n"
"RID_STR_DISABLING_PACKAGES\n"
"string.text"
msgid "Disabling %EXTENSION_NAME"
msgstr "%EXTENSION_NAME tê neçalakkirin"

#: dp_gui_dialog.src
msgctxt ""
"dp_gui_dialog.src\n"
"RID_STR_ACCEPT_LICENSE\n"
"string.text"
msgid "Accept license for %EXTENSION_NAME"
msgstr "Ji bo %EXTENSION_NAME lîsansê bipejirîne"

#: dp_gui_dialog.src
msgctxt ""
"dp_gui_dialog.src\n"
"RID_STR_ERROR_UNKNOWN_STATUS\n"
"string.text"
msgid "Error: The status of this extension is unknown"
msgstr "Çewtî: Rewşa vê pêvekê nayê zanîn"

#: dp_gui_dialog.src
msgctxt ""
"dp_gui_dialog.src\n"
"RID_STR_CLOSE_BTN\n"
"string.text"
msgid "Close"
msgstr "Bigire"

#: dp_gui_dialog.src
msgctxt ""
"dp_gui_dialog.src\n"
"RID_STR_EXIT_BTN\n"
"string.text"
msgid "Quit"
msgstr "Derkeve"

#: dp_gui_dialog.src
msgctxt ""
"dp_gui_dialog.src\n"
"RID_STR_NO_ADMIN_PRIVILEGE\n"
"string.text"
msgid ""
"%PRODUCTNAME has been updated to a new version. Some shared %PRODUCTNAME extensions are not compatible with this version and need to be updated before %PRODUCTNAME can be started.\n"
"\n"
"Updating of shared extension requires administrator privileges. Contact your system administrator to update the following shared extensions:"
msgstr ""
"%PRODUCTNAME ji bo guhertoyekî nû hate rojanekirin. Hinek pêvekên parkirî %PRODUCTNAME bi vê guhertoyê re lihev nayên û beriya %PRODUCTNAME were destpêkirin divê bên rojanekirin.\n"
"\n"
"Rojanekirina pêvekên parkirî pêşîniyên rêveberiyê divê. Ji bo rojanekirina pêvekên parkirî yên jêr, bi rêvebirê xwe re bikevin têkiliyê:"

#: dp_gui_dialog.src
msgctxt ""
"dp_gui_dialog.src\n"
"RID_STR_ERROR_MISSING_DEPENDENCIES\n"
"string.text"
msgid "The extension cannot be enabled as the following system dependencies are not fulfilled:"
msgstr "Pêvek nayê çalakkirin ji ber kubindestên pergalê yên jêr kêm in:"

#: dp_gui_dialog.src
msgctxt ""
"dp_gui_dialog.src\n"
"RID_STR_ERROR_MISSING_LICENSE\n"
"string.text"
msgid "This extension is disabled because you haven't accepted the license yet.\n"
msgstr "Ev pêvek ji ber ku we hê lîsans nepejirandiye betalkirî ye.\n"

#: dp_gui_dialog.src
#, fuzzy
msgctxt ""
"dp_gui_dialog.src\n"
"RID_STR_SHOW_LICENSE_CMD\n"
"string.text"
msgid "Show license"
msgstr "Slaytê ~Nîşan bide"

#: dp_gui_dialog.src
msgctxt ""
"dp_gui_dialog.src\n"
"RID_STR_WARNING_INSTALL_EXTENSION\n"
"string.text"
msgid ""
"You are about to install the extension '%NAME'.\n"
"Click 'OK' to proceed with the installation.\n"
"Click 'Cancel' to stop the installation."
msgstr ""

#: dp_gui_dialog.src
msgctxt ""
"dp_gui_dialog.src\n"
"RID_STR_WARNING_REMOVE_EXTENSION\n"
"string.text"
msgid ""
"You are about to remove the extension '%NAME'.\n"
"Click 'OK' to remove the extension.\n"
"Click 'Cancel' to stop removing the extension."
msgstr ""

#: dp_gui_dialog.src
msgctxt ""
"dp_gui_dialog.src\n"
"RID_STR_WARNING_REMOVE_SHARED_EXTENSION\n"
"string.text"
msgid ""
"Make sure that no further users are working with the same %PRODUCTNAME, when changing shared extensions in a multi user environment.\n"
"Click 'OK' to remove the extension.\n"
"Click 'Cancel' to stop removing the extension."
msgstr ""

#: dp_gui_dialog.src
msgctxt ""
"dp_gui_dialog.src\n"
"RID_STR_WARNING_ENABLE_SHARED_EXTENSION\n"
"string.text"
msgid ""
"Make sure that no further users are working with the same %PRODUCTNAME, when changing shared extensions in a multi user environment.\n"
"Click 'OK' to enable the extension.\n"
"Click 'Cancel' to stop enabling the extension."
msgstr ""

#: dp_gui_dialog.src
msgctxt ""
"dp_gui_dialog.src\n"
"RID_STR_WARNING_DISABLE_SHARED_EXTENSION\n"
"string.text"
msgid ""
"Make sure that no further users are working with the same %PRODUCTNAME, when changing shared extensions in a multi user environment.\n"
"Click 'OK' to disable the extension.\n"
"Click 'Cancel' to stop disabling the extension."
msgstr ""

#: dp_gui_dialog.src
#, fuzzy
msgctxt ""
"dp_gui_dialog.src\n"
"RID_STR_UNSUPPORTED_PLATFORM\n"
"string.text"
msgid "The extension '%Name' does not work on this computer."
msgstr "Pêveka \\'%Name\\' di vê komputerê de naxebite."

#: dp_gui_updatedialog.src
msgctxt ""
"dp_gui_updatedialog.src\n"
"RID_DLG_UPDATE_NONE\n"
"string.text"
msgid "No new updates are available."
msgstr "Rojanekirinên nû tune."

#: dp_gui_updatedialog.src
msgctxt ""
"dp_gui_updatedialog.src\n"
"RID_DLG_UPDATE_NOINSTALLABLE\n"
"string.text"
msgid "No installable updates are available. To see ignored or disabled updates, mark the check box 'Show all updates'."
msgstr ""

#: dp_gui_updatedialog.src
msgctxt ""
"dp_gui_updatedialog.src\n"
"RID_DLG_UPDATE_FAILURE\n"
"string.text"
msgid "An error occurred:"
msgstr "Çewtiyek derket holê:"

#: dp_gui_updatedialog.src
msgctxt ""
"dp_gui_updatedialog.src\n"
"RID_DLG_UPDATE_UNKNOWNERROR\n"
"string.text"
msgid "Unknown error."
msgstr "Çewtiya nenas."

#: dp_gui_updatedialog.src
msgctxt ""
"dp_gui_updatedialog.src\n"
"RID_DLG_UPDATE_NODESCRIPTION\n"
"string.text"
msgid "No more details are available for this update."
msgstr "Ji bo vê pêvekê rave tune."

#: dp_gui_updatedialog.src
msgctxt ""
"dp_gui_updatedialog.src\n"
"RID_DLG_UPDATE_NOINSTALL\n"
"string.text"
msgid "The extension cannot be updated because:"
msgstr "Ev pêvek nayê rojanekirin ji ber ku:"

#: dp_gui_updatedialog.src
msgctxt ""
"dp_gui_updatedialog.src\n"
"RID_DLG_UPDATE_NODEPENDENCY\n"
"string.text"
msgid "Required %PRODUCTNAME version doesn't match:"
msgstr "Bi guhertoya OpenOffice.org ya hewce re li hev nayên:"

#: dp_gui_updatedialog.src
msgctxt ""
"dp_gui_updatedialog.src\n"
"RID_DLG_UPDATE_NODEPENDENCY_CUR_VER\n"
"string.text"
msgid "You have %PRODUCTNAME %VERSION"
msgstr "%PRODUCTNAME %PRODUCTVERSION"

#: dp_gui_updatedialog.src
msgctxt ""
"dp_gui_updatedialog.src\n"
"RID_DLG_UPDATE_BROWSERBASED\n"
"string.text"
msgid "browser based update"
msgstr "rojanekirina bi gerok re"

#: dp_gui_updatedialog.src
msgctxt ""
"dp_gui_updatedialog.src\n"
"RID_DLG_UPDATE_VERSION\n"
"string.text"
msgid "Version"
msgstr "Guherto"

#: dp_gui_updatedialog.src
msgctxt ""
"dp_gui_updatedialog.src\n"
"RID_DLG_UPDATE_IGNORE\n"
"string.text"
msgid "Ignore this Update"
msgstr "Ji bo Rojanekirinê amade ye"

#: dp_gui_updatedialog.src
msgctxt ""
"dp_gui_updatedialog.src\n"
"RID_DLG_UPDATE_IGNORE_ALL\n"
"string.text"
msgid "Ignore all Updates"
msgstr "Hemû rojanekirinan ~nîşan bide"

#: dp_gui_updatedialog.src
msgctxt ""
"dp_gui_updatedialog.src\n"
"RID_DLG_UPDATE_ENABLE\n"
"string.text"
msgid "Enable Updates"
msgstr "Rojanekirina ji Înternetê"

#: dp_gui_updatedialog.src
msgctxt ""
"dp_gui_updatedialog.src\n"
"RID_DLG_UPDATE_IGNORED_UPDATE\n"
"string.text"
msgid "This update will be ignored.\n"
msgstr "Ev taybetmendî wê temamî were rakirin."

#: dp_gui_updateinstalldialog.src
msgctxt ""
"dp_gui_updateinstalldialog.src\n"
"RID_DLG_UPDATE_INSTALL_INSTALLING\n"
"string.text"
msgid "Installing extensions..."
msgstr "Pêvek tên sazkirin..."

#: dp_gui_updateinstalldialog.src
msgctxt ""
"dp_gui_updateinstalldialog.src\n"
"RID_DLG_UPDATE_INSTALL_FINISHED\n"
"string.text"
msgid "Installation finished"
msgstr "Sazkirin bidawî bû"

#: dp_gui_updateinstalldialog.src
msgctxt ""
"dp_gui_updateinstalldialog.src\n"
"RID_DLG_UPDATE_INSTALL_NO_ERRORS\n"
"string.text"
msgid "No errors."
msgstr "Çewtî tunebû."

#: dp_gui_updateinstalldialog.src
msgctxt ""
"dp_gui_updateinstalldialog.src\n"
"RID_DLG_UPDATE_INSTALL_ERROR_DOWNLOAD\n"
"string.text"
msgid "Error while downloading extension %NAME. "
msgstr "Dema daxistina pêveka %NAME de çewtiyek derket. "

#: dp_gui_updateinstalldialog.src
msgctxt ""
"dp_gui_updateinstalldialog.src\n"
"RID_DLG_UPDATE_INSTALL_THIS_ERROR_OCCURRED\n"
"string.text"
msgid "The error message is: "
msgstr "Peyama çewtiyê eve: "

#: dp_gui_updateinstalldialog.src
msgctxt ""
"dp_gui_updateinstalldialog.src\n"
"RID_DLG_UPDATE_INSTALL_ERROR_INSTALLATION\n"
"string.text"
msgid "Error while installing extension %NAME. "
msgstr "Dema sazkirina pêveka %NAME de çewtiyek derket. "

#: dp_gui_updateinstalldialog.src
msgctxt ""
"dp_gui_updateinstalldialog.src\n"
"RID_DLG_UPDATE_INSTALL_ERROR_LIC_DECLINED\n"
"string.text"
msgid "The license agreement for extension %NAME was refused. "
msgstr "Peymana lîsansê ya pêveka %NAME hat red kirin. "

#: dp_gui_updateinstalldialog.src
msgctxt ""
"dp_gui_updateinstalldialog.src\n"
"RID_DLG_UPDATE_INSTALL_EXTENSION_NOINSTALL\n"
"string.text"
msgid "The extension will not be installed."
msgstr "Pêvek wê nayê sazkirin."

#: dp_gui_versionboxes.src
msgctxt ""
"dp_gui_versionboxes.src\n"
"RID_STR_WARNING_VERSION_LESS\n"
"string.text"
msgid ""
"You are about to install version $NEW of the extension '$NAME'.\n"
"The newer version $DEPLOYED is already installed.\n"
"Click 'OK' to replace the installed extension.\n"
"Click 'Cancel' to stop the installation."
msgstr ""

#: dp_gui_versionboxes.src
#, fuzzy
msgctxt ""
"dp_gui_versionboxes.src\n"
"RID_STR_WARNINGBOX_VERSION_LESS_DIFFERENT_NAMES\n"
"string.text"
msgid ""
"You are about to install version $NEW of the extension '$NAME'.\n"
"The newer version $DEPLOYED, named '$OLDNAME', is already installed.\n"
"Click 'OK' to replace the installed extension.\n"
"Click 'Cancel' to stop the installation."
msgstr ""
"Tu dê niha guhertoya $NEW ya pêveka \\'$NAME\\' saz bikî.\n"
"Guhertoya nûtir $DEPLOYED, bi navê \\'$OLDNAME\\', jixwe saz kirî ye.\n"
"Ji bo guherandina pêveka sazkirî pêl \\'TEMAM\\' bike.\n"
"Ji bo rawestandina sazkirinê pêl \\'BETAL\\' bike."

#: dp_gui_versionboxes.src
msgctxt ""
"dp_gui_versionboxes.src\n"
"RID_STR_WARNING_VERSION_EQUAL\n"
"string.text"
msgid ""
"You are about to install version $NEW of the extension '$NAME'.\n"
"That version is already installed.\n"
"Click 'OK' to replace the installed extension.\n"
"Click 'Cancel' to stop the installation."
msgstr ""

#: dp_gui_versionboxes.src
#, fuzzy
msgctxt ""
"dp_gui_versionboxes.src\n"
"RID_STR_WARNINGBOX_VERSION_EQUAL_DIFFERENT_NAMES\n"
"string.text"
msgid ""
"You are about to install version $NEW of the extension '$NAME'.\n"
"That version, named '$OLDNAME', is already installed.\n"
"Click 'OK' to replace the installed extension.\n"
"Click 'Cancel' to stop the installation."
msgstr ""
"Tu dê niha guhertoya $NEW ya pêveka \\'$NAME\\' saz bikî.\n"
"Ev guherto, bi navê \\'$OLDNAME\\', jixwe saz kirî ye.\n"
"Ji bo guherandina pêveka sazkirî pêl \\'TEMAM\\' bike.\n"
"Ji bo rawestandina sazkirinê pêl \\'BETAL\\' bike."

#: dp_gui_versionboxes.src
msgctxt ""
"dp_gui_versionboxes.src\n"
"RID_STR_WARNING_VERSION_GREATER\n"
"string.text"
msgid ""
"You are about to install version $NEW of the extension '$NAME'.\n"
"The older version $DEPLOYED is already installed.\n"
"Click 'OK' to replace the installed extension.\n"
"Click 'Cancel' to stop the installation."
msgstr ""

#: dp_gui_versionboxes.src
#, fuzzy
msgctxt ""
"dp_gui_versionboxes.src\n"
"RID_STR_WARNINGBOX_VERSION_GREATER_DIFFERENT_NAMES\n"
"string.text"
msgid ""
"You are about to install version $NEW of the extension '$NAME'.\n"
"The older version $DEPLOYED, named '$OLDNAME', is already installed.\n"
"Click 'OK' to replace the installed extension.\n"
"Click 'Cancel' to stop the installation."
msgstr ""
"Tu dê niha guhertoya $NEW ya pêveka \\'$NAME\\' saz bikî.\n"
"Guhertoya kevintir $DEPLOYED, bi navê \\'$OLDNAME\\', jixwe saz kirî ye.\n"
"Ji bo guherandina pêveka sazkirî pêl \\'TEMAM\\' bike.\n"
"Ji bo rawestandina sazkirinê pêl \\'BETAL\\' bike."

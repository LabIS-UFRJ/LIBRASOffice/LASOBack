#. extracted from dbaccess/source/ext/macromigration
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-04-22 23:40+0200\n"
"PO-Revision-Date: 2016-09-28 07:27+0000\n"
"Last-Translator: psingh <pirthi.d@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: pa_IN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.7\n"
"X-POOTLE-MTIME: 1475047637.000000\n"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_TITLE_MACRO_MIGRATION\n"
"string.text"
msgid "Database Document Macro Migration"
msgstr "ਡਾਟਾਬੇਸ ਡੌਕੂਮੈਂਟ ਮਾਈਕਰੋ ਮਾਈਗਰੇਸ਼ਨ"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_STATE_CLOSE_SUB_DOCS\n"
"string.text"
msgid "Prepare"
msgstr "ਤਿਆਰੀ"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_STATE_BACKUP_DBDOC\n"
"string.text"
msgid "Backup Document"
msgstr "ਡੌਕੂਮੈਂਟ ਬੈਕਅੱਪ"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_STATE_MIGRATE\n"
"string.text"
msgid "Migrate"
msgstr "ਮਾਈਗਰੇਟ"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_STATE_SUMMARY\n"
"string.text"
msgid "Summary"
msgstr "ਸੰਖੇਪ"

#. This refers to a form document inside a database document.
#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_FORM\n"
"string.text"
msgid "Form '$name$'"
msgstr "ਫਾਰਮ '$name$'"

#. This refers to a report document inside a database document.
#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_REPORT\n"
"string.text"
msgid "Report '$name$'"
msgstr "ਰਿਪੋਰਟ '$name$'"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_OVERALL_PROGRESS\n"
"string.text"
msgid "document $current$ of $overall$"
msgstr "$current$ ਡੌਕੂਮੈਂਟ $overall$ ਵਿੱਚੋਂ"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_DATABASE_DOCUMENT\n"
"string.text"
msgid "Database Document"
msgstr "ਡਾਟਾਬੇਸ ਡੌਕੂਮੈਂਟ"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_SAVED_COPY_TO\n"
"string.text"
msgid "saved copy to $location$"
msgstr "ਕਾਪੀ $location$ ਉੱਤੇ ਸੰਭਾਲੀ ਗਈ"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_MOVED_LIBRARY\n"
"string.text"
msgid "migrated $type$ library '$old$' to '$new$'"
msgstr "$type$ ਲਾਇਬਰੇਰੀ '$old$' ਤੋਂ '$new$' ਉੱਤੇ ਮਾਈਗਰੇਟ ਕੀਤੀ ਗਈ"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_LIBRARY_TYPE_AND_NAME\n"
"string.text"
msgid "$type$ library '$library$'"
msgstr "$type$ library '$library$'"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_MIGRATING_LIBS\n"
"string.text"
msgid "migrating libraries ..."
msgstr "ਲਾਈਬਰੇਰੀ ਮਾਈਗਰੇਟ ਕੀਤੀ ਜਾ ਰਹੀ ਹੈ..."

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_OOO_BASIC\n"
"string.text"
msgid "%PRODUCTNAME Basic"
msgstr "%PRODUCTNAME ਬੇਸਿਕ"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_JAVA_SCRIPT\n"
"string.text"
msgid "JavaScript"
msgstr "ਜਾਵਾਸਕ੍ਰਿਪਟ"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_BEAN_SHELL\n"
"string.text"
msgid "BeanShell"
msgstr "BeanShell"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_JAVA\n"
"string.text"
msgid "Java"
msgstr "ਜਾਵਾ"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_PYTHON\n"
"string.text"
msgid "Python"
msgstr "ਪਾਈਥਨ"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_DIALOG\n"
"string.text"
msgid "dialog"
msgstr "ਡਾਈਲਾਗ"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_ERRORS\n"
"string.text"
msgid "Error(s)"
msgstr "ਗਲਤੀ"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_WARNINGS\n"
"string.text"
msgid "Warnings"
msgstr "ਚੇਤਾਵਨੀ"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_EXCEPTION\n"
"string.text"
msgid "caught exception:"
msgstr "ਅਪਵਾਦ ਮਿਲਿਆ:"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_INVALID_BACKUP_LOCATION\n"
"string.text"
msgid "You need to choose a backup location other than the document location itself."
msgstr "ਤੁਹਾਨੂੰ ਬੈਕਅੱਪ ਟਿਕਾਣਾ ਡੌਕੂਮੈਂਟ ਟਿਕਾਣੇ ਤੋਂ ਵੱਖਰਾ ਚੁਣਨ ਦੀ ਲੋੜ ਹੈ। "

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_INVALID_NUMBER_ARGS\n"
"string.text"
msgid "Invalid number of initialization arguments. Expected 1."
msgstr "ਸ਼ੁਰੂ ਕਰਨ ਲਈ ਆਰਗੂਮੈਂਟ ਲਈ ਗਲਤ ਨੰਬਰ ਹਨ। ਚਾਹੀਦੇ 1"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_NO_DATABASE\n"
"string.text"
msgid "No database document found in the initialization arguments."
msgstr "ਕੋਈ ਡਾਟਾਬੇਸ ਡੌਕੂਮੈਂਟ ਸ਼ੁਰੂਆਤੀ ਆਰਗੂਮੈਂਟਾਂ ਵਿੱਚ ਨਹੀਂ ਲੱਭਿਆ।"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_NOT_READONLY\n"
"string.text"
msgid "Not applicable to read-only documents."
msgstr "ਕੇਵਲ ਪੜ੍ਹਨਯੋਗ ਡੌਕੂਮੈਂਟ ਉੱਤੇ ਲਾਗੂ ਨਹੀਂ ਹੁੰਦਾ"

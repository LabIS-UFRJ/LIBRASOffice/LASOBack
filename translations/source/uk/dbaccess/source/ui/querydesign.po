#. extracted from dbaccess/source/ui/querydesign
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2017-04-03 20:43+0000\n"
"Last-Translator: Андрій Бандура <andriykopanytsia@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1491252187.000000\n"

#: query.src
msgctxt ""
"query.src\n"
"RID_MENU_JOINVIEW_CONNECTION\n"
"SID_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr "Ви~далити"

#: query.src
msgctxt ""
"query.src\n"
"RID_MENU_JOINVIEW_CONNECTION\n"
"ID_QUERY_EDIT_JOINCONNECTION\n"
"menuitem.text"
msgid "Edit..."
msgstr "Змінити..."

#: query.src
msgctxt ""
"query.src\n"
"RID_MENU_JOINVIEW_TABLE\n"
"SID_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr "~Видалити"

#: query.src
msgctxt ""
"query.src\n"
"RID_QUERYCOLPOPUPMENU\n"
"ID_BROWSER_COLWIDTH\n"
"menuitem.text"
msgid "Column ~Width..."
msgstr "Ширина ~стовпця..."

#: query.src
msgctxt ""
"query.src\n"
"RID_QUERYCOLPOPUPMENU\n"
"SID_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr "~Видалити"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_TABWINSHOW\n"
"string.text"
msgid "Add Table Window"
msgstr "Додати вікно таблиці"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_MOVETABWIN\n"
"string.text"
msgid "Move table window"
msgstr "Перемістити вікно таблиці"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_INSERTCONNECTION\n"
"string.text"
msgid "Insert Join"
msgstr "Вставити зв'язок"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_REMOVECONNECTION\n"
"string.text"
msgid "Delete Join"
msgstr "Видалити зв'язок"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_SIZETABWIN\n"
"string.text"
msgid "Resize table window"
msgstr "Змінити розмір вікна таблиці"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_TABFIELDDELETE\n"
"string.text"
msgid "Delete Column"
msgstr "Видалити стовпчик"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_TABFIELDMOVED\n"
"string.text"
msgid "Move column"
msgstr "Перемістити стовпчик"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_TABFIELDCREATE\n"
"string.text"
msgid "Add Column"
msgstr "Додати стовпчик"

#: query.src
msgctxt ""
"query.src\n"
"RID_STR_TABLE_DOESNT_EXIST\n"
"string.text"
msgid "Invalid expression, table '$name$' does not exist."
msgstr "Неправильний вираз, таблиця «$name$» не існує."

#: query.src
msgctxt ""
"query.src\n"
"RID_STR_FIELD_DOESNT_EXIST\n"
"string.text"
msgid "Invalid expression, field name '$name$' does not exist."
msgstr "Неправильний вираз. Поле '$name$' не існує."

#: query.src
msgctxt ""
"query.src\n"
"RID_STR_TOMUCHTABLES\n"
"string.text"
msgid "The query covers #num# tables. The selected database type, however, can only process a maximum of #maxnum# table(s) per statement."
msgstr "Запит охоплює #num# таблиць. Вибрана база даних може обробляти не більше #maxnum# таблиць за команду."

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_TABWINDELETE\n"
"string.text"
msgid "Delete Table Window"
msgstr "Видалити вікно таблиці"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_MODIFY_CELL\n"
"string.text"
msgid "Edit Column Description"
msgstr "Змінити опис стовпчика"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_SIZE_COLUMN\n"
"string.text"
msgid "Adjust column width"
msgstr "Змінити ширину стовпчика"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_SORTTEXT\n"
"string.text"
msgid "(not sorted);ascending;descending"
msgstr "(без сортування);за зростанням;за спаданням"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_FUNCTIONS\n"
"string.text"
msgid "(no function);Group"
msgstr "(без функції);Групувати"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_NOTABLE\n"
"string.text"
msgid "(no table)"
msgstr "(немає таблиці)"

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_ORDERBY_UNRELATED\n"
"string.text"
msgid "The database only supports sorting for visible fields."
msgstr "База даних підтримує сортування лише для видимих полів."

#: query.src
msgctxt ""
"query.src\n"
"RID_QUERYFUNCTION_POPUPMENU\n"
"ID_QUERY_FUNCTION\n"
"menuitem.text"
msgid "Functions"
msgstr "Функції"

#: query.src
msgctxt ""
"query.src\n"
"RID_QUERYFUNCTION_POPUPMENU\n"
"ID_QUERY_TABLENAME\n"
"menuitem.text"
msgid "Table Name"
msgstr "Назва таблиці"

#: query.src
msgctxt ""
"query.src\n"
"RID_QUERYFUNCTION_POPUPMENU\n"
"ID_QUERY_ALIASNAME\n"
"menuitem.text"
msgid "Alias"
msgstr "Псевдонім"

#: query.src
msgctxt ""
"query.src\n"
"RID_QUERYFUNCTION_POPUPMENU\n"
"ID_QUERY_DISTINCT\n"
"menuitem.text"
msgid "Distinct Values"
msgstr "Різні значення"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_HANDLETEXT\n"
"string.text"
msgid "Field;Alias;Table;Sort;Visible;Function;Criterion;Or;Or"
msgstr "Поле;Псевдонім;Таблиця;Сортувати;Видиме;Функція;Критерій;Або;Або"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_LIMIT_ALL\n"
"string.text"
msgid "All"
msgstr "Усі"

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_TOO_MANY_COLUMNS\n"
"string.text"
msgid "There are too many columns."
msgstr "Надто багато колонок."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_CRITERIA_ON_ASTERISK\n"
"string.text"
msgid "A condition cannot be applied to field [*]"
msgstr "Неможливо застосувати дані умови до поля [*]"

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_TOO_LONG_STATEMENT\n"
"string.text"
msgid "The SQL statement created is too long."
msgstr "Створений вираз SQL є занадто довгим."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_TOOCOMPLEX\n"
"string.text"
msgid "Query is too complex"
msgstr "Запит є занадто складним"

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_NOSELECT\n"
"string.text"
msgid "Nothing has been selected."
msgstr "Нічого не виділено."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_TOOMANYCOND\n"
"string.text"
msgid "Too many search criteria"
msgstr "Надто багато критеріїв пошуку"

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_SYNTAX\n"
"string.text"
msgid "SQL syntax error"
msgstr "Помилка у синтаксисі SQL"

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_ORDERBY_ON_ASTERISK\n"
"string.text"
msgid "[*] cannot be used as a sort criterion."
msgstr "Неможливо використовувати [*] у якості критерію."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_TOO_MANY_TABLES\n"
"string.text"
msgid "There are too many tables."
msgstr "Надто багато таблиць."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_NATIVE\n"
"string.text"
msgid "The statement will not be applied when querying in the SQL dialect of the database."
msgstr "При запиті у діалекті бази даних SQL інструкцію не буде застосовано."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_ILLEGAL_JOIN\n"
"string.text"
msgid "Join could not be processed"
msgstr "Не вдається здійснити зв'язок"

#: query.src
msgctxt ""
"query.src\n"
"STR_SVT_SQL_SYNTAX_ERROR\n"
"string.text"
msgid "Syntax error in SQL statement"
msgstr "Синтаксична помилка у виразі SQL"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERYDESIGN_NO_VIEW_SUPPORT\n"
"string.text"
msgid "This database does not support table views."
msgstr "Ця база даних не підтримує представлень у вигляді таблиць."

#: query.src
msgctxt ""
"query.src\n"
"STR_NO_ALTER_VIEW_SUPPORT\n"
"string.text"
msgid "This database does not support altering of existing table views."
msgstr "Ця база даних не підтримує зміну структури наявних представлень таблиць."

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERYDESIGN_NO_VIEW_ASK\n"
"string.text"
msgid "Do you want to create a query instead?"
msgstr "Натомість створити запит?"

#: query.src
msgctxt ""
"query.src\n"
"STR_DATASOURCE_DELETED\n"
"string.text"
msgid "The corresponding data source has been deleted. Therefore, data relevant to that data source cannot be saved."
msgstr "Відповідне джерело даних було видалено. Неможливо зберегти дані, що відносяться до цього джерела."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_COLUMN_NOT_FOUND\n"
"string.text"
msgid "The column '$name$' is unknown."
msgstr "Стовпчик '$name$' не знайдено."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_JOIN_COLUMN_COMPARE\n"
"string.text"
msgid "Columns can only be compared using '='."
msgstr "Колонки можуть бути порівняні тільки за допомогою '='."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_LIKE_LEFT_NO_COLUMN\n"
"string.text"
msgid "You must use a column name before 'LIKE'."
msgstr "Слід використовувати назву стовпця перед 'LIKE'."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_CHECK_CASESENSITIVE\n"
"string.text"
msgid "The column could not be found. Please note that the database is case-sensitive."
msgstr "Неможливо знайти стовпчик. Можливо ця база даних розрізняє назви з великих та малих літер."

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERYDESIGN\n"
"string.text"
msgid " - %PRODUCTNAME Base: Query Design"
msgstr " - Конструктор запитів %PRODUCTNAME Base"

#: query.src
msgctxt ""
"query.src\n"
"STR_VIEWDESIGN\n"
"string.text"
msgid " - %PRODUCTNAME Base: View Design"
msgstr " - View Design %PRODUCTNAME Base"

#. For $object$, one of the values of the RSC_QUERY_OBJECT_TYPE resource will be inserted.
#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_SAVEMODIFIED\n"
"string.text"
msgid ""
"$object$ has been changed.\n"
"Do you want to save the changes?"
msgstr ""
"Об'єкт $object$ був змінений.\n"
"Зберегти зміни?"

#. For $object$, one of the values of the RSC_QUERY_OBJECT_TYPE resource (except "SQL command", which doesn't make sense here) will be inserted.
#: query.src
msgctxt ""
"query.src\n"
"STR_ERROR_PARSING_STATEMENT\n"
"string.text"
msgid "$object$ is based on an SQL command which could not be parsed."
msgstr "Об'єкт $object$ містить інструкції SQL, котрі неможливо розпізнати."

#. For $object$, one of the values of the RSC_QUERY_OBJECT_TYPE resource (except "SQL command", which doesn't make sense here) will be inserted.
#: query.src
msgctxt ""
"query.src\n"
"STR_INFO_OPENING_IN_SQL_VIEW\n"
"string.text"
msgid "$object$ will be opened in SQL view."
msgstr "Об'єкт $object$ буде відкритий у режимі SQL."

#: query.src
msgctxt ""
"query.src\n"
"RSC_QUERY_OBJECT_TYPE\n"
"The table view\n"
"itemlist.text"
msgid "The table view"
msgstr "Табличний вид"

#: query.src
msgctxt ""
"query.src\n"
"RSC_QUERY_OBJECT_TYPE\n"
"The query\n"
"itemlist.text"
msgid "The query"
msgstr "Запит"

#: query.src
msgctxt ""
"query.src\n"
"RSC_QUERY_OBJECT_TYPE\n"
"The SQL statement\n"
"itemlist.text"
msgid "The SQL statement"
msgstr "Вираз SQL"

#: query.src
msgctxt ""
"query.src\n"
"STR_STATEMENT_WITHOUT_RESULT_SET\n"
"string.text"
msgid "The query does not create a result set, and thus cannot be part of another query."
msgstr "Запит не створює множину результату, тому не зможе використовуватись як частина іншого запиту."

#: query.src
msgctxt ""
"query.src\n"
"STR_NO_DATASOURCE_OR_CONNECTION\n"
"string.text"
msgid "Both the ActiveConnection and the DataSourceName parameter are missing or wrong - cannot initialize the query designer."
msgstr "Параметри ActiveConnection та DataSourceName не вказані або некоректні, неможливо ініціалізувати дизайнер запитів."

#: querydlg.src
msgctxt ""
"querydlg.src\n"
"STR_JOIN_TYPE_HINT\n"
"string.text"
msgid "Please note that some databases may not support this join type."
msgstr "Пам'ятайте, що деякі бази даних можуть не підтримувати цей тип з'єднання."

#: querydlg.src
msgctxt ""
"querydlg.src\n"
"STR_QUERY_INNER_JOIN\n"
"string.text"
msgid "Includes only records for which the contents of the related fields of both tables are identical."
msgstr "Включає лише записи, для яких вміст відповідних полів кожної таблиці є ідентичним."

#: querydlg.src
msgctxt ""
"querydlg.src\n"
"STR_QUERY_LEFTRIGHT_JOIN\n"
"string.text"
msgid "Contains ALL records from table '%1' but only records from table '%2' where the values in the related fields are matching."
msgstr "Містить УСІ записи з таблиці '%1' але тільки записи з таблиці '%2', в яких зміст зв'язаних полів обох таблиць є однаковим."

#: querydlg.src
msgctxt ""
"querydlg.src\n"
"STR_QUERY_FULL_JOIN\n"
"string.text"
msgid "Contains ALL records from '%1' and from '%2'."
msgstr "Містить УСІ записи з '%1' та з '%2'."

#: querydlg.src
msgctxt ""
"querydlg.src\n"
"STR_QUERY_CROSS_JOIN\n"
"string.text"
msgid "Contains the Cartesian product of ALL records from '%1' and from '%2'."
msgstr "Містить декартовий добуток усіх записів з '%1' та '%2'."

#: querydlg.src
msgctxt ""
"querydlg.src\n"
"STR_QUERY_NATURAL_JOIN\n"
"string.text"
msgid "Contains only one column for each pair of equally-named columns from '%1' and from '%2'."
msgstr "Містить лише один стовпчик для кожної пари однаково названих стовпчиків з '%1' та '%2'."

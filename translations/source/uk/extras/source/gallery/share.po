#. extracted from extras/source/gallery/share
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2014-11-18 11:23+0100\n"
"PO-Revision-Date: 2015-03-17 20:50+0000\n"
"Last-Translator: Андрій <andriykopanytsia@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Pootle 2.5.1\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1426625459.000000\n"

#: gallery_names.ulf
msgctxt ""
"gallery_names.ulf\n"
"arrows\n"
"LngText.text"
msgid "Arrows"
msgstr "Стрілки"

#: gallery_names.ulf
msgctxt ""
"gallery_names.ulf\n"
"computers\n"
"LngText.text"
msgid "Computers"
msgstr "Комп'ютери"

#: gallery_names.ulf
msgctxt ""
"gallery_names.ulf\n"
"diagrams\n"
"LngText.text"
msgid "Diagrams"
msgstr "Діаграми"

#: gallery_names.ulf
msgctxt ""
"gallery_names.ulf\n"
"education\n"
"LngText.text"
msgid "School & University"
msgstr "Школа та університет"

#: gallery_names.ulf
msgctxt ""
"gallery_names.ulf\n"
"environment\n"
"LngText.text"
msgid "Environment"
msgstr "Середовище"

#: gallery_names.ulf
msgctxt ""
"gallery_names.ulf\n"
"finance\n"
"LngText.text"
msgid "Finance"
msgstr "Фінанси"

#: gallery_names.ulf
msgctxt ""
"gallery_names.ulf\n"
"people\n"
"LngText.text"
msgid "People"
msgstr "Люди"

#: gallery_names.ulf
msgctxt ""
"gallery_names.ulf\n"
"sounds\n"
"LngText.text"
msgid "Sounds"
msgstr "Звуки"

#: gallery_names.ulf
msgctxt ""
"gallery_names.ulf\n"
"symbols\n"
"LngText.text"
msgid "Symbols"
msgstr "Символи"

#: gallery_names.ulf
msgctxt ""
"gallery_names.ulf\n"
"txtshapes\n"
"LngText.text"
msgid "Text Shapes"
msgstr "Текстові форми"

#: gallery_names.ulf
msgctxt ""
"gallery_names.ulf\n"
"transportation\n"
"LngText.text"
msgid "Transportation"
msgstr "Транспорт "

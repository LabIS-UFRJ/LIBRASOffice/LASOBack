#. extracted from svx/source/accessibility
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-04-22 23:41+0200\n"
"PO-Revision-Date: 2017-03-02 22:32+0000\n"
"Last-Translator: Necdet Yucel <necdetyucel@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1488493936.000000\n"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_3D_MATERIAL_COLOR\n"
"string.text"
msgid "3D material color"
msgstr "3B madde rengi"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_TEXT_COLOR\n"
"string.text"
msgid "Font color"
msgstr "Yazı tipi rengi"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_BACKGROUND_COLOR\n"
"string.text"
msgid "Background color"
msgstr "Arkaplan rengi"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_FILLSTYLE_NONE\n"
"string.text"
msgid "None"
msgstr "Hiçbiri"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_FILLSTYLE_SOLID\n"
"string.text"
msgid "Solid"
msgstr "Kesiksiz"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_FILLSTYLE_HATCH\n"
"string.text"
msgid "With hatching"
msgstr "Tarayarak"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_FILLSTYLE_GRADIENT\n"
"string.text"
msgid "Gradient"
msgstr "Değişim ölçüsü"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_FILLSTYLE_BITMAP\n"
"string.text"
msgid "Bitmap"
msgstr "Bit eşlem"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_WITH\n"
"string.text"
msgid "with"
msgstr "ile"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_STYLE\n"
"string.text"
msgid "Style"
msgstr "Biçem"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_AND\n"
"string.text"
msgid "and"
msgstr "ve"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CORN_NAME\n"
"string.text"
msgid "Corner control"
msgstr "Köşe denetimi"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CORN_DESCR\n"
"string.text"
msgid "Selection of a corner point."
msgstr "Bir köşe noktanın seçimi."

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_ANGL_NAME\n"
"string.text"
msgid "Angle control"
msgstr "Açı denetimi"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_ANGL_DESCR\n"
"string.text"
msgid "Selection of a major angle."
msgstr "Büyük bir açı seçimi."

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_LT\n"
"string.text"
msgid "Top left"
msgstr "Üst sol"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_MT\n"
"string.text"
msgid "Top middle"
msgstr "Üst orta"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_RT\n"
"string.text"
msgid "Top right"
msgstr "Üst sağ"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_LM\n"
"string.text"
msgid "Left center"
msgstr "Sol merkez"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_MM\n"
"string.text"
msgid "Center"
msgstr "Merkez"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_RM\n"
"string.text"
msgid "Right center"
msgstr "Sağ merkez"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_LB\n"
"string.text"
msgid "Bottom left"
msgstr "Alt sol"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_MB\n"
"string.text"
msgid "Bottom middle"
msgstr "Alt orta"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_RB\n"
"string.text"
msgid "Bottom right"
msgstr "Alt sağ"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A000\n"
"string.text"
msgid "0 degrees"
msgstr "0 derece"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A045\n"
"string.text"
msgid "45 degrees"
msgstr "45 Derece"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A090\n"
"string.text"
msgid "90 degrees"
msgstr "90 Derece"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A135\n"
"string.text"
msgid "135 degrees"
msgstr "135 derece"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A180\n"
"string.text"
msgid "180 degrees"
msgstr "180 derece"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A225\n"
"string.text"
msgid "225 degrees"
msgstr "225 derece"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A270\n"
"string.text"
msgid "270 degrees"
msgstr "270 derece"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A315\n"
"string.text"
msgid "315 degrees"
msgstr "315 derece"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_GRAPHCTRL_ACC_NAME\n"
"string.text"
msgid "Contour control"
msgstr "Şekil denetimi"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_GRAPHCTRL_ACC_DESCRIPTION\n"
"string.text"
msgid "This is where you can edit the contour."
msgstr "Buradan şekli düzenleyebilirsiniz."

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_CHARACTER_SELECTION\n"
"string.text"
msgid "Special character selection"
msgstr "Özel karakter seçimi"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_CHAR_SEL_DESC\n"
"string.text"
msgid "Select special characters in this area."
msgstr "Bu alan içinde özel karakterler seç."

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_CHARACTER_CODE\n"
"string.text"
msgid "Character code "
msgstr "Karakter kodu "

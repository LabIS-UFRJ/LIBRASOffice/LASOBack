#. extracted from basctl/source/basicide
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2017-01-12 15:37+0000\n"
"Last-Translator: Karl Morten Ramberg <karl.m.ramberg@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1484235469.000000\n"

#: basicprint.src
msgctxt ""
"basicprint.src\n"
"RID_STR_PRINTDLG_RANGE\n"
"string.text"
msgid "Print range"
msgstr "Utskriftsområde"

#: basicprint.src
msgctxt ""
"basicprint.src\n"
"RID_STR_PRINTDLG_ALLPAGES\n"
"string.text"
msgid "All ~Pages"
msgstr "~Alle sider"

#: basicprint.src
msgctxt ""
"basicprint.src\n"
"RID_STR_PRINTDLG_PAGES\n"
"string.text"
msgid "Pa~ges"
msgstr "~Sider"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_FILTER_ALLFILES\n"
"string.text"
msgid "<All>"
msgstr "<Alle>"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_NOMODULE\n"
"string.text"
msgid "< No Module >"
msgstr "< Ingen modul >"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_WRONGPASSWORD\n"
"string.text"
msgid "Incorrect Password"
msgstr "Feil passord."

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_OPEN\n"
"string.text"
msgid "Load"
msgstr "Last inn"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_SAVE\n"
"string.text"
msgid "Save"
msgstr "Lagre"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_SOURCETOBIG\n"
"string.text"
msgid ""
"The source text is too large and can be neither compiled nor saved.\n"
"Delete some of the comments or transfer some methods into another module."
msgstr ""
"Kildeteksten er for stor og kan verken lagres eller kompileres.\n"
"Slett noen av merknadene eller overfør noen av metodene til en annen modul."

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_ERROROPENSTORAGE\n"
"string.text"
msgid "Error opening file"
msgstr "Feil ved åpning av fil"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_ERROROPENLIB\n"
"string.text"
msgid "Error loading library"
msgstr "Feil ved innlasting av bibliotek"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_NOLIBINSTORAGE\n"
"string.text"
msgid "The file does not contain any BASIC libraries"
msgstr "Fila inneholder ingen BASIC-biblioteker"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_BADSBXNAME\n"
"string.text"
msgid "Invalid Name"
msgstr "Ugyldig navn"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_LIBNAMETOLONG\n"
"string.text"
msgid "A library name can have up to 30 characters."
msgstr "Et navn på et bibliotek kan ha opptil 30 bokstaver."

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_ERRORCHOOSEMACRO\n"
"string.text"
msgid "Macros from other documents are not accessible."
msgstr "Makroer fra andre dokumenter er ikke tilgjengelige."

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_LIBISREADONLY\n"
"string.text"
msgid "This library is read-only."
msgstr "Dette biblioteket er skrivebeskyttet."

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_REPLACELIB\n"
"string.text"
msgid "'XX' cannot be replaced."
msgstr "«XX» kan ikke erstattes."

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_IMPORTNOTPOSSIBLE\n"
"string.text"
msgid "'XX' cannot be added."
msgstr "«XX» kan ikke legges til."

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_NOIMPORT\n"
"string.text"
msgid "'XX' was not added."
msgstr "«XX» ble ikke lagt til."

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_ENTERPASSWORD\n"
"string.text"
msgid "Enter password for 'XX'"
msgstr "Oppgi passord for «XX»"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_SBXNAMEALLREADYUSED\n"
"string.text"
msgid "Name already exists"
msgstr "Navnet finnes allerede"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_SIGNED\n"
"string.text"
msgid "(Signed)"
msgstr "(Signert)"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_SBXNAMEALLREADYUSED2\n"
"string.text"
msgid "Object with same name already exists"
msgstr "Det finnes allerede et objekt med dette navnet"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_FILEEXISTS\n"
"string.text"
msgid "The 'XX' file already exists"
msgstr "Fila «XX» finnes allerede"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_CANNOTRUNMACRO\n"
"string.text"
msgid ""
"For security reasons, you cannot run this macro.\n"
"\n"
"For more information, check the security settings."
msgstr ""
"Av sikkerhetsgrunner kan du ikke kjøre denne makroen.\n"
"\n"
"For mer informasjon, sjekk sikkerhetsinnstillingene."

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_RUNTIMEERROR\n"
"string.text"
msgid "Runtime Error: #"
msgstr "Kjørefeil: #"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_SEARCHNOTFOUND\n"
"string.text"
msgid "Search key not found"
msgstr "Fant ikke søketeksten"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_SEARCHFROMSTART\n"
"string.text"
msgid "Search to last module complete. Continue at first module?"
msgstr "Har søkt helt til den siste modulen. Vil du fortsette i den første modulen?"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_SEARCHREPLACES\n"
"string.text"
msgid "Search key replaced XX times"
msgstr "Søketeksten ble erstattet XX ganger"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_COULDNTREAD\n"
"string.text"
msgid "The file could not be read"
msgstr "Klarte ikke å lese fila"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_COULDNTWRITE\n"
"string.text"
msgid "The file could not be saved"
msgstr "Klarte ikke å lagre fila"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_CANNOTCHANGENAMESTDLIB\n"
"string.text"
msgid "The name of the default library cannot be changed."
msgstr "Navnet på standardbiblioteket kan ikke endres."

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_CANNOTCHANGENAMEREFLIB\n"
"string.text"
msgid "The name of a referenced library cannot be changed."
msgstr "Navnet på et referert bibliotek kan ikke endres."

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_CANNOTUNLOADSTDLIB\n"
"string.text"
msgid "The default library cannot be deactivated"
msgstr "Standardbiblioteket må være i bruk"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_GENERATESOURCE\n"
"string.text"
msgid "Generating source"
msgstr "Oppretter kilde"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_FILENAME\n"
"string.text"
msgid "File name:"
msgstr "Filnavn:"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_APPENDLIBS\n"
"string.text"
msgid "Import Libraries"
msgstr "Importer biblioteker"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_QUERYDELMACRO\n"
"string.text"
msgid "Do you want to delete the macro XX?"
msgstr "Vil du slette makroen XX?"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_QUERYDELDIALOG\n"
"string.text"
msgid "Do you want to delete the XX dialog?"
msgstr "Vil du slette dialogvinduet XX?"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_QUERYDELLIB\n"
"string.text"
msgid "Do you want to delete the XX library?"
msgstr "Vil du slette biblioteket XX?"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_QUERYDELLIBREF\n"
"string.text"
msgid "Do you want to delete the reference to the XX library?"
msgstr "Vil du slette referansen til biblioteket XX?"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_QUERYDELMODULE\n"
"string.text"
msgid "Do you want to delete the XX module?"
msgstr "Vil du slette modulen XX?"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_OBJNOTFOUND\n"
"string.text"
msgid "Object or method not found"
msgstr "Fant ikke objektet eller metoden"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_BASIC\n"
"string.text"
msgid "BASIC"
msgstr "BASIC"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_LINE\n"
"string.text"
msgid "Ln"
msgstr "Ln"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_COLUMN\n"
"string.text"
msgid "Col"
msgstr "Kol"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_DOC\n"
"string.text"
msgid "Document"
msgstr "Dokument"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_BASICIDE_OBJECTBAR\n"
"string.text"
msgid "Macro Bar"
msgstr "Makrolinje"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_CANNOTCLOSE\n"
"string.text"
msgid "The window cannot be closed while BASIC is running."
msgstr "Vinduet kan ikke lukkes mens BASIC kjører."

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_REPLACESTDLIB\n"
"string.text"
msgid "The default library cannot be replaced."
msgstr "Standardbiblioteket kan ikke erstattes."

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_REFNOTPOSSIBLE\n"
"string.text"
msgid "Reference to 'XX' not possible."
msgstr "Ikke mulig å referere til «XX»."

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_WATCHNAME\n"
"string.text"
msgid "Watch"
msgstr "Overvåk"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_WATCHVARIABLE\n"
"string.text"
msgid "Variable"
msgstr "Variabel"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_WATCHVALUE\n"
"string.text"
msgid "Value"
msgstr "Verdi"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_WATCHTYPE\n"
"string.text"
msgid "Type"
msgstr "Type"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_STACKNAME\n"
"string.text"
msgid "Call Stack"
msgstr "Kommandostabel"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_INITIDE\n"
"string.text"
msgid "BASIC Initialization"
msgstr "Start av BASIC"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_STDMODULENAME\n"
"string.text"
msgid "Module"
msgstr "Modul"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_STDDIALOGNAME\n"
"string.text"
msgid "Dialog"
msgstr "Dialogvindu"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_STDLIBNAME\n"
"string.text"
msgid "Library"
msgstr "Bibliotek"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_NEWLIB\n"
"string.text"
msgid "New Library"
msgstr "Nytt bibliotek"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_NEWMOD\n"
"string.text"
msgid "New Module"
msgstr "Ny modul"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_NEWDLG\n"
"string.text"
msgid "New Dialog"
msgstr "Nytt dialogvindu"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_ALL\n"
"string.text"
msgid "All"
msgstr "Alle"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_PAGE\n"
"string.text"
msgid "Page"
msgstr "Side"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_MACRONAMEREQ\n"
"string.text"
msgid "A name must be entered."
msgstr "Et navn må skrives inn."

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_WILLSTOPPRG\n"
"string.text"
msgid ""
"You will have to restart the program after this edit.\n"
"Continue?"
msgstr ""
"Du må starte programmet på nytt etter endringa.\n"
"Vil du fortsette?"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_SEARCHALLMODULES\n"
"string.text"
msgid "Do you want to replace the text in all active modules?"
msgstr "Vil du erstatte teksten i alle aktive moduler?"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_REMOVEWATCH\n"
"string.text"
msgid "Watch:"
msgstr "Overvåk:"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_STACK\n"
"string.text"
msgid "Calls: "
msgstr "Kall: "

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_USERMACROS\n"
"string.text"
msgid "My Macros"
msgstr "Mine makroer"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_USERDIALOGS\n"
"string.text"
msgid "My Dialogs"
msgstr "Mine dialogvinduer"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_USERMACROSDIALOGS\n"
"string.text"
msgid "My Macros & Dialogs"
msgstr "Mine makroer og dialogvinduer"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_SHAREMACROS\n"
"string.text"
msgid "%PRODUCTNAME Macros"
msgstr "%PRODUCTNAME-makroer"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_SHAREDIALOGS\n"
"string.text"
msgid "%PRODUCTNAME Dialogs"
msgstr "%PRODUCTNAME-dialogvinduer"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_SHAREMACROSDIALOGS\n"
"string.text"
msgid "%PRODUCTNAME Macros & Dialogs"
msgstr "Makroer og dialogvinduer i %PRODUCTNAME"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_POPUP_BRKPROPS\n"
"RID_ACTIV\n"
"menuitem.text"
msgid "Active"
msgstr "Gjeldende"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_POPUP_BRKPROPS\n"
"RID_BRKPROPS\n"
"menuitem.text"
msgid "Properties..."
msgstr "Egenskaper …"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_POPUP_BRKPROPS\n"
"menu.text"
msgid "Properties"
msgstr "Egenskaper"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_POPUP_BRKDLG\n"
"RID_BRKDLG\n"
"menuitem.text"
msgid "Manage Breakpoints..."
msgstr "Bruddpunkter …"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_POPUP_BRKDLG\n"
"menu.text"
msgid "Manage Breakpoints"
msgstr "Behandle bruddpunkter …"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_POPUP_TABBAR.RID_INSERT\n"
"SID_BASICIDE_NEWMODULE\n"
"menuitem.text"
msgid "BASIC Module"
msgstr "BASIC-modul"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_POPUP_TABBAR.RID_INSERT\n"
"SID_BASICIDE_NEWDIALOG\n"
"menuitem.text"
msgid "BASIC Dialog"
msgstr "BASIC-dialogvindu"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_POPUP_TABBAR\n"
"RID_INSERT\n"
"menuitem.text"
msgid "Insert"
msgstr "Sett inn"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_POPUP_TABBAR\n"
"SID_BASICIDE_DELETECURRENT\n"
"menuitem.text"
msgid "Delete"
msgstr "Slett"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_POPUP_TABBAR\n"
"SID_BASICIDE_RENAMECURRENT\n"
"menuitem.text"
msgid "Rename"
msgstr "Gi nytt navn"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_POPUP_TABBAR\n"
"SID_BASICIDE_HIDECURPAGE\n"
"menuitem.text"
msgid "Hide"
msgstr "Skjul"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_POPUP_TABBAR\n"
"SID_BASICIDE_MODULEDLG\n"
"menuitem.text"
msgid "Modules..."
msgstr "Moduler …"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_REMOVEWATCHTIP\n"
"string.text"
msgid "Remove Watch"
msgstr "Fjern overvåking"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_QUERYREPLACEMACRO\n"
"string.text"
msgid "Do you want to overwrite the XX macro?"
msgstr "Vil du overskrive makroen XX?"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_TRANSLATION_NOTLOCALIZED\n"
"string.text"
msgid "<Not localized>"
msgstr "<Ikke oversatt>"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_TRANSLATION_DEFAULT\n"
"string.text"
msgid "[Default Language]"
msgstr "[ Standardspråk ]"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_DOCUMENT_OBJECTS\n"
"string.text"
msgid "Document Objects"
msgstr "Dokumentobjekter"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_USERFORMS\n"
"string.text"
msgid "Forms"
msgstr "Skjemaer"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_NORMAL_MODULES\n"
"string.text"
msgid "Modules"
msgstr "Moduler"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_CLASS_MODULES\n"
"string.text"
msgid "Class Modules"
msgstr "Klassemoduler"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_DLGIMP_CLASH_RENAME\n"
"string.text"
msgid "Rename"
msgstr "Gi nytt navn"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_DLGIMP_CLASH_REPLACE\n"
"string.text"
msgid "Replace"
msgstr "Erstatt"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_DLGIMP_CLASH_TITLE\n"
"string.text"
msgid "Dialog Import - Name already used"
msgstr "Dialogvindu for import - Navnet er allerede i bruk"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_DLGIMP_CLASH_TEXT\n"
"string.text"
msgid ""
"The library already contains a dialog with the name:\n"
"\n"
"$(ARG1)\n"
"\n"
"Rename dialog to keep current dialog or replace existing dialog.\n"
" "
msgstr ""
"Biblioteket inneholder allerede et dialogvindu med navnet:\n"
"\n"
"$(ARG1)\n"
"\n"
"Endre navnet til dialogvinduet for å beholde det, eller erstatt det eksisterende.\n"
" "

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_DLGIMP_MISMATCH_ADD\n"
"string.text"
msgid "Add"
msgstr "Legg til"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_DLGIMP_MISMATCH_OMIT\n"
"string.text"
msgid "Omit"
msgstr "Utelat"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_DLGIMP_MISMATCH_TITLE\n"
"string.text"
msgid "Dialog Import - Language Mismatch"
msgstr "Dialogvindu for import - Språket stemmer ikke"

#: basidesh.src
msgctxt ""
"basidesh.src\n"
"RID_STR_DLGIMP_MISMATCH_TEXT\n"
"string.text"
msgid ""
"The dialog to be imported supports other languages than the target library.\n"
"\n"
"Add these languages to the library to keep additional language resources provided by the dialog or omit them to stay with the current library languages.\n"
"\n"
"Note: For languages not supported by the dialog the resources of the dialog's default language will be used.\n"
" "
msgstr ""
"Dialogvinduet som blir importert støtter andre språk enn målbiblioteket.\n"
"\n"
"Legg disse språkene til biblioteket for å beholde ytterligere språkressurser gitt av dialogvinduet eller lå være de være med det gjeldende bibliotekets språk.\n"
"\n"
"Merk: For språk som ikke støttes av dialogvinduet, vil standardspråket brukes.\n"
"."

#: macrodlg.src
msgctxt ""
"macrodlg.src\n"
"RID_STR_BTNDEL\n"
"string.text"
msgid "~Delete"
msgstr "~Slett"

#: macrodlg.src
msgctxt ""
"macrodlg.src\n"
"RID_STR_BTNNEW\n"
"string.text"
msgid "~New"
msgstr "~Ny"

#: macrodlg.src
msgctxt ""
"macrodlg.src\n"
"RID_STR_CHOOSE\n"
"string.text"
msgid "Choose"
msgstr "Velg"

#: macrodlg.src
msgctxt ""
"macrodlg.src\n"
"RID_STR_RUN\n"
"string.text"
msgid "Run"
msgstr "Kjør"

#: macrodlg.src
msgctxt ""
"macrodlg.src\n"
"RID_STR_RECORD\n"
"string.text"
msgid "~Save"
msgstr "~Lagre"

#: moduldlg.src
msgctxt ""
"moduldlg.src\n"
"RID_STR_EXPORTPACKAGE\n"
"string.text"
msgid "Export library as extension"
msgstr "Eksporter bibliotek som utvidelse"

#: moduldlg.src
msgctxt ""
"moduldlg.src\n"
"RID_STR_EXPORTBASIC\n"
"string.text"
msgid "Export as BASIC library"
msgstr "Eksporter som et BASIC-bibliotek"

#: moduldlg.src
msgctxt ""
"moduldlg.src\n"
"RID_STR_PACKAGE_BUNDLE\n"
"string.text"
msgid "Extension"
msgstr "Utvidelse"

#: objdlg.src
msgctxt ""
"objdlg.src\n"
"RID_BASICIDE_OBJCAT\n"
"string.text"
msgid "Object Catalog"
msgstr "Objektkatalog"

#: objdlg.src
msgctxt ""
"objdlg.src\n"
"RID_STR_TLB_MACROS\n"
"string.text"
msgid "Objects Tree"
msgstr "Objekttre"

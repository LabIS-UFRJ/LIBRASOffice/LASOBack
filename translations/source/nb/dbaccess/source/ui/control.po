#. extracted from dbaccess/source/ui/control
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-11-20 13:02+0100\n"
"PO-Revision-Date: 2013-01-22 05:35+0000\n"
"Last-Translator: Olav <odahlum@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1358832954.0\n"

#: TableGrantCtrl.src
msgctxt ""
"TableGrantCtrl.src\n"
"STR_TABLE_PRIV_NAME\n"
"string.text"
msgid "Table name"
msgstr "Tabellnavn"

#: TableGrantCtrl.src
msgctxt ""
"TableGrantCtrl.src\n"
"STR_TABLE_PRIV_INSERT\n"
"string.text"
msgid "Insert data"
msgstr "Sett inn data"

#: TableGrantCtrl.src
msgctxt ""
"TableGrantCtrl.src\n"
"STR_TABLE_PRIV_DELETE\n"
"string.text"
msgid "Delete data"
msgstr "Slett data"

#: TableGrantCtrl.src
msgctxt ""
"TableGrantCtrl.src\n"
"STR_TABLE_PRIV_UPDATE\n"
"string.text"
msgid "Modify data"
msgstr "Endre data"

#: TableGrantCtrl.src
msgctxt ""
"TableGrantCtrl.src\n"
"STR_TABLE_PRIV_ALTER\n"
"string.text"
msgid "Alter structure"
msgstr "Endre struktur"

#: TableGrantCtrl.src
msgctxt ""
"TableGrantCtrl.src\n"
"STR_TABLE_PRIV_SELECT\n"
"string.text"
msgid "Read data"
msgstr "Les data"

#: TableGrantCtrl.src
msgctxt ""
"TableGrantCtrl.src\n"
"STR_TABLE_PRIV_REFERENCE\n"
"string.text"
msgid "Modify references"
msgstr "Endre referanser"

#: TableGrantCtrl.src
msgctxt ""
"TableGrantCtrl.src\n"
"STR_TABLE_PRIV_DROP\n"
"string.text"
msgid "Drop structure"
msgstr "Dropp struktur"

#: tabletree.src
msgctxt ""
"tabletree.src\n"
"STR_COULDNOTCREATE_DRIVERMANAGER\n"
"string.text"
msgid "Cannot connect to the SDBC driver manager (#servicename#)."
msgstr "Kan ikke koble til SDBC-driverbehandleren (#servicename#)."

#: tabletree.src
msgctxt ""
"tabletree.src\n"
"STR_NOREGISTEREDDRIVER\n"
"string.text"
msgid "A driver is not registered for the URL #connurl#."
msgstr "Ingen driver er registrert for URL-en #connurl#."

#: tabletree.src
msgctxt ""
"tabletree.src\n"
"STR_COULDNOTCONNECT\n"
"string.text"
msgid "No connection could be established for the URL #connurl#."
msgstr "Klarte ikke å opprette en forbindelse med URL-en #connurl#."

#: tabletree.src
msgctxt ""
"tabletree.src\n"
"STR_COULDNOTCONNECT_PLEASECHECK\n"
"string.text"
msgid "Please check the current settings, for example user name and password."
msgstr "Sjekk alle innstillingene dine, for eksempel brukernavn og passord."

#: tabletree.src
msgctxt ""
"tabletree.src\n"
"STR_NOTABLEINFO\n"
"string.text"
msgid "Successfully connected, but information about database tables is not available."
msgstr "Forbindelsen var vellykket, men informasjon om databasetabeller er ikke tilgjengelig."

#: tabletree.src
msgctxt ""
"tabletree.src\n"
"STR_ALL_TABLES\n"
"string.text"
msgid "All tables"
msgstr "Alle tabeller"

#: tabletree.src
msgctxt ""
"tabletree.src\n"
"STR_ALL_VIEWS\n"
"string.text"
msgid "All views"
msgstr "Alle utsnitt"

#: tabletree.src
msgctxt ""
"tabletree.src\n"
"STR_ALL_TABLES_AND_VIEWS\n"
"string.text"
msgid "All tables and views"
msgstr "Alle tabeller og utsnitt"

#: undosqledit.src
msgctxt ""
"undosqledit.src\n"
"STR_QUERY_UNDO_MODIFYSQLEDIT\n"
"string.text"
msgid "Modify SQL statement(s)"
msgstr "Endre SQL-setning(er)"

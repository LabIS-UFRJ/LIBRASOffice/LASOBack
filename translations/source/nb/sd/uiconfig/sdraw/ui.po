#. extracted from sd/uiconfig/sdraw/ui
msgid ""
msgstr ""
"Project-Id-Version: LibO 40l10n\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2017-01-25 00:58+0000\n"
"Last-Translator: Karl Morten Ramberg <karl.m.ramberg@gmail.com>\n"
"Language-Team: none\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1485305909.000000\n"

#: breakdialog.ui
msgctxt ""
"breakdialog.ui\n"
"BreakDialog\n"
"title\n"
"string.text"
msgid "Break"
msgstr "Del opp"

#: breakdialog.ui
msgctxt ""
"breakdialog.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Processing metafile:"
msgstr "Behandler metafil:"

#: breakdialog.ui
msgctxt ""
"breakdialog.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Broken down metaobjects:"
msgstr "Oppdelte metaobjekter:"

#: breakdialog.ui
msgctxt ""
"breakdialog.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "Inserted drawing objects:"
msgstr "Sett inn tegneobjekter:"

#: bulletsandnumbering.ui
msgctxt ""
"bulletsandnumbering.ui\n"
"BulletsAndNumberingDialog\n"
"title\n"
"string.text"
msgid "Bullets and Numbering"
msgstr "Punkttegn og nummerering"

#: bulletsandnumbering.ui
msgctxt ""
"bulletsandnumbering.ui\n"
"reset\n"
"label\n"
"string.text"
msgid "Reset"
msgstr "Nullstill"

#: bulletsandnumbering.ui
msgctxt ""
"bulletsandnumbering.ui\n"
"bullets\n"
"label\n"
"string.text"
msgid "Bullets"
msgstr "Punkttegn"

#: bulletsandnumbering.ui
msgctxt ""
"bulletsandnumbering.ui\n"
"singlenum\n"
"label\n"
"string.text"
msgid "Numbering"
msgstr "Nummerering"

#: bulletsandnumbering.ui
msgctxt ""
"bulletsandnumbering.ui\n"
"graphics\n"
"label\n"
"string.text"
msgid "Image"
msgstr "Bilde"

#: bulletsandnumbering.ui
msgctxt ""
"bulletsandnumbering.ui\n"
"position\n"
"label\n"
"string.text"
msgid "Position"
msgstr "Posisjon"

#: bulletsandnumbering.ui
msgctxt ""
"bulletsandnumbering.ui\n"
"customize\n"
"label\n"
"string.text"
msgid "Customize"
msgstr "Tilpass"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"DuplicateDialog\n"
"title\n"
"string.text"
msgid "Duplicate"
msgstr "Lag kopi"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"default\n"
"label\n"
"string.text"
msgid "_Default"
msgstr "_Standard"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"label4\n"
"label\n"
"string.text"
msgid "Number of _copies:"
msgstr "Antall kopier:"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"viewdata\n"
"tooltip_text\n"
"string.text"
msgid "Values from Selection"
msgstr "Verdier fra utvalg"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"viewdata-atkobject\n"
"AtkObject::accessible-name\n"
"string.text"
msgid "Values from Selection"
msgstr "Verdier fra utvalg"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"label5\n"
"label\n"
"string.text"
msgid "_X axis:"
msgstr "_X-akse:"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"label6\n"
"label\n"
"string.text"
msgid "_Y axis:"
msgstr "_Y-akse:"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"label7\n"
"label\n"
"string.text"
msgid "_Angle:"
msgstr "_Vinkel:"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Placement"
msgstr "Plassering"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"label8\n"
"label\n"
"string.text"
msgid "_Width:"
msgstr "_Bredde:"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"label9\n"
"label\n"
"string.text"
msgid "_Height:"
msgstr "_Høyde:"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Enlargement"
msgstr "Forstørrelse"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"label10\n"
"label\n"
"string.text"
msgid "_Start:"
msgstr "_Start:"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"endlabel\n"
"label\n"
"string.text"
msgid "_End:"
msgstr "_Slutt:"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "Colors"
msgstr "Farger"

#: crossfadedialog.ui
msgctxt ""
"crossfadedialog.ui\n"
"CrossFadeDialog\n"
"title\n"
"string.text"
msgid "Cross-fading"
msgstr "Overgangstoning"

#: crossfadedialog.ui
msgctxt ""
"crossfadedialog.ui\n"
"orientation\n"
"label\n"
"string.text"
msgid "Same orientation"
msgstr "Samme retning"

#: crossfadedialog.ui
msgctxt ""
"crossfadedialog.ui\n"
"attributes\n"
"label\n"
"string.text"
msgid "Cross-fade attributes"
msgstr "Egenskaper for overgangstoning"

#: crossfadedialog.ui
msgctxt ""
"crossfadedialog.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Increments:"
msgstr "Steg:"

#: crossfadedialog.ui
msgctxt ""
"crossfadedialog.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Settings"
msgstr "Innstillinger"

#: dlgsnap.ui
msgctxt ""
"dlgsnap.ui\n"
"SnapObjectDialog\n"
"title\n"
"string.text"
msgid "New Snap Object"
msgstr "Nytt festeobjekt"

#: dlgsnap.ui
msgctxt ""
"dlgsnap.ui\n"
"xlabel\n"
"label\n"
"string.text"
msgid "_X:"
msgstr "_X:"

#: dlgsnap.ui
msgctxt ""
"dlgsnap.ui\n"
"ylabel\n"
"label\n"
"string.text"
msgid "_Y:"
msgstr "_Y:"

#: dlgsnap.ui
msgctxt ""
"dlgsnap.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Position"
msgstr "Plassering"

#: dlgsnap.ui
msgctxt ""
"dlgsnap.ui\n"
"point\n"
"label\n"
"string.text"
msgid "_Point"
msgstr "_Punkt"

#: dlgsnap.ui
msgctxt ""
"dlgsnap.ui\n"
"vert\n"
"label\n"
"string.text"
msgid "_Vertical"
msgstr "_Loddrett"

#: dlgsnap.ui
msgctxt ""
"dlgsnap.ui\n"
"horz\n"
"label\n"
"string.text"
msgid "Hori_zontal"
msgstr "_Vannrett"

#: dlgsnap.ui
msgctxt ""
"dlgsnap.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Type"
msgstr "Type"

#: drawchardialog.ui
msgctxt ""
"drawchardialog.ui\n"
"DrawCharDialog\n"
"title\n"
"string.text"
msgid "Character"
msgstr "Tegn"

#: drawchardialog.ui
msgctxt ""
"drawchardialog.ui\n"
"RID_SVXPAGE_CHAR_NAME\n"
"label\n"
"string.text"
msgid "Fonts"
msgstr "Skrifter"

#: drawchardialog.ui
msgctxt ""
"drawchardialog.ui\n"
"RID_SVXPAGE_CHAR_EFFECTS\n"
"label\n"
"string.text"
msgid "Font Effects"
msgstr "Skrifteffekter"

#: drawchardialog.ui
msgctxt ""
"drawchardialog.ui\n"
"RID_SVXPAGE_CHAR_POSITION\n"
"label\n"
"string.text"
msgid "Position"
msgstr "Posisjon"

#: drawchardialog.ui
msgctxt ""
"drawchardialog.ui\n"
"RID_SVXPAGE_BACKGROUND\n"
"label\n"
"string.text"
msgid "Highlighting"
msgstr "Utheving"

#: drawpagedialog.ui
msgctxt ""
"drawpagedialog.ui\n"
"DrawPageDialog\n"
"title\n"
"string.text"
msgid "Page Setup"
msgstr "Sideoppsett"

#: drawpagedialog.ui
msgctxt ""
"drawpagedialog.ui\n"
"RID_SVXPAGE_PAGE\n"
"label\n"
"string.text"
msgid "Page"
msgstr "Side"

#: drawpagedialog.ui
msgctxt ""
"drawpagedialog.ui\n"
"RID_SVXPAGE_AREA\n"
"label\n"
"string.text"
msgid "Background"
msgstr "Bakgrunn"

#: drawpagedialog.ui
msgctxt ""
"drawpagedialog.ui\n"
"RID_SVXPAGE_TRANSPARENCE\n"
"label\n"
"string.text"
msgid "Transparency"
msgstr "Gjennomsiktighet"

#: drawparadialog.ui
msgctxt ""
"drawparadialog.ui\n"
"DrawParagraphPropertiesDialog\n"
"title\n"
"string.text"
msgid "Paragraph"
msgstr "Avsnitt"

#: drawparadialog.ui
msgctxt ""
"drawparadialog.ui\n"
"labelTP_PARA_STD\n"
"label\n"
"string.text"
msgid "Indents & Spacing"
msgstr "Innrykk og avstand"

#: drawparadialog.ui
msgctxt ""
"drawparadialog.ui\n"
"labelTP_PARA_ASIAN\n"
"label\n"
"string.text"
msgid "Asian Typography"
msgstr "Asiatisk typografi"

#: drawparadialog.ui
msgctxt ""
"drawparadialog.ui\n"
"labelTP_TABULATOR\n"
"label\n"
"string.text"
msgid "Tabs"
msgstr "Tabulatorer"

#: drawparadialog.ui
msgctxt ""
"drawparadialog.ui\n"
"labelTP_PARA_ALIGN\n"
"label\n"
"string.text"
msgid "Alignment"
msgstr "Justering"

#: drawparadialog.ui
msgctxt ""
"drawparadialog.ui\n"
"labelNUMBERING\n"
"label\n"
"string.text"
msgid "Numbering"
msgstr "Nummerering"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"DrawPRTLDialog\n"
"title\n"
"string.text"
msgid "Presentation Layout"
msgstr "Presentasjonsoppsett"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_LINE\n"
"label\n"
"string.text"
msgid "Line"
msgstr "Linje"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_AREA\n"
"label\n"
"string.text"
msgid "Area"
msgstr "Område"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_SHADOW\n"
"label\n"
"string.text"
msgid "Shadow"
msgstr "Skygge"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_TRANSPARENCE\n"
"label\n"
"string.text"
msgid "Transparency"
msgstr "Gjennomsiktighet"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_CHAR_NAME\n"
"label\n"
"string.text"
msgid "Font"
msgstr "Skrift"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_CHAR_EFFECTS\n"
"label\n"
"string.text"
msgid "Font Effects"
msgstr "Skrifteffekter"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_STD_PARAGRAPH\n"
"label\n"
"string.text"
msgid "Indents & Spacing"
msgstr "Innrykk og avstand"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_TEXTATTR\n"
"label\n"
"string.text"
msgid "Text"
msgstr "Tekst"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_PICK_BULLET\n"
"label\n"
"string.text"
msgid "Bullets"
msgstr "Punkttegn"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_PICK_SINGLE_NUM\n"
"label\n"
"string.text"
msgid "Numbering"
msgstr "Nummerering"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_PICK_BMP\n"
"label\n"
"string.text"
msgid "Image"
msgstr "Bilde"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_NUM_OPTIONS\n"
"label\n"
"string.text"
msgid "Customize"
msgstr "Tilpass"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_ALIGN_PARAGRAPH\n"
"label\n"
"string.text"
msgid "Alignment"
msgstr "Justering"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_PARA_ASIAN\n"
"label\n"
"string.text"
msgid "Asian Typography"
msgstr "Asiatisk typografi"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_TABULATOR\n"
"label\n"
"string.text"
msgid "Tabs"
msgstr "Tabulatorer"

#: insertlayer.ui
msgctxt ""
"insertlayer.ui\n"
"InsertLayerDialog\n"
"title\n"
"string.text"
msgid "Insert Layer"
msgstr "Sett inn lag"

#: insertlayer.ui
msgctxt ""
"insertlayer.ui\n"
"label4\n"
"label\n"
"string.text"
msgid "_Name"
msgstr "_Navn"

#: insertlayer.ui
msgctxt ""
"insertlayer.ui\n"
"label5\n"
"label\n"
"string.text"
msgid "_Title"
msgstr "_Tittel"

#: insertlayer.ui
msgctxt ""
"insertlayer.ui\n"
"description\n"
"label\n"
"string.text"
msgid "_Description"
msgstr "_Beskrivelse"

#: insertlayer.ui
msgctxt ""
"insertlayer.ui\n"
"visible\n"
"label\n"
"string.text"
msgid "_Visible"
msgstr "_Synlig"

#: insertlayer.ui
msgctxt ""
"insertlayer.ui\n"
"printable\n"
"label\n"
"string.text"
msgid "_Printable"
msgstr "_Kan skrives ut"

#: insertlayer.ui
msgctxt ""
"insertlayer.ui\n"
"locked\n"
"label\n"
"string.text"
msgid "_Locked"
msgstr "_Låst"

#: insertslidesdialog.ui
msgctxt ""
"insertslidesdialog.ui\n"
"InsertSlidesDialog\n"
"title\n"
"string.text"
msgid "Insert Slides/Objects"
msgstr "Sett inn lysbilder/objekter"

#: insertslidesdialog.ui
msgctxt ""
"insertslidesdialog.ui\n"
"backgrounds\n"
"label\n"
"string.text"
msgid "Delete unused backg_rounds"
msgstr "Slett ubrukte _bakgrunner"

#: insertslidesdialog.ui
msgctxt ""
"insertslidesdialog.ui\n"
"links\n"
"label\n"
"string.text"
msgid "_Link"
msgstr "_Lenke"

#: namedesign.ui
msgctxt ""
"namedesign.ui\n"
"NameDesignDialog\n"
"title\n"
"string.text"
msgid "Name HTML Design"
msgstr "Gi navn til HTML-utforming"

#: paranumberingtab.ui
msgctxt ""
"paranumberingtab.ui\n"
"checkbuttonCB_NEW_START\n"
"label\n"
"string.text"
msgid "R_estart at this paragraph"
msgstr "Start på nytt ved dette _avsnittet"

#: paranumberingtab.ui
msgctxt ""
"paranumberingtab.ui\n"
"checkbuttonCB_NUMBER_NEW_START\n"
"label\n"
"string.text"
msgid "S_tart with:"
msgstr "Start med:"

#: paranumberingtab.ui
msgctxt ""
"paranumberingtab.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Paragraph Numbering"
msgstr "Avsnittsnummerering"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"printname\n"
"label\n"
"string.text"
msgid "Page name"
msgstr "Sidenavn"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"printdatetime\n"
"label\n"
"string.text"
msgid "Date and time"
msgstr "Dato og klokkeslett"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"label4\n"
"label\n"
"string.text"
msgid "Contents"
msgstr "Innhold"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"originalcolors\n"
"label\n"
"string.text"
msgid "Original size"
msgstr "Opprinnelig størrelse"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"grayscale\n"
"label\n"
"string.text"
msgid "Grayscale"
msgstr "Gråtoner"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"blackandwhite\n"
"label\n"
"string.text"
msgid "Black & white"
msgstr "Svart og hvit"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"label5\n"
"label\n"
"string.text"
msgid "Color"
msgstr "Farge"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"originalsize\n"
"label\n"
"string.text"
msgid "Original size"
msgstr "Opprinnelig størrelse"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"fittoprintable\n"
"label\n"
"string.text"
msgid "Fit to printable page"
msgstr "Tilpass til utskrivbar side"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"distributeonmultiple\n"
"label\n"
"string.text"
msgid "Distribute on multiple sheets of paper"
msgstr "Fordel på flere ark"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"tilesheet\n"
"label\n"
"string.text"
msgid "Tile sheet of paper with repeated pages"
msgstr "Side ved side ark med gjentatte sider"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"label6\n"
"label\n"
"string.text"
msgid "Size"
msgstr "Størrelse"

#: queryunlinkimagedialog.ui
msgctxt ""
"queryunlinkimagedialog.ui\n"
"QueryUnlinkImageDialog\n"
"title\n"
"string.text"
msgid "Release image's link?"
msgstr "Fjern lenken til bildet?"

#: queryunlinkimagedialog.ui
msgctxt ""
"queryunlinkimagedialog.ui\n"
"QueryUnlinkImageDialog\n"
"text\n"
"string.text"
msgid "This image is linked to a document."
msgstr "Dette bildet er lenket til et dokument."

#: queryunlinkimagedialog.ui
msgctxt ""
"queryunlinkimagedialog.ui\n"
"QueryUnlinkImageDialog\n"
"secondary_text\n"
"string.text"
msgid "Do you want to unlink the image in order to edit it?"
msgstr "Vil du fjerne lenken til bildet for å kunne redigere det?"

#: tabledesigndialog.ui
msgctxt ""
"tabledesigndialog.ui\n"
"TableDesignDialog\n"
"title\n"
"string.text"
msgid "Table Design"
msgstr "Tabellutforming"

#: tabledesigndialog.ui
msgctxt ""
"tabledesigndialog.ui\n"
"UseFirstRowStyle\n"
"label\n"
"string.text"
msgid "_Header row"
msgstr "_Overskriftsrad"

#: tabledesigndialog.ui
msgctxt ""
"tabledesigndialog.ui\n"
"UseLastRowStyle\n"
"label\n"
"string.text"
msgid "Tot_al row"
msgstr "Totalrad"

#: tabledesigndialog.ui
msgctxt ""
"tabledesigndialog.ui\n"
"UseBandingRowStyle\n"
"label\n"
"string.text"
msgid "_Banded rows"
msgstr "_Radstriper"

#: tabledesigndialog.ui
msgctxt ""
"tabledesigndialog.ui\n"
"UseFirstColumnStyle\n"
"label\n"
"string.text"
msgid "Fi_rst column"
msgstr "Første kolonne"

#: tabledesigndialog.ui
msgctxt ""
"tabledesigndialog.ui\n"
"UseLastColumnStyle\n"
"label\n"
"string.text"
msgid "_Last column"
msgstr "_Siste kolonne"

#: tabledesigndialog.ui
msgctxt ""
"tabledesigndialog.ui\n"
"UseBandingColumnStyle\n"
"label\n"
"string.text"
msgid "Ba_nded columns"
msgstr "Kolonnestriper"

#: vectorize.ui
msgctxt ""
"vectorize.ui\n"
"VectorizeDialog\n"
"title\n"
"string.text"
msgid "Convert to Polygon"
msgstr "Gjør om til mangekant"

#: vectorize.ui
msgctxt ""
"vectorize.ui\n"
"preview\n"
"label\n"
"string.text"
msgid "Preview"
msgstr "Forhåndsvisning"

#: vectorize.ui
msgctxt ""
"vectorize.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Number of colors:"
msgstr "Antall farger:"

#: vectorize.ui
msgctxt ""
"vectorize.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "Point reduction:"
msgstr "Punktreduksjon:"

#: vectorize.ui
msgctxt ""
"vectorize.ui\n"
"tilesft\n"
"label\n"
"string.text"
msgid "Tile size:"
msgstr "Flisstørrelse:"

#: vectorize.ui
msgctxt ""
"vectorize.ui\n"
"fillholes\n"
"label\n"
"string.text"
msgid "_Fill holes"
msgstr "_Fyll hull"

#: vectorize.ui
msgctxt ""
"vectorize.ui\n"
"label5\n"
"label\n"
"string.text"
msgid "Source image:"
msgstr "Kildebilde:"

#: vectorize.ui
msgctxt ""
"vectorize.ui\n"
"label6\n"
"label\n"
"string.text"
msgid "Vectorized image:"
msgstr "Vektorbilde:"

#. extracted from shell/source/win32/shlxthandler/res
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-04-22 23:40+0200\n"
"PO-Revision-Date: 2015-05-19 08:53+0000\n"
"Last-Translator: Jihui <jihui.choi@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Pootle 2.5.1\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1432025631.000000\n"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%TITLE%\n"
"LngText.text"
msgid "Title"
msgstr "제목"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%TITLE_COLON%\n"
"LngText.text"
msgid "Title:"
msgstr "제목:"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%SUBJECT%\n"
"LngText.text"
msgid "Subject"
msgstr "주제"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%SUBJECT_COLON%\n"
"LngText.text"
msgid "Subject:"
msgstr "주제:"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%AUTHOR%\n"
"LngText.text"
msgid "Author"
msgstr "작성자"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%AUTHOR_COLON%\n"
"LngText.text"
msgid "Author:"
msgstr "작성자: "

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%KEYWORDS%\n"
"LngText.text"
msgid "Keywords"
msgstr "키워드"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%KEYWORDS_COLON%\n"
"LngText.text"
msgid "Keywords:"
msgstr "키워드:"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%COMMENTS%\n"
"LngText.text"
msgid "Comments"
msgstr "설명"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%COMMENTS_COLON%\n"
"LngText.text"
msgid "Comments:"
msgstr "설명:"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%PAGES%\n"
"LngText.text"
msgid "Pages"
msgstr "페이지"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%TABLES%\n"
"LngText.text"
msgid "Tables"
msgstr "표"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%GRAPHICS%\n"
"LngText.text"
msgid "Images"
msgstr "이미지"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%OBJECTS%\n"
"LngText.text"
msgid "Objects"
msgstr "개체"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%OLE_OBJECTS%\n"
"LngText.text"
msgid "OLE Objects"
msgstr "OLE 개체"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%PARAGRAPHS%\n"
"LngText.text"
msgid "Paragraphs"
msgstr "단락"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%WORDS%\n"
"LngText.text"
msgid "Words"
msgstr "단어"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%CHARACTERS%\n"
"LngText.text"
msgid "Characters"
msgstr "문자"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%ROWS%\n"
"LngText.text"
msgid "Lines"
msgstr "행"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%ORIGIN%\n"
"LngText.text"
msgid "Origin"
msgstr "소스"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%VERSION%\n"
"LngText.text"
msgid "Version"
msgstr "버전"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%SHEETS%\n"
"LngText.text"
msgid "Sheets"
msgstr "시트"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%CELLS%\n"
"LngText.text"
msgid "Cells"
msgstr "셀"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%STATISTICS_TITLE%\n"
"LngText.text"
msgid "Document Statistics"
msgstr "문서 통계"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%SUMMARY_TITLE%\n"
"LngText.text"
msgid "Summary"
msgstr "요약"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%PROPERTY%\n"
"LngText.text"
msgid "Property"
msgstr "속성"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%PROPERTY_VALUE%\n"
"LngText.text"
msgid "Value"
msgstr "값"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%MODIFIED%\n"
"LngText.text"
msgid "Modified"
msgstr "변경 날짜"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%MODIFIED_COLON%\n"
"LngText.text"
msgid "Modified:"
msgstr "변경 날짜:"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%DOCUMENT_NUMBER%\n"
"LngText.text"
msgid "Revision number"
msgstr "수정 횟수"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%DOCUMENT_NUMBER_COLON%\n"
"LngText.text"
msgid "Revision number:"
msgstr "수정 횟수:"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%EDITING_TIME%\n"
"LngText.text"
msgid "Total editing time"
msgstr "총 편집 시간"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%EDITING_TIME_COLON%\n"
"LngText.text"
msgid "Total editing time:"
msgstr "총 편집 시간:"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%DESCRIPTION%\n"
"LngText.text"
msgid "Description"
msgstr "설명"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%DESCRIPTION_COLON%\n"
"LngText.text"
msgid "Description:"
msgstr "설명:"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%SIZE_COLON%\n"
"LngText.text"
msgid "Size:"
msgstr "크기:"

#: shlxthdl.ulf
msgctxt ""
"shlxthdl.ulf\n"
"%TYPE_COLON%\n"
"LngText.text"
msgid "Type:"
msgstr "유형:"

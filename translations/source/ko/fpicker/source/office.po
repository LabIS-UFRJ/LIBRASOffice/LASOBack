#. extracted from fpicker/source/office
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-08-25 12:34+0200\n"
"PO-Revision-Date: 2015-06-23 20:58+0000\n"
"Last-Translator: Jihui Choi <jihui.choi@gmail.com>\n"
"Language-Team: \n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Pootle 2.7\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1435093139.000000\n"

#: OfficeFilePicker.src
msgctxt ""
"OfficeFilePicker.src\n"
"STR_SVT_FILEPICKER_AUTO_EXTENSION\n"
"string.text"
msgid "~Automatic file name extension"
msgstr "자동 파일 이름 확장자(~A)"

#: OfficeFilePicker.src
msgctxt ""
"OfficeFilePicker.src\n"
"STR_SVT_FILEPICKER_PASSWORD\n"
"string.text"
msgid "Save with pass~word"
msgstr "암호를 사용하여 저장(~W)"

#: OfficeFilePicker.src
msgctxt ""
"OfficeFilePicker.src\n"
"STR_SVT_FILEPICKER_FILTER_OPTIONS\n"
"string.text"
msgid "~Edit filter settings"
msgstr "필터 설정 편집(~E)"

#: OfficeFilePicker.src
msgctxt ""
"OfficeFilePicker.src\n"
"STR_SVT_FILEPICKER_READONLY\n"
"string.text"
msgid "~Read-only"
msgstr "읽기 전용(~R)"

#: OfficeFilePicker.src
msgctxt ""
"OfficeFilePicker.src\n"
"STR_SVT_FILEPICKER_INSERT_AS_LINK\n"
"string.text"
msgid "~Link"
msgstr "링크(~L)"

#: OfficeFilePicker.src
msgctxt ""
"OfficeFilePicker.src\n"
"STR_SVT_FILEPICKER_SHOW_PREVIEW\n"
"string.text"
msgid "Pr~eview"
msgstr "미리보기(~E)"

#: OfficeFilePicker.src
msgctxt ""
"OfficeFilePicker.src\n"
"STR_SVT_FILEPICKER_PLAY\n"
"string.text"
msgid "~Play"
msgstr "재생(~P)"

#: OfficeFilePicker.src
msgctxt ""
"OfficeFilePicker.src\n"
"STR_SVT_FILEPICKER_VERSION\n"
"string.text"
msgid "~Version:"
msgstr "버전(~V):"

#: OfficeFilePicker.src
msgctxt ""
"OfficeFilePicker.src\n"
"STR_SVT_FILEPICKER_TEMPLATES\n"
"string.text"
msgid "S~tyles:"
msgstr "스타일(~T):"

#: OfficeFilePicker.src
msgctxt ""
"OfficeFilePicker.src\n"
"STR_SVT_FILEPICKER_IMAGE_TEMPLATE\n"
"string.text"
msgid "Style:"
msgstr "스타일:"

#: OfficeFilePicker.src
msgctxt ""
"OfficeFilePicker.src\n"
"STR_SVT_FILEPICKER_SELECTION\n"
"string.text"
msgid "~Selection"
msgstr "선택(~S)"

#: OfficeFilePicker.src
msgctxt ""
"OfficeFilePicker.src\n"
"STR_SVT_FILEPICKER_FILTER_TITLE\n"
"string.text"
msgid "File ~type:"
msgstr "파일 유형(~T):"

#: OfficeFilePicker.src
msgctxt ""
"OfficeFilePicker.src\n"
"STR_SVT_FOLDERPICKER_DEFAULT_TITLE\n"
"string.text"
msgid "Select Path"
msgstr "경로 선택"

#: OfficeFilePicker.src
msgctxt ""
"OfficeFilePicker.src\n"
"STR_SVT_FOLDERPICKER_DEFAULT_DESCRIPTION\n"
"string.text"
msgid "Please select a folder."
msgstr "폴더를 선택하십시오."

#: iodlg.src
msgctxt ""
"iodlg.src\n"
"STR_EXPLORERFILE_OPEN\n"
"string.text"
msgid "Open"
msgstr "열기"

#: iodlg.src
msgctxt ""
"iodlg.src\n"
"STR_EXPLORERFILE_SAVE\n"
"string.text"
msgid "Save as"
msgstr "다른 이름으로 저장"

#: iodlg.src
msgctxt ""
"iodlg.src\n"
"STR_EXPLORERFILE_BUTTONSAVE\n"
"string.text"
msgid "~Save"
msgstr "저장(~S)"

#: iodlg.src
msgctxt ""
"iodlg.src\n"
"STR_PATHNAME\n"
"string.text"
msgid "~Path:"
msgstr "경로(~P):"

#: iodlg.src
msgctxt ""
"iodlg.src\n"
"STR_PATHSELECT\n"
"string.text"
msgid "Select path"
msgstr "경로 선택"

#: iodlg.src
msgctxt ""
"iodlg.src\n"
"STR_BUTTONSELECT\n"
"string.text"
msgid "~Select"
msgstr "선택(~S)"

#: iodlg.src
msgctxt ""
"iodlg.src\n"
"STR_ACTUALVERSION\n"
"string.text"
msgid "Current version"
msgstr "현재 버전"

#: iodlg.src
msgctxt ""
"iodlg.src\n"
"STR_PREVIEW\n"
"string.text"
msgid "File Preview"
msgstr "파일 미리보기"

#: iodlg.src
msgctxt ""
"iodlg.src\n"
"STR_DEFAULT_DIRECTORY\n"
"string.text"
msgid "My Documents"
msgstr "내 문서"

#: iodlg.src
msgctxt ""
"iodlg.src\n"
"STR_PLACES_TITLE\n"
"string.text"
msgid "Places"
msgstr "장소"

#: iodlg.src
msgctxt ""
"iodlg.src\n"
"RID_FILEOPEN_NOTEXISTENTFILE\n"
"string.text"
msgid ""
"The file $name$ does not exist.\n"
"Make sure you have entered the correct file name."
msgstr ""
"파일 $name$ 이(가) 존재하지 않습니다.\n"
"올바른 파일 이름을 지정했는지 확인하십시오."

#: iodlg.src
msgctxt ""
"iodlg.src\n"
"STR_FILTERNAME_ALL\n"
"string.text"
msgid "All files"
msgstr "모든 파일"

#: iodlg.src
msgctxt ""
"iodlg.src\n"
"STR_SVT_ALREADYEXISTOVERWRITE\n"
"string.text"
msgid ""
"A file named \"$filename$\" already exists.\n"
"\n"
"Do you want to replace it?"
msgstr ""
"\"$filename$\"라는 이름을 가진 파일이 이미 있습니다\n"
"\n"
"덮어쓰겠습니까?"

#: iodlg.src
msgctxt ""
"iodlg.src\n"
"STR_SVT_DELETESERVICE\n"
"string.text"
msgid ""
"Are you sure you want to delete the service?\n"
"\"$servicename$\""
msgstr ""

#: iodlg.src
msgctxt ""
"iodlg.src\n"
"STR_SVT_ROOTLABEL\n"
"string.text"
msgid "Root"
msgstr ""

#: iodlg.src
msgctxt ""
"iodlg.src\n"
"STR_SVT_NEW_FOLDER\n"
"string.text"
msgid "Folder"
msgstr "폴더"

#: iodlg.src
msgctxt ""
"iodlg.src\n"
"STR_SVT_NOREMOVABLEDEVICE\n"
"string.text"
msgid ""
"No removable storage device detected.\n"
"Make sure it is plugged in properly and try again."
msgstr ""
"이동식 저장 장치를 찾을 수 없습니다.\n"
"장치가 올바르게 플러그인되었는지 확인한 후 다시 시도하십시오."

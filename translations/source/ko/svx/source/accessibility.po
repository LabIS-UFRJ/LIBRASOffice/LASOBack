#. extracted from svx/source/accessibility
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.freedesktop.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-05-23 12:06+0200\n"
"PO-Revision-Date: 2011-04-05 23:45+0200\n"
"Last-Translator: Andras <timar74@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_3D_MATERIAL_COLOR\n"
"string.text"
msgid "3D material color"
msgstr "3D 재료 색상"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_TEXT_COLOR\n"
"string.text"
msgid "Font color"
msgstr "글꼴 색상"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_BACKGROUND_COLOR\n"
"string.text"
msgid "Background color"
msgstr "배경 색상"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_FILLSTYLE_NONE\n"
"string.text"
msgid "None"
msgstr "없음"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_FILLSTYLE_SOLID\n"
"string.text"
msgid "Solid"
msgstr "채움"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_FILLSTYLE_HATCH\n"
"string.text"
msgid "With hatching"
msgstr "해칭"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_FILLSTYLE_GRADIENT\n"
"string.text"
msgid "Gradient"
msgstr "그라디언트"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_FILLSTYLE_BITMAP\n"
"string.text"
msgid "Bitmap"
msgstr "비트맵"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_WITH\n"
"string.text"
msgid "with"
msgstr "설정 대상"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_STYLE\n"
"string.text"
msgid "Style"
msgstr "스타일"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_AND\n"
"string.text"
msgid "and"
msgstr "와(과)"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CORN_NAME\n"
"string.text"
msgid "Corner control"
msgstr "모서리 콘트롤"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CORN_DESCR\n"
"string.text"
msgid "Selection of a corner point."
msgstr "모서리점 선택"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_ANGL_NAME\n"
"string.text"
msgid "Angle control"
msgstr "각도 콘트롤"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_ANGL_DESCR\n"
"string.text"
msgid "Selection of a major angle."
msgstr "중심각 선택"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_LT\n"
"string.text"
msgid "Top left"
msgstr "왼쪽 위"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_MT\n"
"string.text"
msgid "Top middle"
msgstr "위 가운데"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_RT\n"
"string.text"
msgid "Top right"
msgstr "오른쪽 위"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_LM\n"
"string.text"
msgid "Left center"
msgstr "왼쪽 중간"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_MM\n"
"string.text"
msgid "Center"
msgstr "가운데"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_RM\n"
"string.text"
msgid "Right center"
msgstr "오른쪽 중간"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_LB\n"
"string.text"
msgid "Bottom left"
msgstr "왼쪽 아래"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_MB\n"
"string.text"
msgid "Bottom middle"
msgstr "아래 가운데"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_RB\n"
"string.text"
msgid "Bottom right"
msgstr "오른쪽 아래"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A000\n"
"string.text"
msgid "0 degrees"
msgstr "0 도"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A045\n"
"string.text"
msgid "45 degrees"
msgstr "45 도"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A090\n"
"string.text"
msgid "90 degrees"
msgstr "90 도"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A135\n"
"string.text"
msgid "135 degrees"
msgstr "135 도"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A180\n"
"string.text"
msgid "180 degrees"
msgstr "180 도"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A225\n"
"string.text"
msgid "225 degrees"
msgstr "225 도"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A270\n"
"string.text"
msgid "270 degrees"
msgstr "270 도"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A315\n"
"string.text"
msgid "315 degrees"
msgstr "315 도"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_GRAPHCTRL_ACC_NAME\n"
"string.text"
msgid "Contour control"
msgstr "윤곽 콘트롤"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_GRAPHCTRL_ACC_DESCRIPTION\n"
"string.text"
msgid "This is where you can edit the contour."
msgstr "여기에서 윤곽을 편집할 수 있습니다."

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_CHARACTER_SELECTION\n"
"string.text"
msgid "Special character selection"
msgstr "기호 및 특수 문자 선택"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_CHAR_SEL_DESC\n"
"string.text"
msgid "Select special characters in this area."
msgstr "이 영역에서 기호 및 특수 문자를 선택하십시오."

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_CHARACTER_CODE\n"
"string.text"
msgid "Character code "
msgstr "문자 코드"

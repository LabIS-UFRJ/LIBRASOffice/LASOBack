#. extracted from dbaccess/source/ui/relationdesign
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2014-11-18 11:23+0100\n"
"PO-Revision-Date: 2011-04-05 19:01+0200\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: bo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"

#: relation.src
msgctxt ""
"relation.src\n"
"STR_QUERY_REL_EDIT_RELATION\n"
"string.text"
msgid "This relation already exists. Do you want to edit it or create a new one?"
msgstr "འབྲེལ་བ་འདི་གནས་ཟིན་པས་ཁྱེད་ཀྱིས་འདིར་རྩོམ་སྒྲིག་བྱེད་སམ་ཡང་ན་འབྲེལ་བ་གསར་པ་ཞིག་གཏོད་དམ།"

#: relation.src
msgctxt ""
"relation.src\n"
"STR_QUERY_REL_EDIT\n"
"string.text"
msgid "Edit..."
msgstr "རིམ་འདས་ཕྲེང་སྦྲེལ་རྩོམ་སྒྲིག"

#: relation.src
msgctxt ""
"relation.src\n"
"STR_QUERY_REL_CREATE\n"
"string.text"
msgid "Create..."
msgstr "%1གསར་འཛུགས།"

#: relation.src
msgctxt ""
"relation.src\n"
"STR_RELATIONDESIGN\n"
"string.text"
msgid " - %PRODUCTNAME Base: Relation design"
msgstr " - %PRODUCTNAME Base:འབྲེལ་བ་ཇུས་འགོད།"

#: relation.src
msgctxt ""
"relation.src\n"
"STR_RELATIONDESIGN_NOT_AVAILABLE\n"
"string.text"
msgid "The database does not support relations."
msgstr "གཞི་གྲངས་མཛོད་དེས་འབྲེལ་བའི་བྱེད་ནུས་ལ་རམ་འདེགས་བྱེད་ཀྱི་མེད།"

#: relation.src
msgctxt ""
"relation.src\n"
"STR_QUERY_REL_DELETE_WINDOW\n"
"string.text"
msgid "When you delete this table all corresponding relations will be deleted as well. Continue?"
msgstr "གལ་སྲིད་རེའུ་མིག་འདི་སུབ་ན་དེ་དང་འབྲེལ་ཡོད་ཚང་མ་སུབ་ཀྱི་རེད། མུ་མཐུད་ཡིན་ནམ།"

#: relation.src
msgctxt ""
"relation.src\n"
"STR_QUERY_REL_COULD_NOT_CREATE\n"
"string.text"
msgid ""
"The database could not create the relation. Maybe foreign keys for this kind of table aren't supported.\n"
"Please check your documentation of the database."
msgstr ""

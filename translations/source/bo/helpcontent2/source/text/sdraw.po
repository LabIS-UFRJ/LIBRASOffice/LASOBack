#. extracted from helpcontent2/source/text/sdraw
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-05-02 10:14+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: bo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1462184070.000000\n"

#: main0000.xhp
msgctxt ""
"main0000.xhp\n"
"tit\n"
"help.text"
msgid "Welcome to the $[officename] Draw Help"
msgstr "$[officename] རིས་འགོད་བཟོ་བའི་རོགས་རམ་བེད་སྤྱོད་མཛད་པར་དགའ་བསུ་ཞུ་"

#: main0000.xhp
msgctxt ""
"main0000.xhp\n"
"hd_id3155960\n"
"help.text"
msgid "Welcome to the $[officename] Draw Help"
msgstr ""

#: main0000.xhp
msgctxt ""
"main0000.xhp\n"
"hd_id3154022\n"
"3\n"
"help.text"
msgid "How to Work With $[officename] Draw"
msgstr "ཇི་ལྟར་བྱས་ན་$[officename] རིས་འགོད་བཟོ་བ་ཡོད་པ་"

#: main0000.xhp
msgctxt ""
"main0000.xhp\n"
"hd_id3150363\n"
"5\n"
"help.text"
msgid "$[officename] Draw Menus, Toolbars, and Keys"
msgstr "$[officename] རིས་འགོད་བཟོ་བའི་ཚལ་ཐོ་ ཡོ་བྱད་ཚང་དང་མྱུར་བདེའི་མཐེབ་"

#: main0000.xhp
msgctxt ""
"main0000.xhp\n"
"hd_id3166430\n"
"4\n"
"help.text"
msgid "Help about the Help"
msgstr "རོགས་རམ་སྣེ་སྟོན་"

#: main0100.xhp
msgctxt ""
"main0100.xhp\n"
"tit\n"
"help.text"
msgid "Menus"
msgstr "ཚལ་ཐོ་"

#: main0100.xhp
msgctxt ""
"main0100.xhp\n"
"hd_id3148664\n"
"1\n"
"help.text"
msgid "<variable id=\"main0100\"><link href=\"text/sdraw/main0100.xhp\" name=\"Menus\">Menus</link></variable>"
msgstr "<variable id=\"main0100\"><link href=\"text/sdraw/main0100.xhp\" name=\"ཚལ་ཐོ་\">ཚལ་ཐོ་</link></variable>"

#: main0100.xhp
msgctxt ""
"main0100.xhp\n"
"par_id3154684\n"
"2\n"
"help.text"
msgid "The following is a description of all $[officename] Draw menus, submenus and their dialogs."
msgstr "གཤམ་གསལ་ནི་$[officename] རིས་འགོད་བཟོ་བའི་ཚལ་ཐོ་ ཡན་ལག་ཚལ་ཐོ་དང་གླེང་སྒྲོམ་ཡོད་ཚད་ཀྱི་གསལ་བཤད་ཡིན།"

#: main0101.xhp
msgctxt ""
"main0101.xhp\n"
"tit\n"
"help.text"
msgid "File"
msgstr "ཡིག་ཆ་"

#: main0101.xhp
msgctxt ""
"main0101.xhp\n"
"hd_id3149655\n"
"1\n"
"help.text"
msgid "<link href=\"text/sdraw/main0101.xhp\" name=\"File\">File</link>"
msgstr "<link href=\"text/sdraw/main0101.xhp\" name=\"ཡིག་ཆ་\">ཡིག་ཆ་</link>"

#: main0101.xhp
msgctxt ""
"main0101.xhp\n"
"par_id3150868\n"
"2\n"
"help.text"
msgid "This menu contains general commands for working with Draw documents, such as open, close and print. To close $[officename] Draw, click <emph>Exit</emph>."
msgstr "ཚལ་ཐོ་འདིར་རིས་མཚོན་ཡིག་ཚགས་སྤྱོད་པའི་རྒྱུན་སྲོལ་བཀའ་ཚིག་འདུས། དཔེར་ན་ ཁ་ཕྱེ་ ཁ་རྒྱག་དང་གཏག་པར་ལྟ་བུ། གལ་ཏེ་$[officename] རིས་འགོད་བཟོ་བ་ཁ་རྒྱག་དགོས་ན་<emph>[མཇུག་བསྡུ་]</emph>ལ་རྐྱང་རྡེབ་བྱས་པས་འཐུས་སོ།"

#: main0101.xhp
msgctxt ""
"main0101.xhp\n"
"hd_id3156441\n"
"4\n"
"help.text"
msgid "<link href=\"text/shared/01/01020000.xhp\" name=\"Open\">Open</link>"
msgstr "<link href=\"text/shared/01/01020000.xhp\" name=\"ཁ་ཕྱེ་\">ཁ་ཕྱེ་</link>"

#: main0101.xhp
msgctxt ""
"main0101.xhp\n"
"hd_id3153876\n"
"6\n"
"help.text"
msgid "<link href=\"text/shared/01/01070000.xhp\" name=\"Save As\">Save As</link>"
msgstr "<link href=\"text/shared/01/01070000.xhp\" name=\"གཞན་ཉར་\">གཞན་ཉར་</link>"

#: main0101.xhp
msgctxt ""
"main0101.xhp\n"
"hd_id3150718\n"
"7\n"
"help.text"
msgid "<link href=\"text/simpress/01/01170000.xhp\" name=\"Export\">Export</link>"
msgstr "<link href=\"text/simpress/01/01170000.xhp\" name=\"ཕྱིར་བཏོན་\">ཕྱིར་བཏོན་</link>"

#: main0101.xhp
msgctxt ""
"main0101.xhp\n"
"hd_id3154754\n"
"14\n"
"help.text"
msgid "<link href=\"text/shared/01/01190000.xhp\" name=\"Versions\">Versions</link>"
msgstr "<link href=\"text/shared/01/01190000.xhp\" name=\"པར་གཞི་\">པར་གཞི་</link>"

#: main0101.xhp
msgctxt ""
"main0101.xhp\n"
"hd_id3150044\n"
"9\n"
"help.text"
msgid "<link href=\"text/shared/01/01100000.xhp\" name=\"Properties\">Properties</link>"
msgstr "<link href=\"text/shared/01/01100000.xhp\" name=\"གཏོགས་གཤིས་\">གཏོགས་གཤིས་</link>"

#: main0101.xhp
msgctxt ""
"main0101.xhp\n"
"hd_id3149127\n"
"12\n"
"help.text"
msgid "<link href=\"text/shared/01/01130000.xhp\" name=\"Print\">Print</link>"
msgstr "<link href=\"text/shared/01/01130000.xhp\" name=\"གཏག་པར་\">གཏག་པར་</link>"

#: main0101.xhp
msgctxt ""
"main0101.xhp\n"
"hd_id3145790\n"
"13\n"
"help.text"
msgid "<link href=\"text/shared/01/01140000.xhp\" name=\"Printer Settings\">Printer Settings</link>"
msgstr "<link href=\"text/shared/01/01140000.xhp\" name=\"གཏག་པར་ཆས་ཀྱི་བཀོད་སྒྲིག་\">གཏག་པར་ཆས་ཀྱི་བཀོད་སྒྲིག་</link>"

#: main0102.xhp
msgctxt ""
"main0102.xhp\n"
"tit\n"
"help.text"
msgid "Edit"
msgstr "རྩོམ་སྒྲིག་"

#: main0102.xhp
msgctxt ""
"main0102.xhp\n"
"hd_id3150868\n"
"1\n"
"help.text"
msgid "<link href=\"text/sdraw/main0102.xhp\" name=\"Edit\">Edit</link>"
msgstr "<link href=\"text/sdraw/main0102.xhp\" name=\"རྩོམ་སྒྲིག་\">རྩོམ་སྒྲིག་</link>"

#: main0102.xhp
msgctxt ""
"main0102.xhp\n"
"par_id3146974\n"
"2\n"
"help.text"
msgid "The commands in this menu are used to edit Draw documents (for example, copying and pasting)."
msgstr "ཚལ་ཐོ་འདིའི་ནང་གི་བཀའ་ཚིག་དེ་རིས་མཚོན་ཡིག་ཚགས་རྩོམ་སྒྲིག་ལ་སྤྱོད།(དཔེར་ན་འདྲ་ཕབ་དང་སྦྱར་བ།)"

#: main0102.xhp
msgctxt ""
"main0102.xhp\n"
"hd_id3147396\n"
"3\n"
"help.text"
msgid "<link href=\"text/shared/01/02070000.xhp\" name=\"Paste Special\">Paste Special</link>"
msgstr "<link href=\"text/shared/01/02070000.xhp\" name=\"འདེམས་པའི་རང་བཞིན་གྱི་སྦྱར་བ་\">འདེམས་པའི་རང་བཞིན་གྱི་སྦྱར་བ་</link>"

#: main0102.xhp
msgctxt ""
"main0102.xhp\n"
"hd_id3149400\n"
"4\n"
"help.text"
msgid "<link href=\"text/shared/01/02100000.xhp\" name=\"Find & Replace\">Find & Replace</link>"
msgstr "<link href=\"text/shared/01/02100000.xhp\" name=\"བཙལ་འཚོལ་དང་ཚབ་བརྗེ་\">བཙལ་འཚོལ་དང་ཚབ་བརྗེ་</link>"

#: main0102.xhp
msgctxt ""
"main0102.xhp\n"
"hd_id3153713\n"
"13\n"
"help.text"
msgid "<link href=\"text/shared/01/05270000.xhp\" name=\"Points\">Points</link>"
msgstr "<link href=\"text/shared/01/05270000.xhp\" name=\"རྩེ་ཚེག་རྩོམ་སྒྲིག་\">རྩེ་ཚེག་རྩོམ་སྒྲིག་</link>"

#: main0102.xhp
msgctxt ""
"main0102.xhp\n"
"par_id3147340\n"
"14\n"
"help.text"
msgid "Enables you to edit points on your drawing."
msgstr "རིས་མཚོན་བྱ་སྐབས་རྩེ་ཚེག་རྩོམ་སྒྲིག་ལ་སྤྱོད།"

#: main0102.xhp
msgctxt ""
"main0102.xhp\n"
"hd_id3149258\n"
"15\n"
"help.text"
msgid "<link href=\"text/simpress/02/10030200.xhp\" name=\"Glue points\">Glue points</link>"
msgstr "<link href=\"text/simpress/02/10030200.xhp\" name=\"སྦྱར་མདུད་ཚེག་\">སྦྱར་མདུད་ཚེག་</link>"

#: main0102.xhp
msgctxt ""
"main0102.xhp\n"
"par_id3146315\n"
"16\n"
"help.text"
msgid "Enables you to edit glue points on your drawing."
msgstr "རིས་མཚོན་བྱ་སྐབས་སྦྱར་མདུད་ཚེག་རྩོམ་སྒྲིག་བྱ་བར་སྤྱོད།"

#: main0102.xhp
msgctxt ""
"main0102.xhp\n"
"hd_id3147005\n"
"5\n"
"help.text"
msgid "<link href=\"text/simpress/01/02120000.xhp\" name=\"Duplicate\">Duplicate</link>"
msgstr "<link href=\"text/simpress/01/02120000.xhp\" name=\"འདྲ་ཕབ་བྱ་ཡུལ་\">འདྲ་ཕབ་བྱ་ཡུལ་</link>"

#: main0102.xhp
msgctxt ""
"main0102.xhp\n"
"hd_id3150205\n"
"6\n"
"help.text"
msgid "<link href=\"text/simpress/01/02150000.xhp\" name=\"Cross-fading\">Cross-fading</link>"
msgstr "<link href=\"text/simpress/01/02150000.xhp\" name=\"པར་ངོས་བརྗེ་རེས་\">པར་ངོས་བརྗེ་རེས་</link>"

#: main0102.xhp
msgctxt ""
"main0102.xhp\n"
"hd_id3154650\n"
"7\n"
"help.text"
msgid "<link href=\"text/simpress/01/02160000.xhp\" name=\"Fields\">Fields</link>"
msgstr "<link href=\"text/simpress/01/02160000.xhp\" name=\"ཁོངས་བཀའ་བརྡ་\">ཁོངས་བཀའ་བརྡ་</link>"

#: main0102.xhp
msgctxt ""
"main0102.xhp\n"
"hd_id3156446\n"
"10\n"
"help.text"
msgid "<link href=\"text/shared/01/02180000.xhp\" name=\"Links\">Links</link>"
msgstr "<link href=\"text/shared/01/02180000.xhp\" name=\"ཐག་སྦྲེལ་\">ཐག་སྦྲེལ་</link>"

#: main0102.xhp
msgctxt ""
"main0102.xhp\n"
"hd_id3148699\n"
"11\n"
"help.text"
msgid "<link href=\"text/shared/01/02220000.xhp\" name=\"ImageMap\">ImageMap</link>"
msgstr "<link href=\"text/shared/01/02220000.xhp\" name=\"བཪྙན་རིས་རྟེན་འཕྲོ་\">བཪྙན་རིས་རྟེན་འཕྲོ་</link>"

#: main0102.xhp
msgctxt ""
"main0102.xhp\n"
"hd_id3157867\n"
"12\n"
"help.text"
msgid "<link href=\"text/shared/02/09070000.xhp\" name=\"Hyperlink\">Hyperlink</link>"
msgstr "<link href=\"text/shared/02/09070000.xhp\" name=\"ཆེས་ཐག་སྦྲེལ་\">ཆེས་ཐག་སྦྲེལ་</link>"

#: main0103.xhp
msgctxt ""
"main0103.xhp\n"
"tit\n"
"help.text"
msgid "View"
msgstr "མཐོང་རིས་"

#: main0103.xhp
#, fuzzy
msgctxt ""
"main0103.xhp\n"
"hd_id3152576\n"
"help.text"
msgid "<link href=\"text/sdraw/main0103.xhp\" name=\"View\">View</link>"
msgstr "<link href=\"text/sdraw/main0103.xhp\" name=\"མཐོང་རིས་\">མཐོང་རིས་</link>"

#: main0103.xhp
#, fuzzy
msgctxt ""
"main0103.xhp\n"
"par_id3159155\n"
"help.text"
msgid "Sets the display properties of Draw documents."
msgstr "རིས་མཚོན་ཡིག་ཚགས་ཀྱི་མངོན་པའི་སྡུར་ཚད་བཀོད་སྒྲིག་བྱེད།"

#: main0103.xhp
msgctxt ""
"main0103.xhp\n"
"par_idN105AB\n"
"help.text"
msgid "Normal"
msgstr "ཤོག་ངོས་"

#: main0103.xhp
msgctxt ""
"main0103.xhp\n"
"par_idN105AF\n"
"help.text"
msgid "Switch to normal view of the page."
msgstr ""

#: main0103.xhp
msgctxt ""
"main0103.xhp\n"
"par_idN105B2\n"
"help.text"
msgid "Master"
msgstr "མ་པར་"

#: main0103.xhp
msgctxt ""
"main0103.xhp\n"
"par_idN105B6\n"
"help.text"
msgid "Switch to the master page view."
msgstr ""

#: main0103.xhp
#, fuzzy
msgctxt ""
"main0103.xhp\n"
"hd_id3149666\n"
"help.text"
msgid "<link href=\"text/shared/01/03010000.xhp\" name=\"Zoom\">Zoom</link>"
msgstr "<link href=\"text/shared/01/03010000.xhp\" name=\"སྡུར་ཚད་མངོན་པ་\">སྡུར་ཚད་མངོན་པ་</link>"

#: main0104.xhp
msgctxt ""
"main0104.xhp\n"
"tit\n"
"help.text"
msgid "Insert"
msgstr "བསྒར་འཛུད་"

#: main0104.xhp
msgctxt ""
"main0104.xhp\n"
"hd_id3148797\n"
"1\n"
"help.text"
msgid "<link href=\"text/sdraw/main0104.xhp\" name=\"Insert\">Insert</link>"
msgstr "<link href=\"text/sdraw/main0104.xhp\" name=\"བསྒར་འཛུད་\">བསྒར་འཛུད་</link>"

#: main0104.xhp
msgctxt ""
"main0104.xhp\n"
"par_id3153770\n"
"2\n"
"help.text"
msgid "This menu allows you to insert elements, such as graphics and guides, into Draw documents."
msgstr "ཚལ་ཐོ་འདིས་ཁྱེད་ཀྱིས་གཞི་རྒྱུ་(དཔེར་ན་རིས་དབྱིབས་དང་རམ་འདེགས་ཐིག་)རིས་མཚོན་ཡིག་ཚགས་ནང་བསྒར་འཛུད་བྱ་རུ་འཇུག"

#: main0104.xhp
msgctxt ""
"main0104.xhp\n"
"hd_id3154320\n"
"3\n"
"help.text"
msgid "<link href=\"text/sdraw/01/04010000.xhp\" name=\"Slide\">Slide</link>"
msgstr "<link href=\"text/sdraw/01/04010000.xhp\" name=\"ཤོག་ངོས་གསར་པ་\">ཤོག་ངོས་གསར་པ་</link>"

#: main0104.xhp
msgctxt ""
"main0104.xhp\n"
"hd_id3146974\n"
"4\n"
"help.text"
msgid "<link href=\"text/simpress/01/04020000.xhp\" name=\"Layer\">Layer</link>"
msgstr "<link href=\"text/simpress/01/04020000.xhp\" name=\"རིམ་དབྱེ་བསྒར་འཛུད་\">རིམ་དབྱེ་བསྒར་འཛུད་</link>"

#: main0104.xhp
msgctxt ""
"main0104.xhp\n"
"hd_id3147397\n"
"5\n"
"help.text"
msgid "<link href=\"text/simpress/01/04030000.xhp\" name=\"Insert Snap Point/Line\">Insert Snap Point/Line</link>"
msgstr "<link href=\"text/simpress/01/04030000.xhp\" name=\"གནས་ཚད་ཚེག་/ཐིག་\">གནས་ཚད་ཚེག་/ཐིག་</link>"

#: main0104.xhp
msgctxt ""
"main0104.xhp\n"
"hd_id0915200910361385\n"
"help.text"
msgid "<link href=\"text/shared/01/04050000.xhp\" name=\"Comment\">Comment</link>"
msgstr "<link href=\"text/shared/01/04050000.xhp\" name=\"དཔྱད་མཆན་\">དཔྱད་མཆན་</link>"

#: main0104.xhp
msgctxt ""
"main0104.xhp\n"
"hd_id3154018\n"
"6\n"
"help.text"
msgid "<link href=\"text/shared/01/04100000.xhp\" name=\"Special Character\">Special Character</link>"
msgstr "<link href=\"text/shared/01/04100000.xhp\" name=\"མཚོན་རྟགས་\">མཚོན་རྟགས་</link>"

#: main0104.xhp
msgctxt ""
"main0104.xhp\n"
"hd_id3150749\n"
"11\n"
"help.text"
msgid "<link href=\"text/shared/02/09070000.xhp\" name=\"Hyperlink\">Hyperlink</link>"
msgstr "<link href=\"text/shared/02/09070000.xhp\" name=\"ཆེས་ཐག་སྦྲེལ་\">ཆེས་ཐག་སྦྲེལ་</link>"

#: main0104.xhp
msgctxt ""
"main0104.xhp\n"
"hd_id3156385\n"
"7\n"
"help.text"
msgid "<link href=\"text/simpress/01/04080100.xhp\" name=\"Table\">Table</link>"
msgstr "<link href=\"text/simpress/01/04080100.xhp\" name=\"ལས་ཁྲ་\">ལས་ཁྲ་</link>"

#: main0104.xhp
msgctxt ""
"main0104.xhp\n"
"hd_id3147003\n"
"8\n"
"help.text"
msgid "<link href=\"text/schart/01/wiz_chart_type.xhp\" name=\"Chart\">Chart</link>"
msgstr ""

#: main0104.xhp
msgctxt ""
"main0104.xhp\n"
"par_id0302200904020595\n"
"help.text"
msgid "Inserts a chart."
msgstr ""

#: main0104.xhp
msgctxt ""
"main0104.xhp\n"
"hd_id3155111\n"
"9\n"
"help.text"
msgid "<link href=\"text/shared/01/04160500.xhp\" name=\"Floating Frame\">Floating Frame</link>"
msgstr "<link href=\"text/shared/01/04160500.xhp\" name=\"སྒྲོམ་བྱ་ཡུལ་\">སྒྲོམ་བྱ་ཡུལ་</link>"

#: main0104.xhp
msgctxt ""
"main0104.xhp\n"
"hd_id3157867\n"
"10\n"
"help.text"
msgid "<link href=\"text/simpress/01/04110000.xhp\" name=\"File\">File</link>"
msgstr "<link href=\"text/simpress/01/04110000.xhp\" name=\"ཡིག་ཆ་\">ཡིག་ཆ་</link>"

#: main0105.xhp
msgctxt ""
"main0105.xhp\n"
"tit\n"
"help.text"
msgid "Format"
msgstr "རྣམ་གཞག་"

#: main0105.xhp
msgctxt ""
"main0105.xhp\n"
"hd_id3153770\n"
"1\n"
"help.text"
msgid "<link href=\"text/sdraw/main0105.xhp\" name=\"Format\">Format</link>"
msgstr "<link href=\"text/sdraw/main0105.xhp\" name=\"རྣམ་གཞག་\">རྣམ་གཞག་</link>"

#: main0105.xhp
msgctxt ""
"main0105.xhp\n"
"par_id3152578\n"
"2\n"
"help.text"
msgid "Contains commands for formatting the layout and the contents of your document."
msgstr "རྣམ་གཞག་ཅན་ཡིག་ཚགས་པར་དབྱིབས་དང་ནང་དོན་ལ་སྤྱོད་པའི་བཀའ་ཚིག་འདུས་ཡོད།"

#: main0105.xhp
msgctxt ""
"main0105.xhp\n"
"hd_id3155111\n"
"10\n"
"help.text"
msgid "<link href=\"text/shared/01/05020000.xhp\" name=\"Character\">Character</link>"
msgstr "<link href=\"text/shared/01/05020000.xhp\" name=\"ཡིག་གཟུགས་\">ཡིག་གཟུགས་</link>"

#: main0105.xhp
msgctxt ""
"main0105.xhp\n"
"hd_id3146979\n"
"12\n"
"help.text"
msgid "<link href=\"text/shared/01/05030000.xhp\" name=\"Paragraph\">Paragraph</link>"
msgstr "<link href=\"text/shared/01/05030000.xhp\" name=\"དུམ་མཚམས་\">དུམ་མཚམས་</link>"

#: main0105.xhp
msgctxt ""
"main0105.xhp\n"
"hd_id3166426\n"
"19\n"
"help.text"
msgid "<link href=\"text/shared/01/06050000.xhp\" name=\"Numbering/Bullets\">Bullets and Numbering</link>"
msgstr "<link href=\"text/shared/01/06050000.xhp\" name=\"རྣམ་གྲངས་མཚོན་རྟགས་དང་ཨང་སྒྲིག་\">རྣམ་གྲངས་མཚོན་རྟགས་དང་ཨང་སྒྲིག་</link>"

#: main0105.xhp
msgctxt ""
"main0105.xhp\n"
"hd_id3155091\n"
"14\n"
"help.text"
msgid "<link href=\"text/simpress/01/01180000.xhp\" name=\"Page\">Page</link>"
msgstr "<link href=\"text/simpress/01/01180000.xhp\" name=\"ཤོག་ངོས་\">ཤོག་ངོས་</link>"

#: main0105.xhp
msgctxt ""
"main0105.xhp\n"
"hd_id3146971\n"
"6\n"
"help.text"
msgid "<link href=\"text/shared/01/05230000.xhp\" name=\"Position and Size\">Position and Size</link>"
msgstr "<link href=\"text/shared/01/05230000.xhp\" name=\"གནས་ས་དང་ཆེ་ཆུང་\">གནས་ས་དང་ཆེ་ཆུང་</link>"

#: main0105.xhp
msgctxt ""
"main0105.xhp\n"
"hd_id3148576\n"
"3\n"
"help.text"
msgid "<link href=\"text/shared/01/05200000.xhp\" name=\"Line\">Line</link>"
msgstr "<link href=\"text/shared/01/05200000.xhp\" name=\"ཐིག་སྐུད་གཏོགས་གཤིས་\">ཐིག་སྐུད་གཏོགས་གཤིས་</link>"

#: main0105.xhp
msgctxt ""
"main0105.xhp\n"
"hd_id3151076\n"
"4\n"
"help.text"
msgid "<link href=\"text/shared/01/05210000.xhp\" name=\"Area\">Area</link>"
msgstr "<link href=\"text/shared/01/05210000.xhp\" name=\"སྐོང་གསབ་བཟོ་ལྟ་\">སྐོང་གསབ་བཟོ་ལྟ་</link>"

#: main0105.xhp
msgctxt ""
"main0105.xhp\n"
"hd_id3153878\n"
"5\n"
"help.text"
msgid "<link href=\"text/shared/01/05990000.xhp\" name=\"Text\">Text</link>"
msgstr "<link href=\"text/shared/01/05990000.xhp\" name=\"ཡི་གེ་\">ཡི་གེ་</link>"

#: main0105.xhp
msgctxt ""
"main0105.xhp\n"
"hd_id3153913\n"
"16\n"
"help.text"
msgid "<link href=\"text/simpress/01/05140000.xhp\" name=\"Layer\">Layer</link>"
msgstr "<link href=\"text/simpress/01/05140000.xhp\" name=\"རིམ་དབྱེ་\">རིམ་དབྱེ་</link>"

#: main0106.xhp
msgctxt ""
"main0106.xhp\n"
"tit\n"
"help.text"
msgid "Tools"
msgstr "ཡོ་བྱད་"

#: main0106.xhp
msgctxt ""
"main0106.xhp\n"
"hd_id3159155\n"
"1\n"
"help.text"
msgid "<link href=\"text/sdraw/main0106.xhp\" name=\"Tools\">Tools</link>"
msgstr "<link href=\"text/sdraw/main0106.xhp\" name=\"ཡོ་བྱད་\">ཡོ་བྱད་</link>"

#: main0106.xhp
msgctxt ""
"main0106.xhp\n"
"par_id3156443\n"
"2\n"
"help.text"
msgid "This menu provides tools for $[officename] Draw as well as access to language and system settings."
msgstr "ཚལ་ཐོ་འདིས་$[officename] རིས་མཚོན་བཟོ་བར་སྤྱོད་པའི་ཡོ་བྱད་དང་དེ་བཞིན་སྐད་བརྡ་དང་མ་ལག་བཀོད་སྒྲིག་གི་འཚམས་འདྲི་མཁོ་འདོན་བྱེད།"

#: main0106.xhp
msgctxt ""
"main0106.xhp\n"
"hd_id3153415\n"
"4\n"
"help.text"
msgid "<link href=\"text/shared/01/06040000.xhp\" name=\"AutoCorrect\">AutoCorrect Options</link>"
msgstr "<link href=\"text/shared/01/06040000.xhp\" name=\"རང་འགུལ་དག་བཅོས་འདེམས་གཞི་\">རང་འགུལ་དག་བཅོས་འདེམས་གཞི་</link>"

#: main0106.xhp
msgctxt ""
"main0106.xhp\n"
"hd_id3150044\n"
"6\n"
"help.text"
msgid "<link href=\"text/shared/01/06140000.xhp\" name=\"Customize\">Customize</link>"
msgstr "<link href=\"text/shared/01/06140000.xhp\" name=\"རང་མཚན་འཇོག་\">རང་མཚན་འཇོག་</link>"

#: main0200.xhp
msgctxt ""
"main0200.xhp\n"
"tit\n"
"help.text"
msgid "Toolbars"
msgstr "ཡོ་བྱད་ཚང་"

#: main0200.xhp
msgctxt ""
"main0200.xhp\n"
"hd_id3148663\n"
"1\n"
"help.text"
msgid "<variable id=\"main0200\"><link href=\"text/sdraw/main0200.xhp\" name=\"Toolbars\">Toolbars</link></variable>"
msgstr "<variable id=\"main0200\"><link href=\"text/sdraw/main0200.xhp\" name=\"ཡོ་བྱད་ཚང་\">ཡོ་བྱད་ཚང་</link></variable>"

#: main0200.xhp
msgctxt ""
"main0200.xhp\n"
"par_id3125863\n"
"2\n"
"help.text"
msgid "This section provides an overview of the toolbars available in $[officename] Draw."
msgstr "ས་བཅད་འདིར་ཕྱོགས་བསྡུས་ཀྱིས་$[officename] རིས་མཚོན་བཟོ་བའི་ནང་སྤྱོད་རུང་བའི་ཡོ་བྱད་ཚང་ངོ་སྤྲོད་བྱེད།"

#: main0202.xhp
msgctxt ""
"main0202.xhp\n"
"tit\n"
"help.text"
msgid "Line and Filling Bar"
msgstr "རིས་མཚོན་བྱ་ཡུལ་ཚང་"

#: main0202.xhp
msgctxt ""
"main0202.xhp\n"
"hd_id3149669\n"
"1\n"
"help.text"
msgid "<link href=\"text/sdraw/main0202.xhp\" name=\"Line and Filling Bar\">Line and Filling Bar</link>"
msgstr "<link href=\"text/sdraw/main0202.xhp\" name=\"རིས་མཚོན་བྱ་ཡུལ་ཚང་\">རིས་མཚོན་བྱ་ཡུལ་ཚང་</link>"

#: main0202.xhp
msgctxt ""
"main0202.xhp\n"
"par_id3150543\n"
"2\n"
"help.text"
msgid "The Line and Filling bar contains commands for the current editing mode."
msgstr "<emph>[རིས་མཚོན་བྱ་ཡུལ་ཚང་]</emph>ནང་མིག་སྔའི་རྩོམ་སྒྲིག་མ་ཚུལ་འོག་སྤྱོད་རུང་གི་བཀའ་ཚིག་འདུས་ཡོད།"

#: main0202.xhp
msgctxt ""
"main0202.xhp\n"
"hd_id3149664\n"
"3\n"
"help.text"
msgid "<link href=\"text/shared/01/05200100.xhp\" name=\"Line Style\">Line Style</link>"
msgstr "<link href=\"text/shared/01/05200100.xhp\" name=\"ཐིག་སྐུད་བཟོ་ལྟ་\">ཐིག་སྐུད་བཟོ་ལྟ་</link>"

#: main0202.xhp
msgctxt ""
"main0202.xhp\n"
"hd_id3156285\n"
"4\n"
"help.text"
msgid "<link href=\"text/shared/01/05200100.xhp\" name=\"Line Width\">Line Width</link>"
msgstr "<link href=\"text/shared/01/05200100.xhp\" name=\"ཐིག་སྐུད་སྦོམ་ཕྲ་\">ཐིག་སྐུད་སྦོམ་ཕྲ་</link>"

#: main0202.xhp
msgctxt ""
"main0202.xhp\n"
"hd_id3154015\n"
"5\n"
"help.text"
msgid "<link href=\"text/shared/01/05200100.xhp\" name=\"Line Color\">Line Color</link>"
msgstr "<link href=\"text/shared/01/05200100.xhp\" name=\"ཐིག་སྐུད་ཚོས་གཞི་\">ཐིག་སྐུད་ཚོས་གཞི་</link>"

#: main0202.xhp
msgctxt ""
"main0202.xhp\n"
"hd_id3155767\n"
"6\n"
"help.text"
msgid "<link href=\"text/shared/01/05210100.xhp\" name=\"Area Style / Filling\">Area Style / Filling</link>"
msgstr "<link href=\"text/shared/01/05210100.xhp\" name=\"སྐོང་གསབ་ནང་དོན་\">སྐོང་གསབ་ནང་དོན་</link>"

#: main0202.xhp
msgctxt ""
"main0202.xhp\n"
"hd_id3341471\n"
"help.text"
msgid "<link href=\"text/shared/01/05210600.xhp\" name=\"Shadow\">Shadow</link>"
msgstr "<link href=\"text/shared/01/05210600.xhp\" name=\"གྲིབ་གཟུགས་\">གྲིབ་གཟུགས་</link>"

#: main0210.xhp
msgctxt ""
"main0210.xhp\n"
"tit\n"
"help.text"
msgid "Drawing Bar"
msgstr "ཡོ་བྱད་ཚང་གཙོ་བོ་"

#: main0210.xhp
msgctxt ""
"main0210.xhp\n"
"hd_id3150398\n"
"1\n"
"help.text"
msgid "<link href=\"text/sdraw/main0210.xhp\" name=\"Drawing Bar\">Drawing Bar</link>"
msgstr "<link href=\"text/sdraw/main0210.xhp\" name=\"གཙོ་བོའི་ཡོ་བྱད་ཚང་\">གཙོ་བོའི་ཡོ་བྱད་ཚང་</link>"

#: main0210.xhp
msgctxt ""
"main0210.xhp\n"
"par_id3149656\n"
"2\n"
"help.text"
msgid "The <emph>Drawing</emph> bar holds the main drawing tools."
msgstr "<emph>[ཡོ་བྱད་ཚང་གཙོ་བོ་]</emph>ནང་གཙོ་བོའི་རིས་མཚོན་ཡོ་བྱད་འདུས་ཡོད།"

#: main0210.xhp
msgctxt ""
"main0210.xhp\n"
"par_idN105D1\n"
"help.text"
msgid "<link href=\"text/simpress/02/10060000.xhp\">Rectangle</link>"
msgstr "<link href=\"text/simpress/02/10060000.xhp\">གྲུ་བཞི་</link>"

#: main0210.xhp
msgctxt ""
"main0210.xhp\n"
"par_idN105E1\n"
"help.text"
msgid "Draws a filled rectangle where you drag in the current document. Click where you want to place a corner of the rectangle, and drag to the size you want. To draw a square, hold down Shift while you drag."
msgstr "མིག་སྔའི་ཡིག་ཚགས་ནང་དྲུད་སྐྱེལ་བྱས་པའི་གནས་སར་མདོག་སྣོན་པའི་གྲུ་བཞི་ཞིག་འགོད་བྲིས་བྱ། གྲུ་བཞིའི་ཟུར་ཞིག་འགོད་བྲིས་བྱ་དགོས་པའི་གནས་སར་རྐྱང་རྡེབ་བྱ་ དེ་རྗེས་དགོས་ངེས་ཀྱི་ཆ་རྐྱེན་དེ་བཞིན་དྲུད་པར་བྱ། གྲུ་བཞི་ཁ་གང་འགོད་བྲིས་བྱ་དགོས་ན་དྲུད་སྒུལ་བྱ་སྐབས་ Shift མཐེབ་གནོན་འཛིན་བྱ།"

#: main0210.xhp
msgctxt ""
"main0210.xhp\n"
"par_idN105EE\n"
"help.text"
msgid "<link href=\"text/simpress/02/10070000.xhp\">Ellipse</link>"
msgstr "<link href=\"text/simpress/02/10070000.xhp\">འཇོང་སྒོར་</link>"

#: main0210.xhp
msgctxt ""
"main0210.xhp\n"
"par_idN105FE\n"
"help.text"
msgid "Draws a filled oval where you drag in the current document. Click where you want to draw the oval, and drag to the size you want. To draw a circle, hold down Shift while you drag."
msgstr "མིག་སྔའི་ཡིག་ཚགས་ནང་དྲུད་སྒུལ་བྱས་པའི་གནས་སར་མདོག་སྣོན་བྱས་པའི་འཇོད་སྒོར་ཞིག་འགོད་བྲིས་བྱ། འཇོད་སྒོར་འགོད་བྲིས་བྱ་བའི་གནས་སར་རྐྱང་རྡེབ་བྱ་བ་དང་དེ་རྗེས་དགོས་ངེས་ཀྱི་ཆེ་ཆུང་དེ་ལྟར་དྲུད་པར་བྱ། སྒོར་མོ་འགོད་བྲིས་དགོས་ན་དྲུད་སྒུལ་བྱ་སྐབས་ Shift མཐེབ་གནོན་འཛིན་བྱ་རོགས།"

#: main0210.xhp
msgctxt ""
"main0210.xhp\n"
"par_idN1060B\n"
"help.text"
msgid "<link href=\"text/simpress/02/10050000.xhp\">Text</link>"
msgstr "<link href=\"text/simpress/02/10050000.xhp\">ཡི་གེ་</link>"

#: main0210.xhp
msgctxt ""
"main0210.xhp\n"
"par_idN1061B\n"
"help.text"
msgid "Draws a text box where you click or drag in the current document. Click anywhere in the document, and then type or paste your text."
msgstr "མིག་སྔའི་ཡིག་ཚགས་ནང་རྐྱང་རྡེབ་བམ་དྲུད་སྒུལ་གྱི་སར་ཡི་གེའི་སྒྲོམ་ཞིག་འགོད་བྲིས་བྱ། ཡིག་ཚགས་ནང་གི་གནས་ས་གང་རུང་དུ་རྐྱང་རྡེབ་བྱ་བ་དང་དེ་རྗེས་ཡི་གེ་མཐེབ་འཇུག་དང་སྦྱར་དགོས།"

#: main0210.xhp
msgctxt ""
"main0210.xhp\n"
"par_idN107C8\n"
"help.text"
msgid "<link href=\"text/simpress/02/10120000.xhp\" name=\"Lines and Arrows\">Lines and Arrows</link>"
msgstr ""

#: main0210.xhp
msgctxt ""
"main0210.xhp\n"
"par_idN126D7\n"
"help.text"
msgid "Opens the Arrows toolbar to insert lines and arrows."
msgstr ""

#: main0210.xhp
msgctxt ""
"main0210.xhp\n"
"par_idN106B4\n"
"help.text"
msgid "<link href=\"text/shared/01/05270000.xhp\" name=\"Points\">Points</link>"
msgstr "<link href=\"text/shared/01/05270000.xhp\" name=\"རྩེ་ཚེག་རྩོམ་སྒྲིག་\">རྩེ་ཚེག་རྩོམ་སྒྲིག་</link>"

#: main0210.xhp
msgctxt ""
"main0210.xhp\n"
"par_idN106C3\n"
"help.text"
msgid "Enables you to edit points on your drawing."
msgstr "རིས་མཚོན་བྱ་སྐབས་རྩེ་ཚེག་རྩོམ་སྒྲིག་ལ་སྤྱོད།"

#: main0210.xhp
msgctxt ""
"main0210.xhp\n"
"par_idN106C8\n"
"help.text"
msgid "<link href=\"text/simpress/02/10030200.xhp\" name=\"Glue points\">Glue Points</link>"
msgstr "<link href=\"text/simpress/02/10030200.xhp\" name=\"སྦྱར་མདུད་ཚེག་\">སྦྱར་མདུད་ཚེག་</link>"

#: main0210.xhp
msgctxt ""
"main0210.xhp\n"
"par_idN106D7\n"
"help.text"
msgid "Enables you to edit glue points on your drawing."
msgstr "རིས་མཚོན་བྱ་སྐབས་སྦྱར་མདུད་ཚེག་རྩོམ་སྒྲིག་བྱ་བར་སྤྱོད།"

#: main0210.xhp
msgctxt ""
"main0210.xhp\n"
"par_idN10754\n"
"help.text"
msgid "<link href=\"text/shared/01/04140000.xhp\" name=\"From File\">From File</link>"
msgstr "<link href=\"text/shared/01/04140000.xhp\" name=\"ཡིག་ཆ་ནས་ཡོང་བའི་པར་རིས་\">ཡིག་ཆ་ནས་ཡོང་བའི་པར་རིས་</link>"

#: main0210.xhp
msgctxt ""
"main0210.xhp\n"
"par_idN1072C\n"
"help.text"
msgid "<link href=\"text/shared/02/01170000.xhp\" name=\"Form Controls\">Form Controls</link>"
msgstr "<link href=\"text/shared/02/01170000.xhp\" name=\"ཚོད་ཆས་\">ཚོད་ཆས་</link>"

#: main0210.xhp
msgctxt ""
"main0210.xhp\n"
"par_idN1074B\n"
"help.text"
msgid "<link href=\"text/shared/3dsettings_toolbar.xhp\">Extrusion On/Off</link>"
msgstr "<link href=\"text/shared/3dsettings_toolbar.xhp\">རྩ་གསུམ་ཕན་འབྲས་བཀོད་སྒྲིག་/རྩིས་མེད་</link>"

#: main0210.xhp
msgctxt ""
"main0210.xhp\n"
"par_idN1075A\n"
"help.text"
msgid "Switches the 3D effects on and off for the selected objects."
msgstr "བདམས་པའི་བྱ་ཡུལ་རྩ་གསུམ་ཕན་འབྲས་ཀྱི་ཕྱེ་དང་རྒྱག་བརྗེ་བསྒྱུར་བྱེད།"

#: main0213.xhp
msgctxt ""
"main0213.xhp\n"
"tit\n"
"help.text"
msgid "Options Bar"
msgstr "འདེམས་གཞི་ཚང་"

#: main0213.xhp
msgctxt ""
"main0213.xhp\n"
"hd_id3150793\n"
"1\n"
"help.text"
msgid "<link href=\"text/sdraw/main0213.xhp\" name=\"Options Bar\">Options Bar</link>"
msgstr "<link href=\"text/sdraw/main0213.xhp\" name=\"འདེམས་གཞིའི་ཚང་\">འདེམས་གཞིའི་ཚང་</link>"

#: main0213.xhp
msgctxt ""
"main0213.xhp\n"
"par_id3154685\n"
"2\n"
"help.text"
msgid "The <emph>Options</emph> bar can be displayed by choosing <emph>View - Toolbars - Options</emph>."
msgstr "<emph>[མཐོང་རིས་] - [ཡོ་བྱད་ཚང་] - [འདེམས་གཞི་ཚང་]</emph>འདེམས་པ་བརྒྱུད་<emph>[འདེམས་གཞིའི་ཚང་]</emph>མངོན་པ།"

#: main0213.xhp
msgctxt ""
"main0213.xhp\n"
"hd_id3145251\n"
"3\n"
"help.text"
msgid "<link href=\"text/shared/02/01171200.xhp\" name=\"Display Grid\">Display Grid</link>"
msgstr "<link href=\"text/shared/02/01171200.xhp\" name=\"དྲ་རྒྱ་མངོན་པ་\">དྲ་རྒྱ་མངོན་པ་</link>"

#: main0213.xhp
msgctxt ""
"main0213.xhp\n"
"hd_id3149018\n"
"5\n"
"help.text"
msgid "<link href=\"text/shared/02/01171400.xhp\" name=\"Helplines While Moving\">Helplines While Moving</link>"
msgstr "<link href=\"text/shared/02/01171400.xhp\" name=\"སྤོ་སྒུལ་སྐབས་རམ་འདེགས་ཐིག་མངོན་པ་\">སྤོ་སྒུལ་སྐབས་རམ་འདེགས་ཐིག་མངོན་པ་</link>"

#: main0213.xhp
msgctxt ""
"main0213.xhp\n"
"hd_id3147338\n"
"6\n"
"help.text"
msgid "<link href=\"text/shared/02/01171300.xhp\" name=\"Snap to Grid\">Snap to Grid</link>"
msgstr "<link href=\"text/shared/02/01171300.xhp\" name=\"དྲ་རྒྱ་དང་སྙོམ་གཤིབ་བྱེད་\">དྲ་རྒྱ་དང་སྙོམ་གཤིབ་བྱེད་</link>"

#: main0213.xhp
msgctxt ""
"main0213.xhp\n"
"hd_id3146313\n"
"7\n"
"help.text"
msgid "<link href=\"text/simpress/02/13140000.xhp\" name=\"Snap to Snap Lines\">Snap to Snap Lines</link>"
msgstr "<link href=\"text/simpress/02/13140000.xhp\" name=\"གནས་ཚང་ཐིག་དང་སྙོམ་གཤིབ་\">གནས་ཚང་ཐིག་དང་སྙོམ་གཤིབ་</link>"

#: main0213.xhp
msgctxt ""
"main0213.xhp\n"
"hd_id3155111\n"
"8\n"
"help.text"
msgid "<link href=\"text/simpress/02/13150000.xhp\" name=\"Snap to Page Margins\">Snap to Page Margins</link>"
msgstr "<link href=\"text/simpress/02/13150000.xhp\" name=\"ཤོག་མཐའི་རྒྱང་དང་སྙོམ་གཤིབ་བྱེད་\">ཤོག་མཐའི་རྒྱང་དང་སྙོམ་གཤིབ་བྱེད་</link>"

#: main0213.xhp
msgctxt ""
"main0213.xhp\n"
"hd_id3150343\n"
"9\n"
"help.text"
msgid "<link href=\"text/simpress/02/13160000.xhp\" name=\"Snap to Object Border\">Snap to Object Border</link>"
msgstr "<link href=\"text/simpress/02/13160000.xhp\" name=\"བྱ་ཡུལ་སྒྲོམ་དང་སྙོམ་གཤིབ་བྱེད་\">བྱ་ཡུལ་སྒྲོམ་དང་སྙོམ་གཤིབ་བྱེད་</link>"

#: main0213.xhp
msgctxt ""
"main0213.xhp\n"
"hd_id3150368\n"
"10\n"
"help.text"
msgid "<link href=\"text/simpress/02/13170000.xhp\" name=\"Snap to Object Points\">Snap to Object Points</link>"
msgstr "<link href=\"text/simpress/02/13170000.xhp\" name=\"བྱ་ཡུལ་ཚེག་ལ་དམིགས་པ་\">བྱ་ཡུལ་ཚེག་ལ་དམིགས་པ་</link>"

#: main0213.xhp
msgctxt ""
"main0213.xhp\n"
"hd_id3146980\n"
"11\n"
"help.text"
msgid "<link href=\"text/simpress/02/13180000.xhp\" name=\"Allow Quick Editing\">Allow Quick Editing</link>"
msgstr "<link href=\"text/simpress/02/13180000.xhp\" name=\"མགྱོགས་མྱུར་རྩོམ་སྒྲིག་རུང་བ་\">མགྱོགས་མྱུར་རྩོམ་སྒྲིག་རུང་བ་</link>"

#: main0213.xhp
msgctxt ""
"main0213.xhp\n"
"hd_id3148870\n"
"12\n"
"help.text"
msgid "<link href=\"text/simpress/02/13190000.xhp\" name=\"Select Text Area Only\">Select Text Area Only</link>"
msgstr "<link href=\"text/simpress/02/13190000.xhp\" name=\"ཡི་གེའི་ས་ཁོངས་ཁོ་ན་འདེམས་པ་\">ཡི་གེའི་ས་ཁོངས་ཁོ་ན་འདེམས་པ་</link>"

#: main0503.xhp
msgctxt ""
"main0503.xhp\n"
"tit\n"
"help.text"
msgid "$[officename] Draw Features"
msgstr "$[officename] རིས་འགོད་བཟོ་བའི་བྱེད་ནུས་"

#: main0503.xhp
msgctxt ""
"main0503.xhp\n"
"hd_id3148797\n"
"1\n"
"help.text"
msgid "<variable id=\"main0503\"><link href=\"text/sdraw/main0503.xhp\" name=\"$[officename] Draw Features\">$[officename] Draw Features</link></variable>"
msgstr "<variable id=\"main0503\"><link href=\"text/sdraw/main0503.xhp\" name=\"$[officename] རིས་འགོད་བཟོ་བའི་བྱེད་ནུས་\">$[officename] རིས་འགོད་བཟོ་བའི་བྱེད་ནུས་</link></variable>"

#: main0503.xhp
msgctxt ""
"main0503.xhp\n"
"par_id3146975\n"
"2\n"
"help.text"
msgid "$[officename] Draw lets you create simple and complex drawings and export them in a number of common image formats. You can also insert tables, charts, formulas and other items created in $[officename] programs into your drawings."
msgstr "$[officename] རིས་འགོད་བཟོ་བ་སྤྱད་ནས་རིས་འགོད་སྟབས་བདེའམ་ཪྙོག་འཛིན་ཅན་བཟོ་ཐུབ་ མ་ཟད་རིས་འགོད་དེ་དག་རྒྱུན་སྤྱོད་ཀྱི་པར་རིས་རྣམ་གཞག་སྒོ་ནས་འདྲེན་བཏོན་བྱེད་ཐུབ་ ད་དུང་རིས་འགོད་ནང་$[officename] བྱ་རིམ་གྱིས་གསར་འཛུགས་བྱས་པའི་རེའུ་མིག་ རིས་མཚོན་དང་སྤྱི་འགྲོས་དེ་བཞིན་རྣམ་གྲངས་གཞན་དག་བསྒར་འཛུད་བྱ་ཐུབ།"

#: main0503.xhp
msgctxt ""
"main0503.xhp\n"
"hd_id3147435\n"
"11\n"
"help.text"
msgid "Vector Graphics"
msgstr "ཕྱོགས་ཚང་རིས་དབྱིབས་"

#: main0503.xhp
msgctxt ""
"main0503.xhp\n"
"par_id3153142\n"
"12\n"
"help.text"
msgid "$[officename] Draw creates vector graphics using lines and curves defined by mathematical vectors. Vectors describe lines, ellipses, and polygons according to their geometry."
msgstr "$[officename] རིས་འགོད་བཟོ་བས་གྲངས་རིག་ཕྱོགས་ཚང་གི་དྲང་ཐིག་དང་ཁྱོག་ཐིག་བརྒྱུད་ནས་ཕྱོགས་ཚང་རིས་དབྱིབས་གསར་འཛུགས་བྱེད་ ཕྱོགས་ཚང་ནི་རིས་དབྱིབས་ཀྱིས་དབྱིབས་རྩིས་བཟོ་ལྟར་གཞིགས་ནས་ཐིག་སྐུད་དང་འཇོང་སྒོར་མཐའ་མང་དབྱིབས་མཚན་འཇོག་བྱེད།"

#: main0503.xhp
msgctxt ""
"main0503.xhp\n"
"hd_id3154320\n"
"14\n"
"help.text"
msgid "Creating 3D Objects"
msgstr "རྩ་གསུམ་བྱ་ཡུལ་བཟོ་བ་"

#: main0503.xhp
msgctxt ""
"main0503.xhp\n"
"par_id3145251\n"
"15\n"
"help.text"
msgid "You can create simple 3D objects such as cubes, spheres, and cylinders in $[officename] Draw and even modify the light source of the objects."
msgstr "$[officename] རིས་འགོད་བཟོ་བའི་ནང་སྟབས་བདེའི་རྩ་གསུམ་བྱ་ཡུལ་བཟོ་ཐུབ་(དཔེར་ན་ བསླངས་གཟུགས་གྲུ་བཞི་ ཟླུམ་གཟུགས་དང་ཀ་ཟླུམ་གཟུགས་)མ་ཟད་བྱ་ཡུལ་དེ་དག་གི་འོད་ཁུངས་བཟོ་བཅོས་བྱ་ཐུབ།"

#: main0503.xhp
msgctxt ""
"main0503.xhp\n"
"hd_id3154491\n"
"20\n"
"help.text"
msgid "Grids and Snap Lines"
msgstr ""

#: main0503.xhp
msgctxt ""
"main0503.xhp\n"
"par_id3149379\n"
"6\n"
"help.text"
msgid "Grids and snap lines provide a visual cue to help you align objects in your drawing. You can also choose to snap an object to a grid line, snap line or to the edge of another object."
msgstr "རིས་འགོད་ནང་བྱ་ཡུལ་སྙོམ་གཤིབ་བྱ་སྐབས་དྲ་རྒྱའི་ཐིག་དང་རམ་འདེགས་ཐིག་གིས་མིག་ཤེས་དྲན་བརྡ་མཁོ་སྤྲོད་བྱེད་ ཡང་ན་བྱ་ཡུལ་དང་དྲ་རྒྱའི་ཐིག་དང་རམ་འདེགས་ཐིག་གམ་བྱ་ཡུལ་གཞན་དག་གི་མཐའ་འགྲམ་དང་སྙོམ་གཤིབ་བྱ་བ་འདེམས་ཐུབ།"

#: main0503.xhp
msgctxt ""
"main0503.xhp\n"
"hd_id3155601\n"
"16\n"
"help.text"
msgid "Connecting Objects to Show Relationships"
msgstr "བྱ་ཡུལ་སྦྲེལ་མཐུད་ཀྱིས་འབྲེལ་བ་མངོན་"

#: main0503.xhp
msgctxt ""
"main0503.xhp\n"
"par_id3149124\n"
"17\n"
"help.text"
msgid "You can connect objects in $[officename] Draw with special lines called \"connectors\" to show the relationship between objects. Connectors attach to glue points on drawing objects and remain attached when the connected objects are moved. Connectors are useful for creating organization charts and technical diagrams."
msgstr "$[officename] རིས་འགོད་བཟོ་བའི་ནང་དུ་\"སྦྲེལ་མཐུད་རྟགས་\"སུ་འབོད་པའི་དམིགས་བསལ་ཐིག་སྐུད་ཅིག་སྤྱད་ནས་བྱ་ཡུལ་སྦྲེལ་ཏེ་བྱ་ཡུལ་བར་གྱི་འབྲེལ་བ་མངོན་པར་བྱ་ཐུབ། སྦྲེལ་མཐུད་རྟགས་དང་རིས་འགོད་བྱ་ཡུལ་སྟེང་གི་སྦྱར་མདུང་ཚེག་ཕན་ཚུན་སྦྲེལ་བ་དང་མ་ཟད་ཁྱེད་ཀྱིས་བྱ་ཡུལ་སྤོ་སྒུལ་བྱ་སྐབས་དེ་ལྟའི་སྦྲེལ་མཐུད་འབྲེལ་བ་སྔར་བཞིན་མི་འགྱུར་བར་རང་སོར་གནས། སྒྲིག་འཛུགས་གྲུབ་ཆའི་རི་མོ་དང་ལག་རྩལ་རིས་མཚོན་གསར་འཛུགས་བྱ་སྐབས་སྦྲེལ་མཐུད་རྟགས་ཧ་ཅང་བེད་སྤྱོད་ཆེ།"

#: main0503.xhp
msgctxt ""
"main0503.xhp\n"
"hd_id3155764\n"
"21\n"
"help.text"
msgid "Displaying Dimensions"
msgstr "ཁྲེ་ཚུན་མངོན་པ་"

#: main0503.xhp
msgctxt ""
"main0503.xhp\n"
"par_id3155333\n"
"22\n"
"help.text"
msgid "Technical diagrams often show the dimensions of objects in the drawing. In $[officename] Draw, you can use dimension lines to calculate and display linear dimensions."
msgstr "ལག་རྩལ་རིས་མཚོན་གྱིས་རྒྱུན་པར་རིས་འགོད་ནང་བྱ་ཡུལ་གྱི་ཁྲེ་ཚུན་མངོན། $[officename] རིས་འགོད་བཟོ་བའི་ནང་ཁྱེད་ཀྱིས་ཚང་ངེས་ཐིག་སྤྱད་ནས་ཐིག་གཤིས་ཁྲེ་ཚུན་རྩིས་རྒྱག་དང་མངོན་པར་བྱ་ཐུབ།"

#: main0503.xhp
msgctxt ""
"main0503.xhp\n"
"hd_id3154705\n"
"18\n"
"help.text"
msgid "Gallery"
msgstr "དྲས་སྦྱར་རི་མོ་"

#: main0503.xhp
msgctxt ""
"main0503.xhp\n"
"par_id3154022\n"
"7\n"
"help.text"
msgid "The Gallery contains images, animations, sounds and other items that you can insert and use in your drawings as well as other $[officename] programs."
msgstr "དྲས་སྦྱར་པར་འཛོད་ནང་བཪྙན་རིས་ འགུལ་རིས་དང་སྐད་སྒྲ་སོགས་ཀྱི་རྣམ་གྲངས་ཚུད་ རིས་འགོད་དང་ $[officename] བྱ་རིམ་གཞན་དག་ནང་རྣམ་གྲངས་དེ་དག་བསྒར་འཛུད་དང་བེད་སྤྱོད་བྱས་ཆོག"

#: main0503.xhp
msgctxt ""
"main0503.xhp\n"
"hd_id3149207\n"
"19\n"
"help.text"
msgid "Graphic File Formats"
msgstr "རིས་དབྱིབས་ཡིག་ཆའི་རྣམ་གཞག་"

#: main0503.xhp
msgctxt ""
"main0503.xhp\n"
"par_id3155112\n"
"5\n"
"help.text"
msgid "$[officename] Draw can export to many common graphic file formats, such as BMP, GIF, JPG, and PNG."
msgstr "$[officename] རིས་འགོད་བཟོ་བས་རིས་འགོད་ཡིག་ཚགས་ཀྱིས་རྒྱུན་སྤྱོད་ཀྱི་རིས་དབྱིབས་ཡིག་ཆའི་རྣམ་གཞག་སྣ་ཚོགས་འདྲེན་བཏོན་བྱ་ཐུབ་ དཔེར་ན་ BMP GIF JPG དང་ PNG སོགས།"

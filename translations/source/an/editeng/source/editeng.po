#. extracted from editeng/source/editeng
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-11-10 19:33+0100\n"
"PO-Revision-Date: 2015-11-11 00:11+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: an\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1447200704.000000\n"

#: editeng.src
msgctxt ""
"editeng.src\n"
"RID_EDITUNDO_DEL\n"
"string.text"
msgid "Delete"
msgstr "Eliminar"

#: editeng.src
msgctxt ""
"editeng.src\n"
"RID_EDITUNDO_MOVE\n"
"string.text"
msgid "Move"
msgstr ""

#: editeng.src
msgctxt ""
"editeng.src\n"
"RID_EDITUNDO_INSERT\n"
"string.text"
msgid "Insert"
msgstr ""

#: editeng.src
msgctxt ""
"editeng.src\n"
"RID_EDITUNDO_REPLACE\n"
"string.text"
msgid "Replace"
msgstr ""

#: editeng.src
msgctxt ""
"editeng.src\n"
"RID_EDITUNDO_SETATTRIBS\n"
"string.text"
msgid "Apply attributes"
msgstr ""

#: editeng.src
#, fuzzy
msgctxt ""
"editeng.src\n"
"RID_EDITUNDO_RESETATTRIBS\n"
"string.text"
msgid "Reset attributes"
msgstr "Atributos de texto"

#: editeng.src
msgctxt ""
"editeng.src\n"
"RID_EDITUNDO_INDENT\n"
"string.text"
msgid "Indent"
msgstr ""

#: editeng.src
#, fuzzy
msgctxt ""
"editeng.src\n"
"RID_EDITUNDO_SETSTYLE\n"
"string.text"
msgid "Apply Styles"
msgstr "Estilos aplicaus"

#: editeng.src
msgctxt ""
"editeng.src\n"
"RID_EDITUNDO_TRANSLITERATE\n"
"string.text"
msgid "Change Case"
msgstr ""

#: editeng.src
msgctxt ""
"editeng.src\n"
"RID_MENU_SPELL\n"
"MN_IGNORE\n"
"menuitem.text"
msgid "I~gnore All"
msgstr ""

#: editeng.src
msgctxt ""
"editeng.src\n"
"RID_MENU_SPELL\n"
"MN_INSERT\n"
"menuitem.text"
msgid "~Add to Dictionary"
msgstr ""

#: editeng.src
msgctxt ""
"editeng.src\n"
"RID_MENU_SPELL\n"
"MN_INSERT_SINGLE\n"
"menuitem.text"
msgid "~Add to Dictionary"
msgstr ""

#: editeng.src
msgctxt ""
"editeng.src\n"
"RID_MENU_SPELL\n"
"MN_SPELLING\n"
"menuitem.text"
msgid "~Spellcheck..."
msgstr ""

#: editeng.src
msgctxt ""
"editeng.src\n"
"RID_MENU_SPELL\n"
"MN_AUTOCORR\n"
"menuitem.text"
msgid "AutoCorrect ~To"
msgstr ""

#: editeng.src
msgctxt ""
"editeng.src\n"
"RID_MENU_SPELL\n"
"MN_AUTO_CORRECT_DLG\n"
"menuitem.text"
msgid "Auto~Correct Options..."
msgstr ""

#: editeng.src
msgctxt ""
"editeng.src\n"
"RID_STR_WORD\n"
"string.text"
msgid "Word is %x"
msgstr ""

#: editeng.src
msgctxt ""
"editeng.src\n"
"RID_STR_PARAGRAPH\n"
"string.text"
msgid "Paragraph is %x"
msgstr ""

#. extracted from avmedia/source/framework
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-11-15 15:53+0000\n"
"Last-Translator: Stuart Swales <stuart.swales.croftnuisk@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.7\n"
"X-POOTLE-MTIME: 1479225224.000000\n"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_OPEN\n"
"string.text"
msgid "Open"
msgstr "Open"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_INSERT\n"
"string.text"
msgid "Apply"
msgstr "Apply"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_PLAY\n"
"string.text"
msgid "Play"
msgstr "Play"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_PAUSE\n"
"string.text"
msgid "Pause"
msgstr "Pause"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_STOP\n"
"string.text"
msgid "Stop"
msgstr "Stop"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_ENDLESS\n"
"string.text"
msgid "Repeat"
msgstr "Repeat"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_MUTE\n"
"string.text"
msgid "Mute"
msgstr "Mute"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_ZOOM\n"
"string.text"
msgid "View"
msgstr "View"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_ZOOM_50\n"
"string.text"
msgid "50%"
msgstr "50%"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_ZOOM_100\n"
"string.text"
msgid "100%"
msgstr "100%"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_ZOOM_200\n"
"string.text"
msgid "200%"
msgstr "200%"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_ZOOM_FIT\n"
"string.text"
msgid "Scaled"
msgstr "Scaled"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_STR_MEDIAPLAYER\n"
"string.text"
msgid "Media Player"
msgstr "Media Player"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_MEDIA_PATH\n"
"string.text"
msgid "Media Path"
msgstr "Media Path"

#: mediacontrol.src
msgctxt ""
"mediacontrol.src\n"
"AVMEDIA_MEDIA_PATH_DEFAULT\n"
"string.text"
msgid "No Media Selected"
msgstr "No Media Selected"

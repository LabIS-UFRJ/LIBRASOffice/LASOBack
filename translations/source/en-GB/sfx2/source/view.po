#. extracted from sfx2/source/view
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-12-01 12:11+0100\n"
"PO-Revision-Date: 2016-12-02 00:44+0000\n"
"Last-Translator: Stuart Swales <stuart.swales.croftnuisk@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1480639477.000000\n"

#: view.src
msgctxt ""
"view.src\n"
"STR_NODEFPRINTER\n"
"string.text"
msgid ""
"No default printer found.\n"
"Please choose a printer and try again."
msgstr ""
"No default printer found.\n"
"Please choose a printer and try again."

#: view.src
msgctxt ""
"view.src\n"
"STR_NOSTARTPRINTER\n"
"string.text"
msgid ""
"Could not start printer.\n"
"Please check your printer configuration."
msgstr ""
"Could not start printer.\n"
"Please check your printer configuration."

#: view.src
msgctxt ""
"view.src\n"
"STR_ERROR_PRINTER_BUSY\n"
"string.text"
msgid "Printer busy"
msgstr "Printer busy"

#: view.src
msgctxt ""
"view.src\n"
"STR_ERROR_PRINT\n"
"string.text"
msgid "Error while printing"
msgstr "Error while printing"

#: view.src
msgctxt ""
"view.src\n"
"STR_PAGE\n"
"string.text"
msgid "Page "
msgstr "Page "

#: view.src
msgctxt ""
"view.src\n"
"STR_ERROR_SAVE_TEMPLATE\n"
"string.text"
msgid "Error saving template "
msgstr "Error saving template "

#: view.src
msgctxt ""
"view.src\n"
"STR_READONLY\n"
"string.text"
msgid " (read-only)"
msgstr " (read-only)"

#: view.src
msgctxt ""
"view.src\n"
"STR_PRINT_NEWORI\n"
"string.text"
msgid ""
"The page size and orientation have been modified.\n"
"Would you like to save the new settings in the\n"
"active document?"
msgstr ""
"The page size and orientation have been modified.\n"
"Would you like to save the new settings in the\n"
"active document?"

#: view.src
msgctxt ""
"view.src\n"
"STR_PRINT_NEWSIZE\n"
"string.text"
msgid ""
"The page size has been modified.\n"
"Should the new settings be saved\n"
"in the active document?"
msgstr ""
"The page size has been modified.\n"
"Should the new settings be saved\n"
"in the active document?"

#: view.src
msgctxt ""
"view.src\n"
"STR_PRINT_NEWORISIZE\n"
"string.text"
msgid ""
"The page size and orientation have been modified.\n"
"Would you like to save the new settings in the\n"
"active document?"
msgstr ""
"The page size and orientation have been modified.\n"
"Would you like to save the new settings in the\n"
"active document?"

#: view.src
msgctxt ""
"view.src\n"
"STR_CANT_CLOSE\n"
"string.text"
msgid ""
"The document cannot be closed because a\n"
" print job is being carried out."
msgstr ""
"The document cannot be closed because a\n"
" print job is being carried out."

#: view.src
msgctxt ""
"view.src\n"
"STR_ERROR_SEND_MAIL\n"
"string.text"
msgid ""
"An error occurred in sending the message. Possible errors could be a missing user account or a defective setup.\n"
"Please check the %PRODUCTNAME settings or your e-mail program settings."
msgstr ""
"An error occurred in sending the message. Possible errors could be a missing user account or a defective setup.\n"
"Please check the %PRODUCTNAME settings or your e-mail program settings."

#: view.src
msgctxt ""
"view.src\n"
"STR_QUERY_OPENASTEMPLATE\n"
"string.text"
msgid "This document cannot be edited, possibly due to missing access rights. Do you want to edit a copy of the document?"
msgstr "This document cannot be edited, possibly due to missing access rights. Do you want to edit a copy of the document?"

#: view.src
msgctxt ""
"view.src\n"
"STR_REPAIREDDOCUMENT\n"
"string.text"
msgid " (repaired document)"
msgstr " (repaired document)"

#: view.src
msgctxt ""
"view.src\n"
"STR_NONCHECKEDOUT_DOCUMENT\n"
"string.text"
msgid "This document is not checked out on the server."
msgstr "This document is not checked out on the server."

#: view.src
msgctxt ""
"view.src\n"
"STR_READONLY_DOCUMENT\n"
"string.text"
msgid "This document is open in read-only mode."
msgstr "This document is open in read-only mode."

#: view.src
msgctxt ""
"view.src\n"
"STR_READONLY_PDF\n"
"string.text"
msgid "This PDF is open in read-only mode to allow signing the existing file."
msgstr "This PDF is open in read-only mode to allow signing the existing file."

#: view.src
msgctxt ""
"view.src\n"
"STR_CLASSIFIED_DOCUMENT\n"
"string.text"
msgid "The classification label of this document is %1."
msgstr "The classification label of this document is %1."

#: view.src
msgctxt ""
"view.src\n"
"STR_TARGET_DOC_NOT_CLASSIFIED\n"
"string.text"
msgid "This document must be classified before the clipboard can be pasted."
msgstr "This document must be classified before the clipboard can be pasted."

#: view.src
msgctxt ""
"view.src\n"
"STR_DOC_CLASSIFICATION_TOO_LOW\n"
"string.text"
msgid "This document has a lower classification level than the clipboard."
msgstr "This document has a lower classification level than the clipboard."

#: view.src
msgctxt ""
"view.src\n"
"STR_CLASSIFIED_INTELLECTUAL_PROPERTY\n"
"string.text"
msgid "Intellectual Property:"
msgstr "Intellectual Property:"

#: view.src
msgctxt ""
"view.src\n"
"STR_CLASSIFIED_NATIONAL_SECURITY\n"
"string.text"
msgid "National Security:"
msgstr "National Security:"

#: view.src
msgctxt ""
"view.src\n"
"STR_CLASSIFIED_EXPORT_CONTROL\n"
"string.text"
msgid "Export Control:"
msgstr "Export Control:"

#: view.src
msgctxt ""
"view.src\n"
"STR_CHECKOUT\n"
"string.text"
msgid "Check Out"
msgstr "Check Out"

#: view.src
msgctxt ""
"view.src\n"
"STR_READONLY_EDIT\n"
"string.text"
msgid "Edit Document"
msgstr "Edit Document"

#: view.src
msgctxt ""
"view.src\n"
"STR_READONLY_SIGN\n"
"string.text"
msgid "Sign Document"
msgstr "Sign Document"

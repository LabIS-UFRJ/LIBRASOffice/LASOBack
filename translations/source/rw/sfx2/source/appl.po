#. extracted from sfx2/source/appl
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-04-17 06:46+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: rw\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1460875577.000000\n"

#: app.src
msgctxt ""
"app.src\n"
"STR_NONAME\n"
"string.text"
msgid "Untitled"
msgstr "NtaZina"

#: app.src
msgctxt ""
"app.src\n"
"STR_CLOSE\n"
"string.text"
msgid "Close"
msgstr "Gufunga"

#: app.src
msgctxt ""
"app.src\n"
"STR_STYLE_FILTER_AUTO\n"
"string.text"
msgid "Automatic"
msgstr "Kikoresha"

#: app.src
#, fuzzy
msgctxt ""
"app.src\n"
"STR_STANDARD_SHORTCUT\n"
"string.text"
msgid "Standard"
msgstr "Bisanzwe"

#: app.src
msgctxt ""
"app.src\n"
"STR_BYTES\n"
"string.text"
msgid "Bytes"
msgstr "Bayite"

#: app.src
msgctxt ""
"app.src\n"
"STR_KB\n"
"string.text"
msgid "KB"
msgstr "KB"

#: app.src
msgctxt ""
"app.src\n"
"STR_MB\n"
"string.text"
msgid "MB"
msgstr "MB"

#: app.src
msgctxt ""
"app.src\n"
"STR_GB\n"
"string.text"
msgid "GB"
msgstr "GB"

#: app.src
msgctxt ""
"app.src\n"
"STR_QUERY_LASTVERSION\n"
"string.text"
msgid "Cancel all changes?"
msgstr "Kutita ku byahindutse byose"

#: app.src
msgctxt ""
"app.src\n"
"STR_NO_WEBBROWSER_FOUND\n"
"string.text"
msgid ""
"Opening \"$(ARG1)\" failed with error code $(ARG2) and message: \"$(ARG3)\"\n"
"Maybe no web browser could be found on your system. In that case, please check your Desktop Preferences or install a web browser (for example, Firefox) in the default location requested during the browser installation."
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_NO_ABS_URI_REF\n"
"string.text"
msgid "\"$(ARG1)\" is not an absolute URL that can be passed to an external application to open it."
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"GID_INTERN\n"
"string.text"
msgid "Internal"
msgstr "By'imbere"

#: app.src
msgctxt ""
"app.src\n"
"GID_APPLICATION\n"
"string.text"
msgid "Application"
msgstr "Porogaramu"

#: app.src
msgctxt ""
"app.src\n"
"GID_VIEW\n"
"string.text"
msgid "View"
msgstr "Igaragaza"

#: app.src
msgctxt ""
"app.src\n"
"GID_DOCUMENT\n"
"string.text"
msgid "Documents"
msgstr "Inyandiko"

#: app.src
msgctxt ""
"app.src\n"
"GID_EDIT\n"
"string.text"
msgid "Edit"
msgstr "Kwandika"

#: app.src
msgctxt ""
"app.src\n"
"GID_MACRO\n"
"string.text"
msgid "BASIC"
msgstr "BASIC"

#: app.src
msgctxt ""
"app.src\n"
"GID_OPTIONS\n"
"string.text"
msgid "Options"
msgstr "Amahitamo"

#: app.src
msgctxt ""
"app.src\n"
"GID_MATH\n"
"string.text"
msgid "Math"
msgstr "Imibare"

#: app.src
msgctxt ""
"app.src\n"
"GID_NAVIGATOR\n"
"string.text"
msgid "Navigate"
msgstr "Kubuganya"

#: app.src
msgctxt ""
"app.src\n"
"GID_INSERT\n"
"string.text"
msgid "Insert"
msgstr "Kongeramo"

#: app.src
msgctxt ""
"app.src\n"
"GID_FORMAT\n"
"string.text"
msgid "Format"
msgstr "Imiterere"

#: app.src
msgctxt ""
"app.src\n"
"GID_TEMPLATE\n"
"string.text"
msgid "Templates"
msgstr "Modeli"

#: app.src
msgctxt ""
"app.src\n"
"GID_TEXT\n"
"string.text"
msgid "Text"
msgstr "Umwandiko"

#: app.src
msgctxt ""
"app.src\n"
"GID_FRAME\n"
"string.text"
msgid "Frame"
msgstr "Ikadiri"

#: app.src
msgctxt ""
"app.src\n"
"GID_GRAPHIC\n"
"string.text"
msgid "Image"
msgstr "Ishusho"

#: app.src
msgctxt ""
"app.src\n"
"GID_TABLE\n"
"string.text"
msgid "Table"
msgstr "Imbonerahamwe"

#: app.src
msgctxt ""
"app.src\n"
"GID_ENUMERATION\n"
"string.text"
msgid "Numbering"
msgstr "Itanganomero"

#: app.src
msgctxt ""
"app.src\n"
"GID_DATA\n"
"string.text"
msgid "Data"
msgstr "Ibyatanzwe"

#: app.src
msgctxt ""
"app.src\n"
"GID_SPECIAL\n"
"string.text"
msgid "Special Functions"
msgstr "Imimaro Yihariye"

#: app.src
msgctxt ""
"app.src\n"
"GID_IMAGE\n"
"string.text"
msgid "Image"
msgstr "Ishusho"

#: app.src
msgctxt ""
"app.src\n"
"GID_CHART\n"
"string.text"
msgid "Chart"
msgstr "Igishushanyo"

#: app.src
msgctxt ""
"app.src\n"
"GID_EXPLORER\n"
"string.text"
msgid "Explorer"
msgstr "Mugaragazi"

#: app.src
msgctxt ""
"app.src\n"
"GID_CONNECTOR\n"
"string.text"
msgid "Connector"
msgstr "Impuza"

#: app.src
msgctxt ""
"app.src\n"
"GID_MODIFY\n"
"string.text"
msgid "Modify"
msgstr "Guhindura"

#: app.src
msgctxt ""
"app.src\n"
"GID_DRAWING\n"
"string.text"
msgid "Drawing"
msgstr "Igishushanyo"

#: app.src
msgctxt ""
"app.src\n"
"GID_CONTROLS\n"
"string.text"
msgid "Controls"
msgstr "Amagenzura"

#: app.src
msgctxt ""
"app.src\n"
"STR_ISMODIFIED\n"
"string.text"
msgid "Do you want to save the changes to %1?"
msgstr "Waba ushaka kubika ibyahindutse kuri %1?"

#: app.src
#, fuzzy
msgctxt ""
"app.src\n"
"STR_QUITAPP\n"
"string.text"
msgid "E~xit %PRODUCTNAME"
msgstr "Ibijyanye no %PRODUCTNAME"

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_HELP\n"
"string.text"
msgid "Help"
msgstr "Ifashayobora"

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_NOAUTOSTARTHELPAGENT\n"
"string.text"
msgid "No automatic start at 'XX'"
msgstr "'XX'ntishobora kwitangiza"

#: app.src
msgctxt ""
"app.src\n"
"RID_STR_HLPFILENOTEXIST\n"
"string.text"
msgid "The help file for this topic is not installed."
msgstr "Dosiye ngoboka y'iki ngingo ntayirimo."

#: app.src
msgctxt ""
"app.src\n"
"STR_QUICKSTART_EXIT\n"
"string.text"
msgid "Exit Quickstarter"
msgstr "Gufunga Mutangizavuba"

#: app.src
msgctxt ""
"app.src\n"
"STR_QUICKSTART_TIP\n"
"string.text"
msgid "%PRODUCTNAME %PRODUCTVERSION Quickstarter"
msgstr "Mutangizavuba %PRODUCTNAME %PRODUCTNAME"

#: app.src
msgctxt ""
"app.src\n"
"STR_QUICKSTART_FILEOPEN\n"
"string.text"
msgid "Open Document..."
msgstr "Gufungura Inyandiko..."

#: app.src
msgctxt ""
"app.src\n"
"STR_QUICKSTART_FROMTEMPLATE\n"
"string.text"
msgid "From Template..."
msgstr "Ivuye ku Modeli..."

#: app.src
msgctxt ""
"app.src\n"
"STR_QUICKSTART_PRELAUNCH\n"
"string.text"
msgid "Load %PRODUCTNAME During System Start-Up"
msgstr "Gutangiza %PRODUCTNAME igihe sisitemu yifungura"

#: app.src
msgctxt ""
"app.src\n"
"STR_QUICKSTART_PRELAUNCH_UNX\n"
"string.text"
msgid "Disable systray Quickstarter"
msgstr "Guhagarika systray Quickstarter"

#: app.src
msgctxt ""
"app.src\n"
"STR_QUICKSTART_LNKNAME\n"
"string.text"
msgid "%PRODUCTNAME %PRODUCTVERSION"
msgstr "%PRODUCTNAME %PRODUCTVERSION"

#: app.src
msgctxt ""
"app.src\n"
"STR_QUICKSTART_FILE\n"
"string.text"
msgid "File"
msgstr "Dosiye"

#: app.src
msgctxt ""
"app.src\n"
"STR_QUICKSTART_STARTCENTER\n"
"string.text"
msgid "Startcenter"
msgstr ""

#: app.src
#, fuzzy
msgctxt ""
"app.src\n"
"STR_QUICKSTART_RECENTDOC\n"
"string.text"
msgid "Recent Documents"
msgstr "Inyandiko za Vuba"

#: app.src
msgctxt ""
"app.src\n"
"STR_QUERY_UPDATE_LINKS\n"
"string.text"
msgid ""
"This document contains one or more links to external data.\n"
"\n"
"Would you like to change the document, and update all links\n"
"to get the most recent data?"
msgstr ""

#: app.src
#, fuzzy
msgctxt ""
"app.src\n"
"STR_DDE_ERROR\n"
"string.text"
msgid "DDE link to %1 for %2 area %3 are not available."
msgstr "Akarango ka % ko % umwanya % ntikabonetse."

#: app.src
#, fuzzy
msgctxt ""
"app.src\n"
"STR_SECURITY_WARNING_NO_HYPERLINKS\n"
"string.text"
msgid ""
"For security reasons, the hyperlink cannot be executed.\n"
"The stated address will not be opened."
msgstr ""
"Kubera impamvu z'umutekano, ihuzanyobora ntirishobora gukorwa.\n"
"Aderesi yatanzwe ntabwo izafungurwa."

#: app.src
msgctxt ""
"app.src\n"
"RID_SECURITY_WARNING_TITLE\n"
"string.text"
msgid "Security Warning"
msgstr "Iburira ry'Umutekano"

#: app.src
#, fuzzy
msgctxt ""
"app.src\n"
"RID_SVXSTR_XMLSEC_QUERY_LOSINGSIGNATURE\n"
"string.text"
msgid ""
"Saving will remove all existing signatures.\n"
"Do you want to continue saving the document?"
msgstr ""
"Kubika bizakuramo imikono y'amagenamiterere yose.\n"
"Wifuza gukomeza kubika inyandiko?"

#: app.src
#, fuzzy
msgctxt ""
"app.src\n"
"RID_SVXSTR_XMLSEC_QUERY_SAVEBEFORESIGN\n"
"string.text"
msgid ""
"The document has to be saved before it can be signed.\n"
"Do you want to save the document?"
msgstr ""
"Inyandiko yabitswe mbere y'uko ishyirwaho.\n"
"Urifuza kubika iyi nyandiko?"

#: app.src
msgctxt ""
"app.src\n"
"STR_QUERY_CANCELCHECKOUT\n"
"string.text"
msgid ""
"This will discard all changes on the server since check-out.\n"
"Do you want to proceed?"
msgstr ""

#: app.src
#, fuzzy
msgctxt ""
"app.src\n"
"STR_INFO_WRONGDOCFORMAT\n"
"string.text"
msgid "This document must be saved in OpenDocument file format before it can be digitally signed."
msgstr "Iyi nyandiko igomba kubikwa mu miterere ya dosiye OpenDocument mbere ko ishobora kuba bimenyetso mibare."

#: app.src
msgctxt ""
"app.src\n"
"RID_XMLSEC_DOCUMENTSIGNED\n"
"string.text"
msgid " (Signed)"
msgstr "(Bisinywe)"

#: app.src
#, fuzzy
msgctxt ""
"app.src\n"
"STR_STANDARD\n"
"string.text"
msgid "Standard"
msgstr "Bisanzwe"

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_FILELINK\n"
"string.text"
msgid "Document"
msgstr "Inyandiko"

#: app.src
msgctxt ""
"app.src\n"
"STR_NONE\n"
"string.text"
msgid "- None -"
msgstr "- Habe na kimwe -"

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_GRAFIKLINK\n"
"string.text"
msgid "Image"
msgstr "Ishusho"

#: app.src
msgctxt ""
"app.src\n"
"STR_SFX_FILTERNAME_ALL\n"
"string.text"
msgid "All files"
msgstr "Dosiye zose"

#: app.src
msgctxt ""
"app.src\n"
"STR_SFX_FILTERNAME_PDF\n"
"string.text"
msgid "PDF files"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_EDITGRFLINK\n"
"string.text"
msgid "Link Image"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_ERRUNOEVENTBINDUNG\n"
"string.text"
msgid ""
"An appropriate component method %1\n"
"could not be found.\n"
"\n"
"Check spelling of method name."
msgstr ""
"Uburyo bw'ibice buboneye %1\n"
"ntibwabonetse.\n"
"\n"
"Genzura imyandikire y'izina ry'uburyo."

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_GRFILTER_OPENERROR\n"
"string.text"
msgid "Image file cannot be opened"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_GRFILTER_IOERROR\n"
"string.text"
msgid "Image file cannot be read"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_GRFILTER_FORMATERROR\n"
"string.text"
msgid "Unknown image format"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_GRFILTER_VERSIONERROR\n"
"string.text"
msgid "This version of the image file is not supported"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_GRFILTER_FILTERERROR\n"
"string.text"
msgid "Image filter not found"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_GRFILTER_TOOBIG\n"
"string.text"
msgid "Not enough memory to insert image"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_END_REDLINING_WARNING\n"
"string.text"
msgid ""
"This action will exit the change recording mode.\n"
"Any information about changes will be lost.\n"
"\n"
"Exit change recording mode?\n"
"\n"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_INCORRECT_PASSWORD\n"
"string.text"
msgid "Incorrect password"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_SVXSTR_FORWARD_ERRMSSG\n"
"string.text"
msgid "If you select the option \"%PLACEHOLDER%\", you must enter a URL."
msgstr ""

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_WINDOW_TITLE\n"
"string.text"
msgid "%PRODUCTNAME Help"
msgstr "Kugoboka %PRODUCTNAME"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_BUTTON_INDEX_ON\n"
"string.text"
msgid "Show Navigation Pane"
msgstr "Kugaragaza Umwanya w'Ibuganya"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_BUTTON_INDEX_OFF\n"
"string.text"
msgid "Hide Navigation Pane"
msgstr "Guhisha Umwanya w'Ibuganya"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_BUTTON_START\n"
"string.text"
msgid "First Page"
msgstr "Paji ya mbere"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_BUTTON_PREV\n"
"string.text"
msgid "Previous Page"
msgstr "Paji ibanje"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_BUTTON_NEXT\n"
"string.text"
msgid "Next Page"
msgstr "Paji ikurikira"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_BUTTON_PRINT\n"
"string.text"
msgid "Print..."
msgstr "Gucapa..."

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_BUTTON_ADDBOOKMARK\n"
"string.text"
msgid "Add to Bookmarks..."
msgstr "Kongera ku Birango..."

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_BUTTON_SEARCHDIALOG\n"
"string.text"
msgid "Find on this Page..."
msgstr "Gushaka kuri iyi paji..."

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_BUTTON_SOURCEVIEW\n"
"string.text"
msgid "HTML Source"
msgstr "Inkomoko Ya HTML"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_FIRST_MESSAGE\n"
"string.text"
msgid "The Help is being started..."
msgstr "Ifashayobora ririmo gufunguka"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_MENU_TEXT_SELECTION_MODE\n"
"string.text"
msgid "Select Text"
msgstr "Guhitamo Umwandiko"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_HELP_MENU_TEXT_COPY\n"
"string.text"
msgid "~Copy"
msgstr "Gukoporora"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"MENU_HELP_BOOKMARKS\n"
"MID_OPEN\n"
"menuitem.text"
msgid "Display"
msgstr "Kugaragaza"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"MENU_HELP_BOOKMARKS\n"
"MID_RENAME\n"
"menuitem.text"
msgid "Rename..."
msgstr "Guhindura Izina"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"MENU_HELP_BOOKMARKS\n"
"MID_DELETE\n"
"menuitem.text"
msgid "Delete"
msgstr "Gusiba"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_INFO_NOSEARCHRESULTS\n"
"string.text"
msgid "No topics found."
msgstr "Nta Kintu cyabonetse"

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"STR_INFO_NOSEARCHTEXTFOUND\n"
"string.text"
msgid "The text you entered was not found."
msgstr "Umwandiko winjije ntabwo wabonetse."

#: newhelp.src
msgctxt ""
"newhelp.src\n"
"RID_HELP_ONSTARTUP_TEXT\n"
"string.text"
msgid "~Display %PRODUCTNAME %MODULENAME Help at Startup"
msgstr "Kugaragaza Ifashayobora rya %PRODUCTNAME %MODULENAME kuri Tangiza"

#: sfx.src
#, fuzzy
msgctxt ""
"sfx.src\n"
"STR_ACCTITLE_PRODUCTIVITYTOOLS\n"
"string.text"
msgid "%PRODUCTNAME"
msgstr "%PRODUCTNAME API"

#: sfx.src
msgctxt ""
"sfx.src\n"
"STR_PASSWD_MIN_LEN\n"
"string.text"
msgid "(Minimum $(MINLEN) characters)"
msgstr ""

#: sfx.src
msgctxt ""
"sfx.src\n"
"STR_PASSWD_MIN_LEN1\n"
"string.text"
msgid "(Minimum 1 character)"
msgstr ""

#: sfx.src
msgctxt ""
"sfx.src\n"
"STR_PASSWD_EMPTY\n"
"string.text"
msgid "(The password can be empty)"
msgstr ""

#: sfx.src
msgctxt ""
"sfx.src\n"
"STR_MODULENOTINSTALLED\n"
"string.text"
msgid "The action could not be executed. The %PRODUCTNAME program module needed for this action is currently not installed."
msgstr ""

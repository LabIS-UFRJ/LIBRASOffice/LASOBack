#. extracted from dictionaries/hu_HU/dialog
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-11-20 13:01+0100\n"
"PO-Revision-Date: 2014-03-26 05:54+0000\n"
"Last-Translator: 琨珑 <suokunlong@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Pootle 2.5.1\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1395813264.000000\n"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"spelling\n"
"property.text"
msgid "Spelling"
msgstr "拼写"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"cap\n"
"property.text"
msgid "Capitalization"
msgstr "大小写"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"par\n"
"property.text"
msgid "Parentheses"
msgstr "括号"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"wordpart\n"
"property.text"
msgid "Word parts of compounds"
msgstr "单词组成部分"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"comma\n"
"property.text"
msgid "Comma usage"
msgstr "逗号用法"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"proofreading\n"
"property.text"
msgid "Proofreading"
msgstr "校对"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"style\n"
"property.text"
msgid "Style checking"
msgstr "样式检查"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"compound\n"
"property.text"
msgid "Underline typo-like compound words"
msgstr "为疑似笔误的组合词添加下划线"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"allcompound\n"
"property.text"
msgid "Underline all generated compound words"
msgstr "为所有自造组合词添加下划线"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"grammar\n"
"property.text"
msgid "Possible mistakes"
msgstr "疑似错误"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"money\n"
"property.text"
msgid "Consistency of money amounts"
msgstr "货币单位一致性"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"duplication\n"
"property.text"
msgid "Word duplication"
msgstr "单词重复"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"dup0\n"
"property.text"
msgid "Word duplication"
msgstr "单词重复"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"dup\n"
"property.text"
msgid "Duplication within clauses"
msgstr "从句内重复"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"dup2\n"
"property.text"
msgid "Duplication within sentences"
msgstr "句内重复"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"dup3\n"
"property.text"
msgid "Allow previous checkings with affixes"
msgstr "允许之前的词缀检查"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"numpart\n"
"property.text"
msgid "Thousand separation of numbers"
msgstr "数字千位分隔"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"typography\n"
"property.text"
msgid "Typography"
msgstr "排版"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"quot\n"
"property.text"
msgid "Quotation marks"
msgstr "引号"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"apost\n"
"property.text"
msgid "Apostrophe"
msgstr "撇号"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"dash\n"
"property.text"
msgid "En dash"
msgstr "短破折号"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"elli\n"
"property.text"
msgid "Ellipsis"
msgstr "省略号"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"ligature\n"
"property.text"
msgid "Ligature suggestion"
msgstr "连字建议"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"noligature\n"
"property.text"
msgid "Underline ligatures"
msgstr "为连字加下划线"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"frac\n"
"property.text"
msgid "Fractions"
msgstr "分数"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"thin\n"
"property.text"
msgid "Thin space"
msgstr "短空格"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"spaces\n"
"property.text"
msgid "Double spaces"
msgstr "双空格"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"spaces2\n"
"property.text"
msgid "More spaces"
msgstr "多余空格"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"idx\n"
"property.text"
msgid "Indices"
msgstr "指数"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"minus\n"
"property.text"
msgid "Minus"
msgstr "减号"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"SI\n"
"property.text"
msgid "Measurements"
msgstr "度量衡"

#: hu_HU_en_US.properties
msgctxt ""
"hu_HU_en_US.properties\n"
"hyphen\n"
"property.text"
msgid "Hyphenation of ambiguous words"
msgstr "模糊词汇断词"

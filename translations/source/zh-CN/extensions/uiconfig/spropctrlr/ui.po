#. extracted from extensions/uiconfig/spropctrlr/ui
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2014-11-18 11:23+0100\n"
"PO-Revision-Date: 2015-01-28 04:34+0000\n"
"Last-Translator: 琨珑 <suokunlong@126.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1422419645.000000\n"

#: controlfontdialog.ui
msgctxt ""
"controlfontdialog.ui\n"
"ControlFontDialog\n"
"title\n"
"string.text"
msgid "Character"
msgstr "字符"

#: controlfontdialog.ui
msgctxt ""
"controlfontdialog.ui\n"
"font\n"
"label\n"
"string.text"
msgid "Font"
msgstr "字体"

#: controlfontdialog.ui
msgctxt ""
"controlfontdialog.ui\n"
"fonteffects\n"
"label\n"
"string.text"
msgid "Font Effects"
msgstr "字体效果"

#: datatypedialog.ui
msgctxt ""
"datatypedialog.ui\n"
"DataTypeDialog\n"
"title\n"
"string.text"
msgid "New Data Type"
msgstr "新建数据类型"

#: datatypedialog.ui
msgctxt ""
"datatypedialog.ui\n"
"label\n"
"label\n"
"string.text"
msgid "Type a name for the new data type:"
msgstr "为新数据类型键入一个名称:"

#: formlinksdialog.ui
msgctxt ""
"formlinksdialog.ui\n"
"FormLinks\n"
"title\n"
"string.text"
msgid "Link fields"
msgstr "链接字段"

#: formlinksdialog.ui
msgctxt ""
"formlinksdialog.ui\n"
"suggestButton\n"
"label\n"
"string.text"
msgid "Suggest"
msgstr "建议"

#: formlinksdialog.ui
msgctxt ""
"formlinksdialog.ui\n"
"explanationLabel\n"
"label\n"
"string.text"
msgid "Sub forms can be used to display detailed data about the current record of the master form. To do this, you can specify which columns in the sub form match which columns in the master form."
msgstr "子窗体可用于显示主窗体当前记录的详细数据。您需要指定子窗体中的哪些字段与主窗体中的哪些字段相匹配。"

#: formlinksdialog.ui
msgctxt ""
"formlinksdialog.ui\n"
"detailLabel\n"
"label\n"
"string.text"
msgid "label"
msgstr "标签"

#: formlinksdialog.ui
msgctxt ""
"formlinksdialog.ui\n"
"masterLabel\n"
"label\n"
"string.text"
msgid "label"
msgstr "标签"

#: labelselectiondialog.ui
msgctxt ""
"labelselectiondialog.ui\n"
"LabelSelectionDialog\n"
"title\n"
"string.text"
msgid "Label Field Selection"
msgstr "选择标签字段"

#: labelselectiondialog.ui
msgctxt ""
"labelselectiondialog.ui\n"
"label\n"
"label\n"
"string.text"
msgid "These are control fields that can be used as label fields for the $controlclass$ $controlname$."
msgstr "这些是可用于标签字段 $control_class$ $control_name$ 的所有控件字段。"

#: labelselectiondialog.ui
msgctxt ""
"labelselectiondialog.ui\n"
"noassignment\n"
"label\n"
"string.text"
msgid "_No assignment"
msgstr "未指定(_N)"

#: taborder.ui
msgctxt ""
"taborder.ui\n"
"TabOrderDialog\n"
"title\n"
"string.text"
msgid "Tab Order"
msgstr "Tab 键次序"

#: taborder.ui
msgctxt ""
"taborder.ui\n"
"upB\n"
"label\n"
"string.text"
msgid "_Move Up"
msgstr "向上(_M)"

#: taborder.ui
msgctxt ""
"taborder.ui\n"
"downB\n"
"label\n"
"string.text"
msgid "Move _Down"
msgstr "向下(_D)"

#: taborder.ui
msgctxt ""
"taborder.ui\n"
"autoB\n"
"label\n"
"string.text"
msgid "_Automatic Sort"
msgstr "自动排序(_A)"

#: taborder.ui
msgctxt ""
"taborder.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Controls"
msgstr "控件"

#. extracted from dbaccess/source/ui/browser
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-05-23 06:33+0000\n"
"Last-Translator: 琨珑 锁 <suokunlong@126.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1463985188.000000\n"

#: sbabrw.src
msgctxt ""
"sbabrw.src\n"
"STR_QUERY_BRW_DELETE_ROWS\n"
"string.text"
msgid "Do you want to delete the selected data?"
msgstr "是否要删除选中的数据？"

#: sbabrw.src
msgctxt ""
"sbabrw.src\n"
"RID_STR_DATABROWSER_FILTERED\n"
"string.text"
msgid "(filtered)"
msgstr "(已经筛选)"

#: sbabrw.src
msgctxt ""
"sbabrw.src\n"
"SBA_BROWSER_SETTING_ORDER\n"
"string.text"
msgid "Error setting the sort criteria"
msgstr "在设置排序条件时发生错误"

#: sbabrw.src
msgctxt ""
"sbabrw.src\n"
"SBA_BROWSER_SETTING_FILTER\n"
"string.text"
msgid "Error setting the filter criteria"
msgstr "在设置筛选条件时发生错误"

#: sbabrw.src
msgctxt ""
"sbabrw.src\n"
"RID_STR_CONNECTION_LOST\n"
"string.text"
msgid "Connection lost"
msgstr "失去连接"

#: sbabrw.src
msgctxt ""
"sbabrw.src\n"
"RID_STR_QUERIES_CONTAINER\n"
"string.text"
msgid "Queries"
msgstr "查询"

#: sbabrw.src
msgctxt ""
"sbabrw.src\n"
"RID_STR_TABLES_CONTAINER\n"
"string.text"
msgid "Tables"
msgstr "表"

#: sbabrw.src
msgctxt ""
"sbabrw.src\n"
"MENU_BROWSER_DEFAULTCONTEXT\n"
"ID_TREE_EDIT_DATABASE\n"
"menuitem.text"
msgid "Edit ~Database File..."
msgstr "编辑数据库文件(~D)..."

#: sbabrw.src
msgctxt ""
"sbabrw.src\n"
"MENU_BROWSER_DEFAULTCONTEXT\n"
"ID_TREE_CLOSE_CONN\n"
"menuitem.text"
msgid "Disco~nnect"
msgstr "断开连接(~N)"

#: sbabrw.src
msgctxt ""
"sbabrw.src\n"
"MENU_BROWSER_DEFAULTCONTEXT\n"
"SID_COPY\n"
"menuitem.text"
msgid "~Copy"
msgstr "复制(~C)"

#: sbabrw.src
msgctxt ""
"sbabrw.src\n"
"MENU_BROWSER_DEFAULTCONTEXT\n"
"ID_TREE_ADMINISTRATE\n"
"menuitem.text"
msgid "Registered databases ..."
msgstr "已注册的数据库..."

#: sbabrw.src
msgctxt ""
"sbabrw.src\n"
"STR_TITLE_CONFIRM_DELETION\n"
"string.text"
msgid "Confirm Deletion"
msgstr "确认删除"

#: sbabrw.src
msgctxt ""
"sbabrw.src\n"
"STR_QUERY_DELETE_TABLE\n"
"string.text"
msgid "Do you want to delete the table '%1'?"
msgstr "是否要删除这个表格 '%1'？"

#: sbabrw.src
msgctxt ""
"sbabrw.src\n"
"STR_QUERY_CONNECTION_LOST\n"
"string.text"
msgid "The connection to the database has been lost. Do you want to reconnect?"
msgstr "和数据库的连接已中断。是否要重新连接？"

#: sbabrw.src
msgctxt ""
"sbabrw.src\n"
"STR_OPENTABLES_WARNINGS\n"
"string.text"
msgid "Warnings encountered"
msgstr "遇到警告"

#: sbabrw.src
msgctxt ""
"sbabrw.src\n"
"STR_OPENTABLES_WARNINGS_DETAILS\n"
"string.text"
msgid "While retrieving the tables, warnings were reported by the database connection."
msgstr "在读取表格时发出一个数据库连接的警告。"

#: sbabrw.src
msgctxt ""
"sbabrw.src\n"
"STR_CONNECTING_DATASOURCE\n"
"string.text"
msgid "Connecting to \"$name$\" ..."
msgstr "和 \"$name$\" 连接..."

#: sbabrw.src
msgctxt ""
"sbabrw.src\n"
"STR_LOADING_QUERY\n"
"string.text"
msgid "Loading query $name$ ..."
msgstr "正在载入查询 $name$ ..."

#: sbabrw.src
msgctxt ""
"sbabrw.src\n"
"STR_LOADING_TABLE\n"
"string.text"
msgid "Loading table $name$ ..."
msgstr "正在载入表格 $name$ ..."

#: sbabrw.src
msgctxt ""
"sbabrw.src\n"
"STR_NO_TABLE_FORMAT_INSIDE\n"
"string.text"
msgid "No table format could be found."
msgstr "未找到任何表格格式。"

#: sbabrw.src
msgctxt ""
"sbabrw.src\n"
"STR_COULDNOTCONNECT_DATASOURCE\n"
"string.text"
msgid "The connection to the data source \"$name$\" could not be established."
msgstr "无法建立指向数据源 $name$ 的连接。"

#: sbagrid.src
msgctxt ""
"sbagrid.src\n"
"RID_SBA_GRID_COLCTXMENU\n"
"ID_BROWSER_COLATTRSET\n"
"menuitem.text"
msgid "Column ~Format..."
msgstr "列格式(~F)..."

#: sbagrid.src
msgctxt ""
"sbagrid.src\n"
"RID_SBA_GRID_COLCTXMENU\n"
"ID_BROWSER_COLWIDTH\n"
"menuitem.text"
msgid "Column ~Width..."
msgstr "列宽...(~W)"

#: sbagrid.src
msgctxt ""
"sbagrid.src\n"
"RID_SBA_GRID_COLCTXMENU\n"
"ID_BROWSER_COLUMNINFO\n"
"menuitem.text"
msgid "Copy Column D~escription"
msgstr "复制列标题(~E)"

#: sbagrid.src
msgctxt ""
"sbagrid.src\n"
"RID_SBA_GRID_ROWCTXMENU\n"
"ID_BROWSER_TABLEATTR\n"
"menuitem.text"
msgid "Table Format..."
msgstr "表格式..."

#: sbagrid.src
msgctxt ""
"sbagrid.src\n"
"RID_SBA_GRID_ROWCTXMENU\n"
"ID_BROWSER_ROWHEIGHT\n"
"menuitem.text"
msgid "Row Height..."
msgstr "行高..."

#: sbagrid.src
msgctxt ""
"sbagrid.src\n"
"RID_SBA_GRID_ROWCTXMENU\n"
"SID_COPY\n"
"menuitem.text"
msgid "~Copy"
msgstr "复制(~O)"

#: sbagrid.src
msgctxt ""
"sbagrid.src\n"
"RID_STR_UNDO_MODIFY_RECORD\n"
"string.text"
msgid "Undo: Data Input"
msgstr "撤消: 输入数据"

#: sbagrid.src
msgctxt ""
"sbagrid.src\n"
"RID_STR_SAVE_CURRENT_RECORD\n"
"string.text"
msgid "Save current record"
msgstr "保存当前的数据条目"

#: sbagrid.src
msgctxt ""
"sbagrid.src\n"
"STR_QRY_TITLE\n"
"string.text"
msgid "Query #"
msgstr "查询 #"

#: sbagrid.src
msgctxt ""
"sbagrid.src\n"
"STR_TBL_TITLE\n"
"string.text"
msgid "Table #"
msgstr "表 #"

#: sbagrid.src
msgctxt ""
"sbagrid.src\n"
"STR_VIEW_TITLE\n"
"string.text"
msgid "View #"
msgstr "视图 #"

#: sbagrid.src
msgctxt ""
"sbagrid.src\n"
"STR_NAME_ALREADY_EXISTS\n"
"string.text"
msgid "The name \"#\" already exists."
msgstr "名称 \"#\" 已经存在。"

#: sbagrid.src
msgctxt ""
"sbagrid.src\n"
"STR_NO_COLUMNNAME_MATCHING\n"
"string.text"
msgid "No matching column names were found."
msgstr "无法找到相符的列名称！"

#: sbagrid.src
msgctxt ""
"sbagrid.src\n"
"STR_ERROR_OCCURRED_WHILE_COPYING\n"
"string.text"
msgid "An error occurred. Do you want to continue copying?"
msgstr "出现一个错误！是否要继续复制？"

#: sbagrid.src
msgctxt ""
"sbagrid.src\n"
"STR_DATASOURCE_GRIDCONTROL_NAME\n"
"string.text"
msgid "Data source table view"
msgstr "数据源的表格视图"

#: sbagrid.src
msgctxt ""
"sbagrid.src\n"
"STR_DATASOURCE_GRIDCONTROL_DESC\n"
"string.text"
msgid "Shows the selected table or query."
msgstr "显示已经选择的表格或查询。"

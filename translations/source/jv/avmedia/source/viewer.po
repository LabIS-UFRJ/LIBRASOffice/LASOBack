#. extracted from avmedia/source/viewer
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-11-10 19:32+0100\n"
"PO-Revision-Date: 2015-11-11 20:52+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: jv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1447275143.000000\n"

#: mediawindow.src
msgctxt ""
"mediawindow.src\n"
"AVMEDIA_STR_INSERTMEDIA_DLG\n"
"string.text"
msgid "Insert Audio or Video"
msgstr ""

#: mediawindow.src
msgctxt ""
"mediawindow.src\n"
"AVMEDIA_STR_OPENMEDIA_DLG\n"
"string.text"
msgid "Open Audio or Video"
msgstr ""

#: mediawindow.src
msgctxt ""
"mediawindow.src\n"
"AVMEDIA_STR_ALL_MEDIAFILES\n"
"string.text"
msgid "All audio and video files"
msgstr ""

#: mediawindow.src
msgctxt ""
"mediawindow.src\n"
"AVMEDIA_STR_ALL_FILES\n"
"string.text"
msgid "All files"
msgstr "Kabèh berkas"

#: mediawindow.src
msgctxt ""
"mediawindow.src\n"
"AVMEDIA_STR_ERR_URL\n"
"string.text"
msgid "The format of the selected file is not supported."
msgstr "Berkas sing dipilih ora didukung."

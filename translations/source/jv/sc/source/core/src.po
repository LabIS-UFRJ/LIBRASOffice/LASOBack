#. extracted from sc/source/core/src
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-04-22 23:40+0200\n"
"PO-Revision-Date: 2013-05-24 07:44+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: jv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1369381474.000000\n"

#: compiler.src
msgctxt ""
"compiler.src\n"
"RID_FUNCTION_CATEGORIES\n"
"1\n"
"string.text"
msgid "Database"
msgstr "Basis data"

#: compiler.src
msgctxt ""
"compiler.src\n"
"RID_FUNCTION_CATEGORIES\n"
"2\n"
"string.text"
msgid "Date&Time"
msgstr "Tanggal&Wektu"

#: compiler.src
msgctxt ""
"compiler.src\n"
"RID_FUNCTION_CATEGORIES\n"
"3\n"
"string.text"
msgid "Financial"
msgstr "Keuangan"

#: compiler.src
msgctxt ""
"compiler.src\n"
"RID_FUNCTION_CATEGORIES\n"
"4\n"
"string.text"
msgid "Information"
msgstr "Informasi"

#: compiler.src
msgctxt ""
"compiler.src\n"
"RID_FUNCTION_CATEGORIES\n"
"5\n"
"string.text"
msgid "Logical"
msgstr "Logika"

#: compiler.src
msgctxt ""
"compiler.src\n"
"RID_FUNCTION_CATEGORIES\n"
"6\n"
"string.text"
msgid "Mathematical"
msgstr "Matematika"

#: compiler.src
msgctxt ""
"compiler.src\n"
"RID_FUNCTION_CATEGORIES\n"
"7\n"
"string.text"
msgid "Array"
msgstr "Larik"

#: compiler.src
msgctxt ""
"compiler.src\n"
"RID_FUNCTION_CATEGORIES\n"
"8\n"
"string.text"
msgid "Statistical"
msgstr "Statistika"

#: compiler.src
msgctxt ""
"compiler.src\n"
"RID_FUNCTION_CATEGORIES\n"
"9\n"
"string.text"
msgid "Spreadsheet"
msgstr "Lembar kerjā"

#: compiler.src
msgctxt ""
"compiler.src\n"
"RID_FUNCTION_CATEGORIES\n"
"10\n"
"string.text"
msgid "Text"
msgstr "Teks"

#: compiler.src
msgctxt ""
"compiler.src\n"
"RID_FUNCTION_CATEGORIES\n"
"11\n"
"string.text"
msgid "Add-in"
msgstr "Pengaya"

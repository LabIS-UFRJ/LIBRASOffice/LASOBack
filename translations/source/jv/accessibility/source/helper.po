#. extracted from accessibility/source/helper
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-04-22 23:40+0200\n"
"PO-Revision-Date: 2017-09-20 02:41+0000\n"
"Last-Translator: Ki Drupadi <kidrupadi@yahoo.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: jv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1505875270.000000\n"

#: accessiblestrings.src
msgctxt ""
"accessiblestrings.src\n"
"RID_STR_ACC_NAME_BROWSEBUTTON\n"
"string.text"
msgid "Browse"
msgstr "Tlusur"

#: accessiblestrings.src
msgctxt ""
"accessiblestrings.src\n"
"STR_SVT_ACC_ACTION_EXPAND\n"
"string.text"
msgid "Expand"
msgstr "Mekaraké"

#: accessiblestrings.src
msgctxt ""
"accessiblestrings.src\n"
"STR_SVT_ACC_ACTION_COLLAPSE\n"
"string.text"
msgid "Collapse"
msgstr "Ciutaké"

#: accessiblestrings.src
msgctxt ""
"accessiblestrings.src\n"
"STR_SVT_ACC_LISTENTRY_SELCTED_STATE\n"
"string.text"
msgid "(Selected)"
msgstr "(Dipilih)"

#: accessiblestrings.src
msgctxt ""
"accessiblestrings.src\n"
"RID_STR_ACC_NAME_PREVIEW\n"
"string.text"
msgid "Preview"
msgstr "Pratudhuh"

#: accessiblestrings.src
msgctxt ""
"accessiblestrings.src\n"
"RID_STR_ACC_ACTION_CHECK\n"
"string.text"
msgid "Check"
msgstr "Cènthang"

#: accessiblestrings.src
msgctxt ""
"accessiblestrings.src\n"
"RID_STR_ACC_ACTION_UNCHECK\n"
"string.text"
msgid "Uncheck"
msgstr "Ora dicènthang"

#: accessiblestrings.src
msgctxt ""
"accessiblestrings.src\n"
"RID_STR_ACC_ACTION_DOUBLE_CLICK\n"
"string.text"
msgid "Double click"
msgstr "Klik dobel"

#: accessiblestrings.src
msgctxt ""
"accessiblestrings.src\n"
"RID_STR_ACC_SCROLLBAR_NAME_VERTICAL\n"
"string.text"
msgid "Vertical scroll bar"
msgstr "Bilah gulir vertikal"

#: accessiblestrings.src
msgctxt ""
"accessiblestrings.src\n"
"RID_STR_ACC_SCROLLBAR_NAME_HORIZONTAL\n"
"string.text"
msgid "Horizontal scroll bar"
msgstr "Bilah gulir horisontal"

#: accessiblestrings.src
msgctxt ""
"accessiblestrings.src\n"
"RID_STR_ACC_PANEL_DESCRIPTION\n"
"string.text"
msgid "Please press enter to go into child control for more operations"
msgstr ""

#: accessiblestrings.src
msgctxt ""
"accessiblestrings.src\n"
"RID_STR_ACC_DESC_PANELDECL_TABBAR\n"
"string.text"
msgid "Panel Deck Tab Bar"
msgstr "Panel Tabulasi"

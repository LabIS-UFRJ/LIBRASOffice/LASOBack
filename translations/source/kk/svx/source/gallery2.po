#. extracted from svx/source/gallery2
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2015-12-11 17:45+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: kk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1449855904.000000\n"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_ACTUALIZE_PROGRESS\n"
"string.text"
msgid "Update"
msgstr "Жаңарту"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_FOPENERROR\n"
"string.text"
msgid "This file cannot be opened"
msgstr "Бұл файлды ашу мүмкін емес"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_NOTHEME\n"
"string.text"
msgid "Invalid Theme Name!"
msgstr "Тема аты қате!"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_EXTFORMAT1_UI\n"
"string.text"
msgid "Wave - Sound File"
msgstr "Wave - дыбыстық файлы"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_EXTFORMAT2_UI\n"
"string.text"
msgid "Audio Interchange File Format"
msgstr "Дыбыстық алмасу файлының пішімі"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_EXTFORMAT3_UI\n"
"string.text"
msgid "AU - Sound File"
msgstr "AU - дыбыстық файлы"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_FILTER\n"
"string.text"
msgid "Graphics filter"
msgstr "Графикалық сүзгі"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_LENGTH\n"
"string.text"
msgid "Length:"
msgstr "Ұзындығы:"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_SIZE\n"
"string.text"
msgid "Size:"
msgstr "Өлшемі:"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_DELETEDD\n"
"string.text"
msgid "Do you want to delete the linked file?"
msgstr "Байланысқан файлды өшіру керек пе?"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_SGIERROR\n"
"string.text"
msgid ""
"This file cannot be opened.\n"
"Do you want to enter a different search path? "
msgstr ""
"Файлды ашу мүмкін емес. \n"
"Басқа іздеу жолын беру керек пе? "

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_NEWTHEME\n"
"string.text"
msgid "New Theme"
msgstr "Жаңа тема"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_BROWSER\n"
"string.text"
msgid "~Organizer..."
msgstr "Ұйы~мдастырушы..."

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_THEMEERR\n"
"string.text"
msgid ""
"This theme name already exists.\n"
"Please choose a different one."
msgstr ""
"Теманың бұл аты бар болып тұр.\n"
"Басқасын таңдаңыз."

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_IMPORTTHEME\n"
"string.text"
msgid "I~mport..."
msgstr "И~мпорттау..."

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_CREATETHEME\n"
"string.text"
msgid "New Theme..."
msgstr "Жаңа тема..."

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_DIALOGID\n"
"string.text"
msgid "Assign ID"
msgstr "ID тағайындау"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_TITLE\n"
"string.text"
msgid "Title"
msgstr "Атауы"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_PATH\n"
"string.text"
msgid "Path"
msgstr "Жолы"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_ICONVIEW\n"
"string.text"
msgid "Icon View"
msgstr "Таңбашалар көрінісі"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_LISTVIEW\n"
"string.text"
msgid "Detailed View"
msgstr "Толығырақ түрі"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY1\n"
"MN_ACTUALIZE\n"
"menuitem.text"
msgid "Update"
msgstr "Жаңарту"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY1\n"
"MN_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr "Ө~шіру"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY1\n"
"MN_RENAME\n"
"menuitem.text"
msgid "~Rename"
msgstr "~Атын өзгерту"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY1\n"
"MN_ASSIGN_ID\n"
"menuitem.text"
msgid "Assign ~ID"
msgstr "ID ~тағайындау"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY1\n"
"MN_PROPERTIES\n"
"menuitem.text"
msgid "Propert~ies..."
msgstr "Қасиет~тері..."

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY2\n"
"MN_ADD\n"
"menuitem.text"
msgid "~Insert"
msgstr "~Кірістіру"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY2\n"
"MN_BACKGROUND\n"
"menuitem.text"
msgid "Insert as Bac~kground"
msgstr "Фон ретінде ~кірістіру"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY2\n"
"MN_PREVIEW\n"
"menuitem.text"
msgid "~Preview"
msgstr "~Алдын-ала қарау"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY2\n"
"MN_TITLE\n"
"menuitem.text"
msgid "~Title"
msgstr "~Тақырыбы"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY2\n"
"MN_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr "Ө~шіру"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY2\n"
"MN_COPYCLIPBOARD\n"
"menuitem.text"
msgid "~Copy"
msgstr "~Көшіру"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY2\n"
"MN_PASTECLIPBOARD\n"
"menuitem.text"
msgid "~Insert"
msgstr "Кірі~стіру"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_3D\n"
"string.text"
msgid "3D Effects"
msgstr "3D эффекттер"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_ANIMATIONS\n"
"string.text"
msgid "Animations"
msgstr "Анимация"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_BULLETS\n"
"string.text"
msgid "Bullets"
msgstr "Маркерлер"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_OFFICE\n"
"string.text"
msgid "Office"
msgstr "Офис"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_FLAGS\n"
"string.text"
msgid "Flags"
msgstr "Жалаушалар"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_FLOWCHARTS\n"
"string.text"
msgid "Flow Charts"
msgstr "Блок схемалар"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_EMOTICONS\n"
"string.text"
msgid "Emoticons"
msgstr "Смайликтер"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_PHOTOS\n"
"string.text"
msgid "Pictures"
msgstr "Фотосуреттер"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_BACKGROUNDS\n"
"string.text"
msgid "Backgrounds"
msgstr "Фондар"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_HOMEPAGE\n"
"string.text"
msgid "Homepage"
msgstr "Үй парағы"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_INTERACTION\n"
"string.text"
msgid "Interaction"
msgstr "Әрекеттесу"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_MAPS\n"
"string.text"
msgid "Maps"
msgstr "Карталар"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_PEOPLE\n"
"string.text"
msgid "People"
msgstr "Адамдар"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_SURFACES\n"
"string.text"
msgid "Surfaces"
msgstr "Беттер"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_COMPUTERS\n"
"string.text"
msgid "Computers"
msgstr "Компьютерлер"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_DIAGRAMS\n"
"string.text"
msgid "Diagrams"
msgstr "Диаграммалар"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_ENVIRONMENT\n"
"string.text"
msgid "Environment"
msgstr "Айнала"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_FINANCE\n"
"string.text"
msgid "Finance"
msgstr "Қаржы"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_TRANSPORT\n"
"string.text"
msgid "Transport"
msgstr "Көлік"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_TXTSHAPES\n"
"string.text"
msgid "Textshapes"
msgstr "Пішіндер"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_SOUNDS\n"
"string.text"
msgid "Sounds"
msgstr "Дыбыстар"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_SYMBOLS\n"
"string.text"
msgid "Symbols"
msgstr "Таңбалар"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_MYTHEME\n"
"string.text"
msgid "My Theme"
msgstr "Менің темам"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_ARROWS\n"
"string.text"
msgid "Arrows"
msgstr "Бағдаршалар"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_BALLOONS\n"
"string.text"
msgid "Balloons"
msgstr "Көмектер"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_KEYBOARD\n"
"string.text"
msgid "Keyboard"
msgstr "Пернетақта"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_TIME\n"
"string.text"
msgid "Time"
msgstr "Уақыт"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_PRESENTATION\n"
"string.text"
msgid "Presentation"
msgstr "Презентация"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_CALENDAR\n"
"string.text"
msgid "Calendar"
msgstr "Күнтізбе"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_NAVIGATION\n"
"string.text"
msgid "Navigation"
msgstr "Навигация"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_COMMUNICATION\n"
"string.text"
msgid "Communication"
msgstr "Байланыс"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_FINANCES\n"
"string.text"
msgid "Finances"
msgstr "Қаржы"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_COMPUTER\n"
"string.text"
msgid "Computers"
msgstr "Компьютерлер"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_CLIMA\n"
"string.text"
msgid "Climate"
msgstr "Климат"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_EDUCATION\n"
"string.text"
msgid "School & University"
msgstr "Мектеп және университет"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_TROUBLE\n"
"string.text"
msgid "Problem Solving"
msgstr "Мәселелерді шешу"

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_SCREENBEANS\n"
"string.text"
msgid "Screen Beans"
msgstr "Кішкентай адамдар"

#. extracted from sc/source/core/src
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-04-22 23:40+0200\n"
"PO-Revision-Date: 2017-01-23 08:29+0000\n"
"Last-Translator: Baurzhan Muftakhidinov <baurthefirst@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: kk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1485160146.000000\n"

#: compiler.src
msgctxt ""
"compiler.src\n"
"RID_FUNCTION_CATEGORIES\n"
"1\n"
"string.text"
msgid "Database"
msgstr "Дерекқор"

#: compiler.src
msgctxt ""
"compiler.src\n"
"RID_FUNCTION_CATEGORIES\n"
"2\n"
"string.text"
msgid "Date&Time"
msgstr "Күн/уақыт"

#: compiler.src
msgctxt ""
"compiler.src\n"
"RID_FUNCTION_CATEGORIES\n"
"3\n"
"string.text"
msgid "Financial"
msgstr "Қаржылық"

#: compiler.src
msgctxt ""
"compiler.src\n"
"RID_FUNCTION_CATEGORIES\n"
"4\n"
"string.text"
msgid "Information"
msgstr "Ақпарат"

#: compiler.src
msgctxt ""
"compiler.src\n"
"RID_FUNCTION_CATEGORIES\n"
"5\n"
"string.text"
msgid "Logical"
msgstr "Логикалық"

#: compiler.src
msgctxt ""
"compiler.src\n"
"RID_FUNCTION_CATEGORIES\n"
"6\n"
"string.text"
msgid "Mathematical"
msgstr "Математикалық"

#: compiler.src
msgctxt ""
"compiler.src\n"
"RID_FUNCTION_CATEGORIES\n"
"7\n"
"string.text"
msgid "Array"
msgstr "Массив"

#: compiler.src
msgctxt ""
"compiler.src\n"
"RID_FUNCTION_CATEGORIES\n"
"8\n"
"string.text"
msgid "Statistical"
msgstr "Статистикалық"

#: compiler.src
msgctxt ""
"compiler.src\n"
"RID_FUNCTION_CATEGORIES\n"
"9\n"
"string.text"
msgid "Spreadsheet"
msgstr "Электрондық кесте"

#: compiler.src
msgctxt ""
"compiler.src\n"
"RID_FUNCTION_CATEGORIES\n"
"10\n"
"string.text"
msgid "Text"
msgstr "Мәтін"

#: compiler.src
msgctxt ""
"compiler.src\n"
"RID_FUNCTION_CATEGORIES\n"
"11\n"
"string.text"
msgid "Add-in"
msgstr "Іске қосылатын модуль"

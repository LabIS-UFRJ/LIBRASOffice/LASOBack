#. extracted from sysui/desktop/share
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-05-07 21:35+0200\n"
"PO-Revision-Date: 2016-05-16 17:23+0000\n"
"Last-Translator: Stanislav Horáček <stanislav.horacek@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.7\n"
"X-POOTLE-MTIME: 1463419406.000000\n"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"text\n"
"LngText.text"
msgid "OpenOffice.org 1.0 Text Document"
msgstr "Textový dokument OpenOffice.org 1.0 "

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"text-template\n"
"LngText.text"
msgid "OpenOffice.org 1.0 Text Document Template"
msgstr "Šablona textového dokumentu OpenOffice.org 1.0"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"master-document\n"
"LngText.text"
msgid "OpenOffice.org 1.0 Master Document"
msgstr "Hlavní dokument OpenOffice.org 1.0"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"formula\n"
"LngText.text"
msgid "OpenOffice.org 1.0 Formula"
msgstr "Vzorec OpenOffice.org 1.0"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"presentation\n"
"LngText.text"
msgid "OpenOffice.org 1.0 Presentation"
msgstr "Prezentace OpenOffice.org 1.0"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"presentation-template\n"
"LngText.text"
msgid "OpenOffice.org 1.0 Presentation Template"
msgstr "Šablona prezentace OpenOffice.org 1.0"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"drawing\n"
"LngText.text"
msgid "OpenOffice.org 1.0 Drawing"
msgstr "Kresba OpenOffice.org 1.0"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"drawing-template\n"
"LngText.text"
msgid "OpenOffice.org 1.0 Drawing Template"
msgstr "Šablona kresby OpenOffice.org 1.0"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"spreadsheet\n"
"LngText.text"
msgid "OpenOffice.org 1.0 Spreadsheet"
msgstr "Sešit OpenOffice.org 1.0"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"spreadsheet-template\n"
"LngText.text"
msgid "OpenOffice.org 1.0 Spreadsheet Template"
msgstr "Šablona sešitu OpenOffice.org 1.0"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"oasis-text\n"
"LngText.text"
msgid "OpenDocument Text"
msgstr "Textový dokument OpenDocument"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"oasis-text-flat-xml\n"
"LngText.text"
msgid "OpenDocument Text (Flat XML)"
msgstr "Textový dokument OpenDocument (Flat XML)"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"oasis-text-template\n"
"LngText.text"
msgid "OpenDocument Text Template"
msgstr "Šablona textového dokumentu OpenDocument"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"oasis-master-document\n"
"LngText.text"
msgid "OpenDocument Master Document"
msgstr "Hlavní dokument OpenDocument"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"oasis-master-document-template\n"
"LngText.text"
msgid "OpenDocument Master Document Template"
msgstr "Šablona hlavního dokumentu OpenDocument"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"oasis-formula\n"
"LngText.text"
msgid "OpenDocument Formula"
msgstr "Vzorec OpenDocument"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"oasis-presentation\n"
"LngText.text"
msgid "OpenDocument Presentation"
msgstr "Prezentace OpenDocument"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"oasis-presentation-flat-xml\n"
"LngText.text"
msgid "OpenDocument Presentation (Flat XML)"
msgstr "Prezentace OpenDocument (Flat XML)"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"oasis-presentation-template\n"
"LngText.text"
msgid "OpenDocument Presentation Template"
msgstr "Šablona prezentace OpenDocument"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"oasis-drawing\n"
"LngText.text"
msgid "OpenDocument Drawing"
msgstr "Kresba OpenDocument"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"oasis-drawing-flat-xml\n"
"LngText.text"
msgid "OpenDocument Drawing (Flat XML)"
msgstr "Kresba OpenDocument (Flat XML)"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"oasis-drawing-template\n"
"LngText.text"
msgid "OpenDocument Drawing Template"
msgstr "Šablona kresby OpenDocument"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"oasis-spreadsheet\n"
"LngText.text"
msgid "OpenDocument Spreadsheet"
msgstr "Sešit OpenDocument"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"oasis-spreadsheet-flat-xml\n"
"LngText.text"
msgid "OpenDocument Spreadsheet (Flat XML)"
msgstr "Sešit OpenDocument (Flat XML)"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"oasis-spreadsheet-template\n"
"LngText.text"
msgid "OpenDocument Spreadsheet Template"
msgstr "Šablona sešitu OpenDocument"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"oasis-database\n"
"LngText.text"
msgid "OpenDocument Database"
msgstr "Databáze OpenDocument"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"oasis-web-template\n"
"LngText.text"
msgid "HTML Document Template"
msgstr "Šablona HTML dokumentu"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"extension\n"
"LngText.text"
msgid "%PRODUCTNAME Extension"
msgstr "Rozšíření %PRODUCTNAME"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"ms-excel-sheet\n"
"LngText.text"
msgid "Microsoft Excel Worksheet"
msgstr "Pracovní sešit Microsoft Excel"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"ms-powerpoint-presentation\n"
"LngText.text"
msgid "Microsoft PowerPoint Presentation"
msgstr "Prezentace Microsoft PowerPoint"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"ms-word-document\n"
"LngText.text"
msgid "Microsoft Word Document"
msgstr "Dokument Microsoft Word"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"ms-word-document2\n"
"LngText.text"
msgid "Microsoft Word Document"
msgstr "Dokument Microsoft Word"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"ms-excel-sheet-12\n"
"LngText.text"
msgid "Microsoft Excel Worksheet"
msgstr "Pracovní sešit Microsoft Excel"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"ms-excel-template-12\n"
"LngText.text"
msgid "Microsoft Excel Worksheet Template"
msgstr "Šablona pracovního sešitu Microsoft Excel"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"ms-powerpoint-presentation-12\n"
"LngText.text"
msgid "Microsoft PowerPoint Presentation"
msgstr "Prezentace Microsoft PowerPoint"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"ms-powerpoint-template-12\n"
"LngText.text"
msgid "Microsoft PowerPoint Presentation Template"
msgstr "Šablona prezentace Microsoft PowerPoint"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"ms-word-document-12\n"
"LngText.text"
msgid "Microsoft Word Document"
msgstr "Dokument Microsoft Word"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"ms-word-template-12\n"
"LngText.text"
msgid "Microsoft Word Document Template"
msgstr "Šablona dokumentu Microsoft Word"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"openxmlformats-officedocument-presentationml-presentation\n"
"LngText.text"
msgid "Microsoft PowerPoint Presentation"
msgstr "Prezentace Microsoft PowerPoint"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"openxmlformats-officedocument-presentationml-template\n"
"LngText.text"
msgid "Microsoft PowerPoint Presentation Template"
msgstr "Šablona prezentace Microsoft PowerPoint"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"openxmlformats-officedocument-spreadsheetml-sheet\n"
"LngText.text"
msgid "Microsoft Excel Worksheet"
msgstr "Pracovní sešit Microsoft Excel"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"openxmlformats-officedocument-spreadsheetml-template\n"
"LngText.text"
msgid "Microsoft Excel Worksheet Template"
msgstr "Šablona pracovního sešitu Microsoft Excel"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"openxmlformats-officedocument-wordprocessingml-document\n"
"LngText.text"
msgid "Microsoft Word Document"
msgstr "Dokument Microsoft Word"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"openxmlformats-officedocument-wordprocessingml-template\n"
"LngText.text"
msgid "Microsoft Word Document Template"
msgstr "Šablona dokumentu Microsoft Word"

#: documents.ulf
msgctxt ""
"documents.ulf\n"
"ms-excel-sheet-binary-12\n"
"LngText.text"
msgid "Microsoft Excel Worksheet"
msgstr "Pracovní sešit Microsoft Excel"

#: launcher_comment.ulf
msgctxt ""
"launcher_comment.ulf\n"
"writer\n"
"LngText.text"
msgid "Create and edit text and images in letters, reports, documents and Web pages by using Writer."
msgstr "Writer umožňuje vytvářet a upravovat text a grafiku v dopisech, sestavách, dokumentech a webových stránkách."

#: launcher_comment.ulf
msgctxt ""
"launcher_comment.ulf\n"
"impress\n"
"LngText.text"
msgid "Create and edit presentations for slideshows, meeting and Web pages by using Impress."
msgstr "Impress umožňuje vytvářet a upravovat prezentace pro přednášky, porady a webové stránky."

#: launcher_comment.ulf
msgctxt ""
"launcher_comment.ulf\n"
"draw\n"
"LngText.text"
msgid "Create and edit drawings, flow charts, and logos by using Draw."
msgstr "Draw umožňuje vytvářet a upravovat kresby, vývojové diagramy a loga."

#: launcher_comment.ulf
msgctxt ""
"launcher_comment.ulf\n"
"calc\n"
"LngText.text"
msgid "Perform calculations, analyze information and manage lists in spreadsheets by using Calc."
msgstr "Calc umožňuje vytvářet a spravovat tabulkové seznamy a provádět na těchto datech výpočty a analýzu informací."

#: launcher_comment.ulf
msgctxt ""
"launcher_comment.ulf\n"
"math\n"
"LngText.text"
msgid "Create and edit scientific formulas and equations by using Math."
msgstr "Math umožňuje vytvářet a upravovat vědecké vzorce a rovnice."

#: launcher_comment.ulf
msgctxt ""
"launcher_comment.ulf\n"
"base\n"
"LngText.text"
msgid "Manage databases, create queries and reports to track and manage your information by using Base."
msgstr "Base umožňuje vytvářet a spravovat databáze a vytvářet dotazy a sestavy pro sledování a správu vašich informací."

#: launcher_comment.ulf
msgctxt ""
"launcher_comment.ulf\n"
"startcenter\n"
"LngText.text"
msgid "The office productivity suite compatible to the open and standardized ODF document format. Supported by The Document Foundation."
msgstr "Kancelářský balík kompatibilní s otevřeným a standardizovaným formátem dokumentů ODF. Podporován The Document Foundation."

#: launcher_genericname.ulf
msgctxt ""
"launcher_genericname.ulf\n"
"writer\n"
"LngText.text"
msgid "Word Processor"
msgstr "Textový procesor"

#: launcher_genericname.ulf
msgctxt ""
"launcher_genericname.ulf\n"
"impress\n"
"LngText.text"
msgid "Presentation"
msgstr "Prezentace"

#: launcher_genericname.ulf
msgctxt ""
"launcher_genericname.ulf\n"
"calc\n"
"LngText.text"
msgid "Spreadsheet"
msgstr "Sešit"

#: launcher_genericname.ulf
msgctxt ""
"launcher_genericname.ulf\n"
"base\n"
"LngText.text"
msgid "Database Development"
msgstr "Vývoj databáze"

#: launcher_genericname.ulf
msgctxt ""
"launcher_genericname.ulf\n"
"math\n"
"LngText.text"
msgid "Formula Editor"
msgstr "Editor vzorců"

#: launcher_genericname.ulf
msgctxt ""
"launcher_genericname.ulf\n"
"draw\n"
"LngText.text"
msgid "Drawing Program"
msgstr "Kreslící program"

#: launcher_genericname.ulf
msgctxt ""
"launcher_genericname.ulf\n"
"startcenter\n"
"LngText.text"
msgid "Office"
msgstr "Kancelář"

#: launcher_genericname.ulf
msgctxt ""
"launcher_genericname.ulf\n"
"xsltfilter\n"
"LngText.text"
msgid "XSLT based filters"
msgstr "Filtry používající XSLT"

#: launcher_unityquicklist.ulf
msgctxt ""
"launcher_unityquicklist.ulf\n"
"writer\n"
"LngText.text"
msgid "New Document"
msgstr "Nový dokument"

#: launcher_unityquicklist.ulf
msgctxt ""
"launcher_unityquicklist.ulf\n"
"impress\n"
"LngText.text"
msgid "New Presentation"
msgstr "Nová prezentace"

#: launcher_unityquicklist.ulf
msgctxt ""
"launcher_unityquicklist.ulf\n"
"calc\n"
"LngText.text"
msgid "New Spreadsheet"
msgstr "Nový sešit"

#: launcher_unityquicklist.ulf
msgctxt ""
"launcher_unityquicklist.ulf\n"
"base\n"
"LngText.text"
msgid "New Database"
msgstr "Nová databáze"

#: launcher_unityquicklist.ulf
msgctxt ""
"launcher_unityquicklist.ulf\n"
"math\n"
"LngText.text"
msgid "New Formula"
msgstr "Nový vzorec"

#: launcher_unityquicklist.ulf
msgctxt ""
"launcher_unityquicklist.ulf\n"
"draw\n"
"LngText.text"
msgid "New Drawing"
msgstr "Nová kresba"

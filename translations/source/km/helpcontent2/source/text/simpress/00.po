#. extracted from helpcontent2/source/text/simpress/00
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-07-06 10:06+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: km\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1467799579.000000\n"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"tit\n"
"help.text"
msgid "To access this command"
msgstr "ដើម្បី​ចូល​ដំណើរ​ការ​ពាក្យ​បញ្ជា​នេះ"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"hd_id3149655\n"
"1\n"
"help.text"
msgid "<variable id=\"wie\">To access this command </variable>"
msgstr "<variable id=\"wie\">ដើម្បី​ចូល​ដំណើរការ​ពាក្យ​បញ្ជា​នេះ </variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id8789025\n"
"help.text"
msgid "<ahelp hid=\".\" visibility=\"hidden\">Opens a dialog to save the selected bitmap picture as a file. The default file format is the internal format of the image.</ahelp>"
msgstr "<ahelp hid=\".\" visibility=\"hidden\">បើក​ប្រអប់​មួយ​ដើម្បី​រក្សាទុក​រូបភាព bitmap ​ជា​ឯកសារមួយ ។ ទ្រង់ទ្រាយ​ឯកសារ​លំនាំដើម​ គឺជាទ្រង់ទ្រាយ​ខាងក្នុង​របស់រូបភាព ។</ahelp>"

#: 00000004.xhp
#, fuzzy
msgctxt ""
"00000004.xhp\n"
"par_id5316324\n"
"help.text"
msgid "<ahelp hid=\".\" visibility=\"hidden\">Opens the Custom Animation sidebar deck.</ahelp>"
msgstr "<ahelp hid=\".\" visibility=\"hidden\">បើក​បង្អួច​ចលនា​ផ្ទាល់​ខ្លួន​នៅ​លើ​ស្ថាបព្រិល​​ភារកិច្ច ។</ahelp>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3147435\n"
"help.text"
msgid "<image id=\"img_id3156441\" src=\"cmd/sc_rect.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3156441\">Icon</alt></image>"
msgstr "<image id=\"img_id3156441\" src=\"cmd/sc_rect.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3156441\">រូបតំណាង</alt></image>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3145801\n"
"2\n"
"help.text"
msgid "Rectangle"
msgstr "ចតុកោណកែង"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3150749\n"
"help.text"
msgid "<image id=\"img_id3155065\" src=\"cmd/sc_ellipse.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3155065\">Icon</alt></image>"
msgstr "<image id=\"img_id3155065\" src=\"cmd/sc_ellipse.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3155065\">រូបតំណាង</alt></image>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3147344\n"
"3\n"
"help.text"
msgid "Ellipse"
msgstr "រាង​ពង​ក្រពើ"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3147004\n"
"help.text"
msgid "<image id=\"img_id3159236\" src=\"cmd/sc_linetoolbox.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3159236\">Icon</alt></image>"
msgstr "<image id=\"img_id3159236\" src=\"cmd/sc_linetoolbox.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3159236\">រូបភាព</alt></image>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3150202\n"
"4\n"
"help.text"
msgid "Curve"
msgstr "ខ្សែ​កោង"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3156449\n"
"help.text"
msgid "<image id=\"img_id3149409\" src=\"cmd/sc_objectalign.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3149409\">Icon</alt></image>"
msgstr "<image id=\"img_id3149409\" src=\"cmd/sc_objectalign.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3149409\">រូបតំណាង</alt></image>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3157979\n"
"5\n"
"help.text"
msgid "Alignment"
msgstr "​តម្រឹម"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3159208\n"
"help.text"
msgid "<image id=\"img_id3159231\" src=\"cmd/sc_bringtofront.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3159231\">Icon</alt></image>"
msgstr "<image id=\"img_id3159231\" src=\"cmd/sc_bringtofront.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3159231\">រូបតំណាង</alt></image>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3153013\n"
"6\n"
"help.text"
msgid "Arrange"
msgstr "រៀប​ចំ"

#: 00000401.xhp
msgctxt ""
"00000401.xhp\n"
"tit\n"
"help.text"
msgid "File Menu"
msgstr "ម៉ឺនុយ ឯកសារ"

#: 00000401.xhp
msgctxt ""
"00000401.xhp\n"
"hd_id3153188\n"
"1\n"
"help.text"
msgid "File Menu"
msgstr "ម៉ឺនុយ ឯកសារ"

#: 00000401.xhp
msgctxt ""
"00000401.xhp\n"
"par_id3146974\n"
"2\n"
"help.text"
msgid "<variable id=\"dtvlc\">Choose <emph>File - Export</emph></variable>"
msgstr "<variable id=\"dtvlc\">ជ្រើស​ <emph>ឯកសារ​ - នាំ​ចេញ</emph></variable>"

#: 00000402.xhp
msgctxt ""
"00000402.xhp\n"
"tit\n"
"help.text"
msgid "Edit Menu"
msgstr "ម៉ឺនុយ កែ​សម្រួល"

#: 00000402.xhp
msgctxt ""
"00000402.xhp\n"
"hd_id3150792\n"
"1\n"
"help.text"
msgid "Edit Menu"
msgstr "ម៉ឺនុយ កែ​សម្រួល"

#: 00000402.xhp
msgctxt ""
"00000402.xhp\n"
"par_id3145171\n"
"2\n"
"help.text"
msgid "Choose <emph>Edit - Duplicate</emph>"
msgstr "ជ្រើស​ <emph>កែសម្រួល​ - ច្បាប់​ចម្លង</emph>"

#: 00000402.xhp
msgctxt ""
"00000402.xhp\n"
"par_id3156441\n"
"7\n"
"help.text"
msgid "Shift+F3"
msgstr "​ប្តូរ ​(Shift)​+F3"

#: 00000402.xhp
msgctxt ""
"00000402.xhp\n"
"par_id3149263\n"
"3\n"
"help.text"
msgid "<variable id=\"bearbueber\">Choose <emph>Edit - Cross-fading</emph> (<item type=\"productname\">%PRODUCTNAME</item> Draw only) </variable>"
msgstr "<variable id=\"bearbueber\">ជ្រើស <emph>កែសម្រួល - លេច​កាត់​បន្តិច​ម្តងៗ</emph> (<item type=\"productname\">%PRODUCTNAME</item>តែ Draw​)</variable>"

#: 00000402.xhp
msgctxt ""
"00000402.xhp\n"
"par_id3149666\n"
"4\n"
"help.text"
msgid "<variable id=\"basl\">Choose <emph>Edit - Delete Slide</emph></variable>"
msgstr "<variable id=\"basl\">ជ្រើស <emph>កែសម្រួល - លុប​ស្លាយ</emph></variable>"

#: 00000402.xhp
msgctxt ""
"00000402.xhp\n"
"par_id3147397\n"
"5\n"
"help.text"
msgid "<variable id=\"baebl\">Open the context menu of an inserted layer, then choose <emph>Delete Layer</emph></variable>"
msgstr "<variable id=\"baebl\">បើក​ម៉ឺនុយ​បរិបទ​របស់​ស្រទាប់​ដែល​បានបញ្ចូល បន្ទាប់មកជ្រើស <emph>លុប​ស្រទាប់</emph></variable>"

#: 00000402.xhp
msgctxt ""
"00000402.xhp\n"
"par_id3155603\n"
"6\n"
"help.text"
msgid "<variable id=\"feldbefehl\">Choose <emph>Edit - Fields</emph></variable>"
msgstr "<variable id=\"feldbefehl\">ជ្រើស​ <emph>កែសម្រួល​ - វាល</emph></variable>"

#: 00000402.xhp
msgctxt ""
"00000402.xhp\n"
"par_id8695944\n"
"help.text"
msgid "<variable id=\"gluebar\">Click the <emph>Glue Points</emph> icon on the Drawing Bar </variable>"
msgstr "<variable id=\"gluebar\">ចុច​រូបតំណាង <emph>ចំណុច​ភ្ជាប់</emph> លើ​របារ​គូរ</variable>"

#: 00000403.xhp
msgctxt ""
"00000403.xhp\n"
"tit\n"
"help.text"
msgid "View Menu"
msgstr "ម៉ឺនុយ មើល"

#: 00000403.xhp
#, fuzzy
msgctxt ""
"00000403.xhp\n"
"hd_id3150542\n"
"help.text"
msgid "View Menu"
msgstr "ម៉ឺនុយ មើល"

#: 00000403.xhp
#, fuzzy
msgctxt ""
"00000403.xhp\n"
"par_id3148798\n"
"help.text"
msgid "<variable id=\"aslal\">Choose <emph>View - Rulers</emph> </variable>"
msgstr "<variable id=\"aslal\">ជ្រើស<emph>មើល​ - បន្ទាត់</emph></variable>"

#: 00000403.xhp
#, fuzzy
msgctxt ""
"00000403.xhp\n"
"par_id3153771\n"
"help.text"
msgid "<variable id=\"option\">Choose <emph>View - Toolbars - Options</emph> </variable>"
msgstr "<variable id=\"option\">ជ្រើស <emph>មើល - របារ​ឧបករណ៍ - ជម្រើស</emph></variable>"

#: 00000403.xhp
#, fuzzy
msgctxt ""
"00000403.xhp\n"
"par_id3152576\n"
"help.text"
msgid "Choose <emph>View - Toolbars - Presentation</emph>"
msgstr "ជ្រើស​ <emph>មើល​ - របារ​ឧបករណ៍​ - ការ​បង្ហាញ</emph>"

#: 00000403.xhp
#, fuzzy
msgctxt ""
"00000403.xhp\n"
"par_id3146316\n"
"help.text"
msgid "<variable id=\"quali\">Choose <emph>View - Color/Grayscale</emph> </variable>"
msgstr "<variable id=\"quali\">ជ្រើស​ <emph>​មើល​ - ពណ៌​/​​មាត្រ​ដ្ឋាន​ប្រផេះ</emph></variable>"

#: 00000403.xhp
#, fuzzy
msgctxt ""
"00000403.xhp\n"
"par_id3166426\n"
"help.text"
msgid "Choose <emph>View - Normal</emph>"
msgstr "ជ្រើស​ <emph>មើល​ - ធម្មតា</emph>"

#: 00000403.xhp
#, fuzzy
msgctxt ""
"00000403.xhp\n"
"par_id3157982\n"
"help.text"
msgid "Choose <emph>View - Outline</emph>"
msgstr "ជ្រើស​ <emph>មើល​ - គ្រោង</emph>"

#: 00000403.xhp
#, fuzzy
msgctxt ""
"00000403.xhp\n"
"par_id3149875\n"
"help.text"
msgid "Choose <emph>View - Slide Sorter</emph>"
msgstr "ជ្រើស​ <emph>មើល​ - ឧបករណ៍​តម្រៀប​ស្លាយ</emph>"

#: 00000403.xhp
#, fuzzy
msgctxt ""
"00000403.xhp\n"
"par_id3149352\n"
"help.text"
msgid "Choose <emph>View - Notes </emph>"
msgstr "ជ្រើស​ <emph>មើល​ - ធម្មតា</emph>"

#: 00000403.xhp
#, fuzzy
msgctxt ""
"00000403.xhp\n"
"par_id3155255\n"
"help.text"
msgid "Choose <emph>View - Handout Master</emph>"
msgstr "ជ្រើស​ <emph>មើល​ - ទំព័រ​ប្លង់​បោះ​ពុម្ព</emph>"

#: 00000403.xhp
#, fuzzy
msgctxt ""
"00000403.xhp\n"
"par_id3154328\n"
"help.text"
msgid "Choose <emph>Slide Show - Slide Show</emph>"
msgstr "ជ្រើស​ <emph>បញ្ចាំង​ស្លាយ​ - បញ្ចាំង​ស្លាយ</emph>"

#: 00000403.xhp
#, fuzzy
msgctxt ""
"00000403.xhp\n"
"par_id3150134\n"
"help.text"
msgid "F5"
msgstr "F5"

#: 00000403.xhp
#, fuzzy
msgctxt ""
"00000403.xhp\n"
"par_id3145244\n"
"help.text"
msgid "On the <emph>Standard</emph> toolbar, click"
msgstr "លើ​របារ​ឧបករណ៍ <emph>បញ្ចូល</emph> ចុច"

#: 00000403.xhp
msgctxt ""
"00000403.xhp\n"
"par_id3148768\n"
"help.text"
msgid "<image id=\"img_id3148774\" src=\"cmd/sc_presentation.png\"><alt id=\"alt_id3148774\">Icon</alt></image>"
msgstr ""

#: 00000403.xhp
#, fuzzy
msgctxt ""
"00000403.xhp\n"
"par_id3151016\n"
"help.text"
msgid "Slide Show"
msgstr "បញ្ចាំង​ស្លាយ"

#: 00000403.xhp
#, fuzzy
msgctxt ""
"00000403.xhp\n"
"par_id3153719\n"
"help.text"
msgid "Choose <emph>View - Normal</emph>"
msgstr "ជ្រើស​ <emph>មើល​ - ធម្មតា</emph>"

#: 00000403.xhp
#, fuzzy
msgctxt ""
"00000403.xhp\n"
"par_id3151264\n"
"help.text"
msgid "Choose <emph>View - Master</emph>"
msgstr "ជ្រើស​ <emph>មើល​ - មេ</emph>"

#: 00000403.xhp
#, fuzzy
msgctxt ""
"00000403.xhp\n"
"par_idN10AF7\n"
"help.text"
msgid "<variable id=\"masterlayouts\">Choose <emph>View - Slide Master </emph></variable>"
msgstr "<variable id=\"masterlayouts\">ជ្រើស <emph>មើល - មេ - មេ​ស្លាយ </emph></variable>"

#: 00000403.xhp
#, fuzzy
msgctxt ""
"00000403.xhp\n"
"par_idN10B19\n"
"help.text"
msgid "<variable id=\"notesmaster\">Choose <emph>View - Notes Master</emph> </variable>"
msgstr "<variable id=\"notesmaster\">ជ្រើស <emph>មើល - មេ - ចំណាំ​មេ</emph></variable>"

#: 00000403.xhp
#, fuzzy
msgctxt ""
"00000403.xhp\n"
"par_idN10B07\n"
"help.text"
msgid "<variable id=\"master\">Choose <emph>View - Master Elements</emph> </variable>"
msgstr "<variable id=\"master\">ជ្រើស <emph>មើល - មេ - ធាតុ​មេ</emph></variable>"

#: 00000403.xhp
#, fuzzy
msgctxt ""
"00000403.xhp\n"
"par_idN10B57\n"
"help.text"
msgid "Choose <emph>Insert - Header and Footer</emph>"
msgstr "ជ្រើស <emph>មើល - បឋមកថា និង​បាតកថា</emph>"

#: 00000403.xhp
msgctxt ""
"00000403.xhp\n"
"par_idN10B6E\n"
"help.text"
msgid "Choose <emph>Insert - Page number</emph>"
msgstr "ជ្រើស <emph>បញ្ចូល​ - លេខ​ទំព័រ</emph>"

#: 00000403.xhp
msgctxt ""
"00000403.xhp\n"
"par_idN10B74\n"
"help.text"
msgid "Choose <emph>Insert - Date and time</emph>"
msgstr "ជ្រើស <emph>​បញ្ចូល​ - កាល​បរិច្ឆេទ​ និង​ ពេល​វេលា</emph>"

#: 00000403.xhp
#, fuzzy
msgctxt ""
"00000403.xhp\n"
"par_id3149286\n"
"help.text"
msgid "<variable id=\"hinterzeichnung\">Choose <emph>View - Normal</emph> </variable>"
msgstr "<variable id=\"hinterzeichnung\">ជ្រើស <emph>មើល - ធម្មតា</emph></variable>"

#: 00000403.xhp
#, fuzzy
msgctxt ""
"00000403.xhp\n"
"par_id3153480\n"
"help.text"
msgid "<variable id=\"master_drawing\">Choose <emph>View - Slide Master</emph> </variable>"
msgstr "<variable id=\"master_drawing\">ជ្រើស<emph>មើល​ - មេ​ - មេ​ស្លា​យ</emph></variable>"

#: 00000403.xhp
#, fuzzy
msgctxt ""
"00000403.xhp\n"
"par_id3147254\n"
"help.text"
msgid "<variable id=\"hinternotizen\">Choose <emph>View - Notes</emph> </variable>"
msgstr "<variable id=\"hinternotizen\">ជ្រើស​ <emph>មើល​ - ទំព័រ​ចំណាំ</emph></variable>"

#: 00000404.xhp
msgctxt ""
"00000404.xhp\n"
"tit\n"
"help.text"
msgid "Insert Menu"
msgstr "ម៉ឺនុយ បញ្ចូល"

#: 00000404.xhp
msgctxt ""
"00000404.xhp\n"
"hd_id3143219\n"
"1\n"
"help.text"
msgid "Insert Menu"
msgstr "ម៉ឺនុយ បញ្ចូល"

#: 00000404.xhp
#, fuzzy
msgctxt ""
"00000404.xhp\n"
"par_id3147264\n"
"help.text"
msgid "Choose <emph>Insert - New Page/Slide</emph>"
msgstr "ជ្រើស <emph>បញ្ចូល - ស្លាយ</emph>"

#: 00000404.xhp
msgctxt ""
"00000404.xhp\n"
"par_id3155064\n"
"5\n"
"help.text"
msgid "<variable id=\"seiteduplizieren\">Choose <emph>Insert - Duplicate Slide</emph></variable>"
msgstr "<variable id=\"seiteduplizieren\">ជ្រើស <emph>បញ្ចូល - ស្ទួន​ស្លាយ</emph></variable>"

#: 00000404.xhp
msgctxt ""
"00000404.xhp\n"
"par_id3153711\n"
"6\n"
"help.text"
msgid "<variable id=\"seitegliederung\">Choose <emph>Insert - Expand Slide</emph></variable>"
msgstr "<variable id=\"seitegliederung\">ជ្រើស<emph>បញ្ចូល​ - ពង្រីក​ស្លាយ</emph></variable>"

#: 00000404.xhp
msgctxt ""
"00000404.xhp\n"
"par_id3154254\n"
"7\n"
"help.text"
msgid "<variable id=\"uebersicht\">Choose <emph>Insert - Summary Slide</emph></variable>"
msgstr "<variable id=\"uebersicht\">ជ្រើស​ <emph>បញ្ចូល​ - ស្លាយ​សង្ខេប</emph></variable>"

#: 00000404.xhp
msgctxt ""
"00000404.xhp\n"
"par_id3147002\n"
"8\n"
"help.text"
msgid "Choose <emph>Insert - Layer</emph> (<item type=\"productname\">%PRODUCTNAME</item> Draw only)"
msgstr "ជ្រើស <emph>បញ្ចូល - ស្រទាប់</emph> (<item type=\"productname\">%PRODUCTNAME</item> តែ Draw)"

#: 00000404.xhp
msgctxt ""
"00000404.xhp\n"
"par_id3150363\n"
"27\n"
"help.text"
msgid "Open context menu of layer tabs - choose <emph>Insert Layer</emph> (<item type=\"productname\">%PRODUCTNAME</item> Draw only)"
msgstr "បើក​ម៉ឹនុយ​បរិបទ​នៃ​ផ្ទាំង​ស្រទាប់ - ជ្រើស <emph>បញ្ចូល​ស្រទាប់</emph> (<item type=\"productname\">%PRODUCTNAME</item> តែ Draw)"

#: 00000404.xhp
msgctxt ""
"00000404.xhp\n"
"par_id3155376\n"
"9\n"
"help.text"
msgid "Choose <emph>Insert - Insert Snap Point/Line</emph> (<item type=\"productname\">%PRODUCTNAME</item> Draw only)"
msgstr "ជ្រើស <emph>បញ្ចូល - បញ្ចូល​ចំណុច/បន្ទាត់​ខ្ទាស់</emph> (តែ <item type=\"productname\">%PRODUCTNAME</item> Draw ប៉ុណ្ណោះ)"

#: 00000404.xhp
msgctxt ""
"00000404.xhp\n"
"par_id3154372\n"
"28\n"
"help.text"
msgid "Open a context menu and choose <emph>Insert Snap Point/Line</emph>"
msgstr "បើក​ម៉ឺនុយ​បរិបទ ហើយ​ជ្រើស <emph>បញ្ចូល​ចំណុច/បន្ទាត់​ខ្ទាស់</emph>"

#: 00000404.xhp
msgctxt ""
"00000404.xhp\n"
"par_id3145388\n"
"10\n"
"help.text"
msgid "<variable id=\"efglbe\">Select a snap point or line, open the context menu, and choose <emph>Edit Snap Point/Line</emph></variable>"
msgstr "<variable id=\"efglbe\">ជ្រើស​ចំណុច ឬ​បន្ទាត់​ខ្ទាស់​មួយ បើក​ម៉ឺនុយ​បរិបទ និង ជ្រើស <emph>កែសម្រួល ចំណុច/បន្ទាត់ ខ្ទាស់</emph></variable>>"

#: 00000404.xhp
msgctxt ""
"00000404.xhp\n"
"par_id3151239\n"
"11\n"
"help.text"
msgid "Choose <emph>Insert - Spreadsheet</emph>"
msgstr "ជ្រើស​ <emph>បញ្ចូល​ - សៀវភៅ​បញ្ជី</emph>"

#: 00000404.xhp
msgctxt ""
"00000404.xhp\n"
"par_id3144769\n"
"12\n"
"help.text"
msgid "On the <emph>Insert</emph> toolbar, click"
msgstr "លើ​របារ​ឧបករណ៍ <emph>បញ្ចូល</emph> ចុច"

#: 00000404.xhp
msgctxt ""
"00000404.xhp\n"
"par_id3163703\n"
"help.text"
msgid "<image id=\"img_id3145361\" src=\"cmd/sc_grid.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3145361\">Icon</alt></image>"
msgstr "<image id=\"img_id3145361\" src=\"cmd/sc_grid.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3145361\">រូបតំណាង</alt></image>"

#: 00000404.xhp
msgctxt ""
"00000404.xhp\n"
"par_id3146963\n"
"13\n"
"help.text"
msgid "Spreadsheet"
msgstr "សៀវភៅ​បញ្ជី"

#: 00000404.xhp
msgctxt ""
"00000404.xhp\n"
"par_id3153075\n"
"14\n"
"help.text"
msgid "Choose <emph>Insert - File</emph>"
msgstr "ជ្រើស​ <emph>បញ្ចូល​ - ឯកសារ</emph>"

#: 00000404.xhp
msgctxt ""
"00000404.xhp\n"
"par_id3153958\n"
"15\n"
"help.text"
msgid "On the <emph>Insert</emph> toolbar, click"
msgstr "លើ​របារ​ឧបករណ៍ <emph>បញ្ចូល</emph> ចុច"

#: 00000404.xhp
msgctxt ""
"00000404.xhp\n"
"par_id3156397\n"
"help.text"
msgid "<image id=\"img_id3145237\" src=\"cmd/sc_inserttoolbox.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3145237\">Icon</alt></image>"
msgstr "<image id=\"img_id3145237\" src=\"cmd/sc_inserttoolbox.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3145237\">រូបតំណាង</alt></image>"

#: 00000404.xhp
msgctxt ""
"00000404.xhp\n"
"par_id3157900\n"
"16\n"
"help.text"
msgid "File"
msgstr "ឯកសារ"

#: 00000404.xhp
#, fuzzy
msgctxt ""
"00000404.xhp\n"
"par_id3149351\n"
"help.text"
msgid "<variable id=\"feldbf\">Choose <emph>Insert - Field</emph></variable>"
msgstr "<variable id=\"feldbf\">ជ្រើស <emph>បញ្ចូល​ - វាល</emph></variable>"

#: 00000404.xhp
#, fuzzy
msgctxt ""
"00000404.xhp\n"
"par_id3150477\n"
"help.text"
msgid "<variable id=\"feldbf1\">Choose <emph>Insert - Field - Date (fixed)</emph></variable>"
msgstr "<variable id=\"feldbf1\">​​ជ្រើស <emph>​បញ្ចូល - វាល​​ - កាល​បរិច្ឆេទ​ (​ថេរ​)</emph></variable>"

#: 00000404.xhp
#, fuzzy
msgctxt ""
"00000404.xhp\n"
"par_id3146879\n"
"help.text"
msgid "<variable id=\"feldbf2\">Choose <emph>Insert - Field - Date (variable)</emph></variable>"
msgstr "<variable id=\"feldbf2\">ជ្រើស <emph>បញ្ចូល​ - វាល​​ - កាល​បរិច្ឆេទ​ (អថេរ​)</emph></variable>"

#: 00000404.xhp
#, fuzzy
msgctxt ""
"00000404.xhp\n"
"par_id3153036\n"
"help.text"
msgid "<variable id=\"feldbf3\">Choose <emph>Insert - Field - Time (fixed)</emph></variable>"
msgstr "<variable id=\"feldbf3\">ជ្រើស <emph>បញ្ចូល​ - វាល​ - ពេលវេលា​ (ថេរ​)</emph></variable>"

#: 00000404.xhp
#, fuzzy
msgctxt ""
"00000404.xhp\n"
"par_id3145590\n"
"help.text"
msgid "<variable id=\"feldbf4\">Choose <emph>Insert - Field - Time (variable)</emph></variable>"
msgstr "<variable id=\"feldbf4\">ជ្រើស <emph>បញ្ចូល​ - វាល​​ - ពេល​វេលា ​(អថេរ​)</emph></variable>"

#: 00000404.xhp
#, fuzzy
msgctxt ""
"00000404.xhp\n"
"par_id3153935\n"
"help.text"
msgid "<variable id=\"feldbf5\">Choose <emph>Insert - Field - Page Number</emph></variable>"
msgstr "<variable id=\"feldbf7\">ជ្រើស​ <emph>បញ្ចូល​ - វាល​ - ឈ្មោះ​ឯកសារ</emph></variable>"

#: 00000404.xhp
#, fuzzy
msgctxt ""
"00000404.xhp\n"
"par_id3148583\n"
"help.text"
msgid "<variable id=\"feldbf6\">Choose <emph>Insert - Field - Author</emph></variable>"
msgstr "<variable id=\"feldbf6\">ជ្រើស <emph>បញ្ចូល​ - វាល​ - អ្នកនិពន្ធ</emph></variable>"

#: 00000404.xhp
#, fuzzy
msgctxt ""
"00000404.xhp\n"
"par_id3155951\n"
"help.text"
msgid "<variable id=\"feldbf7\">Choose <emph>Insert - Field - File Name</emph></variable>"
msgstr "<variable id=\"feldbf7\">ជ្រើស​ <emph>បញ្ចូល​ - វាល​ - ឈ្មោះ​ឯកសារ</emph></variable>"

#: 00000405.xhp
msgctxt ""
"00000405.xhp\n"
"tit\n"
"help.text"
msgid "Format Menu"
msgstr "ម៉ឺនុយ ទ្រង់ទ្រាយ"

#: 00000405.xhp
msgctxt ""
"00000405.xhp\n"
"hd_id3147001\n"
"1\n"
"help.text"
msgid "Format Menu"
msgstr "ម៉ឺនុយ ទ្រង់ទ្រាយ"

#: 00000405.xhp
msgctxt ""
"00000405.xhp\n"
"par_id3148489\n"
"2\n"
"help.text"
msgid "In the context menu of a dimension line, choose <emph>Dimensions</emph>."
msgstr "នៅក្នុង​ម៉ឺនុយ​បរិបទ​នៃ​បន្ទាត់​វិមាត្រ ជ្រើស <emph>វិមាត្រ</emph> ។"

#: 00000405.xhp
msgctxt ""
"00000405.xhp\n"
"par_id3150207\n"
"15\n"
"help.text"
msgid "On the <emph>Lines and Arrows</emph> toolbar, click the <emph>Dimension Line</emph> icon."
msgstr "នៅ​លើ​របារ​ឧបករណ៍​ <emph>បន្ទាត់ និង​ព្រួញ</emph> ចុច​រូបតំណាង <emph>បន្ទាត់​វិមាត្រ</emph> ។"

#: 00000405.xhp
msgctxt ""
"00000405.xhp\n"
"par_id3155530\n"
"9\n"
"help.text"
msgid "<variable id=\"frtite\">Choose <emph>Format - Page</emph></variable>"
msgstr "<variable id=\"frtite\">ជ្រើស​ <emph>ទ្រង់​ទ្រាយ​ - ទំព័រ</emph></variable>"

#: 00000405.xhp
msgctxt ""
"00000405.xhp\n"
"par_id3145386\n"
"13\n"
"help.text"
msgid "<variable id=\"frtites\">Choose <emph>Format - Page</emph> and then click the <emph>Page</emph> tab</variable>"
msgstr "<variable id=\"frtites\">ជ្រើស <emph>ទ្រង់ទ្រាយ - ទំព័រ</emph> ហើយ​បន្ទាប់​មក​ចុច​​ផ្ទាំង <emph>ទំព័រ</emph></variable>"

#: 00000405.xhp
msgctxt ""
"00000405.xhp\n"
"par_id3148866\n"
"14\n"
"help.text"
msgid "<variable id=\"frtiteh\">Choose <emph>Format - Page</emph> and then click the <emph>Background</emph> tab</variable>"
msgstr "<variable id=\"frtiteh\">ជ្រើស <emph>ទ្រង់ទ្រាយ - ទំព័រ</emph> ហើយ​បន្ទាប់​មក​ចុច​ផ្ទាំង<emph>ផ្ទៃខាង​ក្រោយ</emph> ។</variable>"

#: 00000405.xhp
msgctxt ""
"00000405.xhp\n"
"par_id3155266\n"
"10\n"
"help.text"
msgid "<variable id=\"adnsei\">Choose <emph>Format - Slide Layout</emph></variable>"
msgstr "<variable id=\"adnsei\">ជ្រើស <emph>ទ្រង់ទ្រាយ - ប្លង់​ស្លាយ</emph></variable>"

#: 00000405.xhp
msgctxt ""
"00000405.xhp\n"
"par_id3152874\n"
"11\n"
"help.text"
msgid "In a Draw document, right-click a layer tab and choose <emph>Modify Layer</emph>"
msgstr "ក្នុង​ឯកសារ Draw មួយ ចុច​កណ្តុរ​ស្តាំ​លើ​ផ្ទាំង​ស្រទាប់​មួយ និង ជ្រើស <emph>កែប្រែ​ស្រទាប់</emph>"

#: 00000405.xhp
msgctxt ""
"00000405.xhp\n"
"par_id3154765\n"
"17\n"
"help.text"
msgid "Choose <emph>Format - Layer</emph> (only $[officename] Draw)"
msgstr "ជ្រើស <emph>ទ្រង់ទ្រាយ - ស្រទាប់</emph> (តែ $[officename] Draw)"

#: 00000405.xhp
msgctxt ""
"00000405.xhp\n"
"par_id3153012\n"
"12\n"
"help.text"
msgid "<variable id=\"seitenvorlage\">Choose <emph>Slide - Slide Master Design</emph></variable>"
msgstr ""

#: 00000406.xhp
msgctxt ""
"00000406.xhp\n"
"tit\n"
"help.text"
msgid "Tools Menu"
msgstr "ម៉ឺនុយ ឧបករណ៍"

#: 00000406.xhp
msgctxt ""
"00000406.xhp\n"
"hd_id3153770\n"
"1\n"
"help.text"
msgid "Tools Menu"
msgstr "ម៉ឺនុយ ឧបករណ៍"

#: 00000406.xhp
msgctxt ""
"00000406.xhp\n"
"par_id3153727\n"
"2\n"
"help.text"
msgid "<variable id=\"silbentrennung\">Choose <emph>Tools - Language - Hyphenation</emph></variable>"
msgstr "<variable id=\"silbentrennung\">ជ្រើស <emph>ឧបករណ៍ - ភាសា - ការ​ដាក់​សហសញ្ញា</emph></variable>"

#: 00000406.xhp
msgctxt ""
"00000406.xhp\n"
"par_id3163803\n"
"22\n"
"help.text"
msgid "<variable id=\"neuprae\">Choose <emph>Slide Show - Custom Slide Show</emph> and then click <emph>New</emph>.</variable>"
msgstr "<variable id=\"neuprae\">ជ្រើស <emph>បញ្ចាំង​ស្លាយ - បញ្ចាំង​ស្លាយ​ផ្ទាល់​ខ្លួន</emph> ហើយ​បន្ទាប់​មក​ចុច <emph>ថ្មី</emph> ។</variable>"

#: 00000407.xhp
msgctxt ""
"00000407.xhp\n"
"tit\n"
"help.text"
msgid "Slide Show Menu"
msgstr "ម៉ឺនុយ​បញ្ចាំង​ស្លាយ​"

#: 00000407.xhp
msgctxt ""
"00000407.xhp\n"
"hd_id3150541\n"
"1\n"
"help.text"
msgid "Slide Show Menu"
msgstr "ម៉ឺនុយ​បញ្ចាំង​ស្លាយ​"

#: 00000407.xhp
msgctxt ""
"00000407.xhp\n"
"par_id3158394\n"
"2\n"
"help.text"
msgid "<variable id=\"etdaw\">Choose <emph>Slide Show - Slide Transition</emph></variable>"
msgstr "<variable id=\"etdaw\">ជ្រើស​ <emph>បញ្ចាំង​ស្លាយ​ - ចលនា​​ស្លាយ</emph></variable>"

#: 00000407.xhp
msgctxt ""
"00000407.xhp\n"
"par_id3152576\n"
"3\n"
"help.text"
msgid "Choose <emph>Insert - Animated Image</emph>"
msgstr "ជ្រើស​ <emph>​​បញ្ចូល​ - រូបភាព​មាន​ចលនា</emph>"

#: 00000407.xhp
msgctxt ""
"00000407.xhp\n"
"par_id3149262\n"
"6\n"
"help.text"
msgid "Choose <emph>Slide Show - Custom Animation</emph>"
msgstr "ជ្រើស <emph>បញ្ចាំង​ស្លាយ - ចលនា​ផ្ទាល់​ខ្លួន</emph>"

#: 00000407.xhp
msgctxt ""
"00000407.xhp\n"
"par_id3146976\n"
"7\n"
"help.text"
msgid "On the <emph>Drawing</emph> toolbar, click"
msgstr "លើ​របារ​ឧបករណ៍ <emph>គូរ</emph> ចុច"

#: 00000407.xhp
msgctxt ""
"00000407.xhp\n"
"par_id3155603\n"
"help.text"
msgid "<image id=\"img_id3149400\" src=\"cmd/sc_customanimation.png\" width=\"0.1665inch\" height=\"0.1665inch\"><alt id=\"alt_id3149400\">Icon</alt></image>"
msgstr "<image id=\"img_id3149400\" src=\"cmd/sc_customanimation.png\" width=\"0.1665inch\" height=\"0.1665inch\"><alt id=\"alt_id3149400\">រូបតំណាង</alt></image>"

#: 00000407.xhp
msgctxt ""
"00000407.xhp\n"
"par_id3154754\n"
"8\n"
"help.text"
msgid "Custom Animation"
msgstr "ចលនា​ផ្ទាល់ខ្លួន"

#: 00000407.xhp
msgctxt ""
"00000407.xhp\n"
"par_id3146316\n"
"9\n"
"help.text"
msgid "Choose <emph>Slide Show - Interaction</emph>"
msgstr "ជ្រើស​ <emph>បញ្ចាំង​ស្លាយ​ - អន្តរ​កម្ម</emph>"

#: 00000407.xhp
msgctxt ""
"00000407.xhp\n"
"par_id3149257\n"
"10\n"
"help.text"
msgid "On the <emph>Drawing</emph> toolbar, click"
msgstr "លើ​របារ​ឧបករណ៍ <emph>គូរ</emph> ចុច"

#: 00000407.xhp
msgctxt ""
"00000407.xhp\n"
"par_id3154649\n"
"help.text"
msgid "<image id=\"img_id3150205\" src=\"cmd/sc_animationeffects.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3150205\">Icon</alt></image>"
msgstr "<image id=\"img_id3150205\" src=\"cmd/sc_animationeffects.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3150205\">រូបតំណាង</alt></image>"

#: 00000407.xhp
msgctxt ""
"00000407.xhp\n"
"par_id3155380\n"
"11\n"
"help.text"
msgid "Interaction"
msgstr "អន្តរកម្ម"

#: 00000407.xhp
msgctxt ""
"00000407.xhp\n"
"par_id3152987\n"
"12\n"
"help.text"
msgid "<variable id=\"praesent\">Choose <emph>Slide Show - Slide Show Settings</emph></variable>"
msgstr "<variable id=\"praesent\">​ជ្រើស​ <emph>បញ្ចាំង​ស្លាយ​ - កំណត់​ការ​បញ្ចាំង​ស្លាយ</emph></variable>"

#: 00000407.xhp
msgctxt ""
"00000407.xhp\n"
"par_id3155089\n"
"13\n"
"help.text"
msgid "<variable id=\"indipra\">Choose <emph>Slide Show - Custom Slide Show</emph></variable>"
msgstr "<variable id=\"indipra\">​ជ្រើស​ <emph>​បញ្ចាំង​ស្លាយ​ - បញ្ចាំង​ស្លាយ​ផ្ទាល់​ខ្លួន</emph></variable>"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"tit\n"
"help.text"
msgid "Modify Menu"
msgstr "ម៉ឺនុយ​កែប្រែ​"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"hd_id3152578\n"
"1\n"
"help.text"
msgid "Modify Menu"
msgstr "ម៉ឺនុយ​កែប្រែ​"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3151075\n"
"2\n"
"help.text"
msgid "Choose <emph>Modify - Convert </emph>(<item type=\"productname\">%PRODUCTNAME</item> Draw only)"
msgstr "ជ្រើស <emph>កែប្រែ - បម្លែង</emph>(<item type=\"productname\">%PRODUCTNAME</item>​ តែ Draw​)"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3153415\n"
"46\n"
"help.text"
msgid "Open the context menu of a selected object and choose <emph>Convert</emph>"
msgstr "បើក​ម៉ឺនុយ​បរិបទ​នៃ​វត្ថុ​ដែល​បាន​ជ្រើស​ និង ជ្រើស​ <emph>បម្លែង</emph>"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3149124\n"
"3\n"
"help.text"
msgid "Choose <emph>Modify - Convert - To Curve</emph> (<item type=\"productname\">%PRODUCTNAME</item> Draw only)"
msgstr "ជ្រើស​ <emph>​កែប្រែ​ - បម្លែង​ - ជា​ខ្សែ​កោង</emph> (<item type=\"productname\">%PRODUCTNAME</item> តែ​ Draw)"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3149018\n"
"27\n"
"help.text"
msgid "Open the context menu of a selected object and choose <emph>Convert - To Curve</emph>"
msgstr "បើក​ម៉ឺនុយ​បរិបទ​នៃ​វត្ថុ​ដែល​បាន​ជ្រើស​ និង​ ជ្រើស​ <emph>បម្លែង​ - ​ជា​ខ្សែ​កោង</emph>"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3156384\n"
"4\n"
"help.text"
msgid "Choose <emph>Modify - Convert - To Polygon</emph> (<item type=\"productname\">%PRODUCTNAME</item> Draw only)"
msgstr "ជ្រើស​ <emph>កែប្រែ​ - បម្លែង​ - ​ជា​ពហុកោណ</emph> (<item type=\"productname\">%PRODUCTNAME</item> តែ​ Draw​)"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3154702\n"
"26\n"
"help.text"
msgid "Open the context menu of a selected object and choose <emph>Convert - To Polygon</emph>"
msgstr "បើក​ម៉ឺនុយ​បរិបទ​នៃ​វត្ថុ​ដែល​បាន​ជ្រើស​ និង ជ្រើស <emph>បម្លែង​ - ​ជា​ពហុកោណ</emph>"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3147001\n"
"5\n"
"help.text"
msgid "Choose <emph>Modify - Convert - To 3D</emph> (<item type=\"productname\">%PRODUCTNAME</item> Draw only)"
msgstr "ជ្រើស​ <emph>​​កែប្រែ​ - បម្លែង​ - ​ជា​ត្រីមាត្រ</emph> (<item type=\"productname\">%PRODUCTNAME</item>​ ​តែ​​ Draw )​"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3155111\n"
"28\n"
"help.text"
msgid "Open the context menu of a selected object and choose <emph>Convert - To 3D </emph>"
msgstr "បើក​ម៉ឺនុយ​បរិបទ​នៃ​វត្ថុ​ដែល​បាន​ជ្រើស​ និង​ ជ្រើស​ <emph>បម្លែង​ - ​ជា​ត្រីមាត្រ</emph>"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3150205\n"
"6\n"
"help.text"
msgid "Choose <emph>Modify - Convert - To 3D Rotation Object</emph> (<item type=\"productname\">%PRODUCTNAME</item> Draw only)"
msgstr "ជ្រើស​ <emph>​កែប្រែ​ - បម្លែង​ - ​វត្ថុ​វិល​ត្រីមាត្រ</emph> (<item type=\"productname\">%PRODUCTNAME</item> ​តែ​ Draw)"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3152992\n"
"29\n"
"help.text"
msgid "Open the context menu of a selected object and choose <emph>Convert - To 3D Rotation Body</emph>"
msgstr "បើក​ម៉ឺនុយ​បរិបទ​នៃ​វត្ថុ​ដែល​បាន​ជ្រើស និង ជ្រើស <emph>បម្លែង - ​តួ​វិល​ត្រីមាត្រ</emph>"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3152986\n"
"33\n"
"help.text"
msgid "Choose <emph>Modify - Convert - To Bitmap</emph> (<item type=\"productname\">%PRODUCTNAME</item> Draw only)"
msgstr "ជ្រើស​ <emph>កែប្រែ​ - បម្លែង​ - ​ជា​រូប​ភាព</emph> (<item type=\"productname\">%PRODUCTNAME</item> តែ​ Draw​)"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3149409\n"
"34\n"
"help.text"
msgid "Open the context menu of a selected object and choose <emph>Convert - To Bitmap</emph>"
msgstr "បើក​ម៉ឺនុយ​បរិបទ​នៃ​វត្ថុ​ដែល​បាន​ជ្រើស និង ជ្រើស <emph>​បម្លែង - ជា​រូបភាព</emph>"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3148870\n"
"35\n"
"help.text"
msgid "Choose <emph>Modify - Convert - To Metafile</emph> (<item type=\"productname\">%PRODUCTNAME</item> Draw only)"
msgstr "ជ្រើស <emph>កែប្រែ​ - បម្លែង​ - ​ជា​ឯកសារ​មេតា</emph>(<item type=\"productname\">%PRODUCTNAME</item> តែ Draw)"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3148608\n"
"36\n"
"help.text"
msgid "Open the context menu of a selected object and choose <emph>Convert - To Metafile</emph>"
msgstr "បើក​ម៉ឺនុយ​បរិបទ​នៃ​វត្ថុ​ដែល​បាន​ជ្រើស និង ជ្រើស <emph>បម្លែង - ​ឯកសារ​​មេតា</emph>"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3153246\n"
"41\n"
"help.text"
msgid "Choose <emph>Modify - Convert - To Contour</emph> (<item type=\"productname\">%PRODUCTNAME</item> Draw only)"
msgstr "ជ្រើស​ <emph>កែប្រែ - បម្លែង​ - ​ជា​វណ្ឌ​វង្ក</emph> (<item type=\"productname\">%PRODUCTNAME</item> ​តែ​ Draw​)"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3159231\n"
"42\n"
"help.text"
msgid "Open the context menu of a selected object and choose <emph>Convert - To Contour</emph>"
msgstr "បើក​ម៉ឺនុយ​បរិបទ​នៃ​វត្ថុ​ដែល​បាន​ជ្រើស និង ជ្រើស <emph>បម្លែង - ជា​វណ្ឌវង្ក</emph>"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3153008\n"
"7\n"
"help.text"
msgid "Choose <emph>Modify - Arrange - In Front of Object</emph> (<item type=\"productname\">%PRODUCTNAME</item> Draw only)"
msgstr "ជ្រើស​ <emph>កែប្រែ - រៀបចំ - ពីមុខ​វត្ថុ</emph> (<item type=\"productname\">%PRODUCTNAME</item> ​តែ​ Draw)"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3145117\n"
"30\n"
"help.text"
msgid "Open the context menu of a selected object and choose <emph>Arrange - In Front of Object</emph>"
msgstr "បើក​ម៉ឺនុយ​​បរិបទ​នៃ​វត្ថុ​ដែល​បាន​ជ្រើស និង ជ្រើស <emph>​​រៀបចំ - ខាង​មុខ​វត្ថុ</emph>"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3147249\n"
"8\n"
"help.text"
msgid "On the Drawing bar, open the <emph>Arrange</emph> toolbar and click:"
msgstr "លើ​របារ​គូរ បើក​របារ​ឧបករណ៍ <emph>រៀបចំ</emph> និង ចុច ៖"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3150537\n"
"help.text"
msgid "<image id=\"img_id3145233\" src=\"cmd/sc_beforeobject.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3145233\">Icon</alt></image>"
msgstr "<image id=\"img_id3145233\" src=\"cmd/sc_beforeobject.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3145233\">រូបតំណាង</alt></image>"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3153121\n"
"9\n"
"help.text"
msgid "In Front of Object"
msgstr "ខាង​មុខ​វត្ថុ"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3150654\n"
"10\n"
"help.text"
msgid "Choose <emph>Modify - Arrange - Behind Object</emph> (<item type=\"productname\">%PRODUCTNAME</item> Draw only)"
msgstr "ជ្រើស​ <emph>​កែប្រែ​ - រៀបចំ​ - ពីក្រោយ​វត្ថុ</emph> (<item type=\"productname\">%PRODUCTNAME</item>តែ​ Draw )"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3150482\n"
"31\n"
"help.text"
msgid "Open the context menu of a selected object and choose <emph>Arrange - Behind Object</emph>"
msgstr "បើក​ម៉ឺនុយ​បរិបទ​នៃ​វត្ថុ​ដែល​បាន​ជ្រើស និង ជ្រើស <emph>​រៀបចំ - ក្រោយ​វត្ថុ</emph>"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3149886\n"
"11\n"
"help.text"
msgid "On the Drawing bar, open the <emph>Arrange</emph> toolbar and click:"
msgstr "លើ​របារ​គូរ បើក​របារ​ឧបករណ៍ <emph>រៀបចំ</emph> និង ចុច ៖"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3150865\n"
"help.text"
msgid "<image id=\"img_id3145597\" src=\"cmd/sc_behindobject.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3145597\">Icon</alt></image>"
msgstr "<image id=\"img_id3145597\" src=\"cmd/sc_behindobject.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3145597\">រូបតំណាង</alt></image>"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3153110\n"
"12\n"
"help.text"
msgid "Behind Object"
msgstr "ខាង​ក្រោយ​វត្ថុ"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3150002\n"
"13\n"
"help.text"
msgid "Choose <emph>Modify - Arrange - Reverse</emph> (<item type=\"productname\">%PRODUCTNAME</item> Draw only)"
msgstr "ជ្រើស <emph>កែប្រែ​ - រៀបចំ​ - បញ្ច្រាស</emph> (<item type=\"productname\">%PRODUCTNAME</item> ​តែ​ Draw )"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3150339\n"
"32\n"
"help.text"
msgid "Open the context menu of a selected object and choose <emph>Arrange - Reverse</emph>"
msgstr "បើក​ម៉ឺនុយ​បរិបទ​នៃ​វត្ថុ​ដែល​បាន​ជ្រើស និង​ ជ្រើស <emph>​​រៀបចំ - ដាក់​បញ្ច្រាស</emph>"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3145164\n"
"14\n"
"help.text"
msgid "On the Drawing bar, open the <emph>Arrange</emph> toolbar and click:"
msgstr "លើ​របារ​គូរ បើក​របារ​ឧបករណ៍ <emph>រៀបចំ</emph> និង ចុច ៖"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3154327\n"
"help.text"
msgid "<image id=\"img_id3155439\" src=\"cmd/sc_reverseorder.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3155439\">Icon</alt></image>"
msgstr "<image id=\"img_id3155439\" src=\"cmd/sc_reverseorder.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3155439\">រូបតំណាង</alt></image>"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3150272\n"
"15\n"
"help.text"
msgid "Reverse"
msgstr "បញ្ច្រាស"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3145298\n"
"21\n"
"help.text"
msgid "Choose <emph>Modify - Combine</emph> (<item type=\"productname\">%PRODUCTNAME</item> Draw only)"
msgstr "ជ្រើស​ <emph>​កែប្រែ​ - ផ្សំ</emph> (<item type=\"productname\">%PRODUCTNAME</item>តែ​​ Draw)"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3148386\n"
"47\n"
"help.text"
msgid "Select two or more objects, open the context menu and choose <emph>Combine</emph>."
msgstr "ជ្រើស​វត្ថុ​ពីរ​ ឬ​ច្រើន​ បើក​ម៉ឺនុយ​បរិបទ​ និង​ ជ្រើស​​ <emph>ផ្សំ</emph> ។"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3150930\n"
"22\n"
"help.text"
msgid "Choose <emph>Modify - Split</emph> (<item type=\"productname\">%PRODUCTNAME</item> Draw only)"
msgstr "ជ្រើស​ <emph>​កែប្រែ - ពុះ</emph> (<item type=\"productname\">%PRODUCTNAME</item> ​តែ​ Draw​)"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3151022\n"
"48\n"
"help.text"
msgid "Select a combined object, open the context menu and choose <emph>Split</emph>."
msgstr "ជ្រើស​វត្ថុ​ដែល​បាន​ផ្សំ  បើក​ម៉ឺនុយ​បរិបទ​ និង​ ជ្រើស​ <emph>​ពុះ</emph> ។"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3154872\n"
"23\n"
"help.text"
msgid "Choose <emph>Modify - Connect</emph> (<item type=\"productname\">%PRODUCTNAME</item> Draw only)"
msgstr "ជ្រើស​ <emph>​កែប្រែ​ - តភ្ជាប់</emph> (<item type=\"productname\">%PRODUCTNAME</item> តែ​ Draw​)"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3150470\n"
"49\n"
"help.text"
msgid "Select two or more lines, open the context menu and choose <emph>Connect</emph>."
msgstr "ជ្រើស​បន្ទាត់​ពីរ​ ឬ​ ច្រើន​ បើក​ម៉ឺនុយ​បរិបទ​ ហើយ​ជ្រើស​ <emph>​តភ្ជាប់</emph> ។"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3153920\n"
"24\n"
"help.text"
msgid "Choose <emph>Modify - Break</emph> (<item type=\"productname\">%PRODUCTNAME</item> Draw only)"
msgstr "ជ្រើស​ <emph>កែប្រែ - បំបែក</emph> (<item type=\"productname\">%PRODUCTNAME</item> ​តែ​ Draw)"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3148430\n"
"50\n"
"help.text"
msgid "Select a line that was created by connecting two or more lines, open the context menu and choose <emph>Break</emph>."
msgstr "ជ្រើស​បន្ទាត់​មួយ ​​​ដែល​ត្រូវ​បាន​បង្កើត​ដោយការ​​តភ្ជាប់​បន្ទាត់​ពីរ​ ឬ​ច្រើន​ បើក​ម៉ឺនុយ​បរិបទ​ និង​ ជ្រើស​ <emph>បំបែក</emph> ។"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3155408\n"
"37\n"
"help.text"
msgid "Choose <emph>Modify - Shapes</emph> (<item type=\"productname\">%PRODUCTNAME</item> Draw only)"
msgstr "ជ្រើស​ <emph>កែប្រែ - រាង</emph> (<item type=\"productname\">%PRODUCTNAME</item> តែ​​ Draw​)"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3145615\n"
"51\n"
"help.text"
msgid "Select two or more objects, open the context menu and choose <emph>Shapes</emph>"
msgstr "ជ្រើស​វត្ថុ​​ពីរ ឬ​ច្រើន បើក​ម៉ឺនុយ​បរិបទ និង ជ្រើស <emph>​​រាង</emph>​"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3163822\n"
"38\n"
"help.text"
msgid "Choose <emph>Modify - Shapes - Merge</emph> (<item type=\"productname\">%PRODUCTNAME</item> Draw only)"
msgstr "ជ្រើស​ <emph>កែប្រែ​ - រាង - បញ្ចូល​គ្នា</emph> (<item type=\"productname\">%PRODUCTNAME</item> តែ Draw​)"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3156309\n"
"43\n"
"help.text"
msgid "Select two or more objects, open the context menu and choose <emph>Shapes - Merge</emph>"
msgstr "ជ្រើស​វត្ថុ​ពីរ ឬ​ច្រើន បើក​ម៉ឺនុយ​​បរិបទ និង ជ្រើស <emph>រាង - បញ្ចូល​ចូល​គ្នា</emph>"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3150874\n"
"39\n"
"help.text"
msgid "Choose <emph>Modify - Shapes - Subtract</emph> (<item type=\"productname\">%PRODUCTNAME</item> Draw only)"
msgstr "ជ្រើស​ <emph>កែប្រែ​ - រាង​ - ដក</emph> (<item type=\"productname\">%PRODUCTNAME</item> តែ​​ Draw​)"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3154643\n"
"44\n"
"help.text"
msgid "Select two or more objects, open the context menu and choose <emph>Shapes - Subtract</emph>"
msgstr "ជ្រើស​វត្ថុ​ពីរ ឬ​ច្រើន បើក​ម៉ឺនុយ​បរិបទ និង ជ្រើស<emph>​រាង - ដក</emph>"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3145204\n"
"40\n"
"help.text"
msgid "Choose <emph>Modify - Shapes - Intersect</emph> (<item type=\"productname\">%PRODUCTNAME</item> Draw only)"
msgstr "ជ្រើស​ <emph>កែប្រែ​ - រាង​ - ប្រសព្វ</emph> (<item type=\"productname\">%PRODUCTNAME</item> ​តែ​​ Draw​)"

#: 00000413.xhp
msgctxt ""
"00000413.xhp\n"
"par_id3152931\n"
"45\n"
"help.text"
msgid "Select two or more objects, open the context menu and choose <emph>Shapes - Intersect</emph>"
msgstr "ជ្រើស​វត្ថុ​ពីរ ឬ​ច្រើន បើក​ម៉ឺនុយ​បរិបទ និង ជ្រើស <emph>រាង - ប្រសព្វ</emph>"

#: slide_menu.xhp
msgctxt ""
"slide_menu.xhp\n"
"tit\n"
"help.text"
msgid "Slide Menu"
msgstr ""

#: slide_menu.xhp
#, fuzzy
msgctxt ""
"slide_menu.xhp\n"
"par_id3134264\n"
"help.text"
msgid "Choose <emph>Slide - New Page/Slide</emph>"
msgstr "ជ្រើស <emph>បញ្ចូល - ស្លាយ</emph>"

#: slide_menu.xhp
#, fuzzy
msgctxt ""
"slide_menu.xhp\n"
"par_id3177597\n"
"help.text"
msgid "On the <emph>Presentation</emph> bar, click"
msgstr "លើ​របារ <emph>ការ​បង្ហាញ</emph> ចុច"

#: slide_menu.xhp
#, fuzzy
msgctxt ""
"slide_menu.xhp\n"
"par_id3685251\n"
"help.text"
msgid "<image id=\"img_id3183073\" src=\"cmd/sc_insertdoc.png\" width=\"0.566cm\" height=\"0.566cm\"><alt id=\"alt_id3183073\">Icon</alt></image>"
msgstr "<image id=\"img_id3151073\" src=\"cmd/sc_insertdoc.png\" width=\"0.222inch\" height=\"0.222inch\"><alt id=\"alt_id3151073\">រូបតំណាង</alt></image>"

#: slide_menu.xhp
msgctxt ""
"slide_menu.xhp\n"
"par_id7354512\n"
"help.text"
msgid "New Page/Slide"
msgstr ""

#. extracted from helpcontent2/source/text/schart/00
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-11-10 19:33+0100\n"
"PO-Revision-Date: 2014-07-08 04:30+0000\n"
"Last-Translator: Sutha <sutha@open.org.kh>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: km\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Pootle 2.7\n"
"X-Accelerator-Marker: ~\n"
"X-Language: km-KH\n"
"X-POOTLE-MTIME: 1404793852.000000\n"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"tit\n"
"help.text"
msgid "To access this function..."
msgstr "ដើម្បី​ចូលដំណើរការ​អនុគមន៍​នេះ..."

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"hd_id3156023\n"
"1\n"
"help.text"
msgid "<variable id=\"wie\">To access this function...</variable>"
msgstr "<variable id=\"wie\">ដើម្បី​ចូល​ដំណើរ​ការ​អនុគមន៍​នេះ...</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3150791\n"
"9\n"
"help.text"
msgid "Choose <emph>View - Chart Data Table</emph> (Charts)"
msgstr "ជ្រើស <emph>ទិដ្ឋភាព - តារាង​ទិន្នន័យ​គំនូស​តាង</emph> (គំនូស​តាង)"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3154686\n"
"55\n"
"help.text"
msgid "On Formatting bar, click"
msgstr "លើ​របារ​ធ្វើ​ទ្រង់​ទ្រាយ ចុច"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3153728\n"
"help.text"
msgid "<image id=\"img_id3147428\" src=\"cmd/sc_grid.png\" width=\"0.1665in\" height=\"0.1665in\"><alt id=\"alt_id3147428\">Icon</alt></image>"
msgstr "<image id=\"img_id3147428\" src=\"cmd/sc_grid.png\" width=\"0.1665in\" height=\"0.1665in\"><alt id=\"alt_id3147428\">រូបតំណាង</alt></image>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3154942\n"
"11\n"
"help.text"
msgid "Chart Data"
msgstr "ទិន្នន័យ​​គំនូស​តាង"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3153160\n"
"12\n"
"help.text"
msgid "<variable id=\"efgttl\">Choose <emph>Insert - Title </emph>(Charts)</variable>"
msgstr "<variable id=\"efgttl\">ជ្រើស <emph>បញ្ចូល - ចំណង​ជើង </emph>(គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3149121\n"
"13\n"
"help.text"
msgid "Choose <emph>Insert - Legend </emph>(Charts)"
msgstr "ជ្រើស <emph>បញ្ចូល - តាង </emph>(គំនូស​តាង)"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3155444\n"
"56\n"
"help.text"
msgid "Choose <emph>Format - Legend - Position</emph> tab (Charts)"
msgstr "ជ្រើស <emph>ទ្រង់ទ្រាយ - តាង - ផ្ទាំង ទីតាំង</emph> (គំនូស​តាង)"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3156385\n"
"16\n"
"help.text"
msgid "Choose <emph>Insert - Data Labels </emph>(Charts)"
msgstr "ជ្រើស <emph>បញ្ចូល - ស្លាក​ទិន្នន័យ </emph>(គំនូស​តាង)"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3147341\n"
"68\n"
"help.text"
msgid "Choose <emph>Format - Format Selection - Data Point/Data Series - Data Labels</emph> tab (for data series and data point) (Charts)"
msgstr "ជ្រើស <emph>ទ្រង់ទ្រាយ - ធ្វើទ្រង់ទ្រាយជម្រើស - ស៊េរី​ទិន្នន័យ/ចំណុច​ទិន្នន័យ - ផ្ទាំង​ស្លាក​ទិន្នន័យ</emph> (សម្រាប់​ស៊េរី​ទិន្នន័យ និងចំណុច​ទិន្នន័យ) (គំនូស​តាង)"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3149565\n"
"69\n"
"help.text"
msgid "<variable id=\"efgaug\">Choose <emph>Insert - Axes </emph>(Charts)</variable>"
msgstr "<variable id=\"efgaug\">ជ្រើស <emph>បញ្ចូល - អ័ក្ស </emph>(គំនូស​តាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3150297\n"
"17\n"
"help.text"
msgid "Choose <emph>Insert - Grids </emph>(Charts)"
msgstr "ជ្រើស <emph>បញ្ចូល - ក្រឡាចត្រង្គ </emph>(គំនូស​តាង)"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3145789\n"
"58\n"
"help.text"
msgid "On Formatting bar, click"
msgstr "លើ​របារ​ធ្វើ​ទ្រង់​ទ្រាយ ចុច"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3150307\n"
"help.text"
msgid "<image id=\"img_id3155111\" src=\"cmd/sc_togglegridhorizontal.png\" width=\"0.2228in\" height=\"0.2228in\"><alt id=\"alt_id3155111\">Icon</alt></image>"
msgstr "<image id=\"img_id3155111\" src=\"cmd/sc_togglegridhorizontal.png\" width=\"0.2228in\" height=\"0.2228in\"><alt id=\"alt_id3155111\">រូបតំណាង</alt></image>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3155378\n"
"19\n"
"help.text"
msgid "Horizontal Grids"
msgstr ""

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3145384\n"
"help.text"
msgid "<image id=\"img_id3152989\" src=\"cmd/sc_togglegridvertical.png\" width=\"0.2228in\" height=\"0.2228in\"><alt id=\"alt_id3152989\">Icon</alt></image>"
msgstr "<image id=\"img_id3152989\" src=\"cmd/sc_togglegridvertical.png\" width=\"0.2228in\" height=\"0.2228in\"><alt id=\"alt_id3152989\">រូបតំណាង</alt></image>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3153067\n"
"20\n"
"help.text"
msgid "Vertical Grids"
msgstr ""

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3148869\n"
"21\n"
"help.text"
msgid "<variable id=\"efgsta\">Choose <emph>Insert - X Error Bars </emph> or <emph>Insert - Y Error Bars </emph>(Charts)</variable>"
msgstr "<variable id=\"efgsta\">ជ្រើស <emph>បញ្ចូល​របារ​កំហុស - X  </emph> ឬ <emph>ឬ​បញ្ចូល​របារ Y </emph>(Charts)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id1061738\n"
"help.text"
msgid "<variable id=\"trendlines\">Choose Insert - Trend Lines (Charts)</variable>"
msgstr "<variable id=\"trendlines\">ជ្រើស​បញ្ចូល - បន្ទាត់ទេរ (គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3154532\n"
"67\n"
"help.text"
msgid "<variable id=\"sonderz\">Choose <emph>Insert - Special Character </emph>(Charts)</variable>"
msgstr "<variable id=\"sonderz\">ជ្រើស <emph>បញ្ចូល - តួអក្សរ​ពិសេស </emph>(គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3153246\n"
"22\n"
"help.text"
msgid "<variable id=\"frtoes\">Choose <emph>Format - Format Selection </emph>(Charts)</variable>"
msgstr "<variable id=\"frtoes\">ជ្រើស <emph>ទ្រង់ទ្រាយ - ការ​ជ្រើស​ទ្រង់ទ្រាយ </emph>(គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3150214\n"
"23\n"
"help.text"
msgid "<variable id=\"frtodd\">Choose <emph>Format - Format Selection - Data Point</emph> dialog (Charts)</variable>"
msgstr "<variable id=\"frtodd\">ជ្រើស <emph>ទ្រង់ទ្រាយ - ការ​ជ្រើស​ទ្រង់ទ្រាយ -  ប្រអប់ ចំណុច​ទិន្នន័យ</emph> (គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3154765\n"
"24\n"
"help.text"
msgid "<variable id=\"frtodr\">Choose <emph>Format - Format Selection - Data Series</emph> dialog (Charts)</variable>"
msgstr "<variable id=\"frtodr\">ជ្រើស <emph>ទ្រង់ទ្រាយ - ការ​ជ្រើស​ទ្រង់ទ្រាយ - ប្រអប់ ស៊េរី​ទិន្នន័យ</emph> (គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3153009\n"
"66\n"
"help.text"
msgid "<variable id=\"optionen\">Choose <emph>Format - Format Selection - Data Series - Options</emph> tab (Charts)</variable>"
msgstr "<variable id=\"optionen\">ជ្រើស <emph>ទ្រង់ទ្រាយ - ការ​ជ្រើស​ទ្រង់ទ្រាយ - ស៊េរីទិន្នន័យ - ជម្រើស</emph>(គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3154707\n"
"25\n"
"help.text"
msgid "<variable id=\"frtttl\">Choose <emph>Format - Title </emph>(Charts)</variable>"
msgstr "<variable id=\"frtttl\">ជ្រើស <emph>ទ្រង់ទ្រាយ - ចំណងជើង </emph>(គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3155758\n"
"26\n"
"help.text"
msgid "<variable id=\"frtoe\">Choose <emph>Format - Format Selection - Title</emph> dialog (Charts)</variable>"
msgstr "<variable id=\"frtoe\">ជ្រើស​ <emph>ទ្រង់ទ្រាយ - ការ​ជ្រើស​ទ្រង់ទ្រាយ - ចំណងជើង</emph> (គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3153075\n"
"27\n"
"help.text"
msgid "<variable id=\"frtegt\">Choose <emph>Format - Format Selection - Title</emph> dialog (Charts)</variable>"
msgstr "<variable id=\"frtegt\">ជ្រើស <emph>ទ្រង់ទ្រាយ - កា​រជ្រើស​ទ្រង់ទ្រាយ - ប្រអប់ ចំណង​ជើង</emph> (គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3149048\n"
"28\n"
"help.text"
msgid "<variable id=\"frttya\">Choose <emph>Format - Title </emph>(Charts)</variable>"
msgstr "<variable id=\"frttya\">ជ្រើស <emph>ទ្រង់ទ្រាយ - ចំណងជើង </emph>(គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3147402\n"
"70\n"
"help.text"
msgid "<variable id=\"frttyab\">Choose <emph>Format - Axis </emph>(Charts)</variable>"
msgstr "<variable id=\"frttyab\">ជ្រើស <emph>ទ្រង់ទ្រាយ - អ័ក្ស </emph>(គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3147297\n"
"29\n"
"help.text"
msgid "<variable id=\"frtlgd\">Choose <emph>Format - Legend, or Format - Format Selection</emph> - <emph>Legend </emph>(Charts)</variable>"
msgstr "<variable id=\"frtlgd\">ជ្រើស <emph>ទ្រង់ទ្រាយ - តាងឬ​ទ្រង់ទ្រាយ - ការ​ជ្រើសទ្រង់ទ្រាយ</emph> - <emph>តាង </emph>(គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3157876\n"
"31\n"
"help.text"
msgid "<variable id=\"frtaa\">Choose <emph>Format - Axis - X Axis/Secondary X Axis/Z Axis/All Axes </emph>(Charts)</variable>"
msgstr "<variable id=\"frtaa\">ជ្រើស <emph>ទ្រង់ទ្រាយ - អ័ក្ស - អ័ក្ស X /អ័ក្ស X រង/អ័ក្ស Z /អ័ក្ស​ទាំង​អស់ </emph>(គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3146883\n"
"32\n"
"help.text"
msgid "<variable id=\"frtyas\">Choose <emph>Format - Axis - Y Axis/Secondary Y Axis </emph>(Charts)</variable>"
msgstr "<variable id=\"frtyas\">ជ្រើស <emph>ទ្រង់ទ្រាយ - អ័ក្ស - អ័ក្ស Y/អ័ក្ស Y រង </emph>(គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3149349\n"
"33\n"
"help.text"
msgid "<variable id=\"frtysk\">Choose <emph>Format - Axis - Y Axis - Scale</emph> tab (Charts)</variable>"
msgstr "<variable id=\"frtysk\">ជ្រើស <emph>ទ្រង់ទ្រាយ - អ័ក្ស - អ័ក្ស Y - ផ្ទាំង មាត្រដ្ឋាន</emph> (គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id1006200812385491\n"
"help.text"
msgid "<variable id=\"positioning\">Choose <emph>Format - Axis - X Axis - Positioning</emph> tab (Charts)</variable>"
msgstr "<variable id=\"positioning\">ជ្រើស <emph>ទ្រង់ទ្រាយ - អ័ក្ស - អ័ក្ស X - ផ្ទាំងទីតាំង</emph> (គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id31493459\n"
"33\n"
"help.text"
msgid "<variable id=\"positioningy\">Choose <emph>Format - Axis - Y Axis - Positioning</emph> tab (Charts)</variable>"
msgstr "<variable id=\"positioningy\">ជ្រើស <emph>ទ្រង់ទ្រាយ - អ័ក្ស - អ័ក្ស Y - ផ្ទាំង ទីតាំង</emph> (គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3150477\n"
"34\n"
"help.text"
msgid "<variable id=\"frtgt\">Choose <emph>Format - Grid </emph>(Charts)</variable>"
msgstr "<variable id=\"frtgt\">ជ្រើស <emph>ទ្រង់ទ្រាយ - ក្រឡាចត្រង្គ </emph>(គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3150746\n"
"35\n"
"help.text"
msgid "<variable id=\"frtgtr\">Choose <emph>Format - Grid - X, Y, Z Axis Major Grid/ X, Y, Z Minor Grid/ All Axis Grids </emph>(Charts)</variable>"
msgstr "<variable id=\"frtgtr\">ជ្រើស <emph>ទ្រង់ទ្រាយ - ក្រឡាចត្រង្គ - ក្រឡាចត្រង្គ​រង្វើល​អ័ក្ស ​X, Y, Z/ក្រឡាចត្រង្គ​ញឹក​អ័ក្ស X, Y, Z / ក្រឡាចត្រង្គអ័ក្ស​ទាំងអស់ </emph>(គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3145828\n"
"36\n"
"help.text"
msgid "<variable id=\"frtdgw\">Choose <emph>Format - Chart Wall - Chart</emph> dialog (Charts)</variable>"
msgstr "<variable id=\"frtdgw\">ជ្រើស <emph>ទ្រង់ទ្រាយ - ជញ្ជាំង​គំនូសតាងl - ប្រអប់ គំនូសតាង</emph> (គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3153039\n"
"37\n"
"help.text"
msgid "<variable id=\"frtdgb\">Choose <emph>Format - Chart Floor</emph>(Charts)</variable>"
msgstr "<variable id=\"frtdgb\">ជ្រើស <emph>ទ្រង់ទ្រាយ - បាត​គំនូសតាង</emph>(គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3150141\n"
"38\n"
"help.text"
msgid "<variable id=\"frtdgf\">Choose <emph>Format - Chart Area</emph>(Charts)</variable>"
msgstr "<variable id=\"frtdgf\">ជ្រើស <emph>ទ្រង់ទ្រាយ - ផ្ទៃគំនូសតាង</emph>(គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3155830\n"
"39\n"
"help.text"
msgid "Choose <emph>Format - Chart Type </emph>(Charts)"
msgstr "ជ្រើស<emph>ទ្រង់ទ្រាយ - គំនូស​តាង </emph>(គំនូសតាង)"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3145140\n"
"59\n"
"help.text"
msgid "On Formatting bar, click"
msgstr "លើ​របារ​ធ្វើ​ទ្រង់​ទ្រាយ ចុច"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3148582\n"
"help.text"
msgid "<image id=\"img_id3153124\" src=\"cmd/sc_diagramtype.png\" width=\"0.2228in\" height=\"0.2228in\"><alt id=\"alt_id3153124\">Icon</alt></image>"
msgstr "<image id=\"img_id3153124\" src=\"cmd/sc_diagramtype.png\" width=\"0.2228in\" height=\"0.2228in\"><alt id=\"alt_id3153124\">រូបតំណាង</alt></image>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3155956\n"
"41\n"
"help.text"
msgid "Edit Chart Type"
msgstr "កែ​សម្រួល​ប្រភេទ​​គំនូស​តាង"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3155621\n"
"45\n"
"help.text"
msgid "<variable id=\"frtdda\">Choose <emph>Format - 3D View</emph>(Charts)</variable>"
msgstr "<variable id=\"frtdda\">ជ្រើស <emph>ទ្រង់ទ្រាយ - ទិដ្ឋភាព​ត្រីមាត្រ3D View</emph>(គំនូសតាង)</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3150661\n"
"61\n"
"help.text"
msgid "Choose <emph>Format - Arrangement </emph>(Charts)"
msgstr "ជ្រើស<emph>ទ្រង់ទ្រាយ - រៀបចំ</emph>(គំនូសតាង)"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3153046\n"
"65\n"
"help.text"
msgid "Open context menu - choose <emph>Arrangement </emph>(Charts)"
msgstr "បើក​ម៉ឺនុយ​បរិបទ - ជ្រើស<emph>រៀបចំ</emph>(គំនូស​តាង)"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3151020\n"
"help.text"
msgid "<image id=\"img_id3150936\" src=\"cmd/sc_toggletitle.png\" width=\"0.2228in\" height=\"0.2228in\"><alt id=\"alt_id3150936\">Icon</alt></image>"
msgstr "<image id=\"img_id3150936\" src=\"cmd/sc_toggletitle.png\" width=\"0.2228in\" height=\"0.2228in\"><alt id=\"alt_id3150936\">រូបតំណាង</alt></image>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3150467\n"
"48\n"
"help.text"
msgid "Title On/Off"
msgstr "បិទ/​បើក​ ចំណង​ជើង"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3149775\n"
"help.text"
msgid "<image id=\"img_id3149781\" src=\"cmd/sc_toggleaxistitle.png\" width=\"0.2228in\" height=\"0.2228in\"><alt id=\"alt_id3149781\">Icon</alt></image>"
msgstr "<image id=\"img_id3149781\" src=\"cmd/sc_toggleaxistitle.png\" width=\"0.2228in\" height=\"0.2228in\"><alt id=\"alt_id3149781\">រូបតំណាង</alt></image>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3147168\n"
"49\n"
"help.text"
msgid "Axis Titles On/Off"
msgstr "បិទ​/​​បើក​​ ចំណង​ជើង​អ័ក្ស"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3163824\n"
"help.text"
msgid "<image id=\"img_id3149708\" src=\"cmd/sc_togglegridhorizontal.png\" width=\"0.2228in\" height=\"0.2228in\"><alt id=\"alt_id3149708\">Icon</alt></image>"
msgstr "<image id=\"img_id3149708\" src=\"cmd/sc_togglegridhorizontal.png\" width=\"0.2228in\" height=\"0.2228in\"><alt id=\"alt_id3149708\">រូបតំណាង</alt></image>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3150962\n"
"50\n"
"help.text"
msgid "Horizontal Grids"
msgstr ""

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3151183\n"
"help.text"
msgid "<image id=\"img_id3151189\" src=\"cmd/sc_toggleaxisdescr.png\" width=\"0.2228in\" height=\"0.2228in\"><alt id=\"alt_id3151189\">Icon</alt></image>"
msgstr "<image id=\"img_id3151189\" src=\"cmd/sc_toggleaxisdescr.png\" width=\"0.2228in\" height=\"0.2228in\"><alt id=\"alt_id3151189\">រូបតំណាង</alt></image>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3153210\n"
"51\n"
"help.text"
msgid "Show/Hide Axis Descriptions"
msgstr "បង្ហាញ​​/​​លាក់​ ពិពណ៌នា​អ័ក្ស​"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3156315\n"
"help.text"
msgid "<image id=\"img_id3156322\" src=\"cmd/sc_togglegridvertical.png\" width=\"0.2228in\" height=\"0.2228in\"><alt id=\"alt_id3156322\">Icon</alt></image>"
msgstr "<image id=\"img_id3156322\" src=\"cmd/sc_togglegridvertical.png\" width=\"0.2228in\" height=\"0.2228in\"><alt id=\"alt_id3156322\">រូបតំណាង</alt></image>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id3153153\n"
"52\n"
"help.text"
msgid "Vertical Grids"
msgstr ""

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id9631641\n"
"help.text"
msgid "Choose Insert - <switchinline select=\"appl\"><caseinline select=\"WRITER\"> Object -</caseinline></switchinline> Chart"
msgstr "ជ្រើស បញ្ចូល - <switchinline select=\"appl\"><caseinline select=\"WRITER\"> វត្ថុ -</caseinline></switchinline> គំនូសតាង"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id2985320\n"
"help.text"
msgid "Choose Insert - <switchinline select=\"appl\"><caseinline select=\"WRITER\"> Object -</caseinline></switchinline> Chart"
msgstr "ជ្រើស បញ្ចូល - <switchinline select=\"appl\"><caseinline select=\"WRITER\"> វត្ថុ -</caseinline></switchinline> គំនូសតាង"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id1096530\n"
"help.text"
msgid "Double-click a chart, then choose Format - Data Ranges"
msgstr "ចុចទ្វេដង​លើ​គំនូស​តាង បន្ទាប់​​មក​ជ្រើស ទ្រង់ទ្រាយ - ជួរ​ទិន្នន័យ"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id733359\n"
"help.text"
msgid "<variable id=\"smlp\">In the Chart Type dialog of a Line chart or XY chart that displays lines, choose Smooth in the Lines type dropdown, then click the Properties button.</variable>"
msgstr "<variable id=\"smlp\">នៅ​ក្នុង​ប្រអប់​​ប្រភេទ​គំនូសតាង​នៃ​គំនូសតាង​បន្ទាត់ ឬ​គំនូស​តាង XY ដែល​បង្ហាញ​បន្ទាត់ ជ្រើស កោង នៅ​ក្នុង​បញ្ជី​ទម្លាក់​ចុះ​ប្រភេទ​បន្ទាត់ បន្ទាប់មក​ចុច​ប៊ូតុង​លក្ខណសម្បត្តិ។</variable>"

#: 00000004.xhp
msgctxt ""
"00000004.xhp\n"
"par_id8513095\n"
"help.text"
msgid "<variable id=\"stlp\">In the Chart Type dialog of a Line chart or XY chart that displays lines, choose Stepped in the Lines type dropdown, then click the Properties button.</variable>"
msgstr "<variable id=\"stlp\">នៅ​ក្នុង​ប្រអប់​​ប្រភេទ​គំនូសតាង​នៃ​គំនូសតាង​បន្ទាត់ ឬ​គំនូស​តាង XY ដែល​បង្ហាញ​បន្ទាត់ ជ្រើស កាំ នៅ​ក្នុង​បញ្ជី​ទម្លាក់​ចុះ​ប្រភេទ​បន្ទាត់ បន្ទាប់មក​ចុច​ប៊ូតុង​លក្ខណសម្បត្តិ។</variable>"

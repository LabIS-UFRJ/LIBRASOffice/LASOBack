#. extracted from wizards/source/template
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2014-05-02 00:06+0200\n"
"PO-Revision-Date: 2014-08-01 15:09+0000\n"
"Last-Translator: Elanjelian <tamiliam@gmail.com>\n"
"Language-Team: Tamil <>\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1406905757.000000\n"

#: template.src
msgctxt ""
"template.src\n"
"SAMPLES\n"
"string.text"
msgid "In order to use the full functionality of this sample, create a document that is based on this template."
msgstr "இந்த மாதிரியை முழு செயல்பாடுகளையும் பயன்படுத்த, ஒரு ஆவணத்தை இந்த வார்ப்புருவின் அடிப்படையில் உருவாக்குக."

#: template.src
msgctxt ""
"template.src\n"
"SAMPLES + 1\n"
"string.text"
msgid "Remarks"
msgstr "குறிப்புக்கள்"

#: template.src
msgctxt ""
"template.src\n"
"STYLES\n"
"string.text"
msgid "Theme Selection"
msgstr "தோற்றக்கரு தெரிவு"

#: template.src
msgctxt ""
"template.src\n"
"STYLES + 1\n"
"string.text"
msgid "Error while saving the document to the clipboard! The following action cannot be undone."
msgstr "ஆவணத்தை கிளிபோர்டில் சேமிக்கும்போது பிழை ஏற்பட்டது.ஆகையால்,கீழ்க்கண்ட செயலை செய்யவியலாது."

#: template.src
msgctxt ""
"template.src\n"
"STYLES + 2\n"
"string.text"
msgid "~Cancel"
msgstr "ரத்து"

#: template.src
msgctxt ""
"template.src\n"
"STYLES + 3\n"
"string.text"
msgid "~OK"
msgstr "சரி"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME\n"
"string.text"
msgid "(Standard)"
msgstr "(செந்தரம்)"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 1\n"
"string.text"
msgid "Autumn Leaves"
msgstr "இலையுதிர்கால இலைகள்"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 2\n"
"string.text"
msgid "Be"
msgstr "இரு"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 3\n"
"string.text"
msgid "Black and White"
msgstr "கருப்பு வெள்ளை"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 4\n"
"string.text"
msgid "Blackberry Bush"
msgstr "ப்ளாக்பெரி புதர்"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 5\n"
"string.text"
msgid "Blue Jeans"
msgstr "நீல ஜீன்ஸ்"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 6\n"
"string.text"
msgid "Fifties Diner"
msgstr "ஃபிஃப்டிஸ் டைனர்"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 7\n"
"string.text"
msgid "Glacier"
msgstr "கிளேசியர்"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 8\n"
"string.text"
msgid "Green Grapes"
msgstr "பச்சை திராட்சை"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 9\n"
"string.text"
msgid "Marine"
msgstr "மெரைன்"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 10\n"
"string.text"
msgid "Millennium"
msgstr "மில்லெனியம்"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 11\n"
"string.text"
msgid "Nature"
msgstr "இயற்கை"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 12\n"
"string.text"
msgid "Neon"
msgstr "நியான்"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 13\n"
"string.text"
msgid "Night"
msgstr "இரவு"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 14\n"
"string.text"
msgid "PC Nostalgia"
msgstr "PC நாஸ்டால்ஜியா"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 15\n"
"string.text"
msgid "Pastel"
msgstr "பாஸ்டெல்"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 16\n"
"string.text"
msgid "Pool Party"
msgstr "பூல் பார்ட்டி"

#: template.src
msgctxt ""
"template.src\n"
"STYLENAME + 17\n"
"string.text"
msgid "Pumpkin"
msgstr "பூசனி"

#: template.src
msgctxt ""
"template.src\n"
"AgendaDlgName\n"
"string.text"
msgid "Minutes Template"
msgstr "தீர்மான படிமஅச்சு"

#: template.src
msgctxt ""
"template.src\n"
"AgendaDlgNoCancel\n"
"string.text"
msgid "An option must be confirmed."
msgstr "விருப்பம் உறுதிசெய்யப்பட வேண்டும்."

#: template.src
msgctxt ""
"template.src\n"
"AgendaDlgFrame\n"
"string.text"
msgid "Minutes Type"
msgstr "தீர்மான வகை"

#: template.src
msgctxt ""
"template.src\n"
"AgendaDlgButton1\n"
"string.text"
msgid "Results Minutes"
msgstr "தீர்மான முடிவுகள்"

#: template.src
msgctxt ""
"template.src\n"
"AgendaDlgButton2\n"
"string.text"
msgid "Evaluation Minutes"
msgstr "மதிப்பிடும் நிமிடங்கள்"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceNoTextmark\n"
"string.text"
msgid "The bookmark 'Recipient' is missing."
msgstr "புத்தகக் குறி 'Recipient' காணப்படவில்லை."

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceNoTextmark+1\n"
"string.text"
msgid "Form letter fields can not be included."
msgstr "படிவ கடிதப் புலங்கள் சேர்க்கப்படவில்லை."

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceMsgError\n"
"string.text"
msgid "An error has occurred."
msgstr "ஒரு வழு ஏற்பட்டது."

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceDialog\n"
"string.text"
msgid "Addressee"
msgstr "முகவரியாளர்"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceDialog+1\n"
"string.text"
msgid "One recipient"
msgstr "ஒரு பெறுநர்"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceDialog+2\n"
"string.text"
msgid "Several recipients (address database)"
msgstr "பல பெறுநர்கள் (முகவரி தரவுதளம்)"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceDialog+3\n"
"string.text"
msgid "Use of This Template"
msgstr "இந்த வார்ப்புருவைப் பயன்படுத்து"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields\n"
"string.text"
msgid "Click placeholder and overwrite"
msgstr "இடத்தின் மேல் க்ளிக் செய்து அதன் திருப்பி எழுது"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+1\n"
"string.text"
msgid "Company"
msgstr "நிறுவனம்"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+2\n"
"string.text"
msgid "Department"
msgstr "துறை"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+3\n"
"string.text"
msgid "First Name"
msgstr "முதற் பெயர்"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+4\n"
"string.text"
msgid "Last Name"
msgstr "இறுதிப் பெயர்"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+5\n"
"string.text"
msgid "Street"
msgstr "வீதி"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+6\n"
"string.text"
msgid "Country"
msgstr "நாடு"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+7\n"
"string.text"
msgid "ZIP/Postal Code"
msgstr "ZIP/அஞ்சலெண்"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+8\n"
"string.text"
msgid "City"
msgstr "நகரம்"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+9\n"
"string.text"
msgid "Title"
msgstr "தலைப்பு"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+10\n"
"string.text"
msgid "Position"
msgstr "நிலை"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+11\n"
"string.text"
msgid "Form of Address"
msgstr "முகவரி வடிவம்"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+12\n"
"string.text"
msgid "Initials"
msgstr "முதலெழுத்துக்கள்"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+13\n"
"string.text"
msgid "Salutation"
msgstr "வரவேற்பு"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+14\n"
"string.text"
msgid "Home Phone"
msgstr "மனை தொலைபேசி"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+15\n"
"string.text"
msgid "Work Phone"
msgstr "பணி தொலைபேசி"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+16\n"
"string.text"
msgid "Fax"
msgstr "தொலைநகல்"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+17\n"
"string.text"
msgid "E-Mail"
msgstr "மின்னஞ்சல்"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+18\n"
"string.text"
msgid "URL"
msgstr "URL"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+19\n"
"string.text"
msgid "Notes"
msgstr "குறிப்பு"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+20\n"
"string.text"
msgid "Alt. Field 1"
msgstr "மாற். புலம் 1"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+21\n"
"string.text"
msgid "Alt. Field 2"
msgstr "மாற். புலம் 2"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+22\n"
"string.text"
msgid "Alt. Field 3"
msgstr "மாற். புலம் 3"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+23\n"
"string.text"
msgid "Alt. Field 4"
msgstr "மாற். புலம் 4"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+24\n"
"string.text"
msgid "ID"
msgstr "ID"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+25\n"
"string.text"
msgid "State"
msgstr "மாநிலம்"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+26\n"
"string.text"
msgid "Office Phone"
msgstr "அலுவலக தொலைபேசி"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+27\n"
"string.text"
msgid "Pager"
msgstr "தொலைஅழைப்பான்"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+28\n"
"string.text"
msgid "Mobile Phone"
msgstr "கையடக்கத் தொலைபேசி"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+29\n"
"string.text"
msgid "Other Phone"
msgstr "மற்ற தொலைபேசி"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+30\n"
"string.text"
msgid "Calendar URL"
msgstr "நாட்காட்டி URL"

#: template.src
msgctxt ""
"template.src\n"
"CorrespondenceFields+31\n"
"string.text"
msgid "Invite"
msgstr "அழைக்க"

#: template.src
msgctxt ""
"template.src\n"
"TextField\n"
"string.text"
msgid "User data field is not defined!"
msgstr "தரவு புலம் வரையறுக்கப்படவில்லை!"

#: template.src
msgctxt ""
"template.src\n"
"Newsletter\n"
"string.text"
msgid "General layout"
msgstr "பொது அடுக்கமைப்பு"

#: template.src
msgctxt ""
"template.src\n"
"Newsletter + 1\n"
"string.text"
msgid "Default layout"
msgstr "முன்னிருப்பு அடுக்கமைப்பு"

#: template.src
msgctxt ""
"template.src\n"
"Newsletter + 2\n"
"string.text"
msgid "Commemorative publication layout"
msgstr "பிரசுரிப்பு அடுக்கமைப்பு"

#: template.src
msgctxt ""
"template.src\n"
"Newsletter + 3\n"
"string.text"
msgid "Brochure layout"
msgstr "சிறு நூல் உருவரை"

#: template.src
msgctxt ""
"template.src\n"
"Newsletter + 10\n"
"string.text"
msgid "Format"
msgstr "வடிவம்"

#: template.src
msgctxt ""
"template.src\n"
"Newsletter + 11\n"
"string.text"
msgid "Single-sided"
msgstr "ஒரு பக்க"

#: template.src
msgctxt ""
"template.src\n"
"Newsletter + 12\n"
"string.text"
msgid "Double-sided"
msgstr "இரு பக்க"

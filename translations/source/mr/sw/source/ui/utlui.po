#. extracted from sw/source/ui/utlui
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-05-25 15:01+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: Marathi <sshedmak@redhat.com>\n"
"Language: mr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1464188466.000000\n"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_FOOTNOTE\n"
"string.text"
msgid "Footnote Characters"
msgstr "तळटीप अक्षरे"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_PAGENO\n"
"string.text"
msgid "Page Number"
msgstr "पृष्ठ क्रमांक"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_LABEL\n"
"string.text"
msgid "Caption Characters"
msgstr "कॅप्शन अक्षरें"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_DROPCAPS\n"
"string.text"
msgid "Drop Caps"
msgstr "मोठ्या अक्षरांना ड्रॉप करा"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_NUM_LEVEL\n"
"string.text"
msgid "Numbering Symbols"
msgstr "क्रमांकन चिन्हे"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_BUL_LEVEL\n"
"string.text"
msgid "Bullets"
msgstr "बुल्लेट्स"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_INET_NORMAL\n"
"string.text"
msgid "Internet Link"
msgstr "इंटरनेट लिंक"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_INET_VISIT\n"
"string.text"
msgid "Visited Internet Link"
msgstr "भेट दिलेली इंटरनेट लिंक"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_JUMPEDIT\n"
"string.text"
msgid "Placeholder"
msgstr "स्थानधारक"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_TOXJUMP\n"
"string.text"
msgid "Index Link"
msgstr "अनुक्रमणिका लिंक"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_ENDNOTE\n"
"string.text"
msgid "Endnote Characters"
msgstr "अंत्यटीप अक्षरे"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_LANDSCAPE\n"
"string.text"
msgid "Landscape"
msgstr "आडवे"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_LINENUM\n"
"string.text"
msgid "Line Numbering"
msgstr "ओळ क्रमांकन"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_IDX_MAIN_ENTRY\n"
"string.text"
msgid "Main Index Entry"
msgstr "मुख्य इंडेक्स नोंदणी"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_FOOTNOTE_ANCHOR\n"
"string.text"
msgid "Footnote Anchor"
msgstr "फूटनोट अँकर"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_ENDNOTE_ANCHOR\n"
"string.text"
msgid "Endnote Anchor"
msgstr "एंडनोट अँकर"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_EMPHASIS\n"
"string.text"
msgid "Emphasis"
msgstr "जोर देणे"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_CITIATION\n"
"string.text"
msgid "Quotation"
msgstr "म्हण"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_STRONG\n"
"string.text"
msgid "Strong Emphasis"
msgstr "जास्त जोर देणे"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_CODE\n"
"string.text"
msgid "Source Text"
msgstr "स्त्रोत मजकूर"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_SAMPLE\n"
"string.text"
msgid "Example"
msgstr "उदाहरण"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_KEYBOARD\n"
"string.text"
msgid "User Entry"
msgstr "वापरकर्ता नोंद"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_VARIABLE\n"
"string.text"
msgid "Variable"
msgstr "वेरीयेबल"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_DEFINSTANCE\n"
"string.text"
msgid "Definition"
msgstr "परिभाषा"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_TELETYPE\n"
"string.text"
msgid "Teletype"
msgstr "टेलिटाइप"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLFRM_FRAME\n"
"string.text"
msgid "Frame"
msgstr "चौकट"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLFRM_GRAPHIC\n"
"string.text"
msgid "Graphics"
msgstr "ग्राफिक्स्"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLFRM_OLE\n"
"string.text"
msgid "OLE"
msgstr "OLE"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLFRM_FORMEL\n"
"string.text"
msgid "Formula"
msgstr "सूत्र"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLFRM_MARGINAL\n"
"string.text"
msgid "Marginalia"
msgstr "मार्जिनालिया"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLFRM_WATERSIGN\n"
"string.text"
msgid "Watermark"
msgstr "जलचिह्न"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLFRM_LABEL\n"
"string.text"
msgid "Labels"
msgstr "लेबल्स"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_STANDARD\n"
"string.text"
msgid "Default Style"
msgstr "पूर्वनिर्धारित शैली"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TEXT\n"
"string.text"
msgid "Text Body"
msgstr "मजकूर बॉडि"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TEXT_IDENT\n"
"string.text"
msgid "First Line Indent"
msgstr "पहिली ओळ संरेषन"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TEXT_NEGIDENT\n"
"string.text"
msgid "Hanging Indent"
msgstr "हँगिंग इंडेंट"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TEXT_MOVE\n"
"string.text"
msgid "Text Body Indent"
msgstr "मजकूर बॉडि इंडेंट"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_GREETING\n"
"string.text"
msgid "Complimentary Close"
msgstr "काँप्लिमेंट्रि क्लोज"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_SIGNATURE\n"
"string.text"
msgid "Signature"
msgstr "स्वाक्षरी"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE_BASE\n"
"string.text"
msgid "Heading"
msgstr "मथळा"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUMBUL_BASE\n"
"string.text"
msgid "List"
msgstr "सूची"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_REGISTER_BASE\n"
"string.text"
msgid "Index"
msgstr "अनुक्रमणिका"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_CONFRONTATION\n"
"string.text"
msgid "List Indent"
msgstr "सूची इन्डेन्ट"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_MARGINAL\n"
"string.text"
msgid "Marginalia"
msgstr "मार्जिनालिया"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE1\n"
"string.text"
msgid "Heading 1"
msgstr "शीर्षक 1"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE2\n"
"string.text"
msgid "Heading 2"
msgstr "शीर्षक 2"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE3\n"
"string.text"
msgid "Heading 3"
msgstr "शीर्षक 3"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE4\n"
"string.text"
msgid "Heading 4"
msgstr "शीर्षक 4"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE5\n"
"string.text"
msgid "Heading 5"
msgstr "शीर्षक 5"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE6\n"
"string.text"
msgid "Heading 6"
msgstr "शीर्षक 6"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE7\n"
"string.text"
msgid "Heading 7"
msgstr "शीर्षक 7"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE8\n"
"string.text"
msgid "Heading 8"
msgstr "शीर्षक 8"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE9\n"
"string.text"
msgid "Heading 9"
msgstr "शीर्षक 9"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE10\n"
"string.text"
msgid "Heading 10"
msgstr "शीर्षक 10"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL1S\n"
"string.text"
msgid "Numbering 1 Start"
msgstr "क्रमांकन 1 सुरू"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL1\n"
"string.text"
msgid "Numbering 1"
msgstr "क्रमांकन 1"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL1E\n"
"string.text"
msgid "Numbering 1 End"
msgstr "क्रमांकन 1 समाप्त"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_NONUM1\n"
"string.text"
msgid "Numbering 1 Cont."
msgstr "क्रमांकन 1 पुढे"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL2S\n"
"string.text"
msgid "Numbering 2 Start"
msgstr "क्रमांकन 2 सुरू"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL2\n"
"string.text"
msgid "Numbering 2"
msgstr "क्रमांकन 2"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL2E\n"
"string.text"
msgid "Numbering 2 End"
msgstr "क्रमांकन 2 समाप्त"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_NONUM2\n"
"string.text"
msgid "Numbering 2 Cont."
msgstr "क्रमांकन 2 पुढे"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL3S\n"
"string.text"
msgid "Numbering 3 Start"
msgstr "क्रमांकन 3 सुरू"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL3\n"
"string.text"
msgid "Numbering 3"
msgstr "क्रमांकन 3"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL3E\n"
"string.text"
msgid "Numbering 3 End"
msgstr "क्रमांकन 3 समाप्त"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_NONUM3\n"
"string.text"
msgid "Numbering 3 Cont."
msgstr "क्रमांकन 3 पुढे"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL4S\n"
"string.text"
msgid "Numbering 4 Start"
msgstr "क्रमांकन 4 सुरू"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL4\n"
"string.text"
msgid "Numbering 4"
msgstr "क्रमांकन 4"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL4E\n"
"string.text"
msgid "Numbering 4 End"
msgstr "क्रमांकन 4 समाप्त"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_NONUM4\n"
"string.text"
msgid "Numbering 4 Cont."
msgstr "क्रमांकन 4 पुढे"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL5S\n"
"string.text"
msgid "Numbering 5 Start"
msgstr "क्रमांकन 5 सुरू"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL5\n"
"string.text"
msgid "Numbering 5"
msgstr "क्रमांकन 5"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL5E\n"
"string.text"
msgid "Numbering 5 End"
msgstr "क्रमांकन 5 समाप्त"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_NONUM5\n"
"string.text"
msgid "Numbering 5 Cont."
msgstr "क्रमांकन 5 पुढे"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL1S\n"
"string.text"
msgid "List 1 Start"
msgstr "सूची 1 सुरु"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL1\n"
"string.text"
msgid "List 1"
msgstr "सूची 1"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL1E\n"
"string.text"
msgid "List 1 End"
msgstr "सूची 1 शेवट"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_NONUM1\n"
"string.text"
msgid "List 1 Cont."
msgstr "सूची 1 चालू ठेवा"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL2S\n"
"string.text"
msgid "List 2 Start"
msgstr "सूची 2 सुरु"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL2\n"
"string.text"
msgid "List 2"
msgstr "सूची 2"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL2E\n"
"string.text"
msgid "List 2 End"
msgstr "सूची 2 शेवट"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_NONUM2\n"
"string.text"
msgid "List 2 Cont."
msgstr "सूची 2 चालू ठेवा"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL3S\n"
"string.text"
msgid "List 3 Start"
msgstr "सूची 3 सुरु"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL3\n"
"string.text"
msgid "List 3"
msgstr "सूची 3"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL3E\n"
"string.text"
msgid "List 3 End"
msgstr "सूची 3 शेवट"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_NONUM3\n"
"string.text"
msgid "List 3 Cont."
msgstr "सूची 3 चालू ठेवा"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL4S\n"
"string.text"
msgid "List 4 Start"
msgstr "सूची 4 सुरु"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL4\n"
"string.text"
msgid "List 4"
msgstr "सूची 4"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL4E\n"
"string.text"
msgid "List 4 End"
msgstr "सूची 4 शेवट"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_NONUM4\n"
"string.text"
msgid "List 4 Cont."
msgstr "सूची 4 चालू ठेवा"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL5S\n"
"string.text"
msgid "List 5 Start"
msgstr "सूची 5 सुरु"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL5\n"
"string.text"
msgid "List 5"
msgstr "सूची 5"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL5E\n"
"string.text"
msgid "List 5 End"
msgstr "सूची 5 शेवट"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_NONUM5\n"
"string.text"
msgid "List 5 Cont."
msgstr "सूची 5 चालू ठेवा"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADER\n"
"string.text"
msgid "Header"
msgstr "शीर्षलेख"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADERL\n"
"string.text"
msgid "Header Left"
msgstr "शीर्षक डावे"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADERR\n"
"string.text"
msgid "Header Right"
msgstr "शीर्षक उजवे"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_FOOTER\n"
"string.text"
msgid "Footer"
msgstr "चरणलेख"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_FOOTERL\n"
"string.text"
msgid "Footer Left"
msgstr "चरणओळ डावे"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_FOOTERR\n"
"string.text"
msgid "Footer Right"
msgstr "चरणओळ उजवे"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TABLE\n"
"string.text"
msgid "Table Contents"
msgstr "तक्ताचा विषय"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TABLE_HDLN\n"
"string.text"
msgid "Table Heading"
msgstr "तालिकेचे शीर्षक"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_FRAME\n"
"string.text"
msgid "Frame Contents"
msgstr "फ्रेम अंतर्भुत माहिती"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_FOOTNOTE\n"
"string.text"
msgid "Footnote"
msgstr "तळटीप"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_ENDNOTE\n"
"string.text"
msgid "Endnote"
msgstr "अंत्यटीप"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_LABEL\n"
"string.text"
msgid "Caption"
msgstr "कॅप्शन"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_LABEL_ABB\n"
"string.text"
msgid "Illustration"
msgstr "दृष्टान्त"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_LABEL_TABLE\n"
"string.text"
msgid "Table"
msgstr "तक्ता"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_LABEL_FRAME\n"
"string.text"
msgid "Text"
msgstr "मजकूर"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_LABEL_DRAWING\n"
"string.text"
msgid "Drawing"
msgstr "चित्र"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_JAKETADRESS\n"
"string.text"
msgid "Addressee"
msgstr "स्वीकारकर्ता"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_SENDADRESS\n"
"string.text"
msgid "Sender"
msgstr "पाठवणारा"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_IDXH\n"
"string.text"
msgid "Index Heading"
msgstr "अनुक्रमणिका शीर्षक"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_IDX1\n"
"string.text"
msgid "Index 1"
msgstr "अनुक्रमणिका 1"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_IDX2\n"
"string.text"
msgid "Index 2"
msgstr "अनुक्रमणिका 2"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_IDX3\n"
"string.text"
msgid "Index 3"
msgstr "अनुक्रमणिका 3"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_IDXBREAK\n"
"string.text"
msgid "Index Separator"
msgstr "अनुक्रमणिका दुभाजक"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNTH\n"
"string.text"
msgid "Contents Heading"
msgstr "अंतर्भूत माहितीचे शीर्षक"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT1\n"
"string.text"
msgid "Contents 1"
msgstr "अंतर्भूत माहिती 1"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT2\n"
"string.text"
msgid "Contents 2"
msgstr "अंतर्भूत माहिती 2"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT3\n"
"string.text"
msgid "Contents 3"
msgstr "अंतर्भूत माहिती 3"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT4\n"
"string.text"
msgid "Contents 4"
msgstr "अंतर्भूत माहिती 4"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT5\n"
"string.text"
msgid "Contents 5"
msgstr "अंतर्भूत माहिती 5"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT6\n"
"string.text"
msgid "Contents 6"
msgstr "अंतर्भूत माहिती 6"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT7\n"
"string.text"
msgid "Contents 7"
msgstr "अंतर्भूत माहिती 7"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT8\n"
"string.text"
msgid "Contents 8"
msgstr "अंतर्भूत माहिती 8"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT9\n"
"string.text"
msgid "Contents 9"
msgstr "अंतर्भूत माहिती 9"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT10\n"
"string.text"
msgid "Contents 10"
msgstr "अंतर्भूत माहिती 10"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USERH\n"
"string.text"
msgid "User Index Heading"
msgstr "वापरकर्ता अनुक्रमणिका शीर्षक"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER1\n"
"string.text"
msgid "User Index 1"
msgstr "वापरकर्ता अनुक्रमणिका 1"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER2\n"
"string.text"
msgid "User Index 2"
msgstr "वापरकर्ता अनुक्रमणिका 2"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER3\n"
"string.text"
msgid "User Index 3"
msgstr "वापरकर्ता अनुक्रमणिका 3"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER4\n"
"string.text"
msgid "User Index 4"
msgstr "वापरकर्ता अनुक्रमणिका 4"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER5\n"
"string.text"
msgid "User Index 5"
msgstr "वापरकर्ता अनुक्रमणिका 5"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER6\n"
"string.text"
msgid "User Index 6"
msgstr "वापरकर्ता अनुक्रमणिका 6"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER7\n"
"string.text"
msgid "User Index 7"
msgstr "वापरकर्ता अनुक्रमणिका 7"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER8\n"
"string.text"
msgid "User Index 8"
msgstr "वापरकर्ता अनुक्रमणिका 8"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER9\n"
"string.text"
msgid "User Index 9"
msgstr "वापरकर्ता अनुक्रमणिका 9"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER10\n"
"string.text"
msgid "User Index 10"
msgstr "वापरकर्ता अनुक्रमणिका 10"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CITATION\n"
"string.text"
msgid "Citation"
msgstr "साइटेशन"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_ILLUSH\n"
"string.text"
msgid "Illustration Index Heading"
msgstr "दृष्टान्त अनुक्रमणिका शीर्षक"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_ILLUS1\n"
"string.text"
msgid "Illustration Index 1"
msgstr "दृष्टान्त अनुक्रमणिका 1"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_OBJECTH\n"
"string.text"
msgid "Object Index Heading"
msgstr "ऑब्जेक्ट इंडेक्स हेडिंग"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_OBJECT1\n"
"string.text"
msgid "Object Index 1"
msgstr "ऑब्जेक्ट इंडेक्स 1"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_TABLESH\n"
"string.text"
msgid "Table Index Heading"
msgstr "तक्ता इंडेक्स हेडिंग"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_TABLES1\n"
"string.text"
msgid "Table Index 1"
msgstr "तक्ता इंडेक्स 1"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_AUTHORITIESH\n"
"string.text"
msgid "Bibliography Heading"
msgstr "ग्रंथ-सूची शीर्षक"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_AUTHORITIES1\n"
"string.text"
msgid "Bibliography 1"
msgstr "ग्रंथ-सूची 1"

#. Document title style, not to be confused with Heading style
#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_DOC_TITEL\n"
"string.text"
msgid "Title"
msgstr "शीर्षक"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_DOC_SUBTITEL\n"
"string.text"
msgid "Subtitle"
msgstr "उपशीर्षक"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HTML_BLOCKQUOTE\n"
"string.text"
msgid "Quotations"
msgstr "म्हण"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HTML_PRE\n"
"string.text"
msgid "Preformatted Text"
msgstr "पूर्वरूपण मजकूर"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HTML_HR\n"
"string.text"
msgid "Horizontal Line"
msgstr "आडवी रेष"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HTML_DD\n"
"string.text"
msgid "List Contents"
msgstr "सूचीतील अंतर्भूत माहिती"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HTML_DT\n"
"string.text"
msgid "List Heading"
msgstr "सूची शीर्षक"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_STANDARD\n"
"string.text"
msgid "Default Style"
msgstr "पूर्वनिर्धारित शैली"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_FIRST\n"
"string.text"
msgid "First Page"
msgstr "पहिले पृष्ठ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_LEFT\n"
"string.text"
msgid "Left Page"
msgstr "डावे पृष्ठ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_RIGHT\n"
"string.text"
msgid "Right Page"
msgstr "उजवे पृष्ठ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_JAKET\n"
"string.text"
msgid "Envelope"
msgstr "पाकीट"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_REGISTER\n"
"string.text"
msgid "Index"
msgstr "अनुक्रमणिका"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_HTML\n"
"string.text"
msgid "HTML"
msgstr "HTML"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_FOOTNOTE\n"
"string.text"
msgid "Footnote"
msgstr "तळटीप"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_ENDNOTE\n"
"string.text"
msgid "Endnote"
msgstr "अंत्यटीप"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_NUM1\n"
"string.text"
msgid "Numbering 1"
msgstr "क्रमांकन 1"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_NUM2\n"
"string.text"
msgid "Numbering 2"
msgstr "क्रमांकन 2"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_NUM3\n"
"string.text"
msgid "Numbering 3"
msgstr "क्रमांकन 3"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_NUM4\n"
"string.text"
msgid "Numbering 4"
msgstr "क्रमांकन 4"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_NUM5\n"
"string.text"
msgid "Numbering 5"
msgstr "क्रमांकन 5"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_BUL1\n"
"string.text"
msgid "List 1"
msgstr "सूची 1"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_BUL2\n"
"string.text"
msgid "List 2"
msgstr "सूची 2"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_BUL3\n"
"string.text"
msgid "List 3"
msgstr "सूची 3"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_BUL4\n"
"string.text"
msgid "List 4"
msgstr "सूची 4"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_BUL5\n"
"string.text"
msgid "List 5"
msgstr "सूची 5"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_RUBYTEXT\n"
"string.text"
msgid "Rubies"
msgstr "रूबीज"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_COLUMN_VALUESET_ITEM0\n"
"string.text"
msgid "1 column"
msgstr "1 स्तंभ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_COLUMN_VALUESET_ITEM1\n"
"string.text"
msgid "2 columns with equal size"
msgstr "2 स्तंभे समांतर आकारसह"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_COLUMN_VALUESET_ITEM2\n"
"string.text"
msgid "3 columns with equal size"
msgstr "3 स्तंभे समांतर आकारसह"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_COLUMN_VALUESET_ITEM3\n"
"string.text"
msgid "2 columns with different size (left > right)"
msgstr "वेगळ्या आकारसह (डावे > उजवे) 2 स्तंभे"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_COLUMN_VALUESET_ITEM4\n"
"string.text"
msgid "2 columns with different size (left < right)"
msgstr "वेगळ्या आकारसह (डावे < उजवे) 2 स्तंभे"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_VERT_NUM\n"
"string.text"
msgid "Vertical Numbering Symbols"
msgstr "लंबरूपी क्रमांकन चिह्ने"

#: poolfmt.src
#, fuzzy
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_DEFAULT\n"
"string.text"
msgid "Default Style"
msgstr "पूर्वनिर्धारित शैली"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_3D\n"
"string.text"
msgid "3D"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_BLACK1\n"
"string.text"
msgid "Black 1"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_BLACK2\n"
"string.text"
msgid "Black 2"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_BLUE\n"
"string.text"
msgid "Blue"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_BROWN\n"
"string.text"
msgid "Brown"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_CURRENCY\n"
"string.text"
msgid "Currency"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_CURRENCY_3D\n"
"string.text"
msgid "Currency 3D"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_CURRENCY_GRAY\n"
"string.text"
msgid "Currency Gray"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_CURRENCY_LAVENDER\n"
"string.text"
msgid "Currency Lavender"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_CURRENCY_TURQUOISE\n"
"string.text"
msgid "Currency Turquoise"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_GRAY\n"
"string.text"
msgid "Gray"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_GREEN\n"
"string.text"
msgid "Green"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_LAVENDER\n"
"string.text"
msgid "Lavender"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_RED\n"
"string.text"
msgid "Red"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_TURQUOISE\n"
"string.text"
msgid "Turquoise"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_YELLOW\n"
"string.text"
msgid "Yellow"
msgstr ""

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_DEL_EMPTY_PARA+1\n"
"string.text"
msgid "Remove empty paragraphs"
msgstr "रिकामे परिच्छेद काढून टाका"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_USE_REPLACE+1\n"
"string.text"
msgid "Use replacement table"
msgstr "प्रतिस्थापना तालिकेचा उपयोग करा"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_CPTL_STT_WORD+1\n"
"string.text"
msgid "Correct TWo INitial CApitals"
msgstr "सुरुवातीची दोन मोठी अक्षरे दुरुस्त करा"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_CPTL_STT_SENT+1\n"
"string.text"
msgid "Capitalize first letter of sentences"
msgstr "वाक्यातील पहिले अक्षर मोठे करा"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_TYPO+1\n"
"string.text"
msgid "Replace \"standard\" quotes with %1 \\bcustom%2 quotes"
msgstr "\"standard\" अवतरण चिह्नाला %1 \\bcustom%2 अवतरण चिन्हाशी बदलवा"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_USER_STYLE+1\n"
"string.text"
msgid "Replace Custom Styles"
msgstr "पूर्वनियत शैली बदला"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_BULLET+1\n"
"string.text"
msgid "Bullets replaced"
msgstr "बदललेल्या बुल्लेट्स"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_UNDER+1\n"
"string.text"
msgid "Automatic _underline_"
msgstr "स्वयं _रेखांकन_"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_BOLD+1\n"
"string.text"
msgid "Automatic *bold*"
msgstr "स्वयं *ठळक*"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_FRACTION+1\n"
"string.text"
msgid "Replace 1/2 ... with ½ ..."
msgstr "1/2ला ½ शी बदला..."

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_DETECT_URL+1\n"
"string.text"
msgid "URL recognition"
msgstr "URL ओळखणे"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_DASH+1\n"
"string.text"
msgid "Replace dashes"
msgstr "डॅश बदला"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_ORDINAL+1\n"
"string.text"
msgid "Replace 1st... with 1^st..."
msgstr "1st... यांस 1^st सह बदला..."

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_RIGHT_MARGIN+1\n"
"string.text"
msgid "Combine single line paragraphs"
msgstr "एका ओळीचे परिच्छेद जोडा"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_SET_TMPL_TEXT +1\n"
"string.text"
msgid "Set \"Text body\" Style"
msgstr "\"मजकूर मुख्य भाग\" शैली प्रस्थापित करा"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_SET_TMPL_INDENT  +1\n"
"string.text"
msgid "Set \"Text body indent\" Style"
msgstr "\"मजकूर मुख्य भाग संरेषीत करा\" शैली सेट करा"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_SET_TMPL_NEG_INDENT  +1\n"
"string.text"
msgid "Set \"Hanging indent\" Style"
msgstr "\"लटकते इन्डेन्ट\"शैली प्रस्थापित करा"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_SET_TMPL_TEXT_INDENT +1\n"
"string.text"
msgid "Set \"Text body indent\" Style"
msgstr "\"मजकूर मुख्य भाग संरेषीत करा\" शैली सेट करा"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_SET_TMPL_HEADLINE +1\n"
"string.text"
msgid "Set \"Heading $(ARG1)\" Style"
msgstr "\"शीर्षक $(ARG1)\" शैली प्रस्थापित करा"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_SET_NUMBULET +1\n"
"string.text"
msgid "Set \"Bullet\" or \"Numbering\" Style"
msgstr "\"बुल्लेट\" किंवा \"क्रमांकन\" शैली प्रस्थापित करा"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_DEL_MORELINES +1\n"
"string.text"
msgid "Combine paragraphs"
msgstr "परिच्छेद जोडा"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_NON_BREAK_SPACE +1\n"
"string.text"
msgid "Add non breaking space"
msgstr "नॉन ब्रेकिंग स्पेस् समाविष्ट करा"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_OBJECT_SELECT\n"
"string.text"
msgid "Click object"
msgstr "वस्तु क्लिक करा"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_START_INS_GLOSSARY\n"
"string.text"
msgid "Before inserting AutoText"
msgstr "स्वयंमजकूर अंतर्भुत करण्या आधी"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_END_INS_GLOSSARY\n"
"string.text"
msgid "After inserting AutoText"
msgstr "स्वयंमजकूर अंतर्भुत केल्यानंतर"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_MOUSEOVER_OBJECT\n"
"string.text"
msgid "Mouse over object"
msgstr "वस्तु वरती माऊस"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_MOUSECLICK_OBJECT\n"
"string.text"
msgid "Trigger hyperlink"
msgstr "ट्रिग्गर हाइपरलिंक"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_MOUSEOUT_OBJECT\n"
"string.text"
msgid "Mouse leaves object"
msgstr "वस्तुला माऊस सोडतो"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_IMAGE_LOAD\n"
"string.text"
msgid "Image loaded successfully"
msgstr "प्रतिमा यशस्वीरित्या लोड केले"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_IMAGE_ABORT\n"
"string.text"
msgid "Image loading terminated"
msgstr "प्रतिमा लोड करणे बंद झाले"

#: utlui.src
#, fuzzy
msgctxt ""
"utlui.src\n"
"STR_EVENT_IMAGE_ERROR\n"
"string.text"
msgid "Could not load image"
msgstr "प्रतिमा लोड करणे अशक्य"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_FRM_KEYINPUT_A\n"
"string.text"
msgid "Input of alphanumeric characters"
msgstr "संख्यायीक अक्षरांचे आदान"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_FRM_KEYINPUT_NOA\n"
"string.text"
msgid "Input of non-alphanumeric characters"
msgstr "संख्यायीक नसलेल्या अक्षरांचे आदान"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_FRM_RESIZE\n"
"string.text"
msgid "Resize frame"
msgstr "चौकटीचे आकारमान बदला"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_FRM_MOVE\n"
"string.text"
msgid "Move frame"
msgstr "चौकट हलवा"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_OUTLINE\n"
"string.text"
msgid "Headings"
msgstr "शीर्षक"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_TABLE\n"
"string.text"
msgid "Tables"
msgstr "तक्ता"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_FRAME\n"
"string.text"
msgid "Text frames"
msgstr "मजकूर चौकट्या"

#: utlui.src
#, fuzzy
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_GRAPHIC\n"
"string.text"
msgid "Images"
msgstr "प्रतिमा"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_OLE\n"
"string.text"
msgid "OLE objects"
msgstr "OLE वस्तु"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_BOOKMARK\n"
"string.text"
msgid "Bookmarks"
msgstr "पुस्तकखूणा"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_REGION\n"
"string.text"
msgid "Sections"
msgstr "विभाग"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_URLFIELD\n"
"string.text"
msgid "Hyperlinks"
msgstr "हाइपरलिंक्स"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_REFERENCE\n"
"string.text"
msgid "References"
msgstr "संदर्भ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_INDEX\n"
"string.text"
msgid "Indexes"
msgstr "अनुक्रमणिका"

#: utlui.src
#, fuzzy
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_DRAWOBJECT\n"
"string.text"
msgid "Drawing objects"
msgstr "वस्तु रेखाटा"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_POSTIT\n"
"string.text"
msgid "Comments"
msgstr "टिप्पणी"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_HEADING1\n"
"string.text"
msgid "Heading 1"
msgstr "शीर्षक 1"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_ENTRY1\n"
"string.text"
msgid "This is the content from the first chapter. This is a user directory entry."
msgstr "हे पहिल्या धड्यातील अंतर्भुत माहिती आहे. हि वापरकर्ता डिरेक्ट्रि नोंदणी आहे."

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_HEADING11\n"
"string.text"
msgid "Heading 1.1"
msgstr "शीर्षक 1.1"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_ENTRY11\n"
"string.text"
msgid "This is the content from chapter 1.1. This is the entry for the table of contents."
msgstr "हे 1.1 धड्यातील अंतर्भुत माहिती आहे. हे टेबल ऑफ कंटेंट्सकरीता नोंदणी आहे."

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_HEADING12\n"
"string.text"
msgid "Heading 1.2"
msgstr "शीर्षक 1.2"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_ENTRY12\n"
"string.text"
msgid "This is the content from chapter 1.2. This keyword is a main entry."
msgstr "हे 1.2 धड्यापासूनची अंतर्भुत माहिती आहे. हे मुख्यशब्द मुख्य नोंदणी आहे."

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_TABLE1\n"
"string.text"
msgid "Table 1: This is table 1"
msgstr "तक्ता 1: हे तक्ता 1 आहे"

#: utlui.src
#, fuzzy
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_IMAGE1\n"
"string.text"
msgid "Image 1: This is image 1"
msgstr "प्रतिमा 1: हे प्रतिमा 1 आहे"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_OUTLINE\n"
"string.text"
msgid "Heading"
msgstr "मथळा"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_TABLE\n"
"string.text"
msgid "Table"
msgstr "तक्ता"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_FRAME\n"
"string.text"
msgid "Text frame"
msgstr "मजकूर चौकट"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_GRAPHIC\n"
"string.text"
msgid "Image"
msgstr "प्रतिमा"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_OLE\n"
"string.text"
msgid "OLE object"
msgstr "OLE वस्तु"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_BOOKMARK\n"
"string.text"
msgid "Bookmark"
msgstr "वाचनखूण"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_REGION\n"
"string.text"
msgid "Section"
msgstr "भाग"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_URLFIELD\n"
"string.text"
msgid "Hyperlink"
msgstr "हाइपरलिंक"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_REFERENCE\n"
"string.text"
msgid "Reference"
msgstr "संदर्भ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_INDEX\n"
"string.text"
msgid "Index"
msgstr "अनुक्रमणिका"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_POSTIT\n"
"string.text"
msgid "Comment"
msgstr "टिप्पणी"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_DRAWOBJECT\n"
"string.text"
msgid "Draw object"
msgstr "वस्तु रेखाटा"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_DEFINE_NUMBERFORMAT\n"
"string.text"
msgid "Additional formats..."
msgstr "अतिरिक्त स्वरूपणे..."

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_STR_SYSTEM\n"
"string.text"
msgid "[System]"
msgstr "[System]"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_MULT_INTERACT_SPELL_WARN\n"
"string.text"
msgid ""
"The interactive spellcheck is already active\n"
"in a different document"
msgstr ""
"संवादी शुध्दलेखन तपासणी आधीपासूनच दुसऱ्या \n"
"दस्तऐवजात सक्रिय आहे"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_MULT_INTERACT_HYPH_WARN\n"
"string.text"
msgid ""
"The interactive hyphenation is already active\n"
"in a different document"
msgstr ""
"संवादी संयोगचिह्नांकन आधीपासूनच दुसऱ्या\n"
"दस्तऐवजात सक्रिय आहे"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_SPELL_TITLE\n"
"string.text"
msgid "Spellcheck"
msgstr "शुध्दलेखन तपासणी"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_HYPH_TITLE\n"
"string.text"
msgid "Hyphenation"
msgstr "संयोगचिह्नांकन"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_HYPERCTRL_SEL\n"
"string.text"
msgid "SEL"
msgstr "SEL"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_HYPERCTRL_HYP\n"
"string.text"
msgid "HYP"
msgstr "HYP"

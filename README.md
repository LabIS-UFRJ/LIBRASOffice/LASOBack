# LASOback (LASO)
----

## O que é?

É o núcleo do LibreOffice 5.3.7.2 modificado para a implementação do LIBRASOffice. As modificações consistem em adição de códigos e arquivos.

----
## Alterações no core do LO

Todas as alterações realizadas são marcadas pelas tags ADDLIBRAS (início da alteração) e ENDLIBRAS (fim da alteração).

Nesta fase inicial, enquanto exploramos o grande código-fonte (literalmente) do LibreOffice, vamos manter manualmente o rastreamento dos arquivos alterados, para facilitar troca de versões-base:

- C:\cygwin\home\lipedev\lode\dev\LASOBack\vcl\inc\helpwin.hxx

- C:\cygwin\home\lipedev\lode\dev\ LASOBack \vcl\source\app\help.cxx

- C:\cygwin\home\lipedev\lode\dev\ LASOBack \vcl\source\app\svmain.cxx

- C:\cygwin\home\lipedev\lode\dev\ LASOBack \vcl\source\app\DefDirLaso.h * : Reconhece o diretório onde o LIBRASOffice está instalado e salva numa string 

- C:\cygwin\home\lipedev\lode\dev\LASOBack\include\LASO.hxx * : Define o diretório do arquivo LASO.log e DEBUG.log como a pasta AppData do usuário que instalou o LIBRASOffice a partir do endereço guardado no DefDirLaso.h

- C: \cygwin\home\lipedev\lode\dev\LASOBack\LASOFront_dist * : pasta adicionada com o [LASOFront](https://gitlab.com/LabIS-UFRJ/LIBRASOffice/LASOFront)

##### Observação: 

> versão base do Libre Office usada no LIBRASOffice é a 5.3.7.2

> '*' cógidos adicionados ao core do LibreOffice.

----

## Construindo LASO no Windows

Esta página guarda um tutorial (atualizado, sempre que possível) sobre como instalar e configurar o ambiente e as ferramentas de desenvolvimento necessárias para uma construção (build) bem-sucedida do LASO no Windows.

Para construir o LIBRASOffice no Windows utilizamos o **lode** (acrônimo de LibreOffice Development Environment), que automatiza uma série de tarefas necessárias para obtermos executáveis do LibreOffice para Windows. Da página oficial o **lode** "é um projeto que ajuda você a configurar um ambiente base para construir o LibreOffice. O objetivo é simplificar a configuração das dependências e ferramentas necessárias para construir o LibreOffice. Isso visa obter novos desenvolvedores de forma rápida e também ajudar a configurar os escravos **tinderbox** e **jenkins**.


**Pré-requisitos**

- [Visual Studio 2013 Update 5](http://download.microsoft.com/download/A/A/D/AAD1AA11-FF9A-4B3C-8601-054E89260B78/vs2013.5_ce_enu.iso) (pode ser a gratuita Community Edition) vs2013.5_ce_enu.iso

- [32-bit(x86) Java Development Kit (JDK)](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) última sub-versão da versão 8.

##### Observação:

> O LIBRASOffice utiliza o núcleo de uma versão antiga do LibreOffice, 
por isso os requisitos para a compilação não são os mesmos encontrados 
no tutorial atual de construção do LibreOffice.

> Após a instalação do JDK, é necessário adicionar a pasta bin do Java na variável de ambiente PATH.  
    - A pasta bin comumente está em C:\Program Files (x86)\Java\jdk1.8.0_xxx.   
    - Sigo o [Tutorial para adicionar na variável de ambiente PATH](https://www.java.com/pt_BR/download/help/path.xml)

**Tutorial Windows**

- [Getting Started](https://wiki.documentfoundation.org/Development/lode)

##### Observação:

> Não é necessário construir o LODE, basta seguir as etapas até antes das instruções para construção.

Após seguir os passos do tutorial, teremos os executáveis da última versão de desenvolvimento do LibreOffice disponíveis em algum sub-diretório do diretório core, geralmente em

    core/instdir/program

A partir deste momento, estamos prontos para construir o LIBRASOffice para Windows. No diretório 

    ~/lode/dev

Digite os seguintes comandos

    git clone https://gitlab.com/LabIS-UFRJ/LIBRASOffice/LASOBack.git
    cd LASOBack
    ./autogen.sh
    make -k

**Demais referências**

- [Building LibreOffice on Windows with Cygwin and MSVC: Tips and Tricks](https://wiki.documentfoundation.org/Development/BuildingOnWindows) (em inglês)
- [LibreOffice Build Demo](https://www.youtube.com/watch?v=2gIqOOajdYQ&hd=1) (em inglês)

## Construindo LASO no GNU/Linux
----

**Ponto de partida**

- [Tutorial inicial de compilação do LibreOffice no Linux](https://pt-br.libreoffice.org/sobre-nos/codigo-fonte/)

**Referência**

- [Document Foundation](https://wiki.documentfoundation.org/Development/BuildingOnLinux) (em inglês)

## Entendendo o LIBRASOffice
----
> O algoritmo funciona da seguinte maneira:

> - Identificar da rotina de renderização das "janelinhas de ajuda" (tooltip/helptext). **(Backend)**

> - Extrair texto a ser exibido na janelinha para um arquivo de Log. **(Backend)**

> - Capturar linha de log, analisar e exibir sinal correspondente. **(Frontend)**

Este processo foi estabelecido de forma a poder ser repetido em outros programas.


## Glossário
----

Esta página guarda um glossário dos termos e abreviações conceituais e técnicas utilizados no projeto.

- ECI: curso de graduação em Engenharia de Computação e Informação da UFRJ, acesse [aqui](http://www.poli.ufrj.br/graduacao_cursos_engenharia_computacao_informacao.php.) 

- LASO: abreviação de LIBRASOffice

- LO: abreveviação LibreOffice, acesse [aqui](https://pt-br.libreoffice.org)

- lode: LibreOffice Development Environment, acesse [aqui](https://wiki.documentfoundation.org/Development/lode)

- LIpE: acrônimo para Laboratório de Informática para Educação da UFRJ

- UFRJ: sigla para Universidade Federal do Rio de Janeiro, acesse [aqui](https://ufrj.br)

- Backend: Alteração direta no código-base C++ do LibreOffice.

- Frontend: Exibição de animação dos sinais. Feito em Java.

## Issues
----

- [X] **[RESOLVIDO]** Assistentes de inserção de fórmulas. (Jonathan)

- [ ] Instalador portátil.

- [X] **[RESOLVIDO]**Versão para Linux. (Rodrigo e Luiza) 

- [ ] Painel de configurações. (Ana Lúcia e Eduardo)

- [ ] **[RESOLVIDO]** Modificação de código-fonte para adaptação de menus (extração do texto do item de menu a ser selecionado). (Jonathan)

- [ ] **[RESOLVIDO]** Migrar base de código-fonte para última versão estável do LibreOffice. (Miguel e Jonathan) **(Versão atual: 5.3.7.2)**

- [ ] "Módulo" para adição de sinais ao projeto diretamente pelo usuário. Integração do LibrasOffice com GoogleDrive – Lógica (Daniel)

- [ ] "Módulo" para adição de sinais ao projeto diretamente pelo usuário. Integração do LibrasOffice com GoogleDrive – Gráfica (Ana Lúcia)

- [ ] Organizar referências do desenvolvimento

- [ ] Fixar bugs de execução em distros não-mint

- [ ] Implementar kill do frontend pelo PID, no Windows

- [X] **[RESOLVIDO]** Mover tempdir para AppData no Windows. (Lidiana)

- [ ] Revisar instaladores LIBRASOffice

- [ ] Implementar splash-screen de abertura para LIBRAS

- [ ] Implementar tradução para LIBRAS na abertura do LibreOffice

- [ ] Compilar branch master em máquina pessoal


## changelog LIBRASOffice
* 01-Out-2019 por [Lidiana](https://gitlab.com/Lidiana)

# LibreOffice
[![Coverity Scan Build Status](https://scan.coverity.com/projects/211/badge.svg)](https://scan.coverity.com/projects/211) [![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/307/badge)](https://bestpractices.coreinfrastructure.org/projects/307)

LibreOffice is an integrated office suite based on copyleft licenses
and compatible with most document formats and standards. Libreoffice
is backed by The Document Foundation, which represents a large
independent community of enterprises, developers and other volunteers
moved by the common goal of bringing to the market the best software
for personal productivity. LibreOffice is open source, and free to
download, use and distribute.

A quick overview of the LibreOffice code structure.

## Overview

You can develop for LibreOffice in one of two ways, one
recommended and one much less so. First the somewhat less recommended
way: it is possible to use the SDK to develop an extension,
for which you can read the API docs [here](http://api.libreoffice.org/)
and [here](http://wiki.services.openoffice.org/wiki/Documentation/DevGuide).
This re-uses the (extremely generic) UNO APIs that are also used by
macro scripting in StarBasic.

The best way to add a generally useful feature to LibreOffice
is to work on the code base however. Overall this way makes it easier
to compile and build your code, it avoids any arbitrary limitations of
our scripting APIs, and in general is far more simple and intuitive -
if you are a reasonably able C++ programmer.


## The important bits of code

Each module should have a `README` file inside it which has some
degree of documentation for that module; patches are most welcome to
improve those. We have those turned into a web page here:

http://docs.libreoffice.org/

However, there are two hundred modules, many of them of only
peripheral interest for a specialist audience. So - where is the
good stuff, the code that is most useful. Here is a quick overview of
the most important ones:

Module    | Description
----------|-------------------------------------------------
sal/      | this provides a simple System Abstraction Layer
tools/    | this provides basic internal types: 'Rectangle', 'Color' etc.
vcl/      | this is the widget toolkit library and one rendering abstraction
framework | UNO framework, responsible for building toolbars, menus, status bars, and the chrome around the document using widgets from VCL, and XML descriptions from */uiconfig/* files
sfx2/     | legacy core framework used by Writer/Calc/Draw: document model / load/save / signals for actions etc.
svx/      | drawing model related helper code, including much of Draw/Impress

Then applications

Module    | Description
----------|-------------------------------------------------
desktop/  | this is where the 'main' for the application lives, init / bootstrap. the name dates back to an ancient StarOffice that also drew a desktop
sw/       | Writer
sc/       | Calc
sd/       | Draw / Impress

There are several other libraries that are helpful from a graphical perspective:

Module    | Description
----------|-------------------------------------------------
basegfx/  | algorithms and data-types for graphics as used in the canvas
canvas/   | new (UNO) canvas rendering model with various backends
cppcanvas/ | C++ helper classes for using the UNO canvas
drawinglayer/ | View code to render drawable objects and break them down into primitives we can render more easily.


## Finding out more

Beyond this, you can read the `README` files, send us patches, ask
on the mailing list libreoffice@lists.freedesktop.org (no subscription
required) or poke people on IRC `#libreoffice-dev` on irc.freenode.net -
we're a friendly and generally helpful mob. We know the code can be
hard to get into at first, and so there are no silly questions.

